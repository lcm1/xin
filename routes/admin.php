<?php

/*
| 后台管理平台接口
*/
Route::group(['prefix' => 'admin'], function() {
//
	//登录
	Route::group(['prefix' => 'login'], function() {
		//////////// 登录
		//登陆--页面
		Route::get('login_html', 'Admin\LoginController@login_html');
		//登陆
		Route::post('login', 'Admin\LoginController@login');
		//退出登录
		Route::post('quit', 'Admin\LoginController@quit');
		
		//////////// 测试
		//编辑OA订单信息
		Route::get('order_update', 'Admin\LoginController@order_update');
		//同步OA订单信息
		Route::get('synchro', 'Admin\LoginController@synchro');
		
		
	});
	
	//首页
	Route::group(['prefix' => 'index', 'middleware' => ['LoginAdmin']], function() {
		//////////// 首页
		//首页--页面
		Route::get('index_html', 'Admin\IndexController@index_html');
		//默认首页--页面
		Route::get('default_index_html', 'Admin\IndexController@default_index_html');
		//验证登录状态
		Route::post('islogin', 'Admin\IndexController@islogin');
		
		
	});
	
	//权限
	Route::group(['prefix' => 'powers', 'middleware' => ['LoginAdmin']], function() {
		//////////// 权限列表
		//权限列表--页面
		Route::get('power_list_html', 'Admin\PowersController@power_list_html');
		//编辑权限分类
		Route::post('powertype_update', 'Admin\PowersController@powertype_update');
		//新增权限分类
		Route::post('powertype_add', 'Admin\PowersController@powertype_add');
		//权限列表
		Route::post('power_list', 'Admin\PowersController@power_list');
		//新增权限
		Route::post('power_add', 'Admin\PowersController@power_add');
		//权限信息
		Route::post('power_info', 'Admin\PowersController@power_info');
		//编辑权限
		Route::post('power_update', 'Admin\PowersController@power_update');
		//删除权限
		Route::post('power_del', 'Admin\PowersController@power_del');
		
		
		Route::post('', 'Admin\PowersController@');
	});
	
	//角色
	Route::group(['prefix' => 'role', 'middleware' => ['LoginAdmin']], function() {
		//////////// 角色列表
		//角色列表--页面
		Route::get('role_list_html', 'Admin\RoleController@role_list_html');
		//角色列表--数据
		Route::post('role_list', 'Admin\RoleController@role_list');
		//新增角色
		Route::post('role_add', 'Admin\RoleController@role_add');
		//角色信息
		Route::post('role_info', 'Admin\RoleController@role_info');
		//编辑角色
		Route::post('role_update', 'Admin\RoleController@role_update');
		//删除角色
		Route::post('role_del', 'Admin\RoleController@role_del');
		
		//////////// 角色权限
		//角色权限--页面
		Route::get('powers_role_html', 'Admin\RoleController@powers_role_html');
		//编辑角色权限
		Route::post('powers_role', 'Admin\RoleController@powers_role');
		
		
		Route::post('', 'Admin\RoleController@');
	});
	
	//用户
	Route::group(['prefix' => 'user', 'middleware' => ['LoginAdmin']], function() {
		//////////// 用户列表
		//用户列表--页面
		Route::get('user_list_html', 'Admin\UserController@user_list_html');
		//用户列表--数据
		Route::post('user_list', 'Admin\UserController@user_list');
		//新增用户
		Route::post('user_add', 'Admin\UserController@user_add');
		//用户信息
		Route::post('user_info', 'Admin\UserController@user_info');
		//编辑用户
		Route::post('user_update', 'Admin\UserController@user_update');
		//获取用户角色
		Route::post('user_role_get', 'Admin\UserController@user_role_get');
		//修改用户角色
		Route::post('user_role_update', 'Admin\UserController@user_role_update');
		
		
		Route::post('', 'Admin\UserController@');
	});
	
	
	//菜单
	Route::group(['prefix' => 'menu', 'middleware' => ['LoginAdmin']], function() {
		//////////// 菜单列表
		//菜单列表--页面
		Route::get('menu_list_html', 'Admin\MenuController@menu_list_html');
		//菜单列表
		Route::post('menu_list', 'Admin\MenuController@menu_list');
		//新增菜单
		Route::post('menu_add', 'Admin\MenuController@menu_add');
		//菜单信息
		Route::post('menu_info', 'Admin\MenuController@menu_info');
		//编辑菜单
		Route::post('menu_update', 'Admin\MenuController@menu_update');
		//删除菜单
		Route::post('menu_del', 'Admin\MenuController@menu_del');
		
		Route::post('', 'Admin\MenuController@');
	});
	
	//系统配置
	Route::group(['prefix' => 'config', 'middleware' => ['LoginAdmin']], function() {
		//////////// 系统配置
		//系统配置--页面
		Route::get('config_html', 'Admin\ConfigController@config_html');
		//系统配置--数据
		Route::post('config_find', 'Admin\ConfigController@config_find');
		//秘钥验证
		Route::post('config_update', 'Admin\ConfigController@config_update');
	});
	
	
	//登录
	Route::group(['prefix' => 'test'], function() {
		//sql测试
		Route::get('sql_test', 'Admin\TestController@sql_test');
		
		//新增订单数据
		Route::get('order_data', 'Admin\TestController@order_data');
		//新增数据
		Route::get('data_add', 'Admin\TestController@data_add');
		
		
		
	});
	
	
	
	
	
});
	
