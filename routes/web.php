<?php
// 前端路由

/*
| Pc路由
*/
Route::group(['prefix' => 'pc', 'namespace' => 'Pc', 'middleware' => ['CrossHttp']], function () {
    //登录
    Route::group(['prefix' => 'login'], function () {
        //////////// 登录
        //登陆
        Route::post('login', 'LoginController@login');
        // 自动登录
        Route::post('login_zd', 'LoginController@login_zd');
        //注册
        Route::post('register', 'LoginController@register');


        //Api调用--测试
        Route::get('api_test', 'LoginController@api_test');
        //订单超期提醒
        Route::get('overdue_remind', 'LoginController@overdue_remind');


        // 任务定时器触发
        Route::get('timer_trigger', 'LoginController@timer_trigger');

    });

    //首页
    Route::group(['prefix' => 'index', 'middleware' => ['ValidateLogon']], function () {
        //获取七牛云token
        Route::post('qiniu_token', 'IndexController@qiniu_token');
        //菜单
        Route::post('menu_list', 'IndexController@menu_list');
        //权限验证
        Route::post('powers_checking', 'IndexController@powers_checking');
        //未读消息
        Route::post('tiding_not', 'IndexController@tiding_not');
        //消息弹窗
        Route::post('tiding_task', 'IndexController@tiding_task');
        //阿里云oss
        Route::post('oss_add', 'IndexController@oss_add');
        //spu图片
        Route::post('get_spu_img', 'IndexController@get_spu_img');
        //菜单初始化列表
        Route::post('getMenuList', 'IndexController@getMenuInitList');
        //首页公告
        Route::post('getAnnouncement', 'AnnouncementController@getAnnouncement');
        //首页公告 - 列表
        Route::post('getAnnouncementList', 'AnnouncementController@getAnnouncementList');
        //首页公告 - 新增
        Route::post('addAnnouncement', 'AnnouncementController@addAnnouncement');
        //首页公告 - 编辑
        Route::post('editAnnouncement', 'AnnouncementController@editAnnouncement');
        //首页公告 - 删除
        Route::post('delAnnouncement', 'AnnouncementController@delAnnouncement');

        //说明文档-列表
        Route::get('GetReadMeList', 'IndexController@GetReadMeList');
        //说明文档-新增修改
        Route::post('SaveReadMe', 'IndexController@SaveReadMe');
        //说明文档-删除
        Route::post('DelReadMe', 'IndexController@DelReadMe');
        //说明文档-一级分类
        Route::get('ReadMeCate', 'IndexController@ReadMeCate');
        Route::get('GetReadMeDetail', 'IndexController@GetReadMeDetail');
        
        
    });

    // 用户
    Route::group(['prefix' => 'user', 'middleware' => ['ValidateLogon']], function () {
        ///////// 用户列表
        //用户列表--数据
        Route::post('user_list', 'UserController@user_list');
        //新增用户
        Route::post('user_add', 'UserController@user_add');
        //编辑用户
        Route::post('user_update', 'UserController@user_update');
        //删除用户
        Route::post('user_del', 'UserController@user_del');
        //获取角色
        Route::post('role_list', 'UserController@role_list');
        //获取用户角色
        Route::post('user_role_get', 'UserController@user_role_get');
        //修改用户角色
        Route::post('user_role_update', 'UserController@user_role_update');
        //用户拉入部门
        Route::post('organize_add', 'UserController@organize_add');
        ///////// 个人中心
        //消息列表--数据
        Route::post('tiding_list', 'UserController@tiding_list');
        //编辑消息
        Route::post('tiding_update', 'UserController@tiding_update');
        //修改密码
        Route::post('password_update', 'UserController@password_update');


    });

    // 权限
    Route::group(['prefix' => 'powers', 'middleware' => ['ValidateLogon']], function () {
        ///////// 权限类别列表
        //权限类别列表--数据
        Route::post('powers_type_list', 'PowersController@powers_type_list');
        //新增权限类别
        Route::post('powers_type_add', 'PowersController@powers_type_add');
        //编辑权限类别
        Route::post('powers_type_update', 'PowersController@powers_type_update');
        //权限列表
        Route::post('powers_list', 'PowersController@powers_list');
        //新增权限
        Route::post('powers_add', 'PowersController@powers_add');
        //权限信息
        Route::post('powers_info', 'PowersController@powers_info');
        //编辑权限
        Route::post('powers_update', 'PowersController@powers_update');
        //删除权限
        Route::post('powers_del', 'PowersController@powers_del');
    });

    //菜单
    Route::group(['prefix' => 'menu', 'middleware' => ['ValidateLogon']], function () {
        //////////// 菜单列表
        //菜单列表
        Route::post('menu_list', 'MenuController@menu_list');
        //新增菜单
        Route::post('menu_add', 'MenuController@menu_add');
        //菜单信息
        Route::post('menu_info', 'MenuController@menu_info');
        //编辑菜单
        Route::post('menu_update', 'MenuController@menu_update');
        //删除菜单
        Route::post('menu_del', 'MenuController@menu_del');
        // 菜单查询列表
        Route::post('getMenuList', 'MenuController@getMenuList');
        // 新增菜单文档
        Route::post('menu_doc_add', 'MenuController@menu_doc_add');
        // 修改菜单文档
        Route::post('menu_doc_update', 'MenuController@menu_doc_update');
        // 文档内容查询列表
        Route::get('menu_doc_select', 'MenuController@menu_doc_select');
        // 文档子菜单查询列表
        Route::get('menus_select', 'MenuController@menus_select');
    });

    //角色
    Route::group(['prefix' => 'role', 'middleware' => ['ValidateLogon']], function () {
        //////////// 角色列表
        //角色列表--数据
        Route::post('role_list', 'RoleController@role_list');
        //新增角色
        Route::post('role_add', 'RoleController@role_add');
        //角色信息
        Route::post('role_info', 'RoleController@role_info');
        //编辑角色
        Route::post('role_update', 'RoleController@role_update');
        //删除角色
        Route::post('role_del', 'RoleController@role_del');

        //////////// 角色权限
        //角色权限--页面
        Route::post('powers_role', 'RoleController@powers_role');
        //编辑角色权限
        Route::post('powers_role_update', 'RoleController@powers_role_update');
        //Route::post('powers_role_check', 'RoleController@powers_role_check');


        Route::post('', 'Admin\RoleController@');
    });

    //组织（部门、小组）
    Route::group(['prefix' => 'organize', 'middleware' => ['ValidateLogon']], function () {
        //////////// 部门列表
        //组织列表--数据
        Route::post('organize_list', 'OrganizeController@organize_list');
        //新增组织
        Route::post('organize_add_depart', 'OrganizeController@organize_add_depart');
        //编辑组织
        Route::post('organize_update', 'OrganizeController@organize_update');
        //删除组织
        Route::post('organize_del', 'OrganizeController@organize_del');
        //成员列表--数据
        Route::post('member_list', 'OrganizeController@member_list');
        //用户列表
        Route::post('user_list', 'OrganizeController@user_list');
        //获取可新增成员列表--数据
        Route::post('user_data', 'OrganizeController@user_data');
        //新增成员
        Route::post('member_add', 'OrganizeController@member_add');
        //编辑组织成员信息
        Route::post('member_update', 'OrganizeController@member_update');
        //移除成员
        Route::post('member_del', 'OrganizeController@member_del');
        //部门新增成员
        Route::post('organize_add', 'UserController@organize_add');

        //获取部门树状图
        Route::get('getChildrenOfA', 'OrganizeController@getChildrenOfA');

        
        Route::post('', 'OrganizeController@');
    });

    //任务
    Route::group(['prefix' => 'task', 'middleware' => ['ValidateLogon']], function () {
//	Route::group(['prefix' => 'task'], function() {
        //////////// 新增任务页面
        //用户列表
        Route::post('user_list', 'TaskController@user_list');
        //部门用户列表
        Route::post('department_user', 'TaskController@department_user');
        // 获取组织列表
        Route::post('getDepartmentList', 'TaskController@getDepartmentList');
        // 任务模板列表
        Route::post('template_list', 'TaskController@template_list');
        //新增任务
        Route::post('task_add', 'TaskController@task_add');
        //获取任务模板分类
        Route::get('getTaskModel', 'TaskController@getTaskModel');
        //////////// 任务列表
        //任务列表--数据
        Route::post('task_list', 'TaskController@task_list');
        //任务审核详情
        Route::post('tasks_examine_info', 'TaskController@tasks_examine_info');
        // 删除任务
        Route::post('task_del', 'TaskController@task_del');
        //任务审核
        Route::post('tasks_examine', 'TaskController@tasks_examine');

        //////////// 任务详情页面
        //任务详情
        Route::post('task_info', 'TaskController@task_info');
        //编辑节点任务
        Route::post('node_task_update', 'TaskController@node_task_update');
        //节点任务--（完成、放弃）
        Route::post('task_son_complete', 'TaskController@task_son_complete');
        //编辑任务
        Route::post('task_update', 'TaskController@task_update');
        //任务消息
        Route::post('tasks_news_list', 'TaskController@tasks_news_list');
        //新增任务消息
        Route::post('task_new_add', 'TaskController@task_new_add');
        // 删除任务消息
        Route::post('task_new_del', 'TaskController@task_new_del');
        // 新增节点任务
        Route::post('node_task_add', 'TaskController@node_task_add');
        // 验收任务
        Route::post('task_determine', 'TaskController@task_determine');

        //////////// 待审核
        //待审核列表--数据
        Route::post('task_audit_list', 'TaskController@task_audit_list');
        //已审核列表--数据
        Route::post('task_reviewed_list', 'TaskController@task_reviewed_list');

        //////////// 任务模板
        //任务模板列表--数据
        Route::post('task_template_list', 'TaskController@task_template_list');
        //新增任务模板
        Route::post('task_template_add', 'TaskController@task_template_add');
        //任务模板分类
        Route::get('task_template_class', 'TaskController@task_template_class');
        // 编辑任务模板
        Route::post('task_template_update', 'TaskController@task_template_update');
        //////////// 任务模板
        // 我的任务列表--数据
        Route::post('task_me', 'TaskController@task_me');
        //////////// 任务定时器
        // 任务定时器--数据
        Route::post('task_timer', 'TaskController@task_timer');
        // 任务列表
        Route::post('task_sel', 'TaskController@task_sel');
        // 新增任务定时器
        Route::post('task_timer_add', 'TaskController@task_timer_add');
        // 编辑任务定时器
        Route::post('task_timer_update', 'TaskController@task_timer_update');
        // 删除任务定时器
        Route::post('task_timer_del', 'TaskController@task_timer_del');
        //关联模板查找
        Route::post('choice_template', 'TaskController@choice_template');
        //删除未发布任务的补货计划
        Route::post('delete_replenishment_data', 'TaskController@delete_replenishment_data');
        //任务分类
        Route::post('task_class', 'TaskController@task_class');
        //接受任务
        Route::post('task_accept', 'TaskController@task_accept');
        //修改进度说明
        Route::post('task_text_update', 'TaskController@task_text_update');
        //反馈任务进度
        Route::post('task_feedback', 'TaskController@task_feedback');
        //任务打分
        Route::post('task_score', 'TaskController@task_score');
        //我发布的任务
        Route::post('task_release', 'TaskController@task_release');
        //任务数据统计
        Route::post('tasks_data', 'TaskController@tasks_data');
        //待验收任务
        Route::post('task_acceptance', 'TaskController@task_acceptance');
        //是否总经办介入
        Route::post('general_office_intervened', 'TaskController@general_office_intervened');
        //亚马逊站内活动任务字段接口
        Route::post('activity_api', 'TaskController@activity_api');
        //亚马逊站外活动任务字段接口
        Route::post('activity_outside_api', 'TaskController@activity_outside_api');
        //生产下单是否新品选择接口
        Route::post('is_new_api', 'TaskController@is_new_api');
        //任务附件上传接口
        Route::post('file_upload', 'TaskController@file_upload');
        //审核建议接口
        Route::post('examine_advice', 'TaskController@examine_advice');
        // 我的任务重新指派执行人
        Route::post('reassignEnforcer', 'TaskController@reassignEnforcer');
        // 获取补货单任务
        Route::get('getBuhuoRequestTaskInfo', 'TaskController@getBuhuoRequestTaskInfo');
        //添加审核人
        Route::post('AddTaskUser', 'TaskController@AddTaskUser');
        //拷贝任务
        Route::post('CopyTask', 'TaskController@CopyTask');
        // 获取历史任务
        Route::get('getHistoryTaskInfo', 'TaskController@getHistoryTaskInfo');
        // 获取任务评论及审核反馈列表
        Route::post('getTaskNewAllList', 'TaskController@getTaskNewAllList');
        // 获取任务列表统计
        Route::get('task_list_statistics', 'TaskController@task_list_statistics');
        // 获取待审核任务列表统计
        Route::get('task_audit_list_statistics', 'TaskController@task_audit_list_statistics');
        // 获取已审核任务列表统计
        Route::get('task_reviewed_list_statistics', 'TaskController@task_reviewed_list_statistics');
        // 获取上级user_id
        Route::get('get_superiors', 'TaskController@get_superiors');
        // 获取个人任务详情列表
        Route::get('getTaskDetailList', 'TaskController@getTaskDetailList');
        // 获取个人任务详情打分
        Route::post('saveTasksSonScore', 'TaskController@saveTasksSonScore');
        // 检测任务是否可以复制
        Route::post('copyTasks', 'TaskController@copyTasks');
    });

    // 国家
    Route::group(['prefix' => 'country', 'middleware' => ['ValidateLogon']], function () {
        //////////// 类目配置
        // 国家列表--数据
        Route::post('country_list', 'CountryController@country_list');
        Route::post('getAllCountryList', 'CountryController@getAllCountryList');
        // 新增国家
        Route::post('country_add', 'CountryController@country_add');
        // 编辑国家
        Route::post('country_update', 'CountryController@country_update');
        // 删除国家
        Route::post('country_del', 'CountryController@country_del');
        // 国家列表--数据
        Route::post('country_currency', 'CountryController@country_currency');

        Route::post('', 'CountryController@');
    });

    // api任务
    Route::group(['prefix' => 'apiplan', 'middleware' => ['ValidateLogon']], function () {
        // 新增
        Route::post('add', 'ApiplanController@add');
        // 列表--数据
        Route::post('index', 'ApiplanController@index');
        // 编辑
        Route::post('update', 'ApiplanController@update');
        // 店铺
        Route::post('shop', 'ApiplanController@shop');
        // 仓库
        Route::post('warehouse', 'ApiplanController@warehouse');

    });

    // 任务模板分类
    Route::group(['prefix' => 'taskclass', 'middleware' => ['ValidateLogon']], function () {
        // 新增
        Route::post('add', 'TaskclassController@add');
        // 列表--数据
        Route::post('index', 'TaskclassController@index');
        // 编辑
        Route::post('update', 'TaskclassController@update');
        // 列表--数据
        Route::post('class_list', 'TaskclassController@class_list');
        // 列表--模块
        Route::post('module', 'TaskclassController@module');
        // 列表
        Route::post('info', 'TaskclassController@info');
        // 删除
        Route::post('del', 'TaskclassController@del');
        //绑定模板
        Route::post('template_binding', 'TaskclassController@template_binding');
        // 保存分类
        Route::post('saveTaskClass', 'TaskclassController@saveTaskClass');
        // 获取列表
        Route::get('getSelect', 'TaskclassController@getSelect');
    });

    //汇率
    Route::group(['prefix' => 'huilv', 'middleware' => ['ValidateLogon']], function () {
        // 新增汇率
        Route::post('huilv_add', 'HuilvController@huilv_add');
        // 汇率列表--数据
        Route::post('huilv_list', 'HuilvController@huilv_list');
        // 编辑汇率
        Route::post('huilv_update', 'HuilvController@huilv_update');
        // 审核
        Route::post('huilv_update_stat', 'HuilvController@update_stat');
    });

    // 平台
    Route::group(['prefix' => 'platform', 'middleware' => ['ValidateLogon']], function () {
        //////////// 类目配置
        // 平台列表--数据
        Route::post('platform_list', 'PlatformController@platform_list');
        // 新增平台
        Route::post('platform_add', 'PlatformController@platform_add');
        // 编辑平台
        Route::post('platform_update', 'PlatformController@platform_update');
        // 删除平台
        Route::post('platform_del', 'PlatformController@platform_del');


        Route::post('', 'PlatformController@');
    });

    // 供应商
    Route::group(['prefix' => 'supplier', 'middleware' => ['ValidateLogon']], function () {
        //////////// 供应商
        // 供应商列表--数据
        Route::post('supplier_list', 'SupplierController@supplier_list');
        // 新增供应商
        Route::post('supplier_add', 'SupplierController@supplier_add');
        // 供应商详情
        Route::post('supplier_info', 'SupplierController@supplier_info');
        // 编辑平台
        Route::post('supplier_update', 'SupplierController@supplier_update');


        Route::post('', 'SupplierController@');
    });


    // SPU管理
    Route::group(['prefix' => 'spu', 'middleware' => ['ValidateLogon']], function () {
        //////////// 配置
        // 配置列表--数据
        Route::post('config_list', 'SpuController@config_list');
        // 新增配置
        Route::post('config_add', 'SpuController@config_add');
        // 编辑配置
        Route::post('config_update', 'SpuController@config_update');
        // 删除配置
        Route::post('config_del', 'SpuController@config_del');

        //////////// 类目配置
        // 类目--Excel导入
        Route::post('category_import', 'SpuController@category_import');

        //////////// spu列表
        // spu列表--数据
        Route::post('spu_list', 'SpuController@spu_list');
        // 平台数据
        Route::post('platform_list', 'SpuController@platform_list');
        // 配置查找
        Route::post('config_find', 'SpuController@config_find');
        // spu预览
        Route::post('spu_preview', 'SpuController@spu_preview');
        // 新增spu
        Route::post('spu_add', 'SpuController@spu_add');

        //////////// spu详情页面
        // 配置数据
        Route::post('get_allocation', 'SpuController@get_allocation');
        // 类目
        Route::post('pattern', 'SpuController@pattern');
        // spu保存信息
        Route::post('spu_conserve', 'SpuController@spu_conserve');
        // spu审核
        Route::post('spu_examine', 'SpuController@spu_examine');
        // 回退完善
        Route::post('spu_back', 'SpuController@spu_back');
        //回退
        Route::post('spu_rollback', 'SpuController@spu_rollback');
        // spu中的sku
        Route::post('spu_sku_list', 'SpuController@spu_sku_list');
        // 最终确认
        Route::post('spu_confirm', 'SpuController@spu_confirm');

        //////////// sku列表页面
        // sku列表--数据
        Route::post('sku_list', 'SpuController@sku_list');
        // sku导出
        Route::get('sku_export', 'SpuController@sku_export');
        // 编辑sku
        Route::post('sku_update', 'SpuController@sku_update');
        // 删除sku
        Route::post('sku_del', 'SpuController@sku_del');
        // 测试
        Route::post('test', 'SpuController@test');

        //对接平台
        //获取店铺分类
        Route::post('main_classification', 'SpuController@main_classification');
        //刷新lazada分类
        Route::post('category_lzd', 'SpuController@category_lzd');
        //上传到lazada平台
        Route::post('create_lzd', 'SpuController@create_lzd');
        //获取lazada品牌
        Route::post('brand_lzd', 'SpuController@brand_lzd');
        //Lazada获取字段数据
        Route::post('data_lzd', 'SpuController@data_lzd');
        //上传到vova平台
        Route::post('create_vova', 'SpuController@create_vova');
        //刷新vova分类
        Route::post('category_vova', 'SpuController@category_vova');
        //vova尺码上传
        Route::post('vova_size', 'SpuController@vova_size');
        //上传到wish平台
        Route::post('create_wish', 'SpuController@create_wish');
//		Route::get('create_wish', 'SpuController@create_wish');
        //vova获取字段数据
        Route::post('data_vova', 'SpuController@data_vova');
        //wish获取字段数据
        Route::post('data_wish', 'SpuController@data_wish');
        //店铺列表
        Route::post('spu_store', 'SpuController@spu_store');
        //测试上传
        Route::post('spu_test', 'SpuController@spu_test');
        Route::post('lzd_croup', 'SpuController@lzd_croup');
        // 获取spu绑定的供应商
        Route::get('getSpuBindSuppliers', 'SpuController@getSpuBindSuppliers');
        // 保存报价需求
        Route::post('saveSelfSpuQuotation', 'SpuController@saveSelfSpuQuotation');
        // spu报价
        Route::post('spuQuotation', 'SpuController@spuQuotation');
        // 获取spu报价日志列表
        Route::get('getSpuQuotationLogList', 'SpuController@getSpuQuotationLogList');
        // 获取spu报价需求列表
        Route::get('getSpuQuotationList', 'SpuController@getSpuQuotationList');
        // 删除spu报价需求
        Route::post('deleteSpuQuotationDemand', 'SpuController@deleteSpuQuotationDemand');
        // 确认spu报价需求
        Route::post('confirmQuotationCompletion', 'SpuController@confirmQuotationCompletion');
        // 保存打样需求
        Route::post('saveProofingDemand', 'SpuController@saveProofingDemand');
        // spu打样
        Route::post('spuProofing', 'SpuController@spuProofing');
        // 获取spu打样日志列表
        Route::get('getSpuProofingLogList', 'SpuController@getSpuProofingLogList');
        // 获取spu打样需求列表
        Route::get('getSpuProofingList', 'SpuController@getSpuProofingList');
        // 删除spu打样需求
        Route::post('deleteSpuProofingDemand', 'SpuController@deleteSpuProofingDemand');
        // 确认spu打样需求
        Route::post('confirmProofingCompletion', 'SpuController@confirmProofingCompletion');
        // 发布报价任务
        Route::post('publishQuotationTask', 'SpuController@publishQuotationTask');
        // 发布打样任务
        Route::post('publishProofingTask', 'SpuController@publishProofingTask');
    });

    // 店铺管理
    Route::group(['prefix' => 'store', 'middleware' => ['ValidateLogon']], function () {
        // 店铺列表--数据
        Route::post('store_list', 'StoreController@store_list');
        // 店铺信息获取
        Route::post('store_find', 'StoreController@store_find');
        // 平台列表
        Route::post('platform_list', 'StoreController@platform_list');
        //获取lazada平台产品分类
        Route::post('category_lzd', 'StoreController@category_lzd');
        //获取vova平台产品分类
        Route::post('category_vova', 'StoreController@category_vova');
        // 新增店铺
        Route::post('store_insert', 'StoreController@store_insert');
        //获取用户
        Route::post('store_user', 'StoreController@store_user');
        //获取国家
        Route::post('store_country', 'StoreController@store_country');
        //获取店铺主营分类
        Route::post('main_category', 'StoreController@main_category');
        // 编辑平台
        Route::post('store_update', 'StoreController@store_update');
        // 店铺状态修改
        Route::post('store_state', 'StoreController@store_state');
        // 获取平台分类列表
        Route::post('store_category', 'StoreController@store_category');
        // 获取平台+店铺列表
        Route::post('platform_store', 'StoreController@platform_store');
        // 店铺授权--wish
//        Route::post('store_authorize_wish', 'StoreController@store_authorize_wish');
        //店铺授权回调信息接收
        //Route::post('store_code', 'StoreController@store_code');

    });

    //平台授权
    Route::group(['prefix' => 'authorize'], function () {
        //店铺授权数据
        Route::post('authorize_list', 'AuthorizeController@authorize_list');
        //店铺授权跳转地址
        Route::post('authorize_website', 'AuthorizeController@authorize_website');
        //店铺授权回调信息接收
        Route::get('authorize_code', 'AuthorizeController@authorize_code');
        //查看授权状态
        Route::post('authorize_status', 'AuthorizeController@authorize_status');
        Route::get('authorize_test', 'AuthorizeController@authorize_test');
    });


    // 外部系统回调我们系统接口
    Route::group(['prefix' => 'callback'], function () {
        //////////// lazada
        // lazada 授权
        Route::get('authorize_lazada', 'CallbackController@authorize_lazada');
        Route::get('authorize_lazada_old', 'CallbackController@authorize_lazada_old');
        //////////// wish
        // wish 授权
        Route::get('authorize_wish', 'CallbackController@authorize_wish');

    });

    //产品
    Route::group(['prefix' => 'product', 'middleware' => ['ValidateLogon']], function () {
        //产品列表
        Route::post('getlist', 'ProductController@getList');
        //wish产品上下架
        Route::post('wishProductInStatus', 'ProductController@wishProductInStatus');
        //编辑wish主产品sku
        Route::post('updateWishParentProduct', 'ProductController@updateWishParentProduct');
        //wish子sku列表
        Route::post('getWishSkuList', 'ProductSkuController@getWishSkuList');
        //wish子sku更新
        Route::post('updateWish', 'ProductSkuController@updateWish');
        //vova编辑spu
        Route::post('vovaUpdateSpu', 'ProductController@vovaUpdateSpu');
        //vova编辑sku
        Route::post('vovaUpdateSku', 'ProductSkuController@vovaUpdateSku');
        //vova产品上下架
        Route::post('vovaGoodsSaleStatus', 'ProductController@vovaGoodsSaleStatus');
        //lazada平台spu编辑
        Route::post('lazadaSpuUpdate', 'ProductController@lazadaSpuUpdate');
        //lazada平台sku编辑
        Route::post('lazadaSkuUpdate', 'ProductSkuController@lazadaSkuUpdate');
        //lazada平台sku上传图片
        Route::post('lazadaSkuUploadImage', 'ProductSkuController@lazadaSkuUploadImage');
        //lazada添加sku
        Route::post('addLazadaSku', 'ProductSkuController@addLazadaSku');
        //vova添加sku
        Route::post('addVovaSku', 'ProductSkuController@addVovaSku');
        //wish添加sku
        Route::post('addWishSku', 'ProductSkuController@addWishSku');
        //获取未使用的sku
        Route::post('getUnusedSkuByProductId', 'SpuSkuController@getUnusedSkuByProductId');
        //条形码导出
        Route::post('barcode_export', 'ProductController@barcode_export');

        //条形码导入查询-供应链
        Route::post('product_excel_export_ck', 'ProductController@product_excel_export_ck');
        //条形码导出-供应链
        Route::post('barcode_export_ck', 'ProductController@barcode_export_ck');
        //获取条形码数据
        Route::post('getBarcode', 'ProductController@getBarcode');
        //获取条形码数据-供应链
        Route::post('getBarcodeCk', 'ProductController@getBarcodeCk');
        //获取条形码数据-平台
        Route::get('getBarcodePlatform', 'ProductController@getBarcodePlatform');
        //获取条形码数据-平台-导入查询
        Route::post('BarcodePlatformExcelImport', 'ProductController@BarcodePlatformExcelImport');

        //获取条形码数据-仓库
        Route::post('getBarcodeWare', 'ProductController@getBarcodeWare');
        Route::get('getBarcodeCkNew', 'ProductController@getBarcodeCkNew');
        //修改条形码中英文名
        Route::post('updateBarcode', 'ProductController@updateBarcode');
        //条形码导入
        Route::post('product_excel_export', 'ProductController@product_excel_export');
        //根据sku获取亚马逊商品
        Route::post('amazon_sku', 'ProductController@amazon_sku');
        Route::post('new_product_export', 'ProductController@new_product_export');
        //仓库调拨导入
        Route::post('BarcodeWareImport','ProductController@BarcodeWareImport');
        Route::post('BarcodeWareCusImport','ProductController@BarcodeWareCusImport');
        //亚马逊产品数据追踪表
        Route::post('product_trace','ProductController@product_trace');
        // 保存sku建议策略
        Route::post('saveSkuStrategy','ProductController@saveSkuStrategy');
        // sku策略详情
        Route::get('strategy_task','ProductController@strategy_task');
        //获取策略统计
        Route::get('getStrategyByUser','ProductController@getStrategyByUser');
        
    });


    //shopee数字化管理
    Route::group(['prefix' => 'shopee', 'middleware' => ['ValidateLogon']], function () {
        //shopee流量转化表
        Route::post('shopee_flow_conversion', 'ShopeeController@shopee_flow_conversion');
        //shopee店铺查询
        Route::post('shopee_shop', 'ShopeeController@shopee_shop');
        //保存量转化表转化率标准
        Route::post('shopee_flow_update', 'ShopeeController@shopee_flow_update');

        //shopee店铺资产表
        Route::post('shopee_shop_assets', 'ShopeeController@shopee_shop_assets');
        //shopee店铺资产编辑
        Route::post('shopee_assets_update', 'ShopeeController@shopee_assets_update');
        //shopee退货率排行
        Route::post('shopee_return', 'ShopeeController@shopee_return');
        //shopee退款原因
        Route::post('shopee_return_reason', 'ShopeeController@shopee_return_reason');
        //shopee广告花费排名
        Route::post('shopee_adv_group', 'ShopeeController@shopee_adv_group');
        //shopee链接广告花费排名
        Route::post('shopee_adv_cost', 'ShopeeController@shopee_adv_cost');
        //shopee库存表
        Route::post('shopee_stock', 'ShopeeController@shopee_stock');
        //shopee个人毛利
        Route::post('shopee_gross_profit', 'ShopeeController@shopee_gross_profit');

        //成本和运营费导入
        Route::post('shopeeCostImport', 'ShopeeController@shopeeCostImport');
        //成本和运营费列表
        Route::post('shopeeCostList', 'ShopeeController@shopeeCostList');
        //成本和运营费导出
        Route::get('adv_excel_export', 'ShopeeController@adv_excel_export');
        //运营费修改
        Route::post('shopee_cost_update', 'ShopeeController@shopee_cost_update');
        Route::post('shopee_week_most', 'ShopeeController@shopeeWeekMost');
        Route::post('shopee_week_most_sku', 'ShopeeController@shopeeWeekMostBySku');
        Route::post('shopee_week_most_name', 'ShopeeController@shopeeWeekMostByName');
        Route::post('shopee_shop', 'ShopeeController@shopee_shop');
        Route::get('shopee_sku_detail', 'ShopeeController@detail_sku');
        Route::get('lirun', 'ShopeeController@lirun');
        //库存导入
        Route::post('shopeeInventoryImport', 'ShopeeController@shopeeInventoryImport');
        Route::post('shopeeStockList', 'ShopeeController@shopeeStockList');
        //查询国家
        Route::post('country_list', 'ShopeeController@country_list');
        //查询用户
        Route::post('user_list', 'ShopeeController@user_list');

    });


    //数组化管理
    Route::group(['prefix' => 'digital'], function () {
        //excel导入数据
        Route::post('excel_insert', 'DigitalController@excel_insert');
        //查询亚马逊补货预警数据
        Route::post('buhuoWarn_Data', 'DigitalController@buhuoWarn_Data');
        //获取合同下单数
        Route::get('getCusNumByContract', 'DigitalController@getCusNumByContract');

        //获取下单id 
        Route::get('getOrderListByCus', 'DigitalController@getOrderListByCus');
        //亚马逊补货数据excel导入数据
        Route::post('replenish_import', 'DigitalController@replenish_Import');
        Route::post('replenish_ExcelImport_confirm', 'DigitalController@replenish_ExcelImport_confirm');
        //补货数据excel返回列表
        Route::post('replenish_excel_list', 'DigitalController@replenish_excel_list');
        //重新提交补货
        Route::post('ReSendBuhuoRequest', 'DigitalController@ReSendBuhuoRequest');
        //查询亚马逊excel数据
        Route::post('amazonExcel_data', 'DigitalController@amazonExcel_data');
        //删除skuList
        Route::post('del_skulist', 'DigitalController@del_skulist');
        //更新skulist
        Route::post('update_skulist', 'DigitalController@update_skulist');
        //生成条形码
        Route::get('make_CodeImg', 'DigitalController@make_CodeImg');
        //生成条形码 - 库存
        Route::get('make_SkuCodeImg', 'DigitalController@make_SkuCodeImg');
        //亚马逊品类数据-新增sku
        Route::post('add_sku', 'DigitalController@add_sku');
        //亚马逊品类数据-修改sku
        Route::post('update_sku', 'DigitalController@update_sku');
        //查询shopee商品广告excel数据
        Route::post('shopeeExcel_adv', 'DigitalController@shopeeExcel_adv');
        //查询shopee商品链接excel数据
        Route::post('shopeeExcel_link', 'DigitalController@shopeeExcel_link');
        //导出excel post方法
        Route::post('excel_postExport', 'DigitalController@excel_postExport');
        //导出excel get方法
        Route::get('excel_getExport', 'DigitalController@excel_getExport');
        //获取亚马逊所有品类
        Route::post('getProducetType', 'DigitalController@getProducetType');
        //获取亚马逊所有销售状态
        Route::post('getSalesType', 'DigitalController@getSalesType');
        //批量修改亚马逊销售计划增长倍数
        Route::post('updatePlan', 'DigitalController@updatePlan');
        //获取所有运营人员
        Route::post('getUser', 'DigitalController@getUser');
        //获取赛盒仓库id
        Route::get('set_fba_id', 'DigitalController@set_fba_id');
        //亚马逊补货单
        Route::post('amazon_buhuo_plan', 'DigitalController@amazon_buhuo_plan');
        //获取亚马逊补货数据
        Route::post('get_buhuoDoc', 'DigitalController@get_buhuoDoc');
        //获取亚马逊补货计划单明细数据
        Route::post('buhuoPlan_data', 'DigitalController@buhuoPlan_data');
        //亚马逊取消补货申请
        Route::post('buhuoPlan_cancel', 'DigitalController@buhuoPlan_cancel');
        //亚马逊补货完成
        Route::post('buhuoPlan_success', 'DigitalController@buhuoPlan_success');
        //亚马逊修改补货单补货数量
        Route::post('update_buhuoNum', 'DigitalController@update_buhuoNum');
        //亚马逊删除补货单补货数量
        Route::post('buhuoNum_delete', 'DigitalController@buhuoNum_delete');
        //根据获取的sku查找可能需要补货的sku
        Route::post('may_buhuo', 'DigitalController@may_buhuo');
        //亚马逊重新提交补货申请
        Route::post('buhuoPlan_again', 'DigitalController@buhuoPlan_again');
        //亚马逊上传创货件pdf
        Route::post('create_goods', 'DigitalController@create_goods');
        //查询亚马逊创货件信息
        Route::post('select_createGoods', 'DigitalController@select_createGoods');
        //修改亚马逊创货件信息
        Route::post('update_createGoods', 'DigitalController@update_createGoods');
        //保存装箱数据
        Route::post('save_box_data', 'DigitalController@save_box_data');
        //修改亚马逊补货单日期
        Route::post('update_buhuoDate', 'DigitalController@update_buhuoDate');
        //补货计划生成pdf
        Route::post('save_pdf', 'DigitalController@save_pdf');
        //生成pdf-异步
        Route::post('save_pdf_job', 'DigitalController@save_pdf_job');
        //补货计划状态
        Route::post('plan_status', 'DigitalController@plan_status');
        //补货计划导出
        Route::get('request_excel_export', 'DigitalController@request_excel_export');
        //补货详情导出
        Route::get('detailed_excel_export', 'DigitalController@detailed_excel_export');
        //装箱单导出
        Route::get('box_excel_export', 'DigitalController@box_excel_export');
        //亚马逊生产下单数据
        Route::post('production_order', 'DigitalController@production_order');
        //亚马逊生产下单导入
        Route::post('production_excel_export', 'DigitalController@production_excel_export');
        //亚马逊补货计划导出
        Route::get('plan_excel_export', 'DigitalController@plan_excel_export');
        //亚马逊创货件pdf状态更改
        Route::post('set_pdf_status', 'DigitalController@set_pdf_status');
        //补货预警导出
        Route::get('warning_export', 'DigitalController@warning_export');
        //补货预警导出
        Route::get('delete_data', 'DigitalController@delete_data');
        //新增pdf
        Route::post('add_pdf', 'DigitalController@add_pdf');
        //更新skulistimg
        Route::post('update_skulist_img', 'DigitalController@update_skulist_img');
        //获取箱号
        Route::post('get_box_id', 'DigitalController@get_box_id');
        //亚马逊销售导入任务列表
        Route::post('AmazonSalesTaskList', 'DigitalController@AmazonSalesTaskList');
        Route::post('IsToken', 'DigitalController@IsToken');
        Route::post('Is_Token', 'DigitalController@IsToken');

        //补货预警报表-新
        Route::get('getBuhuoWarnNew', 'DigitalController@getBuhuoWarnNew');
        //历史库存报表-库存
        Route::get('getHisCusInventory', 'DigitalController@getHisCusInventory');
        //历史库存报表-渠道
        Route::get('getHisSkuInventory', 'DigitalController@getHisSkuInventory');

        //补货预警自动补货任务
        Route::post('create_buhuoDoc_auto', 'DigitalController@create_buhuoDoc_auto');

        //补货预警直发仓补货
        Route::post('amazon_buhuo_planb', 'DigitalController@amazon_buhuo_planb');

          //补货预警直发仓合并导出
        Route::post('FactoryPlanExport', 'DigitalController@FactoryPlanExport');

        

        //更改任务名
        Route::post('UpdateTaskName', 'DigitalController@UpdateTaskName');

        


        // 获取上月款式补货详情列表
        Route::post('getLastMonthStyleReplenishList', 'DigitalController@getLastMonthStyleReplenishList');
        // 批量更新skulist
        Route::post('batchUpdateSkulist', 'DigitalController@batchUpdateSkulist');

        Route::get('GetPdfJonCusRes', 'DigitalController@GetPdfJonCusRes');
        
        Route::get('GetDataOrderList', 'DigitalController@GetDataOrderList');

        Route::get('GetPdfNew', 'DigitalController@GetPdfNew');

        Route::get('GetErrInventory', 'DigitalController@GetErrInventory');

        Route::post('getCustomSkuLockDetail', 'DigitalController@getCustomSkuLockDetail');

        // 判断补货清单是否有亏损策略
        Route::post('checkFasinCodeStrategy', 'DigitalController@checkFasinCodeStrategy');
        // 保存补货装箱详情
        Route::post('saveReplenishmentPackingDetail', 'DigitalController@saveReplenishmentPackingDetail');
        // 获取补货装箱详情列表
        Route::get('getReplenishmentPackingDetailList', 'DigitalController@getReplenishmentPackingDetailList');
        // 保存补货装箱APP
        Route::post('saveAmazonBoxData', 'DigitalController@saveAmazonBoxData');
        // 删除补货装箱详情
        Route::post('deleteReplenishmentPackingDetail', 'DigitalController@deleteReplenishmentPackingDetail');
        // 补货发货
        Route::post('buhuoRequestFbaShippment', 'DigitalController@buhuoRequestFbaShippment');
        //生成理货标签
        Route::get('SendLhLabel', 'DigitalController@SendLhLabel');
        //修改走船，到港时间
        Route::post('UpBuhuoBoat_time', 'DigitalController@UpBuhuoBoat_time');



        //工厂直发补货预警报表
        Route::get('getFactoryBuhuo', 'DigitalController@getFactoryBuhuo');
        Route::post('amazon_buhuo_plan_new', 'DigitalController@amazon_buhuo_plan_new');
        // 修改补货详情数量
        Route::post('updateBuhuoDetailRequestNum', 'DigitalController@updateBuhuoDetailRequestNum');
        //直发仓补货任务-自动装箱
        Route::post('FactoryautoBox', 'DigitalController@FactoryautoBox');
        //直发仓补货任务-自动余数
        Route::post('FactoryOtherBox', 'DigitalController@FactoryOtherBox');
        //直发仓补货任务-装箱详情
        Route::post('FactoryInsertBoxDetail', 'DigitalController@FactoryInsertBoxDetail');
        Route::get('FactoryInsertBoxDetailList', 'DigitalController@FactoryInsertBoxDetailList');
        //工厂直发仓库spu维度列表
        Route::get('FactoryInsertBoxDetailBySpuList', 'DigitalController@FactoryInsertBoxDetailBySpuList');
        Route::post('UpdateFactoryBoxDetail', 'DigitalController@UpdateFactoryBoxDetail');
        
        
        //工厂直发仓库存明显
        Route::get('getFactoryBuhuoNum', 'DigitalController@getFactoryBuhuoNum');
        
        //直发仓装箱修改日志
        Route::get('GetFactoryautoBoxLog', 'DigitalController@GetFactoryautoBoxLog');
        //工厂直发仓补货导入
        Route::post('Importfactory', 'DigitalController@Importfactory');
        //排柜  FactoryDelChest  FactoryUpdateChest FactoryChestList
        Route::post('FactoryDelChest', 'DigitalController@FactoryDelChest');
        Route::post('FactoryUpdateChest', 'DigitalController@FactoryUpdateChest');
        Route::get('FactoryChestList', 'DigitalController@FactoryChestList');
        Route::post('UpdateFactoryBoxDetailSpu', 'DigitalController@UpdateFactoryBoxDetailSpu');
        Route::post('UpdateFactoryBoxDetailSpus', 'DigitalController@UpdateFactoryBoxDetailSpus');
        Route::post('UpdateFactoryBoxDetailRequest', 'DigitalController@UpdateFactoryBoxDetailRequest');
        
        

        //进销存报表
        Route::get('getInventoryReportList', 'DigitalController@getInventoryReportList');
    });


    //数据统计与分析
    Route::group(['prefix' => 'datawhole'], function () {
        //获取分类
        Route::post('getcategory', 'DatawholeController@getcategory');
        //数据报表-总
        Route::post('datawhole_all', 'DatawholeController@datawhole_all');
        //数据报表-新品
        Route::post('datawhole_new', 'DatawholeController@datawhole_new');
        //数据报表-店铺列表
        Route::post('datawhole_shop', 'DatawholeController@datawhole_shop');
        //数据报表-运营列表
        Route::post('datawhole_user', 'DatawholeController@datawhole_user');
        //数据报表-SPU列表
        Route::get('datawhole_spu', 'DatawholeController@datawhole_spu');
        //数据报表-SPU列表(新)
        Route::get('DatawholeSpuNew', 'DatawholeController@DatawholeSpuNew');
        Route::get('DataWholeSpuChangeInfo', 'DatawholeController@DataWholeSpuChangeInfo');
        //库存报表-新
        Route::get('DatawholeCusNew', 'DatawholeController@DatawholeCusNew');
        //spu颜色报表-新
        Route::get('DatawholeSpuColor', 'DatawholeController@DatawholeSpuColor');
        //店铺-spu报表
        Route::get('DatawholeShopSpu', 'DatawholeController@DatawholeShopSpu');
        //spu报表计划发货人
        Route::get('DatawholeSpuGetRequestUser', 'DatawholeController@DatawholeSpuGetRequestUser');
        //店铺-spu颜色报表
        Route::get('DatawholeShopSpuColor', 'DatawholeController@DatawholeShopSpuColor');
        //店铺-库存报表
        Route::get('DatawholeShopCus', 'DatawholeController@DatawholeShopCus');
        //执行缓存任务
        Route::get('runCache', 'DatawholeController@runCache');

        //数据报表-SkU列表-新
        Route::get('DatawholeSku', 'DatawholeController@DatawholeSku');
        //数据报表-Fasin列表-新DatawholeController
        Route::get('DatawholeFasin', 'DatawholeController@DatawholeFasin');
        //数据报表-Fasin列表-店铺
        Route::get('DatawholeFasinByShop', 'DatawholeController@DatawholeFasinByShop');

        //获取fasin每日详情
        Route::get('GetFasinReport', 'DatawholeController@GetFasinReport');
        //获取fasin计划预算
        Route::get('GetFasinPlan', 'DatawholeController@GetFasinPlan');
        //fasin生产下单
        Route::get('GetFasinSkus', 'DatawholeController@GetFasinSkus');
        Route::post('SaveFasinPlaceOrder', 'DatawholeController@SaveFasinPlaceOrder');
        Route::get('GetFasinPlaceOrder', 'DatawholeController@GetFasinPlaceOrder');
        Route::post('UpdateFasinPlaceOrder', 'DatawholeController@UpdateFasinPlaceOrder');
        //保存fasin计划预算
        Route::post('SaveFasinPlan', 'DatawholeController@SaveFasinPlan');

        //fasin其他预算报表
        Route::get('GetFasinPlanOther', 'DatawholeController@GetFasinPlanOther');
        //预算审核
        Route::post('FasinPlanExamine', 'DatawholeController@FasinPlanExamine');
        //fasin预算导入
        Route::post('ImportFasinPlan', 'DatawholeController@ImportFasinPlan');
        //fasin款号预算导入
        Route::post('ImportFasinCusPlan', 'DatawholeController@ImportFasinCusPlan');

        
        
        
        
        //利润导入
        Route::post('SaveProfit', 'DatawholeController@SaveProfit');
        //查看利润
        Route::get('GetProfit', 'DatawholeController@GetProfit');
        

        //仓库盘点
        Route::post('AddCustomSkuCount', 'DatawholeController@AddCustomSkuCount');
        Route::get('CheckCus', 'DatawholeController@CheckCus');
        Route::get('GetCusCount', 'DatawholeController@GetCusCount');
        Route::get('GetCusCountSelf', 'DatawholeController@GetCusCountSelf');
        Route::get('GetCusCountSaihe', 'DatawholeController@GetCusCountSaihe');
        
        Route::post('DelCustomSkuCount', 'DatawholeController@DelCustomSkuCount');
        Route::get('CustomSkuCountByUser', 'DatawholeController@CustomSkuCountByUser');
        Route::get('CustomSkuCountByUserb', 'DatawholeController@CustomSkuCountByUserb');

        Route::get('CustomSkuCountByCodea', 'DatawholeController@CustomSkuCountByCodea');
        Route::get('CustomSkuCountByUserc', 'DatawholeController@CustomSkuCountByUserc');
        Route::get('UpdateCustomSkuCount', 'DatawholeController@UpdateCustomSkuCount');
        Route::get('Instore', 'DatawholeController@Instore');

        
        
        
        
        
        //数据报表-Fasin列表-图片
        Route::get('GetFainimgs', 'DatawholeController@GetFainimgs');
        //数据报表-Fasin列表-修改新增
        Route::post('CreateFasin', 'DatawholeController@CreateFasin');
        //数据报表-Fasin列表-删除
        Route::post('DelFasin', 'DatawholeController@DelFasin');
        //数据报表-Fasin列表-保存备注
        Route::post('saveAmazonFasinMemo', 'DatawholeController@saveAmazonFasinMemo');
        //数据报表-Fasin列表-备注列表
        Route::post('getAmazonFasinMemoList', 'DatawholeController@getAmazonFasinMemoList');
        //数据报表-Fasin列表-保存留言信息
        Route::post('saveFasinLeaveMessage', 'DatawholeController@saveFasinLeaveMessage');
        //数据报表-Fasin列表-删除留言信息
        Route::post('deleteAmazonFasinLeaveMessage', 'DatawholeController@deleteAmazonFasinLeaveMessage');
        //数据报表-Fasin列表-点赞留言信息
        Route::post('thumbsUpAmazonFasinLeaveMessage', 'DatawholeController@thumbsUpAmazonFasinLeaveMessage');
        //数据报表-Fasin列表-拷贝数据
        Route::get('updateFasinMessage', 'DatawholeController@updateFasinMessage');
        //数据报表-Fasin列表-拷贝数据
        Route::get('updateFasinCode', 'DatawholeController@updateFasinCode');
        //数据报表-Fasin列表-保存核心词
        Route::post('saveFasinCodeCoreWord', 'DatawholeController@saveFasinCodeCoreWord');
        //数据报表-Fasin列表-保存fasin紧急情况
        Route::post('saveFasinSituation', 'DatawholeController@saveFasinSituation');
        //数据报表-FasinCode列表
        Route::get('getFasinCodeList', 'DatawholeController@getFasinCodeList');

        //数据报表--spu报表下的库存sku报表
        Route::post('datawhole_customs', 'DatawholeController@datawhole_customs');
        //数据报表--库存sku报表下的sku报表
        Route::post('datawhole_customs_sku', 'DatawholeController@datawhole_customs_sku');
        //数据报表-sku列表
        Route::post('datawhole_sku', 'DatawholeController@datawhole_sku');
        //数据报表-sku详情
        Route::post('datawhole_sku_detail', 'DatawholeController@datawhole_sku_detail');
        //数据报表-父asin
        Route::post('datawhole_father_asin', 'DatawholeController@datawhole_father_asin');

        //数据报表-链接排名-新增
        Route::post('update_linkrank', 'DatawholeController@update_linkrank');
        //数据报表-链接排名-编辑
        Route::post('add_linkrank', 'DatawholeController@add_linkrank');
        //数据报表-链接排名-列表
        Route::get('fnsku_rank_list', 'DatawholeController@fnsku_rank_list');
        //数据报表-链接排名-删除
        Route::post('del_linkrank', 'DatawholeController@del_linkrank');
        //链接排名导入
        Route::post('excel_insert', 'DatawholeController@excel_insert');
        //亚马逊订单sssss
        Route::post('amazonorder', 'DatawholeController@amazonorder');
        //获取亚马逊运营人员和产品部人员
        Route::post('getUser', 'DatawholeController@getUser');
        //派发任务-生成任务
        Route::post('create_task', 'DatawholeController@create_task');

        //日报
        //日报列表
        Route::post('DayReportList', 'DatawholeController@DayReportList');
        //日报详情
        Route::post('DayReportDetail', 'DatawholeController@DayReportDetail');
        //新增/修改日报
        Route::post('DayReport', 'DatawholeController@DayReport');
        //获取链接排名
        Route::post('GetRankByFasin', 'DatawholeController@GetRankByFasin');
        //获得fasin列表
        Route::post('GetFasin', 'DatawholeController@GetFasin');
        //获取上次日报fasin
        Route::post('GetLastReport', 'DatawholeController@GetLastReport');
        //获取周报月报
        Route::get('DayReportTotal', 'DatawholeController@DayReportTotal');
        //日报导入
        Route::post('ReportExcelImport', 'DatawholeController@ReportExcelImport');
        //自动填写日报
        Route::post('ReportAuto', 'DatawholeController@ReportAuto');
        //自动填写日报
        Route::get('ReportAuto', 'DatawholeController@ReportAuto');
        //确认日报
        Route::post('ConfirmDayreport', 'DatawholeController@ConfirmDayreport');
        //获取日报周计划
        Route::get('GetDayReportWeekInfo', 'DatawholeController@GetDayReportWeekInfo');

        //日报列表-fasin
        Route::get('DayReportByFasin', 'DatawholeController@DayReportByFasin');
        //周报列表-fasin
        Route::get('WeekReportByFasin', 'DatawholeController@WeekReportByFasin');
        //日报警戒线
        Route::get('DayReportWarn', 'DatawholeController@DayReportWarn');
        //设置日报警戒线
        Route::post('SetDayReportWarn', 'DatawholeController@SetDayReportWarn');


        
    
        //生产下单任务
        Route::post('PlaceOrderTask', 'DatawholeController@PlaceOrderTask');
        //生产下单任务列表
        Route::post('PlaceOrderTaskList', 'DatawholeController@PlaceOrderTaskList');
        //生产下单导入-新
        Route::post('PlaceOrderImportNew', 'DatawholeController@PlaceOrderImportNew');


        //生产下单详情-分类
        Route::get('PlaceOrderDetailByCate', 'DatawholeController@PlaceOrderDetailByCate');


        //生产下单合并导出
        Route::get('PlaceOrderSumExport', 'DatawholeController@PlaceOrderSumExport');

        //生产下单导出-新
        Route::get('PlaceOrderExport', 'DatawholeController@PlaceOrderExport');
        //生产下单任务详情
        Route::post('PlaceOrderTaskDetailList', 'DatawholeController@PlaceOrderTaskDetailList');

        //生产下单
        //生产下单 - 供应链导入
        Route::post('SupplyChainImport', 'DatawholeController@SupplyChainImport');
        //生产下单 - 生产下单导入
        Route::post('PlaceOrderImport', 'DatawholeController@PlaceOrderImport');
        //生产下单 - 列表
        Route::get('PlaceOrderList', 'DatawholeController@PlaceOrderList');
        //生产下单 - 修改
        Route::post('PlaceOrderEdit', 'DatawholeController@PlaceOrderEdit');
        //生茶下单 - 删除
        Route::post('PlaceOrderDel', 'DatawholeController@PlaceOrderDel');
        //图片上传
        Route::post('UploadImg', 'DatawholeController@UploadImg');
        //亚马逊活动任务
        Route::post('ActivityTask', 'DatawholeController@ActivityTask');
        //补货报表
        Route::get('BuhuoRequestList', 'DatawholeController@BuhuoRequestList');
        //店铺吊牌表
        Route::get('amazonTag', 'DatawholeController@amazonTag');
        //修改下单信息
        Route::post('updateOrder', 'DatawholeController@updateOrder');
        Route::post('updateOrderDetail', 'DatawholeController@updateOrderDetail');
        //新增备面料下单
        Route::post('Sparefabric', 'DatawholeController@Sparefabric');

        //excel列表
        Route::post('ExcelTaskList', 'DatawholeController@ExcelTaskList');
        //spu时效表
        Route::post('spuTime', 'DatawholeController@spuTime');
        //复制下单
        Route::post('copyOrder', 'DatawholeController@copyOrder');
        //新增需求
        Route::post('addDemand', 'DatawholeController@addDemand');
        //删除需求
        Route::post('deleteDemand', 'DatawholeController@deleteDemand');
        //需求列表
        Route::post('demandList', 'DatawholeController@demandList');
        //需求明细
        Route::get('demandDetail', 'DatawholeController@demandDetail');
        //反馈进度节点与内容
        Route::post('demandBack', 'DatawholeController@demandBack');
        //确认需求完成
        Route::post('demandComplete', 'DatawholeController@demandComplete');
        //指派需求执行人
        Route::post('demandExecute', 'DatawholeController@demandExecute');


        //集体下单
        //单人下单
        Route::post('TeamPlaceOrderTaskByOne', 'DatawholeController@TeamPlaceOrderTaskByOne');
        //单人下单-删除
        Route::post('TeamPlaceOrderTaskByOneDel', 'DatawholeController@TeamPlaceOrderTaskByOneDel');
        //单人下单-修改
        Route::post('TeamPlaceOrderTaskByOneupdate', 'DatawholeController@TeamPlaceOrderTaskByOneupdate');
        //单人列表
        Route::get('TeamPlaceOrderTaskByOneList', 'DatawholeController@TeamPlaceOrderTaskByOneList');
        //单人详情
        Route::get('TeamPlaceOrderTaskByOneDetail', 'DatawholeController@TeamPlaceOrderTaskByOneDetail');
        //集体下单数据
        Route::get('TeamPlaceOrderTaskByData', 'DatawholeController@TeamPlaceOrderTaskByData');
        //集体下单
        Route::post('TeamPlaceOrderTaskByTotal', 'DatawholeController@TeamPlaceOrderTaskByTotal');
        //集体下单-详情-获取个人单库存sku下单数
        Route::get('TeamPlaceOrderGetCusUser', 'DatawholeController@TeamPlaceOrderGetCusUser');
        //集体下单-详情-修改个人单库存sku下单数
        Route::post('TeamPlaceOrderUpdateCusUser', 'DatawholeController@TeamPlaceOrderUpdateCusUser');
        //集体下单-详情-删除个人单库存sku下单
        Route::post('TeamPlaceOrderDelCusUser', 'DatawholeController@TeamPlaceOrderDelCusUser');
        //集体下单-详情
        Route::get('TeamPlaceOrderTaskByTotalDetail', 'DatawholeController@TeamPlaceOrderTaskByTotalDetail');
        // 下单报表
        Route::post('getPlaceOrderTaskListBySpu', 'DatawholeController@getPlaceOrderTaskListBySpu');
        //获取美码欧码库存
        Route::get('GetSpuInventoryByBaseSpu', 'DatawholeController@GetSpuInventoryByBaseSpu');
        //更新任务订单详情
        Route::post('updatePlaceOrderDetail', 'DatawholeController@updatePlaceOrderDetail');
        // 时效表保存进度备注
        Route::post('saveEgeingScheduleMemo', 'DatawholeController@saveEgeingScheduleMemo');
        // 获取仓库时效表
        Route::post('getWarehouseTimeTable', 'TaskController@getWarehouseTimeTable');
        // 获取仓库时效表统计
        Route::post('getWarehouseTimeTableStatistics', 'TaskController@getWarehouseTimeTableStatistics');
        //获取app版本
        Route::get('AppV', 'DatawholeController@AppV');

        //款号预算
        Route::post('SaveFasinCusPlan', 'DatawholeController@SaveFasinCusPlan');
        //款号预算列表
        Route::get('FasinCusPlanList', 'DatawholeController@FasinCusPlanList');
        //删除款号预算
        Route::post('DelCusPlan', 'DatawholeController@DelCusPlan');

        //库存出入库统计
        Route::get('GetWareHouseStatDayList', 'WareHouseStatController@GetDayList');
        Route::get('GetWareHouseStatMonthList', 'WareHouseStatController@GetMonthList');
        //库位上下架统计
        Route::get('GetLocationStatDayList', 'WareHouseStatController@GetLocationDayList');

        //平台间库存调拔记录
        Route::get('GetAdjustInventoryList', 'DatawholeController@GetAdjustInventoryList');
    });

    //定时
    Route::group(['prefix' => 'timing'], function () {
        Route::get('amazon_traffic_data', 'TimingController@amazon_traffic_data');
        Route::get('amazon_buhuo_flow', 'TimingController@amazon_buhuo_flow');
        Route::get('tongbu_data', 'TimingController@tongbu_data');
        Route::get('tongbu_saihe_data', 'TimingController@tongbu_saihe_data');
        Route::get('tongbu_fatherASin', 'TimingController@tongbu_fatherASin');
        Route::get('tongbu_kucun', 'TimingController@tongbu_kucun');
        Route::get('addtasks_son', 'TimingController@addtasks_son');
        Route::get('delete_sku', 'TimingController@delete_sku');
        Route::get('create_pdf', 'TimingController@create_pdf');
        Route::get('shippment_status', 'TimingController@shippment_status');
        Route::get('shippment_repeat', 'TimingController@shippment_repeat');
        Route::get('get_report', 'TimingController@get_report');
        Route::get('tongbu_skulist', 'TimingController@tongbu_skulist');
        Route::get('buhuo_status', 'TimingController@buhuo_status');
        Route::get('task_news', 'TimingController@task_news');
        Route::get('getData', 'TimingController@getData');
        Route::post('grn_confirm', 'TimingController@grn_confirm');
        Route::get('send_goods', 'TimingController@send_goods');
        Route::get('skudata', 'TimingController@skudata');
        Route::get('updateData', 'TimingController@updateData');
        Route::get('addData', 'TimingController@addData');
        Route::get('sendJhh', 'TimingController@sendJhh');
        Route::get('check_data', 'TimingController@check_data');
        // 保存上月库存变动统计
        Route::get('saveCustomSkuMonthStockCount', 'TimingController@saveCustomSkuMonthStockCount');

        //周转天数定时任务
        Route::get('InventoryDayTask','TimingController@InventoryDayTask');
        //1688定时任务
        Route::get('AlibabaTask','TimingController@AlibabaTask');
        //沃尔玛任务
        Route::post('walmartOrder', 'TimingController@walmartOrder');
        //沃尔玛任务
        Route::post('updateOrderSku', 'TimingController@updateOrderSku');
        //仓库每日每月统计
        Route::post('warehouseStat', 'TimingController@warehouseStat');
        //库位每日统计
        Route::post('warehouseLocationStat', 'TimingController@warehouseLocationStat');
        //修改spu状态
        Route::post('UpdateSpuStatus','TimingController@UpdateSpuStatus');
        //批量新增角色权限
        Route::get('addRole','TimingController@addRole');

        //同步领新-亚马逊订单
        Route::post('SyncLingxinToAmazon','TimingController@SyncLingxinToAmazon');
        //同步领新-亚马逊订单状态
        Route::post('SyncLingxinToAmazonStatus','TimingController@SyncLingxinToAmazonStatus');
        
        
    });

    //测试
    Route::group(['prefix' => 'test'], function () {
        //////////// 测试
        // 测试
        Route::get('tets', 'TestController@tets');
        // 清空rddis
        Route::get('redis_del', 'TestController@redis_del');
        //去除shopee_cost表中product_sku字段的空白字符串
        Route::get('string_sku', 'TestController@string_sku');
        //去除shopee_order_item表中variation_sku字段的空白字符串
        Route::get('item_sku', 'TestController@item_sku');
        Route::get('spu_return', 'TestController@spu_return');

        //dingdingtest
        Route::get('dingdingtest', 'TestController@dingdingtest');

        //
        Route::post('mytest', 'TestController@mytest');
        Route::get('report_test', 'TestController@report_test');
        //GetToken
        Route::get('GetToken', 'TestController@GetToken');
        Route::get('Uptest', 'TestController@Uptest');
        

        Route::post('removeComment', 'TestController@removeComment');

        Route::post('importSupplier', 'TestController@importSupplier');
        Route::get('printPdf', 'TestController@printPdf');
        Route::get('saveContractPdfFile', 'TestController@saveContractPdfFile');
        Route::get('getSellerList', 'LingXingApiController@getSellerList');
        Route::get('updateContractInfo', 'TestController@updateContractInfo');
        Route::get('updateSpuCate', 'TestController@updateSpuCate');
    });


    Route::group(['prefix' => 'amazon'], function () {
        Route::post('index', 'AmazonController@index');
        Route::post('order', 'AmazonController@order');
    });
    Route::group(['prefix' => 'inventory'], function () {

        Route::get('amazon_inventory', 'InventoryController@amazon_inventory');

        Route::get('amazon_inventory_total', 'InventoryController@amazon_inventory_total');

        //库存总表
        Route::get('all_inventory', 'InventoryController@all_inventory');
        
        Route::get('saihe_inventory', 'InventoryController@saihe_inventory');
        //同安仓库存-汇总-spu
        Route::get('saihe_inventory_spu', 'InventoryController@saihe_inventory_spu');
        //同安仓库存-汇总-大类
        Route::get('saihe_inventory_cate', 'InventoryController@saihe_inventory_cate');
        //泉州仓库存-汇总-spu
        Route::get('quanzhou_inventory_spu', 'InventoryController@quanzhou_inventory_spu');
        //泉州仓库存-汇总-大类
        Route::get('quanzhou_inventory_cate', 'InventoryController@quanzhou_inventory_cate');
        //海外仓库存-汇总-fasin
        Route::get('inventory_fasin', 'InventoryController@inventory_fasin');
        //海外仓库存-汇总-spu
        Route::get('inventory_spu', 'InventoryController@inventory_spu');
        //海外仓库存-汇总-大类
        Route::get('inventory_cate', 'InventoryController@inventory_cate');
        //云仓库存-汇总-spu
        Route::get('cloud_inventory_spu', 'InventoryController@cloud_inventory_spu');
        //云仓库存-汇总-大类
        Route::get('cloud_inventory_cate', 'InventoryController@cloud_inventory_cate');

        Route::get('quanzhou_inventory', 'InventoryController@quanzhou_inventory');

        Route::post('house_data', 'InventoryController@house_data');

        Route::post('quanzhou_data', 'InventoryController@quanzhou_data');
        //云仓库存
        Route::get('cloud_inventory', 'InventoryController@cloud_inventory');
        //工厂虚拟仓库存
        Route::get('factory_inventory', 'InventoryController@factory_inventory');

        //工厂虚拟仓库存-sku合同明细
        Route::get('factory_sku', 'InventoryController@factory_sku');

        Route::post('cloud_data', 'InventoryController@cloud_data');

        Route::post('transfers_data', 'InventoryController@transfers_data');
        //库存变动清单
        Route::post('goods_transfers', 'InventoryController@goods_transfers');

        //获取已推送订单（APP）
        Route::get('GetOutPushOrder', 'InventoryController@GetOutPushOrder');
        //获取出库类型详情
        Route::get('GetOutTypeDetail', 'InventoryController@GetOutTypeDetail');
        
        
        //库存退回入库
        Route::post('goods_transfers_back', 'InventoryController@goods_transfers_back');

        Route::post('transfers_data_detail', 'InventoryController@transfers_data_detail');
        // 采购详情get请求
        Route::get('transfers_data_detail_get', 'InventoryController@transfers_data_detail_get');
        //推送云仓入库单或调拨出库单
        Route::post('goods_cloud', 'InventoryController@goods_cloud');
        //推送补货计划单
        Route::post('goods_cloudOut', 'InventoryController@goods_cloudOut');
        //确认收货
        Route::post('confirmInhouse', 'InventoryController@confirmInhouse');
        //修改待推送库存变动数据
        Route::post('update_goods_transfers', 'InventoryController@update_goods_transfers');
        //工厂寄存仓库存
        Route::get('deposit_inventory', 'InventoryController@deposit_inventory');
        //生成中转箱码
        Route::get('create_transfer_box', 'InventoryController@create_transfer_box');
        //批次中转箱列表
        Route::get('transfers_box_list', 'InventoryController@transfers_box_list');
        //新增中转箱货物信息
        Route::post('update_transfers_box', 'InventoryController@update_transfers_box');
        //显示中转箱数据明细
        Route::post('transfers_box_detail', 'InventoryController@transfers_box_detail');
        //删除中转箱
        Route::get('delete_transfer_box', 'InventoryController@delete_transfer_box');
        //商品上下架库位
        Route::post('location_change', 'InventoryController@location_change');
        Route::get('GetLocationInfo', 'InventoryController@GetLocationInfo');

        Route::get('getCustomSkuLocationInventory', 'InventoryController@getCustomSkuLocationInventory');

        //出库单查询其它可用库位数据
        Route::post('selectLocation', 'InventoryController@selectLocation');
        //出库单解锁库存
        Route::post('unlockInventory', 'InventoryController@unlockInventory');
        //出库单重新分配库存
        Route::post('updateLocation', 'InventoryController@updateLocation');
        //出库单更新实际到达时间和第三方编号
        Route::post('saveGoodsTransfersInfo', 'InventoryController@saveGoodsTransfersInfo');
        //平台间调整库存
        Route::post('adjustInventory', 'InventoryController@adjustInventory');
        Route::get('adjustInventoryDetail', 'InventoryController@adjustInventoryDetail');
        //库存报表查看已占用库存明细
        Route::get('get_push_info', 'InventoryController@get_push_info');

        //生产组货入库单
        Route::post('ConfirmGroupOutInByOrderNo', 'InventoryController@ConfirmGroupOutInByOrderNo');
        // 平台调库申请修改数量
        Route::post('updateadjustInventoryDetail', 'InventoryController@updateadjustInventoryDetail');

        // 下单合同出入库数据汇总列表
        Route::get('getSpuShipmentDeatilList', 'InventoryController@getSpuShipmentDeatilList');
        // spu颜色出入库数据汇总列表
        Route::get('getSpuColorShipmentDeatilList', 'InventoryController@getSpuColorShipmentDeatilList');
        // customSku出入库数据汇总列表
        Route::get('getCustomSkuShipmentDeatilList', 'InventoryController@getCustomSkuShipmentDeatilList');
        // 确认货物到达时间
        Route::post('confirmDeliveryTime', 'InventoryController@confirmDeliveryTime');
        // 保存每月进销存报表数据
        Route::post('saveCustomSkuInventoryMonth', 'InventoryController@saveCustomSkuInventoryMonth');
    });

    Route::group(['prefix' => 'selfspu', 'middleware' => ['ValidateLogon']], function () {
        // Route::group(['prefix' => 'selfspu'], function () {

        //类目
        //导入
        Route::post('CateImport', 'SelfSpuController@CateImport');
        //获取所有分类
        Route::get('CateListAll', 'SelfSpuController@CateListAll');
        
        //添加
        Route::post('CateAdd', 'SelfSpuController@CateAdd');
        //修改
        Route::post('CateUpdate', 'SelfSpuController@CateUpdate');
        //删除
        Route::post('CateDel', 'SelfSpuController@CateDel');
        //列表
        Route::post('CateList', 'SelfSpuController@CateList');

        //颜色尺码
        //添加
        Route::post('ColorSizeAdd', 'SelfSpuController@ColorSizeAdd');
        //颜色添加生成
        Route::post('ColorAdd', 'SelfSpuController@ColorAdd');
        //色系列表
        Route::post('ColorClassList', 'SelfSpuController@ColorClassList');
        //色系添加
        Route::post('ColorClassAdd', 'SelfSpuController@ColorClassAdd');
        //色系删除
        Route::post('ColorClassDel', 'SelfSpuController@ColorClassDel');
        //修改
        Route::post('ColorSizeUpdate', 'SelfSpuController@ColorSizeUpdate');
        //删除
        Route::post('ColorSizeDel', 'SelfSpuController@ColorSizeDel');
        //列表
        Route::post('ColorSizeList', 'SelfSpuController@ColorSizeList');
        //获取对应spu颜色
        Route::post('GetSpuColorBySpu', 'SelfSpuController@GetSpuColorBySpu');
        Route::post('GetSpuColorByBaseSpu', 'SelfSpuController@GetSpuColorByBaseSpu');
        //spu尺码
        Route::post('SpuSize', 'SelfSpuController@SpuSize');
        Route::post('SpuSizeAdd', 'SelfSpuController@SpuSizeAdd');
        Route::post('SpuSizeDel', 'SelfSpuController@SpuSizeDel');


        //获取市场
        Route::post('GetMarket', 'SelfSpuController@GetMarket');
        //生产基础spu
        Route::post('SpuPreviewByBase', 'SelfSpuController@SpuPreviewByBase');
        //生成预览
        Route::post('SpuPreview', 'SelfSpuController@SpuPreview');

        //删除基础spu
        Route::post('DelBaseSpu', 'SelfSpuController@DelBaseSpu');
        //删除
        Route::post('SpuDel', 'SelfSpuController@SpuDel');

        //
        Route::get('updatetest', 'SelfSpuController@updatetest');
        //编辑
        Route::post('SpuUpdate', 'SelfSpuController@SpuUpdate');
        //基础spu列表
        Route::get('SpuListByBase', 'SelfSpuController@SpuListByBase');
        //列表
        Route::get('SpuList', 'SelfSpuController@SpuList');
        //组合spu列表
        Route::get('SpuListByzuhe', 'SelfSpuController@SpuListByzuhe');
        //spu下拉列表
        Route::get('SpuSelect', 'SelfSpuController@SpuSelect');
        //组合spu详情
        Route::post('GetGroupSpu', 'SelfSpuController@GetGroupSpu');
        //组合库存sku详情
        Route::post('GetZuheCustomSku', 'SelfSpuController@GetZuheCustomSku');
        //组合库存sku标题编辑
        Route::post('UpdateZuheCustomSku', 'SelfSpuController@UpdateZuheCustomSku');
        //spu颜色添加
        Route::post('SelfSpuColorAdd', 'SelfSpuController@SelfSpuColorAdd');
        //spu颜色编辑
        Route::post('SelfSpuColorEdit', 'SelfSpuController@SelfSpuColorEdit');
        //spu颜色删除
        Route::post('SelfSpuColorDel', 'SelfSpuController@SelfSpuColorDel');


        //spu下单颜色禁用
        Route::post('SelfSpuColorPlaceOrderAdd', 'SelfSpuController@SelfSpuColorPlaceOrderAdd');
        Route::post('SelfSpuColorPlaceOrderDel', 'SelfSpuController@SelfSpuColorPlaceOrderDel');

        //组合spu颜色详情
        Route::post('ZuheSpuList', 'SelfSpuController@ZuheSpuList');

        //库存sku
        //组合库存sku
        Route::post('ZuheSkuPreview', 'SelfSpuController@ZuheSkuPreview');
        //预览
        Route::post('SkuPreview', 'SelfSpuController@SkuPreview');
        //生成
        Route::post('CustomSkuCreate', 'SelfSpuController@CustomSkuCreate');
        //删除
        Route::post('CustomSkuDel', 'SelfSpuController@CustomSkuDel');
        //列表
        Route::get('CustomSkuList', 'SelfSpuController@CustomSkuList');


        //销售sku
        //生成
        Route::post('SkuCreate', 'SelfSpuController@SkuCreate');
        //删除
        Route::post('SkuDel', 'SelfSpuController@SkuDel');
        //上下架
        Route::post('Sku_update', 'SelfSpuController@Sku_update');
        //列表
        Route::get('SkuList', 'SelfSpuController@SkuList');
        //绑定旧数据-分支测试2
        Route::post('SpuOldBind', 'SelfSpuController@SpuOldBind');
        Route::post('expord_old_spu', 'SelfSpuController@expord_old_spu');
        //批量绑定旧sku
        Route::post('ExpordBindSku', 'SelfSpuController@ExpordBindSku');
        //设置品类负责人
        Route::post('setSupervisor', 'SelfSpuController@setSupervisor');


        //清除缓存
        Route::get('DelCache', 'SelfSpuController@DelCache');


        //产品分类 
        //添加产品分类
        Route::post('AddProductCate', 'SelfSpuController@AddProductCate');
        //删除产品分类
        Route::post('DelProductCate', 'SelfSpuController@DelProductCate');
        //修改产品分类
        Route::post('UpdateProductCateInfo', 'SelfSpuController@UpdateProductCateInfo');
        //查看产品分类
        Route::get('GetProductCate', 'SelfSpuController@GetProductCate');
        //设置面料
        Route::post('AddProductCateInfo', 'SelfSpuController@AddProductCateInfo');
        //查询面料
        Route::get('GetProductCateInfo', 'SelfSpuController@GetProductCateInfo');
        //删除面料
        Route::post('DelProductCateInfo', 'SelfSpuController@DelProductCateInfo');
        //根据spu_id查询面料选项
        Route::get('GetProductCateInfoByBaseSpuId', 'SelfSpuController@GetProductCateInfoByBaseSpuId');
        //根据user_id,spu_id查询是否有权限
        Route::get('GetProductCateAuthByBaseSpuId', 'SelfSpuController@GetProductCateAuthByBaseSpuId');

        //旧库存sku绑定生成
        Route::post('SendAndBindSkuI', 'SelfSpuController@SendAndBindSkuI');
        


    });

    //合同
    // Route::group(['prefix' => 'cloudhouse','middleware' => ['ValidateLogon']], function () {
    Route::group(['prefix' => 'cloudhouse'], function () {
        //合同导入
        Route::post('ContractImport', 'CloudHouseController@ContractImport');
        //走柜导入
        Route::post('ContractZgImport', 'CloudHouseController@ContractZgImport');
        //合同入库导入
        Route::post('ContractOutImport', 'CloudHouseController@ContractOutImport');
        //查询合同spu颜色
        Route::post('GetContractColor', 'CloudHouseController@GetContractColor');
        //合同订单明细列表
        Route::post('ContractList', 'CloudHouseController@ContractList');
        Route::post('ContractListNew', 'CloudHouseController@ContractListNew');
        //尺码排序
        Route::post('SizeBySort', 'CloudHouseController@SizeBySort');
        
        //采购入库
        Route::post('ContractOut', 'CloudHouseController@ContractOut');
        //采购入库手动推送
        Route::post('PushInhouse', 'CloudHouseController@PushInhouse');
        //出库日志
        Route::post('ContractOutlog', 'CloudHouseController@ContractOutlog');
        //取消出库
        Route::post('ContractCancel', 'CloudHouseController@ContractCancel');
        //出库批号详情ContractOutList
        Route::post('ContractOutList', 'CloudHouseController@ContractOutList');
        //手动同步商品
        Route::post('ContractSyncDetail', 'CloudHouseController@ContractSyncDetail');
        //根据库存sku同步商品
        Route::post('cloudhouse_custom_sku', 'CloudHouseController@cloudhouse_custom_sku');
        //取消采购入库
        Route::post('InhouseCancel', 'CloudHouseController@InhouseCancel');
        //取消调拨单
        Route::post('transfersCancel', 'CloudHouseController@transfersCancel');
        //合同列表
        Route::post('ContractTotalList', 'CloudHouseController@ContractTotalList');
        //出入库记录接口
        Route::get('inventoryInOut', 'CloudHouseController@inventoryInOut');
        // 合同出货列表
        Route::post('getContractShipmentList', 'CloudHouseController@getContractShipmentList');
        // 库存月份统计列表
        Route::post('getCustomSkuMonthStockCountList', 'CustomSkuMonthStockCountController@getCustomSkuMonthStockCountList');
        //合同同步到一狗
        Route::get('ConcactToOneDog', 'CloudHouseController@ConcactToOneDog');
        //新增合同到货计划
        Route::post('ContractPlan', 'CloudHouseController@ContractPlan');
        //修改合同到货计划
        Route::post('ContractPlanUpdate', 'CloudHouseController@ContractPlanUpdate');
        //删除合同到货计划
        Route::get('ContractPlanDelete', 'CloudHouseController@ContractPlanDelete');
        //查询合同到货计划
        Route::get('ContractPlanSelect', 'CloudHouseController@ContractPlanSelect');
    });

    //api开放接口
    Route::group(['prefix' => 'apires'], function () {
        //入库单确认
        Route::post('in_confirm', 'ApiresController@in_confirm');
        //出库单确认
        Route::post('out_confirm', 'ApiresController@out_confirm');
        //装箱数据接收
        Route::post('box_receive', 'ApiresController@box_receive');
        //go调用任务完成
        Route::post('task_son_complete_go', 'ApiresController@task_son_complete_go');
        //查询历史下单数据
        Route::get('GetHistoryOrder', 'ApiresController@GetHistoryOrder');
    });

    // 权限
    Route::group(['prefix' => 'virtual', 'middleware' => ['ValidateLogon']], function () {
        //虚拟岗位功能列表


        //虚拟岗位列表
        Route::post('index', 'VirtualPostController@index');
        //设置事项
        Route::post('set_matter', 'VirtualPostController@setMatter');
        //返回需要查询的tab列数据
        Route::post('search_list', 'VirtualPostController@getSearchList');
        //上传文件
        Route::post('upload_file', 'VirtualPostController@uploadFile');
        //上传图片
        Route::post('upload_img', 'VirtualPostController@uploadImg');
        //更新执行结果
        Route::post('results', 'VirtualPostController@createResults');
        //留言，评分，执行结果，批注
        Route::post('comment', 'VirtualPostController@Comment');
        //创建临时事项
        Route::post('create_matter', 'VirtualPostController@createMatter');
        //新增岗位
        Route::post('insert_post', 'VirtualPostController@createPost');
        //编辑岗位
        Route::post('update_post', 'VirtualPostController@updatePost');
        //删除岗位
        Route::post('del_post', 'VirtualPostController@delPost');
        //设置事项
        Route::post('set_matter', 'VirtualPostController@setMatter');
        //岗位绑定事项
        Route::post('bind_matter', 'VirtualPostController@bindMatter');
        //删除事项
        Route::post('del_matter', 'VirtualPostController@delMatter');
        //事项列表
        Route::post('matter_list', 'VirtualPostController@matterList');
        //岗位列表
        Route::post('post_list', 'VirtualPostController@postList');
        //我的岗位
        Route::post('my_post', 'VirtualPostController@myPost');
        //根据岗位获取事项
        Route::post('get_matter_Post', 'VirtualPostController@getMatterByPost');
        //获取下单列表
    });

    Route::group(['prefix' => 'virtual'], function () {
        Route::get('set_matter_list', 'VirtualPostController@setMatterList');
        Route::get('setMatterExcel', 'VirtualPostController@setMatterExcel');
        Route::get('checkAutoList', 'VirtualPostController@checkAutoList');
    });

    Route::group(['prefix' => 'anoke_contact', 'middleware' => ['ValidateLogon']], function () {
        Route::post('getIndex', 'AnokContractController@getContractList');
    });
//
    // 导入模块
    Route::group(['prefix' => 'import', 'middleware' => ['ValidateLogon']], function () {
        Route::post('Import', 'ImportController@import');
//        Route::post('Test', 'QuestionnaireController@test');
    });

    // 导出模块
    Route::group(['prefix' => 'export', 'middleware' => ['ValidateLogon']], function () {
        Route::post('Export', 'ExportController@export');
    });

    // 调查问卷模块
    Route::group(['prefix' => 'questionnaire'], function () {
        Route::post('saveHaixihuiApplication', 'QuestionnaireController@saveHaixihuiApplication');
        Route::post('saveProductResearch', 'QuestionnaireController@saveProductResearch');
        Route::post('get_qiniu_token', 'IndexController@qiniu_token');
        // 海西汇入会申请列表
        Route::post('getHaixihuiApplicationList', 'QuestionnaireController@getHaixihuiApplicationList');
        // 海西汇产品调研列表
        Route::post('getHaixihuiProductResearchList', 'QuestionnaireController@getHaixihuiProductResearchList');
        Route::get('getHaixihuiTableTypeList', 'QuestionnaireController@getHaixihuiTableTypeList');
    });

    // 仓储管理
    Route::group(['prefix' => 'warehouse', 'middleware' => ['ValidateLogon']], function () {
        // 保存仓库
        Route::post('saveCloudhouseWarehouse', 'CloudhouseWarehouseController@saveCloudhouseWarehouse');
        // 保存库位
        Route::post('saveCloudhouseWarehouseLocation', 'CloudhouseWarehouseController@saveCloudhouseWarehouseLocation');
        // 获取仓库列表
        Route::post('getCloudhouseWarehouseList', 'CloudhouseWarehouseController@getCloudhouseWarehouseList');
        // 获取库位列表
        Route::post('getCloudhouseWarehouseLocationList', 'CloudhouseWarehouseController@getCloudhouseWarehouseLocationList');
        // 删除仓库
        Route::post('deleteCloudhouseWarehouse', 'CloudhouseWarehouseController@deleteCloudhouseWarehouse');
        // 删除仓库
        Route::post('deleteCloudhouseWarehouseLocation', 'CloudhouseWarehouseController@deleteCloudhouseWarehouseLocation');
        // 获取运输方式列表
        Route::post('getTransportationModeList', 'DigitalController@getTransportationModeList');
        // 保存运输方式
        Route::post('saveTransportationMode', 'DigitalController@saveTransportationMode');
        // 删除运输方式
        Route::post('deleteTransportationMode', 'DigitalController@deleteTransportationMode');
        // 获取库位库存列表
        Route::get('getCloudhouseWarehouseLocationStockList', 'CloudhouseWarehouseController@getCloudhouseWarehouseLocationStockList');
        // 获取库位库存变动日志
        Route::post('getCloudhouseWarehouseLocationLogList', 'CloudhouseWarehouseController@getCloudhouseWarehouseLocationLogList');
        // 获取库位库存变动日志
        Route::post('moveWarehouseLocationInventory', 'CloudhouseWarehouseController@moveWarehouseLocationInventory');
        // 更新仓库资产
        Route::post('updateCloudhouseWarehouseProperty', 'CloudhouseWarehouseController@updateCloudhouseWarehouseProperty');
        // 平台库存列表
        Route::get('getPlatformInventoryList', 'CloudhouseWarehouseController@getPlatformInventoryList');
        //excel导入查询平台库存
        Route::post('ExcelPlatformInventoryList', 'CloudhouseWarehouseController@ExcelPlatformInventoryList');
        
        // 平台库存列表导入查询
        Route::post('PlatformInventoryExcelImport', 'CloudhouseWarehouseController@PlatformInventoryExcelImport');
        
        // 库存详情列表
        Route::get('getInventoryDetailList', 'CloudhouseWarehouseController@getInventoryDetailList');
        // 扫码获取库位信息
        Route::get('getLocationInfo', 'CloudhouseWarehouseController@getLocationInfo');
    });

    Route::group(['prefix' => 'test', 'middleware' => ['ValidateLogon']], function () {
        Route::get('updateBuhuoRequestNum', 'DigitalController@updateBuhuoRequestNum');
    });
    // 生产下单合同
    Route::group(['prefix' => 'contract', 'middleware' => ['ValidateLogon']], function () {
        Route::post('saveAmazonPlaceOrderContract', 'AmazonPlaceOrderContractController@saveAmazonPlaceOrderContract');
        Route::post('getAmazonPlaceOrderContractList', 'AmazonPlaceOrderContractController@getAmazonPlaceOrderContractList');
        Route::post('deleteAmazonPlaceOrderContract', 'AmazonPlaceOrderContractController@deleteAmazonPlaceOrderContract');
        Route::post('getAmazonPlaceOrderContractDetail', 'AmazonPlaceOrderContractController@getAmazonPlaceOrderContractDetail');
        Route::post('getAmazonPlaceOrderTaskDetail', 'AmazonPlaceOrderContractController@getAmazonPlaceOrderTaskDetail');
        Route::post('getPurchaseAmazonPlaceOrderTaskDetail', 'AmazonPlaceOrderContractController@getPurchaseAmazonPlaceOrderTaskDetail');
        Route::post('formatContractItems', 'AmazonPlaceOrderContractController@formatContractItems');
        Route::post('submitAmazonPlaceOrderContract', 'AmazonPlaceOrderContractController@submitAmazonPlaceOrderContract');
        Route::post('revokeAmazonPlaceOrderContract', 'AmazonPlaceOrderContractController@revokeAmazonPlaceOrderContract');
        Route::get('exportPlaceOrderContractDetail', 'AmazonPlaceOrderContractController@exportPlaceOrderContractDetail');
        // 获取历史采购价
        Route::get('getHistorySpuPrice', 'AmazonPlaceOrderContractController@getHistorySpuPrice');
        // 合同补录
        Route::post('additionalRecordingContract', 'AmazonPlaceOrderContractController@additionalRecordingContract');
        //绑定采购单号
        Route::post('bindPurchaseOrderNo', 'AmazonPlaceOrderContractController@bindPurchaseOrderNo');
        Route::post('batchBindPurchaseOrderNo', 'AmazonPlaceOrderContractController@batchBindPurchaseOrderNo');
        // 合同一键完结
        Route::post('oneClickCompletionContract', 'AmazonPlaceOrderContractController@oneClickCompletionContract');
        // 获取公司列表
        Route::get('getCompanyList', 'AmazonPlaceOrderContractController@getCompanyList');
        // 组织合同明细数据
        Route::post('getFormatContractItems', 'AmazonPlaceOrderContractController@getFormatContractItems');
        // 获取合同结算方式
        Route::get('getContractPaymentMethod', 'AmazonPlaceOrderContractController@getContractPaymentMethod');
        // 获取合同打印文件
        Route::get('getContractPdfPath', 'AmazonPlaceOrderContractController@getContractPdfPath');
        // 批量更新合同打印文件
        Route::post('batchUpdateContractPdf', 'AmazonPlaceOrderContractController@batchUpdateContractPdf');
        // 合同上传理单资料
        Route::post('uploadContractFile', 'AmazonPlaceOrderContractController@uploadContractFile');
        // 采购单跟进表
        Route::get('getOrderReportForms', 'AmazonPlaceOrderContractController@getOrderReportForms');
        // 合同跟进表
        Route::get('getContractReortForms', 'AmazonPlaceOrderContractController@getContractReportForms');
        // 合同到货计划详情表
        Route::get('getContractPlanList', 'AmazonPlaceOrderContractController@getContractPlanList');
        // 合同Spu看板
        Route::get('getContractSpuList', 'AmazonPlaceOrderContractController@getContractSpuList');
        // 合同excel版本
        Route::get('downloadContractExcel', 'AmazonPlaceOrderContractController@downloadContractExcel');
        // 保存付款方式
        Route::post('saveContractPaymentMethod', 'AmazonPlaceOrderContractController@saveContractPaymentMethod');
        //审核合同到货计划
        Route::post('auditContractPlan', 'CloudHouseController@auditContractPlan');
    });
    // 仓储变动
    Route::group(['prefix' => 'inventoryturns'], function () {
        // 走柜导入
        Route::post('ZgImport', 'InventoryTurnsController@ZgImport');
        //走柜详情
        Route::get('GetZg', 'InventoryTurnsController@GetZg');
        //走柜-供应链
        Route::post('SaveZg', 'InventoryTurnsController@SaveZg');
        //走柜-运营
        Route::post('UpdateZg', 'InventoryTurnsController@UpdateZg');
        //走柜列表
        Route::get('GetZgList', 'InventoryTurnsController@GetZgList');
        //走柜spu列表
        Route::get('GetZgSpuList', 'InventoryTurnsController@GetZgSpuList');
    });

    // 4px
    Route::group(['prefix' => 'fpx'], function () {
        // 获取订单
        Route::get('GetFpxOrder', 'FpxController@GetFpxOrder');
        // 取消订单
        Route::post('CancelFpxOrder', 'FpxController@CancelFpxOrder');
        // 修改订单
        Route::post('UpdateFpxOrder', 'FpxController@UpdateFpxOrder');
        // 创建订单
        Route::post('CreateFpxOrder', 'FpxController@CreateFpxOrder');
        // 订单列表
        Route::get('FpxOrderList', 'FpxController@FpxOrderList');
        // 订单详情
        Route::get('FpxOrderDetail', 'FpxController@FpxOrderDetail');
        //订单模版获取
        Route::get('GetFpxTemplate', 'FpxController@GetFpxTemplate');
        //订单模版列表
        Route::get('GetFpxTemplateList', 'FpxController@GetFpxTemplateList');
        //订单模版增加修改
        Route::post('SaveFpxTemplate', 'FpxController@SaveFpxTemplate');
        //创建4px订单
        Route::post('HaveFpxOrder', 'FpxController@HaveFpxOrder');
        //自发货订单列表
        Route::get('FpxAmzonOrderList', 'FpxController@FpxAmzonOrderList');
        //自发货面单
        Route::get('GetFpxOrderLabel', 'FpxController@GetFpxOrderLabel');
        //获取订单-领星
        Route::get('GetFpxLxAmzonOrderList', 'FpxController@GetFpxLxAmzonOrderList');



        
        //实时更新订单
        Route::get('FindOrder', 'FpxController@FindOrder');
        //作废订单
        Route::post('CancelOrder', 'FpxController@CancelOrder');
        //取消作废订单
        Route::post('DeleteCancelOrder', 'FpxController@DeleteCancelOrder');
        //作废订单列表
        Route::get('CancelOrderList', 'FpxController@CancelOrderList');

        //获取所有自发货订单
        Route::get('GetFpxAmzonOrderListAll', 'FpxController@GetFpxAmzonOrderListAll');

        //阿里巴巴api
        Route::get('AliBaBa', 'FpxController@AliBaBa');
        Route::get('AliBaBaLogistics', 'FpxController@AliBaBaLogistics');
        

        //获取lazada授权店铺
        Route::get('GetLazadaShops', 'FpxController@GetLazadaShops');
        //获取lazada授权url
        Route::get('GetlazadaCodeUrl', 'FpxController@GetlazadaCodeUrl');
        //修改lazada授权token
        Route::post('UpdatelazadaToken', 'FpxController@UpdatelazadaToken');

        //获取lazada面单相关
        Route::post('LazadaOrderLabel', 'FpxController@GetLazadaOrderLabel');
        //1688队列
        Route::get('GetAlibabaQueue', 'FpxController@GetAlibabaQueue');

        //获取lazada授权日期
        Route::get('GetLazadaArea', 'FpxController@GetLazadaArea');

        //获取lazada订单列表
        Route::get('LazadaOrders', 'FpxController@LazadaOrders');

        //获取lazada自动下单列表
        Route::get('GetLazadaWaitOrder', 'FpxController@GetLazadaWaitOrder');

        //1688订单
        Route::get('AlibabaOrder', 'FpxController@AlibabaOrder');

        //手动添加订单
        Route::post('ManualAddOrder', 'FpxController@ManualAddOrder');

        //lazada sku 绑定
        Route::post('bindLazadaSku', 'FpxController@bindLazadaSku');

        //自动发货
        Route::post('AutoDeliverGoods', 'FpxController@AutoDeliverGoods');
        //自动发货日志
        Route::get('GetAutoShipmentsLog', 'FpxController@GetAutoShipmentsLog');

    });


    // 缓存数据
    Route::group(['prefix' => 'cachedata'], function () {
        // 获取库存缓存
        Route::get('GetCacheInventory', 'CacheDataController@GetCacheInventoryb');
        Route::get('GetCacheInventoryb', 'CacheDataController@GetCacheInventory');
        
        // 设置库存缓存
        Route::get('SetCacheInventory', 'CacheDataController@SetCacheInventory');
        // 设置库存缓存-任务
        Route::post('SetCacheInventoryTask', 'CacheDataController@SetCacheInventoryTask');
        //获取订单列表
        Route::get('GetOrderList', 'CacheDataController@GetOrderList');
        //获取spu周转天数排行
        Route::get('SpuRank', 'CacheDataController@SpuRank');
        //获取spu颜色周转天数排行
        Route::get('SpuColorRank', 'CacheDataController@SpuColorRank');
        //获取库存sku周转天数排行
        Route::get('CustomSkuRank', 'CacheDataController@CustomSkuRank');
        // 获取平台出入库数量占比详情
        Route::get('getWarehousePlatformInOrOutNumList', 'CacheDataController@getWarehousePlatformInOrOutNumList');
    });


    //spu面料
    Route::group(['prefix' => 'spuinfo'], function () {
        //上传spu图片
        Route::post('SaveSpuImgs', 'SelfSpuInfoController@SaveSpuImgs');
        Route::get('GetSpuImgs', 'SelfSpuInfoController@GetSpuImgs');
        Route::post('DelSpuImgs', 'SelfSpuInfoController@DelSpuImgs');


        //上传spu信息
        Route::post('SaveSpuInfo', 'SelfSpuInfoController@SaveSpuInfo');
        //spu基础信息
        Route::get('GetSpuInfo', 'SelfSpuInfoController@GetSpuInfo');
        //spu状态
        Route::get('getSpuStatusType', 'SelfSpuInfoController@getSpuStatusType');
        //修改spu状态
        Route::post('UpdateSpuStatus', 'SelfSpuInfoController@UpdateSpuStatus');
        
        

        //获取basespu信息
        Route::get('GetSpuCus', 'SelfSpuInfoController@GetSpuCus');
        Route::get('GetSpuColor', 'SelfSpuInfoController@GetSpuColor');
        Route::get('GetSpuSize', 'SelfSpuInfoController@GetSpuSize');

        //获取库存sku信息
        Route::get('GetCusInfo', 'SelfSpuInfoController@GetCusInfo');
        //保存库存sku信息
        Route::post('SaveCusInfo', 'SelfSpuInfoController@SaveCusInfo');
        //批量库存sku信息
        Route::post('UpdateCusInfo', 'SelfSpuInfoController@UpdateCusInfo');
        //上架
        Route::post('SellCus', 'SelfSpuInfoController@SellCus');
        //删除上架
        Route::post('DelSellCus', 'SelfSpuInfoController@DelSellCus');
        //获取上架
        Route::get('GetSellCus', 'SelfSpuInfoController@GetSellCus');
        //操作日志
        Route::get('GetSysLogByspuInfo', 'SelfSpuInfoController@GetSysLogByspuInfo');
        //操作日志-总
        Route::get('GetSysLog', 'SelfSpuInfoController@GetSysLog');
        //模拟请求日志
        Route::get('SysTry', 'SelfSpuInfoController@SysTry');

        //asin获取库存sku
        Route::get('AsinGetCustomSku', 'SelfSpuController@AsinGetCustomSku');

        Route::get('GetCusSuppliers', 'SelfSpuController@GetCusSuppliers');

        //导入库存sku箱码
        Route::post('ImportCustomSkuBox', 'SelfSpuInfoController@ImportCustomSkuBox');
        //箱规列表
        Route::get('CustomSkuBoxList', 'SelfSpuInfoController@CustomSkuBoxList');

        //获取spu价格一览表
        Route::get('getSpuPriceList', 'SelfSpuInfoController@getSpuPriceList');
        
    });

    // 财务管理
    Route::group(['prefix' => 'finance', 'middleware' => ['ValidateLogon']], function () {
        // 保存付账款单据
        Route::post('savePaymentBill', 'FinanceController@savePaymentBill');
        // 获取财务票据下拉菜单选项
        Route::get('getPaymentBillSelectionList', 'FinanceController@getPaymentBillSelectionList');
        // 获取付账单列表
        Route::get('getPaymentBillList', 'FinanceController@getPaymentBillList');
        // 获取付账单详情
        Route::get('getPaymentBillDetail', 'FinanceController@getPaymentBillDetail');
        // 废弃付账款单据
        Route::post('deletePaymentBill', 'FinanceController@deletePaymentBill');
        // 提交付账款单据
        Route::post('submitPaymentBill', 'FinanceController@submitPaymentBill');
        // 获取供应商合同列表
        Route::get('getSupplierContractList', 'FinanceController@getSupplierContractList');
        // 获取入库数据列表
        Route::get('getGoodTransfersDetail', 'FinanceController@getGoodTransfersDetail');
        // 撤销票据审核任务
        Route::post('deApprovalPaymenBill', 'FinanceController@deApprovalPaymenBill');
        // 获取核销列表
        Route::get('getFinancialVerificationList', 'FinanceController@getFinancialVerificationList');
        // 确认支付票据
        Route::post('confirmPaymentBill', 'FinanceController@confirmPaymentBill');
        // 核销票据
        Route::post('writeOffPaymentBill', 'FinanceController@writeOffPaymentBill');
        // 打印票据
        Route::get('printBillPdf', 'FinanceController@printBillPdf');
        // 获取超期入库数据
        Route::get('getDelayedGoodsTransfersDetail', 'FinanceController@getDelayedGoodsTransfersDetail');
        // 获取抵扣记录
        Route::get('getFinancialBillDeductionList', 'FinanceController@getFinancialBillDeductionList');
        // 合同应付账报表
        Route::get('getContractPaymentAccountList', 'FinanceController@getContractPaymentAccountList');
        // 合同周期排款报表
        Route::get('getContractPeriodicArrangementList', 'FinanceController@getContractPeriodicArrangementList');
        // 手动更新票据打印
        Route::post('updateFinanceBillPdfFile', 'FinanceController@updateFinanceBillPdfFile');
    });

    //沃尔玛
    Route::group(['prefix' => 'walmart', 'middleware' => ['ValidateLogon']], function () {
        // 获取付账单列表
        Route::get('GetOrderList', 'WalmartController@GetOrderList');
    });
});



