<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>后台管理平台</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Preview page of Metronic Admin Theme #2 for statistics, charts, recent events and reports" name="description" />
    <meta content="" name="author" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
	
    <link rel="shortcut icon" href="favicon.ico">
    {{--<link href="/admin/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">--}}
    <link href="/admin/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/admin/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="/admin/css/hbwwork.css" rel="stylesheet">
    <link href="/admin/css/style.min862f.css?v=4.1.0" rel="stylesheet">
    <!--分页-->
    <link rel="stylesheet" href="/vendor/page/jquery.sPage.css?v=0.0.2">
    <!--选项-->
    <link href="/admin/selectedfrom/css/site.css" rel="stylesheet">
    <!-- 图标 CSS-->
    <link rel="stylesheet" href="/admin/selectedfrom/fonts/web-icons/web-icons.css">
    <!--阿里图标-->
    <link rel="stylesheet" type="text/css" href="/vendor/aliicon/iconfont.css">
    <!--单选按钮-->
	<link rel="stylesheet" href="/admui/themes/classic/base/css/site.css" id="admui-siteStyle">
    <!-- 自定义css -->
    <link rel="stylesheet" href="/custom/admin/role/css/role_list.css?v=1">
</head>

<body>
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-green-sharp uppercase">
                <a onclick="top.location.href='/admin/index/index_html'" style="color: #3e8ef7">首页</a>&nbsp;/&nbsp;
                <span class="caption-subject font-green-sharp uppercase" style="color: #76838f">角色管理</span>
            </span>
        </div>
        
        {{--<div class="caption add_quarters_div">--}}
            {{--<button type="button" onclick="mymodal1();" class="btn btn-blue">添加岗位</button>--}}
        {{--</div>--}}
    </div>

    <div class="ibox-content main">
        <!--条件查询-->
        <div class="row user_content_con" id="select">
            <div class="sel_div">
                <label class="sel_label">每页条数：</label>
                <div class="sel_divson">
                    <input type="text" class="input-sm form-control number" placeholder="请填写条数" style="width: 60px;" value="10">
                </div>
            </div>
            <div class="sel_div">
                <label class="sel_label">角色名称：</label>
                <div class="sel_divson">
                    <input type="text" class="input-sm form-control name" placeholder="角色名称">
                </div>
            </div>
            <div class="sel_div">
                <button type="button" class="btn btn-sm btn-blue sel_button" onclick="list_data(1)">查询</button>
            </div>
    	</div>

        <!--动态列表-->
        <div class="list_data">
            <div class="tab-pane fade active in zc-table">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>角色名称</th>
                        <th>角色描述</th>
                        <th style="width: 15vw">操作</th>
                    </tr>
                    </thead>
                    <tbody id="list_tbody">
                    <!--资产列表--模板-->
                    <script id="list_script" type="text/html">
                        {each list vo}
                        <tr>
                            <td>{vo['Id']}</td>
                            <td>{vo['name']}</td>
                            <td>{vo['desc']}</td>
                            <td>
                                <button  data-original-title="编辑" data-toggle="modal" onclick="role_update_inv({vo['Id']})" class="btn btn-outline btn-primary td_button">
                                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                </button>
                                <button  data-original-title="权限" data-toggle="modal" onclick="powers_role({vo['Id']}, '{vo['name']}')" class="btn btn-outline btn-primary td_button">
                                    <span class="glyphicon glyphicon-lock" aria-hidden="true"></span>
                                </button>
                                <button data-original-title="删除" data-toggle="modal" class="btn btn-outline btn-primary td_button" onclick="role_del({vo['Id']});">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                </button>
                            </td>
                        </tr>
                        {/each}

                        {if list.length == 0}
                            <div class="nodata">无数据！</div>
                        {/if}
                    </script>
                    </tbody>

                </table>
            </div>
            <div id="my_page" class="demo"></div>
        </div>
		<!--end动态列表-->
    </div>
</div>

{{--新增按钮--}}
<button class="site-action btn-raised btn btn-floating btn-blue" data-page-height="460" type="button" onclick="role_add_inv()">
    <i class="icon wb-plus" aria-hidden="true"></i>
</button>

<!--弹窗-->
<!--新增角色-->
<div class="modal inmodal fade"  id="myModal_1" tabindex="-1" role="dialog"  aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class=" float-e-margins">
                <div class="portlet-title" style="font-size: 18px;padding: 15px 24px;background: #f3f7f9">新增角色</div>
                <div class="ibox-content">
                    <form class="form-horizontal" id="form_1">
                        <div class="form-group">
                            <p style="padding-left: 15px;padding-top: 10px;margin-bottom: 5px;">角色名称</p>
                            <div class="col-sm-12">
                                <input type="text" class="form-control name" placeholder="角色名称">
                            </div>
                        </div>
                        <div class="form-group">
                            <p style="padding-left: 15px;padding-top: 10px;margin-bottom: 5px;">角色描述</p>
                            <div class="col-sm-12">
                                <textarea class="form-control desc" rows="4" required="" aria-required="true" placeholder="角色描述"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-blue Auditing" onclick="role_add()">新增</button>
                <button type="button" class="btn btn-white cancel" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>

<!--编辑角色-->
<div class="modal inmodal fade"  id="myModal_2" tabindex="-1" role="dialog"  aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class=" float-e-margins">
                <div class="portlet-title" style="font-size: 18px;padding: 15px 24px;background: #f3f7f9">编辑角色</div>
                <div class="ibox-content">
                    <!--<p style="margin: 0 15px"> 您可以在此页面中添加设备。注：<span style="color:red;">*&nbsp;</span>为必填项</p>-->
                    <form class="form-horizontal" id="form_2">
                        <input type="hidden" class="role_id"/>
                        <div class="form-group">
                            <p style="padding-left: 15px;padding-top: 10px;margin-bottom: 5px;">角色名称</p>
                            <div class="col-sm-12">
                                <input type="text" class="form-control name" placeholder="角色名称">
                            </div>
                        </div>
                        <div class="form-group">
                            <p style="padding-left: 15px;padding-top: 10px;margin-bottom: 5px;">角色描述</p>
                            <div class="col-sm-12">
                                <textarea class="form-control desc" rows="4" required="" aria-required="true" placeholder="角色描述"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="role_update()" class="btn btn-blue Auditing">保存</button>
                <button type="button" class="btn btn-white cancel" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>


<script src="/admin/js/jquery.min.js?v=2.1.4"></script>
<script src="/admin/js/bootstrap.min.js?v=3.3.6"></script>
<!--分页-->
<script src="/vendor/page/jquery.sPage.js"></script>
<!--提示框-->
<script type="text/javascript" src="/vendor/layer/layer.js" ></script>
<!-- artTemplate模板引擎 -->
<script type="text/javascript" src="/vendor/artTemplate/lib/template-web.js" ></script>
<!-- 自定义js -->
<script type="text/javascript" src="/custom/admin/role/js/role_list.js?v=2"></script>

</body>
</html>
