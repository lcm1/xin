<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>后台管理平台</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Preview page of Metronic Admin Theme #2 for statistics, charts, recent events and reports" name="description" />
    <meta content="" name="author" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
	
    <link rel="shortcut icon" href="favicon.ico">
    {{--<link href="/admin/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">--}}
    <link href="/admin/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/admin/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="/admin/css/hbwwork.css" rel="stylesheet">
    <link href="/admin/css/style.min862f.css?v=4.1.0" rel="stylesheet">
    <!--分页-->
    <link rel="stylesheet" href="/vendor/page/jquery.sPage.css?v=0.0.2">
    <!--选项-->
    <link href="/admin/selectedfrom/css/site.css" rel="stylesheet">
    <!-- 图标 CSS-->
    <link rel="stylesheet" href="/admin/selectedfrom/fonts/web-icons/web-icons.css">
    <!--阿里图标-->
    <link rel="stylesheet" type="text/css" href="/vendor/aliicon/iconfont.css">
    <!--单选按钮-->
	<link rel="stylesheet" href="/admui/themes/classic/base/css/site.css" id="admui-siteStyle">
    <!-- 自定义css -->
    <link rel="stylesheet" href="/custom/admin/role/css/powers_role.css?v=1">
</head>

<body>
<div class="portlet light" style="padding: 12px 20px 50px 20px;">
    <input type="hidden" id="role_id" value="{{$role_id}}" />
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-green-sharp uppercase">
                <a onclick="top.location.href='/admin/index/index_html'" style="color: #3e8ef7">首页</a>&nbsp;/&nbsp;
                <a onclick="window.location.href='/admin/role/role_list_html'" style="color: #3e8ef7">角色管理</a>&nbsp;/&nbsp;
                <span class="caption-subject font-green-sharp uppercase" style="color: #76838f">
                    角色权限（<span style="color: #ff4c52;">{{$role_name}}</span>）
                </span>
            </span>
        </div>
    </div>
    
    <div class="ibox-content main">

        <div class="perkuang">
            @foreach($power_list as $vo)
                <div class="zhengcheng">
                    <div class="perkuangleft">
                        <div class="checkbox-custom checkbox-primary">
                            <input type="checkbox" id="f{{$vo['Id']}}" class="power_type" value="{{$vo['Id']}}" onclick="whole(this)" />
                            <label for="f{{$vo['Id']}}">{{$vo['name']}}</label>
                        </div>
                    </div>
                    <div class="perkuangright">
                        @foreach($vo['children'] as $vo1)
                            <div class="rongqi">
                                <div class="checkbox-custom checkbox-primary">
                                    {{--<input type="checkbox" class="1" fid="1" id="1" onclick="judge(this)" ptype="2" value="1" con="1" checked>--}}
                                    <input type="checkbox" class="powers" name="power_id" fid="{{$vo1['type_id']}}" id="{{$vo1['Id']}}" onclick="judge(this)" value="{{$vo1['Id']}}" {{$vo1['is_checked']==1 ? 'checked' : ''}} />
                                    <label for="{{$vo1['Id']}}">{{$vo1['name']}}</label>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>

    </div>
</div>
<div style="position: fixed;bottom:10px;width: 100%;z-index:100;text-align:center;background-color:#ecececbf;padding: 10px;">
    <button type="button" class="btn btn-blue" onclick="powers_role_update()">保存</button>
    <button type="reset" onclick="javascript:history.back(-1);" class="btn btn-default btn-outline" style="background-color: #fff;margin-left: 10px;">返回</button>
</div>

<!--弹窗-->
<!--新增角色-->
<div class="modal inmodal fade"  id="myModal_1" tabindex="-1" role="dialog"  aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class=" float-e-margins">
                <div class="portlet-title" style="font-size: 18px;padding: 15px 24px;background: #f3f7f9">
                    &nbsp;&nbsp;新增角色
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" id="form_1">
                        <div class="form-group" style="padding: 0 15px;">
                            <p style="padding-left: 15px;padding-top: 10px;margin-bottom: 5px;">角色名称</p>
                            <div class="col-sm-12">
                                <input type="text" class="form-control name" placeholder="角色名称">
                            </div>
                        </div>
                        <div class="form-group" style="padding: 0 15px;">
                            <p style="padding-left: 15px;padding-top: 10px;margin-bottom: 5px;">角色描述</p>
                            <div class="col-sm-12">
                                <textarea class="form-control desc" rows="4" required="" aria-required="true" placeholder="角色描述"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-blue Auditing" onclick="role_add()">新增</button>
                <button type="button" class="btn btn-white cancel" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>

<!--编辑角色-->
<div class="modal inmodal fade"  id="myModal_2" tabindex="-1" role="dialog"  aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class=" float-e-margins">
                <div class="portlet-title" style="font-size: 18px;padding: 15px 24px;background: #f3f7f9">
                    &nbsp;&nbsp;编辑角色
                </div>
                <div class="ibox-content">
                    <!--<p style="margin: 0 15px"> 您可以在此页面中添加设备。注：<span style="color:red;">*&nbsp;</span>为必填项</p>-->
                    <form class="form-horizontal" id="form_2">
                        <input type="hidden" class="role_id"/>
                        <div class="form-group" style="padding: 0 15px;">
                            <p style="padding-left: 15px;padding-top: 10px;margin-bottom: 5px;">角色名称</p>
                            <div class="col-sm-12">
                                <input type="text" class="form-control name" placeholder="角色名称">
                            </div>
                        </div>
                        <div class="form-group" style="padding: 0 15px;">
                            <p style="padding-left: 15px;padding-top: 10px;margin-bottom: 5px;">角色描述</p>
                            <div class="col-sm-12">
                                <textarea class="form-control desc" rows="4" required="" aria-required="true" placeholder="角色描述"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="role_update()" class="btn btn-blue Auditing">保存</button>
                <button type="button" class="btn btn-white cancel" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>

<script src="/admin/js/jquery.min.js?v=2.1.4"></script>
<script src="/admin/js/bootstrap.min.js?v=3.3.6"></script>
<!--分页-->
<script src="/vendor/page/jquery.sPage.js"></script>
<!--提示框-->
<script type="text/javascript" src="/vendor/layer/layer.js" ></script>
<!-- artTemplate模板引擎 -->
<script type="text/javascript" src="/vendor/artTemplate/lib/template-web.js" ></script>
<!-- 自定义js -->
<script type="text/javascript" src="/custom/admin/role/js/powers_role.js?v=2"></script>

</body>
</html>
