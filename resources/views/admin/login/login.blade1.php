<!DOCTYPE html>
<!-- saved from url=(0051)http://demo.admui.com/login?user=xiaxuan@admui_demo -->
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>后台管理平台- 登录</title>
    <meta name="keywords" content="后台管理平台,后台管理平台">
    <meta name="description" content="后台管理平台,后台管理平台">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- 样式 -->
    <link rel="stylesheet" href="/admin/login/css/bootstrap.css">
    <link rel="stylesheet" href="/admin/login/css/site.css" id="siteStyle">
    <!-- 登录页面 -->
    <link rel="stylesheet" href="/admin/login/css/login.css">
    <!-- 插件 -->
    <link rel="stylesheet" href="/admin/login/css/bootstrap-select.css">
    <!--验证码-->
    <link rel="stylesheet" type="text/css" href="/vendor/identifyingCode/css/verify.css">
    <!--自定义-->
    <link rel="stylesheet" type="text/css" href="/custom/admin/login/css/login.css">
    <script type="text/javascript">
        (function (window) {
            var theUA = window.navigator.userAgent.toLowerCase();
            if ((theUA.match(/msie\s\d+/) && theUA.match(/msie\s\d+/)[0]) || (theUA.match(/trident\s?\d+/) && theUA.match(/trident\s?\d+/)[0])) {
                var ieVersion = theUA.match(/msie\s\d+/)[0].match(/\d+/)[0] || theUA.match(/trident\s?\d+/)[0];
                if (ieVersion < 9) {
                    //alert('你的浏览器版本太低了，推荐使用：360急速浏览器、谷歌浏览器、火狐浏览器等，如果你的使用的是双核浏览器,请切换到极速模式访问');
                    var str = "您的浏览器版本太low了,已经和时代脱轨了 :(";
                    var str2 = "为了您更好的体验本系统，推荐您使用以下几种浏览器访问<br>";
                    str2 += "<a href='https://browser.360.cn/ee/'>360急速浏览器</a>、";
                    str2 += '<a href ="https://www.google.cn/chrome/">谷歌浏览器</a>、';
                    str2 += '<a href="http://pc.uc.cn/website_page_ie.html">UC浏览器</a>、';
                    str2 += '<a href="http://dl.pconline.com.cn/download/60835-1.html">IE9下载</a>、';
                    str2 += '<a href="http://dl.pconline.com.cn/download/53735-1.html">IE10下载</a>、';
                    str2 += '<a href="https://support.microsoft.com/zh-cn/help/17621/internet-explorer-downloads">IE11下载</a>';
                    document.writeln("<pre class='worning'>" +
                        "<h3 style='padding-top:200px;margin:0'><strong>" + str + "<br/></strong></h3><h3>" +
                        str2 + "</h3><h3 style='margin:0'>如果您使用的是双核浏览器,请切换到极速模式访问<br/></h3></pre>");
                    document.execCommand("Stop");
                }
                ;
            }
        })(window);
    </script>
</head>

<body class="page-login layout-full page-dark">

<div class="page height-full">
    <div class="page-content height-full">
        <div class="page-brand-info vertical-align animation-slide-left hidden-xs"></div>
        <div class="page-login-main animation-fade">
            <div class="vertical-align">
                <div class="vertical-align-middle">
                    <h3 class="hidden-xs" style="margin-bottom:30px;">
                        登录 后台管理平台
                    </h3>
                    <p class="hidden-xs">onedog 后台管理系统</p>
                    <form class="login-form fv-form fv-form-bootstrap" id="form_login" novalidate="novalidate">
                        <div class="form-group">
                            <input type="text" class="form-control phone" maxlength="11" placeholder="请输入账号" />
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control password" placeholder="请输入密码" />
                        </div>
                        <div class="form-group" id="mpanel1"></div>
                        <div class="form-group clearfix">
                            <div class="pull-right">
                                <a>注册账号</a>/
                                <a>忘记密码</a>
                            </div>
                        </div>
                        <button type="button" class="btn btn-primary btn-block margin-top-30" onclick="login()">立即登录</button>
                    </form>

                </div>

            </div>

            <footer class="page-copyright">
                <p>一狗跟单系统 ©
                    <a href="http://www.onedog.cn" target="_blank">www.onedog.cn</a>
                </p>
            </footer>
        </div>
    </div>
</div>

<!-- JS -->
<script src="/admin/login/js/jquery.js"></script>
<script src="/admin/login/js/bootstrap.js"></script>
<script src="/admin/login/js/bootstrap-select.min.js"></script>
<script src="/admin/login/js/formValidation.min.js" data-name="formValidation"></script>
<script src="/admin/login/js/bootstrap.min.js" data-deps="formValidation"></script>
<!--提示框-->
<script type="text/javascript" src="/vendor/layer/layer.js" ></script>
<!--验证码-->
<script type="text/javascript" src="/vendor/identifyingCode/js/verify.js"></script>
<!--自定义js-->
<script type="text/javascript" src="/custom/admin/login/js/login.js?v=0.0.1"></script>

</body>