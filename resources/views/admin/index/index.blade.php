<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>后台系统</title>

    <link href="/admin/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="/admin/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="/admin/css/animate.min.css" rel="stylesheet">
    <link href="/admin/css/style.min862f.css?v=4.1.0" rel="stylesheet">
    <link href="/admin/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <link rel="stylesheet" href="/admui/themes/classic/base/css/site.css" id="admui-siteStyle">
    <script src="/admin/js/jquery-2.1.1.min.js"></script>
    <!--阿里图标-->
    <link rel="stylesheet" type="text/css" href="/vendor/aliicon/iconfont.css?v=0.0.1">
    <link href="/admin/page/pagination.css" rel="stylesheet">
    <!--自定义css-->
    <link rel="stylesheet" type="text/css" href="/custom/admin/index/css/index.css">
    <style>
        .new-header {
            border: none;
            height: 60px;
            margin-top: 0px;
            margin-left: 0px;
            background: rgb(62, 142, 247);
            padding-left: 25px;
        }

        .new-title-user {
            height: 60px;
            line-height: 60px;
            float: right;
            width: auto;
        }

        .new-title-icon {
            height: 60px;
            line-height: 62px;
            float: right;
            color: #fff;
            margin-right: 10px;
        }

        .new-title-icon .fa-bell {
            font-size: 18px !important;
        }

        .dropdown-menu li a {
            padding: 10px;
        }
        
        .img-circle {
            width: 35px;
            height: 35px;
            /*box-shadow: 0px 14px 24px 0px rgba(0, 0, 0, 0.15);*/
        }

        .jiange {
            padding-left: 20px;
            float: right;
            line-height: 60px;
            margin-right: 30px;
            color: #f3f3f4;
        }

        .text-muted {
            color: #f3f3f4;
        }

        body .layer-ext-moon .layui-layer-btn {
            padding: 0 10px 12px !important;
            background: #fff !important;
            border: none !important;
        }

        .dht-right {
            height: 60px;
            /*background: #4e97d9;*/
            color: #fff;
            position: absolute;
            top: 0;
            right: 15px;
            display: flex;
        }

        .zdy-btn {
            height: 60px;
            line-height: 60px;
            text-align: center;
            cursor: pointer;
        }

        .msgb {
            display: flex;
        }

        .smsspan {
            flex: 1;
            margin: 0 15px;
        }

        .nav > li.active {
            border-left: 4px solid #3e8ef7;
        }


    </style>

</head>
<body class="fixed-sidebar full-height-layout gray-bg" id="body" style="overflow:hidden;">

<div class="header" style="background-color: #3e8ef7!important;">
    <a class="header-left" onclick="MeInfo()">
        <div style="float: left; margin-left: 10px;font-size: 26px;color: #fff;text-align: center;width: 80%;">
            ONEDOG
        </div>
    </a>
    <div class="header-right">
        <div class="row content-tabs"
             style="border: none;height: 60px;margin-top: 0px;margin-left: 0px; background: #3e8ef7;">
            <button class="roll-nav roll-left J_tabLeft"
                    style="background: #4e97d9;height: 60px;color:#fff;border: none;background: #3e8ef7;">
                <i class="fa fa-backward"></i>
            </button>
            <nav class="page-tabs J_menuTabs" style="line-height: 60px; height: 60px;color:#fff;">
                <div class="page-tabs-content">
                    {{--<a href="javascript:;" class="active J_menuTab" data-id="/admin/index/default_index_html">首页</a>--}}
                </div>
            </nav>
            <div class="dht-right">
                <button class="J_tabRight i-btn" style="background: #3e8ef7;height: 60px;">
                    <i class="fa fa-forward"></i>
                </button>
                <div class="row content-tabs new-header">
                    <a class="J_tabExit new-title-user">
                        <div class="dropdown profile-element" style="float: right;margin-right: 40px">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#"
                               style="height: 60px;line-height: 60px">
                                <span class="text-muted text-xs block">
                                    <img alt="image" class="img-circle" src="{{$admin['heard_img']}}"/>
                                    <b class="caret" style="border-top:6px solid;margin-left: 10px"></b>
                                </span>
                            </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs"
                                style="left: -100px;box-shadow: 1px 1px 8px #d5d5d8;">
                                <li>
                                    <a class="J_menuItem" onclick="MeInfo(1)"
                                       style="text-align: center;padding: 10px;color: #262626;">
                                        <span class="iconfont iconzhanghu"
                                              style="font-size: 13px;margin-right:5px"></span>
                                        个人中心
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a id="quit" data-toggle="modal" data-placement="bottom"
                                       style="text-align: center;padding: 5px 10px 10px 10px;color: #262626;">
                                        <span class="iconfont icontuichu"
                                              style="position:  relative;top: 3px;margin-right:5px"></span>
                                        安全退出
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </a>
                    <span class="jiange">|</span>
                    <a class="J_tabExit new-title-icon" onclick="MeInfo(2)" data-toggle="modal" data-placement="bottom" id="adminsms">
                        <i class="fa fa-bell"></i>
                        <span class="badge badge-danger up msg-num" id="msgnum" style="top:-16px;left:-5px;font-weight: 400;padding: 3px 6px;"></span>
                    </a>

                </div>
            </div>
        </div>


    </div>
</div>
<div id="wrapper">
    <!--左侧导航开始-->
    <nav class="navbar-default navbar-static-side" role="navigation" style="padding-bottom:75px">
        <div class="sidebar-collapse" style="margin-top: 15px">
            <ul class="nav" id="side-menu">
                {{--<li style="position: relative">--}}
                    {{--<a class="J_menuItem" href="/admin/index/default_index_html">--}}
                        {{--<div style="display: inline-block;font-weight: 500;text-align: center"><i class="iconfont">&#xe61d;</i>--}}
                        {{--</div>--}}
                        {{--<span class="nav-label name" style="font-weight: 500;">首页</span>--}}
                    {{--</a>--}}
                {{--</li>--}}
                <li>
                    <a class="J_menuItem" href="/admin/menu/menu_list_html">
                        <div style="display: inline-block;font-weight: 500;text-align: center">
                            <i class="iconfont">&#xe65c;</i>
                        </div>
                        <span class="nav-label name" style="font-weight: 500;">菜单管理</span>
                    </a>
                </li>
                <li>
                    <a class="J_menuItem" href="/admin/powers/power_list_html">
                        <div style="display: inline-block;font-weight: 500;text-align: center">
                            <i class="iconfont">&#xe63b;</i>
                        </div>
                        <span class="nav-label name" style="font-weight: 500;">权限管理</span>
                    </a>
                </li>
                <li>
                    <a class="J_menuItem" href="/admin/role/role_list_html">
                        <div style="display: inline-block;font-weight: 500;text-align: center">
                            <i class="iconfont">&#xe60d;</i>
                        </div>
                        <span class="nav-label name" style="font-weight: 500;">角色管理</span>
                    </a>
                </li>
                <li>
                    <a class="J_menuItem" href="/admin/user/user_list_html">
                        <div style="display: inline-block;font-weight: 500;text-align: center">
                            <i class="iconfont">&#xe6e6;</i>
                        </div>
                        <span class="nav-label name" style="font-weight: 500;">用户管理</span>
                    </a>
                </li>
                <li>
                    <a class="J_menuItem" href="/admin/config/config_html">
                        <div style="display: inline-block;font-weight: 500;text-align: center">
                            <i class="iconfont">&#xeb8d;</i>
                        </div>
                        <span class="nav-label name" style="font-weight: 500;">系统配置</span>
                    </a>
                </li>

                {{--<li>
                    <a href="#">
                        <div style="display: inline-block;font-weight: 500;text-align: center">
                            <i class="iconfont">&#xeb8d;</i>
                        </div>
                        <span class="nav-label" style="font-weight: 500;">数据统计</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a class="J_menuItem" href="/admin/powers/power_list_html" style="font-weight: 500"><span
                                        class="name">权限列表</span></a>
                        </li>
                    </ul>
                </li>--}}
                <li style="display: none;">
                    <a class="J_menuItem" id="FaultInfo" href="" data-index="99"></a>
                </li>
            </ul>
        </div>
    </nav>

    <!--左侧导航结束-->
    <!--右侧部分开始-->
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row J_mainContent" id="content-main" style=" height: calc(100% - 50px);">
            {{--<iframe class="J_iframe" id="iframs" name="iframe0" width="100%" height="100%"--}}
                    {{--src="/admin/index/default_index_html" frameborder="0" data-id="default_index_html.html"--}}
                    {{--seamless></iframe>--}}
            <iframe class="J_iframe" id="iframs" name="iframe0" width="100%" height="100%"
                    src="/admin/user/user_list_html" frameborder="0" data-id="default_index_html.html"
                    seamless></iframe>



        </div>
    </div>
    <!--右侧部分结束-->
    <div class="small-chat-box fadeInRight animated" id="msgdisplay"
         style="overflow: hidden;height:auto;width: 360px;top: 70px;right: 105px;bottom: auto">
        <ul class="dropdown-menu dropdown-alerts"
            style="display: block;position: initial; padding: 0px; width: 100%;box-shadow: none">
            <li class="dropdown-menu-header" role="presentation" style="padding:15px; margin:0px;">
                <span>最新消息</span>
                <span class="label label-round label-danger"></span>
            </li>
            <li class="list-group" style="margin-bottom: 0px; margin:0px;">
                <div id="sysmsg" style="max-height: 300px;overflow: auto;"></div>
            </li>
            <li class="dropdown-menu-footer" role="presentation" style="padding:15px; margin:0px;">
                <a onclick="" data-pjax=""><i class="fa fa-navicon"></i> &nbsp;所有消息</a>
            </li>
        </ul>
    </div>
</div>
<audio autoplay="autoplay" id="auto" src=""></audio>
</body>

<script src="/admin/js/jquery.min.js?v=2.1.4"></script>
<script src="/admin/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/admin/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="/admin/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="/admin/js/plugins/layer/layer.min.js"></script>
<script src="/admin/js/hplus.min.js?v=4.1.0"></script>
<script type="text/javascript" src="/admin/js/contabs.min.js?v=0.0.1"></script>
<script src="/admin/js/plugins/pace/pace.min.js"></script>
<script src="/admin/js/plugins/sweetalert/sweetalert.min.js"></script>
<!--分页-->
<script src="/admin/page/jquery.page.js"></script>
<!--提示框-->
<!--<script type="text/javascript" src="/vendor/layer/layer.js" ></script>-->
<!--自定义js-->
<script type="text/javascript" src="/custom/admin/index/js/index.js?v=0.0.1"></script>
<script type="text/javascript">
    $("[data-toggle='modal']").tooltip();

</script>

</html>
