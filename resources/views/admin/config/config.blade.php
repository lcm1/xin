<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>后台管理平台</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Preview page of Metronic Admin Theme #2 for statistics, charts, recent events and reports" name="description" />
    <meta content="" name="author" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
	
    <link rel="shortcut icon" href="favicon.ico">
    {{--<link href="/admin/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">--}}
    <link href="/admin/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/admin/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="/admin/css/hbwwork.css" rel="stylesheet">
    <link href="/admin/css/style.min862f.css?v=4.1.0" rel="stylesheet">
    <!--选项-->
    <link href="/admin/selectedfrom/css/site.css" rel="stylesheet">
    <!-- 图标 CSS-->
    <link rel="stylesheet" href="/admin/selectedfrom/fonts/web-icons/web-icons.css">
    <!--阿里图标-->
    <link rel="stylesheet" type="text/css" href="/vendor/aliicon/iconfont.css">
    <!--单选按钮-->
	<link rel="stylesheet" href="/admui/themes/classic/base/css/site.css" id="admui-siteStyle">
    <!-- 自定义css -->
    <link rel="stylesheet" href="/custom/admin/config/css/config.css?v=1">

</head>

<body>
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-green-sharp uppercase">
                <a onclick="top.location.href='/admin/index/index_html'" style="color: #3e8ef7">首页</a>&nbsp;/&nbsp;
                <span class="caption-subject font-green-sharp uppercase" style="color: #76838f">系统配置</span>
            </span>
        </div>
    </div>

    <div class="ibox-content main">

        <ul class="ul_box" id="config">
            <li class="li_box">
                <p class="li_box_p">基本配置</p>
                <div style="overflow:auto;">
                    <div class="form-group col-sm-4">
                        {{--<label class="col-sm-3 control-label">秘钥</label>--}}
                        <label class="col-sm-2 control-label">秘钥</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control access_key" placeholder="秘钥">
                        </div>
                    </div>
                </div>
            </li>
        </ul>


    </div>
</div>
<div style="position: fixed;bottom:10px;width: 100%;z-index:100;text-align:center;background-color:#ecececbf;padding: 10px;">
    <button type="button" class="btn btn-blue" onclick="config_update()">保存</button>
    {{--<button type="reset" onclick="javascript:history.back(-1);" class="btn btn-default btn-outline" style="background-color: #fff;margin-left: 10px;">返回</button>--}}
</div>

<!--弹窗-->
<!--秘钥验证-->
<div class="modal inmodal fade" id="myModal_1" tabindex="-1" role="dialog"  aria-hidden="true" data-backdrop="static" style="background: rgb(156, 156, 156)">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="float-e-margins">
                <div class="portlet-title" style="font-size: 18px;padding: 15px 24px;background: #f3f7f9">秘钥验证</div>
                <div class="ibox-content">
                    <form class="form-horizontal" id="form_1">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="password" class="form-control access_key" placeholder="请填写秘钥">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-blue Auditing" onclick="checking_key()">确认</button>
                {{--<button type="button" class="btn btn-white cancel" data-dismiss="modal">取消</button>--}}
            </div>
        </div>
    </div>
</div>


<script src="/admin/js/jquery.min.js?v=2.1.4"></script>
<script src="/admin/js/bootstrap.min.js?v=3.3.6"></script>
<!--时间-->
<script src="/vendor/layDate/laydate.js"></script>
<!--提示框-->
<script type="text/javascript" src="/vendor/layer/layer.js" ></script>
<!-- artTemplate模板引擎 -->
<script type="text/javascript" src="/vendor/artTemplate/lib/template-web.js" ></script>
<!-- 自定义js -->
<script type="text/javascript" src="/custom/admin/config/js/config.js?v=2"></script>

</body>
</html>
