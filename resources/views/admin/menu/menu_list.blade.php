<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="Preview page of Metronic Admin Theme #2 for statistics, charts, recent events and reports"
          name="description"/>
    <meta content="" name="author"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--选项-->
    <link href="/admin/selectedfrom/css/site.css" rel="stylesheet">
    <!--单选按钮-->
    <link rel="stylesheet" href="/admui/themes/classic/base/css/site.css" id="admui-siteStyle">
    <link href="/admin/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/admin/css/components.min.css" rel="stylesheet" id="style_components" type="text/css"/>
    <link href="/admin/css/animate.min.css" rel="stylesheet">
    <link href="/admin/css/style.min862f.css?v=4.1.0" rel="stylesheet">
    <link href="/admin/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="/admin/tab/css/stylepc.css">
    <!--图标-->
    <link rel="stylesheet" type="text/css" href="/vendor/aliicon/iconfont.css">

    <!--自定义css-->
    <link rel="stylesheet" href="/custom/admin/menu/css/menu_list.css?v=1">

</head>
<body>
<div class="cecontainer">
    <div class="menu" style="width:220px;">
        <ul class="ulmenu1">
            <li>
                <div class="title">
                    <i class="fa fa-bookmark"></i>&nbsp;&nbsp;一级菜单
                    <span class="type_num"></span>
                </div>
            </li>
        </ul>
        <ul class="ulmenu2" id="menuone_ul" style="max-height: 71vh;overflow: auto;">
            <script id="menuone_script" type="text/html">
                {if data.length>0}
                    {each data vo}
                    <li onclick="menuone_click(this)">
                        <a class="checked com">
                            <span class="menu_name" menu_id="{vo['Id']}">{vo['name']}</span>

                            <span class="operate-del" data-original-title="编辑"
                                  data-toggle="modal" onclick="menuone_update_inv({vo['Id']})" style="right:40px;" />
                            <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                            </span>
                            <span class="operate-del" data-original-title="删除"
                                  data-toggle="modal" onclick="menuone_del({vo['Id']})">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                </span>
                        </a>
                    </li>
                    {/each}
                {else}
                    <li class="menuone_no">暂无一级菜单</li>
                {/if}
            </script>

        </ul>
        <ul style="border: none;padding: 0px">
            <li>
                <a onclick="menuone_add_inv()">
                    <i class="fa fa-plus"></i>&nbsp;&nbsp;添加一级菜单
                </a>
            </li>
        </ul>
    </div>
    <div class="content" style="margin-left:220px;">
        <div class="portlet light ">
            <div class="portlet-title" style="height: 64px;margin-bottom: 15px;">
                <div class="caption">
                    <i class="icon-anchor font-green-sharp"></i>
                    <span id="menuone_name" class="caption-subject uppercase" style="color: #3e8ef7!important"></span>
                </div>
                <div class="title_btn">
                    <button type="button" class="btn btn-blue" onclick="menu_add_inv()">新增菜单</button>
                </div>
            </div>

            <div class="tabbable tabbable-tabdrop">
                <div id="screen">
                    <div class="tab-content">
                        <div class="tab-pane active in fade" style="position: relative;min-height: 60vh;">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>序号</th>
                                    <th>名称</th>
                                    <th>路径</th>
                                    <th>图标</th>
                                    <th>标识</th>
                                    <th style="width: 15vw">操作</th>
                                </tr>
                                </thead>
                                <tbody id="menu_tbody">
                                <!--二级菜单列表--模板-->
                                <script id="menu_script" type="text/html">
                                    {each data vo}
                                    <tr power_id="{vo['ID']}">
                                        <td>{vo['serial_number']}</td>
                                        <td>{vo['name']}</td>
                                        <td>{vo['url']}</td>
                                        <td>{vo['icon']}</td>

                                        <td>{vo['identity']}</td>
                                        <td>
                                            <button  data-original-title="编辑" data-toggle="modal" onclick="menu_update_inv({vo['Id']})" class="btn btn-outline btn-success operation_btn">
                                                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                            </button>
                                            <button data-original-title="删除" data-toggle="modal" class="btn btn-outline btn-success operation_btn" onclick="menu_del({vo['Id']});">
                                                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    {/each}

                                    {if data.length == 0}
                                        <div class="nodata">无数据！</div>
                                    {/if}
                                </script>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

<!--弹窗-->
<!--编辑一级菜单-->
<div class="modal inmodal fade" id="myModal_1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width:500px;">
        <div class="modal-content">
            <div class="float-e-margins">
                <div class="portlet-title new-title">
                    &nbsp;&nbsp;&nbsp;编辑一级菜单
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal m-t" id="from_1">
                        <input type="hidden" class="menu_id" />
                        <div class="form-group">
                            <label class="col-sm-3">菜单名称</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control name" placeholder="菜单名称">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">跳转路径</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control url" placeholder="跳转路径">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">菜单图标</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control icon" placeholder="菜单图标">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">排列序号</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control serial_number" placeholder="排列序号">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">权限标识</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control identity" placeholder="权限标识">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-blue" onclick="menuone_update()">保存</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>

<!--新增一级菜单-->
<div class="modal inmodal fade" id="myModal_2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width:500px;">
        <div class="modal-content">
            <div class=" float-e-margins">
                <div class="portlet-title new-title">
                    &nbsp;&nbsp;&nbsp;新增一级菜单
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal m-t" id="form_2">
                        <div class="form-group">
                            <label class="col-sm-3">菜单名称</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control name" placeholder="菜单名称">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">跳转路径</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control url" placeholder="跳转路径">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">菜单图标</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control icon" placeholder="菜单图标">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">排列序号</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control serial_number" placeholder="排列序号">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">权限标识</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control identity" placeholder="权限标识">
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-blue" onclick="menuone_add()">新增</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>

<!--新增二级菜单-->
<div class="modal inmodal fade" id="myModal_3" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width:500px;">
        <div class="modal-content">
            <div class=" float-e-margins">
                <div class="portlet-title new-title">
                    &nbsp;&nbsp;&nbsp;新增二级菜单
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal m-t" id="form_3">
                        <div class="form-group">
                            <label class="col-sm-3">一级菜单</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control fid" fid="" placeholder="一级菜单" disabled />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">菜单名称</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control name" placeholder="菜单名称" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">跳转路径</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control url" placeholder="跳转路径" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">菜单图标</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control icon" placeholder="菜单图标" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">排列序号</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control serial_number" placeholder="排列序号" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">权限标识</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control identity" placeholder="权限标识" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-blue" onclick="menu_add()">新增</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>

<!--编辑二级菜单-->
<div class="modal inmodal fade" id="myModal_4" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width:500px;">
        <div class="modal-content">
            <div class=" float-e-margins">
                <div class="portlet-title new-title">
                    &nbsp;&nbsp;&nbsp;编辑二级菜单
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal m-t" id="form_4">
                        <input type="hidden" class="menu_id" />
                        <div class="form-group">
                            <label class="col-sm-3">一级菜单</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control fid" placeholder="一级菜单" disabled />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">菜单名称</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control name" placeholder="菜单名称" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">跳转路径</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control url" placeholder="跳转路径" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">菜单图标</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control icon" placeholder="菜单图标" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">排列序号</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control serial_number" placeholder="排列序号" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">权限标识</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control identity" placeholder="权限标识" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-blue" onclick="menu_update()">保存</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>


<script src="/admin/js/jquery-2.1.1.min.js"></script>
<script src="/admin/js/bootstrap.min.js"></script>
<!--分页-->
<script src="/vendor/page/jquery.sPage.js"></script>
<!--layer提示-->
<script type="text/javascript" src="/vendor/layer/layer.js"></script>
<!-- artTemplate模板引擎 -->
<script type="text/javascript" src="/vendor/artTemplate/lib/template-web.js"></script>
<!--自定义js-->
<script src="/custom/admin/menu/js/menu_list.js?v=2"></script>

</body>
</html>
