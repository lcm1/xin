<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="Preview page of Metronic Admin Theme #2 for statistics, charts, recent events and reports"
          name="description"/>
    <meta content="" name="author"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--选项-->
    <link href="/admin/selectedfrom/css/site.css" rel="stylesheet">
    <!--单选按钮-->
    <link rel="stylesheet" href="/admui/themes/classic/base/css/site.css" id="admui-siteStyle">
    <link href="/admin/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/admin/css/components.min.css" rel="stylesheet" id="style_components" type="text/css"/>
    <link href="/admin/css/animate.min.css" rel="stylesheet">
    <link href="/admin/css/style.min862f.css?v=4.1.0" rel="stylesheet">
    <link href="/admin/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="/admin/tab/css/stylepc.css">
    <!--图标-->
    <link rel="stylesheet" type="text/css" href="/vendor/aliicon/iconfont.css">

    <!--自定义css-->
    <link rel="stylesheet" href="/custom/admin/powers/css/powers_list.css?v=4">

</head>
<body>
<div class="cecontainer">
    <div class="menu" style="width:220px;">
        <ul class="ulmenu1">
            <li>
                <div class="title">
                    <i class="fa fa-bookmark"></i>&nbsp;&nbsp;权限分类
                    <span class="type_num">{{count($powers_type)}}</span>
                </div>
            </li>
        </ul>
        <ul class="ulmenu2" style="max-height: 71vh;overflow: auto;">
            @if(count($powers_type) == 0 || $powers_type == '')
                <li class="notypeli">
                    <div class="notype">暂无权限分类</div>
                </li>
            @else
                @foreach($powers_type as $vo)
                    <li onclick="powertype_click(this)">
                        <a class="checked com">
                            <span class="typename" powertype_id="{{$vo['Id']}}">{{$vo['name']}}</span>
                            <span class="operate-del" data-original-title="编辑"
                                  data-toggle="modal" onclick="powertype_update_inv({{$vo['Id']}})">
                                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                            </span>
                            {{--<span class="operate-del" power_id="{{$vo['Id']}}" data-original-title="删除"--}}
                                  {{--data-toggle="modal" onclick="admin_update_inv({{$vo['Id']}})" style="right:40px;">--}}
                                {{--<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>--}}
                            {{--</span>--}}
                        </a>
                    </li>
                @endforeach
            @endif
        </ul>
        <ul style="border: none;padding: 0px">
            <li>
                <a onclick="powertype_add_inv()">
                    <i class="fa fa-plus"></i>&nbsp;&nbsp;添加权限分类
                </a>
            </li>
        </ul>
    </div>
    <div class="content" style="margin-left:220px;">
        <div class="portlet light ">
            <div class="portlet-title" style="height: 64px;margin-bottom: 15px;">
                <div class="caption">
                    <i class="icon-anchor font-green-sharp"></i>
                    <span id="powertype_name" class="caption-subject uppercase" style="color: #3e8ef7!important"></span>
                </div>
                <div class="title_btn">
                    <button type="button" class="btn btn-blue" onclick="power_add_inv()">新增权限</button>
                </div>
            </div>

            <div class="tabbable tabbable-tabdrop" style="position: relative;">
                <div id="screen">
                    <div class="tab-content">
                        <div class="tab-pane active in fade">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>权限名称</th>
                                    <th>权限标识</th>
                                    <th>权限类别</th>
                                    <th>权限类型</th>
                                    <th style="width: 15vw">操作</th>
                                </tr>
                                </thead>
                                <tbody id="area">
                                <!--资产列表--模板-->
                                <script id="content" type="text/html">
                                    {each list vo}
                                    <tr power_id="{vo['Id']}">
                                        <td>{vo['Id']}</td>
                                        <td>{vo['name']}</td>
                                        <td>{vo['identity']}</td>
                                        <td>{vo['powertype_name']}</td>
                                        {if vo['type']==1}
                                        <td>查询</td>
                                        {else}
                                        <td>操作</td>
                                        {/if}
                                        <td>
                                            <button  data-original-title="编辑" data-toggle="modal" onclick="power_update_inv({vo['Id']})" class="btn btn-outline btn-success operation_btn">
                                                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                            </button>
                                            <button data-original-title="删除" data-toggle="modal" class="btn btn-outline btn-success operation_btn" onclick="power_del({vo['Id']});">
                                                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    {/each}

                                    {if list.length == 0}
                                        <div class="nodata">无数据！</div>
                                    {/if}
                                </script>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

<!--弹窗-->
<!--编辑权限分类-->
<div class="modal inmodal fade" id="myModal_1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width:500px;">
        <div class="modal-content">
            <div class=" float-e-margins">
                <div class="portlet-title new-title">
                    &nbsp;&nbsp;&nbsp;编辑权限分类
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal m-t" id="from_1">
                        <input type="hidden" class="powertype_id" />
                        <div class="form-group">
                            {{--<label class="col-sm-3">分类名称</label>--}}
                            <div class="col-sm-12">
                                <input type="text" class="form-control name" placeholder="权限分类名称">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-blue" onclick="powertype_update()">保存</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>

<!--新增权限分类-->
<div class="modal inmodal fade" id="myModal_2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width:500px;">
        <div class="modal-content">
            <div class=" float-e-margins">
                <div class="portlet-title new-title">
                    &nbsp;&nbsp;&nbsp;新增权限分类
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal m-t" id="form_2">
                        <div class="form-group">
                            {{--<label class="col-sm-3">权限名称</label>--}}
                            <div class="col-sm-12">
                                <input type="text" class="form-control name" placeholder="权限分类名称"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-blue" onclick="powertype_add()">新增</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>

<!--新增权限-->
<div class="modal inmodal fade" id="myModal_3" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width:500px;">
        <div class="modal-content">
            <div class=" float-e-margins">
                <div class="portlet-title new-title">
                    &nbsp;&nbsp;&nbsp;新增权限
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal m-t" id="form_3">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" class="form-control power_type" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">权限名称</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control name" placeholder="权限名称">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">权限标识</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control identity" placeholder="权限标识">
                            </div>
                        </div>

                        <div class="form-group" style="padding-left: 0;">
                            <label class="col-sm-4">权限类型</label>
                            <div class="col-sm-12 input_div">
                                <div class="col-sm-3" style="padding:0px">
                                    <div class="radio-custom radio-primary">
                                        <input type="radio" id="add_type02" value="2" name="type" checked="">
                                        <label for="add_type02">操作</label>
                                    </div>
                                </div>
                                <div class="col-sm-3" style="padding:0px">
                                    <div class="radio-custom radio-primary">
                                        <input type="radio" id="add_type01" value="1" name="type">
                                        <label for="add_type01">查询</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-blue" onclick="power_add()">新增</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>

<!--编辑权限-->
<div class="modal inmodal fade" id="myModal_4" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width:500px;">
        <div class="modal-content">
            <div class=" float-e-margins">
                <div class="portlet-title new-title">
                    &nbsp;&nbsp;&nbsp;编辑权限
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal m-t" id="from_4">
                        <input type="hidden" class="power_id" />
                        <div class="form-group">
                            <label class="col-sm-3">权限名称</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control name" placeholder="权限名称">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">权限标识</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control identity" placeholder="权限标识">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4">权限类型</label>
                            <div class="col-sm-12 input_div">
                                <div class="col-sm-3" style="padding:0px">
                                    <div class="radio-custom radio-primary">
                                        <input type="radio" id="type04" value="2" name="type02">
                                        <label for="type04">操作</label>
                                    </div>
                                </div>
                                <div class="col-sm-3" style="padding:0px">
                                    <div class="radio-custom radio-primary">
                                        <input type="radio" id="type03" value="1" name="type02" checked="">
                                        <label for="type03">查询</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-blue" onclick="power_update()">保存</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>


<script src="/admin/js/jquery-2.1.1.min.js"></script>
<script src="/admin/js/bootstrap.min.js"></script>
<!--分页-->
<script src="/vendor/page/jquery.sPage.js"></script>
<!--layer提示-->
<script type="text/javascript" src="/vendor/layer/layer.js"></script>
<!-- artTemplate模板引擎 -->
<script type="text/javascript" src="/vendor/artTemplate/lib/template-web.js"></script>
<!--自定义js-->
<script src="/custom/admin/powers/js/powers_list.js?v=2"></script>

</body>
</html>
