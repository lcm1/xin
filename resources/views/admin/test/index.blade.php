<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ONEDOG.TEST</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="title m-b-md">
            ONEDOG-TEST
        </div>
        <div class="links">
            <div style="border:solid 1px #000">
                <div>
                    <form action="{{route('test.fileUpload')}}" method="post" enctype="multipart/form-data">
                        <table border="1">
                            <tr>
                                <th>文件上传|fileUpload-Test</th>
                                <th>Route:/test/fileUpload</th>
                                <th>Value:/id/file</th>
                                <th>Download</th>
                            </tr>
                            <tr>
                                <td>
                                    id：<input type="number" name="id" value="1" readonly="readonly">
                                </td>
                                <td>
                                    <input type="file" name="file">
                                </td>
                                <td>
                                    <input type="submit">
                                </td>
                                @foreach($orders as $order)
                                    <td>
                                        <a href="http://www.onedog.com/{{$order->inspection_report}}" download="验货报告">
                                            下载文件
                                        </a>
                                    </td>
                                @endforeach
                            </tr>
                        </table>
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
