<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class Temporarily extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:temporarily {type}';

    /**
     * 临时一次性脚本
     *
     * @var string
     */
    protected $description = '临时一次性脚本';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $type = $this->argument('type');

            switch ($type) {
                case 1:
//                    $this->fun1();
                    break;
                case 2:
//                    $this->fun2();
                    break;
                case 3:
//                    $this->fun3();
                    break;
                default:
                    echo '未知参数,程序结束';
                    exit;
            }
            echo '执行成功';
        } catch (\Exception $e) {
            //记录错误日志
            $add['job']  = 'SyncSupplier';
            $add['time'] = time();
            $add['msg']  = $e->getMessage();
            $add['data'] = '';
            DB::table('job_err_log')->insert($add);
        }

    }

    //同步艾诺科同步合同表的供应商id
    private function fun1()
    {
        exit;
        $list = DB::table('cloudhouse_contract_total')
            ->where('supplier_short_name', '<>', '')
            ->where('supplier_id', 0)
            ->get();

        $count = $list->count();

        $n = 1;
        foreach ($list as $value) {
            echo $count . '_____' . $n . "\n";
            $n++;


            $supplierId = DB::table('suppliers')
                ->where('name', $value->supplier_short_name)
                ->orWhere('short_name', $value->supplier_short_name)
                ->value('id');
            if (!$supplierId) {
                continue;
            }

            DB::table('cloudhouse_contract_total')
                ->where('id', $value->id)
                ->update(['supplier_id' => $supplierId]);

        }

    }

    //较验库位库存数据1
    private function fun2()
    {
        exit;
        $locationNumList = DB::table('cloudhouse_location_num')
            ->select('*', DB::raw('sum(num) as sum'))
            ->groupBy('location_id', 'custom_sku_id')
            ->get();


        $total = $locationNumList->count();
        echo $total . "\n";

        $n = 1;
        foreach ($locationNumList as $locationNum) {
            echo $total . '__' . $n . "\n";


            $locationLog = DB::table('cloudhouse_location_log')
                ->select('type', 'num')
                ->where('location_id', $locationNum->location_id)
                ->where('custom_sku_id', $locationNum->custom_sku_id)
                ->whereIn('type', [1, 2, 3, 5, 6])
                ->get();


            $num = 0;
            foreach ($locationLog as $log) {
                if (in_array($log->type, [1, 3, 5])) {
                    $num += $log->num;
                } else {
                    $num -= $log->num;
                }
            }

            if ($num != $locationNum->sum) {
                DB::table('location_data_checkout')->insert([
                    'spu_id'        => $locationNum->spu_id,
                    'spu'           => $locationNum->spu,
                    'custom_sku_id' => $locationNum->custom_sku_id,
                    'custom_sku'    => $locationNum->custom_sku,
                    'location_id'   => $locationNum->location_id,
                    'num'           => $locationNum->sum,
                    'check_num'     => $num,
                    'change'        => $locationNum->change,
                ]);
            }

            $n++;
        }
    }

    //较验库位库存数据2
    private function fun3()
    {
        exit;
        $list = DB::table('location_data_checkout')->where('change', 816)->get();

        $total           = $list->count();
        echo $total . "\n";

        $n = 1;

        foreach ($list as $v) {
            echo $total . '__' . $n . "\n";


            if ($v->check_num < $v->num) {
                continue;
            }

            $balanceNum = $v->check_num - $v->num;

            $locationNumInfo = DB::table('cloudhouse_location_num')
                ->where('platform_id', 12)
                ->where('location_id', $v->location_id)
                ->where('custom_sku_id', $v->custom_sku_id)
                ->where('change', 816)
                ->first();

            if (!$locationNumInfo) {
                continue;
            }


            DB::table('cloudhouse_location_num')
                ->where('platform_id', 12)
                ->where('location_id', $v->location_id)
                ->where('custom_sku_id', $v->custom_sku_id)
                ->where('change', 816)
                ->increment('num', $balanceNum);


            $n++;
        }
    }

}
