<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class FpxUpdate extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'FpxUpdate';
	
	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = '更新4px任务状态';
	
	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}
	
	public $rabbitmq;     //rabbitmq连接
	
	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
        \Log::info('fpxTask executed successfully.');
		//$this->rabbitmq = new \App\Libs\wrapper\Rabbitmq('exchange_3', 'queue_3', 'key_3');
		//$callback = array($this, 'receive');
		//($this->rabbitmq)->deal_mq($callback);
	}
	
	/*
	 * 接收消息并进行处理的回调方法
	 * */
	public function receive($msg) {
		$msg_data = json_decode($msg->body, true);
		
//		var_dump($msg_data);exit;
//		$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);exit;// ack 确认
		// 判断时间
		if($msg_data['time_zx'] > time()){
			// 决绝消息
			$msg->delivery_info['channel']->basic_nack($msg->delivery_info['delivery_tag'], false, true);
		}else{
			
			$call = new \App\Libs\wrapper\Spu();
			//platform_id 平台id：2.wish，3.lazada，4.vova
			if($msg_data['platform_id'] == 2){
				// wish
				$return = $call->product_upload_wish($msg_data);
			}else if($msg_data['platform_id'] == 3){
				// lazada
				$return = $call->product_get_lazada($msg_data);
			}else if($msg_data['platform_id'] == 4){
				// vova
				$return = $call->product_get_vova($msg_data);
				if(is_array($return)){
					($this->rabbitmq)->send_message($return);// 新增任务
				}
			}
//			var_dump($return);exit;
			
			$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);//ack
		    var_dump($return);//exit;
		}
		
		
		
	}
	
	
	
	
	
}//
