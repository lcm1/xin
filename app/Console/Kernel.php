<?php
namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel{
    /**
     * The Artisan commands provided by your application. 调度任务文件路径
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\LingXing::class,//翎星
    ];
	
    /**
     * Define the application's command schedule.  定义应用程序的命令计划。
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void   	/www/wwwroot/default/onedog1/public
     */



    //  ->cron('* * * * *');	在自定义Cron调度上运行任务
    // ->everyMinute();	每分钟运行一次任务
    // ->everyFiveMinutes();	每五分钟运行一次任务
    // ->everyTenMinutes();	每十分钟运行一次任务
    // ->everyThirtyMinutes();	每三十分钟运行一次任务
    // ->hourly();	每小时运行一次任务
    // ->daily();	每天凌晨零点运行任务
    // ->dailyAt('13:00');	每天13:00运行任务
    // ->twiceDaily(1, 13);	每天1:00 & 13:00运行任务
    // ->weekly();	每周运行一次任务
    // ->monthly();
    protected function schedule(Schedule $schedule){
        $schedule->command('command:ling_xing productExpression 1')->dailyAt('19:00');//每天19点 领星 产品表现
        $schedule->command('command:ling_xing productExpression 2')->dailyAt('07:30');//每天7:30 领星 产品表现
        $schedule->command('command:ling_xing adCampaignReports 3')->dailyAt('16:30');//每天16:30 领星 广告活动报表
        $schedule->command('command:ling_xing adCampaignReports 3')->dailyAt('17:30');//每天17:30 领星 广告活动报表
        $schedule->command('command:ling_xing adCampaignReports 3')->dailyAt('22:30');//每天18:30 领星 广告活动报表
        $schedule->command('command:ling_xing get_storage_age')->dailyAt('03:00');//每天03:00 领星 库龄报告
        $schedule->command('command:ling_xing addLxSku')->dailyAt('04:00');//每天04:00 领星 同步新的库存sku
        $schedule->command('command:ling_xing get_spsku_report')->dailyAt('02:00');//每天02:00 领星 获取sp广告商品报告
        $schedule->command('command:ling_xing get_sdsku_report')->dailyAt('02:30');//每天02:30 领星 获取sd广告商品报告
        $schedule->command('command:ling_xing getOrder 1')->everyThirtyMinutes();//每30分钟 领星 拉取订单
        $schedule->command('command:ling_xing getOrder 2')->everyThirtyMinutes();//每30分钟 领星 拉取订单
        $schedule->command('command:ling_xing updateOrder')->everyThirtyMinutes();//每30分钟 领星 更新订单状态
        $schedule->command('command:fpx walmartShipping')->everyThirtyMinutes();//每30分钟 沃尔玛回填物流渠道号码
//        $schedule->command('command:ling_xing createSendedOrder')->hourly();//每小时 领星 生成领星发货单

        $schedule->call(function () {
            $job = new \App\Http\Controllers\Jobs\AutoDayreportJobController;
            $job::runJob(['type'=>1]);
        })->dailyAt('17:56');



        $schedule->call(function () {
            $CacheDataModel = new \App\Models\CacheDataModel();
            $CacheDataModel->SyncLingxinToAmazonStatus();
        })->hourly();

    }
	
    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');
		
        require base_path('routes/console.php');
    }
}
