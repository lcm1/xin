<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class WarehouseLocationStatDayModel extends BaseModel
{

    public function add()
    {
        $day = date('Y-m-d', strtotime('-1 days'));;
        $startDay = $day . ' 00:00:00';
        $endDay   = $day . ' 23:59:59';

        $location = DB::table('cloudhouse_warehouse_location')->where('level', 3)->pluck('id');

        $create = date('Y-m-d H:i:s');
        $insert = [];

        foreach ($location as $v) {
            $insert[$v] = ['location_id' => $v, 'in_num' => 0, 'out_num' => 0, 'inventory_num' => 0, 'difference_num' => 0, 'day' => $day, 'create_time' => $create];
        }

        $locationLog = DB::table('cloudhouse_location_log')
            ->select(['location_id', 'type', DB::raw('sum(num) as num')])
            ->whereIn('type', [1, 2, 3, 5])
            ->whereBetween('time', [$startDay, $endDay])
            ->groupBy(['location_id', 'type'])
            ->get();

        foreach ($locationLog as $v) {
            //防止库位被删除，不存在
            if (!isset($insert[$v->location_id])) {
                continue;
            }

            if ($v->type == 2) {
                $insert[$v->location_id]['out_num'] = $v->num;
            } else {
                $insert[$v->location_id]['in_num'] += $v->num;
            }
        }

        $locationInventory = DB::table('cloudhouse_location_num')
            ->select(['location_id', DB::raw('sum(num) as num')])
            ->groupBy(['location_id'])
            ->get()
            ->pluck('num', 'location_id');

        foreach ($insert as $k => $v) {
            $insert[$k]['difference_num'] = $v['in_num'] - $v['out_num'];
            if (isset($locationInventory[$v['location_id']])) {
                $insert[$k]['inventory_num'] = $locationInventory[$v['location_id']];
            }
        }

        //分片插入数据
        $insert = array_chunk($insert, 200);
        foreach ($insert as $v) {
            DB::table('warehouse_location_stat_day')->insert($v);
        }

        return true;
    }

    public function GetDayListM($params)
    {
        $postType = $params['posttype'] ?? 1;
        $pagenum  = isset($params['page']) ? $params['page'] : 1;
        $limit    = isset($params['limit']) ? $params['limit'] : 30;

        $page = $pagenum - 1;
        if ($page != 0) {
            $page = $page * $limit;
        }

        $query = DB::table('warehouse_location_stat_day as a')
            ->leftJoin('cloudhouse_warehouse_location as b', 'a.location_id', '=', 'b.id')
            ->leftJoin('cloudhouse_warehouse as c', 'b.warehouse_id', '=', 'c.id')
            ->select(['a.*', 'b.location_name', 'b.location_code', 'c.warehouse_name']);

        if (isset($params['start_time']) && isset($params['end_time'])) {
            $startTime = $params['start_time'];
            $endTime   = $params['end_time'];
            $query->whereBetween('a.day', [$startTime, $endTime]);
        }

        if (isset($params['location_id'])) {
            $query->where('a.location_id', $params['location_id']);
        }

        $totalNum = 0;
        if ($postType == 1) {
            $totalNum = $query->count();
            $query->offset($page)->limit($limit);
        }

        $list = $query->orderBy('a.id', 'desc')->get();


        if ($postType == 2) {
            $ps['title']      = '库位上下架报告' . time();
            $ps['title_list'] = [
                'warehouse_name' => '仓库名',
                'location_name'  => '库位名称',
                'location_code'  => '库位编码',
                'in_num'         => '上架数量',
                'out_num'        => '下架数量',
                'difference_num' => '上下架差额',
                'inventory_num'  => '结余库存',
                'day'            => '日期',
            ];

            $ps['data']    = $list;
            $ps['user_id'] = $params['find_user_id'] ?? 1;
            $ps['type']    = '库位上下架报告-导出';


            $job = new \App\Http\Controllers\Jobs\ExcelTaskController;
            try {
                $job::runJob($ps);
            } catch (\Exception $e) {
                return $this->FailBack('加入队列失败');
            }
            return $this->SBack([], '加入队列成功');
        }

        $data['total_num'] = $totalNum;
        $data['data']      = $list;
        return $this->SBack($data);
    }


    //成功返回
    public function SBack($data = [], $msg = '成功')
    {
        return ['type' => 'success', 'data' => $data, 'msg' => $msg];
    }

    //失败返回
    public function FailBack($msg)
    {
        return ['type' => 'fail', 'data' => [], 'msg' => $msg];
    }

}