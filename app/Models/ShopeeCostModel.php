<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopeeCostModel extends Model
{
    //
    protected $table = 'shopee_cost';
    protected $guarded = [];
    public $timestamps = false;
}
