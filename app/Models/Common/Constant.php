<?php

namespace App\Models\Common;

/**
 * 常量配置文件
 */
class Constant
{
    // 合同类型
    const CONTRACT_TYPE = [
        1 => '委托加工',
        2 => '成品采购'
    ];

    // 合同分类
    const CONTRACT_CLASS = [
        1 => '普通合同',
        2 => '快返合同',
        3 => '采购合同',
        4 => '虚拟合同',
        5 => '外发加工合同',
    ];

    // 合同状态
    const CONTRACT_STATUS = [
        -2 => '审核被拒',
        -1 => '已撤销',
        0 => '草拟',
        1 => '待审批',
        2 => '已审批',
        3 => '撤销审批中'
    ];

    // 补货任务发货仓库
    const BUHUO_TASK_SEND_WAREHOUSE_TYPE = [
        2 => '云仓',
        3 => '同安仓',
        4 => '泉州仓'
    ];

    // 站内消息对象类型
    const TIDING_OBJECT_TYPE = [
        1 => '任务',
        5 => 'fasin留言',
    ];

    // fasin留言类型
    const FASIN_CONTENT_TYPE = [
        1 => '运营',
        2 => '产品开发',
        3 => '供应链',
        4 => '美工视觉',
        5 => '策略',
        6 => '链接指导'
    ];

    //

    // 查看历史任务类型
    const HISTORY_TASK_TYPE = [
        1 => '补货请求任务',
        2 => '下单合同审核任务'
    ];

    // 任务评论类型
    const TASKS_NEWS_MSG_TYPE_NAME = [
        1 => '正常评论',
        2 => '产品点评',
        3 => '供应链点评',
    ];

    // 库存上下架日志类型
    const WAREHOUSE_LOCATION_LOG_TYPE_NAME = [
        1 => '上架',
        2 => '下架',
        3 => '调整上架',
        4 => '库位库存移动',
        5 => '验货上架',
        6 => '验货下架'
    ];

    // 票据类型
    const BILL_TYPE = [
        1 => '预付账款',
        2 => '应付账款',
        3 => '附加费用账款',
        4 => '延期扣款'
    ];

    // 票据状态
    const BILL_STATUS = [
        -3 => '已废弃',
        -2 => '审核被拒',
        -1 => '已撤销',
        0 => '待提交',
        1 => '待审核',
        2 => '审核通过',
        3 => '已完结',
    ];

    // 票据是否支付
    const BILL_IS_PAYMENT = [
        0 => "未支付",
        1 => "已支付"
    ];

    // 1688订单状态
    const ALIBABA_ORDER_STATUS = [
        'waitbuyerpay' => '等待买家支付',
        'waitsellersend' => '等待卖家发货',
        'waitbuyerreceive' => '等待买家收货',
        'confirm_goods' => '已收货',
        'success' => '交易成功',
        'cancel' => '交易取消',
        'terminated' => '交易终止',
    ];

    // Lazada订单状态
    const LAZADA_ORDER_STATUS = [
        'confirmed' => '已确认',
        'canceled' => '已取消',
        'delivered' => '已交付',
        'shipped_back_success' => '',
        'returned' => '已退回',
        'shipped_back' => '发货退回',
        'shipped' => '已发货',
        'lost_by_3pl' => '',
        'pending' => '悬而未决的',
        'package_scrapped' => '',
        'ready_to_ship' => '',
        'unpaid' => '',
        'packed' => '',
    ];

    // 报价状态
    const QUOTATION_STATUS = [
        0 => '待报价',
        1 => '报价中',
        2 => '报价完成',
    ];

    // 合同分类编码生成
    const CONTRACT_CLASS_CREATE_KEY = [
        1 => 'AMAZON_PLACE_ORDER_CONTRACT_CODE_PUBLIC',
        2 => 'AMAZON_PLACE_ORDER_CONTRACT_CODE_QUICK_RETURN',
        3 => 'AMAZON_PLACE_ORDER_CONTRACT_PURCHASE',
        4 => 'AMAZON_PLACE_ORDER_CONTRACT_PURCHASE',
        5 => 'AMAZON_PLACE_ORDER_CONTRACT_OUTWARD_PROCESS',
    ];

    // 打样状态
    const PROOFING_STATUS = [
        0 => '待打样',
        1 => '打样中',
        2 => '打样完成',
    ];

    // spu产品状态
    //产品状态0 开发中  1待报价、2.报价中3报价完成，4待打样,5打样中、6打样完成、7已下单、8未发布、9已发布 、10缺货、11促销**% 、12清仓 、13下架（跟供应链）
    const SPU_STATUS = [
        0 => '开发中',
        1 => '待报价',
        2 => '报价中',
        3 => '报价完成',
        4 => '待打样',
        5 => '打样中',
        6 => '打样完成',
        7 => '已下单',
        8 => '未发布',
        9 => '已发布',
        10 => '缺货',
        11 => '促销**%',
        12 => '清仓',
        13 => '下架（跟供应链）'
    ];

    // 合同周期结算方式 预设
    const CONTRACT_PERIODIC_SETTLEMENT_METHOD = [
        1 => "合同签订后，预付10%定金，供应商面辅料到库后（以原材料送货单为依据），再次预付10%定金，剩余80%货款，货出完后结算。    （月结周期：上月1日到31日。本月25日前收到发票及盖章确认后的对账单，我司月底安排付款，遇节假日顺延。）",
        2 => "合同签订后，预付30%定金，剩余70%货款，货出完后结算。    （月结周期：上月1日到31日。本月25日前收到发票及盖章确认后的对账单，我司月底安排付款，遇节假日顺延。）",
        3 => "100%货款货出完后结算。    （月结周期：上月1日到31日。本月25日前收到发票及盖章确认后的对账单，我司月底安排付款，遇节假日顺延。）",
        4 => "合同签订后，预付20%定金，出货后60天内付70%货款，余下10%开具增值税发票收到后一周内付清。",
        5 => "货出完后60天结算100%货款，收到增值税专用发票后一周内付清。",
        6 => "10%预付款，材料到仓20%预付款，出货后月结70%货款带票付。  （月结周期：上月1日到31日。本月25日前收到发票及盖章确认后的对账单，我司月底安排付款，遇节假日顺延。）",
        7 => "出货前100%付款。",
        8 => "发货前支付80%货款，余下20%到仓15天质检没问题结清尾款",
        9 => "发货前支付80%货款，余下20%到仓45天质检没问题结清尾款",
        10 => "合同签订后，预付20%定金，出货后90天收到增值税发票付清80%余款。",
        11 => "合同签订后，预付30%定金，到仓收到增值税发票15天内付清尾款。",
        12 => "成品入仓检验合格后，7天后收到发票及盖章确认后的对账单我司安排付款，遇节假日顺延。每延迟一天交期（延期8天内），货款延迟3天，延期超过8天按照合同规定的延期扣款条款执行。",
        13 => "出货前预付50%货款，余下50%货款收到发票一个月内结清。",
    ];
    // 合同款项信息 预设
    const CONTRACT_PAYMENY_INFORMATION = [
        1 => [
            ['bill_type' => 1, 'title' => "预付10%定金", 'percentage' => 10],
            ['bill_type' => 1, 'title' => "面辅料到库后预付10%定金", 'percentage' => 10],
            ['bill_type' => 2, 'title' => "货出完后结算80%货款", 'percentage' => 80],
        ],
        2 => [
            ['bill_type' => 1, 'title' => "预付30%定金", 'percentage' => 30],
            ['bill_type' => 2, 'title' => "货出完后结算70%货款", 'percentage' => 70],
        ],
        3 => [
            ['bill_type' => 2, 'title' => "货出完后结算100%货款", 'percentage' => 100],
        ],
        4 => [
            ['bill_type' => 1, 'title' => "预付20%定金", 'percentage' => 20],
            ['bill_type' => 2, 'title' => "出货后60天内付70%货款", 'percentage' => 70],
            ['bill_type' => 2, 'title' => "余下10%开具增值税发票收到后一周内付清", 'percentage' => 10],
        ],
        5 => [
            ['bill_type' => 2, 'title' => "货出完后60天结算100%货款，收到增值税专用发票后一周内付清", 'percentage' => 100],
        ],
        6 => [
            ['bill_type' => 1, 'title' => "预付10%定金", 'percentage' => 10],
            ['bill_type' => 1, 'title' => "材料到仓20%预付款", 'percentage' => 20],
            ['bill_type' => 2, 'title' => "出货后月结70%货款带票付", 'percentage' => 70],
        ],
        7 => [
            ['bill_type' => 1, 'title' => "出货前100%付款", 'percentage' => 100],
        ],
        8 => [
            ['bill_type' => 1, 'title' => "发货前支付80%货款", 'percentage' => 80],
            ['bill_type' => 2, 'title' => "20%到仓15天质检通过结清尾款", 'percentage' => 20],
        ],
        9 => [
            ['bill_type' => 1, 'title' => "发货前支付80%货款", 'percentage' => 80],
            ['bill_type' => 2, 'title' => "20%到仓45天质检通过结清尾款", 'percentage' => 20],
        ],
        10 => [
            ['bill_type' => 1, 'title' => "预付20%定金", 'percentage' => 20],
            ['bill_type' => 2, 'title' => "出货后90天收到增值税发票付清80%余款", 'percentage' => 80],
        ],
        11 => [
            ['bill_type' => 1, 'title' => "预付30%定金", 'percentage' => 30],
            ['bill_type' => 2, 'title' => "到仓收到增值税发票15天内付清尾款", 'percentage' => 70],
        ],
        12 => [
            ['bill_type' => 2, 'title' => "成品入仓检验合格后，7天后收到发票及盖章确认后的对账单我司安排付款", 'percentage' => 100],
        ],
        13 => [
            ['bill_type' => 1, 'title' => "出货前预付50%货款", 'percentage' => 50],
            ['bill_type' => 2, 'title' => "余下50%货款收到发票一个月内结清", 'percentage' => 50],
        ],
    ];

    // goods_transfers表type_detail类型
    const GOODS_TRANSFERS_TYPE_DETAIL = [
        1 => '仓库调拨入库',
        2 => '采购入库',
        3 => '手工入库',
        4 => '样品退回入库',
        5 => '订单退货入库',
        6 => 'fba出库',
        7 => '仓库调拨出库',
        8 => '第三方仓出库',
        9 => '采购退货出库',
        10 => '样品调拨出库',
        11 => '手工出库',
        12 => '订单出库',
        13 => '工厂虚拟入库',
        14 => '工厂虚拟出库',
        15 => '验货申请',
        16 => '调货申请',
    ];
    // goods_transfers表is_push类型
    const GOODS_TRANSFERS_IS_PUSH = [
        -2 => '取消失败',
        -1 => '已取消',
        0 => '推送失败',
        1 => '待推送',
        2 => '已推送',
        3 => '已确认',
        4 => '操作中',
        5 => '已退货',
    ];

    const GOODS_TRANSFERS_HOUSE = [
        1 => '同安仓',
        2 => '泉州仓',
        3 => '云仓',
        4 => '工厂虚拟仓',
        5 => '其它',
        6 => '工厂寄存仓',
    ];

    const AMAZON_PLACE_ORDER_TASK_STATUS = [
        -1 => '已撤销',
        0 => '待发布',
        1 => '已发布待审核',
        2 => '已审核待开合同',
        3 => '全部合同待审核',
        4 => '全部合同审核通过',
        5 => '部分开合同',
    ];

    const TASKS_SON_STATUS = [
        1 => '未开始',
        2 => '进行中',
        3 => '已完成',
        4 => '已放弃',
    ];

    const BUHUO_REQUEST_STATUS = [
        1 => '已撤销',
        2 => '待发布任务',
        3 => '已发布，待审核',
        4 => '已审核，待备货',
        5 => '已打印，待装箱',
        6 => '已装箱，待创货件',
        7 => '已创货件，待出库',
        8 => '已确认出库，待发货',
        9 => '已生成物流单号',
        10 => '已完成'
    ];

    const SKU_STRATEGY_TYPE = [
        1 => '降价出售',
        2 => '合并链接',
        3 => '加大广告',
        4 => '清仓处理',
        5 => '移仓换标',
        6 => '清算和专享',
        7 => 'coupon',
        8 => '秒杀'
    ];
    const CONTRACT_PLAN_STATUS = [
        -1 => '审核拒绝',
        0 =>'待审核',
        1 => '审核通过'
    ];
}