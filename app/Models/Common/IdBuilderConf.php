<?php

namespace App\Models\Common;

/**
 * 通用ID生成类定义
 *
 * 调用方法:
 * e.g
 * 定义: "调用的KEY(大写)" => "FORMAT"(组成格式:前缀+[Ymd]+[5], 内容及位可自行调整)
 * 使用:
 */
class IdBuilderConf
{
    public static $conf = [
        // 合同编码(PO前缀+8位年月日+自增数)eg. PO2023042010
        "AMAZON_PLACE_ORDER_CONTRACT_CODE_PUBLIC" => "PO[ymd][2]",
        "AMAZON_PLACE_ORDER_CONTRACT_CODE_QUICK_RETURN" => "PS[ymd][2]",
        'AMAZON_PLACE_ORDER_CONTRACT_CODE_VIRTUAL' => "PV[ymd][2]",
        'AMAZON_PLACE_ORDER_CONTRACT_PURCHASE' => "PP[ymd][2]",
        "AMAZON_PLACE_ORDER_CONTRACT_OUTWARD_PROCESS" => "JG[ymd][2]",
        // 海西汇调查问卷类型
        "HXH_TABLE_TYPE" => "[0]",
        // 预付款单编码
        "PREPAYMENT_BILL_BN" => "DJ[Ymd][2]",
        // 应付账款单编码
        "ACCOUNTS_PAYMENT_BILL_BN" => "YF[Ymd][2]",
        // 附加费用单编码
        "SURCHARGE_BILL_BN" => "FJF[Ymd][2]",
        // 核销记录编码
        "WRITE_OFF_BN" => "HX[Ymd][2]",
        // 延期扣款单编码
        "DELAYED_BILL_BN" => "YQ[Ymd][2]",
    ];

    /**
     * @Desc:获取编码规则
     * @return string[]
     * @author: Liu Sinian
     * @Time: 2023/6/25 9:09
     */
    public static function getConf()
    {
        return self::$conf;
    }

}
