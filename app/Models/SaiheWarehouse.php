<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SaiheWarehouse extends Model
{
    //
    protected $table = 'saihe_warehouse';
    protected $guarded = [];
    public $timestamps = false;

}
