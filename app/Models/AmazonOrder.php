<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AmazonOrder extends Model
{
    //
    protected $table = 'amazon_order';
    protected $guarded = [];
    public $timestamps = false;
}
