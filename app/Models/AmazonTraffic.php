<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AmazonTraffic extends Model
{
    //
    protected $table = 'amazon_traffic';
    protected $guarded = [];
    public $timestamps = false;
}
