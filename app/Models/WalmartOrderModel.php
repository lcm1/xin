<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class WalmartOrderModel extends BaseModel
{
    public function GetOrderListM($params)
    {
        $pagenum = isset($params['page']) ? $params['page'] : 1;
        $limit   = isset($params['limit']) ? $params['limit'] : 30;

        $page = $pagenum - 1;
        if ($page != 0) {
            $page = $page * $limit;
        }

        $posttype = $params['posttype']??1;
        $query = db::table('walmart_order_item as a')
            ->select([
                'id',
                'walmart_order_id as local_order_id',
                'shop_id',
                'purchase_order_id',
                'name',
                'sku',
                'order_line_quantity_amount',
                'order_line_status',
                'charge_amount',
                'tax_amount',
                'order_date',
                'user_id',
                'custom_sku_id',
                'spu_id',
                'sku_id',
                'spu',
                'add_time',
                'update_time',
            ]);

        $type = $params['type'] ?? 1;//1:总订单表 2:待发货表

        if ($type == 2) {
            $notOrderIds = db::table('fpx_order')->where('type', 2)->where('status', '>=', 0)->pluck('order_id');
            $query->whereNotIn('purchase_order_id', $notOrderIds);
        }

        if (isset($params['start_time']) && isset($params['end_time'])) {
            $start_time = $params['start_time'];
            $end_time   = $params['end_time'];
            $query->whereBetween('a.order_date', [$start_time, $end_time]);
        }

        if (isset($params['spu'])) {
            $spuIds = db::table('self_spu')->where('spu', $params['spu'])->orwhere('old_spu', $params['spu'])->pluck('id');
            if ($spuIds->isEmpty()) {
                return $this->SBack(['data' => [], 'total_num' => 0]);
            }

            $query->whereIn('a.spu_id', $spuIds);
        }

        if (isset($params['custom_sku'])) {
            $customSkuIds = db::table('self_custom_sku')
                ->where('custom_sku', $params['custom_sku'])
                ->orwhere('old_custom_sku', $params['custom_sku'])
                ->pluck('id');
            if ($customSkuIds->isEmpty()) {
                return $this->SBack(['data' => [], 'total_num' => 0]);
            }

            $query->whereIn('a.custom_sku_id', $customSkuIds);
        }

        if (isset($params['sku'])) {
            $skuIds = db::table('self_sku')->where('sku', $params['sku'])->orwhere('old_sku', $params['sku'])->pluck('id');
            if ($skuIds->isEmpty()) {
                return $this->SBack(['data' => [], 'total_num' => 0]);
            }

            $query->whereIn('a.sku_id', $skuIds);
        }

        if (isset($params['shop_id'])) {
            $query->where('a.shop_id', $params['shop_id']);
        }

        if (isset($params['user_id'])) {
            $query->where('a.user_id', $params['user_id']);
        }

        if (isset($params['purchase_order_ids'])) {

            if (strstr($params['purchase_order_ids'], '，')) {
                $symbol = '，';
            } elseif (strstr($params['purchase_order_ids'], ',')) {
                $symbol = ',';
            } else {
                $symbol = '';
            }
            if ($symbol) {
                $ids = explode($symbol, $params['purchase_order_ids']);
            } else {
                $ids[0] = $params['purchase_order_ids'];
            }

            $query->whereIn('a.purchase_order_id', $ids);

        }

        if (isset($params['purchase_order_id'])) {
            $query->where('a.purchase_order_id', 'like', '%' . $params['purchase_order_id'] . '%');
        }

        $totalNum = $query->count();
        if ($totalNum == 0) {
            return $this->SBack(['data' => [], 'total_num' => 0]);
        }

        if($posttype==1){
            $list            = $query->offset($page)->limit($limit)->orderBy('a.order_date', 'desc')->get();
        }else{
            $list            = $query->orderBy('a.order_date', 'desc')->get();
        }

        $walmartOrderIds = $list->pluck('local_order_id');

        $address = db::table('walmart_order')
            ->select([
                'id',
                'phone',
                'postal_address_name',
                'address1',
                'address2',
                'city',
                'postal_code',
//                'country',
                DB::raw('left(country, 2) country'),
                'postal_address_state'
            ])
            ->whereIn('id', $walmartOrderIds)
            ->get()
            ->keyBy('id');

        foreach ($address as  $v) {
            if ($v->address2) {
                $v->address1 .= ' , ' . $v->address2;
            }
        }

        $customSkuList = db::table('self_custom_sku')->whereIn('id', $list->pluck('custom_sku_id'))->get()->keyBy('id');
        //匹配库存sku
        foreach ($list as $v) {
            if ($v->custom_sku_id && $v->sku_id && $v->spu_id && $v->spu){
                $customSkuInfo = $customSkuList[$v->custom_sku_id] ?? null;
                $v->custom_sku = $customSkuInfo ? ($customSkuInfo->old_custom_sku ?: $customSkuInfo->custom_sku) : '';
                $v->title      = $customSkuInfo ? $customSkuInfo->name : '';
                continue;
            }

            $skuInfo = db::table("self_sku")->where('sku', $v->sku)->orwhere('old_sku', $v->sku)->first();
            $v->is_spu = true;
            if (!$skuInfo) {
                $v->is_spu = false;
                $v->spu    = 'sku暂未录入系统，无法发货';
                continue;
            }


            $customSkuInfo = db::table('self_custom_sku')->where('id', $skuInfo->custom_sku_id)->first();
            if (!$customSkuInfo) continue;

            db::table('walmart_order_item')->where('id', $v->id)->update([
                'spu_id'        => $customSkuInfo->spu_id,
                'spu'           => $customSkuInfo->spu,
                'custom_sku_id' => $customSkuInfo->id,
                'sku_id'        => $skuInfo->id,
            ]);

            $v->spu_id        = $customSkuInfo->spu_id;
            $v->spu           = $customSkuInfo->spu;
            $v->custom_sku_id = $customSkuInfo->id;
            $v->sku_id        = $skuInfo->id;

            $v->custom_sku = $customSkuInfo->old_custom_sku ?: $customSkuInfo->custom_sku;
            $v->title      = $customSkuInfo->name;
        }

        //查询库存
        $customSkuIds = array_unique($list->pluck('custom_sku_id')->toArray());
        $inventoryArr = $this->getInventoryDetailByWlp($customSkuIds, [1, 2], [], [4, 14]);

        //查询云仓库存
        $cloudInventoryArr = db::table('self_custom_sku')->whereIn('id', $customSkuIds)->pluck('cloud_num', 'id');
        //查询云仓库存锁定情况
        $cloudInventoryLockArr = $this->batchInventoryDetailByWlp($customSkuIds, [3]);

        $spuList = db::table('self_spu')
            ->select('id', 'one_cate_id', 'three_cate_id')
            ->whereIn('id', $list->pluck('spu_id'))
            ->get()
            ->keyBy('id');
        foreach ($list as $v) {
            $v->category = '';
            $spuInfo     = $spuList[$v->spu_id] ?? null;
            if ($spuInfo) {
                $v->category = $this->GetCategory($spuInfo->one_cate_id)['name'] . $this->GetCategory($spuInfo->three_cate_id)['name'];
            }

            $v->shop_name = $this->GetShop($v->shop_id)['shop_name'] ?? '';
            $v->user      = $v->user_id ? ($this->GetUsers($v->user_id)['account'] ?? '') : '';
            $v->address   = $address[$v->local_order_id];


            //同安沃尔玛可用库存
            $tongAnWalmartNum             = $inventoryArr[$v->custom_sku_id . '_1_14'] ?? ['usable_num' => 0];
            $v->tongan_deduct_request_num = $tongAnWalmartNum['usable_num'];

            //泉州沃尔玛可用库存
            $quanzhouWalmartNum             = $inventoryArr[$v->custom_sku_id . '_2_14'] ?? ['usable_num' => 0];
            $v->quanzhou_deduct_request_num = $quanzhouWalmartNum['usable_num'];

            //同安可调库存
            $tongAnTunableNum      = $inventoryArr[$v->custom_sku_id . '_1_4'] ?? ['usable_num' => 0];
            $v->tongan_tunable_num = $tongAnTunableNum['usable_num'];
            //泉州沃尔玛可用库存
            $quanzhouTunableNum      = $inventoryArr[$v->custom_sku_id . '_2_4'] ?? ['usable_num' => 0];
            $v->quanzhou_tunable_num = $quanzhouTunableNum['usable_num'];

            //云仓库存
            $v->cloud_deduct_request_num = $cloudInventoryArr[$v->custom_sku_id] ?? 0;
            if ($v->cloud_deduct_request_num > 0 && isset($cloudInventoryLockArr[$v->custom_sku_id . '_3'])) {
                $cloudInventoryLockNum       = $cloudInventoryLockArr[$v->custom_sku_id . '_3'];
                $v->cloud_deduct_request_num -= ($cloudInventoryLockNum['lock_num'] + $cloudInventoryLockNum['shelf_num']);
            }
        }



        if($posttype==2){
            $p['title']='自发货沃尔玛总订单'.time();
            $p['title_list']  = [
                'purchase_order_id'=>'订单号',
                'title'=>'品名',
                'category'=>'类目',
                'user'=>'运营',
                'shop_name'=>'店铺',
                'spu'=>'spu',
                'custom_sku'=>'库存sku',
                'sku'=>'sku',
                'order_line_status'=>'订单状态',
                'charge_amount'=>'订单金额',
                'tongan_deduct_request_num'=>'同安可用',
                'quanzhou_deduct_request_num'=>'泉州可用',
                'cloud_deduct_request_num'=>'云仓可用',
                'order_date'=>'创建时间',
            ];

            // $this->excel_expord($p);
            
            $p['data'] =   $list;
            $p['user_id'] =$params['find_user_id']??1;
            $p['type'] = '自发货沃尔玛总订单-导出';
            $job = new \App\Http\Controllers\Jobs\ExcelTaskController;
            try {
                $job::runJob($p);
            } catch(\Exception $e){
                return $this->FailBack('加入队列失败');
            }
            return $this->SBack([],'加入队列成功');
        }

        $data['total_num'] = $totalNum;
        $data['data']      = $list;
        return $this->SBack($data);
    }

    //成功返回
    public function SBack($data)
    {
        return ['type' => 'success', 'data' => $data, 'msg' => '成功'];
    }

    //失败返回
    public function FailBack($msg)
    {
        return ['type' => 'fail', 'data' => [], 'msg' => $msg];
    }


}