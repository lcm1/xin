<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopeeStock extends Model
{
    //
    protected $table = 'shopee_stock';
    protected $guarded = [];
    public $timestamps = false;

    public function shop() {
        return $this->hasOne('shop','Id','shop_id');
    }
}
