<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopUsersModel extends Model
{
    //
    protected $table = 'shop_users';

    public $guarded = [];
}
