<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProductSkuModel extends BaseModel
{
    //
    public function getWishChildSkuList($params){
        $page = isset($params['page']) ? $params['page'] : 1;
        $limit = isset($params['limit']) ? $params['limit'] : 15;
        $page = $this->getLimitParam($page,$limit);
        $productId = $params['product_id'];
        $sql = "SELECT SQL_CALC_FOUND_ROWS
                    *
                FROM
                    product_sku
                WHERE
                    product_id = $productId    
                    LIMIT $page,
                    $limit";
        $data = DB::select($sql);
        $totalNum = db::select("SELECT FOUND_ROWS() as total")[0]->total;
        foreach ($data as $k => $v) {
            $data[$k]->price = $v->price / 100;
            $data[$k]->shipping_fee = $v->shipping_fee / 100;
        }
        return [
            'data' => $data,
            'totalNum' => $totalNum
        ];
    }
}
