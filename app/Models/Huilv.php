<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Huilv extends Model
{
    //
    protected $table = 'huilv';
    protected $guarded = [];
    public $timestamps = false;
}
