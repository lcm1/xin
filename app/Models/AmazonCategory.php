<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AmazonCategory extends Model
{
    //
    protected $table = 'amazon_category';
    protected $guarded = [];
    public $timestamps = false;
}
