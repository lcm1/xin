<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AmazonOrderRepeat extends Model
{
    protected $table = 'amazon_order_repeat';
    protected $guarded = [];
    public $timestamps = false;
}
