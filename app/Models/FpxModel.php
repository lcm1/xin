<?php

namespace App\Models;

use App\Jobs\FpxJob;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use App\Http\Controllers\Jobs\ExcelTaskController;
use App\Models\InventoryModel;

//4px物流
class FpxModel extends BaseModel
{

    protected $key    = '2e3e9de9-e10d-45dc-92a9-1b73a9c85c2c';
    protected $secret = '8aeab90e-04fb-4d41-ab5a-eb37dad50aa1';

    protected $area = [
        'MY' => '马来西亚',
        'PH' => '菲律宾',
        'VN' => '越南',
        'TH' => '泰国',
        'ID' => '印度尼西亚',
        'SG' => '新加坡'
    ];

    public function GetFpxErrM($params)
    {

        $res['request_no'] = $params['request_no'];

        $data['method'] = 'ds.xms.order_abnormal.getlist';
        $data['vesion'] = '1.0.0';
        $data['data']   = $res;

        $url = $this->getFpx($data);

        return $this->SBack($url);
    }


    public function GetFpxOrderM($params)
    {

        $res['request_no'] = $params['request_no'];

        $data['method'] = 'ds.xms.order.get';
        $data['vesion'] = '1.1.0';
        $data['data']   = $res;

        $url = $this->getFpx($data);

        return $this->SBack($url);
    }

    //取消订单
    public function CancelFpxOrderM($params)
    {

        $res['request_no']    = $params['request_no'];
        $res['cancel_reason'] = 'test';

        $data['method']  = 'ds.xms.order.cancel';
        $data['vesion']  = '1.0.0';
        $data['data']    = $res;
        $orders          = db::table('fpx_order')->where('request_no', $res['request_no'])->first();
        $cloudhousemodel = new \App\Models\CloudHouse();
        $rmg             = $cloudhousemodel->InhouseCancel(['order_no' => $orders->out_no, 'is_zfh' => 1]);
        if ($rmg['type'] == 'fail' && $rmg['msg'] != '已取消成功，请勿重复推送') {
            return $this->FailBack('系统：' . $rmg['msg']);
        }


        $del['user_id']     = $params['user_id'];
        $del['order_id']    = $orders->order_id;
        $del['type']        = $orders->type;
        $del['create_time'] = date('Y-m-d H:i:s', time());

        db::table('fpx_del_order')->insert($del);


        db::table('fpx_order')->where('request_no', $res['request_no'])->update(['status' => -1]);


        $url   = $this->getFpx($data);
        $rtmsg = json_decode($url, true);
        // var_dump($rtmsg);
        if ($rtmsg['msg'] == 'System processing succeeded') {
            $url = 'fpx' . $rtmsg['result'];
        } else {
            return $this->FailBack('fpx' . $rtmsg['errors'][0]['error_msg']);
        }


        return $this->SBack($url);
    }


    //作废订单
    public function CancelOrderM($params)
    {
        $type = $params['type'] ?? 1;
        foreach ($params['amazon_order_id'] as $amazon_order_id) {
            # code...
            $insert['amazon_order_id'] = $amazon_order_id;
            $insert['user_id']         = $params['user_id'];
            $insert['create_time']     = date('Y-m-d H:i:s', time());
            $insert['type']            = $type;
            db::table('amazon_order_cancel')->insert($insert);
        }

        return $this->SBack([]);
    }


    //取消作废订单
    public function DeleteCancelOrderM($params)
    {
        $amazon_order_id = $params['amazon_order_id'];
        $type            = $params['type'] ?? 1;
        db::table('amazon_order_cancel')->where('amazon_order_id', $amazon_order_id)->where('type', $type)->delete();
        return $this->SBack([]);
    }

    //作废订单列表
    public function CancelOrderListM($params)
    {

        $pagenum = isset($params['page']) ? $params['page'] : 1;
        $limit   = isset($params['limit']) ? $params['limit'] : 30;

        $page = $pagenum - 1;
        if ($page != 0) {
            $page = $page * $limit;
        }

        $list = db::table('amazon_order_cancel');

        if (isset($params['amazon_order_id'])) {
            $list = $list->where('amazon_order_id', $params['amazon_order_id']);
        }

        $totalNum          = $list->groupby('amazon_order_id')->get()->count();
        $list              = $list->offset($page)->limit($limit)->get();
        $data['total_num'] = $totalNum;
        $data['data']      = $list;
        return $this->SBack($data);
    }

    //获取订单面单
    public function GetFpxOrderLabelM($params)
    {

        $data         = [];
        $is_inventory = $params['is_inventory'];
        if (is_array($params['request_no'])) {
            foreach ($params['request_no'] as $v) {
                # code...
                $data[] = $this->GetFpxOrderLabelOneM($v, $is_inventory);
            }
        } else {
            $data[] = $this->GetFpxOrderLabelOneM($params['request_no'], $is_inventory);
        }

        return $this->SBack($data);
    }


    public function GetFpxOrderLabelOneM($params, $is_inventory)
    {
        $res['request_no'][]              = $params;
        $res['logistics_product_code']    = '';
        $res['label_size']                = 'label_80x90';
        $res['is_print_pick_info']        = 'N';
        $res['is_print_time']             = 'N';
        $res['is_print_buyer_id']         = 'N';
        $res['is_print_declaration_list'] = 'N';
        $res['is_print_customer_weight']  = 'N';
        $res['create_package_label']      = 'N';

        $data['method'] = 'ds.xms.label.getlist';
        $data['vesion'] = '1.0.0';
        $data['data']   = $res;

        $urls = $this->getFpx($data);

        $rtmsg = json_decode($urls, true);
        if ($rtmsg['msg'] == 'System processing succeeded') {
            $url = $rtmsg['data'];
        } else {
            return $this->FailBack($rtmsg['errors'][0]['error_msg']);
        }
        // $custom_skus  =  $params['custom_sku'];


        $cus = db::table('fpx_order_detail as a')->leftjoin('self_custom_sku as b', 'a.custom_sku_id', '=', 'b.id')->where('a.request_no', $res['request_no'])->select('a.num', 'b.id as custom_sku_id', 'b.type as custom_sku_type', 'b.custom_sku', 'b.old_custom_sku', 'b.name', 'b.color', 'b.size')->get();


        $fpx_order = db::table('fpx_order')->where('request_no', $res['request_no'])->first();
        if (!$fpx_order) {
            return $this->FailBack('无此订单');
        }

        $orders = db::table('goods_transfers_box')->where('order_no', $fpx_order->out_no)->first();
        if (!$orders) {
            return $this->FailBack('无此装箱');
        }
        $box_id = $orders->id;


        $newcus = [];
        $zuhe   = [];
        $i      = 0;
        foreach ($cus as $v) {
            # code...
            $ondata               = [];
            $ondata['name']       = $v->name ?? '';
            $ondata['custom_sku'] = $v->custom_sku;
            if (!empty($v->old_custom_sku)) {
                $ondata['custom_sku'] = $v->old_custom_sku;
            }
            $ondata['num']   = $v->num;
            $ondata['color'] = $v->color;
            $ondata['size']  = $v->size;

            // if($v->custom_sku_type==2){
            //     $zuhe_cus = $this->GetGroupCustomSku($v->custom_sku);
            //     $newcus[$i]['location_code'] = '';
            //     $newcus[$i]['no'] = '';
            //     $newcus[$i]['box_num'] = '';
            //     foreach ($zuhe_cus as $cv) {
            //         $group_msg = '';
            //         $custom_sku_id = $cv['group_custom_sku_id'];
            //         $location_code = db::table('goods_transfers_box_detail')->where('custom_sku_id',$custom_sku_id)->where('box_id',$box_id)->first();
            //         if(!$location_code){
            //             return $this->FailBack('库位中无此订单1cus_id:'.$custom_sku_id.'box_id'.$box_id);
            //         }

            //         $newcus[$i]['box_code'] = db::table('goods_transfers_box')->where('id',$box_id)->first()->box_code??'';
            //         $location_res = db::table('cloudhouse_warehouse_location')->where('id',$location_code->location_ids)->first();

            //         // $newcus[$i]['location_code'] = $location_res->location_code;

            //         $cuscout = db::table('custom_sku_count')->where('location_code',$location_res->location_code)->where('custom_sku_id',$custom_sku_id)->get()->toarray();
            //         $no = array_column($cuscout,'no');
            //         $v->no = implode(',',$no);
            //         // $newcus[$i]['no'] = $v->no;

            //         // $newcus[$i]['box_num'] =$location_code->box_num;
            //         $group_msg =  $location_res->location_code.'*'.$v->no.'*'.$cv['group_custom_sku'].'*'.$location_code->box_num;
            //         $zuhe[] = $group_msg;
            //         # code...
            //     }
            //     $i++;

            // }else{

            $location_codes = db::table('goods_transfers_box_detail')->where('custom_sku_id', $v->custom_sku_id)->where('box_id', $box_id)->get();
            if (!isset($location_codes[0])) {
                return $this->FailBack('库位中无此订单2cus_id:' . $v->custom_sku_id . 'box_id' . $box_id .'-出库单号'.$fpx_order->out_no. '检查boxid是否正确');
            }


            foreach ($location_codes as $location_code) {
                # code...
                $ondata['box_code'] = db::table('goods_transfers_box')->where('id', $box_id)->first()->box_code ?? '';
                $location_res       = db::table('cloudhouse_warehouse_location')->where('id', $location_code->location_ids)->first();

                $ondata['location_code'] = $location_res->location_code;

                $cuscout           = db::table('custom_sku_count')->where('location_code', $location_res->location_code)->where('custom_sku_id', $v->custom_sku_id)->get()->toarray();
                $no                = array_column($cuscout, 'no');
                $v->no             = implode(',', $no);
                $ondata['no']      = $v->no;
                $ondata['box_num'] = $location_code->box_num;
                $newcus[$i]        = $ondata;
                $i++;
            }

            // }

        }

        $return['url']     = $url;
        $return['cus']     = $newcus;
        $return['is_open'] = $fpx_order->is_open;
        $return['zuhe']    = $zuhe;
        if ($is_inventory == 1) {
            $fpx_order = db::table('fpx_order')->where('request_no', $res['request_no'])->update(['is_open' => 1]);
        }

        return $return;
    }

    //修改订单
    public function UpdateFpxOrderM($params)
    {


        $res['request_no'] = $params['request_no'];
        $res['weight']     = $params['weight'];

        $data['method'] = 'ds.xms.order.updateweight';
        $data['vesion'] = '1.0.0';
        $data['data']   = $res;

        $url = $this->getFpx($data);


        $user_id = $params['user_id'];

        $rtmsg = json_decode($url, true);
        if ($rtmsg['msg'] == 'System processing succeeded') {
            // $is_inventory = $params['is_inventory']??0;
            // if($is_inventory==1){
            //     db::table('fpx_order')->where('request_no',$res['request_no'])->update(['weight'=>$res['weight'],'status'=>1]);
            //     $log['request_no'] = $res['request_no'];
            //     $log['weight'] = $res['weight'];
            //     $log['user_id'] = $user_id;
            //     $log['create_time'] = date('Y-m-d H:i:s',time());
            //     db::table('fpx_order_log')->insert($log);
            // }
            db::table('fpx_order')->where('request_no', $res['request_no'])->update(['weight' => $res['weight'], 'status' => 1, 'out_time' => date('Y-m-d H:i:s', time())]);
            $log['request_no']  = $res['request_no'];
            $log['weight']      = $res['weight'];
            $log['user_id']     = $user_id;
            $log['create_time'] = date('Y-m-d H:i:s', time());
            db::table('fpx_order_log')->insert($log);

            return $this->SBack($rtmsg);
        } else {
            return $this->FailBack('修改失败');
        }

    }




    // pc/inventory/goods_transfers   传参：out_house（出库仓库） token（用户token） skuData(数组类型，数组参数包括 custom_sku , num)  type( 2.订单出入库)  type_detail(12.订单出库 )

    //修改日志


    //拼接信息
    public function HaveFpxOrderM($params)
    {
        $type = $params['type'] ?? 1;

        if ($type == 1) {
            $order_id       = $params['order_id'];
            $orders         = db::table('amazon_order_item')->where('amazon_order_id', $order_id)->get()->toArray();
            if(empty($orders)){
                $this->FailBack('系统无此订单，请手动添加到优先采集订单界面重新采集');
            }
            $custom_sku_ids = array_column($orders, 'custom_sku_id');
            $cus_order      = [];
            $cus_sku        = [];
            //拼接订单信息
            foreach ($orders as $v) {
                # code...
                //商品数量
                $cus_order[$v->custom_sku_id] = $v->quantity_ordered;
                //商品skuid
                $cus_sku[$v->custom_sku_id] = $v->sku_id;
            }
        }


        if ($type == 2) {
            $order_id       = $params['order_id'];
            $orders         = db::table('walmart_order_item')->where('purchase_order_id', $order_id)->get()->toArray();
            $custom_sku_ids = array_column($orders, 'custom_sku_id');
            $cus_order      = [];
            $cus_sku        = [];
            //拼接订单信息
            foreach ($orders as $v) {
                # code...
                if (isset($cus_order[$v->custom_sku_id])) {
                    $cus_order[$v->custom_sku_id] += $v->order_line_quantity_amount;
                } else {
                    $cus_order[$v->custom_sku_id] = $v->order_line_quantity_amount;
                }
                $cus_sku[$v->custom_sku_id] = $v->sku_id;
            }
        }


        if ($type == 3) {
            $order_id       = $params['order_id'];
            $orders         = db::table('lazada_order_item')->where('order_id', $order_id)->get()->toArray();
            $custom_sku_ids = array_column($orders, 'custom_sku_id');
            $cus_order      = [];
            $cus_sku        = [];
            //拼接订单信息
            foreach ($orders as $v) {
                # code...
                if (isset($cus_order[$v->custom_sku_id])) {
                    $cus_order[$v->custom_sku_id] += 1;
                } else {
                    $cus_order[$v->custom_sku_id] = 1;
                }

                $cus_sku[$v->custom_sku_id] = $v->sku_id;
            }
        }


        $template_id = $params['template_id'];
        $templateres = db::table('fpx_template')->where('id', $template_id)->first();
        $template    = json_decode($templateres->template, true);
        // $templateb = json_decode($templateres->template,true);
        //买家信息
        $recipient_info             = $params['recipient_info'];
        $template['recipient_info'] = $recipient_info;
        //
        $template["4px_tracking_no"] = '';
        $template["ref_no"]          = 'Z' . time() . rand(100, 999) . 'T';
        $custom_skus                 = db::table('self_custom_sku')->whereIn('id', $custom_sku_ids)->get()->toArray();

        if(empty($custom_skus)){
            $this->FailBack('未获取到该sku的产品信息');
        }

        //模版信息
        //包裹寄出信息
        $weight          = $template['parcel_list'][0]['weight'];
        $length          = $template['parcel_list'][0]['length'];
        $width           = $template['parcel_list'][0]['width'];
        $height          = $template['parcel_list'][0]['height'];
        $parcel_value    = $template['parcel_list'][0]['parcel_value'];
        $currency        = $template['parcel_list'][0]['currency'];
        $include_battery = $template['parcel_list'][0]['include_battery'];
        $battery_type    = $template['parcel_list'][0]['battery_type'];
        //投保物
        $standard_product_barcode = $template['parcel_list'][0]['product_list'][0]['standard_product_barcode'];
        $product_unit_price       = $template['parcel_list'][0]['product_list'][0]['product_unit_price'];
        $currency                 = $template['parcel_list'][0]['product_list'][0]['currency'];
        $standard_product_barcode = $template['parcel_list'][0]['product_list'][0]['standard_product_barcode'];
        $standard_product_barcode = $template['parcel_list'][0]['product_list'][0]['standard_product_barcode'];
        //海关申报
        $declare_product_code      = $template['parcel_list'][0]['declare_product_info'][0]['declare_product_code'];
        $de_uses                   = $template['parcel_list'][0]['declare_product_info'][0]['uses'];
        $specification             = $template['parcel_list'][0]['declare_product_info'][0]['specification'];
        $component                 = $template['parcel_list'][0]['declare_product_info'][0]['component'];
        $unit_net_weight           = $template['parcel_list'][0]['declare_product_info'][0]['unit_net_weight'];
        $unit_gross_weight         = $template['parcel_list'][0]['declare_product_info'][0]['unit_gross_weight'];
        $material                  = $template['parcel_list'][0]['declare_product_info'][0]['material'];
        $unit_declare_product      = $template['parcel_list'][0]['declare_product_info'][0]['unit_declare_product'];
        $origin_country            = $template['parcel_list'][0]['declare_product_info'][0]['origin_country'];
        $country_export            = $template['parcel_list'][0]['declare_product_info'][0]['country_export'];
        $country_import            = $template['parcel_list'][0]['declare_product_info'][0]['country_import'];
        $hscode_export             = $template['parcel_list'][0]['declare_product_info'][0]['hscode_export'];
        $hscode_import             = $template['parcel_list'][0]['declare_product_info'][0]['hscode_import'];
        $declare_unit_price_export = $template['parcel_list'][0]['declare_product_info'][0]['declare_unit_price_export'];
        $currency_export           = $template['parcel_list'][0]['declare_product_info'][0]['currency_export'];
        $declare_unit_price_import = $template['parcel_list'][0]['declare_product_info'][0]['declare_unit_price_import'];
        $brand_export              = $template['parcel_list'][0]['declare_product_info'][0]['brand_export'];
        $brand_import              = $template['parcel_list'][0]['declare_product_info'][0]['brand_import'];
        $sales_url                 = $template['parcel_list'][0]['declare_product_info'][0]['sales_url'];
        $currency_import           = $template['parcel_list'][0]['declare_product_info'][0]['currency_import'];


        //包裹信息
        $parcel['weight']          = $weight;//预报重量（g）
        $parcel['length']          = $length;//包裹长（cm）
        $parcel['width']           = $width;//包裹宽（cm）
        $parcel['height']          = $height;//	包裹高（cm）
        $parcel['parcel_value']    = $parcel_value;//包裹申报价值（最多4位小数）
        $parcel['currency']        = $currency;//包裹申报价值币别（按照ISO标准三字码；支持的币种，根据物流产品+收件人国家配置；币种需和进出口国申报币种一致）
        $parcel['include_battery'] = $include_battery;//是否含电池（Y/N）
        $parcel['battery_type']    = $battery_type;//是否含电池（Y/N）
        //包裹列表


        foreach ($custom_skus as $custom_sku_one) {
            # code...
            //包裹投保物
            $custom_sku = $custom_sku_one->custom_sku;
            if (!empty($custom_sku_one->old_custom_sku)) {
                $custom_sku = $custom_sku_one->old_custom_sku;
            }
            $order_num = $cus_order[$custom_sku_one->id];
            $en_name   = 'noname';
            $products  = db::table('product_detail')->where('sku_id', $cus_sku[$custom_sku_one->id])->first();
            if ($products) {
                if (empty(!$products->title)) {
                    $en_name = $products->title;
                }

            }

            $en_name = $this->cutSubstr($en_name);

            $product['sku_code']                 = $custom_sku; //[@即将废弃]投保SKU（客户自定义SKUcode）（数字或字母或空格）
            $product['product_name']             = $custom_sku_one->name;//[@即将废弃]投保商品名称
            $product['qty']                      = $order_num;//	[@即将废弃]投保商品数量（单位为pcs）
            $product['product_description']      = $en_name;//[@即将废弃]投保商品描述
            $product['standard_product_barcode'] = $standard_product_barcode; //[@即将废弃]投保商品标准条码（UPC、EAN、JAN…）
            $product['product_unit_price']       = $product_unit_price;//[@即将废弃]投保商品单价（按对应币别的法定单位，最多4位小数点）
            $product['currency']                 = $currency;//[@即将废弃]投保商品单价币别（按照ISO标准三字码；支持的币种，根据物流产品+收件人国家配置；币种需和进出口国申报币种一致）
            //海关申报信息
            $declare_product['declare_product_name_cn']  = $custom_sku_one->name;//申报品名(当地语言)
            $declare_product['declare_product_name_en']  = $en_name;//申报品名（英语）
            $declare_product['declare_product_code_qty'] = $order_num;//申报数量
            $declare_product['package_remarks']          = $custom_sku . '*' . $order_num;//配货字段（打印标签选择显示配货信息是将会显示：package_remarks*qty）

            $declare_product['declare_product_code']      = $declare_product_code;//申报产品代码（在4PX已备案申报产品的代码）
            $declare_product['uses']                      = $de_uses;//用途
            $declare_product['specification']             = $specification;//规格
            $declare_product['component']                 = $component;//成分
            $declare_product['unit_net_weight']           = $unit_net_weight;//单件商品净重（默认以g为单位）
            $declare_product['unit_gross_weight']         = $unit_gross_weight;//单件商品毛重（默认以g为单位）
            $declare_product['material']                  = $material;//材质
            $declare_product['unit_declare_product']      = $unit_declare_product;//单位（点击查看详情；默认值：PCS）
            $declare_product['origin_country']            = $origin_country;//原产地（ISO标准2字码）点击查看详情
            $declare_product['country_export']            = $country_export;//出口国/起始国/发件人国家（ISO标准2字码）
            $declare_product['country_import']            = $country_import;//进口国/目的国/收件人国家（ISO标准2字码）
            $declare_product['hscode_export']             = $hscode_export;//出口国/起始国/发件人国家_海关编码(只支持数字)
            $declare_product['hscode_import']             = $hscode_import;//进口国/目的国/收件人国家_海关编码(只支持数字)
            $declare_product['declare_unit_price_export'] = $declare_unit_price_export;//出口国/起始国/发件人国家_申报单价（按对应币别的法定单位，最多4位小数点）
            $declare_product['currency_export']           = $currency_export;//出口国/起始国/发件人国家_申报单价币种（按照ISO标准；支持的币种，根据物流产品+收件人国家配置；币种需和进口国申报币种一致）
            $declare_product['declare_unit_price_import'] = $declare_unit_price_import;//USD	进口国/目的国/收件人国家_申报单价币种（按照ISO标准；支持的币种，根据物流产品+收件人国家配置；币种需和出口国申报币种一致）
            $declare_product['brand_export']              = $brand_export;//出口国/起始国/发件人国家_品牌(必填；若无，填none即可)
            $declare_product['brand_import']              = $brand_import;//进口国/目的国/收件人国家_品牌(必填；若无，填none即可)
            $declare_product['sales_url']                 = $sales_url;//配货字段（打印标签选择显示配货信息是将会显示：package_remarks*qty）
            $declare_product['currency_import']           = $currency_import;//进口国/目的国/收件人国家_申报单价币种（按照ISO标准；支持的币种，根据物流产品+收件人国家配置；币种需和出口国申报币种一致）

            $product_list[]         = $product;
            $declare_product_info[] = $declare_product;
        }


        $parcel['product_list'] = $product_list;
        // $parcel['product_list'] = $template['parcel_list'][0]['product_list'];
        $parcel['declare_product_info'] = $declare_product_info;//[@即将废弃]投保物品信息（投保、查验、货物丢失作为参考依据）
        // $parcel['declare_product_info'] = $template['parcel_list'][0]['declare_product_info'];
        $parcel_list[] = $parcel;
        // $res['parcel_list'] = $parcel_list;//包裹列表
        $template['parcel_list'] = $parcel_list;
        $template['order_id']    = $order_id;
        $template['type']        = $type;
        // $template['warehouse_id'] = $params['warehouse_id'];
        // Redis::Set('test', json_encode($template));

        return $this->SBack($template);
    }


    public function CreateFpxOrderM($params)
    {


        if (!isset($params['warehouse_id'])) {
            $rt["ref_no"] = 'Z' . time() . rand(100, 999) . 'T';
            $rt["msg"]    = '请选择仓库！';
            return $this->FailBack($rt);
        }
        $warehouse_id   = $params['warehouse_id'];
        $data['method'] = 'ds.xms.order.create';
        $data['vesion'] = '1.1.0';

        $insert['order_id'] = $params['order_id'];
        $user_id            = $params['user_id'];

        $token = $params['token'];
        unset($params['token']);
        unset($params['user_id']);
        unset($params['order_id']);
        unset($params['warehouse_id']);
        unset($params['order_attachment_info']);
        $data['data'] = $params;
        $weight       = $params['parcel_list'][0]['weight'];
        // return $this->SBack($params);
        $type = $params['type'];
        if ($type == 1) {
            $orders = db::table('amazon_order_item')->where('amazon_order_id', $insert['order_id'])->first();
        } elseif ($type == 2) {
            $orders = db::table('walmart_order_item')->where('purchase_order_id', $insert['order_id'])->first();
        } elseif ($type == 3) {
            $orders = db::table('lazada_order_item')->where('order_id', $insert['order_id'])->first();
        } else {
            return $this->FailBack('类型错误');
        }

        if ($orders) {
            $insert['shop_id'] = $orders->shop_id;
        }


        $insert['type']                 = $params['type'] ?? 1;
        $insert['request_no']           = 'zity_self_' . time() . rand(1, 999);//4PX跟踪号
        $insert['ds_consignment_no']    = '';//直发委托单号
        $insert['label_barcode']        = ''; //@ deprecated   标签条码号。*注：参数为deprecated状态
        $insert['ref_no']               = ''; //客户单号/客户参考号
        $insert['logistics_channel_no'] = '';//物流渠道号码。如果结果返回为空字符，表示暂时没有物流渠道号码，请稍后主动调用查询直发委托单接口查询

        $insert['create_time']    = date('Y-m-d H:i:s', time());
        $insert['status']         = 0;
        $insert['user_id']        = $user_id;
        $insert['weight']         = $weight;
        $insert['warehouse_id']   = $warehouse_id;
        $insert['recipient_info'] = json_encode($params['recipient_info']);//买家信息
        db::beginTransaction();    //开启事务


        $fpx_order_id = db::table('fpx_order')->insertGetid($insert);


        $custom_skus = [];
        foreach ($params['parcel_list'] as $v) {
            # code...
            $custom_skus += $v['product_list'];
        }

        $skuData = [];
        foreach ($custom_skus as $v) {
            # code...
            $custom_sku                     = $v['sku_code'];
            $num                            = $v['qty'];
            $detail_insert['custom_sku']    = $custom_sku;
            $detail_insert['custom_sku_id'] = $this->GetCustomSkuId($custom_sku);
            $detail_insert['num']           = $num;
            $detail_insert['request_no']    = $insert['request_no'];
            $onedata['custom_sku']          = $custom_sku;
            $onedata['num']                 = $num;
            db::table('fpx_order_detail')->insert($detail_insert);
            $skuData[] = $onedata;
        }


        //生成出库清单
        $invetoryModel                = new InventoryModel();
        $out_inventory['out_house']   = $warehouse_id;
        $out_inventory['token']       = $token;
        $out_inventory['skuData']     = $skuData;
        $out_inventory['type']        = 2;
        $out_inventory['shop_id']     = $orders->shop_id;
        $out_inventory['type_detail'] = 12;

        $out_msg = $invetoryModel->goods_transfers($out_inventory);
        if ($out_msg['code'] != 200) {
            db::rollback();// 回调
            $rt["ref_no"] = 'Z' . time() . rand(100, 999) . 'T';
            $rt["msg"]    = '生成出库清单失败' . $out_msg['msg'];
            return $this->FailBack($rt);
        }


        $push_msg = $invetoryModel->goods_cloud(['order_no' => $out_msg['order_no']]);

        // $box_id = $out_msg['box_id'];
        if ($push_msg['code'] != 200) {
            db::rollback();// 回调
            $rt["ref_no"] = 'Z' . time() . rand(100, 999) . 'T';
            $rt["msg"]    = '推送失败' . $push_msg['msg'];
            return $this->FailBack($rt);
        }

        $box_id = $push_msg['box_id'];

        $url = $this->getFpx($data);

        $fpxress = json_decode($url, true);

        if ($fpxress['msg'] == 'System processing failed') {
            $rt["ref_no"] = 'Z' . time() . rand(100, 999) . 'T';
            $rt["msg"]    = $fpxress['errors'][0]['error_msg'];
            return $this->FailBack($rt);
        }
        $fpxres = $fpxress['data'];

        $fpx_insert['ds_consignment_no']    = $fpxres['ds_consignment_no'];//直发委托单号
        $fpx_insert['label_barcode']        = $fpxres['label_barcode']; //@ deprecated   标签条码号。*注：参数为deprecated状态
        $fpx_insert['ref_no']               = $fpxres['ref_no']; //客户单号/客户参考号
        $fpx_insert['logistics_channel_no'] = $fpxres['logistics_channel_no'];//物流渠道号码。如果结果返回为空字符，表示暂时没有物流渠道号码，请稍后主动调用查询直发委托单接口查询
        $fpx_insert['request_no']           = $fpxres['4px_tracking_no'];//4PX跟踪号
        $fpx_insert['box_id']               = $box_id;
        $fpx_insert['out_no']               = $out_msg['order_no'];


        db::table('fpx_order')->where('id', $fpx_order_id)->update($fpx_insert);

        db::table('fpx_order_detail')->where('request_no', $insert['request_no'])->update(['request_no' => $fpxres['4px_tracking_no']]);

        db::commit();

        return $this->SBack($push_msg['data']);
    }

    //4px订单列表
    public function FpxOrderListM($params)
    {

        $posttype = $params['posttype'] ?? 1;

        $pagenum = isset($params['page']) ? $params['page'] : 1;
        $limit   = isset($params['limit']) ? $params['limit'] : 30;

        $page = $pagenum - 1;
        if ($page != 0) {
            $page = $page * $limit;
        }

        $list = db::table('fpx_order as a')->leftjoin('fpx_order_detail as b', 'a.request_no', '=', 'b.request_no')->leftjoin('self_custom_sku as c', 'b.custom_sku_id', '=', 'c.id');

        if (isset($params['request_no'])) {
            $list = $list->where('a.request_no', $params['request_no']);
        }
        if (isset($params['order_id'])) {
            $list = $list->where('a.order_id', $params['order_id']);
        }

        if (isset($params['start_time']) && isset($params['end_time'])) {
            $start_time = $params['start_time'];
            $end_time   = $params['end_time'];
            $list       = $list->whereBetween('a.out_time', [$start_time, $end_time]);
        }

        if (isset($params['warehouse_id'])) {
            $list = $list->where('a.warehouse_id', $params['warehouse_id']);
        }

        if (isset($params['user_id'])) {
            $list = $list->where('a.user_id', $params['user_id']);
        }

        if (isset($params['type'])) {
            $list = $list->where('a.type', $params['type']);
        }

        if (isset($params['out_no'])) {
            $list = $list->where('a.out_no', $params['out_no']);
        }


        if (isset($params['status'])) {
            $list = $list->whereIn('a.status', $params['status']);
        } else {
            $list = $list->where('a.status', '>', 0);
        }

        if (isset($params['custom_sku'])) {
            $list = $list->where(function ($query) use ($params) {
                $query->where('c.custom_sku', 'like', '%' . $params['custom_sku'] . '%')
                    ->orwhere('c.old_custom_sku', 'like', '%' . $params['custom_sku'] . '%');
            });
        }

        $totalNum = $list->groupby('a.request_no')->get()->count();
        if ($posttype == 2) {
            $list = $list->select('a.*')->groupby('a.request_no')->get();
        } else {
            $list = $list->select('a.*')->groupby('a.request_no')->offset($page)->limit($limit)->get();
        }

        foreach ($list as $v) {
            # code...
            $v->user_name = $this->GetUsers($v->user_id)['account'] ?? '';
            $v->shop_name = $this->GetShop($v->shop_id)['shop_name'] ?? '';
            $v->log       = [];
            $log          = db::table('fpx_order_log')->where('request_no', $v->request_no)->get();
            if ($log) {
                foreach ($log as $lv) {
                    # code...
                    $lv->user_name = $this->GetUsers($lv->user_id)['account'];
                }
                $v->log = $log;
            }

            $dcus = [];

            $ecus = '';
            $num  = 0;
            $cus  = db::table('fpx_order_detail')->where('request_no', $v->request_no)->get();
            foreach ($cus as $cv) {
                # code...
                $num                    += $cv->num;
                $ecus                   .= $cv->custom_sku . '*' . $cv->num . "     \n";
                $dcus_one['custom_sku'] = $cv->custom_sku;
                $dcus_one['num']        = $cv->num;
                $dcus[]                 = $dcus_one;
            }

            $v->total = $num;
            $v->ecus  = $ecus;

            $v->custom_sku     = $dcus;
            $v->recipient_info = json_decode($v->recipient_info);

            $v->warehouse = '';
            if ($v->warehouse_id == 1) {
                $v->warehouse = '同安';
            }
            if ($v->warehouse_id == 2) {
                $v->warehouse = '泉州';
            }
            $v->cus_res     = json_encode($v->custom_sku);
            $v->weight_log  = json_encode($v->log);
            $v->status_name = '';
            //状态：-1 已取消 0 未发货  1已预报   2已交接/已交货 3库内作业中 4已出库  5已关闭',
            if ($v->status == -1) {
                $v->status_name = '已取消';
            }
            if ($v->status == 0) {
                $v->status_name = '待发货';
            }
            if ($v->status == 1) {
                $v->status_name = '已预报';
            }
            if ($v->status == 2) {
                $v->status_name = '已交接/已交货';
            }
            if ($v->status == 3) {
                $v->status_name = '库内作业中';
            }
            if ($v->status == 4) {
                $v->status_name = '已出库';
            }
            if ($v->status == 5) {
                $v->status_name = '已关闭';
            }

            $v->type_name = '';
            if ($v->type == 1) {
                $v->type_name = '亚马逊';
            }
            if ($v->type == 2) {
                $v->type_name = '沃尔玛';
            }
            if ($v->type == 3) {
                $v->type_name = 'lazada';
            }

            //出库提示
            $v->out_status = '';
            $out_status    = db::table('inhouse_err_log')->where('out_no', $v->out_no)->first();
            if ($out_status) {
                $v->out_status = $out_status->msg;
            }


        }
        $data = ['data' => $list, "totalnum" => $totalNum];


        if ($posttype == 2) {
            $p['title']      = '自发货-已发货' . time();
            $p['title_list'] = [
                'request_no'           => '4px单号',
                'order_id'             => '订单id',
                'ds_consignment_no'    => '直发委托单号',
                'label_barcode'        => '标签条码号',
                'ref_no'               => '客户单号',
                'out_no'               => '系统出库单号',
                'warehouse'            => '仓库',
                'logistics_channel_no' => '物流渠道号码',
                'ecus'                 => '库存sku',
                'total'                => '合计数量',
                'shop_name'            => '店铺',
                'weight'               => '重量',
                'weight_log'           => '重量修改日志',
                'status_name'          => '状态',
                'out_time'             => '出库时间',
                'create_time'          => '创建时间',
                'user_name'            => '创建用户',
                'type_name'            => '订单类型',
            ];

            // $this->excel_expord($p);

            $p['data']    = $list;
            $p['user_id'] = $params['find_user_id'] ?? 1;
            $p['type']    = '自发货-已发货-导出';
            $job          = new \App\Http\Controllers\Jobs\ExcelTaskController;
            try {
                $job::runJob($p);
            } catch (\Exception $e) {
                return $this->FailBack('加入队列失败');
            }
            return $this->SBack([]);
        }

        return $this->SBack($data);
    }


    public function GetLazadaOrderLabel($params)
    {
        $order_id = $params['order_id'];
        $type     = $params['type'] ?? 1; //1打包   2获取面单   3发货   4获取打包码
        $order    = db::table('lazada_order_item')->where('order_id', $order_id);
        if (isset($params['order_item'])) {
            $order_item = $params['order_item'];
            $order      = $order->whereIn('order_item_id', $order_item);
        }

        $order = $order->get();

        $order_itemb = [];
        $package_id  = [];

        $ids = [];

        $cus_id = [];
        foreach ($order as $v) {
            # code...
            $order_itemb[] = $v->order_item_id;
            $package_id[]  = $v->package_id;
            $shop_id       = $v->shop_id;
            $area          = $v->area;
            $ids[]         = $v->id;
            if ($v->custom_sku_id <= 0) {
                return $this->FailBack('该订单未绑定库存sku' . $v->order_id);
            }
            if (isset($cus_id[$v->custom_sku_id])) {
                $cus_id[$v->custom_sku_id] += 1;
            } else {
                $cus_id[$v->custom_sku_id] = 1;
            }

        }

        $skuData = [];

        if ($type == 1) {
            foreach ($cus_id as $cid => $cnum) {
                # code...
                $quanzhou_deduct_request_num = $this->inventoryPlatformDetail($cid, 2, [3])[0]['intsock_num'] ?? 0;

                if ($cnum > $quanzhou_deduct_request_num) {
                    return $this->FailBack('该库存sku库存不足无法发货' . $cid);
                }

                $cus_res = db::table('self_custom_sku')->where('id', $cid)->first();
                if (!$cus_res) {
                    return $this->FailBack('该skuid错误' . $cid);
                }

                $onedata['custom_sku'] = $cus_res->custom_sku;
                $onedata['num']        = $cnum;
                $skuData[]             = $onedata;

            }
        }


        $pdf_data = [];
        if ($type == 2) {
            foreach ($cus_id as $cid => $cnum) {
                # code...
                $cus_res = db::table('self_custom_sku')->where('id', $cid)->first();
                if (!$cus_res) {
                    return $this->FailBack('该skuid错误' . $cid);
                }

                //打印pdf数据
                $pdf_one_data['custom_sku_id'] = $cid;
                $pdf_one_data['custom_sku']    = $cus_res->custom_sku;
                $pdf_one_data['num']           = $cnum;
                $pdf_one_data['color']         = $cus_res->color;
                $pdf_one_data['size']          = $cus_res->size;
                $pdf_one_data['name']          = $cus_res->name;

                $pdf_data[] = $pdf_one_data;


            }
        }


        if (isset($params['order_item'])) {
            sort($order_item);
            sort($order_itemb);
            if ($order_item != $order_itemb) {
                return $this->FailBack('子订单与勾选不符，请重新勾选' . json_encode($order_itemb));
            }
        }

        $lazada_token = db::table('lazada_token')->where('area', $area)->where('shop_id', $shop_id)->first();

        if (!$lazada_token) {
            return $this->FailBack('无授权');
        }

        $token              = $lazada_token->token;
        $data['order_id']   = $order_id;
        $data['order_item'] = $order_itemb;
        $data['package_id'] = $package_id;
        $data['token']      = $token;
        $data['area']       = $area;
        $data['shop_id']    = $shop_id;
        $data['type']       = $type;

        $post_url = 'http://api.zity.cn/lazada/Fulfillment.php?data=' . json_encode($data);


        if ($type == 1) {
            db::beginTransaction();    //开启事务
            //生成出库清单
            $invetoryModel                = new InventoryModel();
            $out_inventory['out_house']   = 2;
            $tokens                       = $params['token'];
            $out_inventory['token']       = $tokens;
            $out_inventory['skuData']     = $skuData;
            $out_inventory['type']        = 2;
            $out_inventory['shop_id']     = $shop_id;
            $out_inventory['type_detail'] = 12;

            $out_msg = $invetoryModel->goods_transfers($out_inventory);
            if ($out_msg['code'] != 200) {
                db::rollback();// 回调
                $rt["ref_no"] = 'Z' . time() . rand(100, 999) . 'T';
                $rt["msg"]    = '生成出库清单失败' . $out_msg['msg'];
                return $this->FailBack($rt);
            }


            $push_msg = $invetoryModel->goods_cloud(['order_no' => $out_msg['order_no']]);

            if ($push_msg['code'] != 200) {
                db::rollback();// 回调
                $rt["ref_no"] = 'Z' . time() . rand(100, 999) . 'T';
                $rt["msg"]    = '推送失败' . $push_msg['msg'];
                return $this->FailBack($rt);
            }

            //  $box_id = $push_msg['box_id'];
            $post = $this->curlPost($post_url);
            $post = json_decode($post, true);

            $rdata = $post['data']['result'];
            if (!$rdata['success'] || !isset($rdata['success'])) {
                db::rollback();// 回调
                return $this->FailBack($rdata['error_msg']);
            }

            //  var_dump($rdata);
            $pack_order_list = $rdata['data']['pack_order_list'][0]['order_item_list'];

            foreach ($pack_order_list as $pov) {
                # code...
                db::table('lazada_order_item')->where('order_item_id', $pov['order_item_id'])->update(['package_id' => $pov['package_id']]);
            }


            $l_o_u['out_no']       = $out_msg['order_no'];
            $l_o_u['order_status'] = 1;
            db::table('lazada_order_item')->whereIn('id', $ids)->update($l_o_u);
            db::commit();
            return $this->SBack($push_msg['data']);
        } else {
            $post = $this->curlPost($post_url);
            $post = json_decode($post, true);

            $rdata = $post['data']['result'];

            if ($rdata['success']) {
                $nowdata = $rdata['data'];
                if ($type == 3) {
                    $lorder        = db::table('lazada_order_item')->whereIn('id', $ids)->first();
                    $invetoryModel = new InventoryModel();
                    try {
                        //code...
                        $out = $invetoryModel->confirmInhouse(['order_no' => $lorder->out_no]);
                    } catch (\Throwable $th) {
                        //throw $th;
                        return $this->SBack($th->getMessage());
                    }

                    if ($out['code'] == 500) {
                        return $this->SBack($out['msg']);
                    }
                    db::table('lazada_order_item')->whereIn('id', $ids)->update(['order_status' => 2]);
                }

                if ($type == 2) {
                    $pdf_url          = '';
                    $pdf_datas['url'] = $nowdata['pdf_url'];
                    $lorder           = db::table('lazada_order_item')->whereIn('id', $ids)->first();

                    foreach ($pdf_data as &$pdfv) {

                        $boxs = db::table('goods_transfers_box')->where('order_no', $lorder->out_no)->first();
                        if (!$boxs) {
                            return $this->FailBack('库位中无此订单:' . $lorder->out_no);
                        }

                        $location_code = db::table('goods_transfers_box_detail')->where('custom_sku_id', $pdfv['custom_sku_id'])->where('box_id', $boxs->id)->first();
                        if (!$location_code) {
                            return $this->FailBack('库位中无此订单1cus_id:' . $pdfv['custom_sku_id'] . 'box_id' . $boxs->id);
                        }


                        $location_res = db::table('cloudhouse_warehouse_location')->where('id', $location_code->location_ids)->first();
                        if (!$location_res) {
                            return $this->FailBack('无此库位:' . $location_code->location_ids);
                        }
                        $pdfv['location_code'] = $location_res->location_code;
                        $pdfv['box_num']       = $location_code->box_num;
                        $pdfv['box_code']      = $boxs->box_code;
                    }
                    $pdf_datas['cus']    = $pdf_data;
                    $pdf_url             = 'http://api.zity.cn/barcode/getlzdcode.php?data=' . urlencode(json_encode($pdf_datas));
                    $nowdata['pdf_urlb'] = $nowdata['pdf_url'];
                    $nowdata['pdf_url']  = $pdf_url;
                }
            } else {
                return $this->FailBack($rdata['error_msg']);
            }
            return $this->SBack($nowdata);
        }


    }


    //订单详情   状态：-1 已取消 0 已预报  1确认（仓库）   2已交接/已交货 3库内作业中 4已出库  5已关闭
    public function FpxOrderDetailM($params)
    {
        $request_no = $params['request_no'];
        $order      = db::table('fpx_order')->where('request_no', $request_no)->first();
        if (!$order) {
            return $this->FailBack('无此订单');
        }

        $order->user_name = $this->GetUsers($order->user_id)['account'];

        $new_detail = $this->GetFpxOrderM(['request_no' => $request_no]);
        if (isset($new_detail['data'])) {
            $new_detail = json_decode($new_detail['data'], true);
        }
        // $status = -1;
        // $order->parcel_confirm_info = [];
        // if(isset($new_detail['data'])){
        //     // if(empty($new_detail['data'])){
        //     //     $status = -1;
        //     // }
        //     // var_dump($new_detail['data']);
        //     $return_datas = json_decode($new_detail['data'],true);
        //     // var_dump( $return_data);
        //     // $return_data = $return_datas[0];
        //     if(empty($return_datas['data'])){
        //         $status = -1;//已取消
        //         db::table('fpx_order')->where('request_no',$request_no)->update(['status'=>$status]);
        //     }
        //     if(isset($return_datas['data'][0])){
        //         $return_data = $return_datas['data'][0];
        //         if(isset($return_data['consignment_info']['consignment_status'])){
        //             $status = $return_data['consignment_info']['consignment_status'];//委托单状态（草稿：D；已预报：P；已交接/已交货：V；库内作业中：H；已出库：C；已关闭：X；）
        //             if($status=='P'){$status=0;}//已预报
        //             if($status=='V'){$status=2;}//已交接/已交货
        //             if($status=='H'){$status=3;}//库内作业中
        //             if($status=='C'){$status=4;}//已出库
        //             if($status=='X'){$status=5;}//已关闭
        //             db::table('fpx_order')->where('request_no',$request_no)->update(['status'=>$status]);
        //         }
        //         if(isset($return_data['parcel_confirm_info'])){
        //             $parcel_confirm_info = $return_data['parcel_confirm_info'];
        //             $order->parcel_confirm_info =$parcel_confirm_info;
        //             // $order->parcel_confirm_info = json_decode($parcel_confirm_info,true);
        //         }
        //     }
        // }

        // $order->status = $status;
        // $detail = db::table('fpx_order_detail')->where('request_no',$request_no)->get();

        // $return['order'] = $order;
        // $return['detail'] = $detail;

        return $this->SBack($new_detail);
    }

    //订单错误查询  
    public function FpxOrderErrM($params)
    {
        $request_no = $params['request_no'];
        $order      = db::table('fpx_order')->where('request_no', $request_no)->first();
        if (!$order) {
            return $this->FailBack('无此订单');
        }

        $order->user_name = $this->GetUsers($order->user_id)['account'];

        $time       = $this->getMillisecond();
        $new_detail = $this->GetFpxErrM(['request_no' => $request_no, 'obnormal_type' => 'IW', 'start_time_of_create_consignment' => $time, 'end_time_of_create_consignment']);
        if (isset($new_detail['data'])) {
            $new_detail = json_decode($new_detail['data'], true);
        }
        return $this->SBack($new_detail);
    }


    //模版查询
    public function GetFpxTemplateM($params)
    {
        $id  = $params['id'];
        $res = db::table('fpx_template')->where('id', $id)->first();
        return $this->SBack($res);
    }

    //模版列表
    public function GetFpxTemplateListM($params)
    {
        $res = db::table('fpx_template')->get();
        return $this->SBack($res);
    }

    //模版新增
    public function SaveFpxTemplateM($params)
    {
        $name               = $params['name'];
        $template           = $params['template'];
        $insert['name']     = $name;
        $insert['template'] = $template;
        if (isset($params['id'])) {
            $id = $params['id'];
            db::table('fpx_template')->where('id', $id)->update($insert);
        } else {
            db::table('fpx_template')->insert($insert);
        }

        return $this->SBack([]);
    }


    public function GetFpxAmzonOrderListM($params)
    {
        $pagenum = isset($params['page']) ? $params['page'] : 1;
        $limit   = isset($params['limit']) ? $params['limit'] : 30;

        $page = $pagenum - 1;
        if ($page != 0) {
            $page = $page * $limit;
        }



        $list = db::table('amazon_order_item');
        if (isset($params['start_time']) && isset($params['end_time'])) {
            $start_time = $params['start_time'];
            $end_time   = $params['end_time'];
            $list       = $list->whereBetween('amazon_time', [$start_time, $end_time]);
        }

        if (isset($params['fh_start_time']) && isset($params['fh_end_time'])) {
            $fh_start_time = $params['fh_start_time'];
            $fh_end_time   = $params['fh_end_time'];
            $list          = $list->where('earliest_ship_date', '>=', $fh_start_time);
            $list          = $list->where('latest_ship_date', '<=', $fh_end_time);
        }

        if (isset($params['sku'])) {
            $skus    = db::table('self_sku')->where('sku', $params['sku'])->orwhere('old_sku', $params['sku'])->get()->toArray();
            $sku_ids = array_column($skus, 'id');
            $list    = $list->whereIn('sku_id', $sku_ids);
        }
        if (isset($params['custom_sku'])) {
            $cus     = db::table('self_custom_sku')->where('custom_sku', $params['custom_sku'])->orwhere('old_custom_sku', $params['custom_sku'])->get()->toArray();
            $cus_ids = array_column($cus, 'id');
            $list    = $list->whereIn('custom_sku_id', $cus_ids);
        }
        if (isset($params['spu'])) {
            $spus    = db::table('self_spu')->where('spu', $params['spu'])->orwhere('old_spu', $params['spu'])->get()->toArray();
            $spu_ids = array_column($spus, 'id');
            $list    = $list->whereIn('spu_id', $spu_ids);
        }

        if (isset($params['order_id'])) {
            $list = $list->where('amazon_order_id', $params['order_id']);
        }


        if (isset($params['amazon_order_ids'])) {

            if (strstr($params['amazon_order_ids'], '，')) {
                $symbol = '，';
            } elseif (strstr($params['amazon_order_ids'], ',')) {
                $symbol = ',';
            } else {
                $symbol = '';
            }
            if ($symbol) {
                $ids = explode($symbol, $params['amazon_order_ids']);
            } else {
                $ids[0] = $params['amazon_order_ids'];
            }

            $list = $list->whereIn('amazon_order_id', $ids);

        }

        if (isset($params['shop_id'])) {
            $list = $list->where('shop_id', $params['shop_id']);
        }

        if (isset($params['user_id'])) {
            $list = $list->where('user_id', $params['user_id']);
        }

        $cancel_orders    = db::table('amazon_order_cancel')->groupby('amazon_order_id')->select('amazon_order_id')->get()->toArray();
        $cancel_order_ids = [];
        if ($cancel_orders) {
            $cancel_order_ids = array_column($cancel_orders, 'amazon_order_id');
        }

        // if(isset($params['status'])){
        $fpx_order = db::table('fpx_order')->whereNotIn('status', [-1, 0]);
        if (isset($params['out_no'])) {
            $fpx_order = $fpx_order->where('out_no', $params['out_no']);
        }

        $fpx_order = $fpx_order->get()->toArray();
        $fpx_ids   = array_column($fpx_order, 'order_id');
        $fpx_ids   = array_merge($cancel_order_ids, $fpx_ids);
        $list      = $list->whereNotIn('amazon_order_id', $fpx_ids);
        // }

        $list     = $list->where('fulfillment_channel', 'MFN')->whereIn('order_status', array('Pending', 'Unshipped'));
        $totalNum = $list->count();


        $list = $list
        ->offset($page)
        ->limit($limit)
        ->orderBy('amazon_time', 'desc')
        ->get();




        $customSkuIds = [];


        foreach ($list as $v) {
            $v->shop_name = $this->GetShop($v->shop_id)['shop_name'] ?? '';
            $v->user      = $this->GetUsers($v->user_id)['account'] ?? '';
            $cus          = db::table('self_custom_sku')->where('id', $v->custom_sku_id)->first();
            if (!$cus) {
                $bsks = db::table("self_sku")->where('sku', $v->seller_sku)->orwhere('old_sku', $v->seller_sku)->first();
                if ($bsks) {
                    $v->custom_sku_id = $bsks->custom_sku_id;
                    $v->spu_id        = $bsks->spu_id;
                    $cus              = db::table('self_custom_sku')->where('id', $bsks->custom_sku_id)->first();
                    if ($cus) {
                        db::table('amazon_order_item')->where('id', $v->id)->update(['spu_id' => $cus->spu_id, 'custom_sku_id' => $cus->id]);
                    }
                }

            }
            if ($v->custom_sku_id > 0) {
                $customSkuIds[] = $v->custom_sku_id;
            }

            $v->custom_sku = '';
            $v->title      = '';
            if ($cus) {
                $v->custom_sku = $cus->custom_sku;
                if (!empty($v->old_custom_sku)) {
                    $v->custom_sku = $cus->old_custom_sku;
                }
                $v->title = $cus->name;
            }
            $spus      = db::table("self_spu")->where('id', $v->spu_id)->first();
            $v->is_spu = true;

            $v->category = '';
            if ($spus) {
                $v->spu      = $spus->spu;
                $cate_three  = $this->GetCategory($spus->three_cate_id)['name'];
                $cate_one    = $this->GetCategory($spus->one_cate_id)['name'];
                $v->category = $cate_one . $cate_three;
            } else {
                $bspus = db::table("self_spu")->where('spu', $v->spu)->orwhere('old_spu', $v->spu)->first();
                if ($bspus) {
                    db::table('amazon_order_item')->where('id', $v->id)->update(['spu_id' => $bspus->id]);
                    $cate_three  = $this->GetCategory($bspus->three_cate_id)['name'];
                    $cate_one    = $this->GetCategory($bspus->one_cate_id)['name'];
                    $v->category = $cate_one . $cate_three;
                    $v->spu_id   = $bspus->id;
                    $v->spu      = $bspus->spu;
                } else {
                    $bsks = db::table("self_sku")->where('sku', $v->seller_sku)->orwhere('old_sku', $v->seller_sku)->first();
                    if ($bsks) {
                        db::table('amazon_order_item')->where('id', $v->id)->update(['sku_id' => $bsks->id, 'custom_sku_id' => $bsks->custom_sku_id, 'spu_id' => $bsks->spu_id]);
                        $bskspus     = db::table("self_spu")->where('id', $bsks->spu_id)->first();
                        $cate_three  = $this->GetCategory($bskspus->three_cate_id)['name'];
                        $cate_one    = $this->GetCategory($bskspus->one_cate_id)['name'];
                        $v->category = $cate_one . $cate_three;
                        $v->spu_id   = $bskspus->id;
                        $v->spu      = $bsks->spu;
                    } else {
                        $v->is_spu = false;
                        $v->spu    = 'sku暂未录入系统，无法发货';
                    }
                }
            }

            $v->is_cancel = 0;

            $fpx_del = db::table('fpx_del_order')->where('order_id', $v->amazon_order_id)->where('type', 1)->first();
            if ($fpx_del) {
                $v->is_cancel = 1;
            }
        }

        //查询库存
        $inventoryArr = $this->getInventoryDetailByWlp($customSkuIds, [1, 2], [], [4, 5]);

        //查询云仓库存
        $cloudInventoryArr = db::table('self_custom_sku')->whereIn('id', $customSkuIds)->pluck('cloud_num', 'id');

        //查询云仓库存锁定情况
        $cloudInventoryLockArr = $this->batchInventoryDetailByWlp($customSkuIds, [3]);


        foreach ($list as $v) {

            //同安亚马逊可用库存
            $tongAnWalmartNum             = $inventoryArr[$v->custom_sku_id . '_1_5'] ?? ['usable_num' => 0];
            $v->tongan_deduct_request_num = $tongAnWalmartNum['usable_num'];

            //泉州亚马逊可用库存
            $quanzhouWalmartNum             = $inventoryArr[$v->custom_sku_id . '_2_5'] ?? ['usable_num' => 0];
            $v->quanzhou_deduct_request_num = $quanzhouWalmartNum['usable_num'] >= 0 ? $quanzhouWalmartNum['usable_num'] : 0;

            //同安可调库存
            $tongAnTunableNum      = $inventoryArr[$v->custom_sku_id . '_1_4'] ?? ['usable_num' => 0];
            $v->tongan_tunable_num = $tongAnTunableNum['usable_num'];

            //泉州沃尔玛可用库存
            $quanzhouTunableNum      = $inventoryArr[$v->custom_sku_id . '_2_4'] ?? ['usable_num' => 0];
            $v->quanzhou_tunable_num = $quanzhouTunableNum['usable_num'];

            //云仓库存
            $v->cloud_deduct_request_num = $cloudInventoryArr[$v->custom_sku_id] ?? 0;
            if ($v->cloud_deduct_request_num > 0 && isset($cloudInventoryLockArr[$v->custom_sku_id . '_3'])) {
                $cloudInventoryLockNum       = $cloudInventoryLockArr[$v->custom_sku_id . '_3'];
                $v->cloud_deduct_request_num -= ($cloudInventoryLockNum['lock_num'] + $cloudInventoryLockNum['shelf_num']);
            }
        }


        $data['total_num'] = $totalNum;
        $data['data']      = $list;
        return $this->SBack($data);
    }

    public function GetFpxLxAmzonOrderList($params)
    {
        $pagenum = isset($params['page']) ? $params['page'] : 1;
        $limit   = isset($params['limit']) ? $params['limit'] : 30;

        $page = $pagenum - 1;
        if ($page != 0) {
            $page = $page * $limit;
        }

        $posttype = $params['posttype']??1;

        $query = DB::table('lingxing_amazon_order_item')
            ->select([
                'id',
                'lingxing_amazon_order_id as local_order_id',
                'amazon_order_id',
                'shop_id',
                'quantity',
                'item_price_amount as amount',
                'msku as sku',
                'order_status',
                'global_purchase_time as order_date',
                'user_id',
                'custom_sku_id',
                'spu_id',
                'sku_id',
                'spu',
                'create_time',
                'update_time',
            ]);

        $type = $params['type'] ?? 1;//1:总订单表 2:待发货表

        if ($type == 2) {
            $notOrderIds = db::table('fpx_order')->where('type', 1)->where('status', '>=', 0)->pluck('order_id');
            $query->whereNotIn('amazon_order_id', $notOrderIds)->whereIn('order_status', ['pending','Unshipped']);
        }

        if (isset($params['start_time']) && isset($params['end_time'])) {
            $start_time = $params['start_time'];
            $end_time   = $params['end_time'];
            $query->whereBetween('global_purchase_time', [strtotime($start_time), strtotime($end_time)]);
        }

        if (isset($params['spu'])) {
            $spuIds = db::table('self_spu')->where('spu', $params['spu'])->orwhere('old_spu', $params['spu'])->pluck('id');
            if ($spuIds->isEmpty()) {
                return $this->SBack(['data' => [], 'total_num' => 0]);
            }

            $query->whereIn('spu_id', $spuIds);
        }

        if (isset($params['custom_sku'])) {
            $customSkuIds = db::table('self_custom_sku')
                ->where('custom_sku', $params['custom_sku'])
                ->orwhere('old_custom_sku', $params['custom_sku'])
                ->pluck('id');
            if ($customSkuIds->isEmpty()) {
                return $this->SBack(['data' => [], 'total_num' => 0]);
            }

            $query->whereIn('custom_sku_id', $customSkuIds);
        }

        if (isset($params['sku'])) {
            $skuIds = db::table('self_sku')->where('sku', $params['sku'])->orwhere('old_sku', $params['sku'])->pluck('id');
            if ($skuIds->isEmpty()) {
                return $this->SBack(['data' => [], 'total_num' => 0]);
            }

            $query->whereIn('sku_id', $skuIds);
        }

        if (isset($params['shop_id'])) {
            $query->where('shop_id', $params['shop_id']);
        }

        if (isset($params['user_id'])) {
            $query->where('user_id', $params['user_id']);
        }

        if (isset($params['amazon_order_ids'])) {
            if (strstr($params['amazon_order_ids'], '，')) {
                $symbol = '，';
            } elseif (strstr($params['amazon_order_ids'], ',')) {
                $symbol = ',';
            } else {
                $symbol = '';
            }
            if ($symbol) {
                $ids = explode($symbol, $params['amazon_order_ids']);
            } else {
                $ids[0] = $params['amazon_order_ids'];
            }

            $query->whereIn('amazon_order_id', $ids);
        }

        if (isset($params['order_id'])) {
            $query->where('amazon_order_id', 'like', '%' . $params['order_id'] . '%');
        }

        if (isset($params['is_cancel'])) {
            if ($params['is_cancel'] == 0) {
                $query->where('order_status', '<>', 'Canceled');
            } else {
                $query->where('order_status', 'Canceled');
            }
        }

        $totalNum = $query->count();
        if ($totalNum == 0) {
            return $this->SBack(['data' => [], 'total_num' => 0]);
        }

        if($posttype==2){
            $list     = $query->orderBy('global_purchase_time', 'desc')->get();
        }else{
            $list     = $query->offset($page)->limit($limit)->orderBy('global_purchase_time', 'desc')->get();
        }

        $orderIds = $list->pluck('local_order_id');

        $address = db::table('lingxing_amazon_order')
            ->select([
                'id',
                'receiver_mobile as phone',
                'receiver_name as postal_address_name',
                'address_line1 as address1',
                'address_line2',
                'address_line3',
                'city',
                'postal_code',
                'receiver_country_code as country',
                'state_or_region as postal_address_state'
            ])
            ->whereIn('id', $orderIds)
            ->get()
            ->keyBy('id');

        foreach ($address as $v) {
            if ($v->address_line2) {
                $v->address1 .= ' , ' . $v->address_line2;
            }
            if ($v->address_line3) {
                $v->address1 .= ' , ' . $v->address_line3;
            }
        }

        $customSkuList = db::table('self_custom_sku')->whereIn('id', $list->pluck('custom_sku_id'))->get()->keyBy('id');
        //匹配库存sku
        foreach ($list as $v) {
            if ($v->custom_sku_id && $v->sku_id && $v->spu_id && $v->spu) {
                $customSkuInfo = $customSkuList[$v->custom_sku_id] ?? null;
                $v->custom_sku = $customSkuInfo ? ($customSkuInfo->old_custom_sku ?: $customSkuInfo->custom_sku) : '';
                $v->title      = $customSkuInfo ? $customSkuInfo->name : '';
                continue;
            }

            $skuInfo   = db::table("self_sku")->where('sku', $v->sku)->orwhere('old_sku', $v->sku)->first();
            $v->is_spu = true;
            if (!$skuInfo) {
                $v->is_spu = false;
                $v->spu    = 'sku暂未录入系统，无法发货';
                continue;
            }


            $customSkuInfo = db::table('self_custom_sku')->where('id', $skuInfo->custom_sku_id)->first();
            if (!$customSkuInfo) continue;

            db::table('lingxing_amazon_order_item')->where('id', $v->id)->update([
                'spu_id'        => $customSkuInfo->spu_id,
                'spu'           => $customSkuInfo->spu,
                'custom_sku_id' => $customSkuInfo->id,
                'sku_id'        => $skuInfo->id,
            ]);

            $v->spu_id        = $customSkuInfo->spu_id;
            $v->spu           = $customSkuInfo->spu;
            $v->custom_sku_id = $customSkuInfo->id;
            $v->sku_id        = $skuInfo->id;

            $v->custom_sku = $customSkuInfo->old_custom_sku ?: $customSkuInfo->custom_sku;
            $v->title      = $customSkuInfo->name;
        }

        //查询库存
        $customSkuIds = array_unique($list->pluck('custom_sku_id')->toArray());
        $inventoryArr = $this->getInventoryDetailByWlp($customSkuIds, [1, 2], [], [4, 5]);

        //查询云仓库存
        $cloudInventoryArr = db::table('self_custom_sku')->whereIn('id', $customSkuIds)->pluck('cloud_num', 'id');
        //查询云仓库存锁定情况
        $cloudInventoryLockArr = $this->batchInventoryDetailByWlp($customSkuIds, [3]);

        $spuList = db::table('self_spu')
            ->select('id', 'one_cate_id', 'three_cate_id')
            ->whereIn('id', $list->pluck('spu_id'))
            ->get()
            ->keyBy('id');

        foreach ($list as $v) {
            $v->category = '';
            $spuInfo     = $spuList[$v->spu_id] ?? null;
            if ($spuInfo) {
                $v->category = $this->GetCategory($spuInfo->one_cate_id)['name'] . $this->GetCategory($spuInfo->three_cate_id)['name'];
            }
            $v->order_date = date('Y-m-d H:i:s', $v->order_date);

            $v->shop_name = $this->GetShop($v->shop_id)['shop_name'] ?? '';
            $v->user      = $v->user_id ? ($this->GetUsers($v->user_id)['account'] ?? '') : '';
            $v->address   = $address[$v->local_order_id];

            //同安沃尔玛可用库存
            $tongAnWalmartNum             = $inventoryArr[$v->custom_sku_id . '_1_5'] ?? ['usable_num' => 0];
            $v->tongan_deduct_request_num = $tongAnWalmartNum['usable_num'];

            //泉州沃尔玛可用库存
            $quanzhouWalmartNum             = $inventoryArr[$v->custom_sku_id . '_2_5'] ?? ['usable_num' => 0];
            $v->quanzhou_deduct_request_num = $quanzhouWalmartNum['usable_num'];

            //同安可调库存
            $tongAnTunableNum      = $inventoryArr[$v->custom_sku_id . '_1_4'] ?? ['usable_num' => 0];
            $v->tongan_tunable_num = $tongAnTunableNum['usable_num'];
            //泉州沃尔玛可用库存
            $quanzhouTunableNum      = $inventoryArr[$v->custom_sku_id . '_2_4'] ?? ['usable_num' => 0];
            $v->quanzhou_tunable_num = $quanzhouTunableNum['usable_num'];

            //云仓库存
            $v->cloud_deduct_request_num = $cloudInventoryArr[$v->custom_sku_id] ?? 0;
            if ($v->cloud_deduct_request_num > 0 && isset($cloudInventoryLockArr[$v->custom_sku_id . '_3'])) {
                $cloudInventoryLockNum       = $cloudInventoryLockArr[$v->custom_sku_id . '_3'];
                $v->cloud_deduct_request_num -= ($cloudInventoryLockNum['lock_num'] + $cloudInventoryLockNum['shelf_num']);
            }
        }





        if($posttype==2){
            $p['title']='自发货亚马逊总订单'.time();
            $p['title_list']  = [
                'amazon_order_id'=>'订单号',
                'title'=>'品名',
                'category'=>'类目',
                'user'=>'运营',
                'shop_name'=>'店铺',
                'spu'=>'spu',
                'custom_sku'=>'库存sku',
                'sku'=>'sku',
                'order_status'=>'订单状态',
                'amount'=>'订单金额',
                'tongan_deduct_request_num'=>'同安可用',
                'quanzhou_deduct_request_num'=>'泉州可用',
                'cloud_deduct_request_num'=>'云仓可用',
                'update_time'=>'创建时间',
            ];

            // $this->excel_expord($p);
            
            $p['data'] =   $list;
            $p['user_id'] =$params['find_user_id']??1;
            $p['type'] = '自发货亚马逊总订单-导出';
            $job = new \App\Http\Controllers\Jobs\ExcelTaskController;
            try {
                $job::runJob($p);
            } catch(\Exception $e){
                return $this->FailBack('加入队列失败');
            }
            return $this->SBack([],'加入队列成功');
        }

        $data['total_num'] = $totalNum;
        $data['data']      = $list;
        return $this->SBack($data);
    }

    //lazada订单
    public function LazadaOrdersM($params)
    {
        $pagenum = isset($params['page']) ? $params['page'] : 1;
        $limit   = isset($params['limit']) ? $params['limit'] : 30;

        $page = $pagenum - 1;
        if ($page != 0) {
            $page = $page * $limit;
        }
        $list = db::table('lazada_order_item');

        if (isset($params['order_id'])) {
            $list = $list->where('order_id', $params['order_id']);
        }
        if (isset($params['order_item_id'])) {
            $list = $list->where('order_item_id', $params['order_item_id']);
        }
        if (isset($params['shop_id'])) {
            $list = $list->where('shop_id', $params['shop_id']);
        }
        if (isset($params['area'])) {
            $list = $list->where('area', $params['area']);
        }

        if (isset($params['sku'])) {
            $list = $list->where('shop_sku', $params['sku']);
        }
        if (isset($params['status'])) {
            $list = $list->whereIn('status', $params['status']);
        }

        if (isset($params['order_status'])) {
            $list = $list->where('order_status', $params['order_status']);
        }


        $totalNum = $list->count();

        $list = $list
            ->offset($page)
            ->limit($limit)
            ->orderBy('created_at', 'desc')
            ->get();

        $orderItemCountList = db::table('lazada_order_item')
            ->groupBy('order_id')
            ->select(['order_id', Db::raw('count(id) as order_num'),])
            ->get()
            ->keyBy('order_id');

        foreach ($list as $v) {
            # code...
            $v->shop_name                   = $this->GetShop($v->shop_id)['shop_name'] ?? '';
            $skus                           = db::table('self_sku')->where('old_sku', $v->shop_sku)->first();
            $v->is_spu                      = true;
            $v->tongan_deduct_request_num   = 0;
            $v->quanzhou_deduct_request_num = 0;
            $v->cloud_deduct_request_num    = 0;
            $v->deposit_deduct_request_num  = 0;
            $v->spu                         = 'sku暂未录入系统，无法发货';
            $v->custom_sku                  = 'sku暂未录入系统，无法发货';
            $v->category                    = '';
            $v->custom_sku_id               = 0;
            $v->spu_id                      = 0;

            if (!$skus) {
                $v->is_spu = false;
            } else {
                if ($v->sku_id == 0) {
                    db::table('lazada_order_item')->where('id', $v->id)->update(['sku_id' => $skus->id, 'spu_id' => $skus->spu_id, 'custom_sku_id' => $skus->custom_sku_id]);
                }
                $customSku  = db::table('self_custom_sku')->where('id', $skus->custom_sku_id)->first();
                $bskspus    = db::table("self_spu")->where('id', $skus->spu_id)->first();
                $cate_one   = '';
                $cate_three = '';
                if (!$bskspus) {
                    $v->spu        = 'sku暂未录入系统，无法发货';
                    $v->custom_sku = 'sku暂未录入系统，无法发货';
                }
                if (isset($bskspus->three_cate_id)) {
                    $cate_three = $this->GetCategory($bskspus->three_cate_id)['name'] ?? '';
                }
                if (isset($bskspus->one_cate_id)) {
                    $cate_one = $this->GetCategory($bskspus->one_cate_id)['name'] ?? '';
                }


                $v->category      = $cate_one . $cate_three;
                $v->spu           = $skus->spu;
                $v->custom_sku    = $customSku->old_custom_sku ?? $skus->custom_sku;
                $v->custom_sku_id = $skus->custom_sku_id;
                $v->spu_id        = $skus->spu_id;
                // $v->tongan_deduct_request_num = $this->inventoryPlatformDetail($v->custom_sku_id,1,[3])[0]['intsock_num']??0;
                $v->quanzhou_deduct_request_num = $this->inventoryPlatformDetail($v->custom_sku_id, 2, [3])[0]['intsock_num'] ?? 0;
                // $v->cloud_deduct_request_num = $this->inventoryPlatformDetail($v->custom_sku_id,3,[3])[0]['intsock_num']??0;
                $v->deposit_deduct_request_num = 0;
            }

            $v->area_name = $this->area[$v->area];
            $v->is_cancel = 0;

            $fpx_del = db::table('fpx_del_order')->where('order_id', $v->order_item_id)->where('type', 3)->first();
            if ($fpx_del) {
                $v->is_cancel = 1;
            }
            $v->order_num = $orderItemCountList[$v->order_id]->order_num ?? 0;
        }

        $data['total_num'] = $totalNum;
        $data['data']      = $list;
        return $this->SBack($data);
        // if(isset($))
    }


    public function GetLazadaWaitOrderM($params)
    {
        $pagenum = isset($params['page']) ? $params['page'] : 1;
        $limit   = isset($params['limit']) ? $params['limit'] : 30;

        $page = $pagenum - 1;
        if ($page != 0) {
            $page = $page * $limit;
        }

        $list = db::table('lazada_order_item as a')->leftjoin('self_sku as b', 'a.shop_sku', '=', 'b.old_sku')->leftjoin('self_custom_sku as c', 'b.custom_sku_id', '=', 'c.id')->where('b.id', '>', 0)->where('a.status', 'packed')->select('a.*', 'b.spu_id', 'b.shop_id', 'c.id as custom_sku_id', 'c.custom_sku', 'c.color', 'c.size', 'c.spu', 'c.base_spu_id');

        if (isset($params['ids'])) {
            $list = $list->whereIn('a.id', $params['ids']);
        }

        $list = $list->get();

        $new_data = [];
        foreach ($list as $v) {
            # code...
            $v->quanzhou_deduct_request_num = $this->inventoryPlatformDetail($v->custom_sku_id, 2, [3])[0]['intsock_num'] ?? 0;
            if ($v->quanzhou_deduct_request_num < 1) {
                $new_data[] = $v;
            }

            $v->num     = [1];
            $v->name_en = '';

        }


        $nlist = [];

        foreach ($new_data as $k => $v) {
            # code...
            for ($i = $limit * ($pagenum - 1); $i < $limit * $pagenum; $i++) {
                # code...
                if ($k == $i) {
                    $nlist[] = $v;
                }
            }

        }

        foreach ($nlist as $v) {
            # code...
            $v->unit_price = 0;
            $base          = db::table('self_spu_info')->where('base_spu_id', $v->base_spu_id)->first();
            if ($base) {
                $v->unit_price = $base->unit_price;
            }

            $bskspus = db::table("self_spu")->where('id', $v->spu_id)->first();
            if (isset($bskspus->three_cate_id)) {
                $cate_three = $this->GetCategory($bskspus->three_cate_id)['name'] ?? '';
            }
            if (isset($bskspus->one_cate_id)) {
                $cate_one = $this->GetCategory($bskspus->one_cate_id)['name'] ?? '';
            }
            $v->category = $cate_one . $cate_three;

            $v->shop_name = $this->GetShop($v->shop_id)['shop_name'] ?? '';
            $v->order_num = 1;
            $v->class     = 2;

        }


        $data['total_num'] = count($new_data);
        $data['data']      = $nlist;
        return $this->SBack($data);
    }


    //1688订单
    public function AlibabaOrder($params)
    {
        if (isset($params['order_id'])) {
            $order_id = $params['order_id'];
            $list     = db::table('alibaba_order_item')->where('order_id', $order_id)->get();
            foreach ($list as $v) {

                $Logistics = json_decode($v->Logistics, true);
                if ($Logistics['type'] == 'success') {
                    if (isset($Logistics['data'][0])) {
                        $v->Logistics = $Logistics['data'][0];
                    } else {
                        $new_data['logisticsSteps'][0] = ['acceptTime' => '', 'remark' => '不需要物流'];
                        $v->Logistics                  = $new_data;
                    }

                } else {
                    $v->Logistics = $Logistics['msg'];
                }
                # code...
            }
        }
        if (isset($params['logistics_code'])) {
            $list = db::table('alibaba_order_item')->where('logisticsBillNo', $params['logistics_code'])->get(['order_id', 'Logistics']);
            foreach ($list as $v) {

                $Logistics = json_decode($v->Logistics, true);
                if ($Logistics['type'] == 'success') {
                    if (isset($Logistics['data'][0])) {
                        $v->Logistics = $Logistics['data'][0];
                    } else {
                        $new_data['logisticsSteps'][0] = ['acceptTime' => '', 'remark' => '不需要物流'];
                        $v->Logistics                  = $new_data;
                    }

                } else {
                    $v->Logistics = $Logistics['msg'];
                }
                # code...
            }
        }

        return $this->Sback($list);
    }

    //查询lazada店铺授权
    public function GetLazadaShopsM($params)
    {

        $pagenum = isset($params['page']) ? $params['page'] : 1;
        $limit   = isset($params['limit']) ? $params['limit'] : 30;

        $page = $pagenum - 1;
        if ($page != 0) {
            $page = $page * $limit;
        }

        $list = db::table('shop')->where('platform_id', 3);
        if (isset($params['shop_id'])) {
            $list = $list->where('Id', $params['shop_id']);
        }
        if (isset($params['shop_name'])) {
            $list = $list->where('shop_name', $params['shop_name']);
        }

        $totalNum = $list->count();
        $list     = $list->offset($page)->limit($limit)->select('shop_name', 'Id as shop_id')->get();
        foreach ($list as $v) {

            $area_token = [];
            foreach ($this->area as $ak => $av) {
                # code...
                $area_token[$ak]['area']        = $ak;
                $area_token[$ak]['area_name']   = $av;
                $area_token[$ak]['is_token']    = 0;
                $area_token[$ak]['is_expire']   = 0;
                $area_token[$ak]['token_day']   = 0;
                $area_token[$ak]['create_time'] = '';
            }

            $time = time();
            $area = db::table('lazada_token')->where('shop_id', $v->shop_id)->get();

            if ($area) {
                foreach ($area as $arv) {
                    # code...
                    // $area_token[$v->area]['']
                    if (!empty($arv->token)) {
                        $area_token[$arv->area]['is_token'] = 1;
                        if (($time - 86400 * 30) < strtotime($arv->create_time)) {
                            $lazadatime                          = strtotime($arv->create_time) + 86400 * 30;
                            $area_token[$arv->area]['is_expire'] = 1;
                            $area_token[$arv->area]['token_day'] = ceil(($lazadatime - $time) / 86400) - 1;
                        }
                        $area_token[$arv->area]['create_time'] = $arv->create_time;
                    }
                }

            }

            $area_token    = array_values($area_token);
            $v->area_token = $area_token;
            # code...
        }

        $data['total_num'] = $totalNum;
        $data['data']      = $list;
        return $this->SBack($data);
    }

    //更改lazada店铺授权
    public function UpdatelazadaTokenM($params)
    {
        $code     = $params['code'];
        $shop_id  = $params['shop_id'];
        $area     = $params['area'];
        $post_url = 'http://api.zity.cn/lazada/gettoken.php?code=' . $code;
        $post     = $this->curlPost($post_url);
        $post     = json_decode($post, true);
        if ($post['code'] == 1) {
            return $this->FailBack($post['msg']);
        } else {
            if ($area != $post['area']) {
                return $this->FailBack('该地区已授权地区不匹配，请重新授权');
            }
            $is['token']         = $post['msg'];
            $is['refresh_token'] = $post['refresh_token'];
            $is['create_time']   = date('Y-m-d H:i:s', time());
            $is['area']          = $area;
            $is['shop_id']       = $shop_id;

            $lazada_token = db::table('lazada_token')->where('shop_id', $shop_id)->where('area', $area)->first();
            if ($lazada_token) {
                db::table('lazada_token')->where('shop_id', $shop_id)->where('area', $area)->update($is);
            } else {
                db::table('lazada_token')->insert($is);
            }

        }
        return $this->SBack([], '授权成功');
    }


    //获取lazada地区
    public function GetLazadaAreaM()
    {
        $data = [];
        foreach ($this->area as $key => $value) {
            # code...
            $area['area']      = $key;
            $area['area_name'] = $value;
            $data[]            = $area;
        }

        return $this->SBack($data, '获取成功');
    }

    //获取lazada授权code
    public function GetlazadaCodeUrl()
    {
        $app_id = 119024;
        $url    = 'https://auth.lazada.com/oauth/authorize?response_type=code&force_auth=true&redirect_uri=https://www.zity.cn/lzd.php&client_id=' . $app_id;
        return $this->SBack(['url' => $url]);
    }


    public function GetFpxAmzonOrderListAllM($params)
    {
        $pagenum = isset($params['page']) ? $params['page'] : 1;
        $limit   = isset($params['limit']) ? $params['limit'] : 30;

        $page = $pagenum - 1;
        if ($page != 0) {
            $page = $page * $limit;
        }

        $list = db::table('amazon_order_item');
        if (isset($params['start_time']) && isset($params['end_time'])) {
            $start_time = $params['start_time'];
            $end_time   = $params['end_time'];
            $list       = $list->whereBetween('amazon_time', [$start_time, $end_time]);
        }

        // if(isset($params['fh_start_time'])&&isset($params['fh_end_time'])){
        //     $fh_start_time = $params['fh_start_time'];
        //     $fh_end_time = $params['fh_end_time']; 
        //     $list = $list->where('earliest_ship_date','>=',$fh_start_time);
        //     $list = $list->where('latest_ship_date','<=',$fh_end_time);
        // }

        // if(isset($params['sku'])){
        //     $skus = db::table('self_sku')->where('sku',$params['sku'])->orwhere('old_sku',$params['sku'])->get()->toArray();
        //     $sku_ids = array_column($skus,'id');
        //     $list = $list->whereIn('sku_id',$sku_ids);
        // }
        // if(isset($params['custom_sku'])){
        //     $cus = db::table('self_custom_sku')->where('custom_sku',$params['custom_sku'])->orwhere('old_custom_sku',$params['custom_sku'])->get()->toArray();
        //     $cus_ids = array_column($cus,'id');
        //     $list = $list->whereIn('custom_sku_id',$cus_ids);
        // }
        // if(isset($params['spu'])){
        //     $spus = db::table('self_spu')->where('spu',$params['spu'])->orwhere('old_spu',$params['spu'])->get()->toArray();
        //     $spu_ids = array_column($spus,'id');
        //     $list = $list->whereIn('spu_id',$spu_ids);
        // }

        if (isset($params['amazon_order_ids'])) {

            if (strstr($params['amazon_order_ids'], '，')) {
                $symbol = '，';
            } elseif (strstr($params['amazon_order_ids'], ',')) {
                $symbol = ',';
            } else {
                $symbol = '';
            }
            if ($symbol) {
                $ids = explode($symbol, $params['amazon_order_ids']);
            } else {
                $ids[0] = $params['amazon_order_ids'];
            }

            $list = $list->whereIn('amazon_order_id', $ids);

        }


        if (isset($params['amazon_order_id'])) {
            $list = $list->where('amazon_order_id', 'like', '%' . $params['amazon_order_id'] . '%');
        }

        // if(isset($params['shop_id'])){
        //     $list = $list->where('shop_id',$params['shop_id']);
        // }

        // if(isset($params['user_id'])){
        //     $list = $list->where('user_id',$params['user_id']);
        // }

        // $cancel_orders = db::table('amazon_order_cancel')->groupby('amazon_order_id')->select('amazon_order_id')->get()->toArray();
        // $cancel_order_ids = [];
        // if($cancel_orders){
        //     $cancel_order_ids = array_column( $cancel_orders,'amazon_order_id');
        // }

        // if(isset($params['status'])){
        // $fpx_order = db::table('fpx_order')->whereNotIn('status',[-1,0])->get()->toArray();
        // $fpx_ids = array_column( $fpx_order,'order_id');
        // $fpx_ids += $cancel_order_ids;
        // $list = $list->whereNotIn('amazon_order_id',$fpx_ids);
        // }

        $list     = $list->where('fulfillment_channel', 'MFN')->whereIn('order_status', array('Shipped', 'Pending', 'Unshipped'));
        $totalNum = $list->count();

        $list = $list
            ->offset($page)
            ->limit($limit)
            ->orderBy('amazon_time', 'desc')
            ->get();

        $customSkuIds = array_column($list->toarray(), 'custom_sku_id');

        //查询库存
        $inventoryArr = $this->getInventoryDetailByWlp($customSkuIds, [1, 2], [], [4, 5]);

        //查询云仓库存
        $cloudInventoryArr = db::table('self_custom_sku')->whereIn('id', $customSkuIds)->pluck('cloud_num', 'id');

        //查询云仓库存锁定情况
        $cloudInventoryLockArr = $this->batchInventoryDetailByWlp($customSkuIds, [3]);


        foreach ($list as $v) {
            $v->shop_name = $this->GetShop($v->shop_id)['shop_name'] ?? '';
            $v->user      = $this->GetUsers($v->user_id)['account'] ?? '';
            $cus          = db::table('self_custom_sku')->where('id', $v->custom_sku_id)->first();
            if (!$cus) {
                $bsks = db::table("self_sku")->where('sku', $v->seller_sku)->orwhere('old_sku', $v->seller_sku)->first();
                if ($bsks) {
                    $cus = db::table('self_custom_sku')->where('id', $bsks->custom_sku_id)->first();
                    if ($cus) {
                        db::table('amazon_order_item')->where('id', $v->id)->update(['spu_id' => $cus->spu_id, 'custom_sku_id' => $cus->id]);
                    }
                }

            }
            $v->custom_sku = '';
            $v->title      = '';
            if ($cus) {
                $v->custom_sku = $cus->custom_sku;
                if (!empty($v->old_custom_sku)) {
                    $v->custom_sku = $cus->old_custom_sku;
                }
                $v->title = $cus->name;
            }
            $spus      = db::table("self_spu")->where('id', $v->spu_id)->first();
            $v->is_spu = true;

            $v->category = '';
            if ($spus) {
                $v->spu      = $spus->spu;
                $cate_three  = $this->GetCategory($spus->three_cate_id)['name'];
                $cate_one    = $this->GetCategory($spus->one_cate_id)['name'];
                $v->category = $cate_one . $cate_three;
            } else {
                $bspus = db::table("self_spu")->where('spu', $v->spu)->orwhere('old_spu', $v->spu)->first();
                if ($bspus) {
                    db::table('amazon_order_item')->where('id', $v->id)->update(['spu_id' => $bspus->id]);
                    $cate_three  = $this->GetCategory($bspus->three_cate_id)['name'];
                    $cate_one    = $this->GetCategory($bspus->one_cate_id)['name'];
                    $v->category = $cate_one . $cate_three;
                    $v->spu_id   = $bspus->id;
                    $v->spu      = $bspus->spu;
                } else {
                    $bsks = db::table("self_sku")->where('sku', $v->seller_sku)->orwhere('old_sku', $v->seller_sku)->first();
                    if ($bsks) {
                        db::table('amazon_order_item')->where('id', $v->id)->update(['sku_id' => $bsks->id, 'custom_sku_id' => $bsks->custom_sku_id, 'spu_id' => $bsks->spu_id]);
                        $bskspus     = db::table("self_spu")->where('id', $bsks->spu_id)->first();
                        $cate_three  = $this->GetCategory($bskspus->three_cate_id)['name'];
                        $cate_one    = $this->GetCategory($bskspus->one_cate_id)['name'];
                        $v->category = $cate_one . $cate_three;
                        $v->spu_id   = $bskspus->id;
                        $v->spu      = $bsks->spu;
                    } else {
                        $v->is_spu = false;
                        $v->spu    = 'sku暂未录入系统，无法发货';
                    }
                }
            }


            // $v->tongan_deduct_request_num = $this->inventoryPlatformDetail($v->custom_sku_id,1,[5])[0]['intsock_num']??0;
            // // var_dump( $this->inventoryPlatformDetail($v->custom_sku_id,2,[5]));
            // $v->quanzhou_deduct_request_num = $this->inventoryPlatformDetail($v->custom_sku_id,2,[5])[0]['intsock_num']??0;
            // $v->cloud_deduct_request_num = $this->inventoryPlatformDetail($v->custom_sku_id,3,[5])[0]['intsock_num']??0;
            // $v->deposit_deduct_request_num = 0;


            //同安亚马逊可用库存
            $tongAnWalmartNum             = $inventoryArr[$v->custom_sku_id . '_1_5'] ?? ['usable_num' => 0];
            $v->tongan_deduct_request_num = $tongAnWalmartNum['usable_num'];

            //泉州亚马逊可用库存
            $quanzhouWalmartNum             = $inventoryArr[$v->custom_sku_id . '_2_5'] ?? ['usable_num' => 0];
            $v->quanzhou_deduct_request_num = $quanzhouWalmartNum['usable_num'];

            //同安可调库存
            $tongAnTunableNum      = $inventoryArr[$v->custom_sku_id . '_1_4'] ?? ['usable_num' => 0];
            $v->tongan_tunable_num = $tongAnTunableNum['usable_num'];

            //泉州沃尔玛可用库存
            $quanzhouTunableNum      = $inventoryArr[$v->custom_sku_id . '_2_4'] ?? ['usable_num' => 0];
            $v->quanzhou_tunable_num = $quanzhouTunableNum['usable_num'];

            //云仓库存
            $v->cloud_deduct_request_num = $cloudInventoryArr[$v->custom_sku_id] ?? 0;
            if ($v->cloud_deduct_request_num > 0 && isset($cloudInventoryLockArr[$v->custom_sku_id . '_3'])) {
                $cloudInventoryLockNum       = $cloudInventoryLockArr[$v->custom_sku_id . '_3'];
                $v->cloud_deduct_request_num -= ($cloudInventoryLockNum['lock_num'] + $cloudInventoryLockNum['shelf_num']);
            }

            // if($cus){
            //     $v->tongan_num = $cus->tongan_inventory;
            //     $v->quanzhou_num = $cus->quanzhou_inventory;
            //     $v->cloud_num =  $cus->cloud_num;
            //     $v->deposit_num = $cus->deposit_inventory;
            // }


            // $requestres = $this->lock_inventory([$v->custom_sku_id]);

            // $v->tongan_request_num = 0;
            // $v->quanzhou_request_num = 0;
            // $v->cloud_request_num = 0;
            // $v->deposit_request_num = 0;

            // if(isset($requestres[$v->custom_sku_id][1])){
            //     $v->tongan_request_num = $requestres[$v->custom_sku_id][1];
            // }
            // if(isset($requestres[$v->custom_sku_id][2])){
            //     $v->quanzhou_request_num = $requestres[$v->custom_sku_id][2];
            // }
            // if(isset($requestres[$v->custom_sku_id][3])){
            //     $v->cloud_request_num = $requestres[$v->custom_sku_id][3];
            // }
            // if(isset($requestres[$v->custom_sku_id][6])){
            //     $v->deposit_request_num = $requestres[$v->custom_sku_id][6];
            // }

            // // if($cus){
            // //     if ($cus->type == 2){
            // //         //                $gres = $this->GetGroupCustomSkuMinInventory($v->custom_sku);
            // //                         $gres = $this->GetGroupCustomSkuMinInventoryB($cus->custom_sku);
            // //                         $v->tongan_num = $gres['tongan_num'];
            // //                         $v->quanzhou_num =  $gres['quanzhou_num'];
            // //                         $v->cloud_num =  $gres['cloud_num'];
            // //                         $v->deposit_num =  $gres['deposit_num'];
            // //     }

            // // }


            // //预减=本地仓-计划
            // $v->tongan_deduct_request_num = $v->tongan_num - $v->tongan_request_num;
            // $v->quanzhou_deduct_request_num = $v->quanzhou_num - $v->quanzhou_request_num;
            // $v->cloud_deduct_request_num = $v->cloud_num - $v->cloud_request_num;
            // $v->deposit_deduct_request_num = $v->deposit_num - $v->deposit_request_num;


            // -2被废弃  -1 被取消  0待发货  1已预报   2已交接/已交货 3库内作业中 4已出库  5已关闭  ',

            $v->fpx_status = 0;
            $fpx_orders    = db::table('fpx_order')->where('order_id', $v->amazon_order_id)->first();
            if ($fpx_orders) {
                $v->fpx_status = $fpx_orders->status;
            }

            $fpx_del = db::table('fpx_del_order')->where('order_id', $v->amazon_order_id)->first();
            if ($fpx_del) {
                $v->fpx_status = -1;
            }

            $fpx_cancel = db::table('amazon_order_cancel')->where('amazon_order_id', $v->amazon_order_id)->first();
            if ($fpx_cancel) {
                $v->fpx_status = -2;
            }


            # code...
        }
        $data['total_num'] = $totalNum;
        $data['data']      = $list;
        return $this->SBack($data);
    }


    public function GetAlibabaQueueM()
    {

        $job  = new \App\Http\Controllers\Jobs\AlibabaJobController;
        $list = db::table('alibaba_order')->get();
        foreach ($list as $v) {
            $job::runJob(['order_id' => $v->order_id]);
        }

        return $this->SBack([], '加入队列成功');
    }


    public function FindOrder($params)
    {
        $data['order_id'] = $params['order_id'];
        $data['shop_id']  = $params['shop_id'];
        Redis::Lpush('orderlist', json_encode($data));
        return $this->SBack([]);
    }


    //成功返回
    public function SBack($data, $msg = '成功')
    {
        return ['type' => 'success', 'data' => $data, 'msg' => $msg];
    }

    //失败返回
    public function FailBack($msg)
    {
        return ['type' => 'fail', 'data' => [], 'msg' => $msg];
    }

    //获取当前毫秒
    function getMillisecond()
    {

        list($microsecond, $time) = explode(' ', microtime()); //' '中间是一个空格

        return (float)sprintf('%.0f', (floatval($microsecond) + floatval($time)) * 1000);

    }

    //超出字符数隐藏
    function cutSubstr($str, $len = 30)
    {
        if (strlen($str) > $len) {
            $str = substr($str, 0, $len) . '...';
        }
        return $str;
    }

    public function getFpx($params)
    {
        $key    = $this->key;
        $method = $params['method'];
        $secret = $this->secret;
        $time   = $this->getMillisecond();
        $vesion = $params['vesion'];

        $data = $params['data'];
        $body = json_encode($data);
        $body =
        $signs = "app_key{$key}formatjsonmethod{$method}timestamp{$time}v{$vesion}{$body}{$secret}";

        $sign = md5($signs);
        $url  = "https://open.4px.com/router/api/service?app_key={$key}&format=json&method={$method}&timestamp={$time}&v={$vesion}&sign={$sign}";

        $post = $this->curlPost($url, $data, 'json');

        return $post;
    }


    public function AliBaBa($params)
    {

        $order_id  = $params['order_id'];
        $url       = 'http://gw.open.1688.com/openapi/';//1688开放平台使用gw.open.1688.com域名
        $appSecret = 'yFb51kJvdxt';
        $apiInfo   = 'param2/1/com.alibaba.trade/alibaba.trade.get.buyerView/9051611';


        //配置参数，请用apiInfo对应的api参数进行替换
        // $code_arr = array(
        //     'orderId' => $order_id,
        //     'webSite' => '1688',
        //     'access_token'=>'58fd1442-0350-4e70-a3ff-684509272df3'
        // );
        $code_arr  = array(
            'orderId'      => $order_id,
            'webSite'      => '1688',
            'access_token' => '58fd1442-0350-4e70-a3ff-684509272df3'
        );
        $aliParams = array();
        foreach ($code_arr as $key => $val) {
            $aliParams[] = $key . $val;
        }
        sort($aliParams);
        $sign_str  = join('', $aliParams);
        $sign_str  = $apiInfo . $sign_str;
        $code_sign = strtoupper(bin2hex(hash_hmac("sha1", $sign_str, $appSecret, true)));

        $data                   = $code_arr;
        $data['_aop_signature'] = $code_sign;
        $post_url               = $url . $apiInfo;

        $post_url = $post_url . '?' . http_build_query($data, '', '&');
        $post     = $this->curlPost($post_url);

        $post = json_decode($post, true);

        if (isset($post['success'])) {
            $returndata = $post['result'];
            return $this->SBack($returndata);
        } else {
            return $this->FailBack($post['error_message']);
        }

    }


    public function AliBaBaLogistics($params)
    {

        $order_id  = $params['order_id'];
        $url       = 'http://gw.open.1688.com/openapi/';//1688开放平台使用gw.open.1688.com域名
        $appSecret = 'yFb51kJvdxt';
        $apiInfo   = 'param2/1/com.alibaba.logistics/alibaba.trade.getLogisticsTraceInfo.buyerView/9051611';


        //配置参数，请用apiInfo对应的api参数进行替换
        // $code_arr = array(
        //     'orderId' => $order_id,
        //     'webSite' => '1688',
        //     'access_token'=>'58fd1442-0350-4e70-a3ff-684509272df3'
        // );
        $code_arr  = array(
            'orderId'      => $order_id,
            'webSite'      => '1688',
            'access_token' => '58fd1442-0350-4e70-a3ff-684509272df3'
        );
        $aliParams = array();
        foreach ($code_arr as $key => $val) {
            $aliParams[] = $key . $val;
        }
        sort($aliParams);
        $sign_str  = join('', $aliParams);
        $sign_str  = $apiInfo . $sign_str;
        $code_sign = strtoupper(bin2hex(hash_hmac("sha1", $sign_str, $appSecret, true)));

        $data                   = $code_arr;
        $data['_aop_signature'] = $code_sign;
        $post_url               = $url . $apiInfo;

        $post_url = $post_url . '?' . http_build_query($data, '', '&');
        $post     = $this->curlPost($post_url);

        $post = json_decode($post, true);

        if (isset($post['success'])) {
            $returndata = $post['logisticsTrace'] ?? [];
            return $this->SBack($returndata);
        } else {
            return $this->FailBack($post['error_message']);
        }

    }

    public function ManualAddOrderM($params)
    {
        if (empty($params['amazon_order_id'])) {
            return $this->FailBack('亚马逊订单号 必填');
        }

        if (empty($params['shop_id'])) {
            return $this->FailBack('店铺 必填');
        }

        if (empty($params['quantity_ordered'])) {
            return $this->FailBack('数量 必填');
        }

        if (empty($params['seller_sku'])) {
            return $this->FailBack('平台sku 必填');
        }

        $selfSku = db::table("self_sku")->where('sku', $params['seller_sku'])->orwhere('old_sku', $params['seller_sku'])->first();
        if (!$selfSku) {
            return $this->FailBack('平台sku 未绑定');
        }

        $selfCustomSku = db::table('self_custom_sku')->where('id', $selfSku->custom_sku_id)->first();
        if (!$selfCustomSku) {
            return $this->FailBack('没有该custom_sku');
        }


        $insert = [
            'amazon_order_id'     => $params['amazon_order_id'],
            'shop_id'             => $params['shop_id'],
            'quantity_ordered'    => $params['quantity_ordered'],
            'seller_sku'          => $params['seller_sku'],
            'order_item_id'       => isset($params['order_item_id']) ?
                $params['order_item_id'] : $params['amazon_order_id'] . '-' . rand(1, 10),
            'sku_id'              => $selfSku->id,
            'custom_sku_id'       => $selfCustomSku->id,
            'spu_id'              => $selfCustomSku->spu_id,
            'fulfillment_channel' => 'MFN',
            'order_status'        => 'Pending',
            'amazon_time'         => date('Y-m-d H:i:s'),
            'add_time'            => time(),
            'update_time'         => time(),
        ];

        DB::table('amazon_order_item')->insert($insert);

        return $this->SBack([]);
    }

    /**
     * 传入数组进行HTTP POST请求
     */
    function curlPost($url, $post_data = array(), $data_type = "")
    {
        $timeout = 5;
        $header  = empty($header) ? '' : $header;
        //支持json数据数据提交
        if ($data_type == 'json') {
            $post_string = json_encode($post_data);
        } elseif ($data_type == 'array') {
            $post_string = $post_data;
        } elseif (is_array($post_data)) {
            $post_string = http_build_query($post_data, '', '&');
        }

        $ch = curl_init();    // 启动一个CURL会话
        curl_setopt($ch, CURLOPT_URL, $url);     // 要访问的地址
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  // 对认证证书来源的检查   // https请求 不验证证书和hosts
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);  // 从证书中检查SSL加密算法是否存在
        // curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
        //curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
        //curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        curl_setopt($ch, CURLOPT_POST, true); // 发送一个常规的Post请求
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);     // Post提交的数据包
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);     // 设置超时限制防止死循环
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        //curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     // 获取的信息以文件流的形式返回 
        // curl_setopt($ch, CURLOPT_HTTPHEADER, $header); //模拟的header头
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($post_string))
        );

        $result = curl_exec($ch);

        // 打印请求的header信息
        //$a = curl_getinfo($ch);
        //var_dump($a);

        curl_close($ch);
        return $result;
    }

    public function bindLazadaSku($params)
    {
        $customSku = db::table('self_custom_sku')
            ->where('id', $params['custom_sku_id'])
            ->first();
        if (empty($customSku)) {
            throw new \Exception('无库存sku数据，请检查！');
        }
        $spu = db::table('self_spu')
            ->where('id', $customSku->spu_id)
            ->first();
        if (empty($spu)) {
            throw new \Exception('无spu数据，请检查！');
        }
        $shop = db::table('shop')
            ->where('Id', $params['shop_id'])
            ->first();
        if (empty($shop)) {
            throw new \Exception('无店铺数据，请检查！');
        }
        $user = $this->GetUsers($params['operatior_id']);
        if (empty($user)) {
            throw new \Exception('运营人员不存在，请检查！');
        }
        $sku = $shop->identifying . $shop->Id . '-' . $customSku->custom_sku;
        if (empty($sku)) {
            throw new \Exception('sku生成失败！');
        }
        $skuMdl = db::table('self_sku')
            ->where('sku', $sku)
            ->first();
        if (!empty($skuMdl)) {
            throw new \Exception('已生成过该sku：' . $sku . '请勿重复生成！');
        }
        $skuId = db::table('self_sku')
            ->insertGetId([
                'sku'             => $sku,
                'old_sku'         => $params['lazada_sku'],
                'shop_id'         => $shop->Id,
                'custom_sku'      => $customSku->old_custom_sku ?? $customSku->custom_sku,
                'custom_sku_id'   => $customSku->id,
                'user_id'         => $params['operatior_id'],
                'status'          => 1,
                'create_time'     => date('Y-m-d H:i:s'),
                'operate_user_id' => $params['user_id'],
                'spu'             => $spu->spu,
            ]);
        if (empty($skuId)) {
            throw new \Exception('保存sku信息失败！');
        }

        return ['code' => 200, 'msg' => '绑定成功！sku:' . $sku];
    }

    //批量自动发货
    public function AutoDeliverGoods($params)
    {
        $job = new FpxJob($params);
        $job->dispatch($params)->onQueue('fpx');


        $orderIds = $params['order_ids'];
        $orderIds = array_unique(explode(',', $orderIds));

        $type = $params['type'];//类型：1.亚马逊 2.沃尔玛 3.lazada

        switch ($type) {
            case 1:
                $orderList = DB::table('lingxing_amazon_order')
                    ->select('id', 'amazon_order_id as order_id', 'shop_id')
                    ->whereIn('id', $orderIds)
                    ->get();

                break;
            case 2:
                $orderList = DB::table('walmart_order')
                    ->select('id', 'purchase_order_id as order_id', 'shop_id')
                    ->whereIn('id', $orderIds)
                    ->get();

                break;
            default:
                $orderList = [];
        }

        $date = date('Y-m-d H:i:s');

        foreach ($orderList as $order) {
            DB::table('auto_shipments_log')->insert([
                'local_order_id' => $order->id,
                'order_id'       => $order->order_id,
                'shop_id'        => $order->shop_id,
                'type'           => $params['type'],
                'warehouse_id'   => $params['warehouse_id'],
                'template_id'    => $params['template_id'],
                'user_id'        => $params['user_id'],
                'create_time'    => $date,
                'update_time'    => $date,
            ]);

        }

        return $this->SBack([], '加入队列成功');
    }

    //自动发货日志
    public function GetAutoShipmentsLog($params)
    {
        $pagenum = isset($params['page']) ? $params['page'] : 1;
        $limit   = isset($params['limit']) ? $params['limit'] : 30;


        $page = $pagenum - 1;
        if ($page != 0) {
            $page = $page * $limit;
        }

        $query = db::table('auto_shipments_log');

        if (isset($params['order_ids'])) {
            if (strstr($params['order_ids'], '，')) {
                $symbol = '，';
            } elseif (strstr($params['order_ids'], ',')) {
                $symbol = ',';
            } else {
                $symbol = '';
            }
            if ($symbol) {
                $ids = explode($symbol, $params['order_ids']);
            } else {
                $ids[0] = $params['order_ids'];
            }

            $query->whereIn('order_id', $ids);
        }

        if (isset($params['order_id'])) {
            $query->where('order_id', 'like', '%' . $params['order_id'] . '%');
        }

        if (isset($params['type'])) {
            $query->where('type', $params['type']);
        }

        if (isset($params['status'])) {
            $query->where('status', $params['status']);
        }

        if (isset($params['warehouse_id'])) {
            $query->where('warehouse_id', $params['warehouse_id']);
        }

        if (isset($params['start_time']) && isset($params['end_time'])) {
            $start_time = $params['start_time'];
            $end_time   = $params['end_time'];
            $query->whereBetween('create_time', [$start_time . ' 00:00:00', $end_time . ' 23:59:59']);
        }

        $totalNum = $query->count();
        if ($totalNum == 0) {
            return $this->SBack(['data' => [], 'total_num' => 0]);
        }

        $list = $query->offset($page)->limit($limit)->orderBy('id', 'desc')->get();

        $statusName    = ['0' => '默认', '1' => '成功', '2' => '失败'];
        $warehouseName = ['1' => '同安仓', '2' => '泉州仓'];
        $typeName      = ['1' => '亚马逊', '2' => '沃尔玛'];
        $templateName  = DB::table('fpx_template')->pluck('name', 'id');

        foreach ($list as $value) {
            $value->status_name    = $statusName[$value->status] ?? '';
            $value->warehouse_name = $warehouseName[$value->warehouse_id] ?? '';
            $value->type_name      = $typeName[$value->type] ?? '';
            $value->template_name  = $templateName[$value->template_id] ?? '';

            $value->shop_name = $this->GetShop($value->shop_id)['shop_name'] ?? '';
            $value->user_name = $value->user_id ? ($this->GetUsers($value->user_id)['account'] ?? '') : '';
        }


        $data['total_num'] = $totalNum;
        $data['data']      = $list;
        return $this->SBack($data);
    }
}