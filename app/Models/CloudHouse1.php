<?php

namespace App\Models;

use App\Models\ResourceModel\CloudhouseCustomSkuModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use App\Models\ResourceModel\ContractModel;
use App\Models\ResourceModel\CustomSkuModel;
use App\Models\ResourceModel\GoodsTransfersModel;
use App\Models\ResourceModel\GoodTransfersDetailModel;
use App\Models\ResourceModel\SpuModel;
class CloudHouse1 extends BaseModel
{
    const WAREHOUSE = [
        1 => "同安仓",
        2 => "泉州仓",
        3 => "云仓",
        4 => "工厂虚拟仓",
        5 => "其他"
    ];
    protected $contractModel;
    protected $goodsTransfersModel;
    protected $goodsTransfersDetailModel;
    protected $contractSkuModel;
    protected $customSkuModel;
    protected $spuModel;
    protected $saiheWarehouseModel;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->contractModel = new ContractModel();
        $this->goodsTransfersModel = new GoodsTransfersModel();
        $this->goodsTransfersDetailModel = new GoodTransfersDetailModel();
        $this->contractSkuModel = new CloudhouseCustomSkuModel();
        $this->customSkuModel = new CustomSkuModel();
        $this->spuModel = new SpuModel();
        $this->saiheWarehouseModel = new SaiheWarehouse();
    }

    public function ContractImport($params){
        ////获取Excel文件数据
        $file = $params['file'];

        $contract_no = $params['contract_no'];
        $is_repeat = DB::table('cloudhouse_contract')->where('contract_no',$contract_no)->first();
//        if($is_repeat){
//            return [
//                'type' => 'fail',
//                'msg' => '导入失败，合同号重复'
//            ];
//        }

        $user_id = $params['user_id'];
        $sign_date = $params['sign_date'];

        //获取文件后缀名
        $extension = $file->getClientOriginalExtension();
        if ($extension == 'csv') {
            $PHPExcel = new \PHPExcel_Reader_CSV();
        } elseif ($extension == 'xlsx') {
            $PHPExcel = new \PHPExcel_Reader_Excel2007();
        } else {
            $PHPExcel = new \PHPExcel_Reader_Excel5();
        }

        if (!$PHPExcel->canRead($file)) {
            return [
                'type' => 'fail',
                'msg' => '导入失败，Excel文件错误'
            ];
        }

        $PHPExcelLoad = $PHPExcel->load($file);
        $Sheet = $PHPExcelLoad->getSheet(0);
        $add_list = array();
        /**取得一共有多少行*/
        $allRow = $Sheet->getHighestRow();

        //获取列数
        for ($i=0; $i < 30; $i++) {
            $res = trim($Sheet->getCellByColumnAndRow($i, 1)->getValue());
            if($res=='下单颜色'){
                $start = $i;
            }
            if($res=='合计'){
                $end = $i;
            }
        }


        //获取尺码标题
        $sizes = array();

        for ($si=$start+1; $si < $end; $si++) {
            $sizes[$si] = trim($Sheet->getCellByColumnAndRow($si, 2)->getValue());
        }

        for ($j = 3; $j <= $allRow; $j++) {

            $spu = trim($Sheet->getCellByColumnAndRow(0, $j)->getValue());
            if($spu){
                $add_list[$j - 3]['spu'] = $spu;
                $color = trim($Sheet->getCellByColumnAndRow(2, $j)->getValue());

                try {
                    //code...
                    $colors = explode('/', $color);
                    $add_list[$j - 3]['color_name'] = $colors[0];
                    $add_list[$j - 3]['color_identifying'] = $colors[1];
                } catch (\Throwable $th) {
                    //throw $th;
                    return [
                        'type' => 'fail',
                        'msg' => 's'.$spu.'解析颜色错误，请检查颜色列数是否为第三列，且中英文是否用/符号隔开'.$color
                    ];
                }

                $color_identifying = Db::table('self_color_size')->where('identifying',$add_list[$j - 3]['color_identifying'])->select('identifying')->first();
                if(!$color_identifying){
                    return [
                        'type' => 'fail',
                        'msg' => '无此颜色，请检查是否填写错误'.$color
                    ];
                }
                foreach ($sizes as $key => $value) {
                    # code...
                    //尺码
                    $add_list[$j - 3][$value] = (int)trim($Sheet->getCellByColumnAndRow($key, $j)->getValue());

                    if($add_list[$j - 3][$value]>0){
                        //如果尺码大于0
                        //查询库存sku 并将其存入库存sku表
                        $spus = Db::table('self_spu')->where('spu',$add_list[$j - 3]['spu'])->orwhere('old_spu',$add_list[$j - 3]['spu'])->select('spu','old_spu')->first();
                        if(!$spus){
                            return [
                                'type' => 'fail',
                                'msg' => '无此spu，请检查是否填写错误'.$add_list[$j - 3]['spu']
                            ];
                        }else{
                            //                         DB::connection()->enableQueryLog();

                            $new_old_spu = $this->GetSpus($spus->spu);

                            $customres = Db::table('self_custom_sku')->where('color',$color_identifying->identifying)->where('size',$value)->whereIn('spu',$new_old_spu)->select('custom_sku')->first();

                            //                         print_r(DB::getQueryLog());
                            if(!$customres){
                                return [
                                    'type' => 'fail',
                                    'msg' => '无此库存sku:'.$add_list[$j - 3]['spu'].'--color:'.$color_identifying->identifying.'--size:'.$value
                                ];
                            }
                        }

                    }
                }

            }
        }


        $customskus = array();
        $spulist = array();
        $total_add = array();
        $total_price = 0;
        $total_count = 0;
        for ($j = 3; $j <= $allRow; $j++) {
            $spu = trim($Sheet->getCellByColumnAndRow(0, $j)->getValue());
            if($spu){
                $spulist[] = $spu;
                //款号
                $add_list[$j - 3]['spu'] =$spu;
                //名字
                $add_list[$j - 3]['title'] = trim($Sheet->getCellByColumnAndRow(1, $j)->getValue());
                //颜色
                $colors = trim($Sheet->getCellByColumnAndRow(2, $j)->getValue());

                try {
                    //code...
                    $colors = explode('/', $colors);
                    $add_list[$j - 3]['color_name'] = $colors[0];
                    $add_list[$j - 3]['color_identifying'] = $colors[1];
                } catch (\Throwable $th) {
                    //throw $th;
                    return [
                        'type' => 'fail',
                        'msg' => 's'.$spu.'解析颜色错误，请检查颜色列数是否为第三列，且中英文是否用/符号隔开'.$spu.$add_list[$j - 3]['color']
                    ];
                }

                $color_identifying = Db::table('self_color_size')->where('identifying',$add_list[$j - 3]['color_identifying'])->select('identifying')->first();
                if(!$color_identifying){
                    return [
                        'type' => 'fail',
                        'msg' => '无此颜色，请检查是否填写错误'.$add_list[$j - 3]['color']
                    ];
                }

                $count = 0;
                db::beginTransaction();    //开启事务
                foreach ($sizes as $key => $value) {
                    # code...
                    //尺码
                    $add_list[$j - 3][$value] = (int)trim($Sheet->getCellByColumnAndRow($key, $j)->getValue());

                    if($add_list[$j - 3][$value]>0){
                        //如果尺码大于0
                        //查询库存sku 并将其存入库存sku表
                        $spus = Db::table('self_spu')->where('spu',$add_list[$j - 3]['spu'])->orwhere('old_spu',$add_list[$j - 3]['spu'])->select('spu','old_spu')->first();
                        if(!$spus){
                            return [
                                'type' => 'fail',
                                'msg' => '无此spu，请检查是否填写错误'.$add_list[$j - 3]['spu']
                            ];
                        }else{
                            // DB::connection()->enableQueryLog();

                            $new_old_spu = $this->GetSpus($spus->spu);

                            $customres = Db::table('self_custom_sku')->where('color',$color_identifying->identifying)->where('size',$value)->whereIn('spu',$new_old_spu)->select('custom_sku','old_custom_sku','spu_id','id')->first();
                            // print_r(DB::getQueryLog());
                            if(!$customres){
                                db::rollback();// 回调
                                return [
                                    'type' => 'fail',
                                    'msg' => '无此库存sku:'.$add_list[$j - 3]['spu'].$add_list[$j - 3][$value].'--size:'.$value
                                ];
                            }else{
                                $customskus[] = $customres->custom_sku;
                                $cusinsert['spu_id'] = $customres->spu_id;
                                $cusinsert['custom_sku_id'] = $customres->id;
                                $cusinsert['spu'] = $add_list[$j - 3]['spu'];
                                $cusinsert['size'] = $value;
                                $cusinsert['num'] = $add_list[$j - 3][$value];
                                $cusinsert['color'] = $color_identifying->identifying;
                                $cusinsert['custom_sku'] = $customres->old_custom_sku??$customres->custom_sku;
                                $cusinsert['contract_no'] = $contract_no;
                                $cusinsert['color_name'] = $add_list[$j - 3]['color_name'];
                                Db::table('cloudhouse_custom_sku')->insert($cusinsert);
                            }
                        }

                    }
                    $count+= $add_list[$j - 3][$value];
                }
                db::commit();
                //合计
                $add_list[$j - 3]['count'] =  $count;
                //单价
                $price = trim($Sheet->getCellByColumnAndRow($end+1, $j)->getValue());
                $add_list[$j - 3]['one_price'] = $price;
                //金额
                $add_list[$j - 3]['price'] =  round($count*$add_list[$j - 3]['one_price'],4);
                //品牌
                $add_list[$j - 3]['brand'] = trim($Sheet->getCellByColumnAndRow($end+3, $j)->getValue());
                //交货期
                $date = $Sheet->getCellByColumnAndRow($end+4, $j)->getValue();
                // $add_list[$j - 3]['report_date'] = $Sheet->getCellByColumnAndRow($end+4, $j)->getValue();
                if (is_numeric($date)){
                    $t1 = intval(($date - 25569) * 3600 * 24); //转换成1970年以来的秒数
                    $date = gmdate('Y-m-d',$t1);
                }
                $add_list[$j - 3]['spu_id'] = $customres->spu_id;
                $add_list[$j - 3]['report_date'] = $date;
                $add_list[$j - 3]['contract_no'] =  $contract_no;
                $add_list[$j - 3]['type'] =  1;
                $add_list[$j - 3]['status'] =  1;
                $add_list[$j - 3]['create_user'] =  $user_id;
                $add_list[$j - 3]['sign_date'] =  $sign_date;
                $add_list[$j - 3]['create_time'] = date('Y-m-d H:i:s',time());

                $total_price += $add_list[$j - 3]['price'];
                $total_count += $count;
            }
        }
        $spulist = array_unique($spulist);
        $total_add['spu_count'] = count($spulist);
        $total_add['sign_date'] = $sign_date;
        $total_add['contract_no'] = $contract_no;
        $total_add['report_date'] = $date;
        $total_add['create_time'] = date('Y-m-d H:i:s');
        $total_add['total_price'] = $total_price;
        $total_add['total_count'] = $total_count;


        $cloud = new \App\Http\Controllers\Jobs\CloudHouseController();
        try {
            $cloud::sendJob($customskus);
        } catch(\Exception $e){
            Redis::Lpush('send_goods_fail',json_encode($params));
        }
        $res = Db::table('cloudhouse_contract')->insert($add_list);
        $total_res = Db::table('cloudhouse_contract_total')->insert($total_add);
        return ['type'=>'success','msg'=>'导入成功'];
    }


    public function ContractOutImport($params){
        ////获取Excel文件数据
        $file = $params['file'];

        //获取文件后缀名
        $extension = $file->getClientOriginalExtension();
        if ($extension == 'csv') {
            $PHPExcel = new \PHPExcel_Reader_CSV();
        } elseif ($extension == 'xlsx') {
            $PHPExcel = new \PHPExcel_Reader_Excel2007();
        } else {
            $PHPExcel = new \PHPExcel_Reader_Excel5();
        }

        if (!$PHPExcel->canRead($file)) {
            return [
                'type' => 'fail',
                'msg' => '导入失败，Excel文件错误'
            ];
        }

        $PHPExcelLoad = $PHPExcel->load($file);
        $Sheet = $PHPExcelLoad->getSheet(0);
        $add_list = array();
        /**取得一共有多少行*/
        $allRow = $Sheet->getHighestRow();



        for ($j = 2; $j <= $allRow; $j++) {
            if(!$Sheet->getCellByColumnAndRow(0, $j)->getValue()){
                continue;
            }
            $add_list[$j - 2]['contract_no'] = trim($Sheet->getCellByColumnAndRow(0, $j)->getValue());
            $add_list[$j - 2]['spu'] = trim($Sheet->getCellByColumnAndRow(1, $j)->getValue());

            if($params['type']=='shipment'){
                $add_list[$j - 2]['brand'] = trim($Sheet->getCellByColumnAndRow(2, $j)->getValue());
                $add_list[$j - 2]['box_id'] = trim($Sheet->getCellByColumnAndRow(4, $j)->getValue());
                $colors = trim($Sheet->getCellByColumnAndRow(3, $j)->getValue());
                $add_list[$j - 2]['XXS'] = (int)trim($Sheet->getCellByColumnAndRow(5, $j)->getValue());
                $add_list[$j - 2]['XS'] = (int)trim($Sheet->getCellByColumnAndRow(6, $j)->getValue())??0;
                $add_list[$j - 2]['S'] = (int)trim($Sheet->getCellByColumnAndRow(7, $j)->getValue())??0;
                $add_list[$j - 2]['M'] = (int)trim($Sheet->getCellByColumnAndRow(8, $j)->getValue())??0;
                $add_list[$j - 2]['L'] = (int)trim($Sheet->getCellByColumnAndRow(9, $j)->getValue())??0;
                $add_list[$j - 2]['XL'] = (int)trim($Sheet->getCellByColumnAndRow(10, $j)->getValue())??0;
                $add_list[$j - 2]['2XL'] = (int)trim($Sheet->getCellByColumnAndRow(11, $j)->getValue())??0;
                $add_list[$j - 2]['3XL'] = (int)trim($Sheet->getCellByColumnAndRow(12, $j)->getValue())??0;
                $add_list[$j - 2]['4XL'] = (int)trim($Sheet->getCellByColumnAndRow(13, $j)->getValue())??0;
                $add_list[$j - 2]['5XL'] = (int)trim($Sheet->getCellByColumnAndRow(14, $j)->getValue())??0;
                $add_list[$j - 2]['6XL'] = (int)trim($Sheet->getCellByColumnAndRow(15, $j)->getValue())??0;
                $add_list[$j - 2]['7XL'] = (int)trim($Sheet->getCellByColumnAndRow(16, $j)->getValue())??0;
                $add_list[$j - 2]['8XL'] = (int)trim($Sheet->getCellByColumnAndRow(17, $j)->getValue())??0;
            }
            if($params['type']=='production'){
                $colors = trim($Sheet->getCellByColumnAndRow(2, $j)->getValue());
                $add_list[$j - 2]['XXS'] = (int)trim($Sheet->getCellByColumnAndRow(4, $j)->getValue());
                $add_list[$j - 2]['XS'] = (int)trim($Sheet->getCellByColumnAndRow(5, $j)->getValue())??0;
                $add_list[$j - 2]['S'] = (int)trim($Sheet->getCellByColumnAndRow(6, $j)->getValue())??0;
                $add_list[$j - 2]['M'] = (int)trim($Sheet->getCellByColumnAndRow(7, $j)->getValue())??0;
                $add_list[$j - 2]['L'] = (int)trim($Sheet->getCellByColumnAndRow(8, $j)->getValue())??0;
                $add_list[$j - 2]['XL'] = (int)trim($Sheet->getCellByColumnAndRow(9, $j)->getValue())??0;
                $add_list[$j - 2]['2XL'] = (int)trim($Sheet->getCellByColumnAndRow(10, $j)->getValue())??0;
                $add_list[$j - 2]['3XL'] = (int)trim($Sheet->getCellByColumnAndRow(11, $j)->getValue())??0;
                $add_list[$j - 2]['4XL'] = (int)trim($Sheet->getCellByColumnAndRow(12, $j)->getValue())??0;
                $add_list[$j - 2]['5XL'] = (int)trim($Sheet->getCellByColumnAndRow(13, $j)->getValue())??0;
                $add_list[$j - 2]['6XL'] = (int)trim($Sheet->getCellByColumnAndRow(14, $j)->getValue())??0;
                $add_list[$j - 2]['7XL'] = (int)trim($Sheet->getCellByColumnAndRow(15, $j)->getValue())??0;
                $add_list[$j - 2]['8XL'] = (int)trim($Sheet->getCellByColumnAndRow(16, $j)->getValue())??0;
            }


            try {
                //code...
                $colorss = explode('/', $colors);
                $add_list[$j - 2]['color_name'] = $colorss[0];
                $add_list[$j - 2]['color_identifying'] = $colorss[1];
            } catch (\Throwable $th) {
                //throw $th;
                return [
                    'type' => 'fail',
                    'msg' =>  $colors.'颜色异常'.$j
                ];
            }

            // try {
            //     //code...
            //     $color_en = explode('/',$add_list[$j - 2]['color'])[1];
            // } catch (\Throwable $th) {
            //     //throw $th;
            //     return [
            //         'type' => 'fail',
            //         'msg' =>  $add_list[$j - 2]['color'].'颜色异常'.$j
            //     ];
            // }
            // echo $color_en;
            $res = Db::table('cloudhouse_contract')->where('contract_no',$add_list[$j - 2]['contract_no'])->where('spu',$add_list[$j - 2]['spu'])->Where('color_identifying',$add_list[$j - 2]['color_identifying'])->first();

            $query['spu'] = $add_list[$j - 2]['spu'];
            $query['color'] = $colors;
            $query['contract_no'] = $add_list[$j - 2]['contract_no'];

            if(!$res){
                return [
                    'type' => 'fail',
                    'msg' => '导入失败，无此合同或颜色或spu--spu:'.$query['spu'] .'--合同号：'.$query['contract_no'].'--颜色：'.$query['color']
                ];
            }
            // $add_list[$j - 2]['color'] = $res->color;
        }

        $add_list = array_values($add_list);

        return ['type'=>'success','msg'=>'导入成功','data'=>$add_list];

    }





    //走柜
    public function ContractZgImport($params){
        ////获取Excel文件数据
        $file = $params['file'];

        //获取文件后缀名
        $extension = $file->getClientOriginalExtension();
        if ($extension == 'csv') {
            $PHPExcel = new \PHPExcel_Reader_CSV();
        } elseif ($extension == 'xlsx') {
            $PHPExcel = new \PHPExcel_Reader_Excel2007();
        } else {
            $PHPExcel = new \PHPExcel_Reader_Excel5();
        }

        if (!$PHPExcel->canRead($file)) {
            return [
                'type' => 'fail',
                'msg' => '导入失败，Excel文件错误'
            ];
        }

        $PHPExcelLoad = $PHPExcel->load($file);
        $Sheet = $PHPExcelLoad->getSheet(0);
        $add_list = array();
        /**取得一共有多少行*/
        $allRow = $Sheet->getHighestRow();



        for ($j = 3; $j <= $allRow; $j++) {
            $add_list[$j - 3]['contract_no'] = trim($Sheet->getCellByColumnAndRow(1, $j)->getValue());
            $add_list[$j - 3]['custom_sku'] = trim($Sheet->getCellByColumnAndRow(2, $j)->getValue());
            // $add_list[$j - 2]['size'] = trim($Sheet->getCellByColumnAndRow(6, $j)->getValue());
            $add_list[$j - 3]['num'] = trim($Sheet->getCellByColumnAndRow(9, $j)->getValue());
            $add_list[$j - 3]['box_num'] = trim($Sheet->getCellByColumnAndRow(10, $j)->getValue());

            $cus = Db::table('self_custom_sku')->where('custom_sku',$add_list[$j - 3]['custom_sku'])->first();
            if(!$cus){
                return [
                    'type' => 'fail',
                    'msg' => '导入失败,无此库存sku'.$add_list[$j - 3]['custom_sku']
                ];
            }


            $spu = $this->GetOldSpu($cus->spu);
            $size = $cus->size;

            // $colors = Db::table('self_color_size')->where('identifying',$cus->color)->first();
            // if($colors){
            //     $color = $colors->name.'/'.$colors->english_name;

            // }

            // if(!$color){
            //     return [
            //         'type' => 'fail',
            //         'msg' => '导入失败,此库存spu无对应颜色'.$add_list[$j - 3]['custom_sku']
            //     ];
            // }

            $is_contract = Db::table('cloudhouse_contract')->where('contract_no',$add_list[$j - 3]['contract_no'])->where('spu',$spu)->where('color_identifying',$cus->color)->first();

            if(!$is_contract){
                return [
                    'type' => 'fail',
                    'msg' => '导入失败,无此合同spu:'.$spu.'--color:'.$cus->color.'--合同号:'.$add_list[$j - 3]['contract_no']
                ];
            }
            $add_list[$j - 3]['spu'] = $spu;
            $add_list[$j - 3]['size'] = $size;
            $add_list[$j - 3]['color_identifying'] = $cus->color;

            $add_list[$j - 3]['color_name'] = $is_contract->color_name;


        }

        $newdata = [];


        //根据spu 颜色分组
        foreach ($add_list as $v) {
            # code...
            $spu_one['size']= $v['size'];
            $spu_one['num']= $v['num'];
            $key = $v['spu'].$v['color'];
            $newdata[$key][] = $spu_one;
        }

        $data = [];

        $ki=0;
        foreach ($newdata as $nk => $nv) {
            # code...
            $return = [];
            $cus = [];
            $boxs = [];
            foreach ($add_list as $ak => $av) {
                # code...
                $key = $av['spu'].$av['color'];
                if($key== $nk){

                    //箱号
                    $box = explode("-",$av['box_num']);

                    if(isset($box[1])){
                        $start = $box[0];
                        $end = $box[1];
                        for ($i=$start; $i < $end+1;$i++) {
                            # code...
                            $boxs[$i] = 1;
                        }

                    }else{
                        $boxs[$av['box_num']] = 1;
                    }



                    $return = $add_list[$ak];
                    $cus[] = $av['custom_sku'];
                    $return['XS'] = 0;
                    $return['S'] = 0;
                    $return['M'] = 0;
                    $return['L'] = 0;
                    $return['XL'] = 0;
                    $return['2XL'] = 0;
                    $return['3XL'] = 0;
                    $return['4XL'] = 0;
                    $return['5XL'] = 0;
                    $num = 0;
                    //尺寸
                    foreach ($nv as $evv) {
                        # code...
                        $size = $evv['size'];
                        $return[$size] = (int)$evv['num'];
                    }

                }
            }
            $return['box_id'] = array_keys($boxs);
            $return['cus'] = $cus;
            $data[$ki] =  $return;
            $ki++;
        }


        return ['type'=>'success','msg'=>'导入成功','data'=>$data];
    }



    public function GetContractColor($params){

        $list = Db::table('cloudhouse_contract');
        if(isset($params['spu'])){
            $list = $list->where('spu','like','%'.$params['spu'].'%');
        }
        if (isset($params['contract_no'])) {
            $list = $list->where('contract_no','like','%'.$params['contract_no'].'%');
        }
        $list = $list->groupBy('color_name')->get(['color_name','color_identifying']);
        // foreach ($list  as $v) {
        //     # code...
        //     $v->color = $v->color_name.'/'.$v->color_identifying;
        // }
        return ['type'=>'success','data'=>$list];
    }

    //合同订单列表
    public function ContractList($params){

        $pagenum = isset($params['page']) ? $params['page'] : 1;
        $limit = isset($params['limit']) ? $params['limit'] : 30;

        $page  =  $pagenum- 1;
        if($page!=0){
            $page  =  $page * $limit;
        }

//        $where['status'] = 1;

        //类型1合同 2出库
        $where['type'] = 1;

        $list = Db::table('cloudhouse_contract')->where('status','!=',0);


        if (isset($params['spu'])) {
            $query_spus = array();
            if (strstr($params['spu'], '，')) {
                $symbol = '，';
            } elseif (strstr($params['spu'], ',')) {
                $symbol = ',';
            } else {
                $symbol = '';
            }
            if ($symbol) {
                $spu_skuarray = explode($symbol,$params['spu']);
                foreach ($spu_skuarray as $key => $spu) {
                    $query_spus[] = $spu;
                }
                $list = $list->whereIn('spu',$query_spus);
            }else {
                $list = $list->where('spu',$params['spu']);
            }

        }


        if (isset($params['contract_no'])) {
            $query_c = array();
            if (strstr($params['contract_no'], '，')) {
                $symbol = '，';
            } elseif (strstr($params['contract_no'], ',')) {
                $symbol = ',';
            } else {
                $symbol = '';
            }
            if ($symbol) {
                $c_array = explode($symbol,$params['contract_no']);
                foreach ($c_array as $key => $c) {
                    $query_c[] = $c;
                }
                $list = $list->whereIn('contract_no',$query_c);
            }else {
                $list = $list->where('contract_no',$params['contract_no']);
            }

        }

        if (isset($params['color'])) {
            $list = $list->whereIn('color_identifying',$params['color']);
        }

        // if(isset($params['spu'])){
        //     $where['spu'] = $params['spu'];
        // }
        // if(isset($params['contract_no'])){
        //     $where['contract_no'] = $params['contract_no'];
        // }


        $list = $list->where($where);

        if(isset($params['start_time'])&&isset($params['end_time'])){
            $list = $list->whereBetween('report_date', [$params['start_time'], $params['end_time']]);
        }


        $totalNum = $list ->count();
        // DB::connection()->enableQueryLog(); 
        $list = $list->offset($page)->limit($limit)->orderBy('create_time','desc')->get()->map(function($value){
            return(array)$value;
        })->toArray();
        // print_r(DB::getQueryLog());

        $newlist = array();
        $checkArr = array();
        $i = 0;
        foreach ($list as $k=>$v) {
//            $checkArr[$v['contract_no'][$v['spu_id']][$v['color_identifying']]] = $k;
//            if(isset($checkArr[$v['contract_no'][$v['spu_id']][$v['color_identifying']]])){
//
//                unset($list[$k]);
//                continue;
//            }
            # code...
            $newlist[$i]['id'] = $v['id'];
            $newlist[$i]['spu'] = $v['spu'];
            $newlist[$i]['spu_id'] = $v['spu_id'];
            $newlist[$i]['title'] = $v['title'];
            $newlist[$i]['color_name'] = $v['color_name'];
            $newlist[$i]['color_identifying'] = $v['color_identifying'];
            $newlist[$i]['count'] = $v['count'];
            $newlist[$i]['one_price'] = $v['one_price'];
            $newlist[$i]['price'] = $v['price'];
            $newlist[$i]['brand'] = $v['brand'];
            $newlist[$i]['report_date'] = $v['report_date'];
            $newlist[$i]['contract_no'] = $v['contract_no'];
            $newlist[$i]['create_user'] = $v['create_user'];
            $newlist[$i]['sign_date'] = $v['sign_date'];
            // $newlist[$i]['create_user'] = $this->GetUsers($v['create_user'])['account'];

            // $out_num = Db::table('cloudhouse_contract')->where('spu',$v['spu'])->where('contract_no',$v['contract_no'])->where('type',2)->where('is_push','!=',3)->get()->map(function($value){
            //     return(array)$value;
            // })->toArray();

            $out_num =  Db::table('goods_transfers_detail')->where('spu',$v['spu'])->where('contract_no',$v['contract_no'])->get();
            // var_dump( $out_num );
            if($out_num){

                //将尺码转化为库存sku
                $customres = Db::table('cloudhouse_custom_sku')->where('spu_id',$v['spu_id'])->where('color', $v['color_identifying'])->where('contract_no',$v['contract_no'])->get();
                $sizecus = array();
                $sizecus['XXS'] = '';
                $sizecus['XS'] = '';
                $sizecus['S'] = '';
                $sizecus['M'] = '';
                $sizecus['L'] = '';
                $sizecus['XL'] = '';
                $sizecus['2XL'] = '';
                $sizecus['3XL'] = '';
                $sizecus['4XL'] = '';
                $sizecus['5XL'] = '';
                $sizecus['6XL'] = '';
                $sizecus['7XL'] = '';
                $sizecus['8XL'] = '';

                // var_dump( $customres);


                foreach ($customres as $key => $value) {
                    # code...
                    $sizecus[$value->size] = $value->custom_sku_id;
                }
                // var_dump($sizecus);

                $XXS = 0;
                $XS = 0;
                $S = 0;
                $M = 0;
                $L = 0;
                $XL = 0;
                $XL2 = 0;
                $XL3 = 0;
                $XL4 = 0;
                $XL5 = 0;
                $XL6 = 0;
                $XL7 = 0;
                $XL8 = 0;

                $factoryXXS = 0;
                $factoryXS = 0;
                $factoryS = 0;
                $factoryM = 0;
                $factoryL = 0;
                $factoryXL = 0;
                $factoryXL2 = 0;
                $factoryXL3 = 0;
                $factoryXL4 = 0;
                $factoryXL5 = 0;
                $factoryXL6 = 0;
                $factoryXL7 = 0;
                $factoryXL8 = 0;
                $goodsData = Db::table('goods_transfers')->where('contract_no','like','%'.$v['contract_no'].'%')->whereIn('type',[1,3])->select('order_no','type')->get();

                $outOrder = array();
                $factoryOrder = array();
                foreach($goodsData as $god){
                    if($god->type==1){
                        $outOrder[] = $god->order_no;
                    }
                    if($god->type==3){
                        $factoryOrder[] = $god->order_no;
                    }
                }
                $factoryOrder = array_unique($factoryOrder);
                if($sizecus['XXS']!=''){
                    $XXS_data = Db::table('goods_transfers_detail')->where('custom_sku_id',$sizecus['XXS'])->where('contract_no',$v['contract_no'])->whereIn('order_no',$outOrder)->select('receive_num')->get();
                    $XXS_factory = Db::table('goods_transfers_detail')->where('custom_sku_id',$sizecus['XXS'])->where('contract_no',$v['contract_no'])->whereIn('order_no',$factoryOrder)->select('receive_num')->get();
                    if(!empty($XXS_data)){
                        foreach ($XXS_data as $xxs_val){
                            $XXS += $xxs_val->receive_num;
                        }
                    }
                    if(!empty($XXS_factory)){
                        foreach ($XXS_factory as $xxs_fval){
                            $factoryXXS += $xxs_fval->receive_num;
                        }
                    }
                }

                if($sizecus['XS']!=''){
                    $XS_data = Db::table('goods_transfers_detail')->where('custom_sku_id',$sizecus['XS'])->where('contract_no',$v['contract_no'])->whereIn('order_no',$outOrder)->select('receive_num')->get();
                    $XS_factory = Db::table('goods_transfers_detail')->where('custom_sku_id',$sizecus['XS'])->where('contract_no',$v['contract_no'])->whereIn('order_no',$factoryOrder)->select('receive_num')->get();
                    if(!empty($XS_data)){
                        foreach ($XS_data as $xs_val){
                            $XS += $xs_val->receive_num;
                        }
                    }
                    if(!empty($XS_factory)){
                        foreach ($XS_factory as $xs_fval){
                            $factoryXS += $xs_fval->receive_num;
                        }
                    }
                }

                if($sizecus['S']!=''){
                    $S_data = Db::table('goods_transfers_detail')->where('custom_sku_id',$sizecus['S'])->where('contract_no',$v['contract_no'])->whereIn('order_no',$outOrder)->select('receive_num')->get();
                    $S_factory = Db::table('goods_transfers_detail')->where('custom_sku_id',$sizecus['S'])->where('contract_no',$v['contract_no'])->whereIn('order_no',$factoryOrder)->select('receive_num')->get();
                    if(!empty($S_data)){
                        foreach ($S_data as $s_val){
                            $S += $s_val->receive_num;
                        }
                    }
                    if(!empty($S_factory)){
                        foreach ($S_factory as $s_fval){
                            $factoryS += $s_fval->receive_num;
                        }
                    }
                }

                if($sizecus['M']!=''){
                    $M_data = Db::table('goods_transfers_detail')->where('custom_sku_id',$sizecus['M'])->where('contract_no',$v['contract_no'])->whereIn('order_no',$outOrder)->select('receive_num')->get();
                    $M_factory = Db::table('goods_transfers_detail')->where('custom_sku_id',$sizecus['M'])->where('contract_no',$v['contract_no'])->whereIn('order_no',$factoryOrder)->select('receive_num')->get();
                    if(!empty($M_data)){
                        foreach ($M_data as $m_val){
                            $M += $m_val->receive_num;
                        }
                    }
                    if(!empty($M_factory)){
                        foreach ($M_factory as $m_fval){
                            $factoryM += $m_fval->receive_num;
                        }
                    }
                }
                if($sizecus['L']!=''){
//                     DB::connection()->enableQueryLog();
                    $L_data = Db::table('goods_transfers_detail')->where('custom_sku_id',$sizecus['L'])->where('contract_no',$v['contract_no'])->whereIn('order_no',$outOrder)->select('receive_num')->get();

                    $L_factory = Db::table('goods_transfers_detail')->where('custom_sku_id',$sizecus['L'])->where('contract_no',$v['contract_no'])->whereIn('order_no',$factoryOrder)->select('receive_num')->get();

//                     print_r(DB::getQueryLog());
                    if(!empty($L_data)){
                        foreach ($L_data as $l_val){
                            $L += $l_val->receive_num;
                        }
                    }

                    if(!empty($L_factory)){
                        foreach ($L_factory as $l_fval){
                            $factoryL += $l_fval->receive_num;
                        }
                    }
                }

                if($sizecus['XL']!=''){
                    $XL_data = Db::table('goods_transfers_detail')->where('custom_sku_id',$sizecus['XL'])->where('contract_no',$v['contract_no'])->whereIn('order_no',$outOrder)->select('receive_num')->get();
                    $XL_factory = Db::table('goods_transfers_detail')->where('custom_sku_id',$sizecus['XL'])->where('contract_no',$v['contract_no'])->whereIn('order_no',$factoryOrder)->select('receive_num')->get();
                    if(!empty($XL_data)){
                        foreach ($XL_data as $xl_val){
                            $XL += $xl_val->receive_num;
                        }
                    }
                    if(!empty($XL_factory)){
                        foreach ($XL_factory as $xl_fval){
                            $factoryXL += $xl_fval->receive_num;
                        }
                    }
                }

                if($sizecus['2XL']!=''){
                    $XL2_data = Db::table('goods_transfers_detail')->where('custom_sku_id',$sizecus['2XL'])->where('contract_no',$v['contract_no'])->whereIn('order_no',$outOrder)->select('receive_num')->get();
                    $XL2_factory = Db::table('goods_transfers_detail')->where('custom_sku_id',$sizecus['2XL'])->where('contract_no',$v['contract_no'])->whereIn('order_no',$factoryOrder)->select('receive_num')->get();
                    if(!empty($XL2_data)){
                        foreach ($XL2_data as $xl2_val){
                            $XL2 += $xl2_val->receive_num;
                        }
                    }
                    if(!empty($XL2_factory)){
                        foreach ($XL2_factory as $xl2_fval){
                            $factoryXL2 = $xl2_fval->receive_num;
                        }
                    }
                }

                if($sizecus['3XL']!=''){
                    $XL3_data = Db::table('goods_transfers_detail')->where('custom_sku_id',$sizecus['3XL'])->where('contract_no',$v['contract_no'])->whereIn('order_no',$outOrder)->select('receive_num')->get();
                    $XL3_factory = Db::table('goods_transfers_detail')->where('custom_sku_id',$sizecus['3XL'])->where('contract_no',$v['contract_no'])->whereIn('order_no',$factoryOrder)->select('receive_num')->get();
                    if(!empty($XL3_data)){
                        foreach ($XL3_data as $xl3_val){
                            $XL3 += $xl3_val->receive_num;
                        }
                    }
                    if(!empty($XL3_factory)){
                        foreach ($XL3_factory as $xl3_fval){
                            $factoryXL3 += $xl3_fval->receive_num;
                        }
                    }
                }

                if($sizecus['4XL']!=''){
                    $XL4_data = Db::table('goods_transfers_detail')->where('custom_sku_id',$sizecus['4XL'])->where('contract_no',$v['contract_no'])->whereIn('order_no',$outOrder)->select('receive_num')->get();
                    $XL4_factory = Db::table('goods_transfers_detail')->where('custom_sku_id',$sizecus['4XL'])->where('contract_no',$v['contract_no'])->whereIn('order_no',$factoryOrder)->select('receive_num')->get();
                    if(!empty($XL4_data)){
                        foreach ($XL4_data as $xl4_val){
                            $XL4 += $xl4_val->receive_num;
                        }
                    }
                    if(!empty($XL4_factory)){
                        foreach ($XL4_factory as $xl4_fval){
                            $factoryXL4 = $xl4_fval->receive_num;
                        }
                    }
                }

                if($sizecus['5XL']!=''){
                    $XL5_data = Db::table('goods_transfers_detail')->where('custom_sku_id',$sizecus['5XL'])->where('contract_no',$v['contract_no'])->whereIn('order_no',$outOrder)->select('receive_num')->get();
                    $XL5_factory = Db::table('goods_transfers_detail')->where('custom_sku_id',$sizecus['5XL'])->where('contract_no',$v['contract_no'])->whereIn('order_no',$factoryOrder)->select('receive_num')->get();
                    if(!empty($XL5_data)){
                        foreach ($XL5_data as $xl5_val){
                            $XL5 += $xl5_val->receive_num;
                        }
                    }
                    if(!empty($XL5_factory)){
                        foreach ($XL5_factory as $xl5_fval){
                            $factoryXL5 = $xl5_fval->receive_num;
                        }
                    }
                }

                if($sizecus['6XL']!=''){
                    $XL6_data = Db::table('goods_transfers_detail')->where('custom_sku_id',$sizecus['6XL'])->where('contract_no',$v['contract_no'])->whereIn('order_no',$outOrder)->select('receive_num')->get();
                    $XL6_factory = Db::table('goods_transfers_detail')->where('custom_sku_id',$sizecus['6XL'])->where('contract_no',$v['contract_no'])->whereIn('order_no',$factoryOrder)->select('receive_num')->get();
                    if(!empty($XL6_data)){
                        foreach ($XL6_data as $xl6_val){
                            $XL6 += $xl6_val->receive_num;
                        }
                    }
                    if(!empty($XL6_factory)){
                        foreach ($XL6_factory as $xl6_fval){
                            $factoryXL6 += $xl6_fval->receive_num;
                        }
                    }
                }

                if($sizecus['7XL']!=''){
                    $XL7_data = Db::table('goods_transfers_detail')->where('custom_sku_id',$sizecus['7XL'])->where('contract_no',$v['contract_no'])->whereIn('order_no',$outOrder)->select('receive_num')->get();
                    $XL7_factory = Db::table('goods_transfers_detail')->where('custom_sku_id',$sizecus['7XL'])->where('contract_no',$v['contract_no'])->whereIn('order_no',$factoryOrder)->select('receive_num')->get();
                    if(!empty($XL7_data)){
                        foreach ($XL7_data as $xl7_val){
                            $XL7 += $xl7_val->receive_num;
                        }
                    }
                    if(!empty($XL7_factory)){
                        foreach ($XL7_factory as $xl7_fval){
                            $factoryXL7 += $xl7_fval->receive_num;
                        }
                    }
                }

                if($sizecus['8XL']!=''){
                    $XL8_data = Db::table('goods_transfers_detail')->where('custom_sku_id',$sizecus['8XL'])->where('contract_no',$v['contract_no'])->whereIn('order_no',$outOrder)->select('receive_num')->get();
                    $XL8_factory = Db::table('goods_transfers_detail')->where('custom_sku_id',$sizecus['8XL'])->where('contract_no',$v['contract_no'])->whereIn('order_no',$factoryOrder)->select('receive_num')->get();
                    if(!empty($XL8_data)){
                        foreach ($XL8_data as $xl8_val){
                            $XL8 += $xl8_val->receive_num;
                        }
                    }
                    if(!empty($XL8_factory)){
                        foreach ($XL8_factory as $xl8_fval){
                            $factoryXL8 = $xl8_fval->receive_num;
                        }
                    }
                }

                $newlist[$i]['total_receive']  = $XXS+$XS+$S+$M+$L+$XL+$XL2+$XL3+$XL4+$XL5+$XL6+$XL7+$XL8;

                // foreach ($out_num as $ov) {
                //     if($sizecus['XXS']!=''&&$ov->custom_sku == $sizecus['XXS']){
                //         $XXS+=$ov->transfers_num;
                //     }

                //     if($sizecus['L']!=''&&$ov->custom_sku == $sizecus['L']){
                //         // exit;
                //         $L+=$ov->transfers_num;
                //     }

                //     $XXS = $sizecus['XXS'];
                //     $XXS+= $ov['XXS'];
                //     $XS+= $ov['XS'];
                //     $S+= $ov['S'];
                //     $M+= $ov['M'];
                //     $L+= $ov['L'];
                //     $XL+= $ov['XL'];
                //     $XL2+= $ov['2XL'];
                //     $XL3+= $ov['3XL'];
                //     $XL4+= $ov['4XL'];
                //     $XL5+= $ov['5XL'];
                //     $XL6+= $ov['6XL'];
                //     $XL7+= $ov['7XL'];
                //     $XL8+= $ov['8XL'];
                // }

//                $newlist[$i]['XXS']['factory'] = $factoryXXS - $XXS;
//                $newlist[$i]['XS']['factory']  =  $factoryXS - $XS;
//                $newlist[$i]['S']['factory']  = $factoryS - $S;
//                $newlist[$i]['M']['factory']  = $factoryM - $M;
//                $newlist[$i]['L']['factory']  = $factoryL - $L;
//                $newlist[$i]['XL']['factory']  = $factoryXL - $XL;
//                $newlist[$i]['2XL']['factory']  = $factoryXL2 - $XL2;
//                $newlist[$i]['3XL']['factory']  = $factoryXL3 - $XL3;
//                $newlist[$i]['4XL']['factory']  = $factoryXL4 - $XL4;
//                $newlist[$i]['5XL']['factory']  = $factoryXL5 - $XL5;
//                $newlist[$i]['6XL']['factory']  = $factoryXL6 - $XL6;
//                $newlist[$i]['7XL']['factory']  = $factoryXL7 - $XL7;
//                $newlist[$i]['8XL']['factory']  = $factoryXL8 - $XL8;

                $newlist[$i]['XXS']['factory'] = $factoryXXS;
                $newlist[$i]['XS']['factory']  =  $factoryXS;
                $newlist[$i]['S']['factory']  = $factoryS;
                $newlist[$i]['M']['factory']  = $factoryM;
                $newlist[$i]['L']['factory']  = $factoryL;
                $newlist[$i]['XL']['factory']  = $factoryXL;
                $newlist[$i]['2XL']['factory']  = $factoryXL2;
                $newlist[$i]['3XL']['factory']  = $factoryXL3;
                $newlist[$i]['4XL']['factory']  = $factoryXL4;
                $newlist[$i]['5XL']['factory']  = $factoryXL5;
                $newlist[$i]['6XL']['factory']  = $factoryXL6;
                $newlist[$i]['7XL']['factory']  = $factoryXL7;
                $newlist[$i]['8XL']['factory']  = $factoryXL8;

                $XXS = $v['XXS'] - $XXS;
                $XS = $v['XS'] - $XS;
                $S = $v['S'] - $S;
                $M = $v['M'] - $M;
                $L = $v['L'] - $L;
                $XL = $v['XL'] - $XL;
                $XL2 = $v['2XL'] - $XL2;
                $XL3 = $v['3XL'] - $XL3;
                $XL4 = $v['4XL'] - $XL4;
                $XL5 = $v['5XL'] - $XL5;
                $XL6 = $v['6XL'] - $XL6;
                $XL7 = $v['7XL'] - $XL7;
                $XL8 = $v['8XL'] - $XL8;

                $newlist[$i]['L']['custom_sku'] = $sizecus['L'];
                $newlist[$i]['XXS']['surplus'] = $XXS;
                $newlist[$i]['XS']['surplus']  =  $XS;
                $newlist[$i]['S']['surplus']  = $S;
                $newlist[$i]['M']['surplus']  = $M;
                $newlist[$i]['L']['surplus']  = $L;
                $newlist[$i]['XL']['surplus']  = $XL;
                $newlist[$i]['2XL']['surplus']  = $XL2;
                $newlist[$i]['3XL']['surplus']  = $XL3;
                $newlist[$i]['4XL']['surplus']  = $XL4;
                $newlist[$i]['5XL']['surplus']  = $XL5;
                $newlist[$i]['6XL']['surplus']  = $XL6;
                $newlist[$i]['7XL']['surplus']  = $XL7;
                $newlist[$i]['8XL']['surplus']  = $XL8;

//                $newlist[$i]['XXS']['show'] = $XXS.'/'.$v['XXS'];
//                $newlist[$i]['XS']['show']  = $XS.'/'.$v['XS'];
//                $newlist[$i]['S']['show']  = $S.'/'.$v['S'];
//                $newlist[$i]['M']['show']  = $M.'/'.$v['M'];
//                $newlist[$i]['L']['show']  = $L.'/'.$v['L'];
//                $newlist[$i]['XL']['show']  = $XL.'/'.$v['XL'];
//                $newlist[$i]['2XL']['show']  = $XL2.'/'.$v['2XL'];
//                $newlist[$i]['3XL']['show']  = $XL3.'/'.$v['3XL'];
//                $newlist[$i]['4XL']['show']  = $XL4.'/'.$v['4XL'];
//                $newlist[$i]['5XL']['show']  = $XL5.'/'.$v['5XL'];
//                $newlist[$i]['6XL']['show']  = $XL6.'/'.$v['6XL'];
//                $newlist[$i]['7XL']['show']  = $XL7.'/'.$v['7XL'];
//                $newlist[$i]['8XL']['show']  = $XL8.'/'.$v['8XL'];
//
                $newlist[$i]['XXS']['show'] = $XXS.'/'.$newlist[$i]['XXS']['factory'].'/'.$v['XXS'];
                $newlist[$i]['XS']['show']  = $XS.'/'.$newlist[$i]['XS']['factory'].'/'.$v['XS'];
                $newlist[$i]['S']['show']  = $S.'/'.$newlist[$i]['S']['factory'].'/'.$v['S'];
                $newlist[$i]['M']['show']  = $M.'/'.$newlist[$i]['M']['factory'].'/'.$v['M'];
                $newlist[$i]['L']['show']  = $L.'/'.$newlist[$i]['L']['factory'].'/'.$v['L'];
                $newlist[$i]['XL']['show']  = $XL.'/'.$newlist[$i]['XL']['factory'].'/'.$v['XL'];
                $newlist[$i]['2XL']['show']  = $XL2.'/'.$newlist[$i]['2XL']['factory'].'/'.$v['2XL'];
                $newlist[$i]['3XL']['show']  = $XL3.'/'.$newlist[$i]['3XL']['factory'].'/'.$v['3XL'];
                $newlist[$i]['4XL']['show']  = $XL4.'/'.$newlist[$i]['4XL']['factory'].'/'.$v['4XL'];
                $newlist[$i]['5XL']['show']  = $XL5.'/'.$newlist[$i]['5XL']['factory'].'/'.$v['5XL'];
                $newlist[$i]['6XL']['show']  = $XL6.'/'.$newlist[$i]['6XL']['factory'].'/'.$v['6XL'];
                $newlist[$i]['7XL']['show']  = $XL7.'/'.$newlist[$i]['7XL']['factory'].'/'.$v['7XL'];
                $newlist[$i]['8XL']['show']  = $XL8.'/'.$newlist[$i]['8XL']['factory'].'/'.$v['8XL'];
            }else{
                $newlist[$i]['total_receive']  = 0;
                //XXS XS S M L XL 2XL 3XL 4XL 5XL 6XL 7XL 8XL
                $newlist[$i]['XXS']['surplus']  =  $v['XXS'];
                $newlist[$i]['XS']['surplus']  =  $v['XS'];
                $newlist[$i]['S']['surplus']  =  $v['S'];
                $newlist[$i]['M']['surplus']  =  $v['M'];
                $newlist[$i]['L']['surplus']  =  $v['L'];
                $newlist[$i]['XL']['surplus']  =  $v['XL'];
                $newlist[$i]['2XL']['surplus']  =  $v['2XL'];
                $newlist[$i]['3XL']['surplus']  =  $v['3XL'];
                $newlist[$i]['4XL']['surplus']  =  $v['4XL'];
                $newlist[$i]['5XL']['surplus']  =  $v['5XL'];
                $newlist[$i]['6XL']['surplus']  =  $v['6XL'];
                $newlist[$i]['7XL']['surplus']  =  $v['7XL'];
                $newlist[$i]['8XL']['surplus']  =  $v['8XL'];

                $newlist[$i]['XXS']['factory'] = $v['XXS'];
                $newlist[$i]['XS']['factory']  = $v['XS'];
                $newlist[$i]['S']['factory']  = $v['S'];
                $newlist[$i]['M']['factory']  =  $v['M'];
                $newlist[$i]['L']['factory']  = $v['L'];
                $newlist[$i]['XL']['factory']  = $v['XL'];
                $newlist[$i]['2XL']['factory']  = $v['2XL'];
                $newlist[$i]['3XL']['factory']  =  $v['3XL'];
                $newlist[$i]['4XL']['factory']  = $v['4XL'];
                $newlist[$i]['5XL']['factory']  = $v['5XL'];
                $newlist[$i]['6XL']['factory']  = $v['6XL'];
                $newlist[$i]['7XL']['factory']  = $v['7XL'];
                $newlist[$i]['8XL']['factory']  =  $v['8XL'];


                $newlist[$i]['XXS']['show']  =  $v['XXS'].'/'.$v['XXS'];
                $newlist[$i]['XS']['show']  =  $v['XS'].'/'.$v['XS'];
                $newlist[$i]['S']['show']  =  $v['S'].'/'.$v['S'];
                $newlist[$i]['M']['show']  =  $v['M'].'/'.$v['M'];
                $newlist[$i]['L']['show']  =  $v['L'].'/'.$v['L'];
                $newlist[$i]['XL']['show']  =  $v['XL'].'/'.$v['XL'];
                $newlist[$i]['2XL']['show']  =  $v['2XL'].'/'.$v['2XL'];
                $newlist[$i]['3XL']['show']  =  $v['3XL'].'/'.$v['3XL'];
                $newlist[$i]['4XL']['show']  =  $v['4XL'].'/'.$v['4XL'];
                $newlist[$i]['5XL']['show']  =  $v['5XL'].'/'.$v['5XL'];
                $newlist[$i]['6XL']['show']  =  $v['6XL'].'/'.$v['6XL'];
                $newlist[$i]['7XL']['show']  =  $v['7XL'].'/'.$v['7XL'];
                $newlist[$i]['8XL']['show']  =  $v['8XL'].'/'.$v['8XL'];
            }
            $i++;

        }
        return [
            'data' => $newlist,
            'totalNum' => $totalNum,
        ];
    }


    //出货
    public function ContractOut($params){

        $detail = array();

        $out_no = rand(100,999).time();

        $house_type = $params['house_type']; //入库仓库  1同安仓 2泉州仓 3云仓 4.工厂虚拟仓 5.其它 6.工厂寄存仓

        $skunum = 0;
        $goodsnum = 0;
        $time = date('Y-m-d H:i:s',time());
        db::beginTransaction();    //开启事务
        foreach ($params as $one) {
            # code...
            $insert = [];
            if(is_array($one)){

                $insert['XXS'] =  $one['XXS']??0;
                $insert['XS'] =  $one['XS']??0;
                $insert['S'] =  $one['S']??0;
                $insert['M'] =  $one['M']??0;
                $insert['L'] =  $one['L']??0;
                $insert['XL'] = $one['XL']??0;
                $insert['2XL'] =  $one['2XL']??0;
                $insert['3XL'] =  $one['3XL']??0;
                $insert['4XL'] =  $one['4XL']??0;
                $insert['5XL'] =  $one['5XL']??0;
                $insert['6XL'] =  $one['6XL']??0;
                $insert['7XL'] =  $one['7XL']??0;
                $insert['8XL'] = $one['8XL']??0;
                foreach ($insert as $key => $num) {
                    # code...
                    if($num>0){

                        // try {
                        //     //code...
                        //     $color_en = explode('/',$one['color'])[1];
                        // } catch (\Throwable $th) {
                        //     //throw $th;
                        //     return [
                        //         'type' => 'fail',
                        //         'msg' => $th->getMessage()
                        //     ];
                        // }
                        $insert['color_name'] = $one['color_name']??'';
                        $insert['color_identifying'] = $one['color_identifying']??'';

                        $GetSpuId = $this->GetSpuId($one['spu']);
                        if(!$GetSpuId){
                            return [
                                'type' => 'fail',
                                'msg' => '未获取到'.$one['spu'].'的spuId'
                            ];
                        }
                        $customsku = Db::table('cloudhouse_custom_sku')->where('contract_no',$one['contract_no'])->where('size',$key)->where('spu_id',$GetSpuId)->Where('color',$insert['color_identifying'])->first();


                        if(!$customsku){
                            db::rollback();// 回调
                            return [
                                'type' => 'fail',
                                'msg' => '非法出货，出货时无此数据,合同号：'.$one['contract_no'].'--spu:'.$one['spu'].'--size:'.$key.'--spuid='.$GetSpuId.'--color='.$insert['color_identifying']
                            ];
                        }else{
                            //sku个数
                            $skunum++;
                            // $detail[]= ['custom_sku'=> $customsku->custom_sku,'num'=>$num];
                            //订单号去重
                            $detail[$one['contract_no']] = 1;
                            //商品总数
                            $goodsnum += $num;

                            $getOld = $this->GetOldCustomSku($customsku->custom_sku);
                            if($getOld){
                                $detailinsert['custom_sku'] = $getOld;
                            }else{
                                $detailinsert['custom_sku'] = $customsku->custom_sku;
                            }

                            $detailinsert['custom_sku_id'] = $customsku->custom_sku_id;
                            $detailinsert['spu_id'] = $customsku->spu_id;
                            //调拨数量
                            $detailinsert['transfers_num'] = $num;

                            //实际数量
                            $factory_num = Db::table('self_custom_sku')->where('id',$customsku->custom_sku_id)->select('factory_num','is_cloud','custom_sku','old_custom_sku')->first();
                            if(empty($factory_num)){
                                db::rollback();// 回调
                                return [
                                    'type' => 'fail',
                                    'msg' => '没有找到此id--'.$customsku->custom_sku_id.'的工厂数据'
                                ];
                            }
                            $factory_num = json_decode(json_encode($factory_num),true);
                            if($house_type==4){
                                $detailinsert['receive_num'] = $num;
                                $detailinsert['receive_time'] =  $time;
                                $update_num = $factory_num['factory_num'] + $num;
                                Db::table('self_custom_sku')->where('id',$customsku->custom_sku_id)->update(['factory_num'=>$update_num]);
                                //查询是否有数量一致合同号也一致的数据
                                $alreadyGoods = DB::table('goods_transfers_detail as a')->leftJoin('goods_transfers as b','a.order_no','=','b.order_no')
                                    ->where('a.custom_sku_id',$customsku->custom_sku_id)->where('a.contract_no',$one['contract_no'])->where('b.type',3)->select('receive_num')->first();
                                if(!empty($alreadyGoods)){
                                    $nowNum = $alreadyGoods->receive_num + $num;
                                    if($nowNum>($customsku->num)*1.1){
                                        db::rollback();// 回调
                                        return [
                                            'type' => 'fail',
                                            'msg' => $customsku->custom_sku.'的录入数量超出合同数量的10%，无法提交'
                                        ];
                                    }
                                }
                                //新增工厂入库记录
                                $record['spu'] = $customsku->spu;
                                $record['custom_sku'] = $customsku->custom_sku;
                                $record['type'] = 2;
                                $record['type_detail'] = 13;
                                $record['order_no'] = $out_no;
                                $record['num'] = $num;
                                $record['custom_sku_id'] = $customsku->custom_sku_id;
                                $record['spu_id'] = $customsku->spu_id;
                                $record['createtime'] = $time;
                                $record['warehouse_id'] = 4;//工厂
                                $insert_record = DB::table('cloudhouse_record')->insert($record);
                                if(!$insert_record){
                                    db::rollback();// 回调
                                    return [
                                        'type' => 'fail',
                                        'msg' => '新增'.$customsku->custom_sku.'的工厂入库记录失败'
                                    ];
                                }
                            }
//                            elseif($house_type==5){
//                                $detailinsert['receive_num'] = 0;
//                                $detailinsert['receive_time'] = $time;
//                                $update_num = $factory_num['factory_num']-$num;
//                                if($update_num<0){
//                                    $update_num= 0;
////                                    return ['type'=>'fail','msg'=>'出货失败，出货数量超出当前工厂虚拟仓库存'];
//                                }
//                                Db::table('self_custom_sku')->where('id',$customsku->custom_sku_id)->update(['factory_num'=>$update_num]);
//                            }
                            else{
                                if($house_type==3){
                                    if($factory_num['old_custom_sku']){
                                        $thisSku = $factory_num['old_custom_sku'];
                                    }else{
                                        $thisSku = $factory_num['custom_sku'];
                                    }
                                    if($factory_num['is_cloud']==0){
                                        $cloud = new \App\Http\Controllers\Jobs\CloudHouseController();
                                        try {
                                            $cloud::sendJob([$thisSku]);
                                        } catch(\Exception $e){
                                            Redis::Lpush('send_goods_fail',json_encode($thisSku));
                                        }
                                    }
                                }
                                $detailinsert['receive_num'] = 0;
                                $detailinsert['receive_time'] = $time;
//                                $update_num = $factory_num['factory_num']-$num;
//                                if($update_num<0){
//                                    $update_num= 0;
//                                }
//                                Db::table('self_custom_sku')->where('id',$customsku->custom_sku_id)->update(['factory_num'=>$update_num]);
                            }
                            //spu
                            $detailinsert['spu'] = $one['spu']??'';
                            //合同号
                            $detailinsert['contract_no'] = $one['contract_no']??'';
                            //批号
                            $detailinsert['order_no'] = $out_no;
                            //创建时间
                            $detailinsert['createtime'] =  $time;
                            //插入详情
                            Db::table('goods_transfers_detail')->insert($detailinsert);

                        }
                    }
                }


            }

        }


        //批号
        $transfersinsert['order_no'] = $out_no;
        //合同号
        $transfersinsert['contract_no'] = json_encode(array_keys($detail));

        $transfersinsert['img_url'] = isset($params['img_url'])?json_encode($params['img_url']):'';//图片
        $transfersinsert['file_url'] = isset($params['file_url'])?json_encode($params['file_url']):''??'';//附件

        $transfersinsert['createtime'] =  $time;
        //出库sku数量
        $transfersinsert['sku_count'] = $skunum;
        //出库数量
        $transfersinsert['total_num'] = $goodsnum;
        //实际数量
        $transfersinsert['receive_total_num'] = 0;
        //入库仓库
        $transfersinsert['in_house'] = $params['house_type']??'';
        //出仓仓库
        $transfersinsert['out_house'] = 4;
        //出库用户
        $transfersinsert['user_id'] = $params['user_id'];
        $dingding = new \App\Models\BaseModel();
        $merchandiser = $dingding->GetUsers($params['user_id']);

        $transfersinsert['anomaly_order'] =  $params['anomaly_order']??'';
        $transfersinsert['text'] =  $params['text']??'';
        if($house_type==1||$house_type==2||$house_type==3){
            //预计交货时间
            $transfersinsert['yj_arrive_time'] =  $params['yj_arrive_time']??'2000-01-01 00:00:00';
            //卸货方
            $transfersinsert['unload'] =  $params['unload']??0;
            if($transfersinsert['unload']==1){
                $unload = '仓库';
            }else{
                $unload = '工厂';
            }
        }

        $transfersinsert['type'] = 1;

        if($house_type==1||$house_type==2||$house_type==3||$house_type==6){
            $transfersinsert['type_detail'] = 2;
            //预计交货时间
            $transfersinsert['yj_arrive_time'] =  $params['yj_arrive_time']??'2000-01-01 00:00:00';
            //卸货方
            $transfersinsert['unload'] =  $params['unload']??0;
            if($transfersinsert['unload']==1){
                $unload = '仓库';
            }else{
                $unload = '工厂';
            }
        }

        if($house_type==4){
            $transfersinsert['type_detail'] = 13;
            $transfersinsert['yj_arrive_time'] = $time;
            $transfersinsert['sj_arrive_time'] = $time;
            $transfersinsert['out_house'] = 0;
            $transfersinsert['type'] = 3;
            $transfersinsert['is_push'] = 3;
            $transfersinsert['receive_total_num'] = $goodsnum;
        }

        Db::table('goods_transfers')->insert($transfersinsert);


        db::commit();

        if($house_type<7){
            if($transfersinsert['in_house']==1){
                $house_name = '同安仓';
                $house_user = [309,313];
            }elseif($transfersinsert['in_house']==2){
                $house_name = '泉州仓';
                $house_user = [74];
            }elseif($transfersinsert['in_house']==3){
                $house_name = '云仓';
            }elseif($transfersinsert['in_house']==4){
                $house_name = '工厂虚拟仓';
            } elseif($transfersinsert['in_house']==6){
                $house_name = '工厂寄存仓';
            }
            if($house_type==1||$house_type==2||$house_type==3){
                $content1 = '工厂有货即将到'.$house_name.'，请安排人员接收，入库单号：'.$out_no.'，跟单员：'.$merchandiser['account'].'卸货方：'.$unload.'，预计到达时间：'.$transfersinsert['yj_arrive_time'];
                $content2 = '工厂有货即将到'.$house_name.',入库单号：'.$out_no.'，请根据单号在采购出入库列表查看明细';
            }elseif($house_type==4){
                $content2 = '工厂有货已生产完毕并加入工厂虚拟仓，入库单号：'.$out_no.'，请根据单号在虚拟仓出入库列表查看明细';
            }elseif($house_type==6){
                $content2 = '工厂有货即将寄存在工厂，入库单号：'.$out_no.'，请根据单号在采购出入库列表查看明细';
            }
            if(isset($house_user)){
                $dingding->msgSend($house_user,$content1);
            }
            if(!empty($params['user_list'])){
                $dingding->msgSend($params['user_list'],$content2);
            }
        }

        return ['type'=>'success','msg'=>'出货成功'];
    }


    public function PushInhouse($params){
        $id = $params['id'];
        $order = Db::table('goods_transfers')->where('id',$id)->first();
        $order_detail =  Db::table('goods_transfers_detail')->where('order_no',$order->order_no)->get();

//        $detail = array();
        foreach ($order_detail as $k => $v) {
            # code...
//            $detail[]= ['custom_sku'=> $v->custom_sku,'num'=>$v->transfers_num];
            $custom_arr[] = $v->custom_sku;
        }
        $cloud = new \App\Http\Controllers\Jobs\CloudHouseController();
        try {
            $cloud::sendJob($custom_arr);
        } catch(\Exception $e){
            Redis::Lpush('send_goods_fail',$custom_arr);
        }
//                //云仓则加入队列
//        $quequeres['order_no'] = $order->order_no;
//        $quequeres['detail'] = $detail;
//        $quequeres['inhouse_type'] = 1;
//        return $quequeres;
        // $cloud = new \App\Http\Controllers\Jobs\CloudHouseController();
        // try {
        //     $cloud::inhouseJob($quequeres);
        // } catch(\Exception $e){
        //     Redis::Lpush('in_house_fail',json_encode($quequeres));
        // }

    }


    public function ContractSyncDetail($params)
    {
        $id = $params['id'];

        $one = DB::table('cloudhouse_contract')->where('id',$id)->limit(1)->get()->map(function($value){
            return(array)$value;
        })->toArray()[0];
        $customskus = array();
        $GetSpuId = $this->GetSpuId($one['spu']);
        if(!$GetSpuId){
            return [
                'type' => 'fail',
                'msg' => '未获取到'.$one->spud.'的spuId'
            ];
        }
        # code...
        $sizes = array();
        $sizes['XXS'] =  $one['XXS']??0;
        $sizes['XS'] =  $one['XS']??0;
        $sizes['S'] =  $one['S']??0;
        $sizes['M'] =  $one['M']??0;
        $sizes['L'] =  $one['L']??0;
        $sizes['XL'] = $one['XL']??0;
        $sizes['2XL'] =  $one['2XL']??0;
        $sizes['3XL'] =  $one['3XL']??0;
        $sizes['4XL'] =  $one['4XL']??0;
        $sizes['5XL'] =  $one['5XL']??0;
        $sizes['6XL'] =  $one['6XL']??0;
        $sizes['7XL'] =  $one['7XL']??0;
        $sizes['8XL'] = $one['8XL']??0;
        foreach ($sizes as $key => $num) {
            # code...
            if($num>0){
                $customsku = Db::table('cloudhouse_custom_sku')->where('contract_no',$one['contract_no'])->where('size',$key)->where('spu_id',$GetSpuId)->where('color',$one['color_identifying'])->first();
                if(!$customsku){
                    return [
                        'type' => 'fail',
                        'msg' => '非法同步，出库时无此数据,合同号：'.$one['contract_no'].'--spu:'.$one['spu'].'--size:'.$key
                    ];
                }else{
                    if($this->GetOldCustomSku($customsku->custom_sku)){
                        $customskus[]= $this->GetOldCustomSku($customsku->custom_sku);
                    }else{
                        $customskus[]= $customsku->custom_sku;
                    }

                }
            }
        }

        $cloud = new \App\Http\Controllers\Jobs\CloudHouseController();
        try {
            $cloud::sendJob($customskus);
        } catch(\Exception $e){
            Redis::Lpush('send_goods_fail',json_encode($params));
            return ['type'=>'fail','msg'=>'加入同步队列失败'];
        }

        return ['type'=>'success','msg'=>'加入同步队列成功'];
    }

    public function ContractOutlog($params)
    {
        $pagenum = isset($params['page']) ? $params['page'] : 1;
        $limit = isset($params['limit']) ? $params['limit'] : 30;

        $page  =  $pagenum- 1;
        if($page!=0){
            $page  =  $page * $limit;
        }

        //类型1合同 2出库
        $where['type'] = 2;


        if(isset($params['out_no'])){
            $where['out_no'] = $params['out_no'];
        }

        $list = Db::table('cloudhouse_contract')->where($where);

        $totalNum = $list->groupBy('out_no')->count();
        $list = $list->offset($page)->limit($limit)->select('out_no','create_user','sign_date','create_time','is_push','status','house_type')->groupBy('out_no')->get()->map(function($value){
            return(array)$value;
        })->toArray();

        foreach ($list as &$v) {
            $v['create_user'] = $this->GetUsers($v['create_user'])['account'];

            if($v['is_push']==0){
                $v['state'] = '待取消';
            }
            if($v['is_push']==1){
                $v['state'] = '待推送';
            }
            if($v['is_push']==2){
                $v['state'] = '推送成功';
            }
            if($v['is_push']==3){
                $v['state'] = '取消成功';
            }
            if($v['is_push']==4){
                $v['state'] = '取消失败';
            }
            if($v['is_push']==5){
                $v['state'] = '云仓已入库';
            }
            //是否推送ispush 0取消推送1等待推送2成功3取消成功4取消失败5已入库

        }
        return [
            'data' => $list,
            'totalNum' => $totalNum,
        ];
    }

    //取消出库
//    public function ContractCancel($params){
//        $out_no = $params['out_no'];
//
//        $outres = Db::table('cloudhouse_contract')->where('out_no', $out_no)->first();
//        if(!$outres ){
//            return ['type'=>'fail','msg'=>'无此出库订单，请检查出库号'];
//        }
//        //是否推送ispush 0取消推送1等待推送2成功3取消成功4取消失败5已入库
//        if($outres->is_push==0){
//            return ['type'=>'fail','msg'=>'已处于待取消状态，请耐心等待'];
//        }
//        if($outres->is_push==5){
//            return ['type'=>'fail','msg'=>'已入库，无法取消'];
//        }
//        if($outres->is_push==3){
//            return ['type'=>'fail','msg'=>'已取消成功，请勿重复推送'];
//        }
//
//        $now = time()-600;
//        $lasttime = $outres->create_time;
//        // if($now<strtotime($lasttime)){
//        //     return ['type'=>'fail','msg'=>'出库后，需要等待10分钟后才可以取消'];
//        // }
//        //取消推送
//        Db::table('cloudhouse_contract')->where('out_no', $out_no)->update(['is_push'=>0]);
//
//        //只有云仓需要加入队列
//        if($outres->house_type ==3){
//            $queueres['order_no'] = $out_no;
//            $queueres['order_type'] = 'in_house';
//            $cloud = new \App\Http\Controllers\Jobs\CloudHouseController();
//            try {
//                $cloud::cancelJob($queueres);
//            } catch(\Exception $e){
//                Redis::Lpush('order_cancel_fail',json_encode($queueres));
//            }
//        }
//
//        return ['type'=>'success','msg'=>'取消成功'];
//    }

    //取消采购入库
    public function InhouseCancel($params){
        $in_no = $params['order_no'];

        $outres = Db::table('goods_transfers')->where('order_no', $in_no)->first();
        if(!$outres ){
            return ['type'=>'fail','msg'=>'无此入库订单，请检查出库号'];
        }
        //是否推送ispush -2取消失败-1取消成功0推送失败1等待推送2成功3已确认
        if($outres->is_push==-2){
            return ['type'=>'fail','msg'=>'已处于取消状态'];
        }
        if($outres->is_push==3){
            return ['type'=>'fail','msg'=>'已确认，无法取消'];
        }
        if($outres->is_push==-1){
            return ['type'=>'fail','msg'=>'已取消成功，请勿重复推送'];
        }


        //只有云仓需要加入队列
        if($outres->in_house ==3){
            $queueres['order_no'] = $in_no;
            $queueres['order_type'] = 'CGRK';
            $cloud = new \App\Http\Controllers\Jobs\CloudHouseController();
            try {
                $cloud::cancelJob($queueres);
            } catch(\Exception $e){
                Redis::Lpush('order_cancel_fail',json_encode($queueres));
                return ['type'=>'fail','msg'=>'取消失败'];
            }
        }else{
            //取消推送
            Db::table('goods_transfers')->where('order_no', $in_no)->update(['is_push'=>-1]);
        }

        return ['type'=>'success','msg'=>'取消成功'];
    }

    //取消调拨
    public function transfersCancel($params){
        $in_no = $params['order_no'];

        $outres = Db::table('goods_transfers')->where('order_no', $in_no)->first();
        if(!$outres ){
            return ['type'=>'fail','msg'=>'无此调拨订单，请检查单号'];
        }
        //是否推送ispush -2取消失败-1取消成功0推送失败1等待推送2成功3已确认
        if($outres->is_push==-2){
            return ['type'=>'fail','msg'=>'已处于取消状态'];
        }
        if($outres->is_push==3){
            return ['type'=>'fail','msg'=>'已确认，无法取消'];
        }
        if($outres->is_push==-1){
            return ['type'=>'fail','msg'=>'已取消成功，请勿重复推送'];
        }

        //只有云仓需要加入队列
        if($outres->out_house ==3){
            $queueres['order_no'] = $in_no;
            $queueres['order_type'] = 'DBCK';
            $cloud = new \App\Http\Controllers\Jobs\CloudHouseController();
            try {
                $cloud::cancelJob($queueres);
                return ['type'=>'success','msg'=>'取消成功'];
            } catch(\Exception $e){
                Redis::Lpush('order_cancel_fail',json_encode($queueres));
                return ['type'=>'fail','msg'=>'取消失败'];
            }
        }
        if($outres->in_house ==3){
            $queueres['order_no'] = $in_no;
            $queueres['order_type'] = 'DBRK';
            $cloud = new \App\Http\Controllers\Jobs\CloudHouseController();
            try {
                $cloud::cancelJob($queueres);
                return ['type'=>'success','msg'=>'取消成功'];
            } catch(\Exception $e){
                Redis::Lpush('order_cancel_fail',json_encode($queueres));
                return ['type'=>'fail','msg'=>'取消失败'];
            }
        }
        //取消推送
        Db::table('goods_transfers')->where('is_push', $in_no)->update(['is_push'=>-1]);
        return ['type'=>'success','msg'=>'取消成功'];
    }

    //查看出库订单
    public function ContractOutList($params){

        $out_no = $params['out_no'];
        $list = Db::table('cloudhouse_contract')->where('out_no',$out_no)->get()->map(function($value){
            return(array)$value;
        })->toArray();

        return ['type'=>'success','data'=>$list];
    }

    public function cloudhouse_custom_sku($params){
        $cloud = new \App\Http\Controllers\Jobs\CloudHouseController();
        try {
            $cloud::sendJob($params);
        } catch(\Exception $e){
            Redis::Lpush('send_goods_fail',json_encode($params));
        }
    }

    public function ContractTotalList($params){
        $pagenum = isset($params['page']) ? $params['page'] : 1;
        $limit = isset($params['limit']) ? $params['limit'] : 30;
        $page  =  $pagenum- 1;
        if($page!=0){
            $page  =  $page * $limit;
        }

        $list = DB::table('cloudhouse_contract_total');
        if(!empty($params['contract_no'])){
            $list = $list->where('contract_no','like','%'.$params['contract_no'].'%');
        }
        if(!empty($params['start_time'])){
            $list = $list->where('create_time','>',$params['start_time']);
        }
        if(!empty($params['end_time'])){
            $list = $list->where('create_time','<',$params['end_time']);
        }
        $totalNum = $list->count();
        $list = $list->orderBy('create_time','desc')->offset($page)->limit($limit)->select()->get();
        foreach ($list as $k=>$v){
            $list[$k]->out_num = DB::table('goods_transfers_detail as a')
                ->leftJoin('goods_transfers as b','a.order_no','=','b.order_no')
                ->where('b.type',1)
                ->where('a.contract_no',$v->contract_no)
                ->sum('a.receive_num');
//             DB::connection()->enableQueryLog();
            $production_num = DB::table('goods_transfers_detail as a')
                ->leftJoin('goods_transfers as b','a.order_no','=','b.order_no')
                ->where('b.type',3)
                ->where('a.contract_no',$v->contract_no)
                ->sum('a.receive_num');

            // 判断合同是否逾期
            $goodTransfers = DB::table('goods_transfers')
                ->where('contract_no',$v->contract_no)
                ->where('type', 3)
                ->where('in_house', 4)
                ->where('out_house', 0)
                ->orderBy('createtime')
                ->first();
            $v->is_be_overdue = '已逾期';
            $timestamp = 0;
            if (!empty($goodTransfers)){
                $v->first_place_date = $goodTransfers->sj_arrive_time;
                $timestamp = strtotime($goodTransfers->sj_arrive_time) - strtotime($v->report_date);
            }else{
                $v->first_place_date = '';
                $timestamp = strtotime(date('Y-m-d H:i:s')) - strtotime($v->report_date);
            }
            if ($timestamp <= 0){
                $v->is_be_overdue = '未逾期';
                $v->be_overdue_time = '';
            }else{
                $v->be_overdue_time = $this->timestampConversion(abs($timestamp));
            }

//             print_r(DB::getQueryLog());
            $list[$k]->surplus_num = $v->total_count - $v->out_num>0 ? $v->total_count - $v->out_num : 0;
//            $list[$k]->production_num = $production_num -  $v->out_num>0 ? $production_num - $v->out_num : 0;
            $list[$k]->production_num = $production_num;
        }

        return [
            'data' => $list,
            'totalNum' => $totalNum,
        ];
    }


    public function financeContract(){
        $pagenum = isset($params['page']) ? $params['page'] : 1;
        $limit = isset($params['limit']) ? $params['limit'] : 30;
        $page  =  $pagenum- 1;
        if($page!=0){
            $page  =  $page * $limit;
        }

        $list = DB::table('goods_transfers as a')
            ->leftJoin('cloudhouse_contract_total as b','a.contract_no','=','b.contract_no')
            ->where('anomaly_order','')
            ->where('is_push',3);

        if(!empty($params['contract_no'])){
            $list = $list->where('contract_no','like','%'.$params['contract_no'].'%');
        }
        if(!empty($params['start_time'])){
            $list = $list->where('yj_arrive_time','>',$params['start_time']);
        }
        if(!empty($params['end_time'])){
            $list = $list->where('yj_arrive_time','<',$params['end_time']);
        }
    }

    //出入库记录
    //type_detail(1.仓库调拨入库 2.采购入库 3.手工入库 4.样品退回入库 5.订单退货入库 6.fba出库 7.仓库调拨出库 8.第三方仓出库 9.采购退货出库 10.样品调拨出库 11.手工出库 12.订单出库)
    public function inventoryInOut($params){
        $pagenum = isset($params['page']) ? $params['page'] : 1;
        $limit = isset($params['limit']) ? $params['limit'] : 30;
        $page  =  $pagenum- 1;
        if($page!=0){
            $page  =  $page * $limit;
        }

//        if(!empty($params['warehouse_id'])){
//            if($params['warehouse_id']==2||$params['warehouse_id']==386||$params['warehouse_id']==560){
//                $list = DB::table('saihe_warehouse_log')->where('warehouse_id',$params['warehouse_id']);
//                if(!empty($params['type'])){
//                    if($params['type']==1){
//                        $list = $list->where('type',2);
//                    }elseif($params['type']==2){
//                        $list = $list->where('type',1);
//                    }
//                }
//                if(!empty($params['spu'])){
//                    $spuid = $this->GetSpuId($params['spu']);
//                    $list = $list->where('spu_id',$spuid);
//                }
//                if(!empty($params['custom_sku'])){
//                    $custom_sku_id = $this->GetCustomSkuId($params['custom_sku']);
//                    $list = $list->where('custom_sku_id',$custom_sku_id);
//                }
//                if(!empty($params['start_time'])){
//                    $list = $list->where('operation_date','>',$params['start_time']);
//                }
//                if(!empty($params['end_time'])){
//                    $list = $list->where('operation_date','<',$params['end_time']);
//                }
//                $totalNum = $list->count();
//                $list = $list->offset($page)->limit($limit)->orderBy('operation_date','desc')->select('operation_date as createtime','type','operation_num as num','operation_username','client_sku as custom_sku','spu_id','remark','warehouse_id')->get();
//                foreach ($list as $k=>$v){
//                    $spuRes = $this->GetSpu($v->spu_id);
//                    if($spuRes['old_spu']){
//                        $list[$k]->spu = $spuRes['old_spu'];
//                    }else{
//                        $list[$k]->spu = $spuRes['spu'];
//                    }
//                }
//
//            }elseif($params['warehouse_id']==1){
        //出入库记录
        $list = DB::table('cloudhouse_record');
        if(!empty($params['contract_no'])){
            $order_nolist = DB::table('goods_transfers as a')->leftJoin('goods_transfers_detail as b','a.order_no','=','b.order_no')
//                        ->where(function ($query){
//                            $query->whereIn('in_house',[3,8])->orWhere('out_house',3);
//                        })
                ->where('b.contract_no',$params['contract_no'])->groupBy('a.order_no')->select('a.order_no')->get();
            $order_nolist = json_decode(json_encode($order_nolist),true);
            $order_noArr = array_column($order_nolist,'order_no');
            $list = $list->whereIn('order_no',$order_noArr);
        }
        if(!empty($params['warehouse_id'])){
            $list = $list->where('warehouse_id',$params['warehouse_id']);
        }
        if(!empty($params['type'])){
            $list = $list->where('type',$params['type']);
        }
        if(!empty($params['type_detail'])){
            $list = $list->where('type_detail',$params['type_detail']);
        }
        if(!empty($params['shop_id'])){
            $list = $list->where('shop_id',$params['shop_id']);
        }
        if(!empty($params['spu'])){
            $spuid = $this->GetSpuId($params['spu']);
            $list = $list->where('spu_id',$spuid);
        }
        if(!empty($params['order_no'])){
            $list = $list->where('order_no',$params['order_no']);
        }
        if(!empty($params['custom_sku'])){
            $custom_sku_id = $this->GetCustomSkuId($params['custom_sku']);
            $list = $list->where('custom_sku_id',$custom_sku_id);
        }
        if(!empty($params['start_time'])){
            $list = $list->where('createtime','>',$params['start_time']);
        }
        if(!empty($params['end_time'])){
            $list = $list->where('createtime','<',$params['end_time']);
        }
        $totalNum = $list->count();
        $list = $list->offset($page)->limit($limit)->orderBy('createtime','desc')->get();
        foreach ($list as $k=>$v){
            $list[$k]->contract_no = '';
            $list[$k]->shop_name = '';
            $list[$k]->cn_name = $this->GetCustomSkus($v->custom_sku)['name']??'';
            if($v->type_detail==1||$v->type_detail==2||$v->type_detail==8){
                $transfers = DB::table('goods_transfers_detail')->where('order_no',$v->order_no)->where('custom_sku_id',$v->custom_sku_id)->select('contract_no')->first();
                if(!empty($transfers)){
                    $list[$k]->contract_no = $transfers->contract_no;
                }
            }
            if($v->shop_id){
                $getShop = $this->GetShop($v->shop_id);
                if($getShop){
                    $list[$k]->shop_name = $getShop['shop_name'];
                }
            }

        }
//            }
        return [
            'data' => $list,
            'totalNum' => $totalNum,
        ];

//        }else{
//            return [
//                'data' => '',
//                'totalNum' => 0,
//            ];
//        }

    }

    /**
     * @Desc:获取合同出货列表
     * @param $params
     * @return array
     * @author: Liu Sinian
     * @Time: 2023/3/1 16:32
     */
    public function getContractShipmentList($params)
    {
        $result = $this->_filterContractShipmentList($params);

        return $result;
    }

    /**
     * @Desc:获取合同出货列表
     * @param $params
     * @return array
     * @author: Liu Sinian
     * @Time: 2023/3/1 16:32
     */
    public function _filterContractShipmentList($params)
    {
        $goodsTransfersDetail = $this->goodsTransfersDetailModel::query()
            ->join('goods_transfers as gt', 'gt.order_no', '=', 'goods_transfers_detail.order_no')
            ->where('gt.type', 1);
        // 过滤预计送达时间
        if (isset($params['estimated_delivery_time']) && !empty($params['estimated_delivery_time'])){
            $goodsTransfersDetail->whereBetween('gt.yj_arrive_time', $params['estimated_delivery_time'] ?? []);
        }
        // 实际送达时间
        if (isset($params['actual_delivery_time']) && !empty($params['actual_delivery_time'])){
            $goodsTransfersDetail->whereBetween('gt.sj_arrive_time', $params['actual_delivery_time'] ?? []);
        }
        // 合同号
        if (isset($params['contract_no']) && !empty($params['contract_no'])){
            $goodsTransfersDetail->where('goods_transfers_detail.contract_no', 'like', '%' . $params['contract_no'] . '%');
        }
        // 品名
        if (isset($params['product_name']) && !empty($params['product_name'])){
            $spuIds = $this->contractModel::query()
                ->where('title', 'like', '%'.$params['product_name'])
                ->where('spu_id', '>', 0)
                ->select('spu_id')
                ->get();
            if (!empty($spuIds)){
                $spuIds = array_column($spuIds->toArray(), 'spu_id');
            }else{
                $spuIds = [];
            }
            $goodsTransfersDetail->whereIn('goods_transfers_detail.spu_id', $spuIds);
        }
        // 款号
        if (isset($params['spu']) && !empty($params['spu'])){
            $goodsTransfersDetail->where('goods_transfers_detail.spu', 'like', '%'.$params['spu'].'%');
        }
        // 入库数量
        if (isset($params['instock_num']) && !empty($params['instock_num'])) {
            $goodsTransfersDetail->where('goods_transfers_detail.receive_num', $params['instock_num']);
        }
        // 入库总数量
        if (isset($params['instock_total_num']) && !empty($params['instock_total_num'])) {
            $goodsTransfersDetail->where('gt.receive_total_num', $params['instock_total_num']);
        }
        // 合同单价
        if (isset($params['one_price']) && !empty($params['one_price'])) {
            $spuIds = $this->contractModel::query()
                ->where('one_price', $params['one_price'])
                ->where('spu_id', '>', 0)
                ->select('spu_id')
                ->get();
            if (!empty($spuIds)){
                $spuIds = array_column($spuIds->toArray(), 'spu_id');
            }else{
                $spuIds = [];
            }
            $goodsTransfersDetail->whereIn('goods_transfers_detail.spu_id', $spuIds);
        }
//        // 合同数量
//        if (isset($params['contract_num']) && !empty($params['contract_num'])) {
//            $goodsTransfersDetail->where('cc.count', $params['contract_num']);
//        }
        // 备注
        if (isset($params['remark']) && !empty($params['remark'])){
            $goodsTransfersDetail->where('gt.text', 'like', '%'.$params['remark'].'%');
        }
        // 过滤id
        if (isset($params['id']) && !empty($params['id'])) {
            $goodsTransfersDetail->whereIn('goods_transfers_detail.id', $params['id']);
        }
        // 仓库名称过滤
        if (isset($params['warehouse']) && !empty($params['warehouse'])){
            $goodsTransfersDetail->where('gt.in_house', $params['warehouse']);
        }
        $count = $goodsTransfersDetail->count();
        // 排序
        $goodsTransfersDetail->orderBy('gt.sj_arrive_time', 'desc');
        // 分页
        if (isset($params['limit']) && !empty($params['limit']) && isset($params['page']) && !empty($params['page'])) {
            $limit = $params['limit'] ?? 30;
            $page = $params['page'] ?? 1;
            $pageNum = $page <=1 ? 0 : $limit*($page-1);
            $goodsTransfersDetail->limit($limit)->offset($pageNum);
        }
        $goodsTransfersDetailList = $goodsTransfersDetail
            ->select('gt.*', 'goods_transfers_detail.*')
            ->get()
            ->toArray();

//        $customMdl = $this->customSkuModel::query()
//            ->get();
        $list = [];
        foreach ($goodsTransfersDetailList as $item){
            // 获取合同库存sku合同数量
            $contactCustomSkuModel = $this->contractSkuModel::query()
                ->where('contract_no', $item['contract_no'])
                ->where('custom_sku_id', $item['custom_sku_id'])
                ->first();
            if (!empty($contactCustomSkuModel)){
                $contactModel = $this->contractModel::query()
                    ->where('contract_no', $contactCustomSkuModel->contract_no)
                    ->where('spu_id', $contactCustomSkuModel->spu_id)
                    ->where('color_identifying', $contactCustomSkuModel->color)
                    ->first();
            }
//            $color = '';
//            $size = '';
            $customMdl = $this->GetCustomSkus($item['custom_sku']);
            $colorMdl = $this->GetColorSize($customMdl['color_id']);
            $color = $colorMdl['name'] ?? '';
            $size = $customMdl['size'] ?? '';
//            foreach ($customMdl as $cu){
//                if ($cu->custom_sku_id == $item['custom_sku_id'] || $cu->custom_sku == $item['custom_sku'] || $cu->old_custom_sku == $item['custom_sku']){
//                    $colorMdl = $this->GetColorSize($cu->color_id);
//                    $color = $colorMdl['name'] ?? '';
//                    $size = $cu->size;
//                    break;
//                }
//            }
            $onePrice = $contactModel->one_price ?? 0;
            $contactNum = $contactCustomSkuModel->num ?? 0;
            $list[] = [
                'id' => $item['id'], // 标识
                'contract_no' => $item['contract_no'], // 合同号
                'spu' => $item['spu'], // 款号
                'custom_sku' => $item['custom_sku'], // 库存SKU
                'yj_arrive_time' => $item['yj_arrive_time'], // 送货时间
                'sj_arrive_time' => $item['sj_arrive_time'], // 实际入库时间
                'receive_num' => $item['receive_num'], // 实际入库数量
                'spu_id' => $item['spu_id'],
                'custom_sku_id' => $item['custom_sku_id'],
                'title' => $contactModel->title ?? '', // 品名
                'text' => $item['text'] ?? '', // 备注
//                'total_num' => $item['total_num'],
                'receive_total_num' => $item['receive_total_num'], // 实际入库总数
                'one_price' => $onePrice ? $onePrice : '', // 单价
                'count' => $contactNum ? $contactNum : '', // 数量
                'price' => $onePrice * $contactNum, // 合计价格
                'plant_code' => '', // 工厂代号,
                'warehouse_name' => self::WAREHOUSE[$item['in_house']] ?? '', // 仓库名称
                'color' => $color,
                'size' => $size
            ];
        }

        return ['count' => $count, 'list' => $list];
    }
    public function exportContractShipmentList($params)
    {
//        $goodsTransfersDetail = $this->goodsTransfersDetailModel::query()
//            ->join('goods_transfers as gt', 'gt.order_no', '=', 'goods_transfers_detail.order_no')
//            ->where('gt.type', 1);
//        // 过滤id
//        if (isset($params['id']) && !empty($params['id'])) {
//            $goodsTransfersDetail->whereIn('goods_transfers_detail.id', $params['id']);
//        }
//        $goodsTransfersDetailList = $goodsTransfersDetail
//            ->select('gt.*', 'goods_transfers_detail.*')
//            ->get()
//            ->toArray();
//        $list = [];
//        foreach ($goodsTransfersDetailList as $item){
//            // 获取合同库存sku合同数量
//            $contactCustomSkuModel = $this->contractSkuModel::query()
//                ->where('contract_no', $item['contract_no'])
//                ->where('custom_sku_id', $item['custom_sku_id'])
//                ->first();
//            if (!empty($contactCustomSkuModel)){
//                $contactModel = $this->contractModel::query()
//                    ->where('contract_no', $contactCustomSkuModel->contract_no)
//                    ->where('spu_id', $contactCustomSkuModel->spu_id)
//                    ->where('color_identifying', $contactCustomSkuModel->color)
//                    ->first();
//            }
//            $onePrice = $contactModel->one_price ?? 0;
//            $contactNum = $contactCustomSkuModel->num ?? 0;
//            $list[] = [
//                'id' => $item['id'], // 标识
//                'contract_no' => $item['contract_no'], // 合同号
//                'spu' => $item['spu'], // 款号
//                'custom_sku' => $item['custom_sku'], // 库存SKU
//                'yj_arrive_time' => $item['yj_arrive_time'], // 送货时间
//                'sj_arrive_time' => $item['sj_arrive_time'], // 实际入库时间
//                'receive_num' => $item['receive_num'], // 实际入库数量
//                'spu_id' => $item['spu_id'],
//                'custom_sku_id' => $item['custom_sku_id'],
//                'title' => $contactModel->title ?? '', // 品名
//                'text' => $item['text'] ?? '', // 备注
//                'receive_total_num' => $item['receive_total_num'], // 实际入库总数
//                'one_price' => $onePrice ? $onePrice : '', // 单价
//                'count' => $contactNum ? $contactNum : '', // 数量
//                'price' => $onePrice * $contactNum, // 合计价格
//                'plant_code' => '', // 工厂代号
//                'warehouse_name' => self::WAREHOUSE[$item['in_house']] ?? '' // 入库仓库
//            ];
//        }
        $data = $this->getContractShipmentList($params);
        $list = $data['list'];
        // excel文件表头
        $excelName = '合同出货列表导出'.time();
        // 表头
        $excelTitle = [
            'yj_arrive_time' => '送货日期',
            'sj_arrive_time' => '实际到货日期',
            'warehouse_name' => '入库仓库',
            'plant_code' => '工厂代号',
            'contract_no' => '合同号',
            'title' => '品名',
            'spu' => '款号',
            'color' => '颜色',
            'size' => '尺码',
            'receive_num' => '入库数量',
            'one_price' => '合同单价',
//            'receive_total_num' => '入库总额',
            'count' => '合同数量',
            'price' => '合同总额',
            'text' => '备注',
        ];
        // 组织导出列表数据
        $excelData = [
            'data' => $list,
            'title_list' => $excelTitle,
            'title' => $excelName,
            'type' => '合同出货列表导出',
            'user_id' => $params['user_id']
        ];

        return $excelData;
    }
}