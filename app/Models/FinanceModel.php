<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Jobs\SaveFinanceBillPdfController;
use App\Models\Common\Constant;
use App\Models\Helper\IdBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FinanceModel extends BaseModel
{
    protected $idBuilder;
    public function __construct()
    {
        $this->idBuilder = new IdBuilder();
    }

    public function savePaymentBill($params)
    {
        try {
            if (!isset($params['bill_type'])){
                throw new \Exception('未给定票据类型！');
            }
            db::beginTransaction();
            switch ($params['bill_type']){
                case 1:
                    // 预付账款
                    $params = $this->_checkAdvancePaymentBill($params);
                    break;
                case 2:
                    // 应付账款
                    $params = $this->_checkAccountsPaymentBill($params);
                    break;
                case 3:
                    // 附加费
                    $params = $this->_checkSurchargeBill($params);
                    break;
                case 4:
                    // 延期扣款
                    $params = $this->_checkDelayedBill($params);
                    break;
                default:
                    throw new \Exception('票据类型非预设值！');
            }
            $billNo = '';
            if (isset($params['bill_id'])){
                $save = [
                    'application_reason' => $params['application_reason'],
//                    'bill_type' => $params['bill_type'],
                    'actual_payment' => $this->floorDecimal($params['actual_payment'], 2),
                    'payee' => $params['payee'],
                    'receiving_bank_card_number' => $params['receiving_bank_card_number'],
                    'payer' => $params['payer'],
                    'payment_bank_card_number' => $params['payment_bank_card_number'],
                    'is_payment' => $params['is_payment'] ?? '',
                    'payment_date' => $params['payment_date'] ?? '',
                    'memo' => $params['memo'] ?? '',
                    'deduction_amount' => $params['deduction_amount'] ?? 0.00,
                    'deduction_ratio' => $params['deduction_ratio'] ?? 0.00,
                    'is_disposable_deduction' => $params['is_disposable_deduction'] ?? 0,
                    'updated_at' => date('Y-m-d H:i:s'),
                    'bind_ids' => json_encode($params['bind_ids'] ?? []),
                    'unit_price' => $params['unit_price'] ?? 0.00,
                    'pricing_quantity' => $params['pricing_quantity'] ?? 0.00,
                    'user_id' => $params['user_id'],
                    'receiving_bank_name' => $params['receiving_bank_name'] ?? '',
                    'payment_bank_name' => $params['payment_bank_name'] ?? '',
                ];

                $update = db::table('financial_bill')->where('id', $params['bill_id'])->update($save);
                if (empty($update)){
                    throw new \Exception('数据无变化！');
                }
                $billNo = $params['bill_no'];
                if ($params['bill_type'] == 2) {
                    // 删除旧抵扣记录
                    $delete = db::table('financial_bill_deduction')
                        ->where('bill_no', $billNo)
                        ->delete();
                    // 更新入库信息
                    $billItems = db::table('financial_bill_items')
                        ->where('bill_no', $billNo)
                        ->get();
                    $error = '';
                    foreach ($billItems as $v){
                        $update = db::table('goods_transfers_detail')
                            ->where('id', $v->goods_transfers_detail_id)
                            ->update(['is_settled' => 0]);
                        if (empty($update)){
                            $error .= '合同号：'.$v->contract_no.'，库存skuId：'.$v->custom_sku_id.'该条入库数据退单状态变更失败！';
                            continue;
                        }
                    }
                    if (!empty($error)){
                        throw new \Exception($error);
                    }
                    // 删除入库详情
                    $delete = db::table('financial_bill_items')
                        ->where('bill_no', $billNo)
                        ->delete();
                }
                if ($params['bill_type'] == 4){
                    // 更新入库信息
                    $billItems = db::table('financial_bill_items')
                        ->where('bill_no', $billNo)
                        ->get();
                    $error = '';
                    foreach ($billItems as $v){
                        $update = db::table('goods_transfers_detail')
                            ->where('id', $v->goods_transfers_detail_id)
                            ->update(['is_created_delayed' => 0]);
                        if (empty($update)){
                            $error .= '合同号：'.$v->contract_no.'，库存skuId：'.$v->custom_sku_id.'该条入库数据退单状态变更失败！';
                            continue;
                        }
                    }
                    if (!empty($error)){
                        throw new \Exception($error);
                    }
                    // 删除入库详情
                    $delete = db::table('financial_bill_items')
                        ->where('bill_no', $billNo)
                        ->delete();
                }
            }else {
                switch ($params['bill_type']){
                    case 1:
                        // 预付账款
                        $billNo = $this->idBuilder->createId('PREPAYMENT_BILL_BN');
                        $status = 0;
                        break;
                    case 2:
                        // 应付账款
                        $billNo = $this->idBuilder->createId('ACCOUNTS_PAYMENT_BILL_BN');
                        $status = 0;
                        break;
                    case 3:
                        // 附加费
                        $billNo = $this->idBuilder->createId('SURCHARGE_BILL_BN');
                        $status = 2;
                        break;
                    case 4:
                        // 延期扣款单
                        $billNo = $this->idBuilder->createId('DELAYED_BILL_BN');
                        $status = 2;
                        break;
                    default:
                        throw new \Exception('票据类型非预设值！');
                }
                if (empty($billNo)){
                    throw new \Exception(Constant::BILL_TYPE[$params['bill_type']].'票据编号自动生成失败！');
                }
                $save = [
                    'bill_no' => $billNo,
                    'application_reason' => $params['application_reason'],
                    'contract_no' => $params['contract_no'],
                    'bill_type' => $params['bill_type'],
                    'status' => $status,
                    'ratio' => $params['ratio'] ?? 0.00,
                    'payable_amount' => $this->floorDecimal($params['payable_amount'], 2),
                    'actual_payment' => $this->floorDecimal($params['actual_payment'] ?? 0, 2),
                    'payee' => $params['payee'],
                    'receiving_bank_card_number' => $params['receiving_bank_card_number'],
                    'payer' => $params['payer'],
                    'payment_bank_card_number' => $params['payment_bank_card_number'],
                    'is_payment' => 0,
                    'payment_date' => '',
                    'memo' => $params['memo'] ?? '',
                    'created_at' => date('Y-m-d H:i:s'),
                    'deduction_amount' => $params['deduction_amount'] ?? 0.00,
                    'deduction_ratio' => $params['deduction_ratio'] ?? 0.00,
                    'is_disposable_deduction' => $params['is_disposable_deduction'] ?? 0,
                    'bind_ids' => json_encode($params['bind_ids'] ?? []),
                    'unit_price' => $params['unit_price'] ?? 0.00,
                    'pricing_quantity' => $params['pricing_quantity'] ?? 0.00,
                    'user_id' => $params['user_id'],
                    'company_id' => $params['company_id'],
                    'company_name' => $params['company_name'],
                    'supplier_id' => $params['supplier_id'],
                    'supplier_name' => $params['supplier_name'],
                    'receiving_bank_name' => $params['receiving_bank_name'] ?? '',
                    'payment_bank_name' => $params['payment_bank_name'] ?? '',
                ];
                if ($params['bill_type'] == 4){
                    $save['status'] = 2;
                }
                $billId = db::table('financial_bill')->insertGetId($save);
                if (empty($billId)){
                    throw new \Exception('未生成票据！');
                }
            }
            // 预付款单
            if ($params['bill_type'] == 1){
                // 更新票据信息
                foreach ($params['bind_ids'] as $bindId){
                    $update = db::table('cloudhouse_contract_bill_bind')->where('id', $bindId)->update(['bill_no' => $billNo,'is_push' => 1]);
                    if (empty($update)){
                        throw new \Exception('绑定票据信息更新推单状态失败！');
                    }
                }
            }
            // 应付款单
            if ($params['bill_type'] == 2){
                // 新增抵扣记录
                if (isset($params['deduction_log']) && !empty($params['deduction_log'])){
                    foreach ($params['deduction_log'] as &$v){
                        $v['bill_no'] = $billNo;
                        $logId = db::table('financial_bill_deduction')->insertGetId($v);
                        if (empty($logId)){
                            throw new \Exception('抵扣记录保存失败！');
                        }
                    }
                }

                // 更新入库详情推单状态
                foreach ($params['in_house_items'] as $v){
                    $update = db::table('goods_transfers_detail')
                        ->where('id', $v['goods_transfers_detail_id'])
                        ->update(['is_settled' => 1]);
                    if (empty($update)){
                        $error .= '合同号：'.$v['contract_no'].'，库存skuId：'.$v['custom_sku_id'].'该条入库数据是否创建应付账单状态变更失败！';
                        continue;
                    }
                    $save = [
                        'bill_no' => $billNo,
                        'contract_no' => $v['contract_no'],
                        'custom_sku_id' => $v['custom_sku_id'],
                        'custom_sku' => $v['custom_sku'],
                        'spu_id' => $v['spu_id'],
                        'spu' => $v['spu'],
                        'num' => $v['num'],
                        'price' => $v['price'],
                        'goods_transfers_detail_id' => $v['goods_transfers_detail_id'],
                        'delivery_time' => $v['delivery_time'],
                        'in_out_house_time' => $v['in_out_house_time'],
                        'in_out_house_type' => $v['in_out_house_type'],
                        'color_identifying' => $v['color_identifying'],
                        'color_name' => $v['color_name'],
                        'name' => $v['name'],
                        'platform_id' => $v['platform_id'],
                        'platform_name' => $v['platform_name'],
                    ];
                    // 保存票据明细
                    $insert = db::table('financial_bill_items')->insertGetId($save);
                    if (empty($insert)){
                        $error .= '合同号：'.$v['contract_no'].'，库存skuId：'.$v['custom_sku_id'].'该条入库数据新增票据明细失败！';
                        continue;
                    }
                }
                if (!empty($error)){
                    throw new \Exception($error);
                }
            }

            // 延期扣款单
            if ($params['bill_type'] == 4){
                // 更新入库详情推单状态
                foreach ($params['in_house_items'] as $v){
                    $update = db::table('goods_transfers_detail')
                        ->where('id', $v['goods_transfers_detail_id'])
                        ->update(['is_created_delayed' => 1]);
                    if (empty($update)){
                        $error .= '合同号：'.$v['contract_no'].'，库存skuId：'.$v['custom_sku_id'].'该条入库数据是否创建延期扣款单状态变更失败！';
                        continue;
                    }
                    $save = [
                        'bill_no' => $billNo,
                        'contract_no' => $v['contract_no'],
                        'custom_sku_id' => $v['custom_sku_id'],
                        'custom_sku' => $v['custom_sku'],
                        'spu_id' => $v['spu_id'],
                        'spu' => $v['spu'],
                        'num' => $v['num'],
                        'price' => $v['price'],
                        'goods_transfers_detail_id' => $v['goods_transfers_detail_id'],
                        'delivery_time' => $v['delivery_time'],
                        'in_out_house_time' => $v['in_out_house_time'],
                        'in_out_house_type' => $v['in_out_house_type'],
                        'color_identifying' => $v['color_identifying'],
                        'color_name' => $v['color_name'],
                        'name' => $v['name'],
                        'platform_id' => $v['platform_id'],
                        'platform_name' => $v['platform_name'],
                        'delayed_price' => $v['delayed_price'],
                        'delayed_day_num' => $v['delayed_day_num'],
                    ];
                    // 保存票据明细
                    $insert = db::table('financial_bill_items')->insertGetId($save);
                    if (empty($insert)){
                        $error .= '合同号：'.$v['contract_no'].'，库存skuId：'.$v['custom_sku_id'].'该条入库数据新增票据明细失败！';
                        continue;
                    }
                }
                if (!empty($error)){
                    throw new \Exception($error);
                }
            }
            $billTypeName = Constant::BILL_TYPE[$params['bill_type']];
            $data = [
                'title' => '财务票据_'.$billTypeName.'_'.$billNo,
                'type' => '财务票据_'.$billTypeName,
                'user_id' => $params['user_id'],
                'id' => $billId
            ];
            $jobMdl = new SaveFinanceBillPdfController();
            $jobMdl::runJob($data);

            db::commit();

            return ['code' => 200, 'msg' => '保存成功！票据号：'.$billNo];
        }catch (\Exception $e){
            DB::rollback();

            return ['code' => 500, 'msg' => '保存失败！原因：'.$e->getMessage().'，'.$e->getLine()];
        }
    }

    /**
     * @Desc:预付账单数据校验
     * @param $params
     * @return mixed
     * @throws \Exception
     * @author: Liu Sinian
     * @Time: 2023/8/3 17:12
     */
    private function _checkAdvancePaymentBill($params)
    {
        // 判断是否给定创建人
        if (!isset($params['user_id']) || empty($params['user_id'])){
            if (isset($params['token']) && !empty($params['token'])){
                $users = db::table('users')->where('token',$params['token'])->first();
                if($users){
                    $params['user_id'] = $users->Id;
                }else{
                    throw new \Exception('未给定创建人标识！');
                }
            }else{
                throw new \Exception('未给定创建人标识！');
            }
        }

        if (isset($params['bill_id']) && !empty($params['bill_id'])){
            $bill = db::table('financial_bill')->where('id', $params['bill_id'])->first();
            if (empty($bill)){
                throw new \Exception('票据不存在！');
            }
            if ($bill->user_id != $params['user_id']){
                throw new \Exception('非本人创建的单据不予修改！');
            }
            if (!in_array($bill->status, [-2, -1, 0])){
                throw new \Exception('票据非 审核被拒/已撤销/待提交 状态不予修改！');
            }
            // 恢复绑定数据推单状态
            $bindIds = json_decode($bill->bind_ids, true);
            $update = db::table('cloudhouse_contract_bill_bind')->whereIn('id', $bindIds)->update(['bill_no' => '','is_push' => 0]);
            if (empty($update)){
                throw new \Exception('绑定票据信息恢复推单状态失败！');
            }
            $params['bill_no'] = $bill->bill_no;
        }

        if (!isset($params['payee']) || empty($params['payee'])){
            throw new \Exception('未给定收款人/单位！');
        }
        if (!isset($params['receiving_bank_card_number']) || empty($params['receiving_bank_card_number'])){
            throw new \Exception('未给定收款银行卡号！');
        }
        if (!isset($params['payer']) || empty($params['payer'])){
            throw new \Exception('未给定付款人/单位！');
        }
        if (!isset($params['payment_bank_card_number']) || empty($params['payment_bank_card_number'])){
            throw new \Exception('未给定付款银行卡号！');
        }
        if (!isset($params['receiving_bank_name']) || empty($params['receiving_bank_name'])){
            throw new \Exception('未给定收款方开户行！');
        }
        if (!isset($params['payment_bank_name']) || empty($params['payment_bank_name'])){
            throw new \Exception('未给定付款方开户行！');
        }

        if (isset($params['contract_no']) && !empty($params['contract_no'])){
            $contractMdl = db::table('cloudhouse_contract_total')->where('contract_no', $params['contract_no'])->first();
            if (!empty($contractMdl)){
                if ($contractMdl->contract_status != 2){
                    throw new \Exception('合同号：'.$contractMdl->contract_no.'该合同非审批通过状态！');
                }
                // 供应商名称
                $params['supplier_name'] = $contractMdl->supplier_short_name;
                $params['supplier_id'] = $contractMdl->supplier_id;
                $params['company_name'] = $contractMdl->company_name;
                $params['company_id'] = $contractMdl->company_id;
                // 校验比率
                if (!isset($params['payment_information']) || empty($params['payment_information'])){
                    throw new \Exception('未选择合同款项信息！');
                }else{
                    $bindIds = [];
                    $ratio = 0;
                    $applicationReason = [];
                    $error = '';
                    foreach ($params['payment_information'] as $v){
                        if (!isset($v['ratio']) || $v['ratio'] <= 0){
                            $error .=  '申请事由：'.$v['application_reason'].'未给定预付比率或数值不正确！';
                            continue;
                        }
                        if (!isset($v['application_reason']) || empty($v['application_reason'])){
                            $error .= '未给定申请事由！';
                            continue;
                        }
                        if (!isset($v['bind_id']) || empty($v['bind_id'])){
                            $error .=  '申请事由：'.$v['application_reason'].'未给定票据绑定标识！';
                            continue;
                        }else{
                            $billBindMdl = db::table('cloudhouse_contract_bill_bind')->where('id', $v['bind_id'])->first();
                            if (empty($billBindMdl)){
                                $error .=  '申请事由：'.$v['application_reason'].'未查询到票据绑定信息！';
                                continue;
                            }
                            if ($billBindMdl->is_push != 0){
                                $error .= '申请事由：'.$v['application_reason'].'，该条款项已推单，不可重复推单！';
                                continue;
                            }
                            if ($billBindMdl->contract_no != $params['contract_no']){
                                $error .= '申请事由：'.$v['application_reason'].'，该条款项绑定的合同号('.$billBindMdl->contract_no.')非请款单合同号('.$params['contract_no'].')！';
                                continue;
                            }
                        }
                        $ratio += $v['ratio'];
                        if ($ratio > 100){
                            $error .= '申请事由：'.$v['application_reason'].'，比率超过100%！';
                            continue;
                        }

                        if (in_array($v['bind_id'], $bindIds)){
                            $error .= '申请事由：'.$v['application_reason'].'，该条款项已存在，不可多次推单！';
                            continue;
                        }
                        $bindIds[] = $v['bind_id'];

                        $applicationReason[] = $v['application_reason'];
                    }
                    if (!empty($error)){
                        throw new \Exception($error);
                    }
                    $params['bind_ids'] = $bindIds;
                    $params['application_reason'] = implode('&', $applicationReason);
                    $params['ratio'] = $ratio;
                }
                $params['contract_total_price'] = $contractMdl->total_price;
                $params['payable_amount'] = ($params['ratio']/100)*$params['contract_total_price'];
                $params['actual_payment'] = $params['payable_amount']; // 默认预付金额等于实付金额
            }else{
                throw new \Exception('该合同数据不存在！');
            }
        }else{
            throw new \Exception('未给定合同号！');
        }

        return $params;
    }

    /**
     * @Desc: 供应商附加费数据校验
     * @param $params
     * @return mixed
     * @throws \Exception
     * @author: Liu Sinian
     * @Time: 2023/8/3 17:18
     */
    private function _checkSurchargeBill($params)
    {
        // 判断是否给定创建人
        if (!isset($params['user_id']) || empty($params['user_id'])){
            if (isset($params['token']) && !empty($params['token'])){
                $users = db::table('users')->where('token',$params['token'])->first();
                if($users){
                    $params['user_id'] = $users->Id;
                }else{
                    throw new \Exception('未给定创建人标识！');
                }
            }else{
                throw new \Exception('未给定创建人标识！');
            }
        }
        if (isset($params['bill_id']) && !empty($params['bill_id'])){
            $bill = db::table('financial_bill')->where('id', $params['bill_id'])->first();
            if (empty($bill)){
                throw new \Exception('票据不存在！');
            }
            if ($bill->user_id != $params['user_id']){
                throw new \Exception('非本人创建的单据不予修改！');
            }
            if (!in_array($bill->status, [-2, -1, 0])){
                throw new \Exception('票据非 审核被拒/已撤销/待提交 状态不予修改！');
            }
        }
        if (!isset($params['application_reason']) || empty($params['application_reason'])){
            throw new \Exception('未给定申请事由！');
        }
        if (!isset($params['payee']) || empty($params['payee'])){
            throw new \Exception('未给定收款人/单位！');
        }
        if (!isset($params['receiving_bank_card_number']) || empty($params['receiving_bank_card_number'])){
            throw new \Exception('未给定收款银行卡号！');
        }
        if (!isset($params['payer']) || empty($params['payer'])){
            throw new \Exception('未给定付款人/单位！');
        }
        if (!isset($params['payment_bank_card_number']) || empty($params['payment_bank_card_number'])){
            throw new \Exception('未给定付款银行卡号！');
        }
        if (!isset($params['receiving_bank_name']) || empty($params['receiving_bank_name'])){
            throw new \Exception('未给定收款方开户行！');
        }
        if (!isset($params['payment_bank_name']) || empty($params['payment_bank_name'])){
            throw new \Exception('未给定付款方开户行！');
        }
        if (isset($params['contract_no']) && !empty($params['contract_no'])){
            if (!is_array($params['contract_no'])){
                throw new \Exception('给定的合同号数据格式类型不正确！');
            }
            // 查找合同数据
            $contractMdl = db::table('cloudhouse_contract_total')->whereIn('contract_no', $params['contract_no'])->get();
            $contractList = [];
            foreach ($contractMdl as $contract){
                $contractList[$contract->contract_no] = $contract;
                // 供应商名称
                $params['supplier_name'] = $contract->supplier_short_name;
                $params['supplier_id'] = $contract->supplier_id;
                $params['company_name'] = $contract->company_name;
                $params['company_id'] = $contract->company_id;
            }
            // 校验合同数据
            $error = '';
            foreach ($params['contract_no'] as $contractNo){
                $contract = $contractList[$contractNo] ?? [];
                if (empty($contract)){
                    $error .= '合同号：'.$contractNo.'合同数据不存在！';
                    continue;
                }
                // 如果合同完结，则不可新增单据
                if ($contract->contract_status != 2){
                    $error .= '合同号：'.$contractNo.'该合同非审批通过状态！';
                    continue;
                }
                // 判断合同是否完结
                if ($contract->is_end != 0){
                    $error .= '合同号：'.$contractNo.'该合同已完结，不可操作数据！';
                    continue;
                }
            }
            if (!empty($error)){
                throw new \Exception($error);
            }
            $params['contract_no'] = implode(',', $params['contract_no']);
        }else{
            throw new \Exception('未给定合同号！');
        }
        if (!isset($params['unit_price']) || $params['unit_price'] < 0){
            throw new \Exception('单价金额不合法！');
        }
        if (!isset($params['pricing_quantity']) || $params['pricing_quantity'] <0){
            throw new \Exception('计价数量不合法！');
        }
        if (!isset($params['payable_amount']) || $params['payable_amount'] <0){
            throw new \Exception('预付金额不合法！');
        }else{
            if (isset($params['type'])){
                // type: 1:收入（供应商支付给我司） 2：支出（我司付给供应商）
                $payableAmount = bcmul($params['unit_price'], $params['pricing_quantity'], 2);
                if ($params['type'] == 2){
                    $payableAmount = bcmul(-1, $payableAmount, 2);
                }
            }else{
                throw new \Exception('未设置收支类型！');
            }
            $params['payable_amount'] = $payableAmount;
            $params['actual_payment'] = $params['payable_amount']; // 默认预付金额等于实付金额
        }

        return $params;
    }

    /**
     * @Desc:应付账数据校验
     * @param $params
     * @return mixed
     * @throws \Exception
     * @author: Liu Sinian
     * @Time: 2023/8/3 17:13
     */
    private function _checkAccountsPaymentBill($params)
    {
        // 判断是否给定创建人
        if (!isset($params['user_id']) || empty($params['user_id'])){
            if (isset($params['token']) && !empty($params['token'])){
                $users = db::table('users')->where('token',$params['token'])->first();
                if($users){
                    $params['user_id'] = $users->Id;
                }else{
                    throw new \Exception('未给定创建人标识！');
                }
            }else{
                throw new \Exception('未给定创建人标识！');
            }
        }
        // 更新
        if (isset($params['bill_id']) && !empty($params['bill_id'])){
            $bill = db::table('financial_bill')->where('id', $params['bill_id'])->first();
            if (empty($bill)){
                throw new \Exception('票据不存在！');
            }
            if ($bill->user_id != $params['user_id']){
                throw new \Exception('非本人创建的单据不予修改！');
            }
            if (!in_array($bill->status, [-2, -1, 0])){
                throw new \Exception('票据非 审核被拒/已撤销/待提交 状态不予修改！');
            }
        }
        if (!isset($params['application_reason']) || empty($params['application_reason'])){
            throw new \Exception('未给定申请事由！');
        }
        if (!isset($params['is_disposable_deduction'])){
            throw new \Exception('未设置是否一次性抵扣！');
        }
        if (!isset($params['payee']) || empty($params['payee'])){
            throw new \Exception('未给定收款人/单位！');
        }
        if (!isset($params['receiving_bank_card_number']) || empty($params['receiving_bank_card_number'])){
            throw new \Exception('未给定收款银行卡号！');
        }
        if (!isset($params['payer']) || empty($params['payer'])){
            throw new \Exception('未给定付款人/单位！');
        }
        if (!isset($params['payment_bank_card_number']) || empty($params['payment_bank_card_number'])){
            throw new \Exception('未给定付款银行卡号！');
        }
        if (!isset($params['receiving_bank_name']) || empty($params['receiving_bank_name'])){
            throw new \Exception('未给定收款方开户行！');
        }
        if (!isset($params['payment_bank_name']) || empty($params['payment_bank_name'])){
            throw new \Exception('未给定付款方开户行！');
        }
        if (isset($params['contract_no']) && !empty($params['contract_no'])){
            $contractMdl = db::table('cloudhouse_contract_total')->where('contract_no', $params['contract_no'])->first();
            if (!empty($contractMdl)){
                if ($contractMdl->contract_status != 2){
                    throw new \Exception('合同号：'.$contractMdl->contract_no.'该合同非审批通过状态！');
                }
                // 供应商名称
                $params['supplier_name'] = $contractMdl->supplier_short_name;
                $params['supplier_id'] = $contractMdl->supplier_id;
                $params['company_name'] = $contractMdl->company_name;
                $params['company_id'] = $contractMdl->company_id;
            }else{
                throw new \Exception('该合同数据不存在！');
            }
        }else{
            throw new \Exception('未给定合同号！');
        }
        if (!isset($params['in_house_items']) || empty($params['in_house_items'])){
            throw new \Exception('未给定入库明细！');
        }else{
            // 应付金额 = 入库货值
            $totalAccount = 0;
            $error = '';
            $customSkuList = [];
            foreach ($params['in_house_items'] as $k => $v){
                $customSku = $v['custom_sku'] ?? '';
                if (empty($customSku)){
                    $error .= '第'.($k+1).'行入库数据未给定库存SKU！';
                    continue;
                }
                if (!isset($v['custom_sku_id']) || empty($v['custom_sku_id'])){
                    $error .= '第'.($k+1).'行库存SKU：'.$customSku.'未给定库存SKU标识！';
                    continue;
                }
                if (!isset($v['spu']) || empty($v['spu'])){
                    $error .= '第'.($k+1).'行库存SKU：'.$customSku.'未给定SPU！';
                    continue;
                }
                if (!isset($v['spu_id']) || empty($v['spu_id'])){
                    $error .= '第'.($k+1).'行库存SKU：'.$customSku.'未给定SPU标识！';
                    continue;
                }
                if (!isset($v['num']) || $v['num'] < 0){
                    $error .= '第'.($k+1).'行库存SKU：'.$customSku.'数量未给定或不合法！';
                    continue;
                }
                if (!isset($v['price']) || $v['price'] <= 0){
                    $error .= '第'.($k+1).'行库存SKU：'.$customSku.'单价未给定或不合法！';
                    continue;
                }
                if (!isset($v['delivery_time']) || empty($v['delivery_time'])){
                    $error .= '第'.($k+1).'行库存SKU：'.$customSku.'未给定送达时间！';
                    continue;
                }
                if (!isset($v['in_out_house_time']) || empty($v['in_out_house_time'])){
                    $error .= '第'.($k+1).'行库存SKU：'.$customSku.'未给定实际出入库时间！';
                    continue;
                }
                if (!isset($v['in_out_house_type']) || empty($v['in_out_house_type'])){
                    $error .= '第'.($k+1).'行库存SKU：'.$customSku.'未给定出入库类型！';
                    continue;
                }
                if (!isset($v['contract_no']) || empty($v['contract_no'])){
                    $error .= '第'.($k+1).'行库存SKU：'.$customSku.'未给定合同号！';
                    continue;
                }
                if (!isset($v['total_price']) || $v['total_price'] < 0){
                    $error .= '第'.($k+1).'行库存SKU：'.$customSku.'未给定合计价格！';
                    continue;
                }
                if (!isset($v['type_detail']) || empty($v['type_detail'])){
                    $error .= '第'.($k+1).'行库存SKU：'.$customSku.'未给定出入库类型！';
                    continue;
                }
                if (!isset($v['goods_transfers_detail_id']) || empty($v['goods_transfers_detail_id'])){
                    $error .= '第'.($k+1).'行库存SKU：'.$customSku.'未给定入库详情标识！';
                    continue;
                }
                if (!isset($v['platform_id']) || empty($v['platform_id'])) {
                    $error .= '第' . ($k + 1) . '行库存SKU：' . $customSku . '未给定平台标识！';
                    continue;
                }
                if (!isset($v['platform_name']) || empty($v['platform_name'])) {
                    $error .= '第' . ($k + 1) . '行库存SKU：' . $customSku . '未给定平台名称！';
                    continue;
                }
                // 单商品合计价格
                $totalPrice = $v['total_price'];
                // 数量
                $inHouseNum = 0; // 入库数量
                $outHouseNum = 0; // 出库数量
                if ($v['type_detail'] == 2){
                    // 入库总货值
                    $totalAccount += $totalPrice;
                    $inHouseNum = $v['num'];
                }elseif ($v['type_detail'] == 9){
                    $totalAccount -= $totalPrice;
                    $outHouseNum = $v['num'];
                }
                if (isset($customSkuList[$v['custom_sku_id']])){
                    $customSkuList[$v['custom_sku_id']]['in_house_num'] += $inHouseNum;
                    $customSkuList[$v['custom_sku_id']]['out_house_num'] += $outHouseNum;
                }else{
                    $customSkuList[$v['custom_sku_id']] = [
                        'custom_sku' => $v['custom_sku'],
                        'in_house_num' => $inHouseNum,
                        'out_house_num' => $outHouseNum
                    ];
                }
            }
            if (!empty($error)){
                throw new \Exception('入库详情参数有误！详情：'.$error);
            }
            $error = '';
            foreach ($customSkuList as $v){
                $num = $v['in_house_num'] - $v['out_house_num'];
                if ($num < 0){
                    $error .= '库存SKU：'.$v['custom_sku'].'入库数量小于出库数量，入库数量：'.$v['in_house_num'].'，出库数量：'.$v['out_house_num'].'，差额为：'.$num.'；';
                    continue;
                }
            }
            if (!empty($error)){
                throw new \Exception('入库商品数值有误！详情：'.$error);
            }
            if ($totalAccount <= 0){
                throw new \Exception('入库商品总货值小于等于零，具体数额为：'.$totalAccount);
            }
            // 获取该合同历史应付账单
            $historyBillMdl = db::table('financial_bill')
                ->where('contract_no', $contractMdl->contract_no)
                ->where('bill_type', 2)
                ->where('status', '<>', -3);
            if (isset($params['bill_id']) && !empty($params['bill_id'])){
                // 更新时过滤自身
                $historyBillMdl = $historyBillMdl->where('id', '<>', $params['bill_id']);
            }
            $historyBillMdl = $historyBillMdl->get();
            $totalPayableAmount = 0;
            if ($historyBillMdl->isNotEmpty()){
                $historyBillMdl = $historyBillMdl->toArrayList();
                $totalPayableAmount = array_sum(array_column($historyBillMdl, 'payable_amount')); // 历史应付账单应付金额合计
                $total = $totalPayableAmount + $totalAccount; // 历史合计货值 + 新入库货值
            }else{
                $total = $totalAccount;
            }
            $contractTotalPrice = $contractMdl->total_price; // 合同总额

            $actualPayment = $totalAccount;// 实付金额 默认等于入库货值
            if ($total > $contractTotalPrice){
                // 取消金额限制 2023-12-13
//                throw new \Exception('入库货值大于合同货值总额，历史单据总额：'.$totalPayableAmount.'，入库货值总额：'.$totalAccount.'，合同总额：'.$contractTotalPrice.'，请检查数据！');
            }

            // 货值比率
            $ratio = bcdiv($totalAccount, $contractTotalPrice, 2);
            // 预付款单抵扣比率
            if ($params['is_disposable_deduction'] == 0){ // 非一次性抵扣
                $deductionRatio = $ratio;
            }else{ // 一次性抵扣
                $deductionRatio = 1;
            }
            // 抵扣比率不可超过100%
            if ($deductionRatio > 1){
                $deductionRatio = 1;
            }
            $params['ratio'] = bcmul($ratio, 100, 2);
            $params['deduction_ratio'] = bcmul($deductionRatio, 100, 2);
            // 获取抵扣记录
            $billDeductionMdl = db::table('financial_bill_deduction')
                ->where('contract_no', $contractMdl->contract_no)
                ->where('is_deleted', 0)
                ->get();
            $billDeductionList = [];
            // 根据类型来统计 1：预付款（定金抵扣） 3：附加费 4：延期扣款
            foreach ($billDeductionMdl as $v){
                // 统计抵扣金额
                if (isset($billDeductionList[$v->deduction_bill_type])){
                    $billDeductionList[$v->deduction_bill_type] += $v->deduction_amount;
                }else{
                    $billDeductionList[$v->deduction_bill_type] = $v->deduction_amount;
                }
            }
            // 待保存抵扣记录
            $deductionLog = [];

            // 预付款单抵扣
            $advancePaymentBillMdl = db::table('financial_bill')
                ->where('contract_no', $params['contract_no'])
                ->whereIn('status', [2, 3])
                ->where('bill_type', 1)
                ->where('is_payment', 1)
                ->get();
            if ($advancePaymentBillMdl->isNotEmpty()){
                $advancePaymentBillMdl = $advancePaymentBillMdl->toArrayList();
                $totalDeposit = array_sum(array_column($advancePaymentBillMdl, 'actual_payment')); // 定金合计
                $addvanceBillNo = implode(',', array_column($advancePaymentBillMdl, 'bill_no'));
                $totalDeducted = $billDeductionList[1] ?? 0; // 已被抵扣金额
                $remainingDeduction = $totalDeposit - $totalDeducted; // 剩余可抵扣金额
                // 剩余抵扣金额大于0即可抵扣
                if ($remainingDeduction > 0 && $actualPayment > 0){
                    // 该次定金抵扣金额 按 实际入库货值比率/100% 抵扣
                    $deductionAccount = bcmul($remainingDeduction, $deductionRatio, 4);
                    if ($deductionAccount >= $actualPayment){
                        // 可抵扣金额大于实付金额，则可抵扣金额 = 实付金额
                        $deductionAccount = $actualPayment;
                    }
                    // 保存抵扣记录
                    $deductionLog[] = [
                        'contract_no' => $contractMdl->contract_no,
                        'deduction_amount' => $deductionAccount,
                        'deduction_ratio' => $params['deduction_ratio'],
                        'base_ammount' => $remainingDeduction,
                        'deduction_bill_type' => 1,
                        'origin_bill_no' => $addvanceBillNo,
                        'created_at' => date('Y-m-d H:i:s'),
                    ];
                    $actualPayment -= (float)$deductionAccount; // 抵扣实付金额
                }elseif ($remainingDeduction < 0){
                    throw new \Exception('合同号：'.$contractMdl->contract_no.'票据号：'.$addvanceBillNo.'，预付款单总金额小于已抵扣金额，请检查数据！');
                }
            }
            // 附加费抵扣
            $surchargeBillMdl = db::table('financial_bill')
                ->where('contract_no', 'like', '%'.$params['contract_no'].'%')
                ->where('bill_type', 3)
                ->where('status', 2)
                ->get();
            if ($surchargeBillMdl->isNotEmpty()){
                $surchargeBillMdl = $surchargeBillMdl->toArrayList();
                $totalDeposit = array_sum(array_column($surchargeBillMdl, 'actual_payment')); // 附加费合计
                $surchargeBillNo = implode(',', array_column($surchargeBillMdl, 'bill_no'));
                $totalDeducted = $billDeductionList[3] ?? 0; // 已被抵扣金额
                $remainingDeduction = $totalDeposit - $totalDeducted; // 剩余可抵扣金额
                // 剩余抵扣金额大于0即可抵扣
                if ($remainingDeduction > 0 && $actualPayment > 0){
                    // 该次抵扣金额 固定按100%抵扣
                    $deductionAccount = bcmul($remainingDeduction, 1, 4);
                    if ($deductionAccount >= $actualPayment){
                        // 可抵扣金额大于实付金额，则可抵扣金额 = 实付金额
                        $deductionAccount = $actualPayment;
                    }
                    // 保存抵扣记录
                    $deductionLog[] = [
                        'contract_no' => $contractMdl->contract_no,
                        'deduction_amount' => $deductionAccount,
                        'deduction_ratio' => 100, // 0.8 => 80
                        'base_ammount' => $remainingDeduction,
                        'deduction_bill_type' => 3,
                        'origin_bill_no' => $surchargeBillNo
                    ];
                    $actualPayment -= (float)$deductionAccount; // 抵扣实付金额
                }elseif ($remainingDeduction < 0){
                    // 需要额外支付供应商费用
                    // 固定按100%计算
                    $deductionAccount = bcmul($remainingDeduction, 1, 4);
                    // 保存抵扣记录
                    $deductionLog[] = [
                        'contract_no' => $contractMdl->contract_no,
                        'deduction_amount' => $deductionAccount,
                        'deduction_ratio' => 100, // 0.8 => 80
                        'base_ammount' => $remainingDeduction,
                        'deduction_bill_type' => 3,
                        'origin_bill_no' => $surchargeBillNo
                    ];
                    $actualPayment -= (float)$deductionAccount; // 抵扣实付金额
                }
            }

            // 延期扣款抵扣 (可选)
            if (isset($params['is_delayed_deduction']) && $params['is_delayed_deduction'] == 1){
                $delayedBillMdl = db::table('financial_bill')
                    ->where('contract_no', 'like', '%'.$params['contract_no'].'%')
                    ->where('bill_type', 4)
                    ->where('status', 2)
                    ->get();
                if ($delayedBillMdl->isNotEmpty()){
                    $delayedBillMdl = $delayedBillMdl->toArrayList();
                    $totalDeposit = array_sum(array_column($delayedBillMdl, 'actual_payment')); // 附加费合计
                    $delayedBillNo = implode(',', array_column($delayedBillMdl, 'bill_no'));
                    $totalDeducted = $billDeductionList[4] ?? 0; // 已被抵扣金额
                    $remainingDeduction = $totalDeposit - $totalDeducted; // 剩余可抵扣金额
                    // 剩余抵扣金额大于0即可抵扣
                    if ($remainingDeduction > 0 && $actualPayment > 0){
                        // 该次定金抵扣金额 固定按100%抵扣
                        $deductionAccount = bcmul($remainingDeduction, 1, 4);
                        if ($deductionAccount >= $actualPayment){
                            // 可抵扣金额大于实付金额，则可抵扣金额 = 实付金额
                            $deductionAccount = $actualPayment;
                        }
                        // 保存抵扣记录
                        $deductionLog[] = [
                            'contract_no' => $contractMdl->contract_no,
                            'deduction_amount' => $deductionAccount,
                            'deduction_ratio' => 100, // 0.8 => 80
                            'base_ammount' => $remainingDeduction,
                            'deduction_bill_type' => 4,
                            'origin_bill_no' => $delayedBillNo
                        ];
                        $actualPayment -= (float)$deductionAccount; // 抵扣实付金额
                    }elseif ($remainingDeduction < 0){
                        throw new \Exception('合同号：'.$contractMdl->contract_no.'票据号：'.$surchargeBillNo.'，延期扣款单总金额小于已抵扣金额，请检查数据！');
                    }
                }
            }

            $params['payable_amount'] = $totalAccount; // 应付金额
            $params['actual_payment'] = $actualPayment; // 实付金额
            $params['deduction_log'] = $deductionLog; // 待保存的抵扣记录
            $params['deduction_amount'] = array_sum(array_column($params['deduction_log'], 'deduction_amount'));
        }
//
//        if (!isset($params['bind_id']) || empty($params['bind_id'])){
//            throw new \Exception('未给定票据绑定标识！');
//        }else{
//            $billBindMdl = db::table('cloudhouse_contract_bill_bind')->where('id', $params['bind_id'])->first();
//            if (empty($billBindMdl)){
//                throw new \Exception('未查询到票据绑定信息！');
//            }
//        }
        return $params;
    }

    private function _checkDelayedBill($params)
    {
        // 判断是否给定创建人
        if (!isset($params['user_id']) || empty($params['user_id'])){
            if (isset($params['token']) && !empty($params['token'])){
                $users = db::table('users')->where('token',$params['token'])->first();
                if($users){
                    $params['user_id'] = $users->Id;
                }else{
                    throw new \Exception('未给定创建人标识！');
                }
            }else{
                throw new \Exception('未给定创建人标识！');
            }
        }
        if (!isset($params['application_reason']) || empty($params['application_reason'])){
            throw new \Exception('未给定申请事由！');
        }
        if (!isset($params['payee']) || empty($params['payee'])){
            throw new \Exception('未给定收款人/单位！');
        }
        if (!isset($params['receiving_bank_card_number']) || empty($params['receiving_bank_card_number'])){
            throw new \Exception('未给定收款银行卡号！');
        }
        if (!isset($params['payer']) || empty($params['payer'])){
            throw new \Exception('未给定付款人/单位！');
        }
        if (!isset($params['payment_bank_card_number']) || empty($params['payment_bank_card_number'])){
            throw new \Exception('未给定付款银行卡号！');
        }
        if (!isset($params['receiving_bank_name']) || empty($params['receiving_bank_name'])){
            throw new \Exception('未给定收款方开户行！');
        }
        if (!isset($params['payment_bank_name']) || empty($params['payment_bank_name'])){
            throw new \Exception('未给定付款方开户行！');
        }
        if (isset($params['contract_no']) && !empty($params['contract_no'])){
            if (!is_array($params['contract_no'])){
                throw new \Exception('给定的合同号数据格式类型不正确！');
            }
            // 查找合同数据
            $contractMdl = db::table('cloudhouse_contract_total')->whereIn('contract_no', $params['contract_no'])->get();
            $contractList = [];
            foreach ($contractMdl as $contract){
                $contractList[$contract->contract_no] = $contract;
                // 供应商名称
                $params['supplier_name'] = $contract->supplier_short_name;
                $params['supplier_id'] = $contract->supplier_id;
                $params['company_name'] = $contract->company_name;
                $params['company_id'] = $contract->company_id;
            }
            // 校验合同数据
            $error = '';
            foreach ($params['contract_no'] as $contractNo){
                $contract = $contractList[$contractNo] ?? [];
                if (empty($contract)){
                    $error .= '合同号：'.$contractNo.'合同数据不存在！';
                    continue;
                }
                // 如果合同完结，则不可新增单据
                if ($contract->contract_status != 2){
                    $error .= '合同号：'.$contractNo.'该合同非审批通过状态！';
                    continue;
                }
                // 判断合同是否完结
                if ($contract->is_end != 0){
                    $error .= '合同号：'.$contractNo.'该合同已完结，不可操作数据！';
                    continue;
                }
            }
            if (!empty($error)){
                throw new \Exception($error);
            }
            $params['contract_no'] = implode(',', $params['contract_no']);
        }else{
            throw new \Exception('未给定合同号！');
        }
        if (!isset($params['in_house_items']) || empty($params['in_house_items'])){
            throw new \Exception('未给定入库明细！');
        }else {
            // 应付金额 = 入库货值
            $payableAmount = 0;
            $error = '';
            $customSkuList = [];
            foreach ($params['in_house_items'] as $k => $v) {
                $customSku = $v['custom_sku'] ?? '';
                if (empty($customSku)) {
                    $error .= '第' . ($k + 1) . '行入库数据未给定库存SKU！';
                    continue;
                }
                if (!isset($v['custom_sku_id']) || empty($v['custom_sku_id'])) {
                    $error .= '第' . ($k + 1) . '行库存SKU：' . $customSku . '未给定库存SKU标识！';
                    continue;
                }
                if (!isset($v['spu']) || empty($v['spu'])) {
                    $error .= '第' . ($k + 1) . '行库存SKU：' . $customSku . '未给定SPU！';
                    continue;
                }
                if (!isset($v['spu_id']) || empty($v['spu_id'])) {
                    $error .= '第' . ($k + 1) . '行库存SKU：' . $customSku . '未给定SPU标识！';
                    continue;
                }
                if (!isset($v['num']) || $v['num'] < 0) {
                    $error .= '第' . ($k + 1) . '行库存SKU：' . $customSku . '数量未给定或不合法！';
                    continue;
                }
                if (!isset($v['price']) || $v['price'] <= 0) {
                    $error .= '第' . ($k + 1) . '行库存SKU：' . $customSku . '单价未给定或不合法！';
                    continue;
                }
                if (!isset($v['delivery_time']) || empty($v['delivery_time'])) {
                    $error .= '第' . ($k + 1) . '行库存SKU：' . $customSku . '未给定送达时间！';
                    continue;
                }
                if (!isset($v['in_out_house_time']) || empty($v['in_out_house_time'])) {
                    $error .= '第' . ($k + 1) . '行库存SKU：' . $customSku . '未给定实际出入库时间！';
                    continue;
                }
                if (!isset($v['in_out_house_type']) || empty($v['in_out_house_type'])) {
                    $error .= '第' . ($k + 1) . '行库存SKU：' . $customSku . '未给定出入库类型！';
                    continue;
                }
                if (!isset($v['contract_no']) || empty($v['contract_no'])) {
                    $error .= '第' . ($k + 1) . '行库存SKU：' . $customSku . '未给定合同号！';
                    continue;
                }
                if (!isset($v['total_price']) || empty($v['total_price'])) {
                    $error .= '第' . ($k + 1) . '行库存SKU：' . $customSku . '未给定合计价格！';
                    continue;
                }
                if (!isset($v['type_detail']) || empty($v['type_detail'])) {
                    $error .= '第' . ($k + 1) . '行库存SKU：' . $customSku . '未给定出入库类型！';
                    continue;
                }
                if (!isset($v['goods_transfers_detail_id']) || empty($v['goods_transfers_detail_id'])) {
                    $error .= '第' . ($k + 1) . '行库存SKU：' . $customSku . '未给定入库详情标识！';
                    continue;
                }
                if (!isset($v['delayed_day_num']) || $v['delayed_day_num'] <= 0) {
                    $error .= '第' . ($k + 1) . '行库存SKU：' . $customSku . '超期天数不合法！';
                    continue;
                }
                if (!isset($v['delayed_price']) || $v['delayed_price'] <= 0) {
                    $error .= '第' . ($k + 1) . '行库存SKU：' . $customSku . '超期扣款金额不合法！';
                    continue;
                }
                if ($v['delayed_price'] > $v['total_price']) {
                    $error .= '第' . ($k + 1) . '行库存SKU：' . $customSku . '超期扣款金额不能大于货值金额！';
                    continue;
                }
                if (!isset($v['is_created_delayed']) || $v['is_created_delayed'] != 0) {
                    $error .= '第' . ($k + 1) . '行库存SKU：' . $customSku . '该数据已创建过延期扣款单！';
                    continue;
                }
                if (!isset($v['platform_id']) || empty($v['platform_id'])) {
                    $error .= '第' . ($k + 1) . '行库存SKU：' . $customSku . '未给定平台标识！';
                    continue;
                }
                if (!isset($v['platform_name']) || empty($v['platform_name'])) {
                    $error .= '第' . ($k + 1) . '行库存SKU：' . $customSku . '未给定平台名称！';
                    continue;
                }
                $payableAmount += $v['delayed_price'];
            }
            if (!empty($error)){
                throw new \Exception('明细异常！详情：'.$error);
            }
            $params['payable_amount'] = $payableAmount;
            $params['actual_payment'] = $payableAmount;
            $params['ratio'] = 7.5;
        }

        if (isset($params['bill_id']) && !empty($params['bill_id'])){
            $bill = db::table('financial_bill')->where('id', $params['bill_id'])->first();
            if (empty($bill)){
                throw new \Exception('票据不存在！');
            }
            if ($bill->user_id != $params['user_id']){
                throw new \Exception('非本人创建的单据不予修改！');
            }
            if (!in_array($bill->status, [-2, -1, 0])){
                throw new \Exception('票据非 审核被拒/已撤销/待提交 状态不予修改！');
            }
        }


        return $params;
    }

    public function getPaymentBillSelectionList($params)
    {
        $data = $this->GetConstantList(['bill_type' => Constant::BILL_TYPE, 'bill_status' => Constant::BILL_STATUS, 'bill_is_payment' => Constant::BILL_IS_PAYMENT]);
        $contractList = db::table('cloudhouse_contract_total as a')
            ->leftJoin('suppliers as b', 'a.supplier_id', '=', 'b.id')
            ->leftJoin('company as c', 'a.company_id', '=', 'c.id')
            ->where('a.contract_status', 2)
            ->orderBy('a.create_time', 'DESC')
            ->get(['a.id', 'a.contract_no', 'a.contract_no', 'a.total_count', 'a.total_price','a.report_date', 'a.company_name', 'a.supplier_short_name','b.payee as supplier_payee', 'b.bank_name as supplier_bank_name', 'b.bank_card_number as supplier_bank_card_number', 'c.payee as company_payee', 'c.bank_name as company_bank_name', 'c.bank_card_number as company_bank_card_number']);
        $contractBillBind = db::table('cloudhouse_contract_bill_bind')
            ->where('is_push', 0)
            ->where('is_deleted', 0)
            ->where('bill_type', 1)
            ->get();
        $paymentInformation = [];
        foreach ($contractBillBind as $v){
            $key = $v->contract_no;
            $v->bind_id = $v->id;
            $paymentInformation[$key][] = $v;
        }
        foreach ($contractList as $v){
            $v->payment_information = $paymentInformation[$v->contract_no] ?? [];
        }
        $data['contract_list'] = $contractList;

        return ['code' => 200, 'msg' => '获取成功！', 'data' => $data];
    }
    public function getPaymentBillList($params)
    {
        $billMdl = db::table('financial_bill')
            ->where('status', '<>', -3);

        if (isset($params['bill_type'])){
            $billMdl = $billMdl->where('bill_type', $params['bill_type']);
        }

        if (isset($params['contract_no']) && !empty($params['contract_no'])){
            $billMdl = $billMdl->where('contract_no', 'like', '%'.$params['contract_no'].'%');
        }

        if (isset($params['bill_no']) && !empty($params['bill_no'])){
            $billMdl = $billMdl->where('bill_no', 'like', '%'.$params['bill_no'].'%');
        }

        if (isset($params['status']) && !empty($params['status'])){
            $billMdl = $billMdl->where('status', $params['status']);
        }

        if (isset($params['payee']) && !empty($params['payee'])){
            $billMdl = $billMdl->where('payee', 'like', '%'.$params['payee'].'%');
        }

        if (isset($params['payer']) && !empty($params['payer'])){
            $billMdl = $billMdl->where('payer', 'like', '%'.$params['payer'].'%');
        }

        if (isset($params['is_payment'])){
            $billMdl = $billMdl->where('is_payment', $params['is_payment']);
        }

        if (isset($params['payment_date']) && !empty($params['payment_date'])){
            $billMdl = $billMdl->whereBetween('payment_date', $params['payment_date']);
        }

        if (isset($params['created_at']) && !empty($params['created_at'])){
            $billMdl = $billMdl->whereBetween('created_at', $params['created_at']);
        }

        if (!isset($params['user_id']) || empty($params['user_id'])){
            if (isset($params['token']) && !empty($params['token'])){
                $users = db::table('users')->where('token',$params['token'])->first();
                $params['user_id'] = $users->Id ?? 0;
            }else{
                $params['user_id'] = 0;
            }
        }else{
            $billMdl = $billMdl->where('user_id', $params['user_id']);
        }

        $controller = new Controller();
        $power = $controller->powers(['user_id' => $params['user_id'], 'identity' => ['show_all_finance_bill']]);

        if (!$power['show_all_finance_bill']){
            $billMdl = $billMdl->where('user_id', $params['user_id']);
        }

        $count = $billMdl->count();

        if (isset($params['page']) || isset($params['limit'])){
            $limit = $params['limit'] ?? 30;
            $page = $params['page'] ?? 1;
            $offsetNum = $limit * ($page-1);
            $billMdl = $billMdl->limit($limit)->offset($offsetNum);
        }

        if ($power['show_all_finance_bill']){
            $billMdl = $billMdl->orderBy('status', 'DESC');
        }

        $list = $billMdl
            ->orderBy('status', 'DESC')
            ->orderBy('created_at', 'DESC')
            ->get();
        $contractMdl = db::table('cloudhouse_contract_total')
            ->get()
            ->keyBy('contract_no');
        $userMdl = db::table('users')
            ->get(['Id', 'account'])
            ->pluck('account', 'Id');
        foreach ($list as $v){
            if ($v->bill_type == 4){
                $v->ratio_name = $v->ratio.'‱';
            }else{
                $v->ratio_name = $v->ratio.'%';
            }
            $v->deduction_ratio = $v->deduction_ratio.'%';
            $v->bill_type_name = Constant::BILL_TYPE[$v->bill_type];
            $v->status_name = Constant::BILL_STATUS[$v->status];
            $v->is_payment_name =Constant::BILL_IS_PAYMENT[$v->is_payment];
            $v->user_name = $this->GetUsers($v->user_id)['account'] ?? '';
            $v->payment_date = $v->payment_date ?? '';
            $v->contract_total_price = 0.00;
            if (isset($contractMdl[$v->contract_no])){
                $v->contract_total_price = $contractMdl[$v->contract_no]->total_price;
            }

            $taskMdl = db::table('tasks as a')
                ->leftJoin('tasks_examine as b', 'a.Id', '=', 'b.task_id')
                ->whereIn('a.class_id', [26, 69])
                ->where('a.is_deleted', 0)
                ->where('a.ext', 'like', '%'.$v->bill_no.'%')
                ->get(['b.*']);
            $examineOk = [];
            $examineNo = [];
            $notExamine = [];
            foreach ($taskMdl as $t){
                if ($t->state == 3){
                    $examineOk[] = $userMdl[$t->user_id];
                    continue;
                }
                if (in_array($t->state, [1, 2])){
                    $notExamine[] = $userMdl[$t->user_id];
                    continue;
                }
                if ($t->state == 4){
                    $examineNo[] = $userMdl[$t->user_id];
                    continue;
                }
            }
            $v->examine_ok = $examineOk;
            $v->examine_no = $examineNo;
            $v->not_examine = $notExamine;
            $v->pdf_file = $v->pdf_file. '?id='.rand(1, 9999);
        }

        return ['code' => 200, 'msg' => '获取成功！', 'data' => ['count' => $count, 'list' => $list]];
    }

    public function getPaymentBillDetail($params)
    {
        try{
            if (isset($params['id']) && !empty($params['id'])){
                $billMdl = db::table('financial_bill')
                    ->where('id', $params['id'])
                    ->first();
            }else if (isset($params['bill_no']) && !empty($params['bill_no'])){
                $billMdl = db::table('financial_bill')
                    ->where('bill_no', $params['bill_no'])
                    ->first();
            }else{
                throw new \Exception('未给定查询条件！');
            }
            if (empty($billMdl)){
                throw new \Exception('未查询到该单据信息！');
            }
            $billMdl->bill_type_name = Constant::BILL_TYPE[$billMdl->bill_type] ?? '未定义的类型';
            $billMdl->status_name = Constant::BILL_STATUS[$billMdl->status] ?? '未定义的状态';
            $billMdl->ratio_name = $billMdl->ratio.'%';
            $billMdl->is_payment_name =Constant::BILL_IS_PAYMENT[$billMdl->is_payment];
            $billMdl->user_name = $this->GetUsers($billMdl->user_id)['account'] ?? '';
            $billMdl->payment_date = $billMdl->payment_date != '0000-00-00 00:00:00' ? date('Y-m-d', strtotime($billMdl->payment_date)) : '';
            $billHistoryMdl = [];
            $billMdl->payment_method = '';
            if ($billMdl->bill_type == 1){
                $billHistoryMdl = db::table('financial_bill')
                    ->where('contract_no', $billMdl->contract_no)
                    ->where('bill_type', 1)
                    ->where('created_at', '<', $billMdl->created_at)
                    ->where('is_payment', 1)
                    ->get();
                $contractMdl = db::table('cloudhouse_contract_total')
                    ->where('contract_no', $billMdl->contract_no)
                    ->first();
                if (!empty($contractMdl)){
                    $billMdl->payment_method = Constant::CONTRACT_PERIODIC_SETTLEMENT_METHOD[$contractMdl->periodic_id] ?? '';
                }
            }else if ($billMdl->bill_type == 2){
                $billHistoryMdl = db::table('financial_bill')
                    ->where('contract_no', $billMdl->contract_no)
                    ->where('created_at', '<', $billMdl->created_at)
                    ->where('is_payment', 1)
                    ->get();
            }

            // 已支付详情
            $paymentDetail = [];
            $advanceDetail = [];
            foreach ($billHistoryMdl as $item){
                if ($billMdl->bill_type == $item->bill_type){
                    $paymentDetail[] = [
                        'application_reason' => $item->application_reason,
                        'bill_no' => $item->bill_no,
                        'bill_type' => $item->bill_type,
                        'bill_type_name' => Constant::BILL_TYPE[$item->bill_type] ?? '',
                        'status' => $item->status,
                        'status_name' => Constant::BILL_STATUS[$item->status],
                        'contract_no' => $item->contract_no,
                        'payable_amount' => $this->floorDecimal($item->payable_amount),
                        'actual_payment' => $this->floorDecimal($item->actual_payment),
                        'payment_date' => date('Y-m-d', strtotime($item->payment_date)) ?? '',
                        'memo' => $item->memo,
                        'ratio_name' => $item->bill_type == 4 ? $item->ratio.'‱' : $item->ratio.'%',
                    ];
                }
                if ($billMdl->bill_type == 2){
                    if ($item->bill_type == 1){
                        $advanceDetail[] = [
                            'application_reason' => $item->application_reason,
                            'bill_no' => $item->bill_no,
                            'bill_type' => $item->bill_type,
                            'bill_type_name' => Constant::BILL_TYPE[$item->bill_type] ?? '',
                            'status' => $item->status,
                            'status_name' => Constant::BILL_STATUS[$item->status],
                            'contract_no' => $item->contract_no,
                            'payable_amount' => $this->floorDecimal($item->payable_amount),
                            'actual_payment' => $this->floorDecimal($item->actual_payment),
                            'payment_date' => date('Y-m-d', strtotime($item->payment_date)) ?? '',
                            'memo' => $item->memo,
                            'ratio_name' => $item->bill_type == 4 ? $item->ratio.'‱' : $item->ratio.'%',
                        ];
                    }
                }
            }
            $billItems = [];
            if ($billMdl->bill_type == 2){
                $billItemsMdl = db::table('financial_bill_items as a')
                    ->leftJoin('goods_transfers_detail as b', 'a.goods_transfers_detail_id', '=', 'b.id')
                    ->where('a.bill_no', $billMdl->bill_no)
                    ->get(['a.*', 'b.order_no']);
                foreach ($billItemsMdl as $v){
                    $inHouseNum = 0;
                    $outHouseNum = 0;
                    $in_out_house_type_name = '';
                    if ($v->in_out_house_type == 1){
                        $inHouseNum = $v->num;
                    }else{
                        $outHouseNum = $v->num;
                    }
                    $num = $inHouseNum - $outHouseNum;
                    if (isset($billItems[$v->custom_sku_id])){
                        $billItems[$v->custom_sku_id]['in_house_num'] += $inHouseNum;
                        $billItems[$v->custom_sku_id]['out_house_num'] += $outHouseNum;
                        $billItems[$v->custom_sku_id]['num'] += $num;
                       $billItems[$v->custom_sku_id]['order_no'] .= ','.$v->order_no;
                    }else{
                        $billItems[$v->custom_sku_id] = [
                            'custom_sku_id' => $v->custom_sku_id,
                            'custom_sku' => $v->custom_sku,
                            'spu_id' => $v->spu_id,
                            'spu' => $v->spu,
                            'name' => $v->name,
                            'color_identifying' => $v->color_identifying,
                            'color_name' => $v->color_name,
                            'num' => $inHouseNum - $outHouseNum,
                            'in_house_num' => $inHouseNum,
                            'out_house_num' => $outHouseNum,
                            'price' => $v->price,
                            'delivery_time' => $v->delivery_time,
                            'in_out_house_time' => $v->in_out_house_time,
                            'contract_no' => $v->contract_no,
                            'bill_no' => $v->bill_no,
                            'platform_name' => $v->platform_name,
                            'order_no' => $v->order_no
                        ];
                    }
                    $billItems[$v->custom_sku_id]['order_no'] = implode(',', array_unique(explode(',', $billItems[$v->custom_sku_id]['order_no'])));
                }
                foreach ($billItems as &$v){
                    $v['total_price'] = $v['price'] * $v['num'];
                    $ratio = $v['total_price'] / $billMdl->payable_amount;
                    $v['ratio'] = $ratio;
                    $v['actual_total_price'] = $v['total_price'] - $billMdl->deduction_amount * $ratio;
                }
            }
            if (in_array($billMdl->bill_type, [1, 3])){
                $contractSpu = db::table('cloudhouse_contract');
                if ($billMdl->bill_type == 1){
                    $contractSpu = $contractSpu->where('contract_no', $billMdl->contract_no);
                }else{
                    $contractNos = explode(',', $billMdl->contract_no);
                    $contractSpu = $contractSpu->whereIn('contract_no', $contractNos);
                }
                $contractSpu = $contractSpu->get();
                $color = [];
                foreach ($contractSpu as $v){
                    if (isset($billItems[$v->spu_id])){
                        $color[$v->spu_id][] = $v->color_name;
                        $color[$v->spu_id] = array_unique($color[$v->spu_id]);
                        $billItems[$v->spu_id]['color_name'] = $color[$v->spu_id];
                        $billItems[$v->spu_id]['num'] += $v->count;
                        $billItems[$v->spu_id]['total_price'] += $v->one_price * $v->count;
                    }else{
                        $billItems[$v->spu_id] = [
                            'contract_no' => $v->contract_no,
                            'spu_id' => $v->spu_id,
                            'spu' => $v->spu,
                            'title' => $v->title,
                            'color_name' => [$v->color_name],
                            'price' => $v->one_price,
                            'num' => $v->count,
                            'total_price' => $v->one_price * $v->count,
                        ];
                        $color[$v->spu_id][] = $v->color_name;
                    }
                }
                if ($billMdl->bill_type == 1){
                    // 获取所有预付账款
                    $allBill = db::table('cloudhouse_contract_bill_bind')
                        ->where('contract_no', $billMdl->contract_no)
                        ->where('bill_type', 1)
                        ->get();
                    $totalAdvanceAmount = 0;
                    if ($allBill->isNotEmpty()){
                        $allBill = $allBill->toArrayList();
                        $totalAdvanceAmount = array_sum(array_column($allBill, 'money'));
                    }
                    $billMdl->total_advance_amount = $totalAdvanceAmount;
                    $writeOffMdl = db::table('financial_verification as a')
                        ->leftJoin('financial_bill as b', 'a.bill_no', '=', 'b.bill_no')
                        ->where('a.contract_no', $billMdl->contract_no)
                        ->where('b.bill_type', 1)
                        ->where('a.is_write_off', 1)
                        ->get();
                    $totalWriteOffAmount = 0;
                    if ($writeOffMdl->isNotEmpty()){
                        $writeOffMdl = $writeOffMdl->toArrayList();
                        $totalWriteOffAmount = array_sum(array_column($writeOffMdl, 'actual_payment'));
                    }
                    $billMdl->total_write_off_amount = $totalWriteOffAmount;
                }
            }
            $billMdl->payment_detail = $paymentDetail;
            $billMdl->advance_detail = $advanceDetail;
            $billMdl->bill_items = array_values($billItems);

            // 合同信息
            $contractMdl = db::table('cloudhouse_contract_total')
                ->select('contract_no', 'total_count', 'total_price', 'report_date', 'sign_date', 'contract_type', 'supplier_short_name', 'supplier_id', 'company_name', 'supplier_no', 'bind_place_ids')
                ->get()
                ->keyBy('contract_no');
            $contractNo = $billMdl->contract_no;
            if ($billMdl->bill_type == 2){
                $contractNo = explode(',', $billMdl->contract_no)[0];
            }
            $contract = $contractMdl[$contractNo] ?? [];
            if (!empty($contract)){
                $billMdl->contract_total_count = $contract->total_count;
                $billMdl->contract_total_price = $contract->total_price;
                $billMdl->report_date = $contract->report_date;
                $billMdl->sign_date = $contract->sign_date;
                $billMdl->contract_type = $contract->contract_type;
                $billMdl->supplier_short_name = $contract->supplier_short_name;
                $billMdl->supplier_id = $contract->supplier_id;
                $billMdl->supplier_no = $contract->supplier_no;
                $billMdl->company_name = $contract->company_name;
                $orderIds = explode(',', $contract->bind_place_ids ?? '');
                $order = db::table('amazon_place_order_task as a')
                    ->leftJoin('shop as b', 'a.shop_id', '=', 'b.id')
                    ->leftJoin('platform as c', 'b.platform_id', '=', 'c.Id')
                    ->whereIn('a.id', $orderIds)
                    ->where('shop_id', '<>', 0)
                    ->get(['b.shop_name', 'c.name as platform_name']);
                $platformName = [];
                foreach ($order as $o){
                    $platformName[] = $o->platform_name.'('.$o->shop_name.')';
                }
                $billMdl->platform_name = implode(',', $platformName);
            }else{
                throw new \Exception('合同号：'.$contractNo.'数据为空！');
            }

            // 获取抵扣详情
            $billMdl->deduction_detail = [];
            if ($billMdl->bill_type == 2){
                $dikouMdl = db::table('financial_bill_deduction')
                    ->where('bill_no', $billMdl->bill_no)
                    ->where('is_deleted', 0)
                    ->get();
                foreach ($dikouMdl as $dk){
                    $dk->deduction_bill_type_name = Constant::BILL_TYPE[$dk->deduction_bill_type] ?? '';
                    if ($dk->deduction_amount >= 0){
                        $dk->type = '抵扣';
                    }else{
                        $dk->type = '支出';
                    }
                }
                $billMdl->deduction_detail = $dikouMdl;
            }

            return ['code' => 200, 'msg' => '获取成功！', 'data' => $billMdl];
        }catch (\Exception $e){
            return ['code' => 500, 'msg' => '获取失败！原因：'.$e->getMessage().'，'.$e->getLine()];
        }
    }

    public function deletePaymentBill($params)
    {
        try {
            if (!isset($params['id']) || empty($params['id'])){
                throw new \Exception('未给定查询条件！');
            }
            if (!isset($params['user_id']) || empty($params['user_id'])){
                if (isset($params['token']) && !empty($params['token'])){
                    $users = db::table('users')->where('token',$params['token'])->first();
                    if($users){
                        $params['user_id'] = $users->Id;
                    }else{
                        throw new \Exception('未给定创建人标识！');
                    }
                }else{
                    throw new \Exception('未给定创建人标识！');
                }
            }
            db::beginTransaction();
            $billMdl = db::table('financial_bill')->whereIn('id', $params['id'])->get();
            $error = '';
            foreach ($billMdl as $v){
                if ($v->status == 1){
                    $error .= '票据号：'.$v->bill_no.'该单据已提交审核，需撤销审核任务！';
                    continue;
                }
                if ($v->status >=2 || $v->is_payment == 1){
                    $error .= '票据号：'.$v->bill_no.'该单据状态已审核通过，需提交取消审核申请！';
                    continue;
                }
                if (!in_array($v->status, [-2,-1, 0])){
                    $error .= '票据号：'.$v->bill_no.'该单据状态不处于 审核拒绝/已撤销/草拟 状态，不予废弃！';
                    continue;
                }
                if ($v->user_id != $params['user_id']){
                    $error .= '票据号：'.$v->bill_no.'该单据非本人创建，不予删除！';
                }
                $update = db::table('financial_bill')->where('id', $v->id)->update([
                    'status' => -3,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
                if (empty($update)){
                    $error .= '票据号：'.$v->bill_no.'该票据废弃失败！';
                }
                if ($v->bill_type == 1){
                    $bindIds = json_decode($v->bind_ids, true);
                    $update = db::table('cloudhouse_contract_bill_bind')->whereIn('id', $bindIds)->update(['bill_no' => '', 'is_push' => 0]);
                    if (empty($update)){
                        $error .= '票据号：'.$v->bill_no.'该票据绑定信息推单状态失败！';
                    }
                }
                if ($v->bill_type == 2){
                    // 更新入库信息
                    $billItems = db::table('financial_bill_items')
                        ->where('bill_no', $v->bill_no)
                        ->get();
                    $error = '';
                    foreach ($billItems as $_v){
                        $update = db::table('goods_transfers_detail')
                            ->where('id', $_v->goods_transfers_detail_id)
                            ->update(['is_settled' => 0]);
                        if (empty($update)){
                            $error .= '合同号：'.$_v->contract_no.'，库存skuId：'.$_v->custom_sku_id.'该条入库数据退单状态变更失败！';
                            continue;
                        }
                    }
                    if (!empty($error)){
                        throw new \Exception($error);
                    }
                    // 删除入库详情
                    $delete = db::table('financial_bill_items')
                        ->where('bill_no', $v->bill_no)
                        ->delete();
                    // 删除抵扣记录
                    $delete = db::table('financial_bill_deduction')
                        ->where('bill_no', $v->bill_no)
                        ->update(['is_deleted' => 1]);
                }

                // 删除审核任务
                $taskMdl = db::table('tasks')
                    ->where('ext', 'like', '%'.$v->id.'%')
                    ->whereIn('class_id', [26, 69, 70])
                    ->update(['is_deleted' => 1]);
            }
            if (!empty($error)){
                throw new \Exception($error);
            }
            db::commit();

            return ['code' => 200, 'msg' => '废弃成功！'];
        }catch (\Exception $e){
            db::rollback();

            return ['code' => 500, 'msg' => '废弃失败！原因：'.$e->getMessage().'，'.$e->getLine()];
        }
    }

    public function submitPaymentBill($params)
    {
        try{
            if (!isset($params['id']) || empty($params['id'])){
                throw new \Exception('未给定票据标识！');
            }
            if (!isset($params['user_id']) || empty($params['user_id'])){
                if (isset($params['token']) && !empty($params['token'])){
                    $users = db::table('users')->where('token',$params['token'])->first();
                    if($users){
                        $params['user_id'] = $users->Id;
                    }else{
                        throw new \Exception('未给定创建人标识！');
                    }
                }else{
                    throw new \Exception('未给定创建人标识！');
                }
            }
            $billMdl = db::table('financial_bill')->where('id', $params['id'])->first();
            if (empty($billMdl)){
                throw new \Exception('未查询到票据数据！');
            }
            if (!in_array($billMdl->status, [-2, -1, 0])){
                throw new \Exception('票据号：'.$billMdl->bill_no.'非撤销/草拟/被拒绝状态不可提交票据！');
            }
            if ($billMdl->user_id != $params['user_id']){
                throw new \Exception('票据号：'.$billMdl->bill_no.'该单据非本人创建，不予操作！');
            }
            switch ($billMdl->bill_type){
                case 1:
                    $classId = 26;
                    break;
                case 2:
                    $classId = 69;
                    break;
                case 3:
                    $classId = 70;
                    break;
                default:
                    throw new \Exception('未定义的票据类型');
            }
            $data = [
                'bill_id' => $billMdl->id,
                'application_reason' => $billMdl->application_reason,
                'bill_no' => $billMdl->bill_no,
                'contract_no' => $billMdl->contract_no,
                'bill_type' => $billMdl->bill_type,
                'bill_type_name' => Constant::BILL_TYPE[$billMdl->bill_type],
                'user_id' => $billMdl->user_id,
                'user_name' => $this->GetUsers($billMdl->user_id)['account'] ?? '',
                'status' => $billMdl->status,
                'status_name' => Constant::BILL_STATUS[$billMdl->status],
                'ratio' => $billMdl->ratio,
                'ratio_name' => $billMdl->ratio . '%',
                'payable_amount' => $this->floorDecimal($billMdl->payable_amount),
                'memo' => $billMdl->memo,
                'created_at' => $billMdl->created_at,
                'payee' => $billMdl->payee,
                'payer' => $billMdl->payer,
                'detail_url' => '/payment_list_details?bill_no='.$billMdl->bill_no,
                'request_type' => $classId, // 任务分类id
            ];
            return ['code' => 200, 'msg' => '获取成功！', 'data' => $data];
        }catch (\Exception $e){
            return ['code' => 500, 'msg' => '获取失败！原因：'.$e->getMessage().'，'.$e->getLine()];
        }
    }

    public function getGoodTransfersDetail($params)
    {
        try {
            if (!isset($params['contract_no']) || empty($params['contract_no'])){
                throw new \Exception('未给定合同号!');
            }
            $goodsTransfersDetailMdl = db::table('goods_transfers_detail as a')
                ->leftJoin('goods_transfers as b', 'a.order_no', '=', 'b.order_no')
                ->where('a.contract_no', $params['contract_no'])
                ->where('a.is_settled', 0)
                ->whereIn('b.is_push', [3, 5])
                ->whereIn('b.type_detail', [2, 9]);

            // 工厂送货时间查询
            if (isset($params['delivery_time']) && !empty($params['delivery_time'])){
                $goodsTransfersDetailMdl = $goodsTransfersDetailMdl->whereBetween('b.push_time', $params['delivery_time']);
            }

            // spu
            if (isset($params['spu']) && !empty($params['spu'])){
                $spu = trim($params['spu']);
                $spuMdl = db::table('self_spu')
                    ->where('spu', 'like', '%'.$spu.'%')
                    ->orWhere('old_spu', 'like', '%'.$spu.'%')
                    ->get()
                    ->toArrayList();
                $spuId = array_column($spuMdl, 'id');

                $goodsTransfersDetailMdl = $goodsTransfersDetailMdl->whereIn('a.spu_id', $spuId);
            }

            // color_id
            if (isset($params['color_id']) && !empty($params['color_id'])){
                $customSkuMdl = db::table('self_custom_sku')
                    ->where('color_id', $params['color_id'])
                    ->get()
                    ->toArrayList();
                $customSkuId = array_column($customSkuMdl, 'id');
                $goodsTransfersDetailMdl = $goodsTransfersDetailMdl->whereIn('a.custom_sku_id', $customSkuId);
            }

            // 出入库类型
            if (isset($params['type_detail']) && !empty($params['type_detail'])){
                $goodsTransfersDetailMdl = $goodsTransfersDetailMdl->where('b.type_detail', $params['type_detail']);
            }

            if (isset($params['order_no'])){
                $goodsTransfersDetailMdl = $goodsTransfersDetailMdl->where('a.order_no', 'like', '%'.trim($params['order_no']).'%');
            }


            $count = $goodsTransfersDetailMdl->count();
            if (isset($params['page']) || isset($params['limit'])){
                $limit = $params['limit'] ?? 30;
                $page = $params['page'] ?? 1;
                $offset = $limit * ($page - 1);
                $goodsTransfersDetailMdl = $goodsTransfersDetailMdl->limit($limit)->offset($offset);
            }

            $goodsTransfersDetailMdl = $goodsTransfersDetailMdl->orderBy('b.push_time', 'DESC')->get(['a.*', 'b.push_time as delivery_time', 'b.type_detail', 'b.platform_id']);
            $customSkuMdl = db::table('self_custom_sku as a')
                ->leftJoin('self_color_size as b', 'a.color_id', '=', 'b.id')
                ->get(['a.*', 'b.name as color_name', 'b.identifying as color_identifying']);
            $contractSpuMdl = db::table('cloudhouse_contract')
                ->where('contract_no', $params['contract_no'])
                ->get();
            // 获取平台名称
            $platformMdl = db::table('platform')
                ->get(['name', 'Id'])
                ->keyBy('Id');
            $customSku = [];
            foreach ($customSkuMdl as $v){
                $customSku[$v->id] = $v;
            }
            $contractSpu = [];
            foreach ($contractSpuMdl as $v){
                $key = $v->spu_id.'-'.$v->color_identifying;
                $contractSpu[$key] = $v;
            }
            $list = [];
            foreach ($goodsTransfersDetailMdl as $k => $v){
                if (isset($customSku[$v->custom_sku_id])){
                    $v->color_id = $customSku[$v->custom_sku_id]->color_id;
                    $v->color_name = $customSku[$v->custom_sku_id]->color_name;
                    $v->color_identifying = $customSku[$v->custom_sku_id]->color_identifying;
                    $v->name = $customSku[$v->custom_sku_id]->name;
                    $v->size = $customSku[$v->custom_sku_id]->size;
                    $v->num = $v->receive_num;
                    $v->goods_transfers_detail_id = $v->id;
                    $v->platform_name = $platformMdl[$v->platform_id]->name ?? '';
                    // 获取合同单价
                    $key = $v->spu_id.'-'.$v->color_identifying;
                    if (isset($contractSpu[$key])){
                        $v->price = $contractSpu[$key]->one_price;
                        $v->total_price = $contractSpu[$key]->one_price * $v->receive_num;
                    }else{
                        $v->price = 0;
                        $v->total_price = 0;
                    }
                    if ($v->type_detail == 2){
                        $v->in_out_house_type = 1;
                        $v->in_out_house_type_name = '采购入库';
                        $v->in_out_house_time = $v->receive_time;
                    }elseif ($v->type_detail == 9){
                        $v->in_out_house_type = 2;
                        $v->in_out_house_type_name = '采购出库';
                        $v->in_out_house_time = $v->createtime;
                    }
                }else{
                    throw new \Exception('第'.($k+1).'行库存sku信息不存在！');
                }
                $list[] = $v;
            }
            return ['code' => 200, 'msg' => '获取成功！', 'data' => ['count' => $count, 'list' => $list]];
        }catch (\Exception $e){
            return ['code' => 500, 'msg' => '获取失败！原因：'.$e->getMessage().'，'.$e->getLine()];
        }
    }

    public function getSupplierContractList($params)
    {
//        $list = db::table('suppliers')->get();
//
//        if (isset($params['supplier_no']) && !empty($params['supplier_no'])){
//            $list = db::table('supplier_no', 'like', '%'.$params['supplier_no'].'%');
//        }

        $contractMdl = db::table('cloudhouse_contract_total')
            ->where('contract_status', 2)
            ->where('is_end', 0)
            ->where('supplier_id', $params['supplier_id'] ?? 0);

        $contractMdl = $contractMdl->get();
//        $contractList = [];
//        foreach ($contractMdl as $v){
//            $contractList[$v->supplier_id][] = $v->contract_no;
//        }
//
//        foreach ($list as $v){
//            $v->contract_no = $contractList[$v->Id] ?? [];
//        }

        return ['code' => 200, 'msg' => '获取成功！', 'data' => $contractMdl];
    }

    public function deApprovalPaymenBill($params)
    {
        try {
            if (!isset($params['bill_id']) || empty($params['bill_id'])){
                throw new \Exception('未给定票据标识!');
            }
            if (!isset($params['user_id']) || empty($params['user_id'])){
                if (isset($params['token']) && !empty($params['token'])){
                    $users = db::table('users')->where('token',$params['token'])->first();
                    if($users){
                        $params['user_id'] = $users->Id;
                    }else{
                        throw new \Exception('未给定创建人标识！');
                    }
                }else{
                    throw new \Exception('未给定创建人标识！');
                }
            }
            $billMdl = db::table('financial_bill')
                ->where('id', $params['bill_id'])
                ->first();
            if (empty($billMdl)){
                throw new \Exception('票据不存在!');
            }
            if ($billMdl->user_id != $params['user_id']){
                throw new \Exception('票据号：'.$billMdl->bill_no.'该单据非本人创建，不予操作！');
            }

            if (in_array($billMdl->status, [1])){ // 待审核不需申请审批
                $update =  db::table('financial_bill')
                    ->where('id', $params['bill_id'])
                    ->update(['status' => 0, 'updated_at' => date('Y-m-d H:i:s')]);
                if (empty($update)){
                    throw new \Exception('票据号：'.$billMdl->bill_no.'该单据无数据变化！');
                }
                $update = db::table('tasks')
                    ->where('ext', 'like', '%'.$billMdl->bill_no.'%')
                    ->whereIn('class_id', [26, 69, 70])
                    ->update(['is_deleted' => 1]);
                if (empty($update)){
                    throw new \Exception('票据号：'.$billMdl->bill_no.'该单据的审核任务删除失败！');
                }
            }else if (in_array($billMdl->status, [2])){ // 已审核需发起审批
                throw new \Exception('已审核票据不可撤销！');
                $update =  db::table('financial_bill')
                    ->where('id', $params['bill_id'])
                    ->update(['status' => 0, 'updated_at' => date('Y-m-d H:i:s')]);
                if (empty($update)){
                    throw new \Exception('票据号：'.$billMdl->bill_no.'该单据无数据变化！');
                }
                $update = db::table('tasks')
                    ->where('ext', 'like', '%'.$billMdl->bill_no.'%')
                    ->whereIn('class_id', [26, 69, 70])
                    ->update(['is_deleted' => 1]);
                if (empty($update)){
                    throw new \Exception('票据号：'.$billMdl->bill_no.'该单据的审核任务删除失败！');
                }
            }else{
                throw new \Exception('票据号：'.$billMdl->bill_no.'该单据非可撤销任务状态，不予操作！');
            }

            return ['code' => 200, 'msg' => '撤销成功！'];
        }catch (\Exception $e){
            return ['code' => 500, 'msg' => '撤销失败！原因：'.$e->getMessage().'，'.$e->getLine()];
        }
    }

    public function getFinancialVerificationList($params)
    {
        $list = db::table('financial_verification as a')
            ->leftJoin('cloudhouse_contract_total as b', 'a.contract_no', '=', 'b.contract_no')
            ->leftJoin('financial_bill as c', 'a.bill_no', '=', 'c.bill_no');

        if (isset($params['contract_no']) && !empty($params['contract_no'])){
            $list = $list->where('a.contract', 'like', '%'.$params['contract_no'].'%');
        }
        if (isset($params['bill_no']) && !empty($params['bill_no'])){
            $list = $list->where('a.bill_no', 'like', '%'.$params['bill_no'].'%');
        }
        if (isset($params['bill_type']) && !empty($params['bill_type'])){
            $list = $list->where('c.bill_type', $params['bill_type']);
        }
        if (isset($params['is_write_off'])){
            $list = $list->where('a.is_write_off', $params['is_write_off']);
        }
        if (isset($params['user_id']) && !empty($params['user_id'])){
            $list = $list->where('a.user_id', $params['user_id']);
        }
        if (isset($params['payment_date']) && !empty($params['payment_date'])){
            $list = $list->whereBetween('a.payment_date', $params['payment_date']);
        }
        if (isset($params['created_at']) && !empty($params['created_at'])){
            $list = $list->whereBetween('a.created_at', $params['created_at']);
        }
        if (isset($params['write_off_date']) && !empty($params['write_off_date'])){
            $list = $list->whereBetween('a.write_off_date', $params['write_off_date']);
        }
        $count = $list->count();

        if (isset($params['page']) || isset($params['limit'])){
            $limit = $params['limit'] ?? 30;
            $page = $params['page'] ?? 1;
            $offsetNum = $limit * ($page-1);
            $list = $list->limit($limit)->offset($offsetNum);
        }

        $list = $list->select('a.*', 'b.total_count as contract_total_count', 'b.total_price as contract_total_price', 'c.user_id as bill_user_id', 'c.receiving_bank_card_number', 'c.payment_bank_card_number')
//            ->orderBy('a.created_at', 'DESC')
            ->get();

        foreach ($list as $v){
            $v->is_write_off_name = $v->is_write_off ? '已核销' : '未核销';
            $v->user_name = $this->GetUsers($v->user_id)['account'] ?? '';
            $v->bill_user_name = $this->GetUsers($v->bill_user_id)['account'] ?? '';
            $v->img = json_decode($v->img, true) ?? [];
            $v->file = json_decode($v->file, true) ?? [];
            $v->ratio_name = $v->ratio.'%';
        }

        return ['code' => 200, 'msg' => '获取成功！', 'data' => ['count' => $count, 'list' => $list]];
    }

    /**
     * @Desc:付账单支付确认
     * @param $params
     * @return array
     * @author: Liu Sinian
     * @Time: 2023/8/12 15:48
     */
    public function confirmPaymentBill($params)
    {
        try {
            if (!isset($params['list']) || empty($params['list'])){
                throw new \Exception('未给定确认参数！');
            }
            if (!isset($params['user_id']) || empty($params['user_id'])){
                if (isset($params['token']) && !empty($params['token'])){
                    $users = db::table('users')->where('token',$params['token'])->first();
                    if($users){
                        $params['user_id'] = $users->Id;
                    }else{
                        throw new \Exception('未给定操作人标识！');
                    }
                }else{
                    throw new \Exception('未给定操作人标识！');
                }
            }
            if (!isset($params['img']) || empty($params['img'])){
                throw new \Exception('未给定需核销单据的银行水单图片！');
            }else{
                $params['img'] = json_encode($params['img']);
            }

            // 获取所有票据
            $billMdl = db::table('financial_bill as a')
                ->leftJoin('cloudhouse_contract_total as b', 'a.contract_no', '=', 'b.contract_no')
                ->select('a.*', 'b.total_price as contract_total_price')
                ->get()
                ->keyBy('id');
            $error = '';
            db::beginTransaction();
            foreach ($params['list'] as $k => $v){
                if (!isset($v['id']) || empty($v['id'])){
                    $error .= '第'.($k+1).'行未给定票据标识！';
                    continue;
                }
                if (!isset($v['payment_date']) || empty($v['payment_date'])){
                    $error .= '第'.($k+1).'行未给定票据支付时间！';
                    continue;
                }
                $bill = $billMdl[$v['id']] ?? [];
                if (empty($bill)){
                    $error .= '第'.($k+1).'行票据数据异常，该票据不存在！';
                    continue;
                }
                if (!in_array($bill->bill_type, [1,2])){
                    $error .= '票据号：'.$bill->bill_no.'的类型为'.Constant::BILL_TYPE[$bill->bill_type].'，非付款单类型不可进行该操作！';
                    continue;
                }
                if ($bill->status != 2){
                    $error .= '票据号：'.$bill->bill_no.'状态为'.Constant::BILL_TYPE[$bill->status].'，该票据未审核通过！';
                    continue;
                }
                if ($bill->is_payment == 1){
                    $error .= '票据号：'.$bill->bill_no.'该票据已支付，不可重复确认！';
                    continue;
                }
                if ($bill->is_payment != 0){
                    $error .= '票据号：'.$bill->bill_no.'支付状态非未支付状态，不可进行支付确认操作！';
                    continue;
                }
                // 查询票据审核任务
                $taskMdl = db::table('tasks as a')
                    ->leftJoin('tasks_node as b', 'a.Id', '=', 'b.task_id')
                    ->where('a.ext', 'like', '%'.$bill->bill_no.'%')
                    ->where('a.is_deleted', 0)
                    ->where('b.name', '支付')
                    ->select('a.Id as task_id', 'b.Id as tasks_node_id', 'b.state as tasks_node_state')
                    ->first();
                if ($taskMdl->tasks_node_state == 1){
                    $error .= '票据号：'.$bill->bill_no.'的支付节点任务还未开始流转！';
                    continue;
                }
                if ($taskMdl->tasks_node_state == 3){
                    $error .= '票据号：'.$bill->bill_no.'的支付节点任务已完成，不可重复操作！';
                    continue;
                }
                if ($taskMdl->tasks_node_state != 2){
                    $error .= '票据号：'.$bill->bill_no.'的支付节点任务未处于进行中状态，不可操作！';
                    continue;
                }
                $taskSonMdl = db::table('tasks_son')
                    ->where('task_id', $taskMdl->task_id)
                    ->where('tasks_node_id', $taskMdl->tasks_node_id)
                    ->select('Id','user_id', 'state')
                    ->first();
                if ($params['user_id'] != $taskSonMdl->user_id){
                    $username = $this->GetUsers($taskSonMdl->user_id)['account'] ?? '查无此人';
                    $error .= '票据号：'.$bill->bill_no.'的支付确认节点执行人为'.$username.'，请联系该执行人进行支付确认操作！';
                    continue;
                }
                if ($taskSonMdl->state == 1){
                    $error .= '请先接受票据号：'.$bill->bill_no.'的支付确认任务！';
                    continue;
                }
                if ($taskSonMdl->state == 3){
                    $error .= '票据号：'.$bill->bill_no.'的支付确认任务已完成，不可重复操作！';
                    continue;
                }
                if ($taskSonMdl->state != 2){
                    $error .= '票据号：'.$bill->bill_no.'的支付确认任务未处于进行中状态，不可操作！';
                    continue;
                }
                // 更新支付状态
                $update = db::table('financial_bill')
                    ->where('id', $bill->id)
                    ->update([
                        'payment_date' => $v['payment_date'],
                        'is_payment' => 1,
                        'status' => 3
                    ]);
                if (empty($update)){
                    $error .= '票据号：'.$bill->bill_no.'数据更新失败！';
                    continue;
                }
                // 生成核销记录
                $writeOffBn = $this->idBuilder->createId("WRITE_OFF_BN");
                $save = [
                    'write_off_bn' => $writeOffBn,
                    'bill_no' => $bill->bill_no,
                    'contract_no' => $bill->contract_no,
                    'contract_total_price' => $bill->contract_total_price,
                    'prepaid_amount' => $bill->actual_payment,
                    'ratio' => $bill->ratio,
                    'actual_payment' => $bill->actual_payment,
                    'payment_date' => $v['payment_date'],
                    'created_at' => date('Y-m-d H:i:s'),
                    'user_id' => $params['user_id'],
                    'is_write_off' => 1,
                    'memo' => $v['memo'] ?? '',
                    'write_off_date' => date('Y-m-d H:i:s'),
                    'img' => $params['img']
                ];

                $id = db::table('financial_verification')->insertGetId($save);
                if (empty($id)){
                    $error .= '票据号：'.$bill->bill_no.'生成核销记录失败！';
                    continue;
                }
                // 自动完成支付确认节点任务
                $call_task = new \App\Libs\wrapper\Task();
                $taskArr = [
                    'Id' => $taskSonMdl->Id,
                    'state' => 3,
                    'feedback_type' => 1,
                ];
                $return = $call_task->task_son_complete($taskArr);
                if ($return != 1) {
                    $error .= '票据号：'.$bill->bill_no."支付任务节点自动完成失败，请手动点击完成任务！";
                    continue;
                }
            }
            if (!empty($error)){
                throw new \Exception($error);
            }
            db::commit();

            return ['code' => 200, 'msg' => '票据支付确认成功！'];
        }catch (\Exception $e){
            db::rollback();
            return ['code' => 500, 'msg' => '票据支付确认失败！原因：'.$e->getMessage().'，'.$e->getLine()];
        }
    }

    public function writeOffPaymentBill($params)
    {
        try {
            if (!isset($params['list']) || empty($params['list'])){
                throw new \Exception('未给定需核销的票据参数！');
            }
            if (!isset($params['user_id']) || empty($params['user_id'])){
                if (isset($params['token']) && !empty($params['token'])){
                    $users = db::table('users')->where('token',$params['token'])->first();
                    if($users){
                        $params['user_id'] = $users->Id;
                    }else{
                        throw new \Exception('未给定操作人标识！');
                    }
                }else{
                    throw new \Exception('未给定操作人标识！');
                }
            }
            // 获取所有核销数据
            $writeOffList = db::table('financial_verification')
                ->get()
                ->keyBy('id');
            $error = '';
            db::beginTransaction();
            foreach ($params['list'] as $k => $v){
                if (!isset($v['id']) || empty($v['id'])){
                    $error .= '第'.($k+1).'行未给核销票据标识！';
                    continue;
                }
                if (!isset($v['write_off_date']) || empty($v['write_off_date'])){
                    $error .= '第'.($k+1).'行未给定核销时间！';
                    continue;
                }
                $bill = $writeOffList[$v['id']] ?? [];
                if (empty($bill)){
                    $error .= '第'.($k+1).'行票据数据异常，该票据不存在！';
                    continue;
                }
                if ($bill->is_write_off == 1){
                    $error .= '核销单号'.$bill->write_off_bn.'，票据号：'.$bill->bill_no.'已核销，不可重复操作！';
                    continue;
                }
                if ($bill->is_write_off != 0){
                    $error .= '核销单号'.$bill->write_off_bn.'，票据号：'.$bill->bill_no.'状态非未核销状态，不可操作！';
                    continue;
                }
                // 查询票据审核任务
                $taskMdl = db::table('tasks as a')
                    ->leftJoin('tasks_node as b', 'a.Id', '=', 'b.task_id')
                    ->where('a.ext', 'like', '%'.$bill->bill_no.'%')
                    ->where('a.is_deleted', 0)
                    ->where('b.name', '核销')
                    ->select('a.Id as task_id', 'b.Id as tasks_node_id', 'b.state as tasks_node_state')
                    ->first();
                if ($taskMdl->tasks_node_state == 1){
                    $error .= '核销单号'.$bill->write_off_bn.'，票据号：'.$bill->bill_no.'的核销节点任务还未开始流转！';
                    continue;
                }
                if ($taskMdl->tasks_node_state == 3){
                    $error .= '核销单号'.$bill->write_off_bn.'，票据号：'.$bill->bill_no.'的核销节点任务已完成，不可重复操作！';
                    continue;
                }
                if ($taskMdl->tasks_node_state != 2){
                    $error .= '核销单号'.$bill->write_off_bn.'，票据号：'.$bill->bill_no.'的核销节点任务未处于进行中状态，不可操作！';
                    continue;
                }
                $taskSonMdl = db::table('tasks_son')
                    ->where('task_id', $taskMdl->task_id)
                    ->where('tasks_node_id', $taskMdl->tasks_node_id)
                    ->select('Id','user_id', 'state')
                    ->first();
                if ($params['user_id'] != $taskSonMdl->user_id){
                    $username = $this->GetUsers($taskSonMdl->user_id)['account'] ?? '查无此人';
                    $error .= '核销单号'.$bill->write_off_bn.'，票据号：'.$bill->bill_no.'的核销节点执行人为'.$username.'，请联系该执行人进行支付确认操作！';
                    continue;
                }
                if ($taskSonMdl->state == 1){
                    $error .= '请先接受核销单号'.$bill->write_off_bn.'，票据号：'.$bill->bill_no.'的核销任务！';
                    continue;
                }
                if ($taskSonMdl->state == 3){
                    $error .= '核销单号'.$bill->write_off_bn.'，票据号：'.$bill->bill_no.'的核销任务已完成，不可重复操作！';
                    continue;
                }
                if ($taskSonMdl->state != 2){
                    $error .= '核销单号'.$bill->write_off_bn.'，票据号：'.$bill->bill_no.'的核销任务未处于进行中状态，不可操作！';
                    continue;
                }
                // 更新核销状态
                $update = db::table('financial_verification')
                    ->where('id', $bill->id)
                    ->update(['write_off_date' => $v['write_off_date'], 'is_write_off' => 1, 'img' => $params['img']]);
                if (empty($update)){
                    $error .= '核销单号'.$bill->write_off_bn.'，票据号：'.$bill->bill_no.'的核销状态更新失败！';
                    continue;
                }
                // 更新票据状态
                $update = db::table('financial_bill')
                    ->where('bill_no', $bill->bill_no)
                    ->update(['status' => 3]);
                if (empty($update)){
                    $error .= '票据号：'.$bill->bill_no.'的状态更新失败！';
                    continue;
                }
                // 自动完成支付确认节点任务
                $call_task = new \App\Libs\wrapper\Task();
                $taskArr = [
                    'Id' => $taskSonMdl->Id,
                    'state' => 3,
                    'feedback_type' => 1,
                ];
                $return = $call_task->task_son_complete($taskArr);
                if ($return != 1) {
                    $error .= '核销单号'.$bill->write_off_bn.'，票据号：'.$bill->bill_no."核销任务节点自动完成失败，请手动点击完成任务！";
                    continue;
                }
            }
            if (!empty($error)){
                throw new \Exception($error);
            }
            db::commit();

            return ['code' => 200, 'msg' => '票据核销成功！'];
        }catch (\Exception $e){
            db::rollback();
            return ['code' => 500, 'msg' => '票据核销失败！原因：'.$e->getMessage().'，'.$e->getLine()];
        }
    }

    public function printBillPdf($params)
    {
        try {
            if (!isset($params['id']) || empty($params['id'])){
                throw new \Exception('未给定票据标识！');
            }
            $billMdl = db::table('financial_bill')
                ->where('id', $params['id'])
                ->first();
            if (empty($billMdl)){
                throw new \Exception('票据不存在！');
            }
//            if (!in_array($billMdl->status, [2,3])){
//                throw new \Exception('票据未审核通过，不可打印票据！');
//            }
            switch($billMdl->bill_type){
                case 1:
                    $result = $this->_getAdvancePaymentBillPdfDetail($billMdl);
                    break;
                case 2:
                    $result = $this->_getAccountsPaymentBillPdfDetail($billMdl);
                    break;
                case 3:
                    $result = $this->_getSurchargePaymentBillPdfDetail($billMdl);
                    break;
                default:
                    throw new \Exception('未定义的票据类型，不可打印票据！');
            }
            require_once(app_path() . DIRECTORY_SEPARATOR . 'Libs' . DIRECTORY_SEPARATOR . 'tcpdf' . DIRECTORY_SEPARATOR . 'examples' . DIRECTORY_SEPARATOR . 'tcpdf_include.php');
            $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

            // 设置文档信息
            $pdf->SetCreator('L');
            $pdf->SetAuthor('L');
            $pdf->SetTitle($result['title']);
            $pdf->SetSubject($result['title']);
            $pdf->SetKeywords('TCPDF, PDF, PHP');

            // 设置页眉和页脚信息
            $pdf->SetHeaderData('', 60, $result['title'], '', [0, 0, 0], [0, 0, 0]);
            $pdf->setFooterData([0, 0, 0], [0, 0, 0]);

            // 设置页眉和页脚字体
            $pdf->setHeaderFont(['stsongstdlight', '', '10']);
            $pdf->setFooterFont(['helvetica', '', '8']);

            //删除预定义的打印 页眉/页尾
            $pdf->setPrintHeader($result['is_header'] ?? false);
            $pdf->setPrintFooter($result['is_footer'] ?? false);

            // 设置默认等宽字体
            $pdf->SetDefaultMonospacedFont('courier');

            // 设置间距
            $pdf->SetMargins(10, 15, 15);//页面间隔
            $pdf->SetHeaderMargin(5);//页眉top间隔
            $pdf->SetFooterMargin(5);//页脚bottom间隔

            // 设置分页
            $pdf->SetAutoPageBreak(TRUE, 10);

            // set default font subsetting mode
            $pdf->setFontSubsetting(true);

            //设置字体 stsongstdlight支持中文
            $pdf->SetFont('stsongstdlight', '', 12);

            $pdf->Ln(5);
            // 设置密码
//            $pdf->SetProtection($permissions = array('print', 'modify', 'copy', 'annot-forms', 'fill-forms', 'extract', 'assemble', 'print-high'), $user_pass = '123456', $owner_pass = null, $mode = 0, $pubkeys = null );

            //第一页
            $pdf->AddPage();
            $pdf->writeHTML($result['html']);

            //输出PDF
            $pdf->Output($result['title'].$billMdl->bill_no.'.pdf', 'I');//I输出、D下载

            return ['code' => 200, 'msg' => '打印成功！'];
        }catch (\Exception $e){
            db::rollback();
            return ['code' => 500, 'msg' => '打印失败！原因：'.$e->getMessage().'，'.$e->getLine()];
        }
    }

    private function  _getAdvancePaymentBillPdfDetail($billMdl)
    {
        $billDetail = $this->getPaymentBillDetail(['id' => $billMdl->id])['data'] ?? [];
        if (empty($billDetail)){
            throw new \Exception('未获取到票据详情数据！');
        }

        $bigWrite = $this->convertAmountToCn($billDetail->actual_payment);
        $html = '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table {
            border-collapse: collapse;
        }
        td, th {
            padding: 0;
        }
        table {
            border-collapse: separate;
            border-spacing: 0;
        }
        td,tr {
            font-size: 10px;
            line-height: 15px;
            vertical-align: middle;
        }
        td{
            width: 10%;
        }
        .border1{
            border: 0.1rem black solid;
            height: 15px;
        }
    </style>
</head>
<body>
    <table  width="100%">
        <tr>
            <td colspan="10" style="text-align: center;font-size: 20px; font-weight: bold;">预付账款单</td>
        </tr>
        <tr>
            <td colspan="7" rowspan="3" style="text-align: center;"></td>
            <td colspan="3">付款单号：'.$billDetail->bill_no.'</td>
        </tr>
        <tr>
            <td colspan="3">合同号：'.$billDetail->contract_no.'</td>
        </tr>
        <tr>
            <td colspan="3">状态：'.$billDetail->status_name.'</td>
        </tr>
        <tr>
            <td class="border1" colspan="1">供应商简称</td>
            <td class="border1" colspan="4">'.$billDetail->supplier_name.'</td>
            <td class="border1" colspan="1">付款日期</td>
            <td class="border1" colspan="4">'.$billDetail->payment_date.'</td>
        </tr>
        <tr>
            <td class="border1" colspan="1">收款人</td>
            <td class="border1" colspan="2">'.$billDetail->payee.'</td>
            <td class="border1" colspan="1">收款人开户银行</td>
            <td class="border1" colspan="2"></td>
            <td class="border1" colspan="1">收款人银行账号</td>
            <td class="border1" colspan="3">'.$billDetail->receiving_bank_card_number.'</td>
        </tr>
        <tr>
            <td class="border1" colspan="1">付款人</td>
            <td class="border1" colspan="2">'.$billDetail->payer.'</td>
            <td class="border1" colspan="1">付款人开户银行</td>
            <td class="border1" colspan="2"></td>
            <td class="border1" colspan="1">付款人银行账号</td>
            <td class="border1" colspan="3">'.$billDetail->payment_bank_card_number.'</td>
        </tr>
        <tr>
            <td class="border1" colspan="1">付款方式</td>
            <td class="border1" colspan="2">T/T</td>
            <td class="border1" colspan="1">合同总金额</td>
            <td class="border1" colspan="2">'.$billDetail->contract_total_price.'</td>
            <td class="border1" colspan="1">预付比率</td>
            <td class="border1" colspan="3">'.$billDetail->ratio_name.'</td>
        </tr>
        <tr>
            <td class="border1" colspan="1" rowspan="2">本次支付</td>
            <td class="border1" colspan="1">小写</td>
            <td class="border1" colspan="1">'.$billDetail->actual_payment.'</td>
            <td class="border1" colspan="1" rowspan="2">币种</td>
            <td class="border1" colspan="2" rowspan="2">RMB</td>
            <td class="border1" colspan="1" rowspan="2">附加费用扣款</td>
            <td class="border1" colspan="3" rowspan="2"></td>
        </tr>
        <tr>
            <td class="border1" colspan="1">大写</td>
            <td class="border1" colspan="1">'.$bigWrite.'</td>
        </tr>
        <tr>
            <td class="border1" colspan="2">付款单类型</td>
            <td class="border1" colspan="8">'.$billDetail->bill_type_name.'</td>
        </tr>
        <tr>
            <td class="border1" colspan="2">备注</td>
            <td class="border1" colspan="2">'.$billDetail->memo.'</td>
            <td class="border1" colspan="2">审核意见反馈</td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="2">申请人</td>
            <td class="border1" colspan="2">'.$billDetail->user_name.'</td>
            <td class="border1" colspan="1">申请日期</td>
            <td class="border1" colspan="5">'.$billDetail->created_at.'</td>
        </tr>
        <tr>
            <td class="border1" colspan="10" style="text-align: center;">预付款产品信息</td>
        </tr>
        <tr>
            <td class="border1" colspan="3">款号</td>
            <td class="border1" colspan="3">品名</td>
            <td class="border1" colspan="4">颜色</td>
        </tr>';
        foreach ($billDetail->bill_items as $v){
            $html .= '<tr>
                <td class="border1" colspan="3">'.$v['spu'].'</td>
                <td class="border1" colspan="3">'.$v['title'].'</td>
                <td class="border1" colspan="4">'.implode(',', $v['color_name']).'</td>
            </tr>';
        }

        $html .= '<tr>
            <td class="border1" colspan="10" style="text-align: center;">付款单核销详情</td>
        </tr>
        <tr>
            <td class="border1" colspan="2">预付款总金额</td>
            <td class="border1" colspan="2">累计已核销金额</td>
            <td class="border1" colspan="2">未付款金额</td>
            <td class="border1" colspan="4">备注</td>
        </tr>
        <tr>
            <td class="border1" colspan="2">'.$billDetail->total_advance_amount.'</td>
            <td class="border1" colspan="2">'.$billDetail->total_write_off_amount.'</td>
            <td class="border1" colspan="2">'.($billDetail->total_advance_amount - $billDetail->total_write_off_amount).'</td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: center;"></td>
        </tr>
        <tr>
            <td class="border1" colspan="3">审批部门</td>
            <td class="border1" colspan="3">审批签字</td>
            <td class="border1" colspan="4">审批意见</td>
        </tr>
        <tr>
            <td class="border1" colspan="3">经办人</td>
            <td class="border1" colspan="3"></td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="3">直属主管</td>
            <td class="border1" colspan="3"></td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="3">财务稽核人员</td>
            <td class="border1" colspan="3"></td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="3">财务主管</td>
            <td class="border1" colspan="3"></td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="3">财务经理</td>
            <td class="border1" colspan="3"></td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="3">总经理</td>
            <td class="border1" colspan="3"></td>
            <td class="border1" colspan="4"></td>
        </tr>
    </table>
</body>
</html>';

        return ['html' => $html, 'title' => $billDetail->bill_type_name];
    }

    private function _getSurchargePaymentBillPdfDetail($billMdl)
    {
        $billDetail = $this->getPaymentBillDetail(['id' => $billMdl->id])['data'] ?? [];
        if (empty($billDetail)){
            throw new \Exception('未获取到票据详情数据！');
        }

//        $bigWrite = $this->convertAmountToCn($billDetail->actual_payment);
        $html = '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table {
            border-collapse: collapse;
        }
        td, th {
            padding: 0;
        }
        table {
            border-collapse: separate;
            border-spacing: 0;
        }
        td,tr {
            font-size: 10px;
            line-height: 15px;
            vertical-align: middle;
        }
        td{
            width: 10%;
        }
        .border1{
            border: 0.1rem black solid;
            height: 15px;
        }
        .fontBlod{
            font-weight: bold;
        }
    </style>
</head>
<body>
    <table  width="100%">
        <tr>
            <td colspan="10" style="text-align: center;font-size: 20px; font-weight: bold; padding-bottom: 20px;">厦门微微尔电子商务有限公司</td>
        </tr>
         <tr>
            <td colspan="10" style="text-align: center;font-size: 20px; font-weight: bold;"></td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: center;font-size: 16px; font-weight: bold; padding-top: 5%">附加费用表</td>
        </tr>
        <tr>
            <td colspan="7" rowspan="3" style="text-align: center;"></td>
            <td colspan="2">单号：'.$billDetail->bill_no.'</td>
        </tr>
        <tr>
            <td colspan="2">日期：'.date('Y-m-d', strtotime($billDetail->created_at)).'</td>
        </tr>
         <tr>
            <td colspan="2">状态：'.$billDetail->status_name.'</td>
        </tr>
        <tr>
            <td class="border1" colspan="1">采购合同号</td>
            <td class="border1" colspan="4">'.$billDetail->contract_no.'</td>
            <td class="border1" colspan="1">供应商简称</td>
            <td class="border1" colspan="4">'.$billDetail->supplier_short_name.'</td>
        </tr>
        <tr>
            <td class="border1" colspan="1">开户行(收款方)</td>
            <td class="border1" colspan="4"></td>
            <td class="border1" colspan="1">银行账号(收款方)</td>
            <td class="border1" colspan="4">'.$billDetail->receiving_bank_card_number.'</td>
        </tr>
         <tr>
            <td class="border1" colspan="1">开户行(我司)</td>
            <td class="border1" colspan="4"></td>
            <td class="border1" colspan="1">银行账号(我司)</td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="2" rowspan="">费用总额</td>
            <td class="border1" colspan="8">'.$billDetail->actual_payment.'</td>
        </tr>
        <tr>
            <td class="border1" colspan="2" rowspan="">费用理由</td>
            <td class="border1" colspan="8">'.$billDetail->application_reason.'</td>
        </tr>
        
        <tr>
            <td class="fontBlod" colspan="10" style="text-align: left;">付款明细</td>
        </tr>
        <tr>
            <td class="border1" colspan="1">采购合同号</td>
            <td class="border1" colspan="2">款号</td>
            <td class="border1" colspan="1">商品编号</td>
            <td class="border1" colspan="1">品名</td>
            <td class="border1" colspan="2">颜色</td>
            <td class="border1" colspan="1">数量</td>
            <td class="border1" colspan="1">合同单价</td>
            <td class="border1" colspan="1">合计金额</td>
        </tr>';

        foreach ($billDetail->bill_items as $v){
            $html .= '<tr>
            <td class="border1" colspan="1">'.$v['contract_no'].'</td>
            <td class="border1" colspan="2">'.$v['spu'].'</td>
            <td class="border1" colspan="1">'.$v['spu'].'</td>
            <td class="border1" colspan="1">'.$v['title'].'</td>
            <td class="border1" colspan="2">'.implode(',', $v['color_name']).'</td>
            <td class="border1" colspan="1">'.$v['num'].'</td>
            <td class="border1" colspan="1">'.$v['price'].'</td>
            <td class="border1" colspan="1">'.$v['total_price'].'</td>
        </tr>';
        }

        $html .= '<tr>
            <td class="fontBlod" colspan="10" style="text-align: left;">备注</td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: left;"></td>
        </tr>
    </table>
</body>
</html>';

        return ['html' => $html, 'title' => $billDetail->bill_type_name];
    }

    private function _getAccountsPaymentBillPdfDetail($billMdl)
    {
        $billDetail = $this->getPaymentBillDetail(['id' => $billMdl->id])['data'] ?? [];
        if (empty($billDetail)) {
            throw new \Exception('未获取到票据详情数据！');
        }
        $bigAmount = $this->convertAmountToCn($billDetail->contract_total_price);
        $html = '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table {
            border-collapse: collapse;
        }
        td, th {
            padding: 0;
        }
        table {
            border-collapse: separate;
            border-spacing: 0;
        }
        td,tr {
            font-size: 10px;
            line-height: 15px;
            vertical-align: middle;
        }
        td{
            width: 10%;
        }
        .border1{
            border: 0.1rem black solid;
            height: 15px;
        }
        .fontBlod{
            font-weight: bold;
        }
    </style>
</head>
<body>
    <table  width="100%">
        <tr>
            <td colspan="10" style="text-align: center;font-size: 20px; font-weight: bold; padding-bottom: 20px;">'.$billDetail->company_name.'</td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: center;font-size: 20px; font-weight: bold;"></td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: center;font-size: 20px; font-weight: bold;">付款申请书</td>
        </tr>
        <tr>
            <td colspan="7">单号：'.$billDetail->bill_no.'</td>
            
        </tr>
        <tr>
            <td colspan="7">合同号：'.$billDetail->contract_no.'</td>
            <td colspan="2">日期：'.date('Y-m-d', strtotime($billDetail->created_at)).'</td>
        </tr>
        <tr>
            <td colspan="7">申请人：'.$billDetail->user_name.'</td>
            <td colspan="2">状态：'.$billDetail->status_name.'</td>
        </tr>
        <tr>
            <td class="border1" colspan="1">申请事由</td>
            <td class="border1" colspan="4">'.$billDetail->application_reason.'</td>
            <td class="border1" colspan="1">付款方式</td>
            <td class="border1" colspan="4">T/T</td>
        </tr>
        <tr>
            <td class="border1" colspan="1">收款(供货)单位</td>
            <td class="border1" colspan="4">'.$billDetail->supplier_short_name.'</td>
            <td class="border1" colspan="1">收款人</td>
            <td class="border1" colspan="4">'.$billDetail->payee.'</td>
        </tr>
         <tr>
            <td class="border1" colspan="1">开户行(供应商)</td>
            <td class="border1" colspan="4"></td>
            <td class="border1" colspan="1">银行账号(供应商)</td>
            <td class="border1" colspan="4">'.$billDetail->receiving_bank_card_number.'</td>
        </tr>
        <tr>
            <td class="border1" colspan="1">开户行(我司)</td>
            <td class="border1" colspan="4"></td>
            <td class="border1" colspan="1">银行账号(我司)</td>
            <td class="border1" colspan="4">'.$billDetail->payment_bank_card_number.'</td>
        </tr>
        <tr>
            <td class="border1" colspan="1">品名</td>
            <td class="border1" colspan="4"></td>
            <td class="border1" colspan="1">交货时间</td>
            <td class="border1" colspan="4">'.$billDetail->report_date.'</td>
        </tr>
        <tr>
            <td class="border1" colspan="1">本次实付金额</td>
            <td class="border1" colspan="1">小写</td>
            <td class="border1" colspan="3">'.$billDetail->actual_payment.'</td>
            <td class="border1" colspan="1">已支付情况</td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="1">应付金额</td>
            <td class="border1" colspan="4">'.$billDetail->payable_amount.'</td>
            <td class="border1" colspan="1">币种</td>
            <td class="border1" colspan="4">RMB</td>
        </tr>
        <tr>
            <td class="border1" colspan="1">采购合同总金额</td>
            <td class="border1" colspan="1">小写</td>
            <td class="border1" colspan="3">'.$billDetail->contract_total_price.'</td>
            <td class="border1" colspan="1">大写</td>
            <td class="border1" colspan="4">'.$bigAmount.'</td>
        </tr>
        <tr>
            <td class="border1" colspan="1">是否已结汇</td>
            <td class="border1" colspan="4"></td>
            <td class="border1" colspan="1">验货情况</td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="1">增值税发票抬头</td>
            <td class="border1" colspan="4"></td>
            <td class="border1" colspan="1">增值税发票号</td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="1">其他</td>
            <td class="border1" colspan="4"></td>
            <td class="border1" colspan="1">总经理</td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="1">备注</td>
            <td class="border1" colspan="9">'.$billDetail->memo.'</td>
        </tr>
        <tr>
            <td class="border1" colspan="1">审批备注</td>
            <td class="border1" colspan="9"></td>
        </tr>
         <tr>
            <td colspan="10" style="text-align: center;font-size: 20px; font-weight: bold;"></td>
        </tr>
        <tr>
            <td class="fontBlod" colspan="10" style="text-align: center;font-size: 14px;">预付款</td>
        </tr>
         <tr>
            <td colspan="10" style="text-align: center;font-size: 20px; font-weight: bold;"></td>
        </tr>
        <tr>
            <td class="border1" colspan="1">预付款单号</td>
            <td class="border1" colspan="1">合同号</td>
            <td class="border1" colspan="2">申请事由</td>
            <td class="border1" colspan="1">预付比率</td>
            <td class="border1" colspan="1">预付金额</td>
            <td class="border1" colspan="1">实付金额</td>
            <td class="border1" colspan="1">支付时间</td>
            <td class="border1" colspan="2">备注</td>
        </tr>';
        if (!empty($billDetail->advance_detail)){
            foreach ($billDetail->advance_detail as $v){
                $html .= '<tr>
                <td class="border1" colspan="1">'.$v['bill_no'].'</td>
                <td class="border1" colspan="1">'.$v['contract_no'].'</td>
                <td class="border1" colspan="2">'.$v['application_reason'].'</td>
                <td class="border1" colspan="1">'.$v['ratio_name'].'</td>
                <td class="border1" colspan="1">'.$v['payable_amount'].'</td>
                <td class="border1" colspan="1">'.$v['actual_payment'].'</td>
                <td class="border1" colspan="1">'.$v['payment_date'].'</td>
                <td class="border1" colspan="2">'.$v['memo'].'</td>
            </tr>';
            }
        }else{
            $html .= '<tr>
                <td class="border1" colspan="1"></td>
                <td class="border1" colspan="1"></td>
                <td class="border1" colspan="2"></td>
                <td class="border1" colspan="1"></td>
                <td class="border1" colspan="1"></td>
                <td class="border1" colspan="1"></td>
                <td class="border1" colspan="1"></td>
                <td class="border1" colspan="2"></td>
            </tr>';
        }

        $html .= '<tr>
            <td colspan="10" style="text-align: center;font-size: 20px; font-weight: bold;"></td>
        </tr>
        <tr>
            <td class="fontBlod" colspan="10" style="text-align: center;font-size: 14px;">已付款</td>
        </tr>
         <tr>
            <td colspan="10" style="text-align: center;font-size: 20px; font-weight: bold;"></td>
        </tr>
        <tr>
            <td class="border1" colspan="1">应付账单号</td>
            <td class="border1" colspan="1">合同号</td>
            <td class="border1" colspan="2">申请事由</td>
            <td class="border1" colspan="1">货值比率</td>
            <td class="border1" colspan="1">应付金额</td>
            <td class="border1" colspan="1">实付金额</td>
            <td class="border1" colspan="1">支付时间</td>
            <td class="border1" colspan="2">备注</td>
        </tr>';

        if (!empty($billDetail->payment_detail)){
            foreach ($billDetail->payment_detail as $v){
                $html .= '<tr>
                <td class="border1" colspan="1">'.$v['bill_no'].'</td>
                <td class="border1" colspan="1">'.$v['contract_no'].'</td>
                <td class="border1" colspan="2">'.$v['application_reason'].'</td>
                <td class="border1" colspan="1">'.$v['ratio_name'].'</td>
                <td class="border1" colspan="1">'.$v['payable_amount'].'</td>
                <td class="border1" colspan="1">'.$v['actual_payment'].'</td>
                <td class="border1" colspan="1">'.$v['payment_date'].'</td>
                <td class="border1" colspan="2">'.$v['memo'].'</td>
            </tr>';
            }
        }else{
            $html .= '<tr>
                <td class="border1" colspan="1"></td>
                <td class="border1" colspan="1"></td>
                <td class="border1" colspan="2"></td>
                <td class="border1" colspan="1"></td>
                <td class="border1" colspan="1"></td>
                <td class="border1" colspan="1"></td>
                <td class="border1" colspan="1"></td>
                <td class="border1" colspan="2"></td>
            </tr>';
        }


        $html .= '<tr>
            <td colspan="10" style="text-align: center;font-size: 20px; font-weight: bold;"></td>
        </tr>
         <tr>
             <td class="fontBlod" colspan="10" style="text-align: center;font-size: 14px;">本次付款详情</td>
        </tr>
         <tr>
            <td colspan="10" style="text-align: center;font-size: 20px; font-weight: bold;"></td>
        </tr>
        <tr>
            <td class="border1" colspan="1">合同号</td>
            <td class="border1" colspan="1">款号</td>
            <td class="border1" colspan="1">中文名</td>
            <td class="border1" colspan="1">颜色</td>
            <td class="border1" colspan="1">数量</td>
            <td class="border1" colspan="1">单价</td>
            <td class="border1" colspan="1">应付金额</td>
            <td class="border1" colspan="1">实付金额</td>
            <td class="border1" colspan="2">备注</td>
        </tr>';

        foreach ($billDetail->bill_items as $v){
            $html .= '<tr>
            <td class="border1" colspan="1">'.$v['contract_no'].'</td>
            <td class="border1" colspan="1">'.$v['spu'].'</td>
            <td class="border1" colspan="1">'.$v['name'].'</td>
            <td class="border1" colspan="1">'.$v['color_name'].'/'.$v['color_identifying'].'</td>
            <td class="border1" colspan="1">'.$v['num'].'</td>
            <td class="border1" colspan="1">'.$v['price'].'</td>
            <td class="border1" colspan="1">'.$v['total_price'].'</td>
            <td class="border1" colspan="1">'.$v['actual_total_price'].'</td>
            <td class="border1" colspan="2"></td>
        </tr>';
        }

        $html .= '<tr>
            <td colspan="10" style="text-align: center;font-size: 20px; font-weight: bold;"></td>
        </tr>
        <tr>
            <td class="border1" colspan="3">审批部门</td>
            <td class="border1" colspan="3">审批签字</td>
            <td class="border1" colspan="4">审批意见</td>
        </tr>
        <tr>
            <td class="border1" colspan="3">经办人</td>
            <td class="border1" colspan="3"></td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="3">直属主管</td>
            <td class="border1" colspan="3"></td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="3">财务稽核人员</td>
            <td class="border1" colspan="3"></td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="3">财务主管</td>
            <td class="border1" colspan="3"></td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="3">财务经理</td>
            <td class="border1" colspan="3"></td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="3">总经理</td>
            <td class="border1" colspan="3"></td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: left;"></td>
        </tr>
    </table>
</body>
</html>';

        return ['html' => $html, 'title' => $billDetail->bill_type_name];
    }

    public function getDelayedGoodsTransfersDetail($params)
    {
        $contractMdl = db::table('cloudhouse_contract_total')
            ->whereIn('contract_no', $params['contract_no'] ?? [])
            ->get(['contract_no', 'report_date'])
            ->keyBy('contract_no');
        $detailMdl = db::table('goods_transfers_detail as a')
            ->leftJoin('goods_transfers as b', 'a.order_no', '=', 'b.order_no')
            ->where('b.type_detail', 2)
            ->whereIn('a.contract_no', $params['contract_no'])
            ->whereIn('b.is_push', [3, 5])
            ->where('a.is_created_delayed', 0);
        if (isset($params['spu']) && !empty($params['spu'])){
            $spu = db::table('self_spu')->where('spu', 'like', '%'.$params['spu'].'%')
                ->orWhere('old_spu', 'like', '%'.$params.'%')
                ->get();
            $spuIds = [];
            if ($spu->isNotEmpty()){
                $spu = $spu->toArrayList();
                $spuIds = array_column($spu, 'Id');
            }
            $detailMdl = $detailMdl->whereIn('a.spu_id', $spuIds);
        }
        $detailMdl = $detailMdl->get(['a.*', 'b.delivery_time', 'b.type_detail', 'b.platform_id']);
        // 获取平台名称
        $platformMdl = db::table('platform')
            ->get(['name', 'Id'])
            ->keyBy('Id');
        // 获取商品基础信息
        $customSkuMdl = db::table('self_custom_sku as a')
            ->leftJoin('self_color_size as b', 'a.color_id', '=', 'b.id')
            ->get(['a.*', 'b.name as color_name', 'b.identifying as color_identifying']);
        // 获取合同单价
        $contractSpuMdl = db::table('cloudhouse_contract')
            ->whereIn('contract_no', $params['contract_no'])
            ->get();
        $customSku = [];
        foreach ($customSkuMdl as $v){
            $customSku[$v->id] = $v;
        }
        $contractSpu = [];
        foreach ($contractSpuMdl as $v){
            $key = $v->spu_id.'-'.$v->color_identifying;
            $contractSpu[$key] = $v;
        }
        $list = [];
        foreach ($detailMdl as $v){
            $contract = $contractMdl[$v->contract_no] ?? [];
            if (!empty($contract)) {
                $delayedDate = date('Y-m-d', strtotime($contract->report_date . '+ 8 days'));
                if ($v->delivery_time > $delayedDate){
                    $v->report_date = $contract->report_date;
                    $v->delayed_day_num = (strtotime(date('Y-m-d', strtotime($v->delivery_time))) - strtotime($delayedDate)) / (60*60*24);
                    $v->ratio_name = '7.5‱';
                    $v->ratio = 0.075;
                    $v->platform_name = $platformMdl[$v->platform_id]->name ?? '';

                    if (isset($customSku[$v->custom_sku_id])) {
                        $v->color_id = $customSku[$v->custom_sku_id]->color_id;
                        $v->color_name = $customSku[$v->custom_sku_id]->color_name;
                        $v->color_identifying = $customSku[$v->custom_sku_id]->color_identifying;
                        $v->name = $customSku[$v->custom_sku_id]->name;
                        $v->size = $customSku[$v->custom_sku_id]->size;
                        $v->num = $v->receive_num;
                        $v->goods_transfers_detail_id = $v->id;
                        // 获取合同单价
                        $key = $v->spu_id . '-' . $v->color_identifying;
                        if (isset($contractSpu[$key])) {
                            $v->price = $contractSpu[$key]->one_price;
                            $v->total_price = $contractSpu[$key]->one_price * $v->receive_num;
                            $v->delayed_price = bcmul(bcmul($v->total_price, $v->delayed_day_num, 8), 0.00075, 2); // 货值 * 超期天数（送货时间-交货时间） * 万分之七点五
                        } else {
                            $v->price = 0;
                            $v->total_price = 0;
                            $v->delayed_price = 0;
                        }
                        if ($v->type_detail == 2) {
                            $v->in_out_house_type = 1;
                            $v->in_out_house_type_name = '采购入库';
                            $v->in_out_house_time = $v->receive_time;
                        }

                        $list[] = $v;
                    }
                }
            }
        }

        return ['code' => 200, 'msg' => '获取成功！', 'data' => ['count' => count($list), 'list' => $list]];
    }

    public function getFinancialBillDeductionList($params)
    {
        $list = db::table('financial_bill_deduction');

        if (isset($params['bill_no']) && !empty($params['bill_no'])){
            $list = $list->where('bill_no', $params['bill_no']);
        }

        $count = $list->count();

        if (isset($params['limit']) || isset($params['page'])){
            $limit = $params['limit'] ?? 30;
            $page = $params['page'] ?? 1;
            $offset = $limit * ($page - 1);
            $list = $limit->limit($limit)->offset($offset);
        }
        $list = $list->orderBy('created_at', 'desc')
            ->orderBy('is_deleted', 'ASC')
            ->get();

        foreach ($list as $v) {
            $v->deduction_ratio_name = $v->deduction_ratio.'%';
            $v->deduction_bill_type_name = Constant::BILL_TYPE[$v->deduction_bill_type] ?? '';
        }

        return ['code' => 200, 'msg' => '获取成功！', 'data' => ['count' => $count, 'list' => $list]];
    }

    public function getContractPaymentAccountList($params)
    {
        // 获取合同数据
        $contractList = db::table('cloudhouse_contract_total')
            ->where('contract_status', 2)
            ->whereIn('contract_class', [1, 2, 5]);

        if (isset($params['contract_no'])){
            $contractList = $contractList->where('contract_no', 'like', '%'.$params['contract_no'].'%');
        }

        if (isset($params['contract_class']) && is_array($params['contract_class'])){
            $contractList = $contractList->whereIn('contract_class', $params['contract_class']);
        }

        if (isset($params['supplier_id'])){
            $contractList = $contractList->whereIn('supplier_id', $params['supplier_id']);
        }

        if (isset($params['maker_id'])){
            $contractList = $contractList->where('maker_id', $params['maker_id']);
        }

        if (isset($params['input_user_id'])){
            $contractList = $contractList->where('input_user_id', $params['input_user_id']);
        }

        if (isset($params['contract_created_at'])){
            $contractList = $contractList->whereBetween('create_time', $params['contract_created_at']);
        }

        if (isset($params['report_date'])){
            $contractList = $contractList->whereBetween('report_date', $params['report_date']);
        }

        if (isset($params['report_date'])){
            $contractList = $contractList->whereBetween('report_date', $params['report_date']);
        }

        $count = $contractList->count();

        if ((isset($params['page']) || isset($params['limit'])) && (!isset($params['post_type']) || $params['post_type'] != 2)){
            $limit = $params['limit'] ?? 30;
            $page = $params['page'] ?? 1;
            $offsetNum = $limit * ($page-1);
            $contractList = $contractList->limit($limit)->offset($offsetNum);
        }

        $contractList = $contractList->orderBy('create_time', 'DESC')->get();

        // 获取定金和结算金额
        $billMdl = db::table('financial_bill')
//            ->where('is_payment', 1)
//            ->whereIn('status', [2,3])
            ->whereIn('bill_type', [1, 2])
            ->select(['contract_no', 'bill_no', 'actual_payment', 'bill_type', 'status'])
            ->get();
        // 获取合同已结算的附加费和延期扣款
        $billDeductionMdl = db::table('financial_bill_deduction')
            ->whereIn('deduction_bill_type', [1, 3, 4])
            ->where('is_deleted', 0)
            ->select(['contract_no', 'deduction_amount', 'deduction_bill_type', 'origin_bill_no'])
            ->get();
        // 处理合同已结算金额明细
        $contractBill = [];
        $dingJin = [];
        $allContractBill = [];
        foreach ($billMdl as $v){
            if (in_array($v->status, [2,3])){
                if ($v->bill_type == 2){
                    $contractBill[$v->contract_no][] = [
                        'contract_no' => $v->contract_no,
                        'bill_no' => $v->bill_no,
                        'amount' => $v->actual_payment,
                        'bill_type' => $v->bill_type,
                        'bill_type_name' => Constant::BILL_TYPE[$v->bill_type] ?? '',
                    ];
                }elseif ($v->bill_type == 1){
                    $dingJin[$v->contract_no][] = [
                        'contract_no' => $v->contract_no,
                        'bill_no' => $v->bill_no,
                        'amount' => $v->actual_payment,
                        'bill_type' => $v->bill_type,
                        'bill_type_name' => Constant::BILL_TYPE[$v->bill_type] ?? '',
                    ];
                }
            }
            if ($v->bill_type == 2){
                $allContractBill[$v->contract_no][] = [
                    'contract_no' => $v->contract_no,
                    'bill_no' => $v->bill_no,
                    'amount' => $v->actual_payment,
                    'bill_type' => $v->bill_type,
                    'bill_type_name' => Constant::BILL_TYPE[$v->bill_type] ?? '',
                ];
            }

        }
        $advancePaymentDeductedAmount = [];
        $delayedDeductionAmount = [];
        $surchargeDeductedAmount = [];
        $deductedAmount = []; // 合同抵扣金额
        foreach ($billDeductionMdl as $v){
            $deductedAmount[$v->contract_no][] = [
                'contract_no' => $v->contract_no,
                'bill_no' => $v->origin_bill_no,
                'amount' => $v->deduction_amount,
                'bill_type' => $v->deduction_bill_type,
                'bill_type_name' => Constant::BILL_TYPE[$v->deduction_bill_type] ?? '',
            ];
            if ($v->deduction_bill_type == 1){
                $advancePaymentDeductedAmount[$v->contract_no][] = [
                    'contract_no' => $v->contract_no,
                    'bill_no' => $v->origin_bill_no,
                    'amount' => $v->deduction_amount,
                    'bill_type' => $v->deduction_bill_type,
                    'bill_type_name' => Constant::BILL_TYPE[$v->deduction_bill_type] ?? '',
                ];
            }
            if ($v->deduction_bill_type == 3){
                $surchargeDeductedAmount[$v->contract_no][] = [
                    'contract_no' => $v->contract_no,
                    'bill_no' => $v->origin_bill_no,
                    'amount' => $v->deduction_amount,
                    'bill_type' => $v->deduction_bill_type,
                    'bill_type_name' => Constant::BILL_TYPE[$v->deduction_bill_type] ?? '',
                ];
            }
            if ($v->deduction_bill_type == 4){
                $delayedDeductionAmount[$v->contract_no][] = [
                    'contract_no' => $v->contract_no,
                    'bill_no' => $v->origin_bill_no,
                    'amount' => $v->deduction_amount,
                    'bill_type' => $v->deduction_bill_type,
                    'bill_type_name' => Constant::BILL_TYPE[$v->deduction_bill_type] ?? '',
                ];
            }
        }
        // 获取合同入库明细
        $goodTransfersDetailMdl = db::table('goods_transfers_detail as a')
            ->leftJoin('goods_transfers as b', 'a.order_no', '=', 'b.order_no')
            ->whereIn('type_detail', [2, 9])
            ->whereIn('is_push', [3, 5])
            ->select(['a.*', 'b.platform_id', 'b.delivery_time', 'b.type_detail'])
            ->get();
        // 获取合同单价
        $contractSkuMdl = db::table('cloudhouse_custom_sku')
            ->select(['contract_no', 'custom_sku_id', 'price'])
            ->get();
        // 获取平台信息
        $platform = db::table('platform')
            ->select(['Id', 'name'])
            ->get()
            ->keyBy('Id');
        $contractSku = [];
        foreach ($contractSkuMdl as $v){
            $key = $v->contract_no.'-'.$v->custom_sku_id;
            $contractSku[$key] = $v->price;
        }
        // 处理合同出库出库未结算明细
        $inHouseUnsettled = []; // 已入库未结算
        $outHouseUnsettled = []; // 已出库未结算
        $inHouseSettled = []; // 已入库已结算
        $outHouseSettled = []; // 已出库已结算
        foreach ($goodTransfersDetailMdl as $v){
            $priceKey = $v->contract_no.'-'.$v->custom_sku_id;
            $price = $contractSku[$priceKey] ?? 0.00;
            if ($v->is_settled == 1){
                if ($v->type_detail == 2){
                    $inHouseSettled[$v->contract_no][] = [
                        'goods_transfers_detail_id' => $v->id,
                        'contract_no' => $v->contract_no,
                        'spu_id' => $v->spu_id,
                        'spu' => $v->spu,
                        'custom_sku_id' => $v->custom_sku_id,
                        'custom_sku' => $v->custom_sku,
                        'num' => $v->receive_num,
                        'delivery_time' => $v->delivery_time,
                        'platform_id' => $v->platform_id,
                        'price' => $price,
                        'total_price' => bcmul($price, $v->receive_num, 2),
                        'platform_name' => $platform[$v->platform_id]->name ?? '',
                    ];
                }
                if ($v->type_detail == 9){
                    $outHouseSettled[$v->contract_no][] = [
                        'goods_transfers_detail_id' => $v->id,
                        'contract_no' => $v->contract_no,
                        'spu_id' => $v->spu_id,
                        'spu' => $v->spu,
                        'custom_sku_id' => $v->custom_sku_id,
                        'custom_sku' => $v->custom_sku,
                        'num' => $v->receive_num,
                        'delivery_time' => $v->delivery_time,
                        'platform_id' => $v->platform_id,
                        'price' => $price,
                        'total_price' => bcmul($price, $v->receive_num, 2),
                        'platform_name' => $platform[$v->platform_id]->name ?? '',
                    ];
                }
            }
            if ($v->is_settled == 0){
                if ($v->type_detail == 2){
                    $inHouseUnsettled[$v->contract_no][] = [
                        'goods_transfers_detail_id' => $v->id,
                        'contract_no' => $v->contract_no,
                        'spu_id' => $v->spu_id,
                        'spu' => $v->spu,
                        'custom_sku_id' => $v->custom_sku_id,
                        'custom_sku' => $v->custom_sku,
                        'num' => $v->receive_num,
                        'delivery_time' => $v->delivery_time,
                        'platform_id' => $v->platform_id,
                        'price' => $price,
                        'total_price' => bcmul($price, $v->receive_num, 2),
                        'platform_name' => $platform[$v->platform_id]->name ?? '',
                    ];
                }
                if ($v->type_detail == 9){
                    $outHouseUnsettled[$v->contract_no][] = [
                        'goods_transfers_detail_id' => $v->id,
                        'contract_no' => $v->contract_no,
                        'spu_id' => $v->spu_id,
                        'spu' => $v->spu,
                        'custom_sku_id' => $v->custom_sku_id,
                        'custom_sku' => $v->custom_sku,
                        'num' => $v->receive_num,
                        'delivery_time' => $v->delivery_time,
                        'platform_id' => $v->platform_id,
                        'price' => $price,
                        'total_price' => bcmul($price, $v->receive_num, 2),
                        'platform_name' => $platform[$v->platform_id]->name ?? '',
                    ];
                }
            }
        }
        $totalContractPrice = 0.00;
        $totalActualPaymentAmount = 0.00;
        $totalAdvancePaymentAmount = 0.00;
        $totalAdvancePaymentNotDeductedAmount = 0.00;
        $totalContractCount = 0;
        $totalInHouseActualNum = 0;
        $totalNotInHouseActualAmount = 0.00;
        foreach ($contractList as $v){
            // 创建人
            $v->input_user_name = $this->GetUsers($v->input_user_id)['account'] ?? '';
            // 经办人
            $v->maker_name = $this->GetUsers($v->maker_id)['account'] ?? '';
            // 合同日期
            $v->sign_date = date('Y-m-d', strtotime($v->sign_date));
            // 交货日期
            $v->report_date = date('Y-m-d', strtotime($v->report_date));
            // 是否完结
            $v->is_end_name = $v->is_end ? '已完结' : '未完结';
            // 合同类型
            $v->contract_type_name = $v->contract_type;
            // 合同分类
            $v->contract_class_name = Constant::CONTRACT_CLASS[$v->contract_class] ?? '';
            // 已结算入库金额
            $v->in_house_settled_amount = 0.00;
            $v->in_house_settled_amount_detail = [];
            $v->in_house_settled_num = 0;
            if (isset($inHouseSettled[$v->contract_no])){
                $amount = $this->floorDecimal(array_sum(array_column($inHouseSettled[$v->contract_no], 'total_price')));
                $num = array_sum(array_column($inHouseSettled[$v->contract_no], 'num'));
                $v->in_house_settled_amount = $amount;
                $v->in_house_settled_amount_detail = $inHouseSettled[$v->contract_no];
                $v->in_house_settled_num = $num;
            }
            // 未结算入库金额
            $v->in_house_unsettled_amount = 0.00;
            $v->in_house_unsettled_amount_detail = [];
            $v->in_house_unsettled_num = 0;
            if (isset($inHouseUnsettled[$v->contract_no])){
                $amount = $this->floorDecimal(array_sum(array_column($inHouseUnsettled[$v->contract_no], 'total_price')));
                $num = array_sum(array_column($inHouseUnsettled[$v->contract_no], 'num'));
                $v->in_house_unsettled_amount = $amount;
                $v->in_house_unsettled_amount_detail = $inHouseUnsettled[$v->contract_no];
                $v->in_house_unsettled_num = $num;
            }
            // 已结算出库金额
            $v->out_house_settled_amount = 0.00;
            $v->out_house_settled_amount_detail = [];
            $v->out_house_settled_num = 0;
            if (isset($outHouseSettled[$v->contract_no])){
                $amount = $this->floorDecimal(array_sum(array_column($outHouseSettled[$v->contract_no], 'total_price')));
                $num = array_sum(array_column($outHouseSettled[$v->contract_no], 'num'));
                $v->out_house_settled_amount = $amount;
                $v->out_house_settled_amount_detail = $outHouseSettled[$v->contract_no];
                $v->out_house_settled_num = $num;
            }
            // 未结算出库金额
            $v->out_house_unsettled_amount = 0.00;
            $v->out_house_unsettled_amount_detail = [];
            $v->out_house_unsettled_num = 0;
            if (isset($outHouseUnsettled[$v->contract_no])){
                $amount = $this->floorDecimal(array_sum(array_column($outHouseUnsettled[$v->contract_no], 'total_price')));
                $num = array_sum(array_column($outHouseUnsettled[$v->contract_no], 'num'));
                $v->out_house_unsettled_amount = $amount;
                $v->out_house_unsettled_amount_detail = $outHouseUnsettled[$v->contract_no];
                $v->out_house_unsettled_num = $num;
            }

            // 已结算实际入库金额 = 已结算入库金额 - 已结算出库金额
            $v->in_house_settled_actual_amount =  bcsub($v->in_house_settled_amount, $v->out_house_settled_amount, 2);
            // 未结算实际入库金额 = 未结算入库金额 - 未结算出库金额
            $v->in_house_unsettled_actual_amount =  bcsub($v->in_house_unsettled_amount, $v->out_house_unsettled_amount, 2);

            // 已结算实际入库数量 = 已结算入库数量 - 已结算出库数量
            $v->in_house_settled_actual_num =  bcsub($v->in_house_settled_num, $v->out_house_settled_num, 2);
            // 未结算实际入库数量 = 未结算入库数量 - 未结算出库数量
            $v->in_house_unsettled_actual_num =  bcsub($v->in_house_unsettled_num, $v->out_house_unsettled_num, 2);

            // 实际入库金额 = 已结算实际入库金额 + 未结算实际入库金额
            $v->in_house_actual_amount = bcadd($v->in_house_settled_actual_amount, $v->in_house_unsettled_actual_amount, 2);
            // 实际入库数量 = 已结算实际入库数量 + 未结算实际入库数量
            $v->in_house_actual_num = bcadd($v->in_house_settled_actual_num, $v->in_house_unsettled_actual_num, 2);

            // 实际未入库金额(未付金额) = 合同总额 - 实际已入库金额
            $v->not_in_house_actual_amount = bcsub($v->total_price, $v->in_house_actual_amount, 2);


            // 未入库订单数量 = 合同总数量 - 实际已入库订单数量
            $v->not_in_house_actual_num = bcsub($v->total_count, $v->in_house_actual_num);

            // 已结算实付金额 = 已付金额
            $v->settled_actual_payment_amount = 0.00;
            if (isset($contractBill[$v->contract_no])){
                $amount = $this->floorDecimal(array_sum(array_column($contractBill[$v->contract_no], 'amount')));
                $v->settled_actual_payment_amount = $amount;
                $v->settled_actual_payment_amount_detail = $contractBill[$v->contract_no];
            }
            $v->settled_actual_payment_amount_ratio = $this->floorDecimal(($v->settled_actual_payment_amount/$v->total_price) * 100);
            $v->not_settled_actual_payment_amount = $v->total_price - $v->settled_actual_payment_amount;
            $v->not_settled_actual_payment_amount_ratio = $this->floorDecimal(100 - $v->settled_actual_payment_amount_ratio);

            $v->settled_actual_payment_amount_ratio = $v->settled_actual_payment_amount_ratio.'%';
            $v->not_settled_actual_payment_amount_ratio = $v->not_settled_actual_payment_amount_ratio.'%';

            // 预付款金额
            $v->advance_payment_amount = 0.00;
            $v->advance_payment_amount_detail = [];
            if (isset($dingJin[$v->contract_no])){
                $amount = $this->floorDecimal(array_sum(array_column($dingJin[$v->contract_no], 'amount')));
                $v->advance_payment_amount = $amount;
                $v->advance_payment_amount_detail = $dingJin[$v->contract_no];
            }

            // 已抵扣定金金额 （预付款已冲销金额）
            $v->advance_payment_deducted_amount = 0.00;
            $v->advance_payment_deducted_amount_detail = [];
            if (isset($advancePaymentDeductedAmount[$v->contract_no])){
                $amount = $this->floorDecimal(array_sum(array_column($advancePaymentDeductedAmount[$v->contract_no], 'amount')));
                $v->advance_payment_deducted_amount = $amount;
                $v->advance_payment_deducted_amount_detail = $advancePaymentDeductedAmount[$v->contract_no];
            }

            // 预付款待冲销金额 = 预付款金额 - 预付款已冲销金额
            $v->advance_payment_not_deducted_amount = bcsub($v->advance_payment_amount, $v->advance_payment_deducted_amount, 2);

            // 实际应付金额 = 已请应付账款总额
//            $v->actual_payment_amount = bcadd($v->settled_actual_payment_amount, $v->in_house_unsettled_actual_amount, 2);
            $v->actual_payment_amount = 0.00;
            if (isset($allContractBill[$v->contract_no])){
            $amount = $this->floorDecimal(array_sum(array_column($allContractBill[$v->contract_no], 'amount')));
            $v->actual_payment_amount = $amount;
            $v->actual_payment_amount_detail = $allContractBill[$v->contract_no];
        }

            // 供应商附加费抵扣 = 供应商附加费 + 延期扣款费
            $v->surcharge_amount = 0.00;
            $v->surcharge_amount_detail = [];
            $surchargeAmount = 0.00;
            $surchargeAmountDetail = [];
            if (isset($delayedDeductionAmount[$v->contract_no])){
                $amount = $this->floorDecimal(array_sum(array_column($delayedDeductionAmount[$v->contract_no], 'total_price')));
                $surchargeAmount += $amount;
                array_merge($surchargeAmountDetail, $delayedDeductionAmount[$v->contract_no]);
            }
            if (isset($surchargeDeductedAmount[$v->contract_no])){
                $amount = $this->floorDecimal(array_sum(array_column($surchargeDeductedAmount[$v->contract_no], 'total_price')));
                $surchargeAmount += $amount;
                array_merge($surchargeAmountDetail, $surchargeDeductedAmount[$v->contract_no]);
            }
            $v->surcharge_amount = $surchargeAmount;
            $v->surcharge_amount_detail = $surchargeAmountDetail;

            // 合同完结时，未入库数量及未支付金额清零
            if ($v->is_end == 1){
                $v->not_in_house_actual_amount = 0.00;
                $v->not_in_house_actual_num = 0;
            }

            $v->payment_method_name = Constant::CONTRACT_PERIODIC_SETTLEMENT_METHOD[$v->periodic_id] ?? '未定义结算方式';

            // 合计
            $totalContractPrice += $v->total_price;
            $totalActualPaymentAmount += $v->in_house_actual_amount;
            $totalAdvancePaymentAmount += $v->advance_payment_amount;
            $totalAdvancePaymentNotDeductedAmount += $v->advance_payment_not_deducted_amount;
            $totalContractCount += $v->total_count;
            $totalInHouseActualNum += $v->in_house_actual_num;
            $totalNotInHouseActualAmount += $v->not_in_house_actual_amount;
        }

        if (isset($params['post_type']) && $params['post_type'] == 2){
            $p['title']='合同应付帐列表'.time();
            $title = [
                'is_end_name'  => '是否完结',
                'contract_no'  => '合同号',
                'contract_type_name' => '合同类型',
                'contract_class_name' => '合同分类',
                'create_time'     => '合同日期',
                'report_date'            => '交货日期',
                'company_name'  => '公司抬头',
                'supplier_short_name'            => '供应商名称',
                'total_count'     => '合同数量',
                'total_price'       => '合同金额',
                'actual_payment_amount'    => '实际应付金额',
                'in_house_actual_num'     => '实际已入库数量',
                'in_house_actual_amount'      => '实际入库金额',
                'not_in_house_actual_num'     => '实际未入库数量',
                'not_in_house_actual_amount'     => '实际未入库金额',
                'advance_payment_amount'     => '预付款金额',
                'advance_payment_deducted_amount'     => '预付已冲销金额',
                'advance_payment_not_deducted_amount'     => '预付待冲销金额',
                'surcharge_amount'     => '供应商附加费用',
                'settled_actual_payment_amount'     => '已付金额',
                'settled_actual_payment_amount_ratio'     => '已付金额占比',
                'not_settled_actual_payment_amount'     => '剩余未付金额',
                'not_settled_actual_payment_amount_ratio'     => '未付金额占比',
                'payment_method_name' => '结算方式',
                'input_user_name'     => '创建人',
                'maker_name'     => '经办人',
            ];

            $p['title_list']  = $title;
            $p['data'] =  $contractList;
            $userId = 1;
            if (!isset($params['user_id']) || empty($params['user_id'])){
                if (isset($params['token']) && !empty($params['token'])){
                    $users = db::table('users')->where('token',$params['token'])->first();
                    if($users){
                        $userId = $users->Id;
                    }
                }
            }
            $find_user_id = $userId;
            $p['user_id'] = $find_user_id;
            $p['type'] = '合同应付帐列表导出';

            $job = new \App\Http\Controllers\Jobs\ExcelTaskController;
            try {
                $job::runJob($p);
            } catch(\Exception $e){
                return ['type' => 'fail', 'msg' => '加入队列失败'];
            }
        }

        return ['code' => 200, 'msg' => '获取成功！', 'data' => ['count' => $count, 'list' => $contractList, 'total_price' => $totalContractPrice, 'in_house_actual_amount' => $totalActualPaymentAmount, 'advance_payment_amount' => $totalAdvancePaymentAmount, 'advance_payment_not_deducted_amount' => $totalAdvancePaymentNotDeductedAmount, 'total_count' => $totalContractCount, 'in_house_actual_num' => $totalInHouseActualNum, 'not_in_house_actual_amount' => $totalNotInHouseActualAmount]];
    }

    public function getContractPeriodicArrangementList($params)
    {
        $list = db::table('goods_transfers_detail as a')
            ->leftJoin('goods_transfers as b', 'a.order_no', '=', 'b.order_no')
            ->whereIn('b.is_push', [3, 5]);
        if (isset($params['contract_no'])){
            $list = $list->where('a.contract_no', 'like', '%'.$params['contract_no'].'%');
        }
        if (isset($params['supplier_id'])){
            $contractMdl = db::table('cloudhouse_contract_total')
                ->where('supplier_id', $params['supplier_id'])
                ->get()
                ->toArrayList();
            $contractNo = array_column($contractMdl, 'contract_no');
            $list = $list->whereIn('a.contract_no', $contractNo);
        }
        if (isset($params['spu'])){
            $spuMdl = db::table('self_spu')
                ->where('spu', 'like', '%'.$params['spu'].'%')
                ->orWhere('old_spu', 'like', '%'.$params['spu'].'%')
                ->get()
                ->toArrayList();
            $spuIds = array_column($spuMdl, 'id');
            $list = $list->whereIn('a.spu_id', $spuIds);
        }
        if (isset($params['custom_sku'])){
            $customSkuMdl = db::table('self_custom_sku')
                ->where('custom_sku', 'like', '%'.$params['custom_sku'].'%')
                ->orWhere('old_custom_sku', 'like', '%'.$params['custom_sku'].'%')
                ->get()
                ->toArrayList();
            $customSkuIds = array_column($customSkuMdl, 'id');
            $list = $list->whereIn('a.custom_sku_id', $customSkuIds);
        }
        if (isset($params['delivery_time'])){
            $list = $list->whereBetween('b.delivery_time', $params['delivery_time']);
        }
        if (isset($params['receive_time'])){
            $list = $list->whereBetween('a.receive_time', $params['receive_time']);
        }
        if (isset($params['type_detail']) && in_array($params['type_detail'], [2, 9])){
            $list = $list->where('b.type_detail', $params['type_detail']);
        }else{
            $list = $list->whereIn('b.type_detail', [2, 9]);
        }

        $count = $list->count();
        if (isset($params['limit']) || isset($params['page'])){
            $limit = $params['limit'] ?? 30;
            $page = $params['page'] ?? 1;
            $offsetNum = $limit * ($page - 1);
            $list = $list->limit($limit)
                ->offset($offsetNum);
        }
        $list = $list->orderBy('a.createtime', 'DESC')
            ->select(['a.*', 'b.in_house', 'b.out_house', 'b.type_detail', 'b.platform_id', 'b.delivery_time', 'b.push_time'])
            ->get();
        $contractSkuMdl = db::table('cloudhouse_custom_sku')
            ->get();
        $contractSkuList = [];
        foreach ($contractSkuMdl as $cSku){
            $key = $cSku->contract_no.'-'.$cSku->custom_sku_id;
            $cSku->total_price = $this->floorDecimal($cSku->price*$cSku->num);
            $contractSkuList[$key] = $cSku;
        }
        $contractSpuMdl = db::table('cloudhouse_contract')
            ->get();
        $contractSpuList = [];
        foreach ($contractSpuMdl as $cSpu){
            $key = $cSpu->contract_no.'-'.$cSpu->spu_id;
            $contractSpuList[$key] = $cSpu;
        }
        $contractMdl = db::table('cloudhouse_contract_total')
            ->where('contract_status', 2)
            ->get()
            ->keyBy('contract_no');
        $dingjinMdl = db::table('financial_bill')
            ->where('bill_type', 1)
            ->where('is_payment' , 1)
            ->get();
        $dingjinList = [];
        foreach ($dingjinMdl as $dj){
            if (isset($dingjinList[$dj->contract_no])){
                $dingjinList[$dj->contract_no] += $dj->payable_amount;
            }else{
                $dingjinList[$dj->contract_no] = $dj->payable_amount;
            }
        }
        $dikouMdl = db::table('financial_bill_deduction')
            ->where('is_deleted', 0)
            ->where('deduction_bill_type', 1)
            ->get();
        $dikouList = [];
        foreach ($dikouMdl as $dk){
            if (isset($dikouList[$dk->contract_no])){
                $dikouList[$dk->contract_no] += $dk->deduction_amount;
            }else{
                $dikouList[$dk->contract_no] = $dk->deduction_amount;
            }
        }
        $warehouseMdl = db::table('cloudhouse_warehouse')
            ->get(['id', 'warehouse_name'])
            ->pluck('warehouse_name', 'id');
        foreach ($list as $v){
            $key1 = $v->contract_no.'-'.$v->custom_sku_id;
            $key2 = $v->contract_no.'-'.$v->spu_id;
            $ratio = 1;
            if ($v->type_detail == 9){
                $ratio = -1;
            }
            $contractSku = $contractSkuList[$key1] ?? [];
            $contractSpu = $contractSpuList[$key2] ?? [];
            $contract = $contractMdl[$v->contract_no] ?? [];
            $v->price = $contractSku->price ?? 0.00;
            $v->contract_total_price = $contractSku->total_price ?? 0.00;
            $v->contract_num = $contractSku->num ?? 0;
            $v->receive_num = $v->receive_num * $ratio;
            $v->total_price = $this->floorDecimal($v->price*$v->receive_num);
            $v->is_settled_price = 0.00;
            if ($v->is_settled == 1){
                $v->is_settled_price = $v->total_price;
            }
            $v->not_is_settled_price = bcsub($v->total_price, $v->is_settled_price, 2);
            $v->supplier_name = $contract->supplier_short_name ?? '';
            $v->report_date = $contractSpu->report_date ?? '';
            $periodic_id = $contract->periodic_id ?? 0;
            $v->delivery_time = $v->push_time;
            $v->periodic_type = '';
            $v->periodic_num = '0天';
            $v->periodic_date = date('Y-m-d',strtotime($v->delivery_time));
            if (in_array($periodic_id, [1,2,3,6])){
                $v->periodic_type = '月结';
                $v->periodic_num = '30天';
                $v->periodic_date = date('Y-m-d', strtotime(date('Y-m-d', strtotime(date('Y-m-01', strtotime($v->delivery_time)).'+1 month -1 day')).'+30 day'));
            }
            if (in_array($periodic_id, [4,5])){
                $v->periodic_type = '非月结';
                $v->periodic_num = '60天';
                $v->periodic_date = date('Y-m-d', strtotime(date('Y-m-d', strtotime(date('Y-m-01', strtotime($v->delivery_time)).'+1 month -1 day')).'+60 day'));
            }
            if (in_array($periodic_id, [8,11])){
                $v->periodic_type = '非月结';
                $v->periodic_num = '15天';
                $v->periodic_date = date('Y-m-d', strtotime(date('Y-m-d', strtotime(date('Y-m-01', strtotime($v->delivery_time)).'+1 month -1 day')).'+15 day'));
            }
            if (in_array($periodic_id, [9])){
                $v->periodic_type = '非月结';
                $v->periodic_num = '45天';
                $v->periodic_date = date('Y-m-d', strtotime(date('Y-m-d', strtotime(date('Y-m-01', strtotime($v->delivery_time)).'+1 month -1 day')).'+45 day'));
            }
            if (in_array($periodic_id, [12])){
                $v->periodic_type = '非月结';
                $v->periodic_num = '7天';
                $v->periodic_date = date('Y-m-d', strtotime(date('Y-m-d', strtotime(date('Y-m-01', strtotime($v->delivery_time)).'+1 month -1 day')).'+7 day'));
            }
            $v->in_house_name = $warehouseMdl[$v->in_house] ?? '';
            $v->out_house_name = $warehouseMdl[$v->out_house] ?? '';
            $v->type_detail_name = Constant::GOODS_TRANSFERS_TYPE_DETAIL[$v->type_detail] ?? '';

            $v->dingjin_account = 0;
            $v->not_deducted_dingjin_account = 0;
            if (isset($dingjinList[$v->contract_no])){
                $v->dingjin_account = $this->floorDecimal($dingjinList[$v->contract_no]);
                if (isset($dikouList[$v->contract_no])){
                    $v->not_deducted_dingjin_account = bcsub($v->dingjin_account, $dikouList[$v->contract_no], 2);
                }
            }
        }

        return ['code' => 200, 'msg' => '获取成功！', 'data' => ['count' => $count, 'list' => $list]];
    }

    public function updateFinanceBillPdfFile($params)
    {
        try {
            if (!isset($params['id']) || !is_array($params['id']) || empty($params['id'])){
                throw new \Exception('未给定票据标识或者给定参数格式不正确！');
            }
            if (!isset($params['user_id']) || empty($params['user_id'])){
                if (isset($params['token']) && !empty($params['token'])){
                    $users = db::table('users')->where('token',$params['token'])->first();
                    if($users){
                        $params['user_id'] = $users->Id;
                    }else{
                        throw new \Exception('未给定操作人标识！');
                    }
                }else{
                    throw new \Exception('未给定操作人标识！');
                }
            }
            $billMdl = db::table('financial_bill')
                ->whereIn('id', $params['id'])
                ->get();
            $jobMdl = new SaveFinanceBillPdfController();
            foreach ($billMdl as $v){
                $billTypeName = Constant::BILL_TYPE[$v->bill_type];
                $data = [
                    'title' => '财务票据_'.$billTypeName.'_'.$v->bill_no,
                    'type' => '财务票据_'.$billTypeName,
                    'user_id' => $params['user_id'],
                    'id' => $v->id
                ];
                $jobMdl::runJob($data);
            }

            return ['code' => 200, 'msg' => '手动更新成功！请等待1-2分钟再次点击打印预览，否则可能导致当前预览非最新数据！'];
        }catch (\Exception $e){
            return ['code' => 500, 'msg' => '更新失败！原因：'.$e->getMessage().'；位置：'.$e->getLine()];
        }
    }
}