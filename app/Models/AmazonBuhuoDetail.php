<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AmazonBuhuoDetail extends Model
{
    //
    protected $table = 'amazon_buhuo_detail';
    protected $guarded = [];
    public $timestamps = false;
}
