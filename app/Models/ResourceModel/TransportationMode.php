<?php

namespace App\Models\ResourceModel;

use Illuminate\Database\Eloquent\Model;

class TransportationMode extends Model
{
    /**
     * 与模型关联的数据表.
     * @var string
     */
    protected $table = 'transportation_mode';
    /**
     * 与数据表关联的主键.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}