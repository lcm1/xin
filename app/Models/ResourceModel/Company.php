<?php

namespace App\Models\ResourceModel;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    /**
     * 与模型关联的数据表.
     * @var string
     */
    protected $table = 'company';
    /**
     * 与数据表关联的主键.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * 指示模型是否主动维护时间戳。
     *
     * @var bool
     */
    public $timestamps = false;
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}