<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class WarehouseStatDayModel extends BaseModel
{

    public function add()
    {
        $day = date('Y-m-d', strtotime('-1 days'));;
        $startDay = $day . ' 00:00:00';
        $endDay   = $day . ' 23:59:59';


        $record = DB::table('cloudhouse_record')
            ->select(['warehouse_id', 'type', DB::raw('sum(num) as num')])
            ->whereIn('warehouse_id', [1, 2, 3])
            ->whereBetween('createtime', [$startDay, $endDay])
            ->groupBy(['warehouse_id', 'type'])
            ->get();

        $create = date('Y-m-d H:i:s');
        $insert = [
            '1' => ['warehouse_id' => 1, 'in_num' => 0, 'out_num' => 0, 'inventory_num' => 0, 'difference_num' => 0, 'day' => $day, 'create_time' => $create],
            '2' => ['warehouse_id' => 2, 'in_num' => 0, 'out_num' => 0, 'inventory_num' => 0, 'difference_num' => 0, 'day' => $day, 'create_time' => $create],
            '3' => ['warehouse_id' => 3, 'in_num' => 0, 'out_num' => 0, 'inventory_num' => 0, 'difference_num' => 0, 'day' => $day, 'create_time' => $create],
        ];
        foreach ($record as $v) {
            if ($v->type == 1) {
                $insert[$v->warehouse_id] ['out_num'] = $v->num;
            } else {
                $insert[$v->warehouse_id] ['in_num'] = $v->num;
            }
        }

        $inventory = DB::table('self_custom_sku')
            ->select([
                DB::raw('sum(tongan_inventory) as tongan_inventory'),
                DB::raw('sum(quanzhou_inventory) as quanzhou_inventory'),
                DB::raw('sum(cloud_num) as cloud_num'),
            ])
            ->first();

        foreach ($insert as $k => $v) {
            $insert[$k]['difference_num'] = $v['in_num'] - $v['out_num'];
            if ($v['warehouse_id'] == 1) {
                $insert[$k]['inventory_num'] = $inventory->tongan_inventory ?: 0;
            } elseif ($v['warehouse_id'] == 2) {
                $insert[$k]['inventory_num'] = $inventory->quanzhou_inventory ?: 0;
            } elseif ($v['warehouse_id'] == 3) {
                $insert[$k]['inventory_num'] = $inventory->cloud_num ?: 0;
            }
        }

        DB::table('warehouse_stat_day')->insert($insert);

        return true;
    }

    public function GetDayListM($params)
    {
        $postType = $params['posttype'] ?? 1;
        $pagenum  = isset($params['page']) ? $params['page'] : 1;
        $limit    = isset($params['limit']) ? $params['limit'] : 30;

        $page = $pagenum - 1;
        if ($page != 0) {
            $page = $page * $limit;
        }

        $query = DB::table('warehouse_stat_day');

        if (isset($params['start_time']) && isset($params['end_time'])) {
            $startTime = $params['start_time'];
            $endTime   = $params['end_time'];
            $query->whereBetween('day', [$startTime, $endTime]);
        }

        if (isset($params['warehouse_id'])) {
            $query->where('warehouse_id', $params['warehouse_id']);
        }

        $totalNum = 0;
        if ($postType == 1) {
            $totalNum = $query->count();
            $query->offset($page)->limit($limit);
        }

        $list = $query->orderBy('id', 'desc')->get();

        foreach ($list as $v) {
            if ($v->warehouse_id == 1) {
                $v->warehouse_name = '同安仓';
            }
            if ($v->warehouse_id == 2) {
                $v->warehouse_name = '泉州仓';
            }
            if ($v->warehouse_id == 3) {
                $v->warehouse_name = '云仓';
            }
        }

        if ($postType == 2) {
            $ps['title']      = '出入库报告-按日' . time();
            $ps['title_list'] = [
                'warehouse_name' => '仓库名',
                'in_num'         => '入库总数',
                'out_num'        => '出库总数',
                'difference_num' => '出入库差额',
                'inventory_num'  => '结余库存',
                'day'            => '日期',
            ];

            $ps['data']    = $list->toArray();
            $ps['user_id'] = $params['find_user_id'] ?? 1;
            $ps['type']    = '出入库报告-按日';


            $job = new \App\Http\Controllers\Jobs\ExcelTaskController;
            try {
                $job::runJob($ps);
            } catch (\Exception $e) {
                return $this->FailBack('加入队列失败');
            }
            return $this->SBack([], '加入队列成功');
        }

        $data['total_num'] = $totalNum;
        $data['data']      = $list;
        return $this->SBack($data);
    }


    //成功返回
    public function SBack($data = [], $msg = '成功')
    {
        return ['type' => 'success', 'data' => $data, 'msg' => $msg];
    }

    //失败返回
    public function FailBack($msg)
    {
        return ['type' => 'fail', 'data' => [], 'msg' => $msg];
    }

}