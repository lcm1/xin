<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailReminder extends Model
{
    //
    protected $table = 'email_reminder';
    protected $guarded = [];
    public $timestamps = false;
}
