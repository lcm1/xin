<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AmazonRequestReport extends Model
{

    //
    protected $table = 'amazon_request_report';
    protected $guarded = [];
    public $timestamps = false;

}
