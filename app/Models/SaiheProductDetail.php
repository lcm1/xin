<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SaiheProductDetail extends Model
{
    //
    protected $table = 'saihe_product_detail';
    protected $guarded = [];
    public $timestamps = false;
}
