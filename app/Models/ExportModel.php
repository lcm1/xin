<?php

namespace App\Models;


use App\Http\Controllers\Jobs\ExcelTaskController;
use App\Http\Controllers\Pc\AmazonPlaceOrderContractController;
use App\Http\Controllers\Pc\ExportController;
use Illuminate\Support\Facades\DB;

class ExportModel extends BaseModel
{
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @Desc: 导出公共方法
     * @param $scene
     * @param $params
     * @return string[]
     * @author: Liu Sinian
     * @Time: 2023/3/8 16:41
     */
    public function exportData($scene, $params = [])
    {
        try {
            // 判断是否给定登录人 后续可以修改为通过token查询当前登录人
            if (!isset($params['export_user_id']) || empty($params['export_user_id'])){
                throw new \Exception('未给定登录人标识');
            }
            switch (ExportController::EXPORT_SCENE[$scene] ?? ''){
                case 'EXPORT_TRANSFER_DATA_DETAIL':
                    // 调拨详情导出
                    $InventoryModel = new InventoryModel();
                    $result = $InventoryModel->exportTransferDataDetail($params);
                    return ['type' => 'success','msg' => '加入队列成功'];
                    break;
                case 'EXPORT_REPLENISHMENT_PLAN_LIST':
                    // 补货计划列表导出
                    $DigitalModel = new DigitalModel();
                    $result = $DigitalModel->exportReplenishmentPlanList($params);
                    break;
                case 'EXPORT_CONTRACT_SHIPMENT_LIST':
                    // 合同出货导出
                    $cloudHouseModel = new CloudHouse();
                    $result = $cloudHouseModel->exportContractShipmentList($params);
                    break;
                case 'EXPORT_CUSTOM_SKU_MONTH_INVENTORY_COUNT_LIST':
                    // 月份库存变动列表导出
                    $customSkuMonthStockCountModel = new CustomSkuMonthStockCount();
                    $result = $customSkuMonthStockCountModel->exportCustomSkuMonthStockCountList($params);
                    break;
                case 'EXPORT_BUHUO_REQUEST_BOX_DATA':
                    // 补货装箱单导出
                    $DigitalModel = new DigitalModel();
                    $result = $DigitalModel->exportBuhuoRequestBoxData($params);
                    break;
                default:
                    throw new \Exception('场景值非预设值！');
            }

            if (empty($result)){
                throw new \Exception('未给定导出数据！');
            }
            if (!is_array($result)){
                // 加入导出队列
                $job = new ExcelTaskController();
                $job::runJob($result);
                return ['type' => 'success','msg' => '加入队列成功'];
            }else{
                // 加入导出队列
                $job = new ExcelTaskController();
                $job::runJob($result['data']);
                $errorMsg = '';
                if (isset($result['error_msg']) && !empty($result['error_msg'])){
                    $errorMsg = '，错误信息：'.$result['error_msg'];
                }
                return ['type' => 'success','msg' => '加入队列成功!'.$errorMsg];
            }

        }catch (\Exception $e){
            return ['type' => 'fail', 'msg' => '加入队列失败,原因：'.$e->getMessage().';错误定位：'.$e->getLine()];
        }

    }
}