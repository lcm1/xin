<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopeeOrder extends Model
{
    //
    protected $table = 'shopee_order';
    protected $guarded = [];
    public $timestamps = false;
}