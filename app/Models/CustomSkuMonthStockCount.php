<?php

namespace App\Models;


use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\ResourceModel\CustomSkuModel;
use App\Models\ResourceModel\CustomSkuMonthStockCountModel;
use App\Models\ResourceModel\SaiheInventoryModel;
use App\Models\ResourceModel\SaiheWarehouseLogModel;
use App\Models\ResourceModel\SpuModel;

class CustomSkuMonthStockCount extends BaseModel
{
    protected $customSkuMonthStockCountModel;
    protected $customSkuModel;
    protected $saiheInventoryModel;
    protected $warehouseLogModel;
    protected $spuModel;
    // 仓库代码
    const WAREHOUSE_CODE = [
        'TONGAN_WAREHOUSE' => 'TONGAN_WAREHOUSE',
        'QUANZHOU_WAREHOUSE' => 'QUANZHOU_WAREHOUSE'
    ];
    // 仓库代码名称
    const WAREHOUSE_CODE_NAME = [
        'TONGAN_WAREHOUSE' => '同安仓',
        'QUANZHOU_WAREHOUSE' => '泉州仓'
    ];
    const COMMISSION_RATIO = 0.15; // 提成比例
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->customSkuMonthStockCountModel = new CustomSkuMonthStockCountModel();
        $this->customSkuModel = new CustomSkuModel();
        $this->saiheInventoryModel = new SaiheInventoryModel();
        $this->warehouseLogModel = new SaiheWarehouseLogModel();
        $this->spuModel = new SpuModel();
    }

    /**
     * @Desc:保存上月库存变更日志
     * @param $params
     * @return array
     * @throws \Exception
     * @author: Liu Sinian
     * @Time: 2023/3/8 17:17
     */
    public function saveCustomSkuMonthStockCount($params = [])
    {
        try {
            // 获取上月第一天0点日期
            $startMonth = Carbon::today()->startOfMonth();
            $lastMonthStart = $startMonth->subMonth()->format('Y-m-d H:i:s');
            $lastMonthYear = $startMonth->startOfMonth()->format('Y');
            $lastMonth = $startMonth->startOfMonth()->format('m');
            // 获取当月第一天0点时间
            $endTime = Carbon::today()->startOfMonth()->format('Y-m-d H:i:s');
            $timeInterval = [$lastMonthStart, $endTime];

            // 搜索上月出入库日志
            $warehouseLog = $this->warehouseLogModel::query()
                ->whereBetween('operation_date', $timeInterval)
                ->get()
                ->toArray();
            $customSkuId = array_unique(array_column($warehouseLog, 'custom_sku_id'));
            $list = [];
            $tonganWarehouId = [2, 386];
            $quanzhouWarehouseId = [560];
            // 开启事务
            DB::beginTransaction();
//            $customSkuId = [12468, 13218, 13222];
//            var_dump($customSkuId);exit();
            $tonganError = "同安仓库存数量异常，库存sku：";
            $quanzhouError = "泉州仓库存数量异常，库存sku：";
            foreach ($customSkuId as $item){
                $quanzhouInStock = []; // 泉州入库数据
                $quanzhouOutStock = []; // 泉州仓出库数据
                $tonganInStock = []; // 同安仓入库数据
                $tonganOutStock = []; // 同安仓出库数据
                foreach ($warehouseLog as $_item){
                    if ($item == $_item['custom_sku_id']){
                        // 同安仓
                        if (in_array($_item['warehouse_id'], $tonganWarehouId)){
                            // 出库
                            if ($_item['type'] == 2 && !empty(strstr($_item['remark'], '手工出库'))){
                                $tonganOutStock[] = $_item;
                            }
                            // 入库
                            if ($_item['type'] == 1){
                                $tonganInStock[] = $_item;
                            }
                        }
                        // 泉州仓
                        if (in_array($_item['warehouse_id'], $quanzhouWarehouseId)){
                            // 出库
                            if ($_item['type'] == 2 && !empty(strstr($_item['remark'], '手工出库'))){
                                $quanzhouOutStock[] = $_item;
                            }
                            // 入库
                            if ($_item['type'] == 1){
                                $quanzhouInStock[] = $_item;
                            }
                        }
                    }
                }
                // 获取库存sku模型
                $customSku = $this->customSkuModel::query()
                    ->where('id', $item)
                    ->first();
                if (empty($customSku)){
                    continue;
                }
                // 获取spu模型
                $spuMdl = $this->spuModel::query()
                    ->where('id', $customSku->spu_id)
                    ->first();
                if (empty($spuMdl)){
                    continue;
                }
                // 加入泉州仓数据
                if (!empty($quanzhouInStock) || !empty($quanzhouOutStock)){
                    // 获取出入库数量合计
                    $quanzhouInStockNum = array_sum(array_column($quanzhouInStock, 'operation_num'));
                    $quanzhouOutStockNum = array_sum(array_column($quanzhouOutStock, 'operation_num'));
                    // 赛盒当月现有库存 逻辑更新：2月仓库库存+当月入库数量-当月出库数量
                    $saiheStock = $customSku->quanzhou_num + $quanzhouInStockNum - $quanzhouOutStockNum;
                    // 超过入库数量部分
                    $spillInStockNum =  $quanzhouOutStockNum - $quanzhouInStockNum;
                    if ($spillInStockNum > 0){ // 出库数量大于入库数量
                        // 计算计件数量
                        $pieceCount = $quanzhouInStockNum;
                        $stockRemainingNum = $customSku->quanzhou_num - $spillInStockNum; // 仓库剩余数量
                        if ($stockRemainingNum >= 0){ // 不超过2月泉州仓现有库存
                            // 资产 计算规则: 出库数量不大于入库数量部分 * 单价 * (1 + self::COMMISSION_RATIO) + 出库数量不大于2月库存数量部分 * 单价 * 1
                            $property =  $pieceCount * $spuMdl->price * (1 + self::COMMISSION_RATIO) + $spillInStockNum * $spuMdl->price * 1;
                            // 更新仓库剩余数量
//                            $customSkuId = $this->customSkuModel::query()->where('id', $customSku->id)->update(['quanzhou_num', $stockRemainingNum]);
                        }else{ // 超过现有库存，数据存在问题，需排查
                            $quanzhouError .= $customSku->custom_sku.',';
//                            continue;
                            $property = $pieceCount * $spuMdl->price * (1 + self::COMMISSION_RATIO) + $customSku->quanzhou_num * $spuMdl->price * 1;
                        }
                    }else{ // 出库数量不大于入库数量
                        $pieceCount = $quanzhouOutStockNum;
                        // 资产 计算规则: 出库数量不大于入库数量部分 * 单价 * (1 + self::COMMISSION_RATIO) + 出库数量不大于2月库存数量部分 * 单价 * 1
                        $property = $pieceCount * $spuMdl->price * (1 + self::COMMISSION_RATIO);
                    }
                    // 佣金计算
                    $commission = self::COMMISSION_RATIO * $pieceCount * $spuMdl->price;

                    $list[] = [
                        'custom_sku_id' => $item,
                        'spu_id' => $customSku->spu_id,
                        'year' => (int)$lastMonthYear,
                        'month' => (int)$lastMonth,
                        'warehouse_code' => self::WAREHOUSE_CODE['QUANZHOU_WAREHOUSE'],
                        'warehouse_stock' => $customSku->quanzhou_num,
                        'saihe_warehouse_stock' => $saiheStock ?? 0,
                        'in_warehouse_stock' => $quanzhouInStockNum,
                        'out_warehouse_stock' => $quanzhouOutStockNum,
                        'piece_count' => $pieceCount,
                        'commission_ratio' => self::COMMISSION_RATIO,
                        'commission' => round($commission, 2),
                        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                        'property' => round($property, 2) // 资产
                    ];
                }
                // 同安仓数据
                if (!empty($tonganInStock) || !empty($tonganOutStock)){
                    $tonganInStockNum = array_sum(array_column($tonganInStock, 'operation_num'));
                    $tonganOutStockNum = array_sum(array_column($tonganOutStock, 'operation_num'));
                    $saiheStock = $customSku->tongan_num + $tonganInStockNum - $tonganOutStockNum;
                    // 超过入库数量部分
                    $spillInStockNum =  $tonganOutStockNum - $tonganInStockNum;
                    if ($spillInStockNum > 0){ // 出库数量大于入库数量
                        // 计算计件数量
                        $pieceCount = $tonganInStockNum;
                        $stockRemainingNum = $customSku->tongan_num - $spillInStockNum; // 仓库剩余数量
                        if ($stockRemainingNum >= 0){ // 不超过2月泉州仓现有库存
                            // 资产 计算规则: 出库数量不大于入库数量部分 * 单价 * (1 + self::COMMISSION_RATIO) + 出库数量不大于2月库存数量部分 * 单价 * 1
                            $property =  $pieceCount * $spuMdl->price * (1 + self::COMMISSION_RATIO) + $spillInStockNum * $spuMdl->price * 1;
                            // 更新仓库剩余数量
//                            $customSkuId = $this->customSkuModel::query()->where('id', $customSku->id)->update(['tongan_num', $stockRemainingNum]);
                        }else{ // 超过现有库存，数据存在问题，需排查
                            $tonganError .= $customSku->custom_sku.',';
//                            continue;
                            $property =  $pieceCount * $spuMdl->price * (1 + self::COMMISSION_RATIO) + $customSku->tongan_num * $spuMdl->price * 1;
                        }
                    }else{ // 出库数量不大于入库数量
                        $pieceCount = $tonganOutStockNum;
                        // 资产 计算规则: 出库数量不大于入库数量部分 * 单价 * (1 + self::COMMISSION_RATIO) + 出库数量不大于2月库存数量部分 * 单价 * 1
                        $property = $pieceCount * $spuMdl->price * (1 + self::COMMISSION_RATIO);
                    }
                    // 佣金计算
                    $commission = self::COMMISSION_RATIO * $pieceCount * $spuMdl->price;

                    $list[] = [
                        'custom_sku_id' => $item,
                        'spu_id' => $customSku->spu_id,
                        'year' => (int)$lastMonthYear,
                        'month' => (int)$lastMonth,
                        'warehouse_code' => self::WAREHOUSE_CODE['TONGAN_WAREHOUSE'],
                        'warehouse_stock' => $customSku->tongan_num,
                        'saihe_warehouse_stock' => $saiheStock ?? 0,
                        'in_warehouse_stock' => $tonganInStockNum,
                        'out_warehouse_stock' => $tonganOutStockNum,
                        'piece_count' => $pieceCount,
                        'commission_ratio' => self::COMMISSION_RATIO,
                        'commission' => round($commission, 2), // 佣金
                        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                        'property' => round($property, 2) // 资产
                    ];
                }
            }
            $id = [];
            foreach ($list as $item){
                $id[] = $this->customSkuMonthStockCountModel::query()->insertGetId($item);
            }
            DB::commit();
        }catch (\Exception $e){
            DB::rollback();
            throw new \Exception('保存失败！原因：'.$e->getMessage());
        }

        return ['id' => $id, 'tongan_error' => $tonganError, 'quanzhou_error' => $quanzhouError];
    }

    /**
     * @Desc:获取上月库存统计列表
     * @param $params
     * @return array
     * @author: Liu Sinian
     * @Time: 2023/3/3 18:05
     */
    public function getCustomSkuMonthStockCountList($params)
    {
        $customSkuMonthStockCount = $this->customSkuMonthStockCountModel::query()
            ->leftJoin('self_custom_sku as scs', 'custom_sku_month_inventory_count.custom_sku_id', '=', 'scs.id')
            ->leftJoin('self_spu as ss', 'custom_sku_month_inventory_count.spu_id', '=', 'ss.id')
            ->select('custom_sku_month_inventory_count.*', 'scs.custom_sku', 'scs.old_custom_sku', 'ss.spu', 'ss.old_spu', 'ss.price');
        // 库存sku
        if (isset($params['custom_sku']) && !empty($params['custom_sku'])){
            $customSkuMonthStockCount->where(function ($query) use ($params){
                return $query->where('scs.custom_sku', 'like', '%'.$params['custom_sku'].'%')
                    ->orWhere('scs.old_custom_sku', 'like', '%'.$params['custom_sku'].'%');
            });
        }
        // spu
        if (isset($params['spu']) && !empty($params['spu'])){
            $customSkuMonthStockCount->where(function ($query) use ($params){
                return $query->where('ss.spu', 'like', '%'.$params['spu'].'%')
                    ->orWhere('ss.old_spu', 'like', '%'.$params['spu'].'%');
            });
        }
        // 年份
        if (isset($params['year']) && !empty($params['year'])){
            $customSkuMonthStockCount->where('custom_sku_month_inventory_count.year', $params['year']);
        }
        // 月份
        if (isset($params['month']) && !empty($params['month'])){
            $customSkuMonthStockCount->where('custom_sku_month_inventory_count.month', $params['month']);
        }
        // 仓库类型
        if (isset($params['warehouse_code']) && !empty($params['warehouse_code'])){
            $customSkuMonthStockCount->where('custom_sku_month_inventory_count.warehouse_code', $params['warehouse_code']);
        }
        // 数据量
        $count = $customSkuMonthStockCount->count();
        // 排序
        $customSkuMonthStockCount->orderBy('custom_sku_month_inventory_count.created_at', 'desc');
        // 分页
        if (isset($params['limit']) && !empty($params['limit']) && isset($params['page']) && !empty($params['page'])){
            $limit = $params['limit'] ?? 30;
            $page = $params['page'] ?? 1;
            $pageNum = $page <= 1 ? 0 : $limit*($page-1);
            $customSkuMonthStockCount->limit($limit)->offset($pageNum);
        }
        $customSkuMonthStockCount = $customSkuMonthStockCount->get()->toArray();
//        $spuId = array_column($customSkuMonthStockCount, 'spu_id');
//        $spuModel = $this->spuModel::query()
//            ->whereIn('id', $spuId)
//            ->get();
        $list = [];
        foreach ($customSkuMonthStockCount as &$item){
//            foreach ($spuModel as $spu){
//                if ($item['spu_id'] == $spu->id){
//                    $item['price'] = $spu->price ?? 0;
//                }
//            }
            $item['warehouse_name'] = self::WAREHOUSE_CODE_NAME[$item['warehouse_code']];
            $item['month'] = $item['month']."月";
            $item['year'] = $item['year']."年";
            $item['commission_ratio'] = $item['commission_ratio'] * 100 . "%";
            $item['price'] = round($item['price'] ?? 0, 2); // 单价
            $item['custom_sku'] = !empty($item['old_custom_sku']) ?  $item['old_custom_sku'] : $item['custom_sku']; // 库存SKU
            $item['spu'] = !empty($item['old_spu']) ?  $item['old_spu'] : $item['spu']; // SPU款号
            $item['property'] = round($item['property'] ?? 0, 2); // 资产
            $list[] = $item;
        }

        return ['count' => $count, 'list' => $list];
    }

    /**
     * @Desc:仓库月提成报表导出
     * @param $params
     * @return array
     * @author: Liu Sinian
     * @Time: 2023/3/8 16:41
     */
    public function exportCustomSkuMonthStockCountList($params)
    {
        $data = $this->getCustomSkuMonthStockCountList($params);
        $list = $data['list'];
        // excel文件表头
        $excelName = '仓库月提成报表导出'.time();
        // 表头
        $excelTitle = [
            "custom_sku" => "库存SKU",
            "spu" => "SPU",
            "year" => "年份",
            "month" => "月份",
            "warehouse_name" => "仓库名称",
            "warehouse_stock" => "2月库存数量",
            "saihe_warehouse_stock" => "赛盒当月首日现有库存",
            "in_warehouse_stock" => "当月入库数量",
            "out_warehouse_stock" => "当月出库数量",
            "price" => "单价",
            "commission_ratio" => "提成比例",
            "piece_count" => "计件数量",
            "commission" => "提成",
            "property" => "资产"
        ];
        // 组织导出列表数据
        $excelData = [
            'data' => $list,
            'title_list' => $excelTitle,
            'title' => $excelName,
            'type' => '仓库月提成报表导出',
            'user_id' => $params['export_user_id']
        ];

        return $excelData;
    }
}