<?php

namespace App\Models;

use App\Http\Controllers\Jobs\CloudHouseController;
use App\Http\Controllers\Pc\ApiresController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\DB;

class ApiresModel extends BaseModel
{
    public function param_check($params){
        $paramArr = array(
            'app_key' => ApiresController::$appKey,
            'customerId' => $params['customerId'],
            'format' => $params['format'],
            'method' => $params['method'],
            'sign_method' => 'md5',
            'timestamp' => $params['timestamp'],
            'v' => '2.0',
        );
        $sign = CloudHouseController::createSign($paramArr, ApiresController::$appSecret);
        return $sign;
    }

    public function xml_success(){
        $response = '<response>
                         <flag>success</flag>
                         <code>0</code>
                         <message>请求成功</message>
                    </response>';

        return str_replace(PHP_EOL, '', $response);
    }

    public function xml_data_success($data){
        $response = '<response>
                         <flag>success</flag>
                         <code>0</code>
                         <data>'.$data.'</data>
                         <message>请求成功</message>                       
                    </response>';

        return str_replace(PHP_EOL, '', $response);
    }

    public function xml_fail($params){
        if($params == 'none'){
            $message = '参数为空';
        }
        if($params == 'invalid'){
            $message = '非法参数';
        }
        if($params == 'data_error'){
            $message = 'xml参数错误';
        }
        if($params == 'data_none'){
            $message = '系统没有此数据';
        }
        if($params == 'data_repeat'){
            $message = '重复确认订单或无该订单';
        }
        if($params == 'task_error'){
            $message = '任务执行错误';
        }
        $response = '<response>
                         <flag>failure</flag>
                         <code>100</code>
                         <message>'.$message.'</message>
                    </response>';

        return str_replace(PHP_EOL, '', $response);
    }

    //查询历史下单数据
    public function GetHistoryOrder($params)
    {
        $page = isset($params['page']) ? $params['page'] : 1;
        $limit = isset($params['limit']) ? $params['limit'] : 30;

        $pagenNum=$page-1;
        if ($pagenNum != 0) {
            $pagenNum = $limit * $pagenNum;
        }

        $data = DB::table('amazon_place_order_detail as b')
            ->leftJoin('amazon_place_order_task as a','a.id','=','b.order_id')
            ->leftJoin('self_spu as c','b.spu_id','=','c.id')
            ->leftJoin('self_custom_sku as d','b.custom_sku_id','=','d.id');

        if(isset($params['spu'])){
            $spu = $params['spu'];
            $data = $data->where(function ($query) use ($spu){
                    $query->where('c.spu','like','%'.$spu.'%')->orWhere('c.old_spu','like','%'.$spu.'%');
                });
        }

        if(isset($params['custom_sku'])){
            $custom_sku = $params['custom_sku'];
            $data = $data->where(function ($query) use ($custom_sku){
                $query->where('d.custom_sku','like','%'.$custom_sku.'%')->orWhere('d.old_custom_sku','like','%'.$custom_sku.'%');
            });
        }

        if(isset($params['status'])){
            $data = $data->where('a.status',$params['status']);
        }

        if(isset($params['color'])){
            $data = $data->where('b.color_name','like','%'.$params['color'].'%');
        }

        if(isset($params['size'])){
            $data = $data->where('b.size',$params['color']);
        }

        if(isset($params['platform_id'])){
            $data = $data->where('a.platform_id',$params['platform_id']);
        }

        if(isset($params['start_time'])){
            $data = $data->where('a.creattime','>',$params['start_time']);
        }

        if(isset($params['end_time'])){
            $data = $data->where('a.creattime','<',$params['end_time']);
        }

//        DB::connection()->enableQueryLog();
        $totalNum = $data->count();
        $data = $data
            ->offset($pagenNum)
            ->limit($limit)
            ->select('a.id as order_id','a.platform_id','a.self_type','b.color_name','b.size','c.spu','c.old_spu','d.custom_sku','d.old_custom_sku',
                'b.order_num','a.creattime','a.status','a.shop_id','a.user_id','a.supply_user','b.custom_sku_id','b.sku_id','d.tongan_inventory',
                'd.quanzhou_inventory','d.deposit_inventory','d.direct_inventory')
            ->get()
            ->toArray();

//        var_dump(DB::getQueryLog());


        $result = array();

        if(!empty($data)){
            $skuArr = array_column($data,'sku_id');
            $customSkuArr = array_column($data,'custom_sku_id');

            //获取月销量
            $date = date('Y-m-d 00:00:00');
            $lastWeek = date('Y-m-d 00:00:00',strtotime('-7 day'));
            $lastMonth = date('Y-m-d 00:00:00',strtotime('-30 day'));
            $amazon_order_item = DB::table('amazon_order_item')
                ->whereIn('sku_id',$skuArr)
                ->where('amazon_time','>',$lastMonth)
                ->where('amazon_time','<',$date)
                ->where('order_status','!=','Canceled')
                ->select('quantity_ordered','sku_id','shop_id','amazon_time')
                ->get()
                ->toArray();

            $weekOrder = [];
            $monthOrder = [];
            if(!empty($amazon_order_item)){
                foreach ($amazon_order_item as $od){
                    if($od->amazon_time>$lastWeek){
                        if(isset($weekOrder[$od->sku_id])){
                            $weekOrder[$od->sku_id] += $od->quantity_ordered;
                        }else{
                            $weekOrder[$od->sku_id] = $od->quantity_ordered;
                        }
                    }
                    if(isset($monthOrder[$od->sku_id])){
                        $monthOrder[$od->sku_id] += $od->quantity_ordered;
                    }else{
                        $monthOrder[$od->sku_id] = $od->quantity_ordered;
                    }
                }
            }

            //获取fba库存
            $product_detail = DB::table('product_detail')->whereIn('sku_id',$skuArr)->select('now_num','sku_id')->get()->toArray();
            $skuFba = [];
            if(!empty($product_detail)){
                foreach ($product_detail as $p){
                    $skuFba[$p->sku_id] = $p->now_num;
                }
            }


            foreach ($data as $k=>$v){
                $result[$k]['order_id'] = $v->order_id;
                if($v->self_type==1){
                    $result[$k]['self_type'] ='线下生产';
                }
                if($v->self_type==2){
                    $result[$k]['self_type'] ='线上采购';
                }
                $result[$k]['inventory']  = $v->tongan_inventory+$v->quanzhou_inventory+$v->deposit_inventory+$v->direct_inventory;
                $result[$k]['monthOrder'] = 0;
                $result[$k]['weekOrder'] = 0;
                $result[$k]['color_name'] = $v->color_name;
                $result[$k]['size'] = $v->size;
                $result[$k]['spu'] = $v->spu;
                $result[$k]['old_spu'] = $v->old_spu;
                $result[$k]['custom_sku'] = $v->custom_sku;
                $result[$k]['old_custom_sku'] = $v->old_custom_sku;

                $orderNum = json_decode($v->order_num,true);
                $result[$k]['order_num'] = array_sum($orderNum);

                $result[$k]['creattime'] = $v->creattime;
                $result[$k]['platform'] = $this->GetPlatform($v->platform_id)?$this->GetPlatform($v->platform_id)['name']:'';
                $result[$k]['platform_id'] = $v->platform_id;
                $result[$k]['status'] = $v->status;
                $result[$k]['shop'] = $this->GetShop($v->shop_id)?$this->GetShop($v->shop_id)['shop_name']:'';
                $result[$k]['user'] = $this->GetUsers($v->user_id)?$this->GetUsers($v->user_id)['account']:'';
                $supply_user = explode(',',$v->supply_user);
                if(!empty($supply_user)){
                    $supplyArr = [];
                    foreach ($supply_user as $s){
                        $getuser = $this->GetUsers($s);
                        if($getuser){
                            $supplyArr[] = $getuser['account'];
                        }
                    }
                    $result[$k]['supply_user'] = implode(',',$supplyArr);
                }else{
                    $result[$k]['supply_user'] = '';
                }

                $result[$k]['contract_no'] = array();
                $cloudhouse_custom_sku = DB::table('cloudhouse_custom_sku')
                    ->where('order_id',$v->order_id)
                    ->where('custom_sku_id',$v->custom_sku_id)
                    ->select('contract_no')->get()->toArray();
                if(!empty($cloudhouse_custom_sku)){
                    $result[$k]['contract_no'] = array_column($cloudhouse_custom_sku,'contract_no');
                }

                if(isset($skuFba[$v->sku_id])){
                    $result[$k]['inventory'] += $skuFba[$v->sku_id];
                }

                if(isset($weekOrder[$v->sku_id])){
                    $result[$k]['weekOrder'] += $weekOrder[$v->sku_id];
                }

                if(isset($monthOrder[$v->sku_id])){
                    $result[$k]['monthOrder'] += $monthOrder[$v->sku_id];
                }

                if($result[$k]['monthOrder']==0){
                    $result[$k]['brokenCode'] = $v->creattime;
                }elseif($result[$k]['inventory']==0){
                    $result[$k]['brokenCode'] = $v->creattime;
                }else{
                    $turnoverDays = ceil($result[$k]['inventory']/($result[$k]['monthOrder']/30));
                    $result[$k]['brokenCode'] = date('Y-m-d 00:00:00',strtotime('+'.$turnoverDays.'day'));
                }


            }
        }

        return ['data'=>$result,'totalNum'=>$totalNum];

    }


}
