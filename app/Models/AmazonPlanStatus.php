<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AmazonPlanStatus extends Model
{
    //
    protected $table = 'amazon_plan_status';
    protected $guarded = [];
    public $timestamps = false;
}
