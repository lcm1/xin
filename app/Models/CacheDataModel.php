<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use App\Http\Controllers\Jobs\ExcelTaskController;


//数据缓存
class CacheDataModel extends BaseModel
{

    private $type_detail_arr = [1=>'同安仓',2=>'泉州仓',3=>'云仓',4=>'工厂虚拟仓',6=>'工厂寄存仓',22=>'工厂直发仓',23=>'深圳寄存仓'];


    
    public function add_array($array_1, $array_2) {
        $add = ['all_inventory','out_num','in_num','out_price','in_price','order_all','order_all_out','order_all_in','order_wait','order_wait_out','order_wait_in','order','order_out','order_in','total_inventory_price'];
        $return = [];

        foreach ($array_1 as $k => $v) {

            if(in_array($k,$add)){
                $return[$k] = round($v+$array_2[$k],2);
            }

            if($k=='order_time'||$k=='order_rate'){
                $return[$k] = round(($v+$array_2[$k])/2,2);
            }
            // # code...
            // if($k=='out_in_time_data'||$k=='out_in_num_data'){
            // //出库数及入库数趋势图
            //     foreach ($v['next_day'] as $nk => $nv) {
            //         # code...
            //         $one['time'] = $nv['time'];
            //         $one['out'] = round($nv['out'] + $array_2[$k]['next_day'][$nk]['out'],2);
            //         $one['in'] = round($nv['in']+ $array_2[$k]['next_day'][$nk]['in'],2);
            //         $return[$k]['next_day'][$nk] = $one;
            //     }

            //     foreach ($v['next_moon'] as $nk => $nv) {
            //         # code...
            //         $one['time'] = $nv['time'];
            //         $one['out'] = round($nv['out'] + $array_2[$k]['next_moon'][$nk]['out'],2);
            //         $one['in'] = round($nv['in']+ $array_2[$k]['next_moon'][$nk]['in'],2);
            //         $return[$k]['next_moon'][$nk] = $one;
            //     }

            //     foreach ($v['next_year'] as $nk => $nv) {
            //         # code...
            //         $one['time'] = $nv['time'];
            //         $one['out'] = round($nv['out'] + $array_2[$k]['next_year'][$nk]['out'],2);
            //         $one['in'] = round($nv['in']+ $array_2[$k]['next_year'][$nk]['in'],2);
            //         $return[$k]['next_year'][$nk] = $one;
            //     }
                
            // }


            // if($k=='order_type'){
            //     //订单类型占比
            //     $w_order_type = [];
            //     if(isset($v['week'])){
            //         foreach ($v['week']['data'] as $wv) {
            //             $wk = $wv['type_name'];
            //             $w_order_type[$wk]=$wv['num'];
            //         }

            //         foreach ($array_2[$k]['week']['data'] as $wv_2) {
            //             # code...
            //             $wk2 = $wv_2['type_name'];
            //             if(isset($w_order_type[$wk2])){
            //                 $w_order_type[$wk2]+=$wv_2['num'];
            //             }else{
            //                 $w_order_type[$wk2]=$wv_2['num'];
            //             }
            //         }

            //         foreach ($w_order_type as $ok => $ov) {
            //             # code...
            //             $w_one['type_name'] = $ok;
            //             $w_one['num'] = $ov;
            //             $return[$k]['week']['data'][] =  $w_one;
            //         }
            //     }

            //     //
            //     $m_order_type = [];
            //     if(isset($v['moon'])){
            //         foreach ($v['moon']['data'] as $mov) {
            //             $mok = $mov['type_name'];
            //             $m_order_type[$mok]=$mov['num'];
            //         }

            //         foreach ($array_2[$k]['moon']['data'] as $mov_2) {
            //             # code...
            //             $mok2 = $mov_2['type_name'];
            //             if(isset($m_order_type[$mok2])){
            //                 $m_order_type[$mok2]+=$mov_2['num'];
            //             }else{
            //                 $m_order_type[$mok2]=$mov_2['num'];
            //             }
            //         }

            //         foreach ($m_order_type as $mok => $mov) {
            //             # code...
            //             $m_one['type_name'] = $mok;
            //             $m_one['num'] = $mov;
            //             $return[$k]['moon']['data'][] =  $m_one;
            //         }
            //     }

            //     $y_order_type = [];
            //     if(isset($v['year'])){

          
            //         foreach ($v['year']['data'] as $yv) {
            //             $yk = $yv['type_name'];
            //             $y_order_type[$yk]=$yv['num'];
            //         }

            //         foreach ($array_2[$k]['year']['data'] as $yv_2) {
            //             # code...
            //             $yk2 = $yv_2['type_name'];
            //             if(isset($y_order_type[$yk2])){
            //                 $y_order_type[$yk2]+=$yv_2['num'];
            //             }else{
            //                 $y_order_type[$yk2]=$yv_2['num'];
            //             }
            //         }

            //         foreach ($y_order_type as $yk => $yov) {
            //             # code...
            //             $y_one['type_name'] = $yk;
            //             $y_one['num'] = $yov;
            //             $return[$k]['year']['data'][] =  $y_one;
            //         }
            //     }
            // }



            
            // if($k=='platform_outNum'){
            //     //平台出库数量
            //     $w_p_type = [];
            //     if(isset($v['week'])){
            //         foreach ($v['week'] as $wpv) {
            //             $wpk = $wpv['platform'];
            //             $w_p_type[$wpk]=$wpv['num'];
            //         }
    
            //         foreach ($array_2[$k]['week'] as $wpv_2) {
            //             # code...
            //             $wpk2 = $wpv_2['platform'];
            //             if(isset($w_p_type[$wpk2])){
            //                 $w_p_type[$wpk2]+=$wpv_2['num'];
            //             }else{
            //                 $w_p_type[$wpk2]=$wpv_2['num'];
            //             }
            //         }
    
            //         foreach ($w_p_type as $wpok => $wpov) {
            //             # code...
            //             $wp_one['platform'] = $wpok;
            //             $wp_one['num'] = $wpov;
            //             $return[$k]['week'][] =  $wp_one;
            //         }
            //     }


            //     //
            //     $m_p_type = [];
            //     if(isset($v['moon'])){
            //         foreach ($v['moon'] as $mpov) {
            //             $mpok = $mpov['platform'];
            //             $m_p_type[$mpok]=$mpov['num'];
            //         }

            //         foreach ($array_2[$k]['moon'] as $mpov_2) {
            //             # code...
            //             $mpok2 = $mpov_2['platform'];
            //             if(isset($m_p_type[$mpok2])){
            //                 $m_p_type[$mpok2]+=$mpov_2['num'];
            //             }else{
            //                 $m_p_type[$mpok2]=$mpov_2['num'];
            //             }
            //         }

            //         foreach ($m_p_type as $mpk => $mpv) {
            //             # code...
            //             $mp_one['platform'] = $mpk;
            //             $mp_one['num'] = $mpv;
            //             $return[$k]['moon'][] =  $mp_one;
            //         }
            //     }

            //     $y_p_type = [];
            //     if(isset($v['year'])){
            //         foreach ($v['year'] as $ypv) {
            //             $ypk = $ypv['platform'];
            //             $y_p_type[$ypk]=$ypv['num'];
            //         }

            //         foreach ($array_2[$k]['year'] as $ypv_2) {
            //             # code...
            //             $ypk2 = $ypv_2['platform'];
            //             if(isset($y_p_type[$ypk2])){
            //                 $y_p_type[$ypk2]+=$ypv_2['num'];
            //             }else{
            //                 $y_p_type[$ypk2]=$ypv_2['num'];
            //             }
            //         }

            //         foreach ($y_p_type as $ypok => $ypov) {
            //             # code...
            //             $yp_one['platform'] = $ypok;
            //             $yp_one['num'] = $ypov;
            //             $return[$k]['year'][] =  $yp_one;
            //         }
            //     }
            // }
            

            // if($k=='order_list'){
            //     $order_list = array_merge($v,$array_2[$k]);
            //     $list_num = array_column($order_list,'createtime');
            //     array_multisort($list_num,SORT_DESC,$order_list);

            //     $return[$k] =  array_slice($order_list,0,10);
            // }

            
            // if($k=='spu_rank'){
            //     $spu_list = array_merge($v,$array_2[$k]);
            //     $spu_num = array_column($spu_list,'num');
            //     array_multisort($spu_num,SORT_DESC,$spu_list);
            //     $return[$k] =  array_slice($spu_list,0,10);
            // }
        }
        
        return $return;
        // "data": {

        //     "order_list": [
        //         {
        //             "platform_id": 5,
        //             "type_detail": 11,
        //             "is_push": 3,
        //             "createtime": "2023-10-10 09:32:03",
        //             "total_num": 1,
        //             "platform": "AMAZON",
        //             "type_detail_name": "手工出库",
        //             "status_name": "已确认"
        //         },
        //         {
        //             "platform_id": 5,
        //             "type_detail": 11,
        //             "is_push": 3,
        //             "createtime": "2023-10-10 09:34:13",
        //             "total_num": 1,
        //             "platform": "AMAZON",
        //             "type_detail_name": "手工出库",
        //             "status_name": "已确认"
        //         },
        //         {
        //             "platform_id": 5,
        //             "type_detail": 11,
        //             "is_push": 3,
        //             "createtime": "2023-10-10 09:44:16",
        //             "total_num": 27,
        //             "platform": "AMAZON",
        //             "type_detail_name": "手工出库",
        //             "status_name": "已确认"
        //         },
        //         {
        //             "platform_id": 5,
        //             "type_detail": 3,
        //             "is_push": 3,
        //             "createtime": "2023-10-10 09:54:07",
        //             "total_num": 54,
        //             "platform": "AMAZON",
        //             "type_detail_name": "手工入库",
        //             "status_name": "已确认"
        //         },
        //         {
        //             "platform_id": 14,
        //             "type_detail": 12,
        //             "is_push": 4,
        //             "createtime": "2023-10-10 10:10:06",
        //             "total_num": 1,
        //             "platform": "沃尔玛",
        //             "type_detail_name": "订单出库",
        //             "status_name": "操作中"
        //         },
        //         {
        //             "platform_id": 14,
        //             "type_detail": 12,
        //             "is_push": 4,
        //             "createtime": "2023-10-10 10:12:01",
        //             "total_num": 2,
        //             "platform": "沃尔玛",
        //             "type_detail_name": "订单出库",
        //             "status_name": "操作中"
        //         },
        //         {
        //             "platform_id": 14,
        //             "type_detail": 12,
        //             "is_push": 4,
        //             "createtime": "2023-10-10 10:13:04",
        //             "total_num": 1,
        //             "platform": "沃尔玛",
        //             "type_detail_name": "订单出库",
        //             "status_name": "操作中"
        //         },
        //         {
        //             "platform_id": 15,
        //             "type_detail": 10,
        //             "is_push": -1,
        //             "createtime": "2023-10-10 10:25:24",
        //             "total_num": 1,
        //             "platform": "PS-2部",
        //             "type_detail_name": "样品调拨出库",
        //             "status_name": "已取消"
        //         },
        //         {
        //             "platform_id": 5,
        //             "type_detail": 6,
        //             "is_push": -1,
        //             "createtime": "2023-10-10 11:08:35",
        //             "total_num": 9,
        //             "platform": "AMAZON",
        //             "type_detail_name": "fba出库",
        //             "status_name": "已取消"
        //         },
        //         {
        //             "platform_id": 14,
        //             "type_detail": 12,
        //             "is_push": 2,
        //             "createtime": "2023-10-10 11:50:37",
        //             "total_num": 1,
        //             "platform": "沃尔玛",
        //             "type_detail_name": "订单出库",
        //             "status_name": "已推送"
        //         }
        //     ],
        //     "spu_rank": [
        //         {
        //             "num": "825",
        //             "spu": "AZUHE00035"
        //         },
        //         {
        //             "num": "120",
        //             "spu": "AUMPO00034"
        //         },
        //         {
        //             "num": "54",
        //             "spu": "APOH700012"
        //         },
        //         {
        //             "num": "50",
        //             "spu": "AUMPO00029"
        //         },
        //         {
        //             "num": "48",
        //             "spu": "CUWFH01413"
        //         },
        //         {
        //             "num": "25",
        //             "spu": "AUMPO00027"
        //         },
        //         {
        //             "num": "9",
        //             "spu": "AUMPO00071"
        //         },
        //         {
        //             "num": "5",
        //             "spu": "AUMPO00045"
        //         },
        //         {
        //             "num": "2",
        //             "spu": "CFMTX00017"
        //         },
        //         {
        //             "num": "2",
        //             "spu": "AUMST00016"
        //         }
        //     ],

        // },
        
    }


    //获取库存缓存
    public function GetCacheInventoryMb($params){
        $is_date = false;
        if(isset($params['ds'])){
            $is_date = true;
        }
        $ds = $params['ds']??date('Y-m-d',time());
        $yes_ds = date('Y-m-d',strtotime($ds)-86400);
        $warehouse_id = $params['warehouse_id']??[1,2,3,4,6,22,23];

        $start_time = date('Y-m-d 00:00:00',strtotime($ds));
        $end_time = date('Y-m-d 23:59:59',strtotime($ds));

        //本周
        $first = 1;//第一天为周一则为1，第一天为周末为0
        $w = date("w",strtotime($ds)) ? date("w",strtotime($ds)) - $first : 6;
        //获取本周开始日期，如果$w是0，则表示周日，减去 6 天
        $week_start = date('Y-m-d 00:00:00', strtotime("$ds-$w"."days"));
        //本周结束日期
        $week_end = date('Y-m-d 23:59:59', strtotime("$week_start +6 days"));

        
        $query_week['warehouse_id'] = $warehouse_id;
        $query_week['start_time'] = $week_start;
        $query_week['end_time'] = $week_end;


        //本月
        $moon_start = date('Y-m'.'-01 00:00:00',strtotime($ds));
        // $moon_end = date('Y-m-d H:i:s',time());
        $moon_end = date("Y-m-d 23:59:59",strtotime("$moon_start +1 month -1 day"));

        $query_moon['warehouse_id'] = $warehouse_id;
        $query_moon['start_time'] = $moon_start;
        $query_moon['end_time'] = $moon_end;

        //本年
        $year_start = date('Y'.'-01-01 00:00:00',strtotime($ds));
        $year_end = date('Y'.'-12-31 23:59:59',strtotime($ds));

        $query_year['warehouse_id'] = $warehouse_id;
        $query_year['start_time'] = $year_start;
        $query_year['end_time'] = $year_end;




        $query['warehouse_id'] = $warehouse_id;
        $query['start_time'] = $start_time;
        $query['end_time'] = $end_time;
       

        //最近七天
        $next_day = [];
        $next_day_1['start_time'] =  date('Y-m-d 00:00:00',strtotime($ds)-(86400*7));
        $next_day_1['end_time'] =  date('Y-m-d 23:59:59',strtotime($ds)-(86400*7));

        $next_day_2['start_time'] =  date('Y-m-d 00:00:00',strtotime($ds)-(86400*1));
        $next_day_2['end_time'] =  date('Y-m-d 23:59:59',strtotime($ds)-(86400*1));

        $next_day_3['start_time'] =  date('Y-m-d 00:00:00',strtotime($ds)-(86400*2));
        $next_day_3['end_time'] =  date('Y-m-d 23:59:59',strtotime($ds)-(86400*2));

        $next_day_4['start_time'] =  date('Y-m-d 00:00:00',strtotime($ds)-(86400*3));
        $next_day_4['end_time'] =  date('Y-m-d 23:59:59',strtotime($ds)-(86400*3));

        $next_day_5['start_time'] =  date('Y-m-d 00:00:00',strtotime($ds)-(86400*4));
        $next_day_5['end_time'] =  date('Y-m-d 23:59:59',strtotime($ds)-(86400*4));

        $next_day_6['start_time'] =  date('Y-m-d 00:00:00',strtotime($ds)-(86400*5));
        $next_day_6['end_time'] =  date('Y-m-d 23:59:59',strtotime($ds)-(86400*5));

        $next_day_7['start_time'] =  date('Y-m-d 00:00:00',strtotime($ds)-(86400*6));
        $next_day_7['end_time'] =  date('Y-m-d 23:59:59',strtotime($ds)-(86400*6));

        $next_day[0] = $next_day_1;
        $next_day[1] = $next_day_7;
        $next_day[2] = $next_day_6;
        $next_day[3] = $next_day_5;
        $next_day[4] = $next_day_4;
        $next_day[5] = $next_day_3;
        $next_day[6] = $next_day_2;


        //最近一月
        $next_moon = [];
        //最近7
        $next_moon_1['start_time'] = date('Y-m-d 00:00:00',strtotime($ds)-(86400*6));
        $next_moon_1['end_time'] = date('Y-m-d 23:59:59',strtotime($ds));
        //最近14
        $next_moon_2['start_time'] = date('Y-m-d 00:00:00',strtotime($ds)-(86400*13));
        $next_moon_2['end_time'] = date('Y-m-d 23:59:59',strtotime($ds)-(86400*7));
        //最近21
        $next_moon_3['start_time'] = date('Y-m-d 00:00:00',strtotime($ds)-(86400*20));
        $next_moon_3['end_time'] = date('Y-m-d 23:59:59',strtotime($ds)-(86400*14));
        //最近28
        $next_moon_4['start_time'] = date('Y-m-d 00:00:00',strtotime($ds)-(86400*27));
        $next_moon_4['end_time'] = date('Y-m-d 23:59:59',strtotime($ds)-(86400*21));
        $next_moon[] =  $next_moon_4;
        $next_moon[] =  $next_moon_3;
        $next_moon[] =  $next_moon_2;
        $next_moon[] =  $next_moon_1;


        //最近一年
        $next_year = [];
        $year = date('Y',strtotime($ds));
        $moon = date('m',strtotime($ds));
        for ($i=1; $i < $moon+1; $i++) { 
            # code...
            $m = str_pad($i,2,"0",STR_PAD_LEFT);
            $s =  $year.'-'.$m.'-01 00:00:00';
            $one_year['start_time'] = $s;
            $one_year['end_time'] = date("Y-m-d 23:59:59",strtotime("$s +1 month -1 day"));
            $next_year[] =  $one_year;
        }








        $list = db::table('cache_inventory')->where('ds','like','%'.$ds.'%')->whereIn('warehouse_id',$warehouse_id)->get();

        $retun_data = [];

        $today_data = [];

        if($list){
            foreach ($list as $k=>$v) {
                # code...
                $retun_data['update_time'] = $v->update_time;
                $retun_data['ds'] = $v->ds;
                $data = json_decode($v->data,true);
                if($k==0){
                    $today_data = $data;
                }else{
                    $today_data = $this->add_array($today_data,$data);
                }

            }
        }



        
        //出库时效及入库时效-周 月 年
        $today_data['out_in_time_data'] =[];
        $w_i = 1;
        foreach ($next_day as $nd) {
            # code...
            $q['warehouse_id'] = $warehouse_id;
            $q['start_time'] = $nd['start_time'];
            $q['end_time'] = $nd['end_time'];
            $d = $this->GetOrderTimes($q);
            $o['date'] = '星期'.$w_i;
            $w_i++;
            $o['time'] = $this->TimeToDay($nd['start_time']);
            $o['out'] = $d['out_ave_time'];
            $o['in'] = $d['in_ave_time'];
            $today_data['out_in_time_data']['next_day'][] = $o;
        }

        $m_i = 1;
        foreach ($next_moon as $nd) {
            # code...
            $q['warehouse_id'] = $warehouse_id;
            $q['start_time'] = $nd['start_time'];
            $q['end_time'] = $nd['end_time'];
            $d = $this->GetOrderTimes($q);
            $o['date'] = '第'.$m_i.'周';
            $m_i++;
            $o['time'] = $this->TimeToDay($nd['start_time']).'-'.$this->TimeToDay($nd['end_time']);
            $o['out'] = $d['out_ave_time'];
            $o['in'] = $d['in_ave_time'];
            $today_data['out_in_time_data']['next_moon'][] = $o;
        }

        $y_i = 1;
        foreach ($next_year as $nd) {
            # code...
            $q['warehouse_id'] = $warehouse_id;
            $q['start_time'] = $nd['start_time'];
            $q['end_time'] = $nd['end_time'];
            $d = $this->GetOrderTimes($q);
            $o['date'] = '第'.$y_i.'月';
            $y_i++;
            $o['time'] = $nd['start_time'].'-'.$nd['end_time'];
            $o['out'] = $d['out_ave_time'];
            $o['in'] = $d['in_ave_time'];
            $today_data['out_in_time_data']['next_year'][] = $o;
        }



        $w_a_i = 1;
        // 出库数及入库数-周 月 年
        $today_data['out_in_num_data'] = [];
        foreach ($next_day as $nd) {
            # code...
            $q['warehouse_id'] = $warehouse_id;
            $q['start_time'] = $nd['start_time'];
            $q['end_time'] = $nd['end_time'];
            $d = $this->GetInventoryNums($q);
            $o['date'] = '星期'.$w_a_i;
            $w_a_i++;
            $o['time'] = $this->TimeToDay($nd['start_time']);
            $o['out'] = $d['out'];
            $o['in'] = $d['in'];
            $today_data['out_in_num_data']['next_day'][] = $o;
        }

        $m_a_i = 1;
        foreach ($next_moon as $nd) {
            # code...
            $q['warehouse_id'] = $warehouse_id;
            $q['start_time'] = $nd['start_time'];
            $q['end_time'] = $nd['end_time'];
            $d = $this->GetInventoryNums($q);
            $o['date'] = '第'.$m_a_i.'周';
            $m_a_i++;
            $o['time'] = $this->TimeToDay($nd['start_time']).'-'.$this->TimeToDay($nd['end_time']);
            $o['out'] = $d['out'];
            $o['in'] = $d['in'];
            $today_data['out_in_num_data']['next_moon'][] = $o;
        }

        $y_a_i = 1;
        foreach ($next_year as $nd) {
            # code...
            $q['warehouse_id'] = $warehouse_id;
            $q['start_time'] = $nd['start_time'];
            $q['end_time'] = $nd['end_time'];
            $d = $this->GetInventoryNums($q);
            $o['date'] = '第'.$y_a_i.'月';
            $y_a_i++;
            $o['time'] = $nd['start_time'].'-'.$nd['end_time'];
            $o['out'] = $d['out'];
            $o['in'] = $d['in'];
            $today_data['out_in_num_data']['next_year'][] = $o;
        }



        //订单类型占比-周 月 年

        $today_data['order_type']['week']['data'] = $this->GetInventoryOrderTypes($query_week);
        $today_data['order_type']['moon']['data'] = $this->GetInventoryOrderTypes($query_moon);
        $today_data['order_type']['year']['data'] = $this->GetInventoryOrderTypes($query_year);





        //平台出库数量占比-周 月 年
        $platform_Num_week = $this->PlatformOutNums($query_week);
        $platform_Num_moon = $this->PlatformOutNums($query_moon);
        $platform_Num_year = $this->PlatformOutNums($query_year);
        $today_data['platform_outNum']['week']=  $platform_Num_week['out'];
        $today_data['platform_outNum']['moon']= $platform_Num_moon['out'];
        $today_data['platform_outNum']['year']=  $platform_Num_year['out'];


        $today_data['platform_inNum']['week']= $platform_Num_week['in'];
        $today_data['platform_inNum']['moon']= $platform_Num_moon['in'];
        $today_data['platform_inNum']['year']= $platform_Num_year['in'];




        //今日订单
        $today_data['order_list'] = $this->OrderLists($query);

        
        // //本月spu出库排名
        // $today_data['spu_rank'] =$this->SpuRanks($query_moon);




        
        $list_ys = db::table('cache_inventory')->where('ds','like','%'.$yes_ds.'%')->whereIn('warehouse_id',$warehouse_id)->get();
        $yedata = [];


        if($list_ys){
            foreach ($list_ys as $yk=>$yv) {
                # code...
                $ydata = json_decode($yv->data,true);
                if($yk==0){
                    $yedata = $ydata;
                }else{
                    $yedata = $this->add_array($yedata,$ydata);
                }

            }            // $yedata = json_decode($list_ys->data,true);
        }

        // foreach ($list as $v) {
        //     # code...
        //     $v->data = json_decode($v->data);
        // }




        //昨天总库存
        $today_data['all_inventory_ys'] = $yedata['all_inventory']??0;
        //环比昨日 = 今日-昨日/今日
        if($today_data['all_inventory']>0){
            $today_data['all_inventory_rate'] = round((($today_data['all_inventory']- $today_data['all_inventory_ys'])/$today_data['all_inventory'])*100,2)??0;
        }else{
            $today_data['all_inventory_rate'] = 0;
        }
        //昨日出库
        $today_data['out_num_ys'] = $yedata['out_num']??0;
        if($today_data['out_num']>0){
            $today_data['out_num_rate'] = round((($today_data['out_num']- $today_data['out_num_ys'])/$today_data['out_num'])*100,2)??0;
        }else{
            $today_data['out_num_rate'] = 0;
        } 
        //昨日入库
        $today_data['in_num_ys'] = $yedata['in_num']??0;
        if($today_data['in_num']>0){
            $today_data['in_num_rate'] = round((($today_data['in_num']- $today_data['in_num_ys'])/$today_data['in_num'])*100,2)??0;
        }else{
            $today_data['in_num_rate'] = 0;
        } 
        //出库资产
        $today_data['out_price_ys'] = $yedata['out_price']??0;
        if($today_data['out_price']>0){
            $today_data['out_price_rate'] = round((($today_data['out_price']- $today_data['out_price_ys'])/$today_data['out_price'])*100,2)??0;
        }else{
            $today_data['out_price_rate'] = 0;
        } 
        //入库资产
        $today_data['in_price_ys'] = $yedata['in_price']??0;
        if($today_data['in_price']>0){
            $today_data['in_price_rate'] = round((($today_data['in_price']- $today_data['in_price_ys'])/$today_data['in_price'])*100,2)??0;
        }else{
            $today_data['in_price_rate'] = 0;
        } 
        //仓库总资产
        $today_data['total_inventory_price_ys'] = $yedata['total_inventory_price']??0;
        if($today_data['total_inventory_price']>0){
            $today_data['total_inventory_rate'] = round((($today_data['total_inventory_price']- $today_data['total_inventory_price_ys'])/$today_data['total_inventory_price'])*100,2)??0;
        }else{
            $today_data['total_inventory_rate'] = 0;
        } 
        //完成时效
        $today_data['order_time_ys'] = $yedata['order_time']??0;
        if($today_data['order_time']>0){
            $today_data['order_time_rate'] = round((($today_data['order_time']- $today_data['order_time_ys'])/$today_data['order_time'])*100,2)??0;
        }else{
            $today_data['order_time_rate'] = 0;
        } 

        //今日订单完成率
        $today_data['order_rate_ys'] = $yedata['order_rate']??0;
        if($today_data['order_rate']>0){
            $today_data['order_rate_rate'] = round((($today_data['order_rate']- $today_data['order_rate_ys'])/$today_data['order_rate'])*100,2)??0;
        }else{
            $today_data['order_rate_rate'] = 0;
        } 

        //如果有日期，出入库订单查日期
        if($is_date){
            $id_InventoryOrder = $this->GetInventoryOrderb($query);

            // var_dump($id_InventoryOrder);
            //总订单
            $today_data['order_all']  = $id_InventoryOrder['order_all'];
            $today_data['order_all_out']  = $id_InventoryOrder['order_all_out'];
            $today_data['order_all_in']  = $id_InventoryOrder['order_all_in'];
            //未完成订单
            $today_data['order_wait_out']  = $id_InventoryOrder['order_wait_out'];
            $today_data['order_wait_in']  = $id_InventoryOrder['order_wait_in'];
            $today_data['order_wait']  = $id_InventoryOrder['order_wait'];
            //已完成订单
            $today_data['order']  = $id_InventoryOrder['order'];
            $today_data['order_out']  = $id_InventoryOrder['order_out'];
            $today_data['order_in']  = $id_InventoryOrder['order_in'];


        }

        $retun_data['data'] = $today_data;
        return $this->SBack($retun_data);        
    }


    //获取库存缓存
    public function GetCacheInventoryM($params){
        $ds = $params['ds']??date('Y-m-d',time());
        $yes_ds = date('Y-m-d',strtotime($ds)-86400);
        $warehouse_id = $params['warehouse_id']??[1,2,3,4,6,22,23];

        $list = db::table('cache_inventory')->where('ds','like','%'.$ds.'%')->whereIn('warehouse_id',$warehouse_id)->first();
        $list->data = json_decode($list->data,true);

        
        $list_ys = db::table('cache_inventory')->where('ds','like','%'.$yes_ds.'%')->whereIn('warehouse_id',$warehouse_id)->first();
        $yedata = [];
        if($list_ys){
            $yedata = json_decode($list_ys->data,true);
        }

        // foreach ($list as $v) {
        //     # code...
        //     $v->data = json_decode($v->data);
        // }

        //昨天总库存
        $list->data['all_inventory_ys'] = $yedata['all_inventory']??0;
        //环比昨日 = 今日-昨日/今日
        if($list->data['all_inventory']>0){
            $list->data['all_inventory_rate'] = round((($list->data['all_inventory']- $list->data['all_inventory_ys'])/$list->data['all_inventory'])*100,2)??0;
        }else{
            $list->data['all_inventory_rate'] = 0;
        }
       
        //昨日出库
        $list->data['out_num_ys'] = $yedata['out_num']??0;
        if($list->data['out_num']>0){
            $list->data['out_num_rate'] = round((($list->data['out_num']- $list->data['out_num_ys'])/$list->data['out_num'])*100,2)??0;
        }else{
            $list->data['out_num_rate'] = 0;
        }
        //昨日入库
        $list->data['in_num_ys'] = $yedata['in_num']??0;
        if($list->data['in_num']>0){
            $list->data['in_num_rate'] = round((($list->data['in_num']- $list->data['in_num_ys'])/$list->data['in_num'])*100,2)??0;
        }else{
            $list->data['in_num_rate'] = 0;
        }
        //出库资产
        $list->data['out_price_ys'] = $yedata['out_price']??0;
        if($list->data['out_price']>0){
            $list->data['out_price_rate'] = round((($list->data['out_price']- $list->data['out_price_ys'])/$list->data['out_price'])*100,2)??0;
        }else{
            $list->data['out_price_rate'] = 0;
        }
        //入库资产
        $list->data['in_price_ys'] = $yedata['in_price']??0;
        if($list->data['in_price']>0){
            $list->data['in_price_rate'] = round((($list->data['in_price']- $list->data['in_price_ys'])/$list->data['in_price'])*100,2)??0;
        }else{
            $list->data['in_price_rate'] = 0;
        }
        //仓库总资产
        $list->data['total_inventory_price_ys'] = $yedata['total_inventory_price']??0;
        if($list->data['total_inventory_price']>0){
            $list->data['total_inventory_rate'] = round((($list->data['total_inventory_price']- $list->data['total_inventory_price_ys'])/$list->data['total_inventory_price'])*100,2)??0;
        }else{
            $list->data['total_inventory_rate'] = 0;
        }
        //完成时效
        $list->data['order_time_ys'] = $yedata['order_time']??0;
        if($list->data['order_time']>0){
            $list->data['order_time_rate'] = round((($list->data['order_time']- $list->data['order_time_ys'])/$list->data['order_time'])*100,2)??0;
        }else{
            $list->data['order_time_rate'] = 0;
        }
        //今日订单完成率
        $list->data['order_rate_ys'] = $yedata['order_rate']??0;

        if($list->data['order_rate']>0){
            $list->data['order_rate_rate'] = round((($list->data['order_rate']- $list->data['order_rate_ys'])/$list->data['order_rate'])*100,2)??0;
        }else{
            $list->data['order_rate_rate'] = 0;
        }
        return $this->SBack($list);        
    }


    public function TimeToDay($time){
        return date('m-d',strtotime($time));
    }
    //设置库存 任务
    //[1=>'同安仓',2=>'泉州仓',3=>'云仓',4=>'工厂虚拟仓',6=>'工厂寄存仓',22=>'工厂直发仓',23=>'深圳寄存仓']
    public function SetCacheInventoryTaskM($params){
        try {
            //code...
            $ds = $params['ds']??date('Y-m-d',time());
            $this->SetCacheInventoryM(['warehouse_id'=>1,'ds'=>$ds]);
            $this->SetCacheInventoryM(['warehouse_id'=>2,'ds'=>$ds]);
            $this->SetCacheInventoryM(['warehouse_id'=>3,'ds'=>$ds]);
            $this->SetCacheInventoryM(['warehouse_id'=>4,'ds'=>$ds]);
            $this->SetCacheInventoryM(['warehouse_id'=>6,'ds'=>$ds]);
            $this->SetCacheInventoryM(['warehouse_id'=>22,'ds'=>$ds]);
            $this->SetCacheInventoryM(['warehouse_id'=>23,'ds'=>$ds]);
      
        } catch (\Throwable $th) {
            //throw $th;
            return $this->FailBack(json_encode($th).$th->getFile().$th->getMessage().$th->getLine());
        }
        return $this->SBack([]);        
    }
    //设置缓存
    public function SetCacheInventoryM($params){
        $ds = $params['ds']??date('Y-m-d',time());
        $warehouse_id = $params['warehouse_id']??1;

        $start_time = date('Y-m-d 00:00:00',strtotime($ds));
        $end_time = date('Y-m-d 23:59:59',strtotime($ds));

        //本周
        $first = 1;//第一天为周一则为1，第一天为周末为0
        $w = date("w",strtotime($ds)) ? date("w",strtotime($ds)) - $first : 6;
        //获取本周开始日期，如果$w是0，则表示周日，减去 6 天
        $week_start = date('Y-m-d 00:00:00', strtotime("$ds-$w"."days"));
        //本周结束日期
        $week_end = date('Y-m-d 23:59:59', strtotime("$week_start +6 days"));

        
        $query_week['warehouse_id'] = $warehouse_id;
        $query_week['start_time'] = $week_start;
        $query_week['end_time'] = $week_end;


        //本月
        $moon_start = date('Y-m'.'-01 00:00:00',strtotime($ds));
        // $moon_end = date('Y-m-d H:i:s',time());
        $moon_end = date("Y-m-d 23:59:59",strtotime("$moon_start +1 month -1 day"));

        $query_moon['warehouse_id'] = $warehouse_id;
        $query_moon['start_time'] = $moon_start;
        $query_moon['end_time'] = $moon_end;

        //本年
        $year_start = date('Y'.'-01-01 00:00:00',strtotime($ds));
        $year_end = date('Y'.'-12-31 23:59:59',strtotime($ds));

        $query_year['warehouse_id'] = $warehouse_id;
        $query_year['start_time'] = $year_start;
        $query_year['end_time'] = $year_end;




        $query['warehouse_id'] = $warehouse_id;
        $query['start_time'] = $start_time;
        $query['end_time'] = $end_time;


        //最近七天
        $next_day = [];
        $next_day_1['start_time'] =  date('Y-m-d 00:00:00',strtotime($ds)-(86400*7));
        $next_day_1['end_time'] =  date('Y-m-d 23:59:59',strtotime($ds)-(86400*7));

        $next_day_2['start_time'] =  date('Y-m-d 00:00:00',strtotime($ds)-(86400*1));
        $next_day_2['end_time'] =  date('Y-m-d 23:59:59',strtotime($ds)-(86400*1));

        $next_day_3['start_time'] =  date('Y-m-d 00:00:00',strtotime($ds)-(86400*2));
        $next_day_3['end_time'] =  date('Y-m-d 23:59:59',strtotime($ds)-(86400*2));

        $next_day_4['start_time'] =  date('Y-m-d 00:00:00',strtotime($ds)-(86400*3));
        $next_day_4['end_time'] =  date('Y-m-d 23:59:59',strtotime($ds)-(86400*3));

        $next_day_5['start_time'] =  date('Y-m-d 00:00:00',strtotime($ds)-(86400*4));
        $next_day_5['end_time'] =  date('Y-m-d 23:59:59',strtotime($ds)-(86400*4));

        $next_day_6['start_time'] =  date('Y-m-d 00:00:00',strtotime($ds)-(86400*5));
        $next_day_6['end_time'] =  date('Y-m-d 23:59:59',strtotime($ds)-(86400*5));

        $next_day_7['start_time'] =  date('Y-m-d 00:00:00',strtotime($ds)-(86400*6));
        $next_day_7['end_time'] =  date('Y-m-d 23:59:59',strtotime($ds)-(86400*6));

        $next_day[0] = $next_day_1;
        $next_day[1] = $next_day_7;
        $next_day[2] = $next_day_6;
        $next_day[3] = $next_day_5;
        $next_day[4] = $next_day_4;
        $next_day[5] = $next_day_3;
        $next_day[6] = $next_day_2;


        //最近一月
        $next_moon = [];
        //最近7
        $next_moon_1['start_time'] = date('Y-m-d 00:00:00',strtotime($ds)-(86400*6));
        $next_moon_1['end_time'] = date('Y-m-d 23:59:59',strtotime($ds));
        //最近14
        $next_moon_2['start_time'] = date('Y-m-d 00:00:00',strtotime($ds)-(86400*13));
        $next_moon_2['end_time'] = date('Y-m-d 23:59:59',strtotime($ds)-(86400*7));
        //最近21
        $next_moon_3['start_time'] = date('Y-m-d 00:00:00',strtotime($ds)-(86400*20));
        $next_moon_3['end_time'] = date('Y-m-d 23:59:59',strtotime($ds)-(86400*14));
        //最近28
        $next_moon_4['start_time'] = date('Y-m-d 00:00:00',strtotime($ds)-(86400*27));
        $next_moon_4['end_time'] = date('Y-m-d 23:59:59',strtotime($ds)-(86400*21));
        $next_moon[] =  $next_moon_4;
        $next_moon[] =  $next_moon_3;
        $next_moon[] =  $next_moon_2;
        $next_moon[] =  $next_moon_1;


        //最近一年
        $next_year = [];
        $year = date('Y',strtotime($ds));
        $moon = date('m',strtotime($ds));
        for ($i=1; $i < $moon+1; $i++) { 
            # code...
            $m = str_pad($i,2,"0",STR_PAD_LEFT);
            $s =  $year.'-'.$m.'-01 00:00:00';
            $one_year['start_time'] = $s;
            $one_year['end_time'] = date("Y-m-d 23:59:59",strtotime("$s +1 month -1 day"));
            $next_year[] =  $one_year;
        }



        $data = [];

        $data['ds']['week'] = $query_week['start_time'].'-'.$query_week['end_time'];
        $data['ds']['moon'] = $query_moon['start_time'].'-'.$query_moon['end_time'];
        $data['ds']['year'] = $query_year['start_time'].'-'.$query_year['end_time'];
        $data['ds']['next_year'] = $next_year;
        $data['ds']['next_moon'] = $next_moon;
        $data['ds']['next_day'] = $next_day;

   
        //仓库总库存
        $data['all_inventory']  = $this->GetAllInventory()[$warehouse_id];



        $InventoryNum = $this->GetInventoryNum($query);
        //今日出库（件/个）
        $data['out_num']  =  $InventoryNum['out'];
        //今日入库（件/个）
        $data['in_num']  =  $InventoryNum['in'];
          
        //今日出库资产
        $data['out_price']  = $InventoryNum['out_price'];

        //今日入库资产  （元）
        $data['in_price']  = $InventoryNum['in_price'];

        $InventoryOrder = $this->GetInventoryOrder($query);
        //总订单
        $data['order_all']  = $InventoryOrder['order_all'];
        $data['order_all_out']  = $InventoryOrder['order_all_out'];
        $data['order_all_in']  = $InventoryOrder['order_all_in'];
        //未完成订单
        $data['order_wait_out']  = $InventoryOrder['order_wait_out'];
        $data['order_wait_in']  = $InventoryOrder['order_wait_in'];
        $data['order_wait']  = $InventoryOrder['order_wait'];
        //已完成订单
        $data['order']  = $InventoryOrder['order'];
        $data['order_out']  = $InventoryOrder['order_out'];
        $data['order_in']  = $InventoryOrder['order_in'];

        //仓库总资产
        $data['total_inventory_price']  = db::table("cloudhouse_warehouse")->where('id',$warehouse_id)->first()->total_price;
      

        //今日订单完成率
        $data['order_rate'] = 0;
        if($data['order_all']>0&&$data['order']>0){
            $data['order_rate'] = round((($data['order_all']-$data['order'])/$data['order_all'])*100,2);
        }

        $OrderTime = $this->GetOrderTime($query);

        //今日订单完成平均时效（小时）
        $data['order_time'] =  $OrderTime['ave_time'];


        // //出库时效及入库时效-周 月 年
        // $data['out_in_time_data'] =[];
        // $w_i = 1;
        // foreach ($next_day as $nd) {
        //     # code...
        //     $q['warehouse_id'] = $warehouse_id;
        //     $q['start_time'] = $nd['start_time'];
        //     $q['end_time'] = $nd['end_time'];
        //     $d = $this->GetOrderTime($q);
        //     $o['date'] = '星期'.$w_i;
        //     $w_i++;
        //     $o['time'] = $this->TimeToDay($nd['start_time']);
        //     $o['out'] = $d['out_ave_time'];
        //     $o['in'] = $d['in_ave_time'];
        //     $data['out_in_time_data']['next_day'][] = $o;
        // }

        // $m_i = 1;
        // foreach ($next_moon as $nd) {
        //     # code...
        //     $q['warehouse_id'] = $warehouse_id;
        //     $q['start_time'] = $nd['start_time'];
        //     $q['end_time'] = $nd['end_time'];
        //     $d = $this->GetOrderTime($q);
        //     $o['date'] = '第'.$m_i.'周';
        //     $m_i++;
        //     $o['time'] = $this->TimeToDay($nd['start_time']).'-'.$this->TimeToDay($nd['end_time']);
        //     $o['out'] = $d['out_ave_time'];
        //     $o['in'] = $d['in_ave_time'];
        //     $data['out_in_time_data']['next_moon'][] = $o;
        // }

        // $y_i = 1;
        // foreach ($next_year as $nd) {
        //     # code...
        //     $q['warehouse_id'] = $warehouse_id;
        //     $q['start_time'] = $nd['start_time'];
        //     $q['end_time'] = $nd['end_time'];
        //     $d = $this->GetOrderTime($q);
        //     $o['date'] = '第'.$y_i.'月';
        //     $y_i++;
        //     $o['time'] = $nd['start_time'].'-'.$nd['end_time'];
        //     $o['out'] = $d['out_ave_time'];
        //     $o['in'] = $d['in_ave_time'];
        //     $data['out_in_time_data']['next_year'][] = $o;
        // }



        // $w_a_i = 1;
        // // 出库数及入库数-周 月 年
        // $data['out_in_num_data'] = [];
        // foreach ($next_day as $nd) {
        //     # code...
        //     $q['warehouse_id'] = $warehouse_id;
        //     $q['start_time'] = $nd['start_time'];
        //     $q['end_time'] = $nd['end_time'];
        //     $d = $this->GetInventoryNum($q);
        //     $o['date'] = '星期'.$w_a_i;
        //     $w_a_i++;
        //     $o['time'] = $this->TimeToDay($nd['start_time']);
        //     $o['out'] = $d['out'];
        //     $o['in'] = $d['in'];
        //     $data['out_in_num_data']['next_day'][] = $o;
        // }

        // $m_a_i = 1;
        // foreach ($next_moon as $nd) {
        //     # code...
        //     $q['warehouse_id'] = $warehouse_id;
        //     $q['start_time'] = $nd['start_time'];
        //     $q['end_time'] = $nd['end_time'];
        //     $d = $this->GetInventoryNum($q);
        //     $o['date'] = '第'.$m_a_i.'周';
        //     $m_a_i++;
        //     $o['time'] = $this->TimeToDay($nd['start_time']).'-'.$this->TimeToDay($nd['end_time']);
        //     $o['out'] = $d['out'];
        //     $o['in'] = $d['in'];
        //     $data['out_in_num_data']['next_moon'][] = $o;
        // }

        // $y_a_i = 1;
        // foreach ($next_year as $nd) {
        //     # code...
        //     $q['warehouse_id'] = $warehouse_id;
        //     $q['start_time'] = $nd['start_time'];
        //     $q['end_time'] = $nd['end_time'];
        //     $d = $this->GetInventoryNum($q);
        //     $o['date'] = '第'.$y_a_i.'月';
        //     $y_a_i++;
        //     $o['time'] = $nd['start_time'].'-'.$nd['end_time'];
        //     $o['out'] = $d['out'];
        //     $o['in'] = $d['in'];
        //     $data['out_in_num_data']['next_year'][] = $o;
        // }



        // //订单类型占比-周 月 年

        // $data['order_type']['week']['data'] = $this->GetInventoryOrderType($query_week);
        // $data['order_type']['moon']['data'] = $this->GetInventoryOrderType($query_moon);
        // $data['order_type']['year']['data'] = $this->GetInventoryOrderType($query_year);



        // //平台出库数量占比-周 月 年
        // $data['platform_outNum']['week']= $this->PlatformOutNum($query_week)['out'];
        // $data['platform_outNum']['moon']= $this->PlatformOutNum($query_moon)['out'];
        // $data['platform_outNum']['year']= $this->PlatformOutNum($query_year)['out'];


        // //今日订单
        // $data['order_list'] = $this->OrderList($query);

        
        // //本月spu出库排名
        // $data['spu_rank'] =$this->SpuRank($query_moon);



        $is['ds'] = $ds;
        $is['update_time'] = date('Y-m-d H:i:s',time());
        $is['warehouse_id'] = $warehouse_id;
        $is['data'] = json_encode($data);
        // return $this->SBack($data); 
        $inventory = db::table('cache_inventory')->where('ds','like','%'.$ds.'%')->where('warehouse_id',$warehouse_id)->first();
        if($inventory){
            db::table('cache_inventory')->where('id',$inventory->id)->update($is);
        }else{
            db::table('cache_inventory')->insert($is);
        }
        return $this->SBack($is); 
    }


    //获取总库存
    //[1=>'同安仓',2=>'泉州仓',3=>'云仓',4=>'工厂虚拟仓',6=>'工厂寄存仓',22=>'工厂直发仓',23=>'深圳寄存仓']
    public function GetAllInventory(){
        $total = db::table('self_custom_sku')->get();

        $t_num = 0;$q_num = 0;$c_num = 0;$g_a_num = 0;$g_b_num = 0;$g_c_num = 0;$s_num = 0;
        foreach ($total as $v) {
            # code...
            $t_num+=$v->tongan_inventory;
            $q_num+=$v->quanzhou_inventory;
            $c_num+=$v->cloud_num;
            $g_a_num+=$v->factory_num;
            $g_b_num+=$v->deposit_inventory;
            $g_c_num+=$v->direct_inventory;
            $s_num +=$v->shenzhen_inventory;
        }

        $return[1] = $t_num;
        $return[2] = $q_num;
        $return[3] = $c_num;
        $return[4] = $g_a_num;
        $return[6] = $g_b_num;
        $return[22] = $g_c_num;
        $return[23] = $s_num;
        return $return;
    }


    //获取出入库件数及资产
    public function GetInventoryNum($params){
        $start_time = $params['start_time'];
        $end_time = $params['end_time'];
        $warehouse_id = $params['warehouse_id'];

        $list =  db::table('cloudhouse_record')->whereBetween('createtime', [$start_time, $end_time])->where('warehouse_id', $warehouse_id)->get();
        $out = 0;
        $in = 0;
        $out_price = 0;
        $in_price = 0;
        foreach ($list as $v) {
            # code...
            if($v->type==1){
                $out+=$v->num;
                $out_price+=$v->num*$v->price;
            }
            if($v->type==2){
                $in+=$v->num;
                $in_price+=$v->num*$v->price;
            }
        }

        $out_price = round($out_price,2);
        $in_price = round($in_price,2);

        return ['out'=>$out,'in'=>$in,'out_price'=>$out_price,'in_price'=>$in_price];
    }

    
    //获取出入库件数及资产
    public function GetInventoryNums($params){
        $start_time = $params['start_time'];
        $end_time = $params['end_time'];
        $warehouse_id = $params['warehouse_id'];

        $list =  db::table('cloudhouse_record')->whereBetween('createtime', [$start_time, $end_time])->whereIn('warehouse_id', $warehouse_id)->get();
        $out = 0;
        $in = 0;
        $out_price = 0;
        $in_price = 0;
        foreach ($list as $v) {
            # code...
            if($v->type==1){
                $out+=$v->num;
                $out_price+=$v->num*$v->price;
            }
            if($v->type==2){
                $in+=$v->num;
                $in_price+=$v->num*$v->price;
            }
        }

        $out_price = round($out_price,2);
        $in_price = round($in_price,2);

        return ['out'=>$out,'in'=>$in,'out_price'=>$out_price,'in_price'=>$in_price];
    }


    //获取库存订单及商品数
    public function GetInventoryOrder($params){
        $start_time = $params['start_time'];
        $end_time = $params['end_time'];
        $warehouse_id = $params['warehouse_id'];
        // $list =  db::table('goods_transfers_detail as a')->leftjoin('goods_transfers as b','a.order_no','=','b.order_no')->where(function($query) use($warehouse_id){
        //     $query->Where('b.out_house',$warehouse_id)->orWhere('b.in_house',$warehouse_id);
        // })->whereBetween('b.createtime', [$start_time, $end_time])->whereIn('is_push',[1,2,3,4])->get();
        $list =  db::table('goods_transfers_detail as a')->leftjoin('goods_transfers as b','a.order_no','=','b.order_no')->where(function($query) use($warehouse_id){
            $query->Where('b.out_house',$warehouse_id)->orWhere('b.in_house',$warehouse_id);
        })->whereIn('is_push',[1,2,3,4])->get();
        $order_no = [];
        $order_no_wait = [];
        $goods_no = 0;
        $goods_no_wait = 0;

        $order_no['out'] = [];
        $order_no['in'] = [];
        $order_no_wait['in'] = [];
        $order_no_wait['out'] = [];
        foreach ($list as $v) {
            # code...
            if($v->is_push==3){
                //已完成
                if(in_array($v->type_detail,$this->GetFieldTypeDetail())){
                    $order_no['out'][$v->order_no] = 1;
                }else{
                    $order_no['in'][$v->order_no] = 1;
                }
               
                $goods_no+=$v->receive_num;
            }else{
                //待完成
                if(in_array($v->type_detail,$this->GetFieldTypeDetail())){
                    $order_no_wait['out'][$v->order_no] = 1;
                }else{
                    $order_no_wait['in'][$v->order_no] = 1;
                }
               
                $goods_no_wait+=$v->transfers_num;
            }
        }
        $return['order_all'] = count($order_no['out'])+count($order_no['in'])+count($order_no_wait['out'])+count($order_no_wait['in']);
        $return['order_all_out'] = count($order_no['out'])+count($order_no_wait['out']);
        $return['order_all_in'] =count($order_no['in'])+count($order_no_wait['in']);
        $return['order_out'] = count($order_no['out']);
        $return['order_in'] = count($order_no['in']);
        $return['order'] = count($order_no['in'])+count($order_no['out']);
        $return['order_wait_out'] = count($order_no_wait['out']);
        $return['order_wait_in'] = count($order_no_wait['in']);
        $return['order_wait'] = count($order_no_wait['in'])+ count($order_no_wait['out']);
        return $return;
    }

    //获取库存订单及商品数
    public function GetInventoryOrderb($params){
        $start_time = $params['start_time'];
        $end_time = $params['end_time'];
        $warehouse_id = $params['warehouse_id'];
        $list =  db::table('goods_transfers')->where(function($query) use($warehouse_id){
            $query->WhereIn('out_house',$warehouse_id)->orWhereIn('in_house',$warehouse_id);
        })->whereBetween('createtime', [$start_time, $end_time])->whereIn('is_push',[1,2,3,4])->get();
 
        // var_dump( $list);
        // $list =  db::table('goods_transfers_detail as a')->leftjoin('goods_transfers as b','a.order_no','=','b.order_no')->where(function($query) use($warehouse_id){
        //     $query->Where('b.out_house',$warehouse_id)->orWhere('b.in_house',$warehouse_id);
        // })->whereIn('is_push',[1,2,3,4])->get();
        $order_no = [];
        $order_no_wait = [];


        $order_no['out'] = [];
        $order_no['in'] = [];
        $order_no_wait['in'] = [];
        $order_no_wait['out'] = [];
        foreach ($list as $v) {
            # code...
            if($v->is_push==3){
                //已完成
                if(in_array($v->type_detail,$this->GetFieldTypeDetail())){
                    $order_no['out'][$v->order_no] = 1;
                }else{
                    $order_no['in'][$v->order_no] = 1;
                }
               
            }else{
                //待完成
                if(in_array($v->type_detail,$this->GetFieldTypeDetail())){
                    $order_no_wait['out'][$v->order_no] = 1;
                }else{
                    $order_no_wait['in'][$v->order_no] = 1;
                }
               
            }
        }
        $return['order_all'] = count($order_no['out'])+count($order_no['in'])+count($order_no_wait['out'])+count($order_no_wait['in']);
        $return['order_all_out'] = count($order_no['out'])+count($order_no_wait['out']);
        $return['order_all_in'] =count($order_no['in'])+count($order_no_wait['in']);
        $return['order_out'] = count($order_no['out']);
        $return['order_in'] = count($order_no['in']);
        $return['order'] = count($order_no['in'])+count($order_no['out']);
        $return['order_wait_out'] = count($order_no_wait['out']);
        $return['order_wait_in'] = count($order_no_wait['in']);
        $return['order_wait'] = count($order_no_wait['in'])+ count($order_no_wait['out']);
        return $return;
    }


    //查询订单时效
    public function GetOrderTime($params){
        $start_time = $params['start_time'];
        $end_time = $params['end_time'];
        $warehouse_id = $params['warehouse_id'];
        $list =  db::table('cloudhouse_record as a')->leftjoin('goods_transfers as b','a.order_no','=','b.order_no')->whereBetween('a.createtime', [$start_time, $end_time])->where('a.warehouse_id', $warehouse_id)->select('a.createtime as end_time','b.createtime as start_time','a.type')->get();
        $out_time = 0;
        $in_time = 0;
        $i = 0;
        foreach ($list as $v) {
            # code...
            if($v->type==1){
                $out_time+=strtotime($v->end_time)-strtotime($v->start_time);
            }else{
                $in_time+=strtotime($v->end_time)-strtotime($v->start_time);
            }
            $i++;
        }
        $time = $out_time+$in_time;
        $ave_time = 0;
        $out_ave_time = 0;
        $in_ave_time = 0;
        if($time>0&&$i>0){
            $ave_time = round(($time/$i)/3600,2);
        }
        if($out_time>0&&$i>0){
            $out_ave_time =  round(($out_time/$i)/3600,2);
        }
        if($in_time>0&&$i>0){
            $in_ave_time =  round(($in_time/$i)/3600,2);
        }

        return ['ave_time'=>$ave_time,'out_ave_time'=>$out_ave_time,'in_ave_time'=>$in_ave_time];
    }


    //查询订单时效
    public function GetOrderTimes($params){
        $start_time = $params['start_time'];
        $end_time = $params['end_time'];
        $warehouse_id = $params['warehouse_id'];
        $list =  db::table('cloudhouse_record as a')->leftjoin('goods_transfers as b','a.order_no','=','b.order_no')->whereBetween('a.createtime', [$start_time, $end_time])->whereIn('a.warehouse_id', $warehouse_id)->select('a.createtime as end_time','b.createtime as start_time','a.type')->get();
        $out_time = 0;
        $in_time = 0;
        $i = 0;
        foreach ($list as $v) {
            # code...
            if($v->type==1){
                $out_time+=strtotime($v->end_time)-strtotime($v->start_time);
            }else{
                $in_time+=strtotime($v->end_time)-strtotime($v->start_time);
            }
            $i++;
        }
        $time = $out_time+$in_time;
        $ave_time = 0;
        $out_ave_time = 0;
        $in_ave_time = 0;
        if($time>0&&$i>0){
            $ave_time = round(($time/$i)/3600,2);
        }
        if($out_time>0&&$i>0){
            $out_ave_time =  round(($out_time/$i)/3600,2);
        }
        if($in_time>0&&$i>0){
            $in_ave_time =  round(($in_time/$i)/3600,2);
        }

        return ['ave_time'=>$ave_time,'out_ave_time'=>$out_ave_time,'in_ave_time'=>$in_ave_time];
    }


    //查询订单类型占比
    //  `type` int DEFAULT '0' COMMENT '1.采购出入库 2.订单出入库 3.工厂出入库 4.样品出入库 5.手工出入库 6.调拨出入库
    public function GetInventoryOrderType($params){
        $start_time = $params['start_time'];
        $end_time = $params['end_time'];
        $warehouse_id = $params['warehouse_id'];

        $list =  db::table('goods_transfers')->whereBetween('createtime', [$start_time, $end_time])->where(function($query) use($warehouse_id){
            $query->Where('out_house',$warehouse_id)->orWhere('in_house',$warehouse_id);
        })->where('is_push','>',0)->get();

        $data = [];
        foreach ($list as $v) {
            # code...

            if(isset($data[$v->type_detail])){
                $data[$v->type_detail]+=1;
            }else{
                $data[$v->type_detail] = 1;
            }
        }

        $ndata = [];
        foreach ($data as $t => $v) {
            # code...
            if($t>0){
                // $nsdata['type_name'] = $t;
                $nsdata['type_name'] = $this->GetFieldTypeDetailArr()[$t];
                $nsdata['num'] = $v;
                $ndata[] = $nsdata;
            }

        }
        return $ndata;
    }


     //查询订单类型占比
    //  `type` int DEFAULT '0' COMMENT '1.采购出入库 2.订单出入库 3.工厂出入库 4.样品出入库 5.手工出入库 6.调拨出入库
    public function GetInventoryOrderTypes($params){
        $start_time = $params['start_time'];
        $end_time = $params['end_time'];
        $warehouse_id = $params['warehouse_id'];

        $list =  db::table('goods_transfers')->whereBetween('createtime', [$start_time, $end_time])->where(function($query) use($warehouse_id){
            $query->WhereIn('out_house',$warehouse_id)->orWhereIn('in_house',$warehouse_id);
        })->where('is_push','>',0)->get();

        $data = [];
        foreach ($list as $v) {
            # code...

            if(isset($data[$v->type_detail])){
                $data[$v->type_detail]+=1;
            }else{
                $data[$v->type_detail] = 1;
            }
        }

        $ndata = [];
        foreach ($data as $t => $v) {
            # code...
            if($t>0){
                // $nsdata['type_name'] = $t;
                $nsdata['type_name'] = $this->GetFieldTypeDetailArr()[$t];
                $nsdata['num'] = $v;
                $ndata[] = $nsdata;
            }

        }
        return $ndata;
    }


    //平台出库占比
    public function PlatformOutNum($params){
        $start_time = $params['start_time'];
        $end_time = $params['end_time'];
        $warehouse_id = $params['warehouse_id'];

        $out = [];
        $in = [];
        $list =  db::table('cloudhouse_record as a')->leftjoin('shop as b','a.shop_id','=','b.Id')->whereBetween('createtime', [$start_time, $end_time])->where('warehouse_id', $warehouse_id)->get();
        foreach ($list as $v) {
            # code...
            if($v->platform_id==''){
                $v->platform_id = 4;
            }
            if($v->type==1){
                if(isset($out[$v->platform_id])){
                    $out[$v->platform_id]+=$v->num;
                }else{
                    $out[$v->platform_id]=$v->num;
                }
            }else{
                if(isset($in[$v->platform_id])){
                    $in[$v->platform_id]+=$v->num;
                }else{
                    $in[$v->platform_id]=$v->num;
                }
            }
        }

        $new_out = [];
        $new_in = [];
        foreach ($out as $k => $v) {
            # code...
            $new_outs['platform'] = $this->GetPlatform($k)['name'];
            $new_outs['num'] = $v;
            $new_out[] = $new_outs;
        }

        foreach ($in as $k => $v) {
            # code...
            $new_ins['platform'] = $this->GetPlatform($k)['name'];
            $new_ins['num'] = $v;
            $new_in[] = $new_ins;
        }

        return ['out'=>$new_out,'in'=>$new_in];
    }


    
    //平台出库占比
    public function PlatformOutNums($params){
        $start_time = $params['start_time'];
        $end_time = $params['end_time'];
        $warehouse_id = $params['warehouse_id'];

        $out = [];
        $in = [];
        $list =  db::table('cloudhouse_record as a')->leftjoin('shop as b','a.shop_id','=','b.Id')->whereBetween('createtime', [$start_time, $end_time])->whereIn('warehouse_id', $warehouse_id)->get();
        foreach ($list as $v) {
            # code...
            if($v->platform_id==''){
                $v->platform_id = 4;
            }
            if($v->type==1){
                if(isset($out[$v->platform_id])){
                    $out[$v->platform_id]+=$v->num;
                }else{
                    $out[$v->platform_id]=$v->num;
                }
            }else{
                if(isset($in[$v->platform_id])){
                    $in[$v->platform_id]+=$v->num;
                }else{
                    $in[$v->platform_id]=$v->num;
                }
            }
        }

        $new_out = [];
        $new_in = [];
        foreach ($out as $k => $v) {
            # code...
            $new_outs['platform'] = $this->GetPlatform($k)['name'];
            $new_outs['num'] = $v;
            $new_out[] = $new_outs;
        }

        foreach ($in as $k => $v) {
            # code...
            $new_ins['platform'] = $this->GetPlatform($k)['name'];
            $new_ins['num'] = $v;
            $new_in[] = $new_ins;
        }

        return ['out'=>$new_out,'in'=>$new_in];
    }


    //本月spu出库排名
    public function SpuRank($params){
        // $start_time = date('Y-m',time()).'-01 00:00:00';
        // $end_time = date('Y-m-d 23:59:59',time());
        $start_time = $params['start_time'];
        $end_time = $params['end_time'];
        $warehouse_id = $params['warehouse_id'];

        $list = db::table('cloudhouse_record')->whereBetween('createtime', [$start_time, $end_time])->where('warehouse_id', $warehouse_id)->select(db::raw('sum(num) as num'),'spu')->groupby('spu')->orderby('num','desc')->limit(10)->get();

        foreach ($list as $v) {
            $v->cate_name = '';
            $spu =  Db::table('self_spu')->where('spu', $v->spu)->orwhere('old_spu', $v->spu)->first();
            $v->cate_name =  $this->GetCategory($spu->one_cate_id)['name'].$this->GetCategory($spu->three_cate_id)['name'];
            # code...
        }
        return $list;
    }


     //本月spu出库排名
     public function SpuRankM($params){
        // $start_time = date('Y-m',time()).'-01 00:00:00';
        // $end_time = date('Y-m-d 23:59:59',time());

        $pagenum = isset($params['page']) ? $params['page'] : 1;
        $limit = isset($params['limit']) ? $params['limit'] : 30;

        
        $page  =  $pagenum- 1;
        if($page!=0){
            $page  =  $page * $limit;
        }

        $start_time = $params['start_time']??date('Y-m-d 00:00:00',time()-(86400*28));
        $end_time = $params['end_time']??date('Y-m-d H:i:s',time());
        $warehouse_id = $params['warehouse_id']??[1,2,3,4,6,22,23];

        $list = db::table('cloudhouse_record')->whereBetween('createtime', [$start_time, $end_time])->where('warehouse_id', $warehouse_id)->select(db::raw('sum(num) as num'),'spu_id','spu')->groupby('spu')->get();

        $lists = $list->toarray();

        $spu_ids = array_column($lists,'spu_id');

                

         //获取总库存
        //[1=>'同安仓',2=>'泉州仓',3=>'云仓',4=>'工厂虚拟仓',6=>'工厂寄存仓',22=>'工厂直发仓',23=>'深圳寄存仓']

        $total = db::table('self_custom_sku')->whereIn('spu_id',$spu_ids)->get();

        $t_num = 0;$q_num = 0;$c_num = 0;$g_a_num = 0;$g_b_num = 0;$g_c_num = 0;$s_num = 0;

        $spu_inventory = [];
        foreach ($total as $v) {
            # code...
            $inven = 0;
            if(in_array(1,$warehouse_id)){ $inven+=$v->tongan_inventory;}
            if(in_array(2,$warehouse_id)){ $inven+=$v->quanzhou_inventory;}
            if(in_array(3,$warehouse_id)){ $inven+=$v->cloud_num;}
            if(in_array(4,$warehouse_id)){ $inven+=$v->factory_num;}
            if(in_array(6,$warehouse_id)){ $inven+=$v->deposit_inventory;}
            if(in_array(22,$warehouse_id)){ $inven+=$v->direct_inventory;}
            if(in_array(23,$warehouse_id)){ $inven+=$v->shenzhen_inventory;}

            if(isset($spu_inventory[$v->spu_id])){
                $spu_inventory[$v->spu_id]+= $inven;
            }else{
                $spu_inventory[$v->spu_id] = $inven;
            }

        }

        foreach ($list as $v) {
            # code...
            if(isset($spu_inventory[$v->spu_id])){
                $v->inventory = $spu_inventory[$v->spu_id];
            }else{
                $v->inventory = 0;
            }
            
        }

        $ware_name = [1=>'同安仓',2=>'泉州仓',3=>'云仓',4=>'工厂虚拟仓',6=>'工厂寄存仓',22=>'工厂直发仓',23=>'深圳寄存仓'];

        $warehouse = [];
        foreach ($warehouse_id as $wid) {
            # code...
            $warehouse [] =$ware_name[$wid];
        }

        $inventory_day = [];
        foreach ($list as $v) {
            $v->cate_name = '';
            $spu =  Db::table('self_spu')->where('id', $v->spu_id)->first();
            if(isset($spu->old_spu)){
                if(!empty($spu->old_spu)){
                    $v->spu = $spu->old_spu;
                }
            }
            if($spu){
                $v->cate_name =  $this->GetCategory($spu->one_cate_id)['name']??''.$this->GetCategory($spu->three_cate_id)['name']??'';
            }else{
                unset($v);
            }

            # code...
            $v->inventory_day = 0;
            if($v->num>0){
                $v->inventory_day = round($v->inventory/$v->num,2);
            }

            $inventory_day[] = $v->inventory_day;

                               
            $v->time = $this->TimeToDay($start_time).'-'.$this->TimeToDay($end_time);

            $v->warehouse = implode('/',$warehouse);
        }


        $list = $list->toarray();

        $orderby = $params['order_by']??1;
        if($orderby==1){
            array_multisort($inventory_day,SORT_DESC,$list);
            $order_type = 'SORT_DESC';
        }else{
            array_multisort($inventory_day,SORT_ASC,$list);
        }
                    //     $spu_list = array_merge($v,$array_2[$k]);
            //     $spu_num = array_column($spu_list,'num');



        foreach ($list as $v) {
            # code...
            $v->img = $this->GetSpuImg($v->spu);
        }

        // $returnlist = [];
        // foreach ($list as $k => $v) {
        //     # code...
        //     for ($i=$limit * ($page-1); $i < $limit * $page; $i++) { 
        //         # code...
        //         if($k==$i){
        //             $returnlist[] = $v;
        //         }
        //     }

        // }


        $data['total_num'] = count($list);
        $data['data'] =  array_slice($list,$page,$limit);
        return $this->SBack($data);
    }


    //spu颜色周转天数表
    public function SpuColorRank($params){

        $pagenum = isset($params['page']) ? $params['page'] : 1;
        $limit = isset($params['limit']) ? $params['limit'] : 30;

        
        $page  =  $pagenum- 1;
        if($page!=0){
            $page  =  $page * $limit;
        }
        $start_time = $params['start_time']??date('Y-m-d 00:00:00',time()-(86400*28));
        $end_time = $params['end_time']??date('Y-m-d H:i:s',time());
        $warehouse_id = $params['warehouse_id']??[1,2,3,4,6,22,23];
        $spu_id = $params['spu_id'];
 
        $list = db::table('self_custom_sku')->where('spu_id',$spu_id)->select(db::raw('group_concat(id) as cus_ids'),'spu','spu_id','color')->groupby('color')->get();


        
         //获取总库存
        //[1=>'同安仓',2=>'泉州仓',3=>'云仓',4=>'工厂虚拟仓',6=>'工厂寄存仓',22=>'工厂直发仓',23=>'深圳寄存仓']

        $total = db::table('self_custom_sku')->where('spu_id',$spu_id)->get();

        $t_num = 0;$q_num = 0;$c_num = 0;$g_a_num = 0;$g_b_num = 0;$g_c_num = 0;$s_num = 0;

        $spu_inventory = [];
        foreach ($total as $v) {
            # code...
            $inven = 0;
            if(in_array(1,$warehouse_id)){ $inven+=$v->tongan_inventory;}
            if(in_array(2,$warehouse_id)){ $inven+=$v->quanzhou_inventory;}
            if(in_array(3,$warehouse_id)){ $inven+=$v->cloud_num;}
            if(in_array(4,$warehouse_id)){ $inven+=$v->factory_num;}
            if(in_array(6,$warehouse_id)){ $inven+=$v->deposit_inventory;}
            if(in_array(22,$warehouse_id)){ $inven+=$v->direct_inventory;}
            if(in_array(23,$warehouse_id)){ $inven+=$v->shenzhen_inventory;}

            if(isset($spu_inventory[$v->id])){
                $spu_inventory[$v->id]+= $inven;
            }else{
                $spu_inventory[$v->id] = $inven;
            }

        }

        $ware_name = [1=>'同安仓',2=>'泉州仓',3=>'云仓',4=>'工厂虚拟仓',6=>'工厂寄存仓',22=>'工厂直发仓',23=>'深圳寄存仓'];

        $warehouse = [];
        foreach ($warehouse_id as $wid) {
            # code...
            $warehouse [] =$ware_name[$wid];
        }

        foreach ($list as $v) {
            $cus = explode(',',$v->cus_ids);

            $num =  db::table('cloudhouse_record')->whereIn('custom_sku_id',$cus)->whereBetween('createtime', [$start_time, $end_time])->where('warehouse_id', $warehouse_id)->sum('num');
            # code...
            $v->num =  $num;
            $inventory = 0;
            foreach ($cus as $cid) {
                # code...
                if(isset($spu_inventory[$cid])){
                    $inventory+=$spu_inventory[$cid];
                }
            }
            $v->inventory = $inventory;
            $v->inventory_day = 0;
            if($num>0){
                $v->inventory_day = round($v->inventory/$num,2);
            }
       
                   
            $v->time = $this->TimeToDay($start_time).'-'.$this->TimeToDay($end_time);

            $v->warehouse = implode('/',$warehouse);

            $inventory_day[] = $v->inventory_day;


        }

        $list = $list->toarray();

        $orderby = $params['order_by']??1;
        if($orderby==1){
            array_multisort($inventory_day,SORT_DESC,$list);
            $order_type = 'SORT_DESC';
        }else{
            array_multisort($inventory_day,SORT_ASC,$list);
        }


        $data['total_num'] = count($list);
        $list =  array_slice($list,$page,$limit);

        foreach ($list as $v) {
            # code...
            $v->color_name  = '';
            $v->img = '';
            $colors = db::table('self_color_size')->where('identifying',$v->color)->first();
            if($colors){
                $v->color_name = $colors->name??'';            
                $base_spu_id = $this->GetBaseSpuIdBySpu($v->spu);
                $v->img = db::table('self_spu_color')->where('base_spu_id',$base_spu_id)->where('color_id', $colors->id)->first()->img??'';
            }



        }

        $data['data'] = $list;
        return $this->SBack($data);
    }


     //库存sku周转天数表
    public function CustomSkuRank($params){
        $pagenum = isset($params['page']) ? $params['page'] : 1;
        $limit = isset($params['limit']) ? $params['limit'] : 30;

        
        $page  =  $pagenum- 1;
        if($page!=0){
            $page  =  $page * $limit;
        }
        $start_time = $params['start_time']??date('Y-m-d 00:00:00',time()-(86400*28));
        $end_time = $params['end_time']??date('Y-m-d H:i:s',time());
        $warehouse_id = $params['warehouse_id']??[1,2,3,4,6,22,23];
        $spu_id = $params['spu_id'];
        $color =  $params['color'];
 
        $list = db::table('self_custom_sku')->where('spu_id',$spu_id)->where('color',$color)->select('id','custom_sku','color','spu','spu_id')->get();


        
         //获取总库存
        $ware_name = [1=>'同安仓',2=>'泉州仓',3=>'云仓',4=>'工厂虚拟仓',6=>'工厂寄存仓',22=>'工厂直发仓',23=>'深圳寄存仓'];

        $total = db::table('self_custom_sku')->where('spu_id',$spu_id)->get();



        $spu_inventory = [];
        foreach ($total as $v) {
            # code...
            $inven = 0;
            if(in_array(1,$warehouse_id)){ $inven+=$v->tongan_inventory;}
            if(in_array(2,$warehouse_id)){ $inven+=$v->quanzhou_inventory;}
            if(in_array(3,$warehouse_id)){ $inven+=$v->cloud_num;}
            if(in_array(4,$warehouse_id)){ $inven+=$v->factory_num;}
            if(in_array(6,$warehouse_id)){ $inven+=$v->deposit_inventory;}
            if(in_array(22,$warehouse_id)){ $inven+=$v->direct_inventory;}
            if(in_array(23,$warehouse_id)){ $inven+=$v->shenzhen_inventory;}

            if(isset($spu_inventory[$v->id])){
                $spu_inventory[$v->id]+= $inven;
            }else{
                $spu_inventory[$v->id] = $inven;
            }

        }

        $warehouse = [];
        foreach ($warehouse_id as $wid) {
            # code...
            $warehouse [] =$ware_name[$wid];
        }

        foreach ($list as $v) {

            $num =  db::table('cloudhouse_record')->where('custom_sku_id',$v->id)->whereBetween('createtime', [$start_time, $end_time])->where('warehouse_id', $warehouse_id)->sum('num');
            # code...
            $v->num =  $num;
            $inventory = 0;
           
            # code...
            if(isset($spu_inventory[$v->id])){
                $inventory=$spu_inventory[$v->id];
            }
            
            $v->inventory = $inventory;
            $v->inventory_day = 0;
            if($num>0){
                $v->inventory_day = round($v->inventory/$num,2);
            }
       
            $v->time = $this->TimeToDay($start_time).'-'.$this->TimeToDay($end_time);

            $v->warehouse = implode('/',$warehouse);
            $inventory_day[] = $v->inventory_day;

        }

        $list = $list->toarray();

        $orderby = $params['order_by']??1;
        if($orderby==1){
            array_multisort($inventory_day,SORT_DESC,$list);
            $order_type = 'SORT_DESC';
        }else{
            array_multisort($inventory_day,SORT_ASC,$list);
        }

        
        $data['total_num'] = count($list);
        $list =  array_slice($list,$page,$limit);
        
        foreach ($list as $v) {
            # code...
            $v->color_name = db::table('self_color_size')->where('identifying',$v->color)->first()->name??'';
            $v->img = $this->GetCustomskuImg($v->custom_sku);
        }

        $data['data'] = $list;
        return $this->SBack($data);
    }

    //本月spu出库排名
    public function SpuRanks($params){
        // $start_time = date('Y-m',time()).'-01 00:00:00';
        // $end_time = date('Y-m-d 23:59:59',time());
        $start_time = $params['start_time'];
        $end_time = $params['end_time'];
        $warehouse_id = $params['warehouse_id'];

        $list = db::table('cloudhouse_record')->whereBetween('createtime', [$start_time, $end_time])->whereIn('warehouse_id', $warehouse_id)->select(db::raw('sum(num) as num'),'spu')->groupby('spu')->orderby('num','desc')->limit(10)->get();

        foreach ($list as $v) {
            $v->cate_name = '';
            $spu =  Db::table('self_spu')->where('spu', $v->spu)->orwhere('old_spu', $v->spu)->first();
            $v->cate_name =  $this->GetCategory($spu->one_cate_id)['name'].$this->GetCategory($spu->three_cate_id)['name'];
            # code...
        }
        return $list;
    }





    //当日订单列表
    public function OrderList($params){
        $start_time = $params['start_time'];
        $end_time = $params['end_time'];
        $warehouse_id = $params['warehouse_id'];

        $list =  db::table('goods_transfers')->whereBetween('createtime', [$start_time, $end_time])->where(function($query) use($warehouse_id){
            $query->Where('out_house',$warehouse_id)->orWhere('in_house',$warehouse_id);
        })->select('order_no','platform_id','type_detail','is_push','createtime','total_num')->limit(20)->get();
        $push = ['2'=>'取消失败','-1'=>'已取消','0'=>'推送失败','1'=>'待推送','2'=>'已推送','3'=>'已确认','4'=>'操作中','5'=>'已退货'];
        foreach ($list as $v) {
            $v->platform = $this->GetPlatform($v->platform_id)['name'];
            $v->type_detail_name = $this->GetFieldTypeDetailArr()[$v->type_detail];
            $v->status_name = $push[$v->is_push];
            # code...
        }

        return $list;
    }



     //当日订单列表
     public function OrderLists($params){
        $start_time = $params['start_time'];
        $end_time = $params['end_time'];
        $warehouse_id = $params['warehouse_id'];

        $list =  db::table('goods_transfers as a')->leftjoin('cloudhouse_location_log as b','b.order_no','=','a.order_no')->whereNotIn('a.type',[3,5])->whereNotIn('a.is_push',[3,-1])->where(function($query) use($warehouse_id){
            $query->WhereIn('a.out_house',$warehouse_id)->orWhereIn('a.in_house',$warehouse_id);
        })->whereBetween('a.createtime', [$start_time, $end_time])->groupby('a.order_no')->select(db::raw('group_concat(b.user_id) as contrl_user_ids'),db::raw('group_concat(b.time) as times'),'a.sj_arrive_time','a.user_id','a.confirm_id','a.order_no','a.push_time','a.platform_id','a.type_detail','a.is_push','a.createtime','a.total_num','a.delivery_time')->limit(20)->get();

        $push = ['-2'=>'取消失败','-1'=>'已取消','0'=>'推送失败','1'=>'待推送','2'=>'已推送','3'=>'已确认','4'=>'操作中','5'=>'已退货'];

  
        foreach ($list as $v) {
            $v->platform = $this->GetPlatform($v->platform_id)['name'];
            $v->type_detail_name = $this->GetFieldTypeDetailArr()[$v->type_detail];
            $v->status_name = $push[$v->is_push];
            $v->time = 0;
            if(strtotime($v->push_time)>strtotime('2023-01-01')){
                if(strtotime($v->sj_arrive_time)>strtotime($v->push_time)){
                    $v->time = round((strtotime($v->sj_arrive_time)- strtotime($v->push_time))/60,2);
                }else{
                    $v->time =  round((time() - strtotime($v->push_time))/60,2);
                }
            }


            $v->efficiency = 0;
            $v->efficiency_time = 0;
            // if($v->time>0&&$v->total_num>0){
            //     $v->efficiency = round($v->time/$v->total_num,2);
            // }
            if($v->is_push==3){
                //最早的操作日志
                $timess = explode(',',$v->times);
                $times = 0;
                foreach ($timess as $tbv) {
                    # code...
                    if($times==0){
                        $times = strtotime($tbv);
                    }else{
                        if(strtotime($tbv)<$times){
                            $times = strtotime($tbv);
                        }
                    }
                   
                }

                $v->times = date('Y-m-d H:i:s',$times);
                if($times>0){
                    $efficiency_time = round(strtotime($v->sj_arrive_time) - $times,2);
                    $v->efficiency_time = $efficiency_time;
                    if($efficiency_time>0&&$v->total_num>0){
                        $v->efficiency = round($v->efficiency_time/$v->total_num,2);
                    }
                }
            }

            
            $v->time = $v->time.'(≈'.round($v->time/60,2).'H)';
            # code...
        }

        return $list;
    }


    //当日订单列表
    public function GetOrderListM($params){

        $push = ['-2'=>'取消失败','-1'=>'已取消','0'=>'推送失败','1'=>'待推送','2'=>'已推送','3'=>'已确认','4'=>'操作中','5'=>'已退货'];

        $page = isset($params['page']) ? $params['page'] : 1;
        $limit = isset($params['limit']) ? $params['limit'] : 30;
        //分页
        $pagenNum=$page-1;
        if ($pagenNum != 0) {
            $pagenNum = $limit * $pagenNum;
        }
        $posttype = $params['posttype']??1;

        $list =  db::table('goods_transfers as a')->leftjoin('cloudhouse_location_log as b','b.order_no','=','a.order_no')->whereNotIn('a.type',[3,5])->whereNotIn('a.is_push',[-1]);
        


        //订单号
        if(isset($params['order_no'])){
            $list = $list->where('a.order_no',$params['order_no']);
        }
            

        //仓库
        if(isset($params['warehouse_id'])){
            $warehouse_id = $params['warehouse_id'];
            $list = $list->where(function($query) use($warehouse_id){
                $query->WhereIn('a.out_house',$warehouse_id)->orWhereIn('a.in_house',$warehouse_id);
            });
    
        }
        
        //平台
        if(isset($params['platform_id'])){
            $platform_id = $params['platform_id'];
            $list = $list->where('a.platform_id',$platform_id);
        }

        //订单类型
        if(isset($params['type_detail'])){
            $type_detail = $params['type_detail'];
            $list = $list->whereIn('a.type_detail',$type_detail);
        }

        //状态
        if(isset($params['is_push'])){
            $is_push = $params['is_push'];
            $list = $list->whereIn('a.is_push',$is_push);
        }

        //创建人
        if(isset($params['user_id'])){
            $list = $list->where('a.user_id',$params['user_id']);
        }
        //确认人
        if(isset($params['confirm_id'])){
            $list = $list->where('a.confirm_id',$params['confirm_id']);
        }

        //操作人
        if(isset($params['control_user_id'])){
            $list = $list->where('b.user_id',$params['control_user_id']);
        }


        $start_time = $params['start_time']??date('Y-m-d',time());
        $end_time = $params['end_time']??date('Y-m-d',time());
        $start_time  = $start_time.' 00:00:00';
        $end_time = $end_time.' 23:59:59';


        //创建时间     //完成时间   //推送时间 
        $time_type = $params['time_type']??'createtime';
        $list = $list->whereBetween('a.'.$time_type, [$start_time, $end_time]);
  
        // //完成时间
        // if(isset($params['delivery_time'])){
        //     $list = $list->whereBetween('a.delivery_time', [$start_time, $end_time]);
        // }

        //  //推送时间    
        // if(isset($params['push_time'])){
        //     $list = $list->whereBetween('a.push_time', [$start_time, $end_time]);
        // } 



         //出入库
        if(isset($params['type'])){
            $out_type =$this->GetFieldTypeDetail();
            $in_type = $this->GetFieldTypeDetail2();
            if($params['type']==1){
                //出库
                $list = $list->whereIn('a.type_detail',$out_type);
            }else{
                //入库
                $list = $list->whereIn('a.type_detail',$in_type);
            }

        }


       $list = $list->groupby('a.order_no');
    //    $totalnum = $list->get()->count();
       $list = $list->select(db::raw('group_concat(b.user_id) as contrl_user_ids'),db::raw('group_concat(b.time) as times'),'a.sj_arrive_time','a.user_id','a.confirm_id','a.order_no','a.push_time','a.platform_id','a.type_detail','a.is_push','a.createtime','a.total_num','a.delivery_time')->get();

       $totalnum = 0;
       $total_order_num = 0;
       $total_time = 0;
       $total_efficiency = 0;

       $effici_num = 0;

       $t_s  = 0;
       foreach ($list as $v) {
            $v->platform = $this->GetPlatform($v->platform_id)['name'];
            $v->type_detail_name = $this->GetFieldTypeDetailArr()[$v->type_detail];
            $v->status_name = $push[$v->is_push];
            $v->create_user = $this->GetUsers($v->user_id)['account'];
            $v->confirm_user = $this->GetUsers($v->confirm_id)['account'];
            $contrl_user_ids = array_unique(explode(',',$v->contrl_user_ids));
            $v->contrl_user = '';
            if(count($contrl_user_ids)>=1){
                foreach ($contrl_user_ids as $ui) {
                    # code...
                    if(is_numeric($ui)){
                        $v->contrl_user.= $this->GetUsers($ui)['account'];
                    }
                }
            }

            $v->type = '入库';

            if(in_array($v->type_detail,$this->GetFieldTypeDetail())){
                $v->type = '出库';
            }

            $v->time = 0;
            $v->wait_time = 0;
            if(strtotime($v->push_time)>strtotime('2023-01-01')){
                $t_s ++;
                if(strtotime($v->sj_arrive_time)>strtotime($v->push_time)){
                    $v->time = round((strtotime($v->sj_arrive_time)- strtotime($v->push_time))/60,2);
                }else{
                    $v->time =  round((time() - strtotime($v->push_time))/60,2);
                }
            }


           


            

            $v->efficiency = 0;
            $v->efficiency_time = 0;
            if($v->is_push==3){
                //最早的操作日志
                $timess = explode(',',$v->times);
                $times = 0;
                foreach ($timess as $tbv) {
                    # code...
                    if($times==0){
                        $times = strtotime($tbv);
                    }else{
                        if(strtotime($tbv)<$times){
                            $times = strtotime($tbv);
                        }
                    }
                   
                }

                $v->times = date('Y-m-d H:i:s',$times);
                if($times>0){
                    $efficiency_time = round(strtotime($v->sj_arrive_time) - $times,2);
                    $v->efficiency_time = $efficiency_time;
                    if($efficiency_time>0&&$v->total_num>0){
                        $effici_num++;
                        $v->efficiency = round(($v->efficiency_time/$v->total_num)/60,2);
                        if($v->efficiency==0){
                            $v->efficiency = 0.01;
                        }
                    }
                }
        
                
            }

            // if($v->time>0&&$v->total_num>0){
            //     // echo 1;
            //     $effici_num++;
            //     $v->efficiency = round($v->time/$v->total_num,2);
            // }

            $total_time+=$v->time;
            $total_order_num+=$v->total_num;
            $total_efficiency += $v->efficiency;
            $totalnum++;

            $v->time = $v->time.'(≈'.round($v->time/60,2).'H)';

        }

        $returnlist = [];
        if($posttype==2){
            $returnlist = $list;
        }else{
            foreach ($list as $k => $v) {
                # code...
                for ($i=$limit * ($page-1); $i < $limit * $page; $i++) { 
                    # code...
                    if($k==$i){
                        $returnlist[] = $v;
                    }
                }
    
            }

        }



        $total['order'] = $totalnum;
        $total['time'] = 0;
        if($t_s>0){
            $total['time'] = round($total_time/$t_s,2).'(≈'.round($total_time/(60*$t_s),2).'H)';
        }

        $total['order_num'] = $total_order_num;
        $total['total_efficiency']  = $total_efficiency;
        $total['avg_efficiency'] = 0;
        if($total_efficiency>0&&$effici_num>0){
            $total['avg_efficiency'] =round($total_efficiency/$effici_num,2);
        }

        $return['totalnum'] =  $totalnum;
        $return['data'] =  $returnlist;
        $return['total'] =  $total;



        if($posttype==2){
            $p['title']='仓管-订单时效表-导出'.time();
            $p['title_list'] = [
                'order_no'=>'订单号',
                'type_detail_name'=>'订单类型',
                'type' => '出/入库',
                'status_name' => '状态',
                'platform'=>'平台',
                'total_num'=>'数量',
                'time'=>'消耗时长（分）',
                'efficiency'=>'时效（分/个）',
                'confirm_user'=>'确认人',
                'contrl_user'=>'操作人',
                'create_user'=>'创建人',
                'push_time'=>'推送时间',
                'sj_arrive_time'=>'完成时间',
                'createtime'=>'创建时间',
            ];

            $onedataa['order_no'] = '';
            $onedataa['type_detail_name'] = '';
            $onedataa['type'] = '';
            $onedataa['status_name'] = '';
            $onedataa['platform'] = '';
            $onedataa['total_num'] = '';
            $onedataa['time'] = '';
            $onedataa['efficiency'] = '';
            $onedataa['confirm_user'] = '';
            $onedataa['contrl_user'] = '';
            $onedataa['create_user'] = '';
            $onedataa['push_time'] = '';
            $onedataa['sj_arrive_time'] = '';
            $onedataa['createtime'] = '';

            $onedata['order_no'] = '订单合计:';
            $onedata['type_detail_name'] = $total['order'];
            $onedata['type'] = '';
            $onedata['status_name'] = '平均时长(分钟)';
            $onedata['platform'] = $total['time'];
            $onedata['total_num'] = '';
            $onedata['time'] = '数量合计';
            $onedata['efficiency'] = $total['order_num'];
            $onedata['confirm_user'] = '';
            $onedata['contrl_user'] = '平均时效(分钟/个)';
            $onedata['create_user'] = $total['avg_efficiency'];
            $onedata['push_time'] = '';
            $onedata['sj_arrive_time'] = '';
            $onedata['createtime'] = '';

            $returnlist[] = $onedataa;
            $returnlist[] = $onedata;

            $p['data'] =   $returnlist;
            $p['user_id'] =  $params['find_user_id']??1;  
            $p['type'] = '仓管-订单时效表-导出';
            // var_dump($p);
            $job = new \App\Http\Controllers\Jobs\ExcelTaskController;
            try {
                $job::runJob($p);
            } catch(\Exception $e){
                return $this->FailBack('加入队列失败');
            }
            return $this->SBack([],'加入队列成功');
        }
        return $this->SBack($return);
    }


    public function SyncLingxinToAmazonStatus(){
        $time = date('Y-m-d',time()-(14*86400));
        // $amazon = db::table('amazon_order_item')->where('amazon_time','>',$time)->get();
        $linxin = db::table('lingxing_amazon_order')->where('order_status','!=','')->where('create_time','>',$time)->get();
        foreach ($linxin as $v) {
            # code...
            echo $v->amazon_order_id."\n";
            db::table('amazon_order_item')->where('amazon_order_id',$v->amazon_order_id)->update(['order_status'=>$v->order_status,'update_time'=>time()]);
        }
        echo '完成';
    }

    //成功返回
    public function SBack($data,$msg='成功'){
        return['type'=>'success','data'=>$data,'msg'=>$msg];
    }

    //失败返回
    public function FailBack($msg){
        return['type'=>'fail','data'=>[],'msg'=>$msg];
    }

    public function getWarehousePlatformInOrOutNumList($params)
    {
        $list =  db::table('cloudhouse_record as a')
            ->leftjoin('shop as b','a.shop_id','=','b.Id');
//            ->whereIn('warehouse_id', [1, 2]);

        $ds = $params['ds']??date('Y-m-d',time());

        if (isset($params['time']) && $params['time'] == 'week'){
            //本周
            $first = 1;//第一天为周一则为1，第一天为周末为0
            $w = date("w",strtotime($ds)) ? date("w",strtotime($ds)) - $first : 6;
            //获取本周开始日期，如果$w是0，则表示周日，减去 6 天
            $week_start = date('Y-m-d 00:00:00', strtotime("$ds-$w"."days"));
            //本周结束日期
            $week_end = date('Y-m-d 23:59:59', strtotime("$week_start +6 days"));

            $list = $list->whereBetween('a.createtime', [$week_start, $week_end]);
        }

        if (isset($params['time']) && $params['time'] == 'month'){
            //本月
            $moon_start = date('Y-m'.'-01 00:00:00',strtotime($ds));
            $moon_end = date("Y-m-d 23:59:59",strtotime("$moon_start +1 month -1 day"));
            $list = $list->whereBetween('a.createtime', [$moon_start, $moon_end]);
        }

        if (isset($params['time']) && $params['time'] == 'year'){
            //本年
            $year_start = date('Y'.'-01-01 00:00:00',strtotime($ds));
            $year_end = date('Y'.'-12-31 23:59:59',strtotime($ds));
            $list = $list->whereBetween('a.createtime', [$year_start, $year_end]);
        }

        if (isset($params['warehouse_id']) && is_array($params['warehouse_id'])){
            $list = $list->whereIn('a.warehouse_id', $params['warehouse_id']);
        }

        if (isset($params['platform_id']) && is_array($params['platform_id'])){
            $list = $list->whereIn('b.platform_id', $params['platform_id']);
        }

        if (isset($params['type'])){
            $list = $list->where('a.type', $params['type']);
        }

        $list = $list
            ->select('a.*', 'b.platform_id')
            ->get();
        $platformMdl = db::table('platform')
            ->get()
            ->pluck('name', 'Id');
        $warehouseName = [
            1 => '同安仓',
            2 => '泉州仓',
            3 => '云仓'
        ];
        $type = [
            1 => '出库',
            2 => '入库'
        ];
        $tongan = [];
        $quanzhou = [];
        $cloud = [];
        foreach ($list as $v){
            $v->platform_id = $v->platform_id ?? 4;
            $key = $v->platform_id.'-'.$v->type;
            if ($v->warehouse_id == 1){
                if (isset($tongan[$key])){
                    $tongan[$key]['num'] += $v->num;
                }else{
                    $tongan[$key] = [
                        'num' => $v->num,
                        'platform_name' => $platformMdl[$v->platform_id],
                        'warehouse_name' => $warehouseName[$v->warehouse_id] ?? '未定义的仓库',
                        'type' => $type[$v->type] ?? '未定义的出入库类型'
                    ];
                }
            }
            if ($v->warehouse_id == 2){
                if (isset($quanzhou[$key])){
                    $quanzhou[$key]['num'] += $v->num;
                }else{
                    $quanzhou[$key] = [
                        'num' => $v->num,
                        'platform_name' => $platformMdl[$v->platform_id],
                        'warehouse_name' => $warehouseName[$v->warehouse_id] ?? '未定义的仓库',
                        'type' => $type[$v->type] ?? '未定义的出入库类型'
                    ];
                }
            }

            if ($v->warehouse_id == 3){
                if (isset($cloud[$key])){
                    $cloud[$key]['num'] += $v->num;
                }else{
                    $cloud[$key] = [
                        'num' => $v->num,
                        'platform_name' => $platformMdl[$v->platform_id],
                        'warehouse_name' => $warehouseName[$v->warehouse_id] ?? '未定义的仓库',
                        'type' => $type[$v->type] ?? '未定义的出入库类型'
                    ];
                }
            }

        }

        return ['code' => 200, 'msg' => '获取成功！', 'data' => ['tongan' => array_values($tongan), 'quanzhou' => array_values($quanzhou), 'cloud' => array_values($cloud)]];
    }
}