<?php

namespace App\Models;

use App\Models\Helper\IdBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\ResourceModel\HaixihuiApplicationModel;
use App\Models\ResourceModel\HaixihuiProductResearchModel;
class QuestionnaireModel extends Model
{
    protected $haixihuiApplicationModel;
    protected $haixihuiProductResearchModel;

    protected $idBuilder;
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->haixihuiApplicationModel = new HaixihuiApplicationModel();
        $this->haixihuiProductResearchModel = new HaixihuiProductResearchModel();
        $this->idBuilder = new IdBuilder();
    }


    /**
     * @Desc:保存海西汇入会申请表
     * @param $data
     * @return mixed
     * @throws \Exception
     * @author: Liu Sinian
     * @Time: 2023/2/17 9:52
     */
    public function saveHaixihuiApplication($data)
    {
        // 保存申请表
        $save = [
            'enterprise_name' => $data['enterprise_name'] ?? '',
            'platform_name' => $data['platform_name'] ?? '',
            'applicat_name' => $data['applicat_name'] ?? '',
            'main_product' => $data['main_product'] ?? '',
            'job' => $data['job'] ?? '',
            'contact_number' => $data['contact_number'] ?? '',
            'email' => $data['email'] ?? '',
            'company_sales_volume' => $data['company_sales_volume'] ?? '',
            'company_address' => $data['company_address'] ?? '',
            'wechat_number' => $data['wechat_number'] ?? '',
            'membership_requirements' => $data['membership_requirements'] ?? '',
            'background_screenshot' => $data['background_screenshot'] ?? '',
            'business_license' => $data['business_license'] ?? '',
            'created_at' => date('Y-m-d H:i:s')
        ];
        $hxhModelId = Db::table('hxh_application_table')->insertGetId($save);

        return $hxhModelId;
    }

    public function saveProductResearch($data)
    {
        try {
            db::beginTransaction();
            $tableType = db::table('hxh_table_type')->where('table_name', $data['title'])->first();
            if (empty($tableType)){
                $type = $this->idBuilder->createId("HXH_TABLE_TYPE");
                if(empty($type)){
                    throw new \Exception('问卷类型生成失败！');
                }
                db::table('hxh_table_type')->insertGetId([
                    'table_name' => $data['title'],
                    'type' => $type
                ]);
            }else{
                $type = $tableType->type;
            }
            $save = [
                'title' => $data['title'] ?? '',
                'content' => json_encode($data['content']) ?? '',
                'type' => $type,
                'created_at' => date('Y-m-d H:i:s')
            ];

            $productResearchModelId = Db::table('hxh_product_research_table')->insertGetId($save);

            db::commit();
            return ['code' => 200, 'msg' => '保存成功！'];
        }catch (\Exception $e){
            db::rollback();
            return ['code' => 500, 'msg' => '保存失败！原因：'.$e->getMessage().'；位置：'.$e->getLine()];
        }

    }

    /**
     * @Desc:获取海西汇入会申请表列表
     * @param $params
     * @return array
     * @author: Liu Sinian
     * @Time: 2023/3/2 17:11
     */
    public function getHaixihuiApplicationList($params = [])
    {
        $haixihuiModel = $this->haixihuiApplicationModel::query();

        // 企业名称
        if (isset($params['enterprise_name']) && !empty($params['enterprise_name'])){
            $haixihuiModel->where('enterprise_name', 'like', '%'.$params['enterprise_name'].'%');
        }
        // 平台名称
        if (isset($params['platform_name']) && !empty($params['platform_name'])){
            $haixihuiModel->where('platform_name', 'like', '%'.$params['platform_name'].'%');
        }
        // 申请人名称
        if (isset($params['applicat_name']) && !empty($params['applicat_name'])){
            $haixihuiModel->where('applicat_name', 'like', '%'.$params['applicat_name'].'%');
        }
        // 主营产品
        if (isset($params['main_product']) && !empty($params['main_product'])){
            $haixihuiModel->where('main_product', 'like', '%'.$params['main_product'].'%');
        }
        // 职务
        if (isset($params['job']) && !empty($params['job'])){
            $haixihuiModel->where('job', 'like', '%'.$params['job'].'%');
        }
        // 联系电话
        if (isset($params['contact_number']) && !empty($params['contact_number'])){
            $haixihuiModel->where('contact_number', 'like', '%'.$params['contact_number'].'%');
        }
        // 邮箱
        if (isset($params['email']) && !empty($params['email'])){
            $haixihuiModel->where('email', 'like', '%'.$params['email'].'%');
        }
        // 公司年销售额
        if (isset($params['company_sales_volume']) && !empty($params['company_sales_volume'])){
            $haixihuiModel->where('company_sales_volume', 'like', '%'.$params['company_sales_volume'].'%');
        }
        // 公司地址
        if (isset($params['company_address']) && !empty($params['company_address'])){
            $haixihuiModel->where('email', 'company_address', '%'.$params['company_address'].'%');
        }
        // 微信号
        if (isset($params['wechat_number']) && !empty($params['wechat_number'])){
            $haixihuiModel->where('wechat_number', 'like', '%'.$params['wechat_number'].'%');
        }
        // 入会需求
        if (isset($params['membership_requirements']) && !empty($params['membership_requirements'])){
            $haixihuiModel->where('membership_requirements', 'like', '%'.$params['membership_requirements'].'%');
        }
        // 创建日期
        if (isset($params['created_at']) && !empty($params['created_at'])){
            $haixihuiModel->whereBetween('created_at', (array)$params['created_at']);
        }
        // 获取数据条数
        $count = $haixihuiModel->count();
        if (isset($params['limit']) && !empty($params['limit']) && isset($params['page']) && !empty($params['page'])){
            $limit = $params['limit'] ?? 30;
            $page = $params['page'] ?? 1;
            $pageNum = $page <= 1 ? 0 : $limit*($page-1);
            $haixihuiModel->limit($limit)->offset($pageNum);
        }
        $list = $haixihuiModel->get()->toArray();

        return ['count' => $count, 'list' => $list];
    }

    /**
     * @Desc:获取海西汇产品调研数据列表
     * @param $params
     * @return array
     * @author: Liu Sinian
     * @Time: 2023/3/2 17:11
     */
    public function getHaixihuiProductResearchList($params = [])
    {
        $haixihuiModel = $this->haixihuiProductResearchModel::query();

        if (!empty($params['type'])){
            $haixihuiModel = $haixihuiModel->where('type', $params['type']);
        }
        if (isset($params['limit']) && !empty($params['limit']) && isset($params['page']) && !empty($params['page'])){
            $limit = $params['limit'] ?? 30;
            $page = $params['page'] ?? 1;
            $pageNum = $page <= 1 ? 0 : $limit*($page-1);
            $haixihuiModel->limit($limit)->offset($pageNum);
        }
        $count = $haixihuiModel->count();
        $list = $haixihuiModel->get()->toArray();
        foreach ($list as &$item){
            $item['content'] = json_decode($item['content']);
        }

        return ['count' => $count, 'list' => $list];
    }

    public function getHaixihuiTableTypeList()
    {
        $list = db::table('hxh_table_type');
        $count = $list->count();
        $list = $list->get();

        return ['count' => $count, 'list' => $list];
    }
}