<?php

namespace App\Http\Controllers\Pc;

use App\Http\Controllers\Controller;
use App\Models\AmazonOrder;
use App\Models\AmazonOrderItem;
use App\Models\BaseModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Datawhole;
use App\Http\Requests\DataWhole as DataWholes;
use Illuminate\Support\Facades\Redis;

class DatawholeController extends Controller
{
    /**
     * 数据统计与分析模块代码
     * $model 模型
     * request 参数
     */

    private $model;
    private $request;
    private $validate;

    public function __construct(Request $request)
    {
        $this->model = new Datawhole();
        $this->validate = new DataWholes();
        $this->request = $request;
    }

    //获取分类
    public function getcategory()
    {
        $result = $this->model->getCategoryId();
        return $this->back('获取成功', '200', $result);
    }


    //数据报表-总
    public function datawhole_all()
    {

        $params = $this->request->all();

        $result = $this->model->getDatawholeAll($params);

        return $this->back('获取成功', '200', $result);
    }
    //数据报表-新品
    public function datawhole_new()
    {

        $params = $this->request->all();

        $result = $this->model->getDatawholeNew($params);

        return $this->back('获取成功', '200', $result);
    }

    //数据报表-店铺列表
    public function datawhole_shop()
    {
        $params = $this->request->all();

        $result = $this->model->getDatawholeShop($params);

        return $this->back('获取成功', '200', $result);
    }

    //数据报表-运营人员列表
    public function datawhole_user()
    {
        $params = $this->request->all();

        $result = $this->model->getDatawholeUser($params);

        return $this->back('获取成功', '200', $result);
    }


    //数据报表-spu列表
    public function datawhole_spu()
    {
        $params = $this->request->all();

        $result = $this->model->getDatawholeSpu($params);

        return $this->back('获取成功', '200', $result);
    }

    //数据报表-spu列表
    public function DatawholeSku()
    {
        $params = $this->request->all();

        $result = $this->model->DatawholeSku($params);

        return $this->back('获取成功', '200', $result);
    }


    //数据报表-fasin列表(新)
    public function DatawholeFasin()
    {
        $params = $this->request->all();

        $result = $this->model->DatawholeFasin($params);

        return $this->back('获取成功', '200', $result);
    }

    //数据报表-fasin店铺列表
    public function DatawholeFasinByShop()
    {
        $params = $this->request->all();

        $result = $this->model->DatawholeFasinByShop($params);

        return $this->back('获取成功', '200', $result);
    }

    

    //获取fasin每日详情
    public function GetFasinReport(){

        $params = $this->request->all();

        $result = $this->model->GetFasinReportM($params);

        return $this->back('获取成功', '200', $result);
        
    }

    //获取fasin计划
    public function GetFasinPlan(){

        $params = $this->request->all();

        $result = $this->model->GetFasinPlanM($params);

        return $this->back('获取成功', '200', $result);
        
    }


    
    //获取fasin预算报表-其他
    public function GetFasinPlanOther(){

        $params = $this->request->all();

        $result = $this->model->GetFasinPlanOtherM($params);

        return $this->back('获取成功', '200', $result);
        
    }

    //fasin预算审核
    public function FasinPlanExamine(){

        $params = $this->request->all();

        $result = $this->model->FasinPlanExamine($params);

        return $this->back('获取成功', '200', $result);
        
    }

    //预算导入
    public function ImportFasinPlan(){

        $params = $this->request->all();

        $result = $this->model->ImportFasinPlan($params);

        if($result['type']=='success'){
            return $this->back('获取成功', '200', $result);
        }else{
            return $this->back($result['msg'], '300');
        }
        
    }


    //款号预算导入
    public function ImportFasinCusPlan(){

        $params = $this->request->all();

        $result = $this->model->ImportFasinCusPlan($params);

        if($result['type']=='success'){
            return $this->back('获取成功', '200', $result);
        }else{
            return $this->back($result['msg'], '300');
        }
        
    }

    

    //款号预算
    public function SaveFasinCusPlan(){

        $params = $this->request->all();

        $result = $this->model->SaveFasinCusPlanM($params);

        if ($result['type'] == 'success') {
            return $this->back('获取成功', '200', $result);
        } else {
            return $this->back($result['msg'], '300');
        }
        // return $this->back('获取成功', '200', $result);
        
    }


    //款号预算列表
    public function FasinCusPlanList(){

        $params = $this->request->all();

        $result = $this->model->FasinCusPlanListM($params);

        return $this->back('获取成功', '200', $result);
        
    }


    //删除款号预算
    public function DelCusPlan(){

        $params = $this->request->all();

        $result = $this->model->DelCusPlanM($params);

        return $this->back('获取成功', '200', $result);
        
    }

    

    //获取fasin-sku列表
    public function GetFasinSkus(){

        $params = $this->request->all();

        $result = $this->model->GetFasinSkusM($params);

        return $this->back('获取成功', '200', $result);
        
    }

    //fasin生产下单
    // public function SaveFasinPlaceOrder(){

    //     $params = $this->request->all();

    //     $result = $this->model->SaveFasinPlaceOrderM($params);

    //     return $this->back('获取成功', '200', $result);
        
    // }

    //fasin生产下单列表
    public function GetFasinPlaceOrder(){

        $params = $this->request->all();

        $result = $this->model->GetFasinPlaceOrderM($params);

        return $this->back('获取成功', '200', $result);
        
    }

    //fasin生产下单列表
    public function UpdateFasinPlaceOrder(){

        $params = $this->request->all();

        $result = $this->model->UpdateFasinPlaceOrderM($params);

        return $this->back('获取成功', '200', $result);
        
    }

    //数据报表-新增fasin(新)
    public function SaveFasinPlan()
    {
        $params = $this->request->all();

        $result = $this->model->SaveFasinPlanM($params);

        if ($result['type'] == 'success') {
            return $this->back('获取成功', '200', $result);
        } else {
            return $this->back($result['msg'], '300');
        }
    }
    
    //数据报表-fasin图片(新)
    public function GetFainimgs()
    {
        $params = $this->request->all();

        $result = $this->model->GetFainimgs($params);

        return $this->back('获取成功', '200', $result);
    }

    

    //数据报表-新增fasin(新)
    public function CreateFasin()
    {
        $params = $this->request->all();

        $result = $this->model->CreateFasin($params);

        if ($result['type'] == 'success') {
            return $this->back('获取成功', '200', $result);
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    //数据报表-删除fasin(新)
    public function DelFasin()
    {
        $params = $this->request->all();

        $result = $this->model->DelFasin($params);

        return $this->back('获取成功', '200', $result);
    }
    
    //日报-fasin
    public function DayReportByFasin()
    {
        $params = $this->request->all();

        $result = $this->model->DayReportByFasin($params);

        return $this->back('获取成功', '200', $result);
    }

    //周报-fasin
    public function WeekReportByFasin()
    {
        $params = $this->request->all();

        $result = $this->model->WeekReportByFasin($params);

        return $this->back('获取成功', '200', $result);
    }


    //获取日报警戒线
    public function DayReportWarn()
    {
        $params = $this->request->all();

        $result = $this->model->DayReportWarn($params);

        return $this->back('获取成功', '200', $result);
    }


    //设置日报警戒线
    public function SetDayReportWarn()
    {
        $params = $this->request->all();

        $result = $this->model->SetDayReportWarn($params);

        return $this->back('设置成功', '200');
    }


    //数据报表-spu报表下的库存sku报表
    public function datawhole_customs(){

        $params = $this->validate->scene('dataWholeCustoms')->check($this->request);

        $result = $this->model->getDatawholeCustoms($params);

        return $this->back('获取成功', '200', $result);
    }

    //数据报表-库存sku报表下的sku报表
    public function datawhole_customs_sku(){
        $params = $this->validate->scene('getDatawholeCustomsSku')->check($this->request);

        $result = $this->model->getDatawholeCustomsSku($params);

        return $this->back('获取成功', '200', $result);
    }

    //数据报表-asin列表
    public function datawhole_asin()
    {
        $params = $this->request->all();

        $result = $this->model->getDatawholeAsin($params);

        return $this->back('获取成功', '200', $result);
    }

    //数据报表-sku列表
    public function datawhole_sku()
    {
        $params = $this->request->all();

        $result = $this->model->getDatawholeSku($params);

        return $this->back('获取成功', '200', $result);
    }

    //数据报表-父asin列表
    public function datawhole_father_asin()
    {
        $params = $this->request->all();

        $result = $this->model->getDatawholeFatherAsin($params);

        return $this->back('获取成功', '200', $result);
    }

    //根据sku获取数据
    public function datawhole_sku_detail()
    {
        $params = $this->request->all();

        $result = $this->model->getDatawholeSkuDetail($params);

        return $this->back('获取成功', '200', $result);
    }

    //亚马逊订单列表
    public function amazonorder()
    {

        $params = $this->request->all();
        if (!isset($params['page_count'])) {
            $params['page_count'] = 100;
        }
        $AmazonOrderItemsCom = new \App\Libs\wrapper\AmazonOrderItems();
        $AmazonOrderItemsModel = new \App\Models\AmazonOrderItem();
         $AmazonOrderItemsModel = $AmazonOrderItemsCom->pageSearch($AmazonOrderItemsModel, $params);
        $AmazonPage = $AmazonOrderItemsModel->orderBy('id', 'desc')->paginate($params['page_count']);
        $AmazonOrderItemsCom->dealResult($AmazonPage);

        $AmazonOrderItemsModel2 = new \App\Models\AmazonOrderItem();
        $AmazonOrderItemsModel2 = $AmazonOrderItemsCom->pageSearch($AmazonOrderItemsModel2, $params);
        $count = $AmazonOrderItemsModel2->whereIn('order_status',array('Shipped','Pending','Unshipped'))->count('amazon_order_id');

        $AmazonOrderItemsModel3 = new \App\Models\AmazonOrderItem();
        $AmazonOrderItemsModel3 = $AmazonOrderItemsCom->pageSearch($AmazonOrderItemsModel3, $params);
        $saleNum = $AmazonOrderItemsModel3->whereIn('order_status',array('Shipped','Pending','Unshipped'))->sum('quantity_ordered');

        $AmazonOrderItemsModel4 = new \App\Models\AmazonOrderItem();
        $AmazonOrderItemsModel4 = $AmazonOrderItemsCom->pageSearch($AmazonOrderItemsModel4, $params);
        $salePrice = $AmazonOrderItemsModel3->whereIn('order_status',array('Shipped','Pending','Unshipped'))->sum('amount_us');

        $this->back('获取成功', '200', array('pagedata'=>$AmazonPage,'order_count'=>$count,'sale_num'=>$saleNum,'sale_price'=>$salePrice));
        //$this->back('获取成功', '200', $AmazonPage);
    }

    //新增备面料下单
//    public function Sparefabric(){
//        $params = $this->request->all();
//
//        $result = $this->model->Sparefabric($params);
//
//        if ($result['type'] == 'success') {
//            return $this->back('新增成功', '200', $result);
//        } else {
//            return $this->back($result['msg'], '500');
//        }
//    }

    //生产下单任务
    public function PlaceOrderTask(){
        $params = $this->request->all();

        $result = $this->model->PlaceOrderTask($params);

        if ($result['type'] == 'success') {
            return $this->back('获取成功', '200', $result);
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    //生产下单任务列表
    public function PlaceOrderTaskList()
    {
        $params = $this->request->all();

        $result = $this->model->PlaceOrderTaskList($params);

        return $this->back('获取成功', '200', $result);
    }

    
    //生产下单任务合并导出
    public function PlaceOrderSumExport()
    {
        $params = $this->request->all();

        $result = $this->model->PlaceOrderSumExport($params);

        return $this->back('获取成功', '200', $result);
    }

    
    //生产下单任务列表-列表
    public function PlaceOrderDetailByCate()
    {
        $params = $this->request->all();

        $result = $this->model->PlaceOrderDetailByCate($params);

        return $this->back('获取成功', '200', $result);
    }

    //生产下单任务详情
    public function PlaceOrderTaskDetailList()
    {
        $params = $this->request->all();

        $result = $this->model->PlaceOrderTaskDetailList($params);

        return $this->back('获取成功', '200', $result);
    }

    //生产下单任务修改
    public function updateOrder(){
        $params = $this->request->all();

        $result = $this->model->updateOrder($params);

        if($result){
            return $this->back('修改成功', '200', $result);
        }else{
            return $this->back('修改失败', '500', $result);
        }

    }

    //生产下单任务详情修改
    public function updateOrderDetail(){
        $params = $this->request->all();

        $result = $this->model->updateOrderDetail($params);

        if($result['code'] ?? 500 == 200){
            return $this->back('修改成功', '200');
        }else{
            return $this->back('修改失败', '500', $result['msg']);
        }

    }

    /**
     * @Desc:更新任务订单详情
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/2/22 14:25
     */
    public function updatePlaceOrderDetail()
    {
        // 获取参数
        $params = $this->request->all();
        // 处理逻辑
        $result = $this->model->updatePlaceOrderDetail($params);
        // 判断返回
        if ($result['code'] != 200){
            return $this->back($result);
        }

        return $this->back($result, 200);
    }

    //生产下单导入-新
    public function PlaceOrderImportNew()
    {
        $params = $this->request->all();
        $result = $this->model->PlaceOrderImportNew($params);
        if ($result['type'] == 'success') {
            return $this->back('导入成功', '200', $result);
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    //生产下单导出-新
    public function PlaceOrderExport()
    {
        $params = $this->request->all();
        $result = $this->model->PlaceOrderExport($params);
        if ($result['type'] == 'success') {
            return $this->back('导出成功', '200', $result);
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    public function copyOrder(){
        $params = $this->request->all();
        $result = $this->model->copyOrder($params);
        if ($result['type'] == 'success') {
            return $this->back('复制成功', '200', $result);
        } else {
            return $this->back($result['msg'], '300');
        }

    }


    //日报列表
    public function DayReportList()
    {
        $params = $this->request->all();

        $result = $this->model->DayReportList($params);

        return $this->back('获取成功', '200', $result);
    }

    //日报详情
    public function GetLastReport()
    {
        $params = $this->request->all();

        $result = $this->model->GetLastReport($params);

        return $this->back('获取成功', '200', $result);
    }
    


    //日报详情
    public function DayReportDetail()
    {
        $params = $this->request->all();

        $result = $this->model->DayReportDetail($params);

        return $this->back('获取成功', '200', $result);
    }

    //新增/修改日报
    public function DayReport()
    {
        $params = $this->request->all();

        $result = $this->model->DayReport($params);

        $this->AddSysLog($params,$result,'添加日报',__METHOD__);


        if( $result['type']=='success'){
            return $this->back('操作成功', '200');
        }else{
            return $this->back('操作失败', '300', $result['msg']);
        }
    }


    //获取日报周计划
    public function GetDayReportWeekInfo()
    {
        $params = $this->request->all();

        $result = $this->model->GetDayReportWeekInfo($params);

        return $this->back('获取成功', '200', $result);
    }


    //自动填写日报
    public function ReportAuto()
    {
        $params = $this->request->all();

        $result = $this->model->ReportAutoM($params);
        if( $result['type']=='success'){
            return $this->back('操作成功', '200',$result);
        }else{
            return $this->back('操作失败'.$result['msg'], '300', $result['msg']);
        }
    }

    //确认日报
    public function ConfirmDayreport()
    {
        $params = $this->request->all();

        $result = $this->model->ConfirmDayreport($params);

        return $this->back('获取成功', '200', $result);
    }

    

    //日报导入
    public function ReportExcelImport()
    {
        $params = $this->request->all();

        $result = $this->model->ReportExcelImportM($params);
        if( $result['type']=='success'){
            return $this->back('操作成功', '200',$result);
        }else{
            return $this->back('操作失败'.$result['msg'], '300', $result['msg']);
        }
    }

    //获取链接排名
    public function GetRankByFasin()
    {
        $params = $this->request->all();

        $result = $this->model->GetRankByFasin($params);

        return $this->back('获取成功', '200', $result);
    }

    //获取fasin
    public function GetFasin()
    {
        $params = $this->request->all();

        $result = $this->model->GetFasin($params);

        return $this->back('获取成功', '200', $result);
    }

     //新增链接排名
     public function add_linkrank(){
        $params = $this->request->all();
    
        $result = $this->model->addLinkrank($params);
    
        if ($result['code'] == 'success') {
            return $this->back('新增成功', '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }
    //编辑链接排名
    public function update_linkrank(){
        $params = $this->request->all();
    
        $result = $this->model->updateLinkrank($params);
    
        if ($result['code'] == 'success') {
            return $this->back('修改成功', '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    //删除链接排名
    public function del_linkrank(){
        $params = $this->request->all();

        $result = $this->model->delLinkrank($params);

        if ($result['code'] == 'success') {
            return $this->back('删除成功', '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }
    
    //链接排名列表
    public function fnsku_rank_list()
    {
        $params = $this->request->all();

        $result = $this->model->FnskuRankList($params);

        return $this->back('获取成功', '200', $result);
    }

    //链接导入
    public function excel_insert()
    {

        $params = $this->request->all();
        $result = $this->model->excelInsert($params);

        if ($result['type'] == 'success') {
            return $this->back('导入成功', '200', $result['msg']);
        } else {
            return $this->back('导入失败', '200', $result['msg']);
        }
    }


    //获取亚马逊运营人员和产品部人员
    public function getUser()
    {
        $params = $this->request->all();
        $where = "state !=2";
        if(isset($params['key'])) {
            $key = $params['key'];
            $where .= " and account like '%$key%'";
        }
        $sql = "select account,Id,state from users where $where";
//        $sql = "select a.account,a.Id from users a left join organizes_member b on a.Id=b.user_id where $where";
        $data = DB::select($sql)??'';
        return $this->back('获取成功', '200', $data);
    }

    //派发任务-生成任务
    public function create_task(){

        $params = $this->request->all();

        $result = $this->model->distributed_task($params);

        if ($result['type'] == 'success') {
            return $this->back('成功', '200', $result['msg']);
        } else {
            return $this->back('失败', '500', $result['msg']);
        }
    }
        
    //生产下单导入
    public function PlaceOrderImport()
    {
        $params = $this->validate->scene('PlaceOrderImport')->check($this->request);
        $result = $this->model->PlaceOrderImport($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    //供应链导入
    public function SupplyChainImport()
    {
        $params = $this->validate->scene('SupplyChainImport')->check($this->request);
        $result = $this->model->SupplyChainImport($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }


     //生产下单 - 列表
    public function PlaceOrderList()
    {
        $params = $this->request->all();
        $result = $this->model->PlaceOrderList($params);
        return $this->back('获取成功', '200', $result);
    }

     //生产下单 - 修改
    public function PlaceOrderEdit()
    {
        $params = $this->validate->scene('PlaceOrderEdit')->check($this->request);
        $result = $this->model->PlaceOrderEdit($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }
    //生产下单 - 删除
    public function PlaceOrderDel()
    {
        $params = $this->validate->scene('PlaceOrderDel')->check($this->request);
        $result = $this->model->PlaceOrderDel($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    //图片上传
    public function UploadImg()
    {
        $params = $this->request->all();
        $result = $this->model->UploadImg($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    //亚马逊活动任务
    public function ActivityTask(){
        $params = $this->request->all();
        $result = $this->model->newActivityTaskList($params);
        return $this->back('获取成功', '200', $result);
    }

    //补货计划明细 - 列表
    public function BuhuoRequestList()
    {
        $params = $this->request->all();
        $result = $this->model->BuhuoRequestList($params);
        return $this->back('获取成功', '200', $result);
    }

    //店铺吊牌信息
    public function amazonTag(){

        $params = $this->request->all();

        $tag = DB::table('amazon_tag')->get();

        return $tag;
    }


    //excel导出队列
    public function ExcelTaskList()
    {
        $params = $this->request->all();

        $result = $this->model->ExcelTaskList($params);

        return $this->back('获取成功', '200', $result);
    }

    //spu时效表
    public function spuTime(){
        return $this->back('该功能需重新开发，具体开放时间待定。', 500);
        $params = $this->request->all();

        $result = $this->model->spuTime($params);

        return $this->back('获取成功', '200', $result);
    }

    //新增需求
    public function addDemand(){
        $params = $this->request->all();

        $result = $this->model->addDemand($params);

        return $this->back($result['msg'], '200');
    }

    //删除需求
    public function deleteDemand(){
        $params = $this->request->all();

        $result = $this->model->deleteDemand($params);

        return $this->back($result['msg'], '200');
    }

    //需求列表
    public function demandList(){
        $params = $this->request->all();

        $result = $this->model->demandList($params);

        return $this->back('获取成功', '200', $result);
    }

    //需求明细
    public function demandDetail(){
        $params = $this->request->all();

        $result = $this->model->demandDetail($params);

        return $this->back('获取成功', '200', $result);
    }

    //指派需求执行人
    public function demandExecute(){
        $params = $this->request->all();

        $idlist = implode(',',$params['execute_id']);

        $update = DB::table('demand_collect')->where('id',$params['id'])->update(['execute_id'=>$idlist,'start_time'=>date('Y-m-d H:i:s'),'progress'=>3]);

        if($update){
            return $this->back('指派成功', '200');
        }else{
            return $this->back('指派失败', '200');
        }


    }

    //反馈进度节点与内容
    public function demandBack(){
        $params = $this->request->all();

        if($params['progress']==2){
            $update = DB::table('demand_collect')->where('id',$params['id'])->update(['progress'=>$params['progress'],'feedback'=>$params['feedback'],'complete_time'=>date('Y-m-d H:i:s'),'start_time'=>date('Y-m-d H:i:s')]);
        }else{
            $update = DB::table('demand_collect')->where('id',$params['id'])->update(['progress'=>$params['progress'],'feedback'=>$params['feedback']]);
        }
        if($update){
            return $this->back('反馈成功', '200');
        }else{
            return $this->back('反馈失败', '200');
        }


    }

    //确认需求完成
    public function demandComplete(){
        $params = $this->request->all();

        $update = DB::table('demand_collect')->where('id',$params['id'])->update(['progress'=>5,'complete_time'=>date('Y-m-d H:i:s')]);

        if($update){
            return $this->back('确认成功', '200');
        }else{
            return $this->back('确认失败', '200');
        }


    }


    

    //数据报表-spu列表-走势图
    public function DataWholeSpuChangeInfo()
    {
        $params = $this->request->all();

        $result = $this->model->DataWholeSpuChangeInfo($params);
        if($result['type']=='success'){
            return $this->back('获取成功', '200', $result);
        }else{
            return $this->back($result['msg'], '300');
        }

    }
    
    //数据报表-spu列表-新
    public function DatawholeSpuNew()
    {
        $params = $this->request->all();

        $result = $this->model->DatawholeSpuNew($params);
        if($result['type']=='success'){
            return $this->back('获取成功', '200', $result);
        }else{
            return $this->back($result['msg'], '300');
        }

    }

    //spu报表查询计划发货人
    public function DatawholeSpuGetRequestUser(){
        $params = $this->request->all();
 
        $result = $this->model->DatawholeSpuGetRequestUser($params);
        if($result['type']=='success'){
            return $this->back('获取成功', '200', $result);
        }else{
            return $this->back('获取失败', '300');
        }
    }

     //数据报表-库存sku列表-新
     public function DatawholeCusNew()
     {
         $params = $this->request->all();
 
         $result = $this->model->DatawholeCusNew($params);
         if($result['type']=='success'){
             return $this->back('获取成功', '200', $result);
         }else{
             return $this->back($result['msg'], '300');
         }
 
     }

    //数据报表-库存sku列表-新
    public function DatawholeSpuColor()
    {
        $params = $this->request->all();

        $result = $this->model->DatawholeSpuColor($params);
        if($result['type']=='success'){
            return $this->back('获取成功', '200', $result);
        }else{
            return $this->back($result['msg'], '300');
        }

    }

    //数据报表-店铺-spu报表
    public function DatawholeShopSpu()
    {
        $params = $this->request->all();
        $result = $this->model->DatawholeShopSpu($params);
        if($result['type']=='success'){
            return $this->back('获取成功', '200', $result);
        }else{
            return $this->back($result['msg'], '300');
        }
    }

    //数据报表-店铺-spu报表
    public function DatawholeShopSpuColor()
    {
        $params = $this->request->all();
        $result = $this->model->DatawholeShopSpuColor($params);
        if($result['type']=='success'){
            return $this->back('获取成功', '200', $result);
        }else{
            return $this->back($result['msg'], '300');
        }
    }

          //数据报表-店铺-spu报表
    public function DatawholeShopCus()
    {
        $params = $this->request->all();
        $result = $this->model->DatawholeShopCus($params);
        if($result['type']=='success'){
            return $this->back('获取成功', '200', $result);
        }else{
            return $this->back($result['msg'], '300');
        }
    }


    //需求列表
    public function runCache(){
        $params = $this->request->all();

        $result = $this->model->runCache($params);

        if($result['type']=='success'){
            return $this->back($result['msg'], '200');
        }else{
            return $this->back('删除失败', '300');
        }
    }
  

    public function DayReportTotal(){
        $params = $this->request->all();
        $result = $this->model->DayReportTotal($params);
        if($result['type']=='success'){
            return $this->back('获取成功', '200', $result);
        }else{
            return $this->back($result['msg'], '300');
        }
    }


    //单人下单
    public function TeamPlaceOrderTaskByOne(){
        $params = $this->request->all();
        $result = $this->model->TeamPlaceOrderTaskByOne($params);
        if($result['type']=='success'){
            return $this->back('获取成功', '200', $result);
        }else{
            return $this->back($result['msg'], '300');
        }
    }

    public function  TeamPlaceOrderTaskByOneDel(){
        $params = $this->request->all();
        $result = $this->model-> TeamPlaceOrderTaskByOneDel($params);
        if($result['type']=='success'){
            return $this->back('获取成功', '200', $result);
        }else{
            return $this->back('删除失败', '300');
        }
    }

    public function  TeamPlaceOrderTaskByOneupdate(){
        $params = $this->request->all();
        $result = $this->model-> TeamPlaceOrderTaskByOneupdate($params);
        if($result['type']=='success'){
            return $this->back('获取成功', '200', $result);
        }else{
            return $this->back('删除失败', '300');
        }
    }
    

    //单人下单-列表
    public function TeamPlaceOrderTaskByOneList(){
        $params = $this->request->all();
        $result = $this->model->TeamPlaceOrderTaskByOneList($params);
        if($result['type']=='success'){
            return $this->back('获取成功', '200', $result);
        }else{
            return $this->back($result['msg'], '300');
        }
    }


    //单人下单-详情
    public function TeamPlaceOrderTaskByOneDetail(){
        $params = $this->request->all();
        $result = $this->model->TeamPlaceOrderTaskByOneDetail($params);
        if($result['type']=='success'){
            return $this->back('获取成功', '200', $result);
        }else{
            return $this->back($result['msg'], '300');
        }
    }

    //仓库盘点
    public function AddCustomSkuCount(){
//        return $this->back('非盘点期间，该功能暂时关闭！', '300');
        $params = $this->request->all();
        $result = $this->model->AddCustomSkuCount($params);
        if(empty($params['token'])){
            $params['token'] = '0BFE3116FB6DC775132D3063CAFC2C62';
        }
        $this->AddSysLog($params,$result,'盘点提交',__METHOD__);
        if($result['type']=='success'){
            return $this->back('获取成功', '200', $result);
        }else{
            return $this->back($result['msg'], '300');
        }
    }

    //库存盘点列表-小组
    public function CustomSkuCountByUser(){
        $params = $this->request->all();
        $result = $this->model->CustomSkuCountByUser($params);
        return $this->back('获取成功', '200', $result);
    }

    public function CustomSkuCountByUserb(){
        $params = $this->request->all();
        $result = $this->model->CustomSkuCountByUserb($params);
        return $this->back('获取成功', '200', $result);
    }

    public function CustomSkuCountByCodea(){
        $params = $this->request->all();
        $result = $this->model->CustomSkuCountByCodea($params);
        return $this->back('获取成功', '200', $result);
    }
    public function CustomSkuCountByUserc(){
        $params = $this->request->all();
        $result = $this->model->CustomSkuCountByUserc($params);
        return $this->back('获取成功', '200', $result);
    }
    public function UpdateCustomSkuCount(){
        $params = $this->request->all();
        $result = $this->model->UpdateCustomSkuCount($params);
        return $this->back('获取成功', '200', $result);
    }
    
    //仓库盘点删除
    public function DelCustomSkuCount(){
//        return $this->back('非盘点期间，该功能暂时关闭！', '300');
        $params = $this->request->all();
        $result = $this->model->DelCustomSkuCount($params);
        if(empty($params['token'])){
            $params['token'] = '0BFE3116FB6DC775132D3063CAFC2C62';
        }
        $this->AddSysLog($params,$result,'盘点删除',__METHOD__);
        if($result['type']=='success'){
            return $this->back('获取成功', '200', $result);
        }else{
            return $this->back($result['msg'], '300');
        }
    }
    
    //入库清单
    public function Instore(){
        $params = $this->request->all();
        $result = $this->model->Instore($params);
        return $this->back('获取成功', '200', $result);
    }
    //仓库库存sku验证
    public function CheckCus(){
        $params = $this->request->all();
        $result = $this->model->CheckCus($params);
        $this->AddSysLog($params,$result,'盘点扫码',__METHOD__);
        if($result['type']=='success'){
            return $this->back('获取成功', '200', $result);
        }else{
            return $this->back($result['msg'], '300');
        }
    }
    
    //库存盘点列表
    public function GetCusCount(){
        $params = $this->request->all();
        $result = $this->model->GetCusCount($params);
        return $this->back('获取成功', '200', $result);
    }

    //库存盘点列表-系统
    public function GetCusCountSelf(){
        $params = $this->request->all();
        $result = $this->model->GetCusCountSelf($params);
        return $this->back('获取成功', '200', $result);
    }

    public function GetCusCountSaihe(){
        $params = $this->request->all();
        $result = $this->model->GetCusCountSaihe($params);
        return $this->back('获取成功', '200', $result);
    }


    
    //集体下单-数据
    public function TeamPlaceOrderTaskByData(){
        $params = $this->request->all();
        $result = $this->model->TeamPlaceOrderTaskByData($params);
        if($result['type']=='success'){
            return $this->back('获取成功', '200', $result);
        }else{
            return $this->back($result['msg'], '300');
        }
    }

    //集体下单
    public function TeamPlaceOrderTaskByTotal(){
        $params = $this->request->all();
        $result = $this->model->TeamPlaceOrderTaskByTotal($params);
        if($result['type']=='success'){
            return $this->back('获取成功', '200', $result);
        }else{
            return $this->back($result['msg'], '300');
        }
    }

    //集体下单-获取个人下单订单数
    public function TeamPlaceOrderGetCusUser(){
        $params = $this->request->all();
        $result = $this->model->TeamPlaceOrderGetCusUser($params);
        if($result['type']=='success'){
            return $this->back('获取成功', '200', $result);
        }else{
            return $this->back($result['msg'], '300');
        }
    }

    //集体下单-修改个人下单订单数
    public function TeamPlaceOrderUpdateCusUser(){
        $params = $this->request->all();
        $result = $this->model->TeamPlaceOrderUpdateCusUser($params);
        if($result['type']=='success'){
            return $this->back('获取成功', '200', $result);
        }else{
            return $this->back($result['msg'], '300');
        }
    }


    //集体下单-删除个人下单详情
    public function TeamPlaceOrderDelCusUser(){
        $params = $this->request->all();
        $result = $this->model->TeamPlaceOrderDelCusUser($params);
        if($result['type']=='success'){
            return $this->back('获取成功', '200', $result);
        }else{
            return $this->back('删除失败', '300');
        }
    }

    

    //集体下单-详情
    public function TeamPlaceOrderTaskByTotalDetail(){
        $params = $this->request->all();
        $result = $this->model->TeamPlaceOrderTaskByTotalDetail($params);
        if($result['type']=='success'){
            return $this->back('获取成功', '200', $result);
        }else{
            return $this->back($result['msg'], '300');
        }
    }

    public function getPlaceOrderTaskListBySpu()
    {
        $params = $this->request->all();

        $result = $this->model->getPlaceOrderTaskListBySpu($params);

        return $this->back($result['msg'] ?? '获取失败！', $result['code'] ?? 500,$result['data'] ?? []);
    }


    //获取spu美码欧码本地fba库存
    public function GetSpuInventoryByBaseSpu(){
        $params = $this->request->all();
        $result = $this->model->GetSpuInventoryByBaseSpu($params);
        if($result['type']=='success'){
            return $this->back('获取成功', '200', $result);
        }else{
            return $this->back('获取失败', '300');
        }
    }

    /**
     * @Desc:保存时效进度备注
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/4/23 17:45
     */
    public function saveEgeingScheduleMemo()
    {
        $params = $this->request->all();

        $result = $this->model->saveEgeingScheduleMemo($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500,$result['data'] ?? []);
    }

    /**
     * @Desc:保存fasin备注
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/5/4 15:04
     */
    public function saveAmazonFasinMemo()
    {
        $params = $this->request->all();

        $result = $this->model->saveAmazonFasinMemo($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500,$result['data'] ?? []);
    }

    /**
     * @Desc:保存fasin留言
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/5/8 14:00
     */
    public function saveFasinLeaveMessage()
    {
        $params = $this->request->all();

        $result = $this->model->saveFasinLeaveMessage($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500,$result['data'] ?? []);
    }

    public function getAmazonFasinMemoList()
    {
        $params = $this->request->all();

        $result = $this->model->getAmazonFasinMemoList($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500,$result['data'] ?? []);
    }

    /**
     * @Desc:删除留言
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/5/10 16:06
     */
    public function deleteAmazonFasinLeaveMessage()
    {
        $params = $this->request->all();

        $result = $this->model->deleteAmazonFasinLeaveMessage($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500,$result['data'] ?? []);
    }

    /**
     * @Desc:点赞留言
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/5/10 16:06
     */
    public function thumbsUpAmazonFasinLeaveMessage()
    {
        $params = $this->request->all();

        $result = $this->model->thumbsUpAmazonFasinLeaveMessage($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500,$result['data'] ?? []);
    }

    /**
     * @Desc:保存核心词
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/6/5 15:14
     */
    public function saveFasinCodeCoreWord()
    {
        $params = $this->request->all();

        $result = $this->model->saveFasinCodeCoreWord($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500,$result['data'] ?? []);
    }

    public function updateFasinMessage()
    {
        $params = $this->request->all();
//        $fasin = DB::table('amazon_fasin')->whereNotNull('memo')
//            ->get();
//        foreach ($fasin as $item){
//            $msgSave = [
//                'reviewer_id' => 14, // 点评人id
//                'content' => $item->memo, // 留言内容
//                'reviewer_name' => 'zitychen',
//                'reviewer_department' => '总经办',
//                'head_img' => 'http://q.zity.cn/headimg.jpg',
//                'fid' => 0, // 上级留言id
//                'fasin' => $item->fasin, // fasin
//                'receiver_id' => 0, // 被指定人id
//                'receiver_name' => '', // 被指定人名称
//                'content_type' => 1, // 留言类型 默认1：普通留言， 2：困难留言
//                'is_currency' => 1, // 是否通用留言
//                'created_at' => date('Y-m-d H:i:s', microtime(true)),
//            ];
//            // 保存数据
//            $msgId = DB::table('amazon_fasin_message')->insertGetId($msgSave);
//        }
        $fasinMessage = db::table('amazon_fasin_message')->get();


        return 1;
    }

    public function updateFasinCode(){
        $fasin = DB::table('amazon_fasin')
            ->where('fasin_code', '')
            ->get();
        $base = new BaseModel();
        DB::beginTransaction();
        $error = [];
        foreach ($fasin as $item){
            $shopIds = explode(',', $item->shops);
            foreach ($shopIds as $shopId){
                if ($shopId <= 0){
                    continue;
                }
                $fasinBind = db::table('amazon_fasin_bind')->where('shop_id', $shopId)->orderBy('fasin_code', 'DESC')->first();
                $shop = $base->GetShop($shopId)['identifying'];
                if ($fasinBind){
                    $no = preg_replace('/\D/','', $fasinBind->fasin_code);
                    $code = (int)$no+1;
                    $code = str_pad($code, 2, '0',STR_PAD_LEFT);
                    $fasinCode = $shop.$code;
                }else{
                    $fasinCode = $shop."01";
                }
                try {
                    $id = DB::table('amazon_fasin_bind')->insertGetId(['fasin' => $item->fasin, 'shop_id' => $shopId, 'fasin_code' => $fasinCode]);
                }catch (\Exception $e){
                    $error[] = ['更新错误！错误原因：'.$e->getMessage()];
                    continue;
                }
            }
        }
        DB::commit();
        return $error;
    }


    //导入利润表
    public function SaveProfit()
    {
        $params = $this->request->all();

        $result = $this->model->SaveProfitM($params);

        if ($result['type'] == 'success') {
            return $this->back('获取成功', '200', $result);
        } else {
            return $this->back($result['msg'], '300');
        }
    }

     //查看利润表
     public function GetProfit()
     {
         $params = $this->request->all();
 
         $result = $this->model->GetProfitM($params);
         
         return $this->back('获取成功', '200', $result);
     }

     public function saveFasinSituation()
     {
         $params = $this->request->all();

         $result = $this->model->saveFasinSituation($params);

         return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500,$result['data'] ?? []);
     }


     public function AppV(){
        $params = $this->request->all();
        $type = $params['type'];
        if($type=='set'){
            $v = $params['v'];
            Redis::Set('app-V',$v);
        }
        if($type=='get'){
            $v =  Redis::Get('app-V')??0;
        }
        return $this->back('获取成功', '200', $v);
     }

     public function getFasinCodeList()
     {
         $params = $this->request->all();

         $result = $this->model->getFasinCodeList($params);

         return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500,$result['data'] ?? []);
     }

    public function GetAdjustInventoryList()
    {
        $params = $this->request->all();
        $result = $this->model->GetAdjustInventoryList($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500,$result['data'] ?? []);
    }
}

