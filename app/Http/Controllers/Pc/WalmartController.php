<?php

namespace App\Http\Controllers\Pc;


use App\Http\Controllers\Controller;
use App\Models\WalmartOrderModel;
use Illuminate\Http\Request;

class WalmartController extends Controller
{
    private $model;
    private $request;

    public function __construct(Request $request)
    {
        $this->model   = new WalmartOrderModel();
        $this->request = $request;
    }

    public function GetOrderList()
    {
        $params = $this->request->all();

        $result = $this->model->GetOrderListM($params);

        return $this->Ts_back($result);
    }

    private function Ts_back($params)
    {
        if ($params['type'] == 'success') {
            return $this->back('获取成功', '200', $params['data']);
        } else {
            return $this->back($params['msg'], '300');
        }
    }
}