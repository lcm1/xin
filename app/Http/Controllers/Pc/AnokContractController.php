<?php

namespace App\Http\Controllers\Pc;

use App\Http\Controllers\Controller;
use App\Libs\wrapper\AnokContract;
use Illuminate\Support\Facades\DB;

class AnokContractController extends Controller
{
    /**
     * 获取表格数据
     * @return array
     */
    public function getContractList(){
       list($list,$count) = AnokContract::index();
        $this->back('获取成功', '200', ['list' => $list, 'count' => $count]);
    }
}