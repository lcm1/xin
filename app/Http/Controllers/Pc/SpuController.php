<?php
namespace App\Http\Controllers\Pc;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class SpuController extends Controller{
//////////////////////////////////////////////// SPU
////////////////////// 配置
    protected $request;
    protected $model;
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->model = new \App\Libs\wrapper\Spu();
    }
	/**
	 * 配置列表--数据
	 * @param name 名称
	 * @param identifying 标识
	 */
	public function config_list(Request $request){
		$data = $request->all();
		if(empty($data)){
			$this->back('接收值为空');
		}
		
		$field = "sc.*, (select count(Id) from spu_config where fid=sc.Id) as f_num";
		$call_spu = new \App\Libs\wrapper\Spu();
		$return = $call_spu->config_list($data, $field);
//		var_dump($return);exit;
		if(is_array($return['list'])) {
			//数据处理
			if(count($return['list'])>0 and $data['type']==1){
				foreach ($return['list'] as $k=>$v){
					$return['list'][$k]['hasChildren'] = $v['f_num']==0 ? false : true;
				}
			}
			
			$this->back('获取成功', '200', $return);
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 新增配置
	 * @logic 名称唯一
	 */
	public function config_add(Request $request){
		$this->validate($request, [
			'fid' => 'required| integer',
			'name' => 'required| string',
			'type' => 'required| string'
		], [
			'fid.required' => 'fid为空',
			'fid.integer' => 'fid必须是数字',
			'name.required' => 'name为空',
			'name.string' => 'name必须是字符串',
			'type.required' => 'type为空',
			'type.integer' => 'type必须是数字',
		]);
		
		$data = $request->all();
		$call_spu = new \App\Libs\wrapper\Spu();
		$return = $call_spu->config_add($data);
//		var_dump($return);exit;
		if($return === 1){
			$this->back('新增成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 编辑配置
	 * @param Id
	 * @logic 名称唯一
	 */
	public function config_update(Request $request){
		$this->validate($request, [
			'Id' => 'required| integer',
		], [
			'Id.required' => 'Id为空',
			'Id.integer' => 'Id必须是数字',
		]);
		
		$data = $request->all();
		$call_spu = new \App\Libs\wrapper\Spu();
		$return = $call_spu->config_update($data);
//		var_dump($return);exit;
		if($return === 1) {
			$this->back('编辑成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 删除配置
	 * @param Id
	 * @param type 类型：1.类目，2.颜色，3.尺码，4.材质，5.产品属性，6.包装状态，7.产品描述
	 */
	public function config_del(Request $request){
		$this->validate($request, [
			'Id' => 'required| integer',
		], [
			'Id.required' => 'Id为空',
			'Id.integer' => 'Id必须是数字',
		]);
		
		$data = $request->all();
		$call_spu = new \App\Libs\wrapper\Spu();
		$return = $call_spu->config_del($data);
//		var_dump($return);exit;
		if($return === 1) {
			$this->back('删除成功', '200');
		}else{
			$this->back($return);
		}
	}


////////////////////// 类目配置
	/**
	 * 类目--Excel导入
	 * @param file 文件流
	 */
	public function category_import(Request $request){
		$data['file'] = $_FILES['file'];
		if(empty($data['file'])){
			$this->back('文件为空');
		}
		
		$call_spu = new \App\Libs\wrapper\Spu();
		$return = $call_spu->category_import($data);
//		var_dump($return);exit;
		if($return === 1){
			$this->back('导入成功', '200');
		}else{
			$this->back($return);
		}
		
	}



////////////////////// spu列表
	/**
	 * spu列表--数据
	 * @param spu
	 * @parma user_kf_name 开发人
	 * @parma platform_id 平台id
	 * @param title 标题
	 * @param type_id_1 大类
	 * @param type_id_2 二类
	 * @param type_id_3 三类
	 * @param state 状态：1.未使用，2.待完善，3.待审核，4.未通过，5.已通过，6.已确认
	 * @param create_time 创建时间
	 */
	public function spu_list(Request $request){
		$data = $request->all();
		if(empty($data)){
			$this->back('接收值为空');
		}
		
		////权限验证
		$arr = array();
		$arr['user_id'] = $data['user_info']['Id'];
		$arr['identity'] = array('spu_list_all');
		$powers = $this->powers($arr);
		$data['list_all'] = $powers['spu_list_all'];
		
		$call_spu = new \App\Libs\wrapper\Spu();
		$return = $call_spu->spu_list($data);
//		var_dump($return);exit;
		if(is_array($return['list'])) {
			$this->back('获取成功', '200', $return);
		}else{
			$this->back($return);
		}
	}
	
	// 平台数据
	public function platform_list(){
		$sql = "select *
				from `platform`
				where `state`=1";
		$list = json_decode(json_encode(db::select($sql)),true);
		
		if(is_array($list)){
			$this->back('获取成功', '200', $list);
		}else{
			$this->back('获取失败');
		}
	}
	
	/*
	 * 配置查找 -- find
	 * @parma fid 父id
	 * @parma name 名称
	 * @param identifying 标识
	 * @Parma type 类型：1.类目，2.颜色，3.尺码，4.材质，5.产品属性，6.包装状态，7.产品描述
	 * */
	public function config_find(Request $request){
		$data = $request->all();
		
		$field = '';
		if($data['type'] == 1){
			$field = "sc.*, (select count(Id) from spu_config where fid=sc.Id) as f_num";
		}
		
		$call_spu = new \App\Libs\wrapper\Spu();
		$return = $call_spu->config_list($data, $field);
//		var_dump($return);exit;
		if(is_array($return['list'])) {
			$this->back('获取成功', '200', $return['list']);
		}else{
			$this->back($return);
		}
	}
	
	
	/**
	 * spu预览
	 * @param platform_id 平台id
	 * @param type_id_1 大类id
	 * @param type_id_2 二类id
	 * @param type_id_3 三类id
	 */
	public function spu_preview(Request $request){
		$this->validate($request, [
			'platform_id' => 'required| integer',
			'type_id_1' => 'required| integer',
			'type_id_2' => 'required| integer',
			'type_id_3' => 'required| integer',
		], [
			'platform_id.required' => 'platform_id为空',
			'platform_id.integer' => 'platform_id必须是数字',
			'type_id_1.required' => 'type_id_1为空',
			'type_id_1.integer' => 'type_id_1必须是数字',
			'type_id_2.required' => 'type_id_2为空',
			'type_id_2.integer' => 'type_id_2必须是数字',
			'type_id_3.required' => 'type_id_3为空',
			'type_id_3.integer' => 'type_id_3必须是数字',
		]);
		
		$data = $request->all();
		$call_spu = new \App\Libs\wrapper\Spu();
		$return = $call_spu->spu_preview($data);
//		var_dump($return);exit;
		if(is_array($return)) {
			$this->back('获取成功', '200', $return);
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 新增spu
	 * @param platform_id 平台id
	 * @param type_id_1 大类id
	 * @param type_id_2 二类id
	 * @param type_id_3 三类id
	 */
	public function spu_add(Request $request){
		$this->validate($request, [
			'platform_id' => 'required| integer',
			'type_id_1' => 'required| integer',
			'type_id_2' => 'required| integer',
			'type_id_3' => 'required| integer',
		], [
			'platform_id.required' => 'platform_id为空',
			'platform_id.integer' => 'platform_id必须是数字',
			'type_id_1.required' => 'type_id_1为空',
			'type_id_1.integer' => 'type_id_1必须是数字',
			'type_id_2.required' => 'type_id_2为空',
			'type_id_2.integer' => 'type_id_2必须是数字',
			'type_id_3.required' => 'type_id_3为空',
			'type_id_3.integer' => 'type_id_3必须是数字',
		]);
		
		$data = $request->all();
		////权限验证
		$arr = array();
		$arr['user_id'] = $data['user_info']['Id'];
		$arr['identity'] = array('spu_add');
		$powers = $this->powers($arr);
		if(!$powers['spu_add']){
			$this->back('无该权限');
		}
		
		$data['user_cj'] = $data['user_info']['Id'];// 创建人
		$data['user_kf'] = $data['user_info']['Id'];// 开发人
		$call_spu = new \App\Libs\wrapper\Spu();
		$return = $call_spu->spu_add($data);
//		var_dump($return);exit;
		if($return == 1) {
			$this->back('新增成功', '200');
		}else{
			$this->back($return);
		}
	}

////////////////////// spu详情页面
	/**
	 * 配置数据
	 * @param Id spu的id
	 * 数据：spu基本信息、用户信息、第三类目(根据二类获取)、颜色、尺寸、产品描述、供应商
	 */
	public function get_allocation(Request $request){
		$this->validate($request, [
			'Id' => 'required| integer',
		], [
			'Id.required' => 'Id为空',
			'Id.integer' => 'Id必须是数字',
		]);
		
		$data = $request->all();
		$call_spu = new \App\Libs\wrapper\Spu();
		$return = $call_spu->get_allocation($data);
//		var_dump($return);exit;
		if(is_array($return)) {
			$this->back('获取成功', '200', $return);
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 类目
	 * @param Id spu的id
	 * @param fid 父id
	 * @param type 类型：1.类目，2.尺寸
	 */
	public function pattern(Request $request){
		$data = $request->all();
		
		$where = '1=1';
		// fid 父id
		if(isset($data['fid'])){
			$where .= " and `fid`={$data['fid']}";
		}
		// type 类型：1.类目，2.颜色，3.尺码，4.材质，5.产品属性，6.包装形式，7.产品描述，8.包装材质，9.款式
		if(!empty($data['type'])){
			$where .= " and `type`={$data['type']}";
		}
		
		$sql = "select *
				from ss_pattern
				where {$where}";
		$list = json_decode(json_encode(db::select($sql)),true);
		if(is_array($list)) {
			$this->back('获取成功', '200', $list);
		}else{
			$this->back('获取失败');
		}
	}
	
	/**
	 * spu保存信息
	 * @param basic array 基本信息
	 * @param color array 颜色
	 * @param size array 尺码
	 * @param style array 款式
	 * @param img array 图片
	 * @param price array 定价
	 * @param pack array 包装
	 * @param material array 材质
	 * @param productstate array 产品属性
	 */
	public function spu_conserve(Request $request){
		$data = $request->all();
		$call_spu = new \App\Libs\wrapper\Spu();
		$return = $call_spu->spu_conserve($data);
		if($return === 1) {
			$this->back('操作成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * spu审核
	 * @param spu_id
	 * @param handle_desc 处理说明
	 * @param state 状态：1.未使用，2.待完善，3.待审核，4.未通过，5.已通过，6.已确认
	 */
	public function spu_examine(Request $request){
		$this->validate($request, [
			'spu_id' => 'required| integer',
			'state' => 'required| integer',
		], [
			'spu_id.required' => 'spu_id为空',
			'spu_id.integer' => 'spu_id必须是数字',
			'state.required' => 'state为空',
			'state.integer' => 'state必须是数字',
		]);
		
		$data = $request->all();
		////权限验证
		$arr = array();
		$arr['user_id'] = $data['user_info']['Id'];
		$arr['identity'] = array('spu_examine');
		$powers = $this->powers($arr);
		if(!$powers['spu_examine']){
			$this->back('无该权限');
		}
		
		$call_spu = new \App\Libs\wrapper\Spu();
		$return = $call_spu->spu_examine($data);
//		var_dump($return);exit;
		if($return === 1) {
			$this->back('操作成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 回退完善
	 * @param Id
	 */
	public function spu_back(Request $request){
		$this->validate($request, [
			'Id' => 'required| integer',
		], [
			'Id.required' => 'Id为空',
			'Id.integer' => 'Id必须是数字',
		]);
		
		$data = $request->all();
		$data['user_cl'] = 0;
		$data['handle_time'] = '0000-00-00 00:00:00';
		$data['handle_desc'] = '';
		
		$call_spu = new \App\Libs\wrapper\Spu();
		$return = $call_spu->spu_update($data);
//		var_dump($return);exit;
		if($return === 1) {
			$this->back('操作成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * spu中的sku列表--数据
	 * @param spu_id
	 */
	public function spu_sku_list(Request $request){
		$data = $request->all();
		if(empty($data)){
			$this->back('接收值为空');
		}
		
		// 列表
		$sql= "select s.Id, s.sku, s.title, s.purchase_price, s.weight_j, s.weight_m, s.img_zt, c.name as color_name, se.name as size_name, sy.name as style_name
		       from spu_sku s
		       left join spu_config c on c.Id = s.color_id
			   left join spu_config se on se.Id = s.size_id
			   left join spu_config sy on sy.Id = s.style_id
		       where s.spu_id={$data['spu_id']}";
		$list = json_decode(json_encode(db::select($sql)),true);
		if(is_array($list)) {
			$this->back('获取成功', '200', $list);
		}else{
			$this->back('获取失败');
		}
	}
	
	/**
	 * 最终确认
	 * @param Id
	 */
	public function spu_confirm(Request $request){
		$this->validate($request, [
			'Id' => 'required| integer',
		], [
			'Id.required' => 'Id为空',
			'Id.integer' => 'Id必须是数字',
		]);
		
		$data = $request->all();
		
		$call_spu = new \App\Libs\wrapper\Spu();
		$return = $call_spu->spu_update($data);
//		var_dump($return);exit;
		if($return === 1) {
			$this->back('操作成功', '200');
		}else{
			$this->back($return);
		}
	}

////////////////////// sku列表
	/**
	 * sku列表--数据
	 * @param platform_id 平台id
	 * @param type_id_1 大类
	 * @param type_id_2 二类
	 * @param type_id_3 三类
	 * @param title 标题
	 * @param spu spu编码
	 * @param sku sku编码
	 */
	public function sku_list(Request $request){
		$data = $request->all();
		if(empty($data)){
			$this->back('接收值为空');
		}
		
		$call_spu = new \App\Libs\wrapper\Spu();
		$return = $call_spu->sku_list($data);
//		var_dump($return);exit;
		if(is_array($return['list'])) {
			$this->back('获取成功', '200', $return);
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * sku导出
	 * @param sku_ids sku id 字符串  1,2,3
	 * @param sku_title sku标题
	 * @param sku sku编码
	 * @param spu spu编码
	 */
	public function sku_export(Request $request){
		$data = $request->all();
		
		if(empty($data)){
			$this->back('接收值为空');
		}
		
		$call_spu = new \App\Libs\wrapper\Spu();
		$return = $call_spu->sku_export($data);
//		var_dump($return);exit;
	}
	
	/**
	 * 编辑sku
	 * @param Id
	 */
	public function sku_update(Request $request){
		$this->validate($request, [
			'Id' => 'required| integer',
		], [
			'Id.required' => 'Id为空',
			'Id.integer' => 'Id必须是数字',
		]);
		
		$data = $request->all();
		$call_spu = new \App\Libs\wrapper\Spu();
		$return = $call_spu->sku_update($data);
		if($return === 1) {
			$this->back('编辑成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 删除sku
	 * @param Id
	 */
	public function sku_del(Request $request){
		$this->validate($request, [
			'Id' => 'required| integer',
		], [
			'Id.required' => 'Id为空',
			'Id.integer' => 'Id必须是数字',
		]);
		
		$data = $request->all();
		$call_spu = new \App\Libs\wrapper\Spu();
		$return = $call_spu->sku_del($data);
//		var_dump($return);exit;
		if($return === 1) {
			$this->back('删除成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	// 测试
	public function test(Request $request){
//		require_once 'F:\www\zity\zity\app\Libs\phpspreadsheet\src\PhpSpreadsheet\Spreadsheet.php';
		
		//// 读取Excel
		$spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
		// 1获取活动工作薄
		$sheet = $spreadsheet->getActiveSheet();
		// 2获取单元格
		$cell = $sheet->getCell('A1');   //方法1
		$cell = $sheet->getCellByColumnAndRow(1,1); //方法2
		
		
		# 获取总列数
		$highestColumn = $sheet->getHighestColumn();
		# 列数 改为数字显示
		$highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);
		# 获取总行数
		$highestRow = $sheet->getHighestRow();
		
		//// 获取数据
		$add_list = [];
		for($a=2; $a<$highestRow; $a++){
			//行
			for ($b=1; $b <= $highestColumnIndex; $b++) {
				//列  列，行
				$key = $sheet->getCellByColumnAndRow($b, 1)->getValue();
				$val = $sheet->getCellByColumnAndRow($b, $a)->getValue();
				$gva = $sheet->getCellByColumnAndRow($b, $a)->getFormattedValue();
				
				//判断键名是否正确
				$array_key=array('大类名称', '大类标识', '二类名称', '二类标识', '三类名称', '三类标识');
				if(!in_array($key, $array_key)){
					var_dump('Excel数据格式不对,请下载Excel模板进行导入');exit;
				}
				
				if(empty($val)){
					break;
				}
				
				$data_key = $a - 2;
				if($key=='大类名称'){
					$add_list[$data_key]['name_1'] = $val;
				}else if($key=='大类标识'){
					$add_list[$data_key]['identifying_1'] = $val;
				}else if($key=='二类名称'){
					$add_list[$data_key]['name_2'] = $val;
				}else if($key=='二类标识'){
					$add_list[$data_key]['identifying_2'] = $val;
				}else if($key=='三类名称'){
					$add_list[$data_key]['name_3'] = $val;
				}else if($key=='三类标识'){
					$add_list[$data_key]['identifying_3'] = $val;
				}
			}
		}
		
		var_dump($add_list);exit;
		
	}
	
	
	
	
	
	public function test1(Request $request){
//		require_once 'F:\www\zity\zity\app\Libs\phpspreadsheet\src\PhpSpreadsheet\Spreadsheet.php';
		
		//// 读取Excel
		$file = $_FILES['file'];
//		$spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
		$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file['tmp_name']);
		# 获取活动工作薄
		$sheet = $spreadsheet->getActiveSheet();
		# 获取总列数
		$highestColumn = $sheet->getHighestColumn();
		# 列数 改为数字显示
		$highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);
		# 获取总行数
		$highestRow = $sheet->getHighestRow();
		
		//// 获取数据
		$add_list = [];
		for($a=2; $a<$highestRow; $a++){
			//行
			for ($b=1; $b <= $highestColumnIndex; $b++) {
				//列  列，行
				$key = $sheet->getCellByColumnAndRow($b, 1)->getValue();
				$val = $sheet->getCellByColumnAndRow($b, $a)->getValue();
				$gva = $sheet->getCellByColumnAndRow($b, $a)->getFormattedValue();
				
				//判断键名是否正确
				$array_key=array('大类名称', '大类标识', '二类名称', '二类标识', '三类名称', '三类标识');
				if(!in_array($key, $array_key)){
					var_dump('Excel数据格式不对,请下载Excel模板进行导入');exit;
				}
				
				if(empty($val)){
					break;
				}
				
				$data_key = $a - 2;
				if($key=='大类名称'){
					$add_list[$data_key]['name_1'] = $val;
				}else if($key=='大类标识'){
					$add_list[$data_key]['identifying_1'] = $val;
				}else if($key=='二类名称'){
					$add_list[$data_key]['name_2'] = $val;
				}else if($key=='二类标识'){
					$add_list[$data_key]['identifying_2'] = $val;
				}else if($key=='三类名称'){
					$add_list[$data_key]['name_3'] = $val;
				}else if($key=='三类标识'){
					$add_list[$data_key]['identifying_3'] = $val;
				}
			}
		}
		
		var_dump($add_list);exit;
		
	}
	
	//获取店铺分类
	public function main_classification(Request $request){
        $data = $request->all();
        $call_spu = new \App\Libs\wrapper\Spu();
        $return = $call_spu->category_main($data);//category_main
//		var_dump($return);exit;
        if(is_array($return)) {
            $this->back('获取成功', '200',$return);
        }else{
            $this->back($return);
        }
    }



	
	//获取lazada字段数据
	public  function data_lzd(Request $request){
		$data = $request->all();
		$call_spu = new \App\Libs\wrapper\Spu();
		$return = $call_spu->data_lzd($data);
		if(is_array($return)) {
			//数据处理
			$this->back('获取成功', '200',$return);
		}else{
			$this->back($return);
		}
	}
	
	//获取lazada平台品牌
	public function brand_lzd(Request $request){
		$data = $request->all();
		$call_spu = new \App\Libs\wrapper\Spu();
		$return = $call_spu->brand_lzd($data);
		if(is_array($return)) {
			//数据处理
			$this->back('获取成功', '200',$return);
		}else{
			$this->back($return);
		}
	}
	
	
	//上传到lazada平台
	public function create_lzd(Request $request){
		$data = $request->all();
		$call_spu = new \App\Libs\wrapper\Spu();
		$return = $call_spu->create_lzd($data);
//        var_dump($return);exit;
		if($return === 1) {
			$this->back('上传成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	//获取所有店铺
	public function spu_store(Request $request){
		$data = $request->all();
		$call_spu = new \App\Libs\wrapper\Spu();
		$return = $call_spu->spu_store($data);
		if(is_array($return['list'])) {
			//数据处理
			$this->back('获取成功', '200', $return);
		}else{
			$this->back($return);
		}
	}
	
	//刷新lazada平台分类
	public function category_lzd(Request $request){
		$data = $request->all();
		$call_spu = new \App\Libs\wrapper\Spu();
		$return = $call_spu->category_lzd($data);
//		var_dump($return);exit;
		if(is_array($return)) {
			$this->back('获取成功', '200',$return);
		}else{
			$this->back($return);
		}
	}
	//刷新vova平台分类
	public function category_vova(Request $request){
		$data = $request->all();
		$call_spu = new \App\Libs\wrapper\Spu();
		$return = $call_spu->category_vova($data);
		if(is_array($return)) {
			//数据处理
			$this->back('获取成功', '200',$return);
		}else{
			$this->back($return);
		}
	}
	
	//获取vova字段数据
	public  function data_vova(Request $request){
		$data = $request->all();
		$call_spu = new \App\Libs\wrapper\Spu();
		$return = $call_spu->data_vova($data);
		if(is_array($return)) {
			//数据处理
			$this->back('获取成功', '200',$return);
		}else{
			$this->back($return);
		}
	}

    /*
	 * 产品上传--vova
	 * */
    public function create_vova(Request $request){
        $data = $request->all();
        $call_spu = new \App\Libs\wrapper\Spu();
        $return = $call_spu->create_vova($data);
//        var_dump($return);exit;
        if($return === 1) {
            $this->back('上传成功', '200');
        }else{
            $this->back($return);
        }
    }
	
	//获取wish字段数据
	public  function data_wish(Request $request){
		$data = $request->all();
		$call_spu = new \App\Libs\wrapper\Spu();
		$return = $call_spu->data_wish($data);
		if(is_array($return)) {
			//数据处理
			$this->back('获取成功', '200',$return);
		}else{
			$this->back($return);
		}
	}
	
	/*
	 * 上传产品--wish
	 * */
	public function create_wish(Request $request){
		$data = $request->all();
		$call_spu = new \App\Libs\wrapper\Spu();
		$return = $call_spu->create_wish($data);
		if($return===1) {
			$this->back('上传成功', '200');
		}else{
			$this->back($return);
		}
	}

	public function spu_rollback(Request $request){
        $data = $request->all();
        ////权限验证
        $arr = array();
        $arr['user_id'] = $data['user_info']['Id'];
        $arr['identity'] = array('spu_rollback');
        $powers = $this->powers($arr);
        $data['list_all'] = $powers['spu_rollback'];
        $call_spu = new \App\Libs\wrapper\Spu();
        $return = $call_spu->spu_rollback($data);
        if($return ===1){
            $this->back('回退成功','200');
        }else{
            $this->back($return);
        }
    }



    //测试接口
	public function lzd_croup(Request $request){
		$data = $request->all();
		$call_spu = new \App\Libs\wrapper\Spu();
		$return = $call_spu->lzd_croup($data);
		if($return===1) {
			//数据处理
			$this->back('上传成功', '200');
		}else{
			$this->back('上传失败', '400',$return);
		}
	}


    //测试vova平台尺码接口
    public function vova_size(Request $request){
        $data = $request->all();
        $call_spu = new \App\Libs\wrapper\Spu();
        $return = $call_spu->vova_size($data);
        if($return===1) {
            //数据处理
            $this->back('上传成功', '200');
        }else{
            $this->back('上传失败', '400',$return);
        }
    }

    /**
     * @Desc:获取spu绑定的供应商
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/9/11 17:59
     */
	public function getSpuBindSuppliers()
    {
        $params = $this->request->all();
        $result = $this->model->getSpuBindSuppliers($params);

        return $this->back($result['msg'] ?? '未知错误', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc:保存报价需求
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/9/11 17:59
     */
	public function saveSelfSpuQuotation()
    {
        $params = $this->request->all();
        $result = $this->model->saveSelfSpuQuotation($params);

        return $this->back($result['msg'] ?? '未知错误', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc:保存spu报价
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/9/11 18:00
     */
	public function spuQuotation()
    {
        $params = $this->request->all();
        $result = $this->model->spuQuotation($params);

        return $this->back($result['msg'] ?? '未知错误', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc:获取报价记录列表
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/9/11 18:00
     */
	public function getSpuQuotationLogList()
    {
        $params = $this->request->all();
        $result = $this->model->getSpuQuotationLogList($params);

        return $this->back($result['msg'] ?? '未知错误', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc:获取报价需求列表
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/9/11 18:00
     */
    public function getSpuQuotationList()
    {
        $params = $this->request->all();
        $result = $this->model->getSpuQuotationList($params);

        return $this->back($result['msg'] ?? '未知错误', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc:删除spu报价需求
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/9/11 18:00
     */
    public function deleteSpuQuotationDemand()
    {
        $params = $this->request->all();
        $result = $this->model->deleteSpuQuotationDemand($params);

        return $this->back($result['msg'] ?? '未知错误', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc:确认报价完成
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/9/13 9:18
     */
    public function confirmQuotationCompletion()
    {
        $params = $this->request->all();
        $result = $this->model->confirmQuotationCompletion($params);

        return $this->back($result['msg'] ?? '未知错误', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc: 获取打样需求列表
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/9/13 9:21
     */
    public function getSpuProofingList()
    {
        $params = $this->request->all();
        $result = $this->model->getProofingDemandList($params);

        return $this->back($result['msg'] ?? '未知错误', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc:保存打样需求
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/9/15 14:10
     */
    public function saveProofingDemand()
    {
        $params = $this->request->all();
        $result = $this->model->saveProofingDemand($params);

        return $this->back($result['msg'] ?? '未知错误', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc:spu打样
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/9/15 14:10
     */
    public function spuProofing()
    {
        $params = $this->request->all();
        $result = $this->model->spuProofing($params);

        return $this->back($result['msg'] ?? '未知错误', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc:获取打样记录列表
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/9/11 18:00
     */
    public function getSpuProofingLogList()
    {
        $params = $this->request->all();
        $result = $this->model->getSpuProofingLogList($params);

        return $this->back($result['msg'] ?? '未知错误', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc:删除spu打样需求
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/9/11 18:00
     */
    public function deleteSpuProofingDemand()
    {
        $params = $this->request->all();
        $result = $this->model->deleteSpuProofingDemand($params);

        return $this->back($result['msg'] ?? '未知错误', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc: 确认打样完成
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/9/15 14:10
     */
    public function confirmProofingCompletion()
    {
        $params = $this->request->all();
        $result = $this->model->confirmProofingCompletion($params);

        return $this->back($result['msg'] ?? '未知错误', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc:发布报价任务
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/10/11 15:03
     */
    public function publishQuotationTask()
    {
        $params = $this->request->all();
        $result = $this->model->publishQuotationTask($params);

        return $this->back($result['msg'] ?? '未知错误', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc:发布打样任务
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/10/11 15:03
     */
    public function publishProofingTask()
    {
        $params = $this->request->all();
        $result = $this->model->publishProofingTask($params);

        return $this->back($result['msg'] ?? '未知错误', $result['code'] ?? 500, $result['data'] ?? []);
    }
}//类结束