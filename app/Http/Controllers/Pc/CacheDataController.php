<?php

namespace App\Http\Controllers\Pc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CacheDataModel;
use Illuminate\Support\Facades\DB;

class CacheDataController extends Controller
{
    private $model;
    private $request;

    public function __construct(Request $request)
    {
        $this->model = new CacheDataModel();
        $this->request = $request;
    }

    /**
     * 获取库存缓存
     */
    public function GetCacheInventory()
    {

        $params = $this->request->all();

        $result = $this->model->GetCacheInventoryM($params);

        return $this->Ts_back($result);
    }

    /**
     * 获取spu出库周转天数排行
     */
    public function SpuRank()
    {

        $params = $this->request->all();

        $result = $this->model->SpuRankM($params);

        return $this->Ts_back($result);
    }

        /**
     * 获取spu出库周转天数排行
     */
    public function SpuColorRank()
    {

        $params = $this->request->all();

        $result = $this->model->SpuColorRank($params);

        return $this->Ts_back($result);
    }



            /**
     * 获取spu出库周转天数排行
     */
    public function CustomSkuRank()
    {

        $params = $this->request->all();

        $result = $this->model->CustomSkuRank($params);

        return $this->Ts_back($result);
    }
    /**
     * 获取库存缓存
     */
    public function GetCacheInventoryb()
    {

        $params = $this->request->all();

        $result = $this->model->GetCacheInventoryMb($params);

        return $this->Ts_back($result);
    }

       /**
     * 设置库存缓存-任务
     */
    public function SetCacheInventoryTask()
    {

        $params = $this->request->all();

        $result = $this->model->SetCacheInventoryTaskM($params);

        return $this->Ts_back($result);
    }


     /**
     * 查询订单列表
     */
    public function GetOrderList()
    {

        $params = $this->request->all();

        $result = $this->model->GetOrderListM($params);

        return $this->Ts_back($result);
    }
    

   /**
     * 设置库存缓存
     */
    public function SetCacheInventory()
    {

        $params = $this->request->all();

        $result = $this->model->SetCacheInventoryM($params);

        return $this->Ts_back($result);
    }


    
   
    public function Ts_back($params){

        if($params['type']=='success'){
            return $this->back('获取成功', '200', $params['data']);
        }else{
            return $this->back($params['msg'], '300');
        }
    }

    public function getWarehousePlatformInOrOutNumList()
    {
        $params = $this->request->all();

        $result = $this->model->getWarehousePlatformInOrOutNumList($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500, $result['data'] ?? []);
    }
}