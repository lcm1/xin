<?php

namespace App\Http\Controllers\Pc;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class HuilvController extends Controller
{
    /**
     * 新增汇率
     * @param name 名称
     * @param name_yw 英文名
     * @logic 名称唯一
     */
    public function huilv_add(Request $request)
    {
        $data = $request->all();

        /**
         * 防错机制
         */

        $HuilvLast = \App\Models\Huilv::where('currency', $data['currency'])->orderBy('id', 'desc')->first();
        if ($HuilvLast == null) {   //第一条数据需要审核
            $add = array();
            $add['currency'] = $data['currency'];
            $add['dollar_rate'] = $data['dollar_rate'];
            $add['add_time'] = time();
            $add['add_date'] = date("Ymd");
            $add['stat'] = 0; //不启用
            $id = \App\Models\Huilv::insertGetId($add);
        } else {
            $HuilvLast->toArray();
            if ((abs(($data['dollar_rate'] - $HuilvLast['dollar_rate'])) / $HuilvLast['dollar_rate']) > 0.3) {
                $this->back('汇率错误');
            }
            $add = array();
            $add['currency'] = $data['currency'];
            $add['dollar_rate'] = $data['dollar_rate'];
            $add['add_time'] = time();
            $add['add_date'] = date("Ymd");
            $add['stat'] = 1;  //启用
            $id = \App\Models\Huilv::insertGetId($add);
        }

        if ($id > 0) {
            $this->back('新增成功', '200');
        } else {
            $this->back($id);
        }

    }

    /**
     * 国家列表--数据
     * @param name 名称
     * @param name_yw 英文名
     */
    public function huilv_list(Request $request)
    {
        $data = $request->all();
        if (empty($data)) {
            $this->back('接收值为空');
        }
        $HuilvCom = new \App\Libs\wrapper\Huilv();
        $return = $HuilvCom->huilv_list($data);

        if (!empty($return['list'])) {
            foreach ($return['list'] as &$item) {
                $item['power'] = 0;
                $item['add_date_time'] = date('Y-m-d H:i:s', $item['add_time']);
                if ($item['stat'] != 1) {  //显示权限
                    $userinfo = session('userinfo.Id');
                    $arr = array();
                    $arr['user_id'] = $data['user_info']['Id'];
                    $arr['identity'] = array('huilv_update_stat');
                    $powers = $this->powers($arr);
                    if (isset($powers['huilv_update_stat']) && $powers['huilv_update_stat'] == true) {
                        $item['power'] = 1;
                    }
                }
            }
        }


        if (is_array($return['list'])) {

            $this->back('获取成功', '200', $return);
        } else {
            $this->back($return);
        }
    }


    /**
     * 编辑国家
     * @param Id
     * @parma name 名称
     * @param name_yw 英文名
     * @logic 名称唯一
     */
    public function huilv_update(Request $request)
    {

        $data = $request->all();

        /**
         * 防错机制
         */
        $HuilvLast = \App\Models\Huilv::where('currency', $data['currency'])->where('id', '<', $data['id'])->orderBy('id', 'desc')->first();

        if ($HuilvLast == null) {  //修改第一条汇率，需要审核
            $update = array();
            $update['dollar_rate'] = $data['dollar_rate'];
            $update['stat'] = 0;
        } else {
            if (isset($HuilvLast['dollar_rate'])) {
                if ((abs(($data['dollar_rate'] - $HuilvLast['dollar_rate'])) / $HuilvLast['dollar_rate']) > 0.3) {
                    $this->back('汇率错误');
                }
            }
            $update = array();
            $update['dollar_rate'] = $data['dollar_rate'];
        }

        $updateResult = \App\Models\Huilv::where('id', $data['id'])->update($update);

        if ($updateResult > 0) {
            $this->back('编辑成功', '200');
        } else {
            $this->back($updateResult);
        }

    }

    /**
     * 审核第一条汇率数据
     * @param Request $request
     */
    public function update_stat(Request $request)
    {

        $data = $request->all();
        $HuilvHas = \App\Models\Huilv::where('id', $data['id'])->first();
        if ($HuilvHas == null) {
            $this->back('数据不存在');
        }
        $HuilvLast = \App\Models\Huilv::where('currency', $HuilvHas['currency'])->where('id', '<', $HuilvHas['id'])->orderBy('id', 'desc')->first();
        if ($HuilvLast == null) {  //只能审核第一条汇率数据
            $update = array();
            $update['stat'] = 1;
            $updateResult = \App\Models\Huilv::where('id', $data['id'])->update($update);
            if ($updateResult > 0) {
                $this->back('编辑成功', '200');
            } else {
                $this->back($updateResult);
            }
        } else {
            $this->back('只能修改第一条汇率状态，其他都是启用状态');
        }

    }
}
