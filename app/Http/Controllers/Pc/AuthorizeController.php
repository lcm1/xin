<?php
namespace App\Http\Controllers\Pc;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon; //时间
use App\Http\Controllers\Controller;

class AuthorizeController extends Controller{
    //店铺授权数据
    public function authorize_list(Request $request){
        $data = $request->all();
        //var_dump($data);die;
        ////权限验证
        $arr = array();
        $arr['user_id'] = $data['user_id'];
        $arr['identity'] = array('store_authorize');
        $powers = $this->powers($arr);
        if(!$powers['store_authorize']){
            $this->back('无该权限');
        }
        $call_authorize = new \App\Libs\wrapper\Authorize();
        $return = $call_authorize->authorize_list($data);
        if(is_array($return)){
            $this->back('获取成功', '200',$return);
        }else{
            $this->back($return);
        }
    }
    //店铺授权跳转地址
    public function authorize_website(Request $request){
        $data = $request->all();
        //var_dump($data);die;
        ////权限验证
        $arr = array();
        $arr['user_id'] = $data['user_id'];
        $arr['identity'] = array('store_authorize');
        $powers = $this->powers($arr);
        if(!$powers['store_authorize']){
            $this->back('无该权限');
        }
        $call_authorize = new \App\Libs\wrapper\Authorize();
        $return = $call_authorize->authorize_website($data);
        if($return){
            //echo "111";die;
            $this->back('获取成功', '200',$return);
        }elseif($return===1){
            //echo "22";die;
            $this->back('授权成功', '200');
        }else{
            //echo "333";die;
            //var_dump('11');die;
            $this->back($return);
        }
    }

    //店铺授权回调信息接收
    public function authorize_code(Request $request){
        //echo "11";die;
        $data = $request->all();
        $call_authorize = new \App\Libs\wrapper\Authorize();
        $return = $call_authorize->authorize_code($data);
        if($return==1){
            echo "<p>授权成功</p>";
            //$this->back('授权成功', '200');
        }else{
            $this->back('授权失败');
        }
    }

    public function authorize_status(Request $request){
        $data = $request->all();
        $call_authorize = new \App\Libs\wrapper\Authorize();
        $return = $call_authorize->authorize_status($data);
        if(is_array($return)){
            $this->back('获取成功', '200',$return);
        }else{
            $this->back($return);
        }
    }


    public function authorize_test(Request $request){
        $data = $request->all();
        $call_authorize = new \App\Libs\wrapper\Authorize();
        $return = $call_authorize->authorize_test($data);
        if(is_array($return)){
            $this->back('获取成功', '200',$return);
        }else{
            $this->back($return);
        }
    }












}