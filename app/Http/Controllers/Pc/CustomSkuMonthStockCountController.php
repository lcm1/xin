<?php

namespace App\Http\Controllers\Pc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CustomSkuMonthStockCount;

class CustomSkuMonthStockCountController extends Controller
{
    protected $request;
    protected $model;
    public function __construct()
    {
        $this->request = new Request();
        $this->model = new CustomSkuMonthStockCount();
    }

    public function getCustomSkuMonthStockCountList(Request $request)
    {
        $params = $request->all();

        $result = $this->model->getCustomSkuMonthStockCountList($params);

        if (empty($result)){
            return $this->back('获取失败！');
        }

        return $this->back('获取成功', 200, $result);
    }
}