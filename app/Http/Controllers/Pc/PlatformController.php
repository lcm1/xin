<?php
namespace App\Http\Controllers\Pc;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class PlatformController extends Controller{
//////////////////////////////////////////////// 平台
////////////////////// 平台列表
	/**
	 * 平台列表--数据
	 * @param name 名称
	 * @param identifying 标识
	 */
	public function platform_list(Request $request){
		$data = $request->all();
//		if(empty($data)){
//			$this->back('接收值为空');
//		}
		
		$call_platform = new \App\Libs\wrapper\Platform();
		$return = $call_platform->platform_list($data);
//		var_dump($return);exit;
		if(is_array($return['list'])) {
			$this->back('获取成功', '200', $return);
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 新增平台
	 * @param name 名称
	 * @param identifying 标识
	 * @logic 名称、标识唯一
	 */
	public function platform_add(Request $request){
		$this->validate($request, [
			'name' => 'required| string',
			'identifying' => 'required| string',
		], [
			'name.required' => 'name为空',
			'name.string' => 'name必须是字符串',
			'identifying.required' => 'identifying为空',
			'identifying.string' => 'identifying必须是字符串',
		]);
		
		$data = $request->all();
		$call_platform = new \App\Libs\wrapper\Platform();
		$return = $call_platform->platform_add($data);
//		var_dump($return);exit;
		if($return === 1) {
			$this->back('新增成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 编辑平台
	 * @param Id
	 * @parma name 名称
	 * @param identifying 标识
	 * @param identifying_log 旧标识
	 * @logic 名称、标识唯一
	 */
	public function platform_update(Request $request){
		$this->validate($request, [
			'Id' => 'required| integer',
		], [
			'Id.required' => 'Id为空',
			'Id.integer' => 'Id必须是数字',
		]);
		
		$data = $request->all();
		$call_platform = new \App\Libs\wrapper\Platform();
		$return = $call_platform->platform_update($data);
		if($return === 1) {
			$this->back('编辑成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 删除平台
	 * @param Id
	 */
	public function platform_del(Request $request){
		$this->validate($request, [
			'Id' => 'required| integer',
		], [
			'Id.required' => 'Id为空',
			'Id.integer' => 'Id必须是数字',
		]);
		
		$data = $request->all();
		$call_platform = new \App\Libs\wrapper\Platform();
		$return = $call_platform->platform_del($data);
//		var_dump($return);exit;
		if($return === 1) {
			$this->back('删除成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	





}//类结束