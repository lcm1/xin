<?php
namespace App\Http\Controllers\Pc;

use Illuminate\Http\Request;
use App\Models\Announcement;
use App\Http\Controllers\Controller;

class AnnouncementController extends Controller{

    private $model;
    private $request;
    private $result;
    private $msg;
    private $code = 200;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->model = new Announcement();
        $this->request = $request;
    }
    function getAnnouncement(){
        $this->result = $this->model->getAnnouncement();

        $this->msg = $this->result ? '获取成功' : '获取失败';
        $this->code = $this->result ? 200 : 500;
    }
    function getAnnouncementList(){
        $data = $this->request->all();
        $timeArr = $this->getThisWeek();
        $data['start'] = $timeArr['start'];
        $data['end'] = $timeArr['end'];
        $result = $this->model->announcementList($data);

        return $this->back($result['msg'] ?? '获取失败！', $result['code'] ?? '500！', $result['data'] ?? []);
    }
    function addAnnouncement(){
        $data = $this->request->all();
        $result = $this->model->announcementAdd($data);

        return $this->back($result['msg'] ?? '新增失败！', $result['code'] ?? '500！', $result['data'] ?? []);
    }
    function editAnnouncement(){
        $data = $this->request->all();
        $result = $this->model->announcementUpdate($data);
        return $this->back($result['msg'] ?? '更新失败！', $result['code'] ?? '500！', $result['data'] ?? []);
    }
    function delAnnouncement(){
        $data = $this->request->all();
        $result = $this->model->announcementDel($data);
        return $this->back($result['msg'] ?? '删除失败！', $result['code'] ?? '500！', $result['data'] ?? []);
    }
//    public function __destruct() {
//        // TODO: Implement __destruct() method.
//        return $this->back($this->msg, $this->code, $this->result);
//    }

    /**
     * 获取本周起止日期
     * @return array
     */
    function getThisWeek(){
        $date = date("Y-m-d H:i:s");
        $w=date('w',strtotime($date));
        $week_start=date('Y-m-d H:i:s',strtotime("$date -".($w ? $w - 1 : 6).' days'));
        $week_end=date('Y-m-d H:i:s',strtotime("$week_start +6 days"));
        $week_end=date('Y-m-d H:i:s',strtotime("$date -6 days"));
        return ['start' => $week_end,'end' => $date];
    }

}