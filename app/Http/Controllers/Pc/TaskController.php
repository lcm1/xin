<?php

namespace App\Http\Controllers\Pc;

use App\Models\TaskClass;
use App\Models\TaskClassModule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\TaskModel;

class TaskController extends Controller
{

    private $model;
    private $request;

    public function __construct(Request $request)
    {
        $this->model = new TaskModel();
        $this->request = $request;
    }
//////////////////////////////////////////////// 任务
////////////////////// 新增任务页面
    /**
     * 用户列表--数据
     */
    public function user_list()
    {
        // 状态正常
        $sql = "select `Id`, `account`, `phone`, `heard_img`
					from users
					where `state`=1";
        $list = json_decode(json_encode(db::select($sql)), true);

        if (is_array($list)) {
            $this->back('获取成功', '200', $list);
        } else {
            $this->back('获取失败');
        }
    }

    /**
     * 部门用户列表--数据
     */
    public function department_user()
    {

        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->department_user();

        if (is_array($return)) {
            $this->back('获取成功', '200', $return);
        } else {
            $this->back('获取失败');
        }
    }

    /**
     * @Desc:获取部门人员列表
     * @param $params
     * @return array
     * @author: Liu Sinian
     * @Time: 2023/2/28 18:26
     */
    public function getDepartmentList(Request $request)
    {
        $request = $request->all();
        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->getDepartmentList($request);

        if (is_array($return)) {
            $this->back('获取成功', '200', $return);
        } else {
            $this->back('获取失败');
        }
    }

    /**
     * 任务模板列表
     */
    public function template_list(Request $request)
    {
        $data = $request->all();

        // 获取所有模板
        $sql = "select `Id`, `name`, `type_mb`
					from tasks
					where `type`=2";
        $list = json_decode(json_encode(db::select($sql)), true);

        $tasks_model = DB::table('tasks_model')->get()->toArray();
        foreach ($tasks_model as $k=>$t){
            $arr[$k+1]['value'] = $t->id;
            $arr[$k+1]['label'] = $t->name;
            $arr[$k+1]['children'] = array();
        }
        // 处理数据


        foreach ($list as $v) {
            $aa = array();
            $aa['value'] = $v['Id'];
            $aa['label'] = $v['name'];

            $arr[($v['type_mb'])]['children'][] = $aa;
        }

        if (is_array($arr)) {
            $this->back('获取成功', '200', $arr);
        } else {
            $this->back('获取失败');
        }
    }

    /**
     * 新增任务
     * @parma name 任务名称
     * @param user_fz 负责人
     * @param file_fj 附件
     * @param desc 任务描述
     * @param examine 审核人  数组
     * @param user_cs 抄送人  数组
     * @param node 节点、节点任务   数组
     *              name 节点名称
     *              node_task_data 节点任务     数组
     *                      user_id 执行人
     *                      day 预计完成天数
     *                      task 任务
     */
    public function task_add(Request $request)
    {
        $this->validate($request, [
            'name' => 'required| string',
            'user_fz' => 'required| integer',
        ], [
            'name.required' => 'name为空',
            'name.string' => 'name必须是字符串',
            'user_fz.required' => 'user_fz为空',
            'user_fz.integer' => 'user_fz必须是字符串',
        ]);

        $data = $request->all();
        $data['user_cj'] = $data['user_info']['Id'];
        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->task_add($data);
//		var_dump($return);exit;
        if (isset($return['type']) && $return['type'] =="success") {
            $this->back('新增成功', '200',$return);
        } else {
            $this->back($return);
        }
    }

    public function abnormal_task_add(Request $request){
        $data = $request->all();
    }

////////////////////// 任务列表

    /**
     * 任务列表--数据
     * @param name 任务名称
     * @param user_fz_name 负责人名称
     * @param state 状态：1.待审核、2.已拒绝、3.进行中、4.待验收、5.已结束
     * @param type 类型：1.任务、2.任务模板
     * @param create_time 创建时间
     */
    public function task_list(Request $request)
    {
        $data = $request->all();
        if (empty($data)) {
            $this->back('接收值为空');
        }

        ////权限验证
        $arr = array();
        $arr['user_id'] = $data['user_info']['Id'];
        $arr['identity'] = array('task_list_all');
        $powers = $this->powers($arr);
        $data['list_all'] = $powers['task_list_all'];

        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->task_list($data);
        if (is_array($return['list'])) {
            $this->back($return['msg'] ?? '获取失败', $return['code'] ?? 500, $return);
        } else {
            $this->back($return);
        }
//        return $this->back($return['msg'] ?? '未知错误！', $return['code'] ?? 500, $return['data'] ?? []);
    }

    /**
     * 任务审核详情
     * @param task_id 任务id
     */
    public function tasks_examine_info(Request $request)
    {
        $this->validate($request, [
            'task_id' => 'required| integer',
        ], [
            'task_id.required' => 'task_id为空',
            'task_id.integer' => 'task_id必须是数字',
        ]);

        $data = $request->all();
        $sql = "select te.*, u.account, u.heard_img
					from tasks_examine te
					inner join users u on u.Id = te.user_id
					where task_id = {$data['task_id']}";
        $list = json_decode(json_encode(db::select($sql)), true);
        if (is_array($list)) {
            $this->back('获取成功', '200', $list);
        } else {
            $this->back('获取失败');
        }
    }

    /*
     * 删除任务消息
     * @param Id 任务id
     */
    public function task_del(Request $request)
    {
        $this->validate($request, [
            'Id' => 'required| integer',
        ], [
            'Id.required' => 'Id为空',
            'Id.integer' => 'Id必须是数字',
        ]);

        $data = $request->all();
        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->task_del($data);
        if ($return == 1) {
            $this->back('删除成功', '200');
        } else {
            $this->back($return);
        }
    }

    /**
     * 任务审核
     * @parma Id 任务审核id
     * @param task_id 任务id
     * @param desc 审核描述
     * @param state 状态：1.待审核、2.已同意、3.已拒绝
     */
    public function tasks_examine(Request $request)
    {
        $this->validate($request, [
            'Id' => 'required| integer',
            'task_id' => 'required| integer',
            'state' => 'required| integer',
            'user_id' => 'integer',
        ], [
            'Id.required' => 'Id为空',
            'Id.integer' => 'Id必须是数字',
            'user_id.integer' => 'user_id必须是数字',
            'task_id.required' => 'task_id为空',
            'task_id.integer' => 'task_id必须是数字',
            'state.required' => 'state为空',
            'state.integer' => 'state必须是数字',
        ]);

        $data = $request->all();
        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->tasks_examine($data);
//		var_dump($return);exit;
        if ($return === 1) {
            $this->back('审核成功', '200');
        } else {
            $this->back($return);
        }
    }

////////////////////// 任务详情

    /**
     * 任务详情
     * @param task_id 任务id
     */
    public function task_info(Request $request)
    {
        $this->validate($request, [
            'task_id' => 'required| integer',
        ], [
            'task_id.required' => 'task_id为空',
            'task_id.integer' => 'task_id必须是数字',
        ]);

        $data = $request->all();
        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->task_info($data);
//		var_dump($return);exit;
        if (is_array($return)) {
            $this->back('获取成功', '200', $return);
        } else {
            $this->back($return);
        }
    }

    /**
     * 编辑节点任务
     * @param Id 节点任务id
     */
    public function node_task_update(Request $request)
    {
        $this->validate($request, [
            'Id' => 'required| integer',
        ], [
            'Id.required' => 'Id为空',
            'Id.integer' => 'Id必须是数字',
        ]);

        $data = $request->all();
        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->node_task_update($data);
//		var_dump($return);exit;
        if (is_array($return)) {
            $this->back('编辑成功', '200', $return);
        } else {
            $this->back($return);
        }
    }

    /**
     * 节点任务--（完成、放弃）
     * @param Id 节点任务id
     * @param state 状态：1.未开始、2.进行中、3.已完成、4.已放弃
     * @param feedback_type 反馈类型：1.文字、2.图片、3.文件
     * @param feedback_content 反馈内容
     */
    public function task_son_complete(Request $request)
    {
        $this->validate($request, [
            'Id' => 'required| integer',
            'state' => 'required| integer',
            'feedback_type' => 'required| integer',
        ], [
            'Id.required' => 'Id为空',
            'Id.integer' => 'Id必须是数字',
            'state.required' => 'state为空',
            'state.integer' => 'state必须是数字',
            'feedback_type.required' => 'feedback_type为空',
            'feedback_type.integer' => 'feedback_type必须是数字',
        ]);

        $data = $request->all();
        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->task_son_complete($data);
//		var_dump($return);exit;
        if ($return === 1) {
            $this->back('操作成功', '200');
        } else {
            $this->back($return);
        }
    }

    /**
     * 编辑任务
     * @parma Id 任务id
     * @param user_fz 负责人
     * @Parma desc 任务描述
     */
    public function task_update(Request $request)
    {
        $this->validate($request, [
            'Id' => 'required| integer',
            'user_fz' => 'required| integer',
        ], [
            'Id.required' => 'Id为空',
            'Id.integer' => 'Id必须是数字',
            'user_fz.required' => 'user_fz为空',
            'user_fz.integer' => 'user_fz必须是数字',
        ]);

        $data = $request->all();
        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->task_update($data);
        if ($return == 1) {
            $this->back('编辑成功', '200');
        } else {
            $this->back($return);
        }

    }

    /**
     * 任务消息
     * @param task_id 任务id
     */
    public function tasks_news_list(Request $request)
    {
        $this->validate($request, [
            'task_id' => 'required| integer',
        ], [
            'task_id.required' => 'task_id为空',
            'task_id.integer' => 'task_id必须是数字',
        ]);

        $data = $request->all();
        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->tasks_news_list($data);
        if (is_array($return)) {
            $this->back('获取成功', '200', $return);
        } else {
            $this->back($return);
        }
    }

    /*
     * 新增任务消息
     * @param task_id 任务id
     * @param task_name 任务名称
     * @param user_js 接收人
     * @param type 类型：1.文字、2.图片、3.文件
     * @param content 内容
     */
    public function task_new_add(Request $request)
    {
        $this->validate($request, [
            'task_id' => 'required| integer',
        ], [
            'task_id.required' => 'task_id为空',
            'task_id.integer' => 'task_id必须是数字',
        ]);

        $data = $request->all();
        $data['user_fs'] = $data['user_info']['Id'];

        if (!isset($data['send_type']) || empty($data['send_type'])){
            $data['send_type'] = 1;//1站内信  2站内信且发qq邮箱
        }
        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->task_new_add($data);
//		var_dump($return);exit;
        if ($return == 1) {
            $this->back('发送成功', '200');
        } else {
            $this->back($return);
        }
    }

    /*
     * 删除任务消息
     * @param Id 任务消息id
     */
    public function task_new_del(Request $request)
    {
        $this->validate($request, [
            'Id' => 'required| integer',
        ], [
            'Id.required' => 'Id为空',
            'Id.integer' => 'Id必须是数字',
        ]);

        $data = $request->all();
        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->task_new_del($data);
//		var_dump($return);exit;
        if ($return == 1) {
            $this->back('删除成功', '200');
        } else {
            $this->back($return);
        }
    }

    /**
     * 新增节点任务
     */
    public function node_task_add(Request $request)
    {
        $data = $request->all();
        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->node_task_add($data);
//		var_dump($return);exit;
        if ($return == 1) {
            $this->back('新增成功', '200');
        } else {
            $this->back($return);
        }

    }

    /**
     * 验收任务
     * @param task_id 任务id
     */
    public function task_determine(Request $request)
    {
        $this->validate($request, [
            'task_id' => 'required| integer',
        ], [
            'task_id.required' => 'task_id为空',
            'task_id.integer' => 'task_id必须是数字',
        ]);

        $data = $request->all();
        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->task_determine($data);
//		var_dump($return);exit;
        if ($return == 1) {
            $this->back('验收成功', '200');
        } else {
            $this->back($return);
        }
    }

////////////////////// 待审核

    /**
     * 待审核列表--数据
     * @param name 任务名称
     * @param user_fz_name 负责人名称
     * @param state 状态：1.待审核、2.已拒绝、3.进行中、4.待验收、5.已结束
     * @param type 类型：1.任务、2.任务模板
     * @param create_time 创建时间
     */
    public function task_audit_list(Request $request)
    {
        $data = $request->all();
        if (empty($data)) {
            $this->back('接收值为空');
        }

        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->task_audit_list($data);
        if (is_array($return['list'])) {
            $this->back('获取成功', '200', $return);
        } else {
            $this->back($return);
        }
    }

    /**
     *  已审核
     * @param Request $request
     */
    public function task_reviewed_list(Request $request)
    {
        $data = $request->all();
        if (empty($data)) {
            $this->back('接收值为空');
        }

        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->task_reviewed_list($data);
        if (is_array($return['list'])) {
            $this->back('获取成功', '200', $return);
        } else {
            $this->back($return);
        }
    }


    /**
     *  待验收
     * @param Request $request
     */
    public function task_acceptance(Request $request)
    {
        $data = $request->all();
        if (empty($data)) {
            $this->back('接收值为空');
        }

        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->task_me_acceptance($data);
        if (is_array($return['list'])) {
            $this->back('获取成功', '200', $return);
        } else {
            $this->back($return);
        }
    }



////////////////////// 任务模板

    /**
     * 任务模板列表--数据
     * @param name 任务名称
     * @param user_cj_name 创建人名称
     * @param state 状态：1.待审核、2.已拒绝、3.进行中、4.待验收、5.已结束
     * @param type 类型：1.任务、2.任务模板
     * @param type_mb 模板类型：1.财务模板、2.人事模板、3.产品开发
     * @param create_time 创建时间
     */
    public function task_template_list(Request $request)
    {
        $data = $request->all();
        if (empty($data)) {
            $this->back('接收值为空');
        }

        ////权限验证
        $arr = array();
        $arr['user_id'] = $data['user_info']['Id'];
        $arr['identity'] = array('task_template_list_all');
        $powers = $this->powers($arr);
        $data['list_all'] = $powers['task_template_list_all'];

        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->task_list($data);
        if (is_array($return['list'])) {
            $this->back('获取成功', '200', $return);
        } else {
            $this->back($return);
        }
    }


    /**
     * 任务模板分类--数据
     * 严威
     * @param create_time 2023.11.10
     */
    public function task_template_class(Request $request){

        $data = $request->all();
        $model = DB::table('tasks_model')->where('is_del',0)->get()->toArray();
        $this->back('获取成功', '200', $model);

    }

    /**
     * 新增任务模板
     * @param type_mb 模板类型：1.财务模板、2.人事模板、3.产品开发
     * @parma name 任务模板名称
     * @param user_fz 负责人
     * @param desc 任务描述
     * @param examine 审核人  数组
     * @param node 节点、节点任务   数组
     *              name 节点名称
     *              node_task_data 节点任务     数组
     *                      user_id 执行人
     *                      day 预计完成天数
     *                      task 任务
     */
    public function task_template_add(Request $request)
    {
        $this->validate($request, [
            'type_mb' => 'required| integer',
            'name' => 'required| string',
            'user_fz' => 'required| integer',
        ], [
            'type_mb.required' => 'type_mb为空',
            'type_mb.integer' => 'type_mb必须是字符串',
            'name.required' => 'name为空',
            'name.string' => 'name必须是字符串',
            'user_fz.required' => 'user_fz为空',
            'user_fz.integer' => 'user_fz必须是字符串',
        ]);

        $data = $request->all();
        $data['user_cj'] = $data['user_info']['Id'];
        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->task_template_add($data);
//		var_dump($return);exit;
        if ($return === 1) {
            $this->back('新增成功', '200');
        } else {
            $this->back($return);
        }
    }

    /**
     * 编辑任务模板
     * @param Id 任务模板id
     * @param type_mb 模板类型：1.财务模板、2.人事模板、3.产品开发
     * @parma name 任务模板名称
     * @param user_fz 负责人
     * @param desc 任务描述
     * @param examine 审核人  数组
     * @param node 节点、节点任务   数组
     *              name 节点名称
     *              node_task_data 节点任务     数组
     *                      user_id 执行人
     *                      day 预计完成天数
     *                      task 任务
     */
    public function task_template_update(Request $request)
    {
        $this->validate($request, [
            'Id' => 'required| integer',
            'name' => 'required| string',
//            'user_fz' => 'required| integer',
        ], [
            'Id.required' => 'Id为空',
            'Id.integer' => 'Id必须是字符串',
            'name.required' => 'name为空',
            'name.string' => 'name必须是字符串',
            'user_fz.required' => 'user_fz为空',
            'user_fz.integer' => 'user_fz必须是字符串',
        ]);

        $data = $request->all();
        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->task_template_update($data);
//		var_dump($return);exit;
        if ($return === 1) {
            $this->back('保存成功', '200');
        } else {
            $this->back($return);
        }
    }

////////////////////// 我的任务

    /**
     * 我的任务列表--数据
     * @param task_name 任务名称
     * @param task 任务描述
     * @param time_end_yj 预计结束时间
     */
    public function task_me(Request $request)
    {
        $data = $request->all();
        if (empty($data)) {
            $this->back('接收值为空');
        }

        $data['user_id'] = $data['user_info']['Id'];

        $call_task = new \App\Libs\wrapper\Task();
        $returns = $call_task->task_me($data);

        $data['class_id'] = [];
        $return = $call_task->task_me($data);

        //已放弃abandon   已完成complete  未开始 not_start  进行中not_overdue
        $returns['abandon_class'] = [];
        $returns['complete_class'] = [];
        $returns['not_start_class'] = [];
        $returns['not_overdue_class'] = [];
        $returns['overdue_class'] = [];


        $abandon = [];
        foreach ($return['abandon'] as $av) {

            if(isset($abandon[$av['class_id']]['num'])){
                $abandon[$av['class_id']]['num'] += 1;
            }else{
                $abandon[$av['class_id']]['num'] = 1;
            }

            $abandon[$av['class_id']]['class_id'] = $av['class_id'];
            $abandon[$av['class_id']]['class_name'] = $this->GetClassNameFid($av['class_id']);
            
        }

        $returns['abandon_class'] = array_values($abandon);
        

        
        $complete = [];
        foreach ($return['complete'] as $bv) {

            if(isset($complete[$bv['class_id']]['num'])){
                $complete[$bv['class_id']]['num'] += 1;
            }else{
                $complete[$bv['class_id']]['num'] = 1;
            }

            $complete[$bv['class_id']]['class_id'] = $bv['class_id'];
            $complete[$bv['class_id']]['class_name'] =$this->GetClassNameFid($bv['class_id']);
            
        }

        $returns['complete_class'] = array_values($complete);


                
        $not_start = [];
        foreach ($return['not_start'] as $cv) {

            if(isset($not_start[$cv['class_id']]['num'])){
                $not_start[$cv['class_id']]['num'] += 1;
            }else{
                $not_start[$cv['class_id']]['num'] = 1;
            }

            $not_start[$cv['class_id']]['class_id'] = $cv['class_id'];
            $not_start[$cv['class_id']]['class_name'] = $this->GetClassNameFid($cv['class_id']);
            
        }

        $returns['not_start_class'] = array_values($not_start);


        $not_overdue = [];
        foreach ($return['not_overdue'] as $dv) {

            if(isset($not_overdue[$dv['class_id']]['num'])){
                $not_overdue[$dv['class_id']]['num'] += 1;
            }else{
                $not_overdue[$dv['class_id']]['num'] = 1;
            }

            $not_overdue[$dv['class_id']]['class_id'] = $dv['class_id'];
            $not_overdue[$dv['class_id']]['class_name'] = $this->GetClassNameFid($dv['class_id']);
            
        }

        $returns['not_overdue_class'] = array_values($not_overdue);



        $overdue = [];
        foreach ($return['overdue'] as $ev) {

            if(isset($overdue[$ev['class_id']]['num'])){
                $overdue[$ev['class_id']]['num'] += 1;
            }else{
                $overdue[$ev['class_id']]['num'] = 1;
            }

            $overdue[$ev['class_id']]['class_id'] = $ev['class_id'];
            $overdue[$ev['class_id']]['class_name'] = $this->GetClassNameFid($ev['class_id']);
            
        }

        $returns['overdue_class'] = array_values($overdue);


        


//		var_dump($return);exit;
        if (is_array($returns)) {
            $this->back('获取成功', '200', $returns);
        } else {
            $this->back($returns);
        }
    }



    public function GetClassNameFid($id){
        $class = db::table('task_class')->where('id',$id)->first();
        $name = $class->title;
        if($class->fid>0){
            $classb = db::table('task_class')->where('id',$class->fid)->first();
            $name = $classb->title.'-'.$name;
            if($classb->fid>0){
                $classc = db::table('task_class')->where('id',$classb->fid)->first();
                $name = $classc->title.'-'.$name;
            }
        }

        return $name;
    }

////////////////////// 任务定时器

    /**
     * 任务定时器--数据
     * @param name 定时器名称
     * @param account_cj 创建人
     * @param task_name 循环任务
     * @param state 状态：1.未开始、2.运行中、3.暂停、4.结束
     * @param create_time 创建时间
     */
    public function task_timer(Request $request)
    {
        $data = $request->all();
        if (empty($data)) {
            $this->back('接收值为空');
        }

        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->task_timer($data);
//		var_dump($return);exit;
        if (is_array($return['list'])) {
            $this->back('获取成功', '200', $return);
        } else {
            $this->back($return);
        }
    }

    /**
     * 任务列表
     */
    public function task_sel(Request $request)
    {
        $data = $request->all();
        $where = "1=1";
        // type 类型：1.任务、2.任务模板
        if (!empty($data['type'])) {
            $where .= " and `type`={$data['type']}";
        }

        $sql = "select `Id`, `name`
					from tasks
					where {$where}";
        $list = json_decode(json_encode(db::select($sql)), true);

        if (is_array($list)) {
            $this->back('获取成功', '200', $list);
        } else {
            $this->back('获取失败');
        }
    }

    /**
     * 新增任务定时器
     * @parma name 定时器名称
     * @param task_id 任务id
     * @param desc 描述
     * @param time_state 开始时间
     * @param time_end 结束时间
     * @param loop_type 循环类型：1.每天、2.每周、3.每月
     * @param loop_time 循环时间
     */
    public function task_timer_add(Request $request)
    {
        $this->validate($request, [
            'name' => 'required| string',
        ], [
            'name.required' => 'name为空',
            'name.string' => 'name必须是字符串',
        ]);

        $data = $request->all();
        $data['user_id'] = $data['user_info']['Id'];
        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->task_timer_add($data);
        if ($return === 1) {
            $this->back('新增成功', '200');
        } else {
            $this->back($return);
        }
    }

    /**
     * 编辑任务定时器
     * @param Id 任务任务定时器id
     */
    public function task_timer_update(Request $request)
    {
        $this->validate($request, [
            'Id' => 'required| integer',
        ], [
            'Id.required' => 'Id为空',
            'Id.integer' => 'Id必须是字符串',
        ]);

        $data = $request->all();
        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->task_timer_update($data);
//		var_dump($return);exit;
        if ($return === 1) {
            $this->back('编辑成功', '200');
        } else {
            $this->back($return);
        }
    }

    /*
     * 删除任务定时器
     * @param Id 任务定时器id
     */
    public function task_timer_del(Request $request)
    {
        $this->validate($request, [
            'Id' => 'required| integer',
        ], [
            'Id.required' => 'Id为空',
            'Id.integer' => 'Id必须是数字',
        ]);

        $data = $request->all();
        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->task_timer_del($data);
        if ($return == 1) {
            $this->back('删除成功', '200');
        } else {
            $this->back($return);
        }
    }


    /**
     * 选择关联模板
     */
    public function choice_template(Request $request)
    {
        $data = $request->all();
        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->choice_template($data);
        if ($return) {
            $this->back('获取成功', '200', $return);
        } else {
            $this->back('获取失败');
        }
    }

    /**
     * 触发定时器
     */
    public function timer_trigger()
    {
        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->timer_trigger();
        var_dump($return);
        exit;

    }

    /**
     * 删除未发布任务的补货计划
     */
    public function delete_replenishment_data(Request $request)
    {
        $data = $request->all();
        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->delete_replenishment_data($data);
        if ($return == 1) {
            $this->back('删除成功', '200', $return);
        } else {
            $this->back('删除失败');
        }
    }


    /**
     * 任务分类
     */
    public function task_class()
    {
        $TaskClass = TaskClass::all();

        foreach ($TaskClass as $item) {
            $TaskClassModule = TaskClassModule::where('class_id', $item['id'])->get()->toArray();
            $item['module'] = $TaskClassModule;
        }


        $this->back('获取成功', '200', $TaskClass);
    }

    /**
     * 接受任务
     */
    public function task_accept(Request $request)
    {
        $data = $request->all();
        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->task_accept_model($data);
        if ($return['type'] == 'success') {
            $this->back('接受成功', '200',$return['msg']);
        } else {
            $this->back('接受失败','500',$return['msg']);
        }
    }

    /**
     * 修改进度说明
     */
//    public function task_text_update(Request $request)
//    {
//        $data = $request->all();
//        $call_task = new \App\Libs\wrapper\Task();
//        $return = $call_task->text_update($data);
//        if ($return['type'] == 'success') {
//            $this->back('修改成功', '200',$return['msg']);
//        } else {
//            $this->back('修改失败','500',$return['msg']);
//        }
//    }


    /**
     * 任务验收评分
     */
    public function task_score(Request $request)
    {
        $data = $request->all();
        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->user_cj_score($data);
        if ($return['type'] == 'success') {
            $this->back('打分成功', '200',$return['msg']);
        } else {
            $this->back('打分失败','500',$return['msg']);
        }
    }

    /**
     * 我发布的任务
     */
    public function task_release(Request $request)
    {
        $data = $request->all();
        if (empty($data)) {
            $this->back('接收值为空');
        }
        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->task_released($data);
        if (is_array($return['list'])) {
            $this->back('获取成功', '200', $return);
        } else {
            $this->back($return);
        }
//        else{
//            $user_cj = $data['user_id'];
//            $task_update = DB::table('tasks')->where('user_cj',$user_cj)->select()->get();
//            $return['list'] = json_decode(json_encode($task_update), true);
//
//        }
    }

    /**
     * 任务数据统计
     */
    public function tasks_data(Request $request){

        $data = $request->all();

        $call_task = new \App\Libs\wrapper\Task();

        $return = $call_task->task_lib_data($data);

        $this->back('获取成功', '200',$return);
    }

    /**
     * 任务是否需要总经办介入
     * 1.是 2.否
     */
    public function general_office_intervened(Request $request){

        $data = $request->all();

        $call_task = new \App\Libs\wrapper\Task();

        $return = $call_task->general_office_intervened($data);

        $this->back('请求成功', '200',$return);
    }

    /**
     * 亚马逊站内活动任务字段接口
     * @param Request $request
     */
    public function activity_api(Request $request){
        $data = $request->all();
        $call_task = new \App\Libs\wrapper\Task();

        $return = $call_task->activity_api($data);

        $this->back('获取成功', '200',$return);

    }

    /**
     * 亚马逊站外活动任务字段接口
     * @param Request $request
     */
    public function activity_outside_api(Request $request){
        $data = $request->all();
        $call_task = new \App\Libs\wrapper\Task();

        $return = $call_task->activity_outside_api($data);

        $this->back('获取成功', '200',$return);

    }

    /**
     * 生产下单是否新品选择接口
     * @param Request $request
     */
    public function is_new_api(Request $request){
        $data = $request->all();
        $call_task = new \App\Libs\wrapper\Task();

        $return = $call_task->is_new_api($data);

        $this->back('获取成功', '200',$return);

    }

    /**
     * 任务中反馈
     * @param Request $request
     */
    public function task_feedback(Request $request){

        $data = $request->all();

        $call_task = new \App\Libs\wrapper\Task();

        $return = $call_task->task_feedback($data);

        if ($return['type'] == 'success') {
            $this->back('请求成功', '200',$return['msg']);
        } else {
            $this->back('请求失败','500',$return['msg']);
        }
    }

    public function file_upload(Request $request){

        $data = $request->all();

        $result = $this->model->file_upload($data);

        if ($result['type'] == 'success') {
            $this->back($result['msg'], '200',$result['data']);
        } else {
            $this->back($result['msg'],'500',$result['data']);
        }
    }

    //审核建议
    public function examine_advice(Request $request){
        $data = $request->all();

        $result = $this->model->examine_advice($data);

        return $this->back('请求成功', '200',$result);

    }

    /**
     * @Desc: 重新指派任务执行人
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/4/7 11:02
     */
    public function reassignEnforcer()
    {
        $params = $this->request->all();
        $result = $this->model->reassignEnforcer($params);

        return $this->back($result['msg'] ?? '未知错误！请联系开发人员！', $result['code'] ?? 500, $result['data'] ?? []);
    }


    public function getBuhuoRequestTaskInfo()
    {
        $params = $this->request->all();
        $result = $this->model->getBuhuoRequestTaskInfo($params);

        return $this->back($result['msg'] ?? '未知错误！请联系开发人员！', $result['code'] ?? 500, $result['data'] ?? []);
    }

    public function getHistoryTaskInfo()
    {
        $params = $this->request->all();
        $result = $this->model->getHistoryTaskInfo($params);

        return $this->back($result['msg'] ?? '未知错误！请联系开发人员！', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc:仓库时效表
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/4/24 9:27
     */
    public function getWarehouseTimeTable()
    {
        $params = $this->request->all();
        $result = $this->model->getWarehouseTimeTable($params);

        return $this->back($result['msg'] ?? '未知错误！请联系开发人员！', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc:仓库时效表
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/4/24 9:27
     */
    public function getWarehouseTimeTableStatistics()
    {
        $params = $this->request->all();
        $result = $this->model->getWarehouseTimeTableStatistics($params);

        return $this->back($result['msg'] ?? '未知错误！请联系开发人员！', $result['code'] ?? 500, $result['data'] ?? []);
    }


    //添加审核人
    public function AddTaskUser()
    {
        $params = $this->request->all();
        $result = $this->model->AddTaskUser($params);
        if($result['type']=='success'){
            return $this->back('请求成功',200,$result);
        }else{
            return $this->back('请求失败',500);
        }
    }


    //拷贝任务

    public function CopyTask()
    {
        $params = $this->request->all();
        $result = $this->model->CopyTaskM($params);

        if($result['type']=='success'){
            return $this->back('请求成功',200,$result);
        }else{
            return $this->back($result['msg'],500);
        }
    }

    public function getTaskNewAllList()
    {
        $params = $this->request->all();

        $result = $this->model->getTaskNewAllList($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500,$result['data'] ?? []);
    }

    public function task_list_statistics(Request $request)
    {
        $params = $request->all();
        if (empty($params)) {
            $this->back('接收值为空');
        }

        ////权限验证
        $arr = array();
        $arr['user_id'] = $params['user_info']['Id'];
        $arr['identity'] = array('task_list_all');
        $powers = $this->powers($arr);
        $params['list_all'] = $powers['task_list_all'];

        $call_task = new \App\Libs\wrapper\Task();
        $result = $call_task->task_list_statistics($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500,$result['data'] ?? []);
    }

    public function task_audit_list_statistics(Request $request)
    {
        $data = $request->all();
        if (empty($data)) {
            $this->back('接收值为空');
        }

        $call_task = new \App\Libs\wrapper\Task();
        $result = $call_task->task_audit_list_statistics($data);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500,$result['data'] ?? []);
    }

    public function task_reviewed_list_statistics(Request $request)
    {
        $data = $request->all();
        if (empty($data)) {
            $this->back('接收值为空');
        }

        $call_task = new \App\Libs\wrapper\Task();
        $result = $call_task->task_reviewed_list_statistics($data);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500,$result['data'] ?? []);
    }

    public function get_superiors(){
        $params = $this->request->all();
        if (empty($params)) {
            $this->back('接收值为空');
        }
        $call_task = new \App\Libs\wrapper\Task();
        $result = $call_task->get_superiors($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500,$result['data'] ?? []);
    }

    public function getTaskDetailList(Request $request)
    {
        $params = $request->all();
        $call_task = new \App\Libs\wrapper\Task();
        $result = $call_task->getTaskDetailList($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500,$result['data'] ?? []);
    }

    public function getTaskModel(){
        $params = $this->request->all();
        if (empty($params)) {
            $this->back('接收值为空');
        }
        $result = $this->model->getTaskModel($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500,$result['data'] ?? []);
    }

    public function saveTasksSonScore()
    {
        $params = $this->request->all();
        $result = $this->model->saveTasksSonScore($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500,$result['data'] ?? []);
    }

    public function copyTasks()
    {
        $params = $this->request->all();
        $result = $this->model->copyTasks($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500,$result['data'] ?? []);
    }
}//类结束

