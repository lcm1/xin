<?php

namespace App\Http\Controllers\Pc;

use App\Http\Controllers\Controller;
use App\Jobs\FpxJob;
use Illuminate\Http\Request;
use App\Models\FpxModel;

class FpxController extends Controller
{
    private $model;
    private $request;

    public function __construct(Request $request)
    {
        $this->model = new FpxModel();
        $this->request = $request;
    }



    /**
     * 自发货订单列表
     */
    public function FpxAmzonOrderList()
    {

        $params = $this->request->all();

        $result = $this->model->GetFpxAmzonOrderListM($params);

        return $this->Ts_back($result);
    }


        /**
     * 自发货订单列表-总
     */
    public function GetFpxAmzonOrderListAll()
    {

        $params = $this->request->all();

        $result = $this->model->GetFpxAmzonOrderListAllM($params);

        return $this->Ts_back($result);
    }

    public function GetFpxLxAmzonOrderList()
    {

        $params = $this->request->all();

        $result = $this->model->GetFpxLxAmzonOrderList($params);

        return $this->Ts_back($result);
    }


    public function AliBaBa()
    {

        $params = $this->request->all();

        $result = $this->model->AliBaBa($params);

        return $this->Ts_back($result);
    }


    public function AliBaBaLogistics()
    {
        $params = $this->request->all();

        $result = $this->model->AliBaBaLogistics($params);

        return $this->Ts_back($result);
    }
    
    /**
     * 获取4px物流信息
     */
    public function GetFpxOrder()
    {

        $params = $this->request->all();

        $result = $this->model->GetFpxOrderM($params);

        return $this->Ts_back($result);
    }


    /**
     * 获取4px面单信息
     */
    public function GetFpxOrderLabel()
    {

        $params = $this->request->all();

        $result = $this->model->GetFpxOrderLabelM($params);

        return $this->Ts_back($result);
    }
    
    /**
     * 取消4px物流信息
     */
    public function CancelFpxOrder()
    {

        $params = $this->request->all();

        $result = $this->model->CancelFpxOrderM($params);
        $this->AddSysLog($params,$result,'取消4px物流信息',__METHOD__);
        return $this->Ts_back($result);
    }
    

    /**
     * 修改4px物流信息
     */
    public function UpdateFpxOrder()
    {

        $params = $this->request->all();

        $result = $this->model->UpdateFpxOrderM($params);
        $this->AddSysLog($params,$result,'修改4px物流信息',__METHOD__);

        return $this->Ts_back($result);
    }
    
    /**
     * 创建4px物流信息
     */
    public function CreateFpxOrder()
    {

        $params = $this->request->all();

        $result = $this->model->CreateFpxOrderM($params);

        return $this->Ts_back($result);
    }

    /**
     * 4px订单列表
     */
    public function FpxOrderList()
    {

        $params = $this->request->all();

        $result = $this->model->FpxOrderListM($params);

        return $this->Ts_back($result);
    }

        
    /**
     * 4px作废订单
     */
    public function CancelOrder()
    {

        $params = $this->request->all();

        $result = $this->model->CancelOrderM($params);

        return $this->Ts_back($result);
    }


    /**
     * 4px取消作废订单
     */
    public function DeleteCancelOrder()
    {

        $params = $this->request->all();

        $result = $this->model->DeleteCancelOrderM($params);

        return $this->Ts_back($result);
    }

    
       
    /**
     * 4px作废订单列表
     */
    public function CancelOrderList()
    {

        $params = $this->request->all();

        $result = $this->model->CancelOrderListM($params);

        return $this->Ts_back($result);
    }



    /**
     * 4px模版查询
     */
    public function GetFpxTemplate()
    {

        $params = $this->request->all();

        $result = $this->model->GetFpxTemplateM($params);

        return $this->Ts_back($result);
    }


    /**
     * 4px模版新增
     */
    public function SaveFpxTemplate()
    {

        $params = $this->request->all();

        $result = $this->model->SaveFpxTemplateM($params);

        return $this->Ts_back($result);
    }

    /**
     * 4px模版新增
     */
    public function GetFpxTemplateList()
    {

        $params = $this->request->all();

        $result = $this->model->GetFpxTemplateListM($params);

        return $this->Ts_back($result);
    }



    /**
     * 4px订单详情
     */
    public function FpxOrderDetail()
    {

        $params = $this->request->all();

        $result = $this->model->FpxOrderDetailM($params);

        return $this->Ts_back($result);
    }


    /**
     * 创建4px订单
     */
    public function HaveFpxOrder()
    {

        $params = $this->request->all();

        $result = $this->model->HaveFpxOrderM($params);

        $this->AddSysLog($params,$result,'创建4px订单',__METHOD__);
        return $this->Ts_back($result);
    }


    //实时更新订单
    public function FindOrder()
    {

        $params = $this->request->all();

        $result = $this->model->FindOrder($params);

        return $this->Ts_back($result);
    }


    //获取lazada授权信息
    public function GetLazadaShops()
    {

        $params = $this->request->all();

        $result = $this->model->GetLazadaShopsM($params);

        return $this->Ts_back($result);
    }

   //获取lazada订单列表
   public function LazadaOrders()
   {

       $params = $this->request->all();

       $result = $this->model->LazadaOrdersM($params);

       return $this->Ts_back($result);
   }


    //获取lazada待下单任务
    public function GetLazadaWaitOrder()
    {

        $params = $this->request->all();

        $result = $this->model->GetLazadaWaitOrderM($params);

        return $this->Ts_back($result);
    }
       
    //获取1688订单详情
    public function AlibabaOrder()
    {

        $params = $this->request->all();

        $result = $this->model->AlibabaOrder($params);

        return $this->Ts_back($result);
    }
        

   //lazada面单相关信息
   public function GetLazadaOrderLabel()
   {

       $params = $this->request->all();

       $result = $this->model->GetLazadaOrderLabel($params);

       return $this->Ts_back($result);
   }
   
    
    //获取lazada获取token
    public function UpdatelazadaToken()
    {

        $params = $this->request->all();

        $result = $this->model->UpdatelazadaTokenM($params);

        return $this->Ts_back($result);
    }


    //获取lazada授权地区
    public function GetLazadaArea()
    {

        $params = $this->request->all();

        $result = $this->model->GetLazadaAreaM($params);

        return $this->Ts_back($result);
    }
    
    

    //获取lazada授权url
    public function GetlazadaCodeUrl()
    {

        $params = $this->request->all();

        $result = $this->model->GetlazadaCodeUrl($params);

        return $this->Ts_back($result);
    }

    
    //阿里巴巴采集队列
    public function GetAlibabaQueue()
    {

        $params = $this->request->all();

        $result = $this->model->GetAlibabaQueueM($params);

        return $this->Ts_back($result);
    }

    //手动添加订单
    public function ManualAddOrder()
    {
        $params = $this->request->all();

        $result = $this->model->ManualAddOrderM($params);

        return $this->Ts_back($result);
    }

    //自动发货
    public function AutoDeliverGoods()
    {
        $params = $this->request->all();
//        return $this->Ts_back(['type' => 'fail', 'msg' => '维护中', 'data' => []]);

        if (!isset($params['type'])) {
            $this->back('type类型必传');
        }

        if (!isset($params['order_ids'])) {
            $this->back('order_ids订单id必传');
        }

        if (!isset($params['warehouse_id'])) {
            $this->back('warehouse_id仓库id必传');
        }

        if (!isset($params['template_id'])) {
            $this->back('template_id必传');
        }

        if (!isset($params['user_id'])) {
            $this->back('user_id必传');
        }

        $this->model->AutoDeliverGoods($params);

        return $this->Ts_back(['type' => 'success', 'msg' => '加入队列成功', 'data' => []]);
    }

    //获取lazada授权url
    public function GetAutoShipmentsLog()
    {

        $params = $this->request->all();

        $result = $this->model->GetAutoShipmentsLog($params);

        return $this->Ts_back($result);
    }


    public function Ts_back($params){

        if(!isset($params['msg'])){
            $params['msg'] = '获取成功';
        }
        if($params['type']=='success'){
            return $this->back($params['msg'], '200', $params['data']);
        }else{
            return $this->back($params['msg'], '300');
        }
    }

    public function bindLazadaSku()
    {
        $params = $this->request->all();
        try {
            if (!isset($params['lazada_sku']) || empty($params['lazada_sku'])){
                throw new \Exception('未给定Lazada Sku !');
            }
            if (!isset($params['custom_sku_id']) || empty($params['custom_sku_id'])){
                throw new \Exception('未选择库存sku！');
            }
            if (!isset($params['shop_id']) || empty($params['shop_id'])){
                throw new \Exception('未选择店铺！');
            }
            if (!isset($params['operatior_id']) || empty($params['operatior_id'])){
                throw new \Exception('未选择运营人员!');
            }
            if (!isset($params['user_id']) || empty($params['user_id'])){
                throw new \Exception('未选择运营人员!');
            }
            $result = $this->model->bindLazadaSku($params);
            $this->back($result['msg'] ?? '绑定成功！', $result['code']);
        }catch (\Exception $e){
            $this->back('绑定出错！原因：'.$e->getMessage().'；位置：'.$e->getLine(), $result['code'] ?? 500);
        }
    }
}