<?php

namespace App\Http\Controllers\Pc;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;


class ApiplanController extends Controller
{

    /**
     * api任务
     * @param Request $request
     */
    public function index(Request $request)
    {
        $data = $request->all();
        if (empty($data)) {
            $this->back('接收值为空');
        }

        $ApiplanComm = new \App\Libs\wrapper\Apiplan();
        $ApiPlanModel = new \App\Models\ApiPlan();
        $ApiPlanModel = $ApiplanComm->pageSearch($ApiPlanModel, $data);
        $ApiPlanPage = $ApiPlanModel->orderBy('id','desc')->paginate($data['page_count']);
        $ApiplanComm->deal($ApiPlanPage, array('shop', 'warehouse'));

        $this->back('获取成功', '200', $ApiPlanPage);

//            $this->back($return);

    }

    /**
     * 添加api任务
     * @param Request $request
     */
    public function add(Request $request)
    {
        $data = $request->all();
        $add = array();
        $checkModel = new \App\Models\ApiPlan();
        if (isset($data['api_name']) && $data['api_name'] != '') {
            $add['api_name'] = $data['api_name'];
            $checkModel =  $checkModel->where('api_name', $data['api_name']);
        } else {
            $this->back('请选择api名称');
        }
        if (isset($data['shop_id']) &&  $data['shop_id'] != '') {
            $add['shop_id'] = $data['shop_id'];
            $checkModel =  $checkModel->where('shop_id', $data['shop_id']);
        }

        if (isset($data['warehouse_id']) && $data['warehouse_id'] != '') {
            $add['warehouse_id'] = $data['warehouse_id'];
            $checkModel =  $checkModel->where('warehouse_id', $data['warehouse_id']);
        }

        if (isset($data['api_name']) && $data['api_name'] == 'SaiheGetProductInventory') {

            if (!isset($data['warehouse_id']) || $add['warehouse_id'] < 2) {
                $this->back('塞盒采集库存接口请选择仓库');
            }
        }

        if ($data['api_name'] == 'AmazonListOrders' || $data['api_name'] == 'AmazonListOrderItems') {
            if (!isset($data['shop_id']) || $add['shop_id'] < 2) {
                $this->back('请选择店铺');
            }
        }


        if (isset($data['is_finish']) && $data['is_finish'] != '') {
            $add['is_finish'] = $data['is_finish'];
        }

        $add['type'] = 2;
        if (isset($data['param_start_time_show']) && $data['param_start_time_show'] != '') {
            $add['param_start_time'] = strtotime($data['param_start_time_show']);
            $checkModel =  $checkModel->where('param_start_time', $add['param_start_time']);
        }
        if (isset($data['param_end_time_show']) && $data['param_end_time_show'] != '') {
            $add['param_end_time'] = strtotime($data['param_end_time_show']);
        }
        if (isset($data['next_token']) && $data['next_token'] != '') {
            $add['next_token'] = trim($data['next_token']);
        }

        $has = $checkModel->first();
        if (isset($has['id'])) {
            $this->back('数据已经存在');
        }
        $id = \App\Models\ApiPlan::insertGetId($add);
        if ($id > 0) {
            $this->back('添加成功', '200');
        } else {
            $this->back('添加失败');
        }
    }

    /**
     *  修改api计划任务
     * @param Request $request
     */
    public function update(Request $request)
    {
        $data = $request->all();
        $ApiPlan = \App\Models\ApiPlan::where('id', $data['id'])->where('type', 2)->first();
        if (!isset($ApiPlan['id'])) {
            $this->back('任务不存在');
        }
        $update = array();
        $update['is_finish'] = $data['is_finish'];

        $update['param_start_time'] = strtotime($data['param_start_time_show']);


        $update['param_end_time'] = strtotime($data['param_end_time_show']);

        $update['next_token'] = trim($data['next_token']);

        $updateResult = \App\Models\ApiPlan::where('id', $data['id'])->update($update);

        if ($updateResult > 0) {
            $this->back('编辑成功', '200');
        } else {
            $this->back($updateResult);
        }

    }

    /**
     * 获取亚马逊的店铺
     * @param Request $request
     */
    public function shop(Request $request)
    {
        $call_Apiplan = new \App\Libs\wrapper\Apiplan();
        $return = $call_Apiplan->plan_shop();
        if (is_array($return['list'])) {
            $this->back('获取成功', '200', $return);
        } else {
            $this->back($return);
        }

    }


    /**
     * 获取仓库
     * @param Request $request
     */
    public function warehouse(Request $request)
    {
        $SaiheWarehouse = \App\Models\SaiheWarehouse::where('id', '>', 1)->get()->toArray();
        $return['list'] = $SaiheWarehouse;
        if (is_array($return['list'])) {
            $this->back('获取成功', '200', $return);
        } else {
            $this->back($return);
        }
    }
}
