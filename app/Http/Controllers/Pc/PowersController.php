<?php
namespace App\Http\Controllers\Pc;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class PowersController extends Controller{
//////////////////////////////////////////////// 权限
////////////////////// 权限类别列表
	/**
	 * 权限类别列表--数据
	 * @param name 类别名称
	 */
	public function powers_type_list(Request $request){
		//获取权限分类
		$sql = "select *
					from xt_powers_type";
		$list = json_decode(json_encode(db::select($sql)),true);
		if(is_array($list)){
			$this->back('获取成功', '200', $list);
		}else{
			$this->back($list);
		}
	}
	
	/**
	 * 新增权限类别
	 * @parma name 权限类别名称
	 */
	public function powers_type_add(Request $request){
		$this->validate($request, [
			'name' => 'required| string',
		], [
			'name.required' => '请填写权限类别名称',
			'name.string' => 'name必须是字符串',
		]);
		
		$data = $request->all();
		$call_powers = new \App\Libs\wrapper\Powers();
		$return = $call_powers->powers_type_add($data);
		if($return === 1) {
			$this->back('新增成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 编辑权限类别
	 * @param Id 权限类别id
	 * @param name 权限类别名称
	 */
	public function powers_type_update(Request $request){
		//验证参数
		$this->validate($request, [
			'Id' => 'required| integer',
			'name' => 'required| string'
		], [
			'Id.required' => 'Id为空',
			'Id.integer' => 'Id不是整型数据',
			'name.required' => '请填写权限类别名称',
			'name.string' => 'name不是字符串',
		]);
		$data = $request->all();
		if(empty($data)){
			$this->back('接收值为空');
		}
		$call_powers = new \App\Libs\wrapper\Powers();
		$return = $call_powers->powers_type_update($data);
		if($return === 1){
			$this->back('编辑成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 权限列表
	 * @param name 权限名称
	 * @param identity 权限标识
	 * @param type_id 权限类别id
	 * @param type 类型：1查询、2操作
	 */
	public function powers_list(Request $request){
		$data = $request->all();
		if(empty($data)){
			$this->back('接收值为空');
		}
		
		$call_powers = new \App\Libs\wrapper\Powers();
		$return = $call_powers->powers_list($data);
		if(is_array($return)){
			$this->back('成功', '200', $return);
		}else{
			$this->back('获取数据失败');
		}
	}
	
	/**
	 * 新增权限
	 * @param name 权限名称
	 * @param identity 权限标识
	 * @param type_id 权限类别id
	 * @param type 类型：1查询、2操作
	 */
	public function powers_add(Request $request){
		//验证参数
		$this->validate($request, [
			'name' => 'required| string',
			'identity' => 'required| string',
			'type_id' => 'required| integer',
			'type' => 'required| integer',
		], [
			'name.required' => '请填写权限名称',
			'name.string' => 'name必须是字符串',
			'identity.required' => '请填写权限标识',
			'identity.string' => 'identity必须是字符串',
			'type_id.required' => 'type_id为空',
			'type_id.integer' => 'type_id必须是整型数据',
			'type.required' => '请选择权限类型',
			'type.integer' => 'type必须是整型数据',
		]);
		
		$data = $request->all();
		$call_powers = new \App\Libs\wrapper\Powers();
		$return = $call_powers->powers_add($data);
		if($return === 1){
			$this->back('新增成功','200');
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 权限信息
	 * @param power_id 权限id
	 */
	public function powers_info(Request $request){
		//验证参数
		$this->validate($request, [
			'power_id' => 'required| integer',
		], [
			'power_id.required' => 'power_id为空',
			'power_id.integer' => 'power_id不是整型数据',
		]);
		
		$data = $request->all();
		$call_powers = new \App\Libs\wrapper\Powers();
		$return = $call_powers->powers_info($data);
		if(is_array($return)){
			$this->back('获取成功', '200', $return[0]);
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 编辑权限
	 * @apram Id 权限id
	 * @param name 权限名称
	 * @param identity 权限标识
	 * @param type_id 权限类别id
	 * @param type 类型：1查询、2操作
	 */
	public function powers_update(Request $request){
		//验证参数
		$this->validate($request, [
			'Id' => 'required| integer',
			'name' => 'required| string',
			'identity' => 'required| string',
			'type_id' => 'required| integer',
			'type' => 'required| integer'
		], [
			'Id.required' => 'Id为空',
			'Id.integer' => 'Id必须是整型数据',
			'name.required' => '请填写权限名称',
			'name.string' => 'name不是字符串',
			'identity.required' => '请填写权限标识',
			'identity.string' => 'identity必须是字符串',
			'type_id.required' => '请选择权限类别',
			'type_id.integer' => 'type_id必须是整型数据',
			'type.required' => '请选择权限类型',
			'type.integer' => 'type必须是整型数据',
		]);
		$data = $request->all();
		if(empty($data)){
			$this->back('接收值为空');
		}
		$call_powers = new \App\Libs\wrapper\Powers();
		$return = $call_powers->powers_update($data);
		if($return === 1){
			$this->back('编辑成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 删除权限
	 * @param Id 权限id
	 */
	public function powers_del(Request $request){
		//验证参数
		$this->validate($request, [
			'Id' => 'required| integer',
		], [
			'Id.required' => 'Id为空',
			'Id.integer' => 'Id必须是整型数据',
		]);
		
		$data = $request->all();
		$call_powers = new \App\Libs\wrapper\Powers();
		$return = $call_powers->powers_del($data);
		if($return === 1){
			$this->back('删除成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}//类结束
