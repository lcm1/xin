<?php

namespace App\Http\Controllers\Pc;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
//////////////////////////////////////////////// 角色
////////////////////// 角色列表
    /**
     * @desc 角色列表--数据
     * @param name 角色名称
     * @param page 页码
     * @param page_count 每页条数
     */
    public function role_list(Request $request)
    {
        $data = $request->all();
        if (empty($data)) {
            $this->back('接收值为空');
        }
        $arr = array();
        //var_dump($data);die;
        $arr['user_id'] = $data['user_info']['Id'];
        $arr['identity'] = array('powers_role_update');
        $powers = $this->powers($arr);
        $data['power'] = $powers;
        $call_role = new \App\Libs\wrapper\Role();
        $return = $call_role->role_list($data);
        if (is_array($return['list'])) {
            $this->back('获取成功', '200', $return);
        } else {
            $this->back($return);
        }
    }

    /**
     * 新增角色
     * @param name 角色名称
     * @param desc 角色描述
     */
    public function role_add(Request $request)
    {
        //验证参数
        $this->validate($request, [
            'name' => 'required| string'
        ], [
            'name.required' => '请填写角色名称',
            'name.string' => 'name不是字符串',
        ]);

        $data = $request->all();
        $call_role = new \App\Libs\wrapper\Role();
        $return = $call_role->role_add($data);
        if ($return === 1) {
            $this->back('新增成功', 200);
        } else {
            $this->back($return);
        }
    }

    /**
     * 角色信息
     * @param Id 角色id
     */
    public function role_info(Request $request)
    {
        //验证参数
        $this->validate($request, [
            'Id' => 'required| integer'
        ], [
            'Id.required' => 'Id为空',
            'Id.integer' => 'Id必须是整型数字',
        ]);

        $data = $request->all();
        $call_role = new \App\Libs\wrapper\Role();
        $return = $call_role->role_info($data);
        if (is_array($return)) {
            $this->back('获取成功', 200, $return[0]);
        } else {
            $this->back($return);
        }
    }

    /**
     * 编辑角色
     * @param Id 角色id
     * @param name 角色名称
     * @param desc 角色描述
     */
    public function role_update(Request $request)
    {
        //验证参数
        $this->validate($request, [
            'Id' => 'required| integer',
        ], [
            'Id.required' => 'Id为空',
            'Id.integer' => 'Id必须是整型数字',
        ]);

        $data = $request->all();
        $call_role = new \App\Libs\wrapper\Role();
        $return = $call_role->role_update($data);
        if ($return === 1) {
            $this->back('编辑成功', 200);
        } else {
            $this->back($return);
        }
    }

    /**
     * 删除角色
     * @param Id 角色id
     */
    public function role_del(Request $request)
    {
        //验证参数
        $this->validate($request, [
            'Id' => 'required| integer',
        ], [
            'Id.required' => 'Id为空',
            'Id.integer' => 'Id必须是整型数字',
        ]);

        $Id = $request->get('Id');
        $call_role = new \App\Libs\wrapper\Role();
        $return = $call_role->role_del($Id);
//		var_dump($return);return;
        if ($return === 1) {
            $this->back('删除成功', 200);
        } else {
            $this->back($return);
        }
    }

////////////////////// 角色权限

    /**
     * 角色权限--数据
     * @param role_id 角色id
     */
    public function powers_role(Request $request)
    {
        //验证参数
        $this->validate($request, [
            'role_id' => 'required| integer',
        ], [
            'role_id.required' => 'role_id为空',
            'role_id.integer' => 'role_id必须是整型数字',
        ]);

        //验证参数
        $data = $request->all();
        $identity = 'powers_role_update';
        //var_dump($data);die;
        $userId = $data['user_info']['Id'];
        $sqlPower = "select pr.*,xr.*
				       from xt_role_user_join xruj
				       left join (select xprj.role_id, xprj.power_id, xp.*
				                         from xt_powers_role_join xprj
				                         inner join xt_powers xp on xp.Id=xprj.power_id
                                      ) as pr on pr.role_id = xruj.role_id
                       left join xt_role xr on xr.Id = pr.role_id 
				       where xruj.user_id={$userId} and pr.identity = '{$identity}'
				       group by pr.power_id";
        $power_list = json_decode(json_encode(db::select($sqlPower)), true);
        $sql = "select * 
                        from xt_role r
                        inner join xt_role_user_join ruj on ruj.role_id=r.Id
                        where ruj.user_id={$userId}";
        $userRole = json_decode(json_encode(db::select($sql)), true);
        //var_dump($userRole);die;
        //权限分类
        if (empty($power_list)&&empty($userRole[0])) {
            $sql = "select *
					from xt_role_user_join where user_id={$userId} and role_id={$data['role_id']}";
            $userPower = json_decode(json_encode(db::select($sql)), true);

            if(!empty($userPower)){
                $powersData['power'] = 1;
            }
            $sqlRole = "select *
                    from xt_role_user_join
                    where user_id = {$userId}";
            $listRole = json_decode(json_encode(db::select($sqlRole)), true);

            foreach ($listRole as $k => $v) {
                $sql = "select *
                    from xt_role
                    where `Id` in ({$v['role_id']}) and `power`=1";
                $role = json_decode(json_encode(db::select($sql)), true);

                /*foreach ($role as $ke => $va) {
                    $rolePid[] = $va['pid'];
                }*/
                //var_dump($role);
                //var_dump($role);
            }
            //$roleId = implode(',', $rolePid);
            die;
            $sql = "select power_id
					from xt_powers_role_join where role_id={$role[0]['Id']}";
            $powers_id = json_decode(json_encode(db::select($sql)), true);
            //var_dump($powers_id);die;
            foreach ($powers_id as $k => $v) {
                $sql = "select type_id
					from xt_powers where id={$v['power_id']}";
                $type_id = json_decode(json_encode(db::select($sql)), true);
                $sql = "select *
					from xt_powers_type where id={$type_id[0]['type_id']}";
                $type = json_decode(json_encode(db::select($sql)), true);
                foreach ($type as $ke => $va) {
                    $typeData[$ke]['id'][] = $va['Id'];
                    $typeData[$ke]['name'][] = $va['name'];
                }
            }
            $typeId = array_unique($typeData[0]['id']);
            $typeName = array_unique($typeData[0]['name']);
            foreach ($typeId as $key => $value) {
                $powers_type[$key]['Id'] = $value;
                $powers_type[$key]['name'] = $typeName[$key];
            }

            $sql = "select *
				       from xt_role 
				       where Id = {$data['role_id']}";
            $user = json_decode(json_encode(db::select($sql)), true);
            if($user[0]['power']==1){
                $role_id = $user[0]['Id'];
            }else{
                $sql = "select *
				       from xt_role 
				       where pid = {$user[0]['pid']} and power=1";
                //var_dump($sql);die;
                $powerUser = json_decode(json_encode(db::select($sql)), true);

                $role_id = $powerUser[0]['Id'];
            }
            $sql = "select p.*,prj.power_id
				       from xt_powers p
				       left join xt_powers_role_join prj on prj.power_id = p.Id where prj.role_id = {$role_id}";
            $powerData = json_decode(json_encode(db::select($sql)), true);

            /*$sql = "select p.*,prj.power_id, if(prj.Id, 'true', 'false') as is_checked
				       from xt_powers p
				       left join xt_powers_role_join prj on prj.power_id = p.Id where prj.role_id = {$data['role_id']}";
            $powers = json_decode(json_encode(db::select($sql)), true);*/
            //echo "<pre>";
            //var_dump($powers);die;
            $sql = "select *
				       from xt_role
				       where Id={$data['role_id']}";
            $is_powers = json_decode(json_encode(db::select($sql)), true);

            $sql = "select p.*, if(pow.Id, 'true', 'false') as is_checked
				       from xt_powers p
				       left join (select * from xt_powers_role_join xprj where xprj.role_id={$data['role_id']}) as pow on pow.power_id=p.Id
				       left join xt_powers_role_join prj on prj.power_id = p.Id where prj.role_id = {$role_id}";
            //var_dump($sql);die;
            $powers = json_decode(json_encode(db::select($sql)), true);

            $sql = "select p.*, if(pow.Id, 'true', 'false') as is_checked
				       from xt_powers p
				       left join (select * from xt_powers_role_join xprj where xprj.role_id={$data['role_id']}) as pow on pow.power_id=p.Id
				       left join xt_powers_role_join prj on prj.power_id = p.Id where prj.role_id = {$role_id}";
            //var_dump($sql);die;
            $powersUser = json_decode(json_encode(db::select($sql)), true);
            //echo "<pre>";
            //var_dump($powers);die;
            if($is_powers[0]['power']==1){
                /*foreach ($powers_type as $k => $v) {
                    //var_dump($v);die;
                    $powers_type[$k]['is_checked'] = false;
                    $powers_type[$k]['children'] = array();
                    foreach ($powers as $k1 => $v1) {
                        if ($v['Id'] == $v1['type_id']) {

                            $v1['is_checked'] = $v1['is_checked'] == 'true' ? true : false;

                            $powers_type[$k]['children'][] = $v1;
                            //$powers_type[$k]['children'][] = $v1;
                        }
                    }
                }*/
                $sql = "select *
					from xt_powers_type";
                $powers_type = json_decode(json_encode(db::select($sql)), true);
                $sql = "select p.*, if(prj.Id, 'true', 'false') as is_checked
				       from xt_powers p
				       left join xt_powers_role_join prj on prj.power_id = p.Id and prj.role_id = {$data['role_id']}";
                $powers = json_decode(json_encode(db::select($sql)), true);
                foreach ($powers_type as $k => $v) {
                    //var_dump($v);die;
                    $powers_type[$k]['is_checked'] = false;
                    $powers_type[$k]['children'] = array();
                    foreach ($powers as $k1 => $v1) {
                        if ($v['Id'] == $v1['type_id']) {
                            $v1['is_checked'] = $v1['is_checked'] == 'true' ? true : false;
                            $powers_type[$k]['children'][] = $v1;
                            //$powers_type[$k]['children'][] = $v1;
                        }
                    }
                }
            }else{
                foreach ($powers_type as $k => $v) {
                    //var_dump($v);die;
                    $powers_type[$k]['is_checked'] = false;
                    $powers_type[$k]['children'] = array();
                    foreach ($powers as $k1 => $v1) {
                        if ($v['Id'] == $v1['type_id']) {

                            $v1['is_checked'] = $v1['is_checked'] == 'true' ? true : false;

                            $powers_type[$k]['children'][] = $v1;
                            //$powers_type[$k]['children'][] = $v1;
                        }
                    }
                }
                /*foreach ($powers_type as $k => $v) {
                    //var_dump($v);die;
                    $powers_type[$k]['is_checked'] = false;
                    $powers_type[$k]['children'] = array();
                    foreach ($powerData as $k1 => $v1) {
                        if ($v['Id'] == $v1['type_id']) {
                            foreach ($powers as $k2=>$v2){
                                if($v['Id'] == $v2['type_id']){
                                    if($v1['power_id']==$v2['power_id']){
                                        //echo "<pre>";
                                        //var_dump($v1);
                                        $v1['is_checked'] = $v2['is_checked'] == 'true' ? true : false;
                                        //var_dump($v1);

                                    }
                                    $powers_type[$k]['children'][] = $v1;
                                }


                            }



                            //$powers_type[$k]['children'][] = $v1;
                        }
                    }
                }*///die;
            }
            //$powers = json_decode(json_encode(db::select($sql)), true);



        } else {
            $role_id = $data['role_id'];
            $sql = "select *
					from xt_role where Id={$role_id}";
            $role = json_decode(json_encode(db::select($sql)), true);
            if($role[0]['power']==2){
                $sql = "select *
					from xt_role where pid={$role[0]['pid']} and power=1";
                $powerRole = json_decode(json_encode(db::select($sql)), true);
                $powerId = $powerRole[0]['Id'];
                $sql = "select * 
                            from xt_powers_type pt 
                            inner join (select p.type_id from xt_powers p inner join 
                                                (select prj.power_id from xt_powers_role_join prj where role_id={$powerId}) 
                                                        as pow on pow.power_id=p.Id) 
                                        as pros on pros.type_id=pt.Id 
                            GROUP BY pt.Id";
                $powers_type = json_decode(json_encode(db::select($sql)), true);
                $sql = "select p.*, if(pow.Id, 'true', 'false') as is_checked
				       from xt_powers p
				       left join (select * from xt_powers_role_join xprj where xprj.role_id={$data['role_id']}) as pow on pow.power_id=p.Id
				       left join xt_powers_role_join prj on prj.power_id = p.Id where prj.role_id = {$powerId}";
                $powers = json_decode(json_encode(db::select($sql)), true);
                //echo "<pre>";
                //var_dump($powers_type);die;
                ob_start();
                foreach ($powers_type as $k => $v) {
                    //var_dump($v);die;
                    $powers_type[$k]['is_checked'] = false;
                    $powers_type[$k]['children'] = array();
                    foreach ($powers as $k1 => $v1) {

                        if ($v['Id'] == $v1['type_id']) {

                            $v1['is_checked'] = $v1['is_checked'] == 'true' ? true : false;
                            //var_dump($v1);
                            $powers_type[$k]['children'][] = $v1;
                            //$powers_type[$k]['children'][] = $v1;
                        }
                    }
                }
            }else{
                $sql = "select *
					from xt_powers_type";
                $powers_type = json_decode(json_encode(db::select($sql)), true);
                $sql = "select p.*, if(prj.Id, 'true', 'false') as is_checked
				       from xt_powers p
				       left join xt_powers_role_join prj on prj.power_id = p.Id and prj.role_id = {$data['role_id']}";
                $powers = json_decode(json_encode(db::select($sql)), true);
                foreach ($powers_type as $k => $v) {
                    //var_dump($v);die;
                    $powers_type[$k]['is_checked'] = false;
                    $powers_type[$k]['children'] = array();
                    foreach ($powers as $k1 => $v1) {
                        if ($v['Id'] == $v1['type_id']) {
                            $v1['is_checked'] = $v1['is_checked'] == 'true' ? true : false;
                            $powers_type[$k]['children'][] = $v1;

                        }
                    }
                }
            }
            //echo "<pre>";
            //var_dump($powers_type);die;

        }

        $powersData['powers_type'] = $powers_type;
        // echo "<pre>";
        //var_dump($powers_type);exit;
        if (is_array($powers_type)) {
            $this->back('获取成功', 200, $powersData);
        } else {
            $this->back('获取失败');
        }
    }


    /**
     * 编辑角色权限
     * @param role_id 角色id
     * @apram power_ids 权限id 例如：1,2,3,
     */
    public function powers_role_update(Request $request)
    {
        //验证参数
        $this->validate($request, [
            'role_id' => 'required| integer',
            'power_ids' => 'required| string',
        ], [
            'role_id.required' => 'role_id为空',
            'role_id.integer' => 'role_id必须是整型数字',
            'power_ids.required' => 'power_ids为空',
            'power_ids.string' => 'power_ids必须是字符串',
        ]);

        $data = $request->all();
        $call_role = new \App\Libs\wrapper\Role();
        $return = $call_role->powers_role_update($data);
//		var_dump($return);exit;
        if ($return === 1) {
            $this->back('保存成功', 200);
        }elseif($return===2){
            $this->back('保存成功', 201);
        }else {
            $this->back($return);
        }
    }


}//类结束符