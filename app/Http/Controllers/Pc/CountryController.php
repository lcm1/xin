<?php
namespace App\Http\Controllers\Pc;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class CountryController extends Controller{
//////////////////////////////////////////////// 国家
////////////////////// 国家列表
	/**
	 * 国家列表--数据
	 * @param name 名称
	 * @param name_yw 英文名
	 */
	public function country_list(Request $request){
		$data = $request->all();
		if(empty($data)){
			$this->back('接收值为空');
		}

		$call_country = new \App\Libs\wrapper\Country();
		$return = $call_country->country_list($data);
		if(is_array($return['list'])) {
			$this->back('获取成功', '200', $return);
		}else{
			$this->back($return);
		}
	}


    /**
     * 国家列表--数据
     * @param name 名称
     * @param name_yw 英文名
     */
    public function country_currency(Request $request){
        $data = $request->all();
        if(empty($data)){
            $this->back('接收值为空');
        }

        $call_country = new \App\Libs\wrapper\Country();
        $return = $call_country->country_currency($data);
        if(is_array($return['list'])) {
            $this->back('获取成功', '200', $return);
        }else{
            $this->back($return);
        }
    }


	/**
	 * 新增国家
	 * @param name 名称
	 * @param name_yw 英文名
	 * @logic 名称唯一
	 */
	public function country_add(Request $request){
		$this->validate($request, [
			'name' => 'required| string',
		], [
			'name.required' => 'name为空',
			'name.string' => 'name必须是字符串',
		]);

		$data = $request->all();
		$call_country = new \App\Libs\wrapper\Country();
		$return = $call_country->country_add($data);
//		var_dump($return);exit;
		if($return === 1) {
			$this->back('新增成功', '200');
		}else{
			$this->back($return);
		}
	}

	/**
	 * 编辑国家
	 * @param Id
	 * @parma name 名称
	 * @param name_yw 英文名
	 * @logic 名称唯一
	 */
	public function country_update(Request $request){
		$this->validate($request, [
			'Id' => 'required| integer',
		], [
			'Id.required' => 'Id为空',
			'Id.integer' => 'Id必须是数字',
		]);

		$data = $request->all();
		$call_country = new \App\Libs\wrapper\Country();
		$return = $call_country->country_update($data);
//		var_dump($return);exit;
		if($return === 1) {
			$this->back('编辑成功', '200');
		}else{
			$this->back($return);
		}
	}

	/**
	 * 删除国家
	 * @param Id
	 */
	public function country_del(Request $request){
		$this->validate($request, [
			'Id' => 'required| integer',
		], [
			'Id.required' => 'Id为空',
			'Id.integer' => 'Id必须是数字',
		]);

		$data = $request->all();
		$call_country = new \App\Libs\wrapper\Country();
		$return = $call_country->country_del($data);
//		var_dump($return);exit;
		if($return === 1) {
			$this->back('删除成功', '200');
		}else{
			$this->back($return);
		}
	}

	public function getAllCountryList(Request $request) {
        $data = $request->all();
        if(empty($data)){
            $this->back('接收值为空');
        }

        $call_country = new \App\Libs\wrapper\Country();
        $return = $call_country->getAllCountryList($data);
        if(is_array($return['list'])) {
            $this->back('获取成功', '200', $return);
        }else{
            $this->back($return);
        }
    }




















}//类结束
