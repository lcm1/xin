<?php

namespace App\Http\Controllers\Pc;

use App\Http\Controllers\Controller;
use App\Models\ExportModel;
use Illuminate\Http\Request;

class ExportController extends Controller
{
    protected $request;

    protected $exportModel;
    const EXPORT_SCENE = [
        'EXPORT_TRANSFER_DATA_DETAIL' => 'EXPORT_TRANSFER_DATA_DETAIL', // 导出采购详情
        'EXPORT_REPLENISHMENT_PLAN_LIST' => 'EXPORT_REPLENISHMENT_PLAN_LIST', // 导出补货计划列表
        'EXPORT_CONTRACT_SHIPMENT_LIST' => 'EXPORT_CONTRACT_SHIPMENT_LIST', // 导出合同出货列表
        'EXPORT_CUSTOM_SKU_MONTH_INVENTORY_COUNT_LIST' => 'EXPORT_CUSTOM_SKU_MONTH_INVENTORY_COUNT_LIST', // 导出月份库存变动列表
        'EXPORT_BUHUO_REQUEST_BOX_DATA' => 'EXPORT_BUHUO_REQUEST_BOX_DATA', // 导出补货装箱单
    ];

    public function __construct()
    {
        $this->request = new Request();
        $this->exportModel = new ExportModel();
    }

    public function export(Request $request)
    {
        // 获取所有请求参数
        $re = $request->all();
        // 获取场景值
        $scene = isset($re['scene']) ? $re['scene'] : '';
        // 获取条件参数
        $params = isset($re['params']) ? $re['params'] : [];
        // 处理导入功能
        $result = $this->exportModel->exportData($scene, $params);

        if ($result['type'] != 'success'){
            return $this->back($result['msg'], 500);
        }

        return $this->back($result['msg'], 200, $result['data'] ?? []);
    }

}