<?php

namespace App\Http\Controllers\Pc;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class CallbackController extends Controller
{
//////////////////////////////////////////////// 回调：   外部系统回调我们系统接口
////////////////////// lazada
    /*
     * lazada 授权
     * @param code
     * https://auth.lazada.com/oauth/authorize?response_type=code&force_auth=true&redirect_uri=https://new.zity.cn/pc/authorize/authorize_code&client_id=100498
     * http://newzity.com/pc/callback/authorize_lazada?code=null100498_SdmP3xF2sgPumB4M0uLAPbWB11103
     * */
    //店铺授权数据
    public function authorize_lazada(Request $request)
    {
        $data = $request->all();
        if (empty($data['code'])) {
            exit('code为空');
        }
        $call_lazada = new \App\Libs\platformApi\Lazada();
        $return = $call_lazada->access_token_get($data);
        echo (json_encode($return));
        exit;
//        var_dump($return);exit;
        if (empty($return)) {
            exit('返回值为空');
        }
        if ($return['code'] != 0) {
            exit($return['message']);
        }

        //// 存储 access_token
        $app_data = array(
            'app_key' => $return['app_key'],
            'app_secret' => $return['app_secret'],
            'refresh_token' => $return['refresh_token']
        );

        $app_data = json_encode($app_data);
        $sql = "update `shop`
	                set authorization_state=2, access_token='{$return['access_token']}', app_data='{$app_data}'
	                where `account`='{$return['account']}' and `platform_id`=(select `Id`
	                																from platform
	                																where `mark`=3)";
        $update = db::update($sql);
        if ($update) {
            exit('授权成功，如店铺授权状态未改变，请刷新页面');
        } else {
            exit('授权失败');
        }
    }


    //店铺授权数据--给老的易老板用
    public function authorize_lazada_old(Request $request)
    {
        $data = $request->all();
        if (empty($data['code'])) {
            exit('code为空');
        }
        $call_lazada = new \App\Libs\platformApi\Lazada();
        $return = $call_lazada->access_token_get($data);
        var_dump($return);
        exit;

    }



////////////////////// wish
    /*
     * wish 授权
     * @param code
     * @param shop_id 店铺id
     * https://www.merchant.wish.com/v3/oauth/authorize?client_id=5ee73a5529120705cc464dab
     * http://newzity.com/pc/callback/authorize_wish?shop_id=4&code=123456
     * */
    //店铺授权数据
    public function authorize_wish(Request $request)
    {
        $data = $request->all();
        if (empty($data['code'])) {
            exit('code为空');
        }

        //// 店铺信息
        $sql = "select `app_data`
					from `shop`
					where `Id` = {$data['shop_id']}
					limit 1";
        $find = json_decode(json_encode(db::select($sql)), true);
        if (empty($find)) {
            exit('店铺不存在');
        }

        $app_data = json_decode($find[0]['app_data'], true);
        $app_data['redirect_uri'] = $request->url() . '?shop_id=' . $data['shop_id'];
        $app_data['code'] = $data['code'];
        $call_wish = new \App\Libs\platformApi\Wish();
        $return = $call_wish->access_token_get($app_data);
        if (empty($return)) {
            exit('返回值为空');
        } else if ($return['code'] != 0) {
            exit($return['message']);
        }

        //// 存储 access_token
        $app_data = array(
            'client_id' => $app_data['client_id'],
            'client_secret' => $app_data['client_secret'],
            'refresh_token' => $return['data']['refresh_token']
        );

        $app_data = json_encode($app_data);
        $sql = "update `shop`
	                set authorization_state=2, access_token='{$return['data']['access_token']}', app_data='{$app_data}'
	                where `Id` = {$data['shop_id']}";
        $update = db::update($sql);
        if ($update) {
            exit('授权成功，如店铺授权状态未改变，请刷新页面');
        } else {
            exit('授权失败');
        }
    }


//
}