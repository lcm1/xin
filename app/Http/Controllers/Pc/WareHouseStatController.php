<?php

namespace App\Http\Controllers\Pc;


use App\Http\Controllers\Controller;
use App\Models\WarehouseStatDayModel;
use App\Models\WarehouseStatMonthModel;
use App\Models\WarehouseLocationStatDayModel;
use Illuminate\Http\Request;

class WareHouseStatController extends Controller
{
    private $model;
    private $request;

    public function __construct(Request $request)
    {
        $this->model   = new WarehouseStatDayModel();
        $this->request = $request;
    }

    public function GetDayList()
    {
        $params = $this->request->all();

        $result = $this->model->GetDayListM($params);

        return $this->Ts_back($result);
    }

    public function GetMonthList()
    {
        $params = $this->request->all();

        $result = (new WarehouseStatMonthModel())->GetMonthListM($params);

        return $this->Ts_back($result);
    }

    public function GetLocationDayList()
    {
        $params = $this->request->all();

        $result = (new WarehouseLocationStatDayModel())->GetDayListM($params);

        return $this->Ts_back($result);
    }

    private function Ts_back($params)
    {
        if ($params['type'] == 'success') {
            return $this->back($params['msg'], '200', $params['data']);
        } else {
            return $this->back($params['msg'], '300');
        }
    }
}