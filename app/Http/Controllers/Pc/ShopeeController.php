<?php

namespace App\Http\Controllers\Pc;

use App\Models\Countrys;
use App\Models\ShopeeCostModel;
use App\Models\ShopeeStock;
use App\Models\Shop;
use App\Models\Huilv;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


//时间
use App\Http\Controllers\Controller;
use Shopee\Client;

class ShopeeController extends Controller
{

    /**
     * shopee流量转化表 --数据
     *
     */
    public function shopee_flow_conversion(Request $request)
    {

        $data = $request->all();
        $arr = array();
        if(isset($data['user_info'])){
            $arr['user_id'] = $data['user_info']['Id'];
            $arr['identity'] = array('shopee_list_all');
            $powers = $this->powers($arr);
//        var_dump($powers);die;
            $data['shopee_list_all'] = $powers['shopee_list_all'];
        }else{
            $data['shopee_list_all'] = true;
        }
        if (empty($data)) {
            $this->back('接收值为空');
        }
        $call_shopee = new \App\Libs\wrapper\Shopee();
        $return = $call_shopee->shopee_flow_conversion($data);
        if (isset($return['list'])) {
            $this->back('获取成功', '200', $return);
        } else {
            $this->back($return);
        }
    }


    /**
     * 修改shopee流量转化的转化率指标
     *
     */
    public function shopee_flow_update(Request $request)
    {
        $this->validate($request, [
            'access_confirm_task' => 'required|numeric',

        ], [
            'current_balance.numeric' => '转化率标准必须是数字',
            'current_balance.required' => '转化率标准不能为空',
        ]);
        $data = $request->all();
        if (empty($data)) {
            $this->back('接收值为空');
        }
        $call_shopee = new \App\Libs\wrapper\Shopee();
        $return = $call_shopee->shopee_flow_update($data);
        if ($return === 1) {
            $this->back('编辑成功', '200');
        } else {
            $this->back($return);
        }
    }

    /**
     * 店铺资产表
     *
     */
    public function shopee_shop_assets(Request $request)
    {
        $data = $request->all();
        if (empty($data)) {
            $this->back('接收值为空');
        }
        $arr = array();
        if(isset($data['user_info'])){
            $arr['user_id'] = $data['user_info']['Id'];
            $arr['identity'] = array('shopee_list_all');
            $powers = $this->powers($arr);
//        var_dump($powers);die;
            $data['shopee_list_all'] = $powers['shopee_list_all'];
        }else{
            $data['shopee_list_all'] = true;
        }
        $call_shopee = new \App\Libs\wrapper\Shopee();
        $return = $call_shopee->shopee_shop_assets($data);
        if (isset($return['list'])) {
            $this->back('获取成功', '200', $return);
        } else {
            $this->back($return);
        }
    }

    /**
     * shopee店铺资产编辑
     */
    public function shopee_assets_update(Request $request)
    {
        $this->validate($request, [
            'current_balance' => 'numeric',
            'collection_amount' => 'numeric',
            'pending_payment' => 'numeric',
            'inventory_value' => 'numeric',

        ], [
            'current_balance.numeric' => '账号余额必须是数字',
            'collection_amount.numeric' => '回款金额必须是数字',
            'pending_payment.numeric' => '待回款必须是数字',
            'inventory_value.numeric' => '库存价值必须是数字',
        ]);
        $data = $request->all();
        if (empty($data)) {
            $this->back('接收值为空');
        }
        $call_shopee = new \App\Libs\wrapper\Shopee();
        $return = $call_shopee->shopee_assets_update($data);
        if ($return === 1) {
            $this->back('编辑成功', '200');
        } else {
            $this->back($return);
        }
    }


    /**
     * shopee退货排行
     */
    public function shopee_return(Request $request)
    {
        $data = $request->all();
        if (empty($data)) {
            $this->back('接收值为空');
        }
        $arr = array();
        if(isset($data['user_info'])){
            $arr['user_id'] = $data['user_info']['Id'];
            $arr['identity'] = array('shopee_list_all');
            $powers = $this->powers($arr);
//        var_dump($powers);die;
            $data['shopee_list_all'] = $powers['shopee_list_all'];
        }else{
            $data['shopee_list_all'] = true;
        }
        $call_shopee = new \App\Libs\wrapper\Shopee();
        $return = $call_shopee->shopee_return($data);
        if (is_array($return['list'])) {
            $this->back('获取成功', '200', $return);
        } else {
            $this->back($return);
        }
    }

    /**
     * shopee退款原因
     */
    public function shopee_return_reason(Request $request)
    {
        $data = $request->all();
        if (empty($data)) {
            $this->back('接收值为空');
        }
        $arr = array();
        if(isset($data['user_info'])){
            $arr['user_id'] = $data['user_info']['Id'];
            $arr['identity'] = array('shopee_list_all');
            $powers = $this->powers($arr);
//        var_dump($powers);die;
            $data['shopee_list_all'] = $powers['shopee_list_all'];
        }else{
            $data['shopee_list_all'] = true;
        }
        $call_shopee = new \App\Libs\wrapper\Shopee();
        $return = $call_shopee->shopee_return_reason($data);
        if (is_array($return['list'])) {
            $this->back('获取成功', '200', $return);
        } else {
            $this->back($return);
        }
    }

    /**
     * shopee广告组花费排名
     */

    public function shopee_adv_group(Request $request)
    {
        $data = $request->all();
        if (empty($data)) {
            $this->back('接收值为空');
        }
        $arr = array();
        if(isset($data['user_info'])){
            $arr['user_id'] = $data['user_info']['Id'];
            $arr['identity'] = array('shopee_list_all');
            $powers = $this->powers($arr);
//        var_dump($powers);die;
            $data['shopee_list_all'] = $powers['shopee_list_all'];
        }else{
            $data['shopee_list_all'] = true;
        }
        $call_shopee = new \App\Libs\wrapper\Shopee();
        $return = $call_shopee->shopee_adv_group($data);
        if (isset($return['list'])) {
            $this->back('获取成功', '200', $return);
        } else {
            $this->back($return);
        }
    }

    /**
     * shopee链接广告花费排名
     */
    public function shopee_adv_cost(Request $request)
    {
        $data = $request->all();
        if (empty($data)) {
            $this->back('接收值为空');
        }
        $arr = array();
        if(isset($data['user_info'])){
            $arr['user_id'] = $data['user_info']['Id'];
            $arr['identity'] = array('shopee_list_all');
            $powers = $this->powers($arr);
//        var_dump($powers);die;
            $data['shopee_list_all'] = $powers['shopee_list_all'];
        }else{
            $data['shopee_list_all'] = true;
        }
        $call_shopee = new \App\Libs\wrapper\Shopee();
        $return = $call_shopee->shopee_adv_cost($data);
        if (isset($return['list'])) {
            $this->back('获取成功', '200', $return);
        } else {
            $this->back($return);
        }
    }

    /**
     * shopee库存表
     */
    public function shopee_stock(Request $request)
    {
        $data = $request->all();
        if (empty($data)) {
            $this->back('接收值为空');
        }
        $arr = array();
        if(isset($data['user_info'])){
            $arr['user_id'] = $data['user_info']['Id'];
            $arr['identity'] = array('shopee_list_all');
            $powers = $this->powers($arr);
//        var_dump($powers);die;
            $data['shopee_list_all'] = $powers['shopee_list_all'];
        }else{
            $data['shopee_list_all'] = true;
        }
        $call_shopee = new \App\Libs\wrapper\Shopee();
        $return = $call_shopee->shopee_stock($data);
        if (isset($return['list']) && is_array($return['list'])) {
            $this->back('获取成功', '200', $return);
        } else {
            $this->back($return);
        }
    }

    /**
     * shopee个人毛利
     */
    public function shopee_gross_profit(Request $request){
        $data = $request->all();
        if (empty($data)) {
            $this->back('接收值为空');
        }
        $arr = array();
        if(isset($data['user_info'])){
            $arr['user_id'] = $data['user_info']['Id'];
            $arr['identity'] = array('shopee_list_all');
            $powers = $this->powers($arr);
//        var_dump($powers);die;
            $data['shopee_list_all'] = $powers['shopee_list_all'];
        }else{
            $data['shopee_list_all'] = true;
        }
        $call_shopee = new \App\Libs\wrapper\Shopee();
        $return = $call_shopee->shopee_gross_profit($data);
        if (isset($return['list']) && is_array($return['list'])) {
            $this->back('获取成功', '200', $return);
        } else {
            $this->back($return);
        }
    }

    /**
     * 获取shopee店铺
     *
     */
    public function shopee_shop(Request $request)
    {
        $data = $request->all();
        if (empty($data)) {
            $this->back('接收值为空');
        }
        ////权限验证
        $arr = array();
        if(isset($data['user_info'])){
            $arr['user_id'] = $data['user_info']['Id'];
            $arr['identity'] = array('shopee_list_all');
            $powers = $this->powers($arr);
//        var_dump($powers);die;
            $data['shopee_list_all'] = $powers['shopee_list_all'];
        }else{
            $data['shopee_list_all'] = true;
        }

//        $data['powers'] = $powers;
        $call_shopee = new \App\Libs\wrapper\Shopee();
        $return = $call_shopee->shopee_shop($data);
        if (is_array($return['list'])) {
            $this->back('获取成功', '200', $return);
        } else {
            $this->back($return);
        }
    }


    public function shopeeCostImport(Request $request)
    {
        $file = $request->file('file');
        //获取文件后缀名
        $extension = $file->getClientOriginalExtension();
        if ($extension == 'csv') {
            $PHPExcel = new \PHPExcel_Reader_CSV();
        } elseif ($extension == 'xlsx') {
            $PHPExcel = new \PHPExcel_Reader_Excel2007();
        } else {
            $PHPExcel = new \PHPExcel_Reader_Excel5();
        }

//        var_dump($PHPExcel);

        if (!$PHPExcel->canRead($file)) {
            return [
                'code' => '500',
                'msg' => '导入失败，Excel文件错误'
            ];
        }


        $Huilv = Huilv::where('currency', 'CNY')->orderBy('id', 'desc')->first();

        $PHPExcelLoad = $PHPExcel->load($file);
        $Sheet = $PHPExcelLoad->getSheet(0);

        /**取得一共有多少行*/
        $allRow = $Sheet->getHighestRow();
        //循环插入流量数据
        $a = 0;
        $b = 0;
        DB::beginTransaction();
        try {
            for ($j = 2; $j < $allRow; $j++) {
                $productSku = $Sheet->getCellByColumnAndRow(0, $j)->getValue();
                $sku = $Sheet->getCellByColumnAndRow(1, $j)->getValue();
                $cost = $Sheet->getCellByColumnAndRow(2, $j)->getValue();
                $name = $Sheet->getCellByColumnAndRow(3, $j)->getValue();
                $zongheFee = $Sheet->getCellByColumnAndRow(4, $j)->getValue();

                $costUs = round(($cost * $Huilv['dollar_rate']), 2);
                $zongheFeeUs = round(($zongheFee * $Huilv['dollar_rate']), 2);

                if (empty($productSku) || empty($sku) || empty($cost) || empty($zongheFee)) continue;
                ShopeeCostModel::updateOrCreate(
                    [
                        'product_sku' => $productSku,
                    ],
                    [
                        'sku' => $sku,
                        'cnname' => $name,
                        'cost_us' => $costUs,
                        'cost' => $cost,
                        'comprehensive_cost' => $zongheFee,
                        'comprehensive_cost_us' => $zongheFeeUs,
                        'huilv_id' => $Huilv['id'],
                    ]
                );
            }
            DB::commit();
            $code = 1;
            $msg = 'ok';
        } catch (\Exception $e) {
            DB::rollBack();
            $code = 0;
            $msg = '系统异常,异常原因:' . $e->getMessage() . ',请联系开发人员排查该问题!';
        }
        return [
            'code' => $code,
            'msg' => $msg
        ];
    }

    public function shopeeCostList(Request $request)
    {
        $data = $request->all();
        $call_shopee = new \App\Libs\wrapper\Shopee();
        $return = $call_shopee->shopeeCostList($data);
        if (is_array($return['list'])) {
            $this->back('获取成功', '200', $return);
        } else {
            $this->back($return);
        }
    }

    /**
     * 成本和运营费导出
     */
    public function adv_excel_export(Request $request)
    {
        $data = $request->all();
        $call_shopee = new \App\Libs\wrapper\Shopee();
        $return = $call_shopee->adv_excel_export($data);
    }

    /**
     * shopee成本编辑
     */
    public function shopee_cost_update(Request $request)
    {
        $this->validate($request, [
            'cost' => 'numeric',
            'comprehensive_cost' => 'numeric',

        ], [
            'cost.numeric' => '成本必须是数字',
            'collection_amount.comprehensive_cost' => '运营费必须是数字',
        ]);
        $data = $request->all();
        ////权限验证
        $arr = array();
        $arr['user_id'] = $data['user_info']['Id'];
        $arr['identity'] = array('shopee_cost_update');
        $powers = $this->powers($arr);
        if(!$powers['shopee_cost_update']){
            $this->back('无编辑权限');
        }
        if (empty($data)) {
            $this->back('接收值为空');
        }
        $call_shopee = new \App\Libs\wrapper\Shopee();
        $return = $call_shopee->shopee_cost_update($data);
        if ($return === 1) {
            $this->back('编辑成功', '200');
        } else {
            $this->back($return);
        }
    }


    public function shopeeInventoryImport(Request $request)
    {
        $file = $request->file('file');
        $shops = $request->get('store');
        $shops = explode(',', $shops);
//        $month = $request->get('month');
//        $week = $request->get('week');
        $startTime = $request->get('start_date');
        $endTime = $request->get('end_date');
        $weekarray = array("日", "一", "二", "三", "四", "五", "六");
        if ($weekarray[date("w", strtotime($startTime))] != '五' ||
            $weekarray[date("w", strtotime($endTime))] != '四') {
            return [
                'type' => 'fail',
                'msg' => "请选择起始日为周五,且结束日为周四的区间, 当前选择起始日为周{$weekarray[date("w", strtotime($startTime))]}
                ,结束日为周{$weekarray[date("w", strtotime($endTime))]}"
            ];
        }
//        return $request;
//        if ($week == '1') {
//            $day = '01';
//        } elseif ($week == '2') {
//            $day = '08';
//        } elseif ($week == '3') {
//            $day = '15';
//        } else {
//            $day = '22';
//        }
//        $date = date("Y-" . $month . "-" . $day);

        //获取文件后缀名
        $extension = $file->getClientOriginalExtension();
        if ($extension == 'csv') {
            $PHPExcel = new \PHPExcel_Reader_CSV();
        } elseif ($extension == 'xlsx') {
            $PHPExcel = new \PHPExcel_Reader_Excel2007();
        } else {
            $PHPExcel = new \PHPExcel_Reader_Excel5();
        }
        if (!$PHPExcel->canRead($file)) {
            return [
                'code' => '500',
                'msg' => '导入失败，Excel文件错误'
            ];
        }
        $PHPExcelLoad = $PHPExcel->load($file);

        $Sheet = $PHPExcelLoad->getSheet(0);
//        dump( $Sheet->getCell('AN2')->getFormattedValue());
//
//        die();
        /**取得一共有多少行*/
        $allRow = $Sheet->getHighestRow();
        DB::beginTransaction();
        try {
            foreach ($shops as $v) {

                $shopData = Shop::where('id', $v)->first();
                if (!isset($shopData['country_id'])) {
                    return [
                        'code' => '0',
                        'msg' => '找不到对应店铺'
                    ];

                }
                if (!($shopData['country_id'] > 0)) {
                    return [
                        'code' => '0',
                        'msg' => '店铺未设置国家'
                    ];
                }
                /*    $Countrys = Countrys::where('id', $shopData['country_id'])->first();
                    if (!isset($Countrys['currency'])) {
                        return [
                            'code' => '0',
                            'msg' => '对应国家的货币未设置'
                        ];
                    }*/
                $Huilv = Huilv::where('currency', 'CNY')->orderBy('id', 'desc')->first();
                if (!isset($Huilv['dollar_rate'])) {
                    return [
                        'code' => '500',
                        'msg' => '货币汇率未设置'
                    ];
                }

                for ($j = 2; $j < $allRow; $j++) {
                    $sku = $Sheet->getCellByColumnAndRow(0, $j)->getValue();
                    $product_name = $Sheet->getCellByColumnAndRow(1, $j)->getValue();
                    $opening_inventory = $Sheet->getCellByColumnAndRow(2, $j)->getValue();
                    $price = $Sheet->getCellByColumnAndRow(3, $j)->getValue();
                    $opening_inventory_value = $Sheet->getCell('E' . $j)->getFormattedValue();//getOldCalculatedValue getFormattedValue
                    $warehousing_month = $Sheet->getCellByColumnAndRow(5, $j)->getValue();
                    $warehousing_price = $Sheet->getCell('G' . $j)->getFormattedValue();

                    $total_shipment = $Sheet->getCell('AN' . $j)->getFormattedValue();
                    $shipment_price = $Sheet->getCell('AO' . $j)->getFormattedValue();
                    $ending_inventory = $Sheet->getCell('AP' . $j)->getFormattedValue();
                    $ending_inventory_price = $Sheet->getCell('AQ' . $j)->getOldCalculatedValue();
                    $quantity_transit = $Sheet->getCell('AR' . $j)->getValue();
                    $amount_transit = $Sheet->getCell('AS' . $j)->getOldCalculatedValue();
                    $stock_quantity = $Sheet->getCell('AT' . $j)->getFormattedValue();
                    $stock_amount = $Sheet->getCell('AU' . $j)->getFormattedValue();
                    $average_daily_sales = $Sheet->getCell('AV' . $j)->getOldCalculatedValue();


                    $opening_inventory_value_us = round(((float)$opening_inventory_value * $Huilv['dollar_rate']), 2);
                    $warehousing_price_us = round(((float)$warehousing_price * $Huilv['dollar_rate']), 2);
                    $shipment_price_us = round(((float)$shipment_price * $Huilv['dollar_rate']), 2);
                    $ending_inventory_price_us = round(((float)$ending_inventory_price * $Huilv['dollar_rate']), 2);
                    $amount_transit_us = round(((float)$amount_transit * $Huilv['dollar_rate']), 2);
                    $stock_amount_us = round(((float)$stock_amount * $Huilv['dollar_rate']), 2);


                    if (empty($sku) || empty($product_name)) continue;
                    ShopeeStock::updateOrCreate(
                        [
                            'sku' => $sku,
                            'shop_id' => $v,
                            'start_date' => $startTime,
                            'end_date' => $endTime
//                            'date' => $date,
                        ],
                        [
                            'product_name' => $product_name,
                            'opening_inventory' => $opening_inventory,
                            'price' => $price,
                            'opening_inventory_value' => $opening_inventory_value,
                            'warehousing_month' => $warehousing_month,
                            'warehousing_price' => $warehousing_price,
                            'total_shipment' => $total_shipment,
                            'shipment_price' => $shipment_price,
                            'ending_inventory' => $ending_inventory,
                            'ending_inventory_price' => $ending_inventory_price,
                            'quantity_transit' => $quantity_transit,
                            'amount_transit' => $amount_transit,
                            'stock_quantity' => $stock_quantity,
                            'stock_amount' => $stock_amount,
                            'average_daily_sales' => $average_daily_sales,
                            'opening_inventory_value_us' => $opening_inventory_value_us,
                            'warehousing_price_us' => $warehousing_price_us,
                            'shipment_price_us' => $shipment_price_us,
                            'ending_inventory_price_us' => $ending_inventory_price_us,
                            'amount_transit_us' => $amount_transit_us,
                            'stock_amount_us' => $stock_amount_us,
                            'huilv_id' => $Huilv['id'],

                        ]
                    );
                }
            }
            DB::commit();
            $code = 1;
            $msg = 'ok';
        } catch (\Exception $e) {
            DB::rollBack();
            $code = 0;
            $msg = '系统异常,异常原因:' . $e->getMessage() . ',请联系开发人员排查该问题!';
        }

        return [
            'code' => $code,
            'msg' => $msg
        ];

        //循环插入流量数据
//        DB::beginTransaction();
//        try {
//            for ($j = 2; $j < $allRow; $j++) {
//
//                $sku = $Sheet->getCellByColumnAndRow(0, $j)->getValue();
//                $product_name = $Sheet->getCellByColumnAndRow(1, $j)->getValue();
//                $opening_inventory = $Sheet->getCellByColumnAndRow(2, $j)->getValue();
//                $price = $Sheet->getCellByColumnAndRow(3, $j)->getValue();
//                $opening_inventory_value = $Sheet->getCellByColumnAndRow(4, $j)->getValue();
//                $warehousing_month = $Sheet->getCellByColumnAndRow(5, $j)->getValue();
//                $warehousing_price = $Sheet->getCellByColumnAndRow(6, $j)->getValue();
//                $total_shipment = $Sheet->getCellByColumnAndRow(7, $j)->getValue();
////                echo $sku;
////                echo $product_name;
////                echo $opening_inventory;
////                echo $price;
////                echo $opening_inventory*$price;
////                echo $warehousing_month;
////                echo $price*$warehousing_month;
//                $shipment_price = $Sheet->getCellByColumnAndRow(8, $j)->getValue();
//                echo $shipment_price;
//                die();
//                $ending_inventory = $Sheet->getCellByColumnAndRow(3, $j)->getValue();
//                $ending_inventory_price = $Sheet->getCellByColumnAndRow(3, $j)->getValue();
//                $quantity_transit = $Sheet->getCellByColumnAndRow(3, $j)->getValue();
//                $amount_transit = $Sheet->getCellByColumnAndRow(3, $j)->getValue();
//                $stock_quantity = $Sheet->getCellByColumnAndRow(3, $j)->getValue();
//                $stock_amount = $Sheet->getCellByColumnAndRow(3, $j)->getValue();
//                $average_daily_sales = $Sheet->getCellByColumnAndRow(3, $j)->getValue();
//                if (empty($sku) || empty($cost) || empty($zongheFee)) continue;
//                ShopeeStock::updateOrCreate(
//                    [
//                        'sku' => $sku,
//                    ],
//                    [
//                        'cnname' => $name,
//                        'cost' => $cost,
//                        'comprehensive_cost' => $zongheFee,
//                    ]
//                );
//            }
//            DB::commit();
//            $code = 1;
//            $msg = 'ok';
//        } catch (\Exception $e) {
//            DB::rollBack();
//            $code = 0;
//            $msg = '系统异常,异常原因:' . $e->getMessage() . ',请联系开发人员排查该问题!';
//        }
//        return [
//            'code' => $code,
//            'msg' => $msg
//        ];
    }

    public function shopeeStockList(Request $request)
    {
        $data = $request->all();
        $call_shopee = new \App\Libs\wrapper\Shopee();
        $return = $call_shopee->shopeeStockList($data);
        if (is_array($return['list'])) {
            $this->back('获取成功', '200', $return);
        } else {
            $this->back($return);
        }
    }

    /**
     * 查询国家
     */
    public function country_list(Request $request){
        $data = $request->all();
        if(isset($data['user_info'])) {
            $arr = array();
            $arr['user_id'] = $data['user_info']['Id'];
            $arr['identity'] = array('shopee_list_all');
            $powers = $this->powers($arr);
//        var_dump($powers);die;
            $data['shopee_list_all'] = $powers['shopee_list_all'];
        }else{
            $data['shopee_list_all'] = true;
        }
        $data['powers'] = $powers;
        $call_shopee = new \App\Libs\wrapper\Shopee();
        $return = $call_shopee->country_list($data);
        if (is_array($return['list'])) {
            $this->back('获取成功', '200', $return);
        } else {
            $this->back($return);
        }
    }

    /**
     * 查询用户
     */
    public function user_list(Request $request){
        $data = $request->all();
        ////权限验证
        if(isset($data['user_info'])) {
            $arr = array();
            $arr['user_id'] = $data['user_info']['Id'];
            $arr['identity'] = array('shopee_list_all');
            $powers = $this->powers($arr);
//        var_dump($powers);die;
            $data['shopee_list_all'] = $powers['shopee_list_all'];
        }else{
            $data['shopee_list_all'] = true;
        }
        $data['powers'] = $powers;
        $call_shopee = new \App\Libs\wrapper\Shopee();
        $return = $call_shopee->user_list($data);
        if (is_array($return['list'])) {
            $this->back('获取成功', '200', $return);
        } else {
            $this->back($return);
        }
    }


    /**
     * 销量前10的商品按照sku
     * @param Request $request
     */
    public function shopeeWeekMostBySku(Request $request)
    {
        set_time_limit(120);

        $week = 0; //前几周
        $data = $request->all();

        if (!isset($data['shop_id'])) {
            $shop_sql = "select Id,shop_name from shop where platform_id=7 and state=1";
            $shop = json_decode(json_encode(db::select($shop_sql)), true);
            if (!$shop) {
                return '没有shopee店铺';
            }
            $data['shop_id'] = $shop[0]['Id'];
        }

        if (is_array($data['shop_id'])) {
            $arr = $data['shop_id'];
            if (count($arr) == 1) {
                $shopId = $data['shop_id'];
            } else {
                $shopId = $arr;
            }
        } else {
            $shopId = $data['shop_id'];
        }


        if (!isset($data['start_date']) || !isset($data['end_date'])) {
            $data['start_date'] = date('Y-m-d');
            $data['end_date'] = date('Y-m-d', strtotime('- 7 days'));
        }

        $startDateTime = $data['start_date'] . ' 00:00:00';//起始时间 日期格式
        $endDateTime = $data['end_date'] . ' 23:59:59'; //截止时间 日期格式

        $startTime = strtotime($data['start_date']);
        $startTimeLastWeek = $startTime - 7 * 86400; //起始时间7天前时间戳
        $startDateTimeLastWeek = date("Y-m-d H:i:s", $startTimeLastWeek);

        $endTimeLastWeek = $startTimeLastWeek + 7 * 86400;
        $endDateTimeLastWeek = date("Y-m-d H:i:s", $endTimeLastWeek);

        $ShopeeModel = new \App\Libs\wrapper\Shopee();
        // $allSkuCount = $ShopeeModel->shopWeekAllgoodCount($startDateTime, $endDateTime, $shopId);
        $shopWeekSaleTotal = $ShopeeModel->shopWeekSaleTotal($startDateTime, $endDateTime, $shopId);
        $allAddFee = $ShopeeModel->shopWeekAllAdFee($startDateTime, $endDateTime, $shopId);
        $itemList = $ShopeeModel->mostProductBySku($startDateTime, $endDateTime, $shopId);

        if ($shopWeekSaleTotal > 0) {
            $skuAdFee = ($allAddFee / $shopWeekSaleTotal);  //单个sku广告费
        } else {
            $skuAdFee = 0;
        }

        $i = 0;
        foreach ($itemList as $key => $item) {
            $i++;
            $orders = $ShopeeModel->orders($item->variation_sku, $startDateTime, $endDateTime, $shopId);

            $shopeeGoodsCost = $ShopeeModel->shopeeGoodsCost($item->variation_sku, $startDateTime, $endDateTime, $shopId, $orders);  //订单成本
            $skuNum = $ShopeeModel->skuNum($item->variation_sku, $startDateTime, $endDateTime, $shopId);  //sku行数
            $costOtherFee = $ShopeeModel->costOtherFee($item->variation_sku, $startDateTime, $endDateTime, $shopId, $orders);  //综合费用
            $OrderOtherFee = $ShopeeModel->OrderOtherFee($item->variation_sku, $startDateTime, $endDateTime, $shopId, $orders);  //手续费

            $ordersNumByItemTime = $ShopeeModel->ordersNumBySkuTime($item->variation_sku, $startDateTime, $endDateTime, $shopId);
            $orderCount = $ordersNumByItemTime->countorder;  //订单数
            $salePriceByItemTime = $ShopeeModel->salePriceBySkuTime($item->variation_sku, $startDateTime, $endDateTime, $shopId);

            $salePriceTotal = $salePriceByItemTime->pricetotal;  //销售额
            $lastNumItems = $ShopeeModel->numSku($item->variation_sku, $startDateTimeLastWeek, $endDateTimeLastWeek, $shopId);
            $lastNum = $lastNumItems->num; //上周销量
            if ($lastNum > 0) {
                $riseRate = round((($item->nums_all - $lastNum) / $lastNum) * 100, 2) . "%";  //增长率
            } else {
                $riseRate = "100%";
            }
            if ($item->nums_all > 0) {
                $unitPrice = round(($salePriceTotal / $item->nums_all), 2);//客单价

            } else {
                $unitPrice = "0";
            }

            $cancelOrderNum = $ShopeeModel->cancelOrderNumBySku($item->variation_sku, $startDateTime, $endDateTime, $shopId);
            $cancelNum = $cancelOrderNum->num;  //取消订单数量
            $returnOrderNum = $ShopeeModel->returnOrderNumBySku($item->variation_sku, $startDateTime, $endDateTime, $shopId);

            $returnNum = $returnOrderNum->num;  //退货订单数量
            if (($cancelNum + $orderCount) > 0) {
                $cancelRate = round(($cancelNum / ($cancelNum + $orderCount)) * 100, 2) . "%";  //订单取消率
            } else {
                $cancelRate = "0%";
            }
            if ($item->nums_all > 0) {
                $retrnRate = round(($returnNum / $item->nums_all) * 100, 2) . "%";  //退货率
            } else {
                $retrnRate = "0%";
            }

            $adFee = $skuAdFee * $salePriceTotal;

            if ($salePriceTotal > 0) {

                // $lirunArr = $ShopeeModel->shopeeLirunBySku($salePriceTotal, $item->variation_sku, $startDateTime, $endDateTime, $shopId, $skuAdFee);//毛利
                $lirun = $salePriceTotal - $shopeeGoodsCost - $costOtherFee - $adFee - $OrderOtherFee;
                $lirun_rate = round(($lirun / $salePriceTotal) * 100, 2) . "%";
            } else {
                $lirun = 0;
                $lirun_rate = 0;
            }

            $getShopeeCost = $ShopeeModel->getShopeeCostBysku($item->variation_sku);

            if (isset($getShopeeCost[0]->cnname)) {
                $item->item_name = $getShopeeCost[0]->cnname;
            } else {
                $item->item_name = '';
            }

            $item->order_count = $orderCount;

            $item->sale_price_total = $salePriceTotal;
            $item->lirun = round($lirun, 2);
            $item->lirun_rate = $lirun_rate;
            $item->unit_price = $unitPrice;
            $item->last_num = $lastNum;
            $item->rise_rate = $riseRate;
            $item->cancel_num = $cancelNum;
            $item->cancel_rate = $cancelRate;
            $item->return_num = $returnNum;
            $item->return_rate = $retrnRate;
            $item->adFeeAll = $allAddFee;
            $item->adFee = round($adFee, 2);
            $item->shopWeekSaleTotal = $shopWeekSaleTotal;
            $item->cost_goods = round($shopeeGoodsCost, 2);
            $item->skucount = $skuNum;
            $item->skuAdFee = round($skuAdFee, 2);
            $item->costOtherFee = round($costOtherFee, 2);
            $item->OrderOtherFee = round($OrderOtherFee, 2);
//            var_dump(trim($lirun_rate,'%'));
            if (trim($lirun_rate,'%') * 100 > 15) {
                $item->is_remind = 1;
            } elseif (trim($lirun_rate,'%') * 100 < 15) {
                $item->is_remind = 2;
            }
            $itemList[$key] = $item;
        }

        $itemList = json_decode(json_encode($itemList), true);

        $return['list'] = $itemList;
        //店铺
        // $shopee_shop= $ShopeeModel->shopee_shop();
        if (is_array($return['list'])) {
            $return['total'] = $i;
            $this->back('获取成功', '200', $return);
        } else {
            $return['total'] = 0;
            $this->back($return);
        }
    }


    /**
     * 销量前10的商品按照名称
     * @param Request $request
     */
    public function shopeeWeekMostByName(Request $request)
    {
        set_time_limit(120);

        $week = 0; //前几周
        $data = $request->all();
        if (!isset($data['shop_id'])) {
            $shop_sql = "select Id,shop_name from shop where platform_id=7 and state=1";
            $shop = json_decode(json_encode(db::select($shop_sql)), true);
            if (!$shop) {
                return '没有shopee店铺';
            }
            $data['shop_id'] = $shop[0]['Id'];
        }
        if (is_array($data['shop_id'])) {
            $arr = $data['shop_id'];
            if (count($arr) == 1) {
                $shopId = $data['shop_id'];
            } else {
                $shopId = $arr;
            }
        } else {
            $shopId = $data['shop_id'];
        }

        if (!isset($data['start_date']) || !isset($data['end_date'])) {
            $data['start_date'] = date('Y-m-d');
            $data['end_date'] = date('Y-m-d', strtotime('- 7 days'));
        }
        $time = time();
        $startDateTime = $data['start_date'] . ' 00:00:00';//起始时间 日期格式
        $endDateTime = $data['end_date'] . ' 23:59:59'; //截止时间 日期格式

        $startTime = strtotime($data['start_date']);
        $startTimeLastWeek = $startTime - 7 * 86400; //起始时间7天前时间戳
        $startDateTimeLastWeek = date("Y-m-d H:i:s", $startTimeLastWeek);

        $endTimeLastWeek = $startTimeLastWeek + 7 * 86400;
        $endDateTimeLastWeek = date("Y-m-d H:i:s", $endTimeLastWeek);


        $ShopeeModel = new \App\Libs\wrapper\Shopee();
        //  $allSkuCount = $ShopeeModel->shopWeekAllgoodCount($startDateTime, $endDateTime, $shopId);
        $shopWeekSaleTotal = $ShopeeModel->shopWeekSaleTotal($startDateTime, $endDateTime, $shopId);
        $allAddFee = $ShopeeModel->shopWeekAllAdFee($startDateTime, $endDateTime, $shopId);
        $itemList = $ShopeeModel->mostProductByName($startDateTime, $endDateTime, $shopId);

        if ($shopWeekSaleTotal > 0) {
            $skuAdFee = ($allAddFee / $shopWeekSaleTotal);  //
        } else {
            $skuAdFee = 0;
        }


        $ShopeeCostModel = new \App\Models\ShopeeCostModel();

        $i = 0;

        $time1 = time();
        // echo 'crond before ' . ($time1 - $time),PHP_EOL;
        foreach ($itemList as $key => $item) {
            $i++;
            //  if ($item->cnname == 'IX9战术裤') {

            //  var_dump($item->cnname);
            $orders = $ShopeeModel->ordersByName($item->cnname, $startDateTime, $endDateTime, $shopId);
            // var_dump($orders);
            $skus = $ShopeeModel->ordersNameSku($item->cnname, $orders);
            $time2 = time();
            //  echo 'time2 ' . ($time2 - $time),PHP_EOL;
            $shopeeGoodsCost = $ShopeeModel->shopeeGoodsCostByName($item->cnname, $startDateTime, $endDateTime, $shopId);  //订单成本
            // $skuNum = $ShopeeModel->nameNumBySku($item->cnname, $startDateTime, $endDateTime, $shopId);  //sku行数
            $time5 = time();
            //  echo 'time5 ' . ($time5 - $time),PHP_EOL;
            $costOtherFee = $ShopeeModel->costOtherFeeByName($skus, $startDateTime, $endDateTime, $shopId, $orders);  //综合费用
            //  $costOtherFee = $ShopeeModel->costOtherFeeByNameQuick($item->cnname, $startDateTime, $endDateTime, $shopId);  //综合费用
            $time6 = time();
            //  echo 'time6 ' . ($time6 - $time),PHP_EOL;
            $OrderOtherFee = $ShopeeModel->OrderOtherFeeByName($skus, $startDateTime, $endDateTime, $shopId, $orders);  //手续费
            // $OrderOtherFee = $ShopeeModel->OrderOtherFeeByNameQuick($item->cnname, $startDateTime, $endDateTime, $shopId); //手续费快速计算
            $time7 = time();
            //echo 'time7 ' . ($time7 - $time),PHP_EOL;
            $ordersNumByItemTime = $ShopeeModel->ordersNumByName($item->cnname, $startDateTime, $endDateTime, $shopId);
            $orderCount = $ordersNumByItemTime->countorder;  //订单数
            $time3 = time();
            //echo 'time3 ' . ($time3 - $time),PHP_EOL;
            $salePriceByItemTime = $ShopeeModel->salePriceByNameTime($item->cnname, $startDateTime, $endDateTime, $shopId);
            $salePriceTotal = $salePriceByItemTime->pricetotal;  //销售额

            $lastNumItems = $ShopeeModel->numName($item->cnname, $startDateTimeLastWeek, $endDateTimeLastWeek, $shopId);
            $lastNum = $lastNumItems->num; //上周销量
            if ($lastNum > 0) {
                $riseRate = round((($item->nums_all - $lastNum) / $lastNum) * 100, 2) . "%";  //增长率
            } else {
                $riseRate = "100%";
            }
            if ($item->nums_all > 0) {
                $unitPrice = round(($salePriceTotal / $item->nums_all), 2);//客单价
            } else {
                $unitPrice = "0";
            }
            $cancelOrderNum = $ShopeeModel->cancelOrderNumByName($item->cnname, $startDateTime, $endDateTime, $shopId);

            $cancelNum = $cancelOrderNum->num;  //取消订单数量
            $returnOrderNum = $ShopeeModel->returnOrderNumByName($item->cnname, $startDateTime, $endDateTime, $shopId);
            $returnNum = $returnOrderNum->num;  //退货订单数量
            $time4 = time();
            //  echo 'time4 ' . ($time4 - $time),PHP_EOL;
            if (($cancelNum + $orderCount) > 0) {
                $cancelRate = round(($cancelNum / ($cancelNum + $orderCount)) * 100, 2) . "%";  //订单取消率
            } else {
                $cancelRate = "0%";
            }
            if ($item->nums_all > 0) {
                $retrnRate = round(($returnNum / $item->nums_all) * 100, 2) . "%";  //退货率
            } else {
                $retrnRate = "0%";
            }
            $adFee = $skuAdFee * $salePriceTotal;

            if ($salePriceTotal > 0) {

                // $lirunArr = $ShopeeModel->shopeeLirunBySku($salePriceTotal, $item->variation_sku, $startDateTime, $endDateTime, $shopId, $skuAdFee);//毛利
                $lirun = $salePriceTotal - $shopeeGoodsCost - $costOtherFee - $adFee - $OrderOtherFee;
                $lirun_rate = round(($lirun / $salePriceTotal) * 100, 2) . "%";
            } else {
                $lirun = 0;
                $lirun_rate = 0;
            }

            $item->item_name = $item->cnname;
            $item->order_count = $orderCount;

            $item->sale_price_total = $salePriceTotal;
            $item->lirun = round($lirun, 2);
            $item->lirun_rate = $lirun_rate;
            $item->unit_price = $unitPrice;
            $item->last_num = $lastNum;
            $item->rise_rate = $riseRate;
            $item->cancel_num = $cancelNum;
            $item->cancel_rate = $cancelRate;
            $item->return_num = $returnNum;
            $item->return_rate = $retrnRate;
            $item->allAddFee = round($allAddFee, 2);
            $item->skuAdFee = round($skuAdFee, 2);
            // $item->skuNum = $skuNum;
            $item->adFee = $adFee;
            $item->costOtherFee = $costOtherFee;
            $item->OrderOtherFee = $OrderOtherFee;
            $item->shopeeGoodsCost = $shopeeGoodsCost;
            if (trim($lirun_rate,'%') * 100 > 15) {
                $item->is_remind = 1;
            } elseif (trim($lirun_rate,'%') * 100 < 15) {
                $item->is_remind = 2;
            }

            $itemList[$key] = $item;
            $timeend = time();
            //  echo 'timeend ' . ($timeend - $time),PHP_EOL;

            //  }
        }

        $itemList = json_decode(json_encode($itemList), true);

        $return['list'] = $itemList;
        //店铺
        // $shopee_shop= $ShopeeModel->shopee_shop();
        if (is_array($return['list'])) {
            $return['total'] = $i;
            $this->back('获取成功', '200', $return);
        } else {
            $return['total'] = 0;
            $this->back($return);
        }

    }


    public function demo()
    {
        $sql = "SELECT id, ordersn,item_id,variation_id,order_item_id FROM shopee_order_items GROUP BY ordersn,item_id,variation_id,order_item_id HAVING COUNT(1) > 1";
        $list = json_decode(json_encode(db::select($sql)), true);
        $id_str = '';
        foreach ($list as $k => $v) {
            $items = Db::table('shopee_order_items')
                ->select('id', 'ordersn', 'item_id', 'variation_id', 'order_item_id', 'variation_quantity_purchased')
                ->where([
                    'ordersn' => $v['ordersn'],
                    'item_id' => $v['item_id'],
                    'variation_id' => $v['variation_id'],
                    'order_item_id' => $v['order_item_id']
                ])
                ->get()
                ->toArray();
            unset($items[0]);
            $id_str .= ',' . implode(',', array_column($items, 'id'));
        }
        $ids = explode(',', substr($id_str, 1));
        $del = Db::table('shopee_order_items')->whereIn('id', $ids)->delete();

    }


    /**
     * 统计的详细列表
     * @param Request $request
     */
    public function detail_sku(Request $request)
    {
        $data = $request->all();

        $ShopeeComm = new \App\Libs\wrapper\Shopee();
        $ShopeeOrderItemsModel = new \App\Models\ShopeeOrderItems();
        $ShopeeOrderItemsModel = $ShopeeComm->pageSearch($ShopeeOrderItemsModel, $data);
        $ShopeeOrderItems = $ShopeeOrderItemsModel->paginate($data['page_count']);
        $this->back('获取成功', '200', $ShopeeOrderItems);
    }

    /**
     * 利润计算
     * @param Request $request
     */
    public function lirun(Request $request)
    {
        $data = $request->all();
        $ShopeeComm = new \App\Libs\wrapper\Shopee();
        $startDateTime = $data['start_date'] . ' 00:00:00';//起始时间 日期格式
        $endDateTime = $data['end_date'] . ' 23:59:59'; //截止时间 日期格式

        $lirunDetail = $ShopeeComm->lirunDetail($data['sku'], $startDateTime, $endDateTime, $data['shop_id']);
        $lirunDetail['sku'] = $data['sku'];
        $return[] = $lirunDetail;
        $this->back('获取成功', '200', $return);
    }

    /**
     * 统计的详细列表
     * @param Request $request
     */
    public function name_sku(Request $request)
    {
        $data = $request->all();

        $ShopeeComm = new \App\Libs\wrapper\Shopee();
        $ShopeeOrderItemsModel = new \App\Models\ShopeeOrderItems();
        $ShopeeOrderItemsModel = $ShopeeComm->pageSearch($ShopeeOrderItemsModel, $data);
        $ShopeeOrderItems = $ShopeeOrderItemsModel->paginate($data['page_count']);
        $this->back('获取成功', '200', $ShopeeOrderItems);
    }


    /**
     * 同步数据
     */
    public function tongbu(Request $request)
    {
        set_time_limit(0);


    }


    /**
     * 处理bug
     */
    public function bug()
    {

    }


//
}
