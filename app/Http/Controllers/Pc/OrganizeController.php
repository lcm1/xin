<?php
namespace App\Http\Controllers\Pc;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;


class OrganizeController extends Controller{
/////////////////////////////////////////////// 组织（部门、小组）
////////////////////// 组织列表
	/**
	 * 组织列表--数据
	 * @param name 部门名称
	 * @Parma type 类型：1.部门、2.小组
	 */
	public function organize_list(Request $request){
		$data = $request->all();
		$call_organize = new \App\Libs\wrapper\Organize();
		$return = $call_organize->organize_list($data);
		if(is_array($return['list'])){
			$this->back('获取成功', '200', $return);
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 新增组织
	 * @parma name 名称
	 * @param desc 说明
	 * @apram type 类型：1.部门、2.小组
	 */
	public function organize_add_depart(Request $request){
		$this->validate($request, [
			'name' => 'required| string',
		], [
			'name.required' => 'name为空',
			'name.string' => 'name必须是整型数字',
		]);
		
		$data = $request->all();
		$call_organize = new \App\Libs\wrapper\Organize();
		$return = $call_organize->organize_add_depart($data);
		if($return === 1){
			$this->back('新增成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 编辑组织
	 * @param Id 组织id
	 */
	public function organize_update(Request $request){
		$this->validate($request, [
			'Id' => 'required| integer',
		], [
			'Id.required' => 'Id为空',
			'Id.integer' => 'Id必须是整型数字',
		]);
		
		$data = $request->all();
		$call_organize = new \App\Libs\wrapper\Organize();
		$return = $call_organize->organize_update($data);
		if($return === 1){
			$this->back('修改成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 删除组织
	 * @param Id 组织id
	 */
	public function organize_del(Request $request){
		$this->validate($request, [
			'Id' => 'required| integer',
		], [
			'Id.required' => 'Id为空',
			'Id.integer' => 'Id必须是整型数字',
		]);
		
		$data = $request->all();
		$call_organize = new \App\Libs\wrapper\Organize();
		$return = $call_organize->organize_del($data);
		if($return === 1){
			$this->back('删除成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 成员列表--数据
	 * @param organize_id 组织id
	 * @param page 页码
	 * @param number 每页条数
	 */
	public function member_list(Request $request){
		$data = $request->all();
		if(empty($data)){
			$this->back('接收值为空');
		}
		
		$call_organize = new \App\Libs\wrapper\Organize();
		$return = $call_organize->member_list($data);
		if(is_array($return['list'])){
			$this->back('获取成功', '200', $return);
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 用户列表--数据
	 */
	public function user_list(Request $request){
		$sql = "select `Id`, `account`
					from users
					where `state`=1";
		$list = json_decode(json_encode(db::select($sql)),true);
		
		if(is_array($list)){
			$this->back('获取成功', '200', $list);
		}else{
			$this->back('获取失败');
		}
	}
	
	/**
	 * 获取可新增成员列表--数据
	 * @param organize_id 组织id
	 */
	public function user_data(Request $request){
		$data = $request->all();
		$call_organize = new \App\Libs\wrapper\Organize();
		$return = $call_organize->user_data($data);
		if(is_array($return['list'])){
			$this->back('获取成功', '200', $return);
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 新增成员
	 * @param organize_id 组织id
	 * @param user_ids 用户id  例如：1,2,3
	 */
	public function member_add(Request $request){
		$this->validate($request, [
			'organize_id' => 'required| integer',
			'user_ids' => 'required| string',
		], [
			'organize_id.required' => 'organize_id为空',
			'organize_id.integer' => 'organize_id必须是整型数字',
			'user_ids.required' => 'user_ids为空',
			'user_ids.integer' => 'user_ids必须字符串',
		]);
		
		$data = $request->all();
		$call_organize = new \App\Libs\wrapper\Organize();
		$return = $call_organize->member_add($data);
		if($return === 1){
			$this->back('新增成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 编辑组织成员信息
	 * @param Id 组织成员id
	 */
	public function member_update(Request $request){
		$this->validate($request, [
			'Id' => 'required| integer',
		], [
			'Id.required' => 'Id为空',
			'Id.integer' => 'Id必须是整型数字',
		]);
		
		$data = $request->all();
		$call_organize = new \App\Libs\wrapper\Organize();
		$return = $call_organize->member_update($data);
		if($return === 1){
			$this->back('修改成功', '200');
		}else{
			$this->back($return);
		}
	}
 
	/**
	 * 移除成员
	 * @parma Id 组织id
	 */
    public function member_del(Request $request){
	    $this->validate($request, [
		    'Id' => 'required| integer',
	    ], [
		    'Id.required' => 'Id为空',
		    'Id.integer' => 'Id必须是整型数字',
	    ]);
	
	    $data = $request->all();
	    $call_organize = new \App\Libs\wrapper\Organize();
	    $return = $call_organize->member_del($data);
//		var_dump($return);exit;
	    if($return === 1){
		    $this->back('移除成功', '200');
	    }else{
		    $this->back($return);
	    }
    }
    
    
	//获取部门树状图
	public function getChildrenOfA(Request $request)
	{
		$data = $request->all();
		$id = $data['id']??0;

		$data = [];
		if($id==0){
			$list = DB::table('organizes')->where('fid',0)->where('type',1)->where('state',1)->get();
			foreach ($list as $v) {
				// # code...
				// $one = [];
				// $one = $v;
                $childen = $this->findAllChildren($v->Id);
                $v->children = $childen;
                if ($childen->isEmpty()){
                    unset($v->children);
                }
				$data[] = $v;
			}
		}else{
			$data = $this->findAllChildren($id); // Assuming 'a' has an ID of 1
		}


		$this->back('获取成功！', 200, $data);
	}

	//获取子成员
	private function findAllChildren($parentId)
	{
		$results = DB::table('organizes')->where('fid', $parentId)->get();

		foreach ($results as $result) {
			$childResults = $this->findAllChildren($result->Id);
            $result->children = $childResults;
            if ($childResults->isEmpty()){
                unset($result->children);
            }
		}

		return $results;
	}
 
}//
