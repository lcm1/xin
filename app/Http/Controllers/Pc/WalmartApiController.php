<?php

namespace App\Http\Controllers\Pc;

use Illuminate\Support\Facades\Redis;

class WalmartApiController
{
    public  $shop_id;
    private $client_id;
    private $client_secret;

    public function __construct($shop_id, $client_id, $client_secret)
    {
        $this->shop_id       = $shop_id;
        $this->client_id     = $client_id;
        $this->client_secret = $client_secret;
    }

    //沃尔玛API
    public function walmartApi($curl, $type, $data = [])
    {
        $url = "https://marketplace.walmartapis.com";
        //常规参数
        $uniqid            = uniqid();
        $authorization_key = base64_encode($this->client_id . ":" . $this->client_secret);
        $header            = [];
        $header[]          = "WM_SVC.NAME:Walmart Marketplace";
        $header[]          = "WM_QOS.CORRELATION_ID:{$uniqid}";
        $header[]          = "Authorization: Basic {$authorization_key}";
        $header[]          = "Accept: application/json";
        $header[]          = "Content-Type: application/x-www-form-urlencoded";
        $strData           = '';

        if ($type == 'token') {
            $tokenKey = 'walmart_access_token_' . $this->shop_id;
            $token    = Redis::get($tokenKey);
            if ($token) {
                return $token;
            }
            $url     .= "/v3/token";
            $strData = "grant_type=client_credentials";
            $res     = $this->httpRequest('post', $url, $header, $strData);
            if (isset($res['access_token'])) {
                Redis::set($tokenKey, $res['access_token']);
                Redis::expire($tokenKey, 900);//有效期15分钟
                return $res['access_token'];
            } else {
                echo '获取token错误，终止程序!';
                return false;
            }
        }

        $log['name'] = 'walmartApi';
        $log['shop_id'] = $this->shop_id;
        $log['client_id'] = $this->client_id;
        $log['client_secret'] = $this->client_secret;
        $this->writeLog(var_export($log, true));
        
        $token    = $this->walmartApi('post', 'token');
        $header[] = "WM_SEC.ACCESS_TOKEN:{$token}";

        $url .= $type;

        if ($curl == 'get') {
            if ($data) {
                $url .= "?{$data}";
            }
        } elseif ($curl == 'post') {
            if ($data) {
                $strData = json_encode($data);
            }
        }
        return $this->httpRequest($curl, $url, $header, $strData);
    }

    /**
     * curl 请求
     * @param $type
     * @param $url
     * @param array $header
     * @param $data
     * @return mixed
     */
    private function httpRequest($type, $url, array $header, $data)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE); // https请求 不验证证书和hosts
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        if ($type != 'get') {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $output = curl_exec($curl);

        //打印请求日志
        $log = [
            'url'    => $url,
            'header' => $header,
            'data'   => $data,
            'output' => $output,
        ];
        $this->writeLog(var_export($log, true));


        if ($output === false) {
            return false;
        }

        return json_decode($output, true);

    }

    private function writeLog($log)
    {
        $logFile = fopen(
            storage_path('logs' . DIRECTORY_SEPARATOR . date('Y-m-d').'walmart_order_job.log'),
            'a'
        );
        fwrite($logFile, date('Y-m-d H:i:s') . ': ' . $log . PHP_EOL);
        fclose($logFile);
    }

}