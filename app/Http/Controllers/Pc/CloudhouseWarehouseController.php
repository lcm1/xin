<?php

namespace App\Http\Controllers\Pc;

use App\Http\Controllers\Controller;
use App\Models\CloudhouseWarehouseModel;
use Illuminate\Http\Request;

class CloudhouseWarehouseController extends Controller
{
    protected $model;
    protected $request;
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->model = new CloudhouseWarehouseModel();
    }

    public function getCloudhouseWarehouseList()
    {
        $params = $this->request->all();
        $result = $this->model->getCloudhouseWarehouseList($params);

        return $this->back($result['msg'], $result['code'], $result['data']);

    }

    /**
     * @Desc:保存仓库
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/3/22 16:19
     */
    public function saveCloudhouseWarehouse()
    {
        $params = $this->request->all();
        $result = $this->model->saveCloudhouseWarehouse($params);

        return $this->back($result['msg'], $result['code'], $result['data'] ?? '');
    }

    /**
     * @Desc:删除仓库
     * @author: Liu Sinian
     * @Time: 2023/3/22 16:19
     */
    public function deleteCloudhouseWarehouse()
    {
        $params = $this->request->all();
        $result = $this->model->deleteCloudhouseWarehouse($params);

        return $this->back($result['msg'], $result['code'], $result['data'] ?? '');
    }

    public function getCloudhouseWarehouseLocationList()
    {
        $params = $this->request->all();
        $result = $this->model->getCloudhouseWarehouseLocationList($params);

        return $this->back($result['msg'], $result['code'], $result['data']);
    }

    /**
     * @Desc:保存库位
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/3/22 16:19
     */
    public function saveCloudhouseWarehouseLocation()
    {
        $params = $this->request->all();
        $result = $this->model->saveCloudhouseWarehouseLocation($params);

        return $this->back($result['msg'], $result['code'], $result['data'] ?? '');
    }

    /**
     * @Desc:删除库位
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/5/3 11:28
     */
    public function deleteCloudhouseWarehouseLocation()
    {
        $params = $this->request->all();
        $result = $this->model->deleteCloudhouseWarehouseLocation($params);

        return $this->back($result['msg'], $result['code'], $result['data'] ?? '');
    }

    /**
     * @Desc:获取库位库存列表
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/5/3 11:28
     */
    public function getCloudhouseWarehouseLocationStockList()
    {
        $params = $this->request->all();
        $result = $this->model->getCloudhouseWarehouseLocationStockList($params);

        return $this->back('获取成功', '200', $result);
    }

    /**
     * @Desc:获取库位库存变动日志列表
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/5/3 11:28
     */
    public function getCloudhouseWarehouseLocationLogList()
    {
        $params = $this->request->all();
        $result = $this->model->getCloudhouseWarehouseLocationLogList($params);

        return $this->back($result['msg'], $result['code'], $result['data'] ?? '');
    }

    /**
     * @Desc: 库位库存移动
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/6/25 9:02
     */
    public function moveWarehouseLocationInventory()
    {
//        return "系统调整中...";
        $params = $this->request->all();
        $result = $this->model->moveWarehouseLocationInventory($params);

        return $this->back($result['msg'], $result['code'], $result['data'] ?? '');
    }

    /**
     * @Desc:更新仓库资产
     * @author: Liu Sinian
     * @Time: 2023/7/25 17:22
     */
    public function updateCloudhouseWarehouseProperty()
    {
        $params = $this->request->all();
        $result = $this->model->updateCloudhouseWarehouseProperty($params);

        return $this->back($result['msg'], $result['code'], $result['data'] ?? '');
    }

    /**
     * @Desc:获取平台库存列表
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/8/7 14:59
     */
    public function getPlatformInventoryList()
    {
        $params = $this->request->all();
        $result = $this->model->getPlatformInventoryList($params);

        return $this->back('获取成功', '200', $result);
    }


      /**
     * @Desc:EXcel平台库存列表查询
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/8/7 14:59
     */
    public function ExcelPlatformInventoryList()
    {
        $params = $this->request->all();
        $result = $this->model->ExcelPlatformInventoryList($params);

        if($result['type']=='fail'){
            return $this->back('获取成败', '300', $result['msg']);
        }else{
            return $this->back('获取成功', '200', $result);
        }

      
    }



      /**
     * @Desc:获取平台库存列表导入查询
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/8/7 14:59
     */
    public function PlatformInventoryExcelImport()
    {
        $params = $this->request->all();
        $result = $this->model->PlatformInventoryExcelImport($params);

        return $this->back('获取成功', '200', $result);
    }

    

    /**
     * @Desc:获取库存详情列表
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/8/7 15:00
     */
    public function getInventoryDetailList(){
        $params = $this->request->all();
        $result = $this->model->getInventoryDetailList($params);

        return $this->back($result['msg'], $result['code'], $result['data'] ?? '');
    }

    /**
     * @Desc:扫码获取库位信息
     * @author: Liu Sinian
     * @Time: 2023/8/23 21:36
     */
    public function getLocationInfo()
    {
        try {
            $params = $this->request->all();
            if (!isset($params['warehouse_id'])){
                throw new \Exception("未给定仓库标识！");
            }
            if (!isset($params['location_code'])){
                throw new \Exception("未给定库位标识！");
            }
            $result = $this->model->getLocationInfo($params);

            return $this->back('获取成功！', 200, $result['data'] ?? '');
        }catch (\Exception $e){
            return $this->back('获取失败！原因'.$e->getMessage().'；位置：'.$e->getLine(), 500);
        }

    }
}