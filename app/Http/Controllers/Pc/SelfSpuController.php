<?php

namespace App\Http\Controllers\Pc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SelfSpu;
use App\Http\Requests\SelfSpu as SelfSpus;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class SelfSpuController extends Controller
{
    /**
     * spu/sku生成模块
     * $model 模型
     * request 参数
     */

    private $model;
    private $request;
    private $validate;

    public function __construct(Request $request)
    {
        $this->model = new SelfSpu();
        $this->validate = new SelfSpus();
        $this->request = $request;
    }

    //分类excel导入
    public function CateImport()
    {
        $params = $this->validate->scene('CateImport')->check($this->request);
        $result = $this->model->expord_old_spu($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    //获取分类
    public function CateList()
    {
        $params = $this->request->all();
        $result = $this->model->CateList($params);
        return $this->back('获取成功', '200', $result);
    }

    //分类添加
    public function CateAdd()
    {
        $params = $this->validate->scene('CateAdd')->check($this->request);
        $result = $this->model->CateAdd($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    //分类修改
    public function CateUpdate()
    {
        $params = $this->validate->scene('CateUpdate')->check($this->request);
        $result = $this->model->CateUpdate($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    //分类删除
    public function CateDel()
    {
        $params = $this->validate->scene('CateDel')->check($this->request);
        $result = $this->model->CateDel($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    //获取分类
    public function ColorSizeList()
    {
        $params = $this->request->all();
        $result = $this->model->ColorSizeList($params);
        return $this->back('获取成功', '200', $result);
    }

     //获取色系列表分类
     public function ColorClassList()
     {
         $params = $this->request->all();
         $result = $this->model->ColorClassList($params);
         return $this->back('获取成功', '200', $result);
     }

    //色系添加
    public function ColorClassAdd()
    {
        $params = $this->request->all();
        $result = $this->model->ColorClassAdd($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    //色系删除
    public function ColorClassDel()
    {
        $params = $this->request->all();
        $result = $this->model->ColorClassDel($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }


    //颜色添加
    public function ColorAdd()
    {
        $params = $this->request->all();
        $result = $this->model->ColorAdd($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200',$result['data']);
        } else {
            return $this->back($result['msg'], '300',$result['data']);
        }
    }



    //获取spu颜色
    public function GetSpuColorBySpu()
    { 
        $params = $this->request->all();
        $result = $this->model->GetSpuColorBySpu($params);
        return $this->back('获取成功', '200', $result);
    }

    //获取spu颜色
    public function GetSpuColorByBaseSpu()
    { 
        $params = $this->request->all();
        $result = $this->model->GetSpuColorByBaseSpu($params);
        return $this->back('获取成功', '200', $result);
    }


     //获取spu尺码
     public function SpuSize()
     {
         $params = $this->request->all();
         $result = $this->model->SpuSize($params);
         return $this->back('获取成功', '200', $result);
     }

     public function SpuSizeAdd()
     {
        $params = $this->request->all();
         $result = $this->model->SpuSizeAdd($params);
         if ($result['type'] == 'success') {
             return $this->back($result['msg'], '200');
         } else {
             return $this->back($result['msg'], '300');
         }
     }

     public function SpuSizeDel()
     {
        $params = $this->request->all();
         $result = $this->model->SpuSizeDel($params);

         $this->AddSysLog($params,$result,'删除spu尺码',__METHOD__);
         
         if ($result['type'] == 'success') {
             return $this->back($result['msg'], '200');
         } else {
             return $this->back($result['msg'], '300');
         }
     }
 

    //分类添加
    public function ColorSizeAdd()
    {
        $params = $this->validate->scene('ColorSizeAdd')->check($this->request);
        $result = $this->model->ColorSizeAdd($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    //分类修改
    public function ColorSizeUpdate()
    {
        $params = $this->validate->scene('ColorSizeUpdate')->check($this->request);
        $result = $this->model->ColorSizeUpdate($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    //分类删除
    public function ColorSizeDel()
    {
        $params = $this->validate->scene('ColorSizeDel')->check($this->request);
        $result = $this->model->ColorSizeDel($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    //基础spu-获取市场
    public function GetMarket()
    {
        $params = $this->request->all();
        $result = $this->model->GetMarket($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200',$result['data']);
        } else {
            return $this->back($result['msg'], '300');
        }
    }


    //基础spu生成
    public function SpuPreviewByBase()
    {
        $params = $this->request->all();
        $result = $this->model->SpuPreviewByBase($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200',$result['spu']);
        } else {
            return $this->back($result['msg'], '300');
        }
    }



    //spu生成
    public function SpuPreview()
    {
        $params = $this->validate->scene('SpuPreview')->check($this->request);
        $result = $this->model->SpuPreview($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    //分类修改
    public function SpuUpdate()
    {
        $params = $this->request->all();
        $result = $this->model->SpuUpdate($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    
    //基础spu删除
    public function DelBaseSpu()
    {
        $params = $this->request->all();
        $result = $this->model->DelBaseSpu($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }
    //spu删除
    public function SpuDel()
    {
        $params = $this->validate->scene('SpuDel')->check($this->request);
        $result = $this->model->SpuDel($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }
    
    //spu列表-组合
    public function SpuListByzuhe()
    {
        $params = $this->request->all();
        $result = $this->model->SpuListByzuhe($params);
        return $this->back('获取成功', '200', $result);
    }

    public function updatetest(){
        $params = $this->request->all();
        $result = $this->model->updatetest($params);
        return $this->back('获取成功', '200', $result);
    }
    //spu列表
    public function SpuListByBase()
    {
        $params = $this->request->all();
        $result = $this->model->SpuListByBase($params);
        return $this->back('获取成功', '200', $result);
    }


    //库存sku获取供应商
    public function GetCusSuppliers()
    {
        $params = $this->request->all();
        $result = $this->model->GetCusSuppliersM($params);
        return $this->back('获取成功', '200', $result);
    }


    //asin获取库存sku
    public function AsinGetCustomSku()
    {
        $params = $this->request->all();
        $result = $this->model->AsinGetCustomSkuM($params);
        return $this->back('获取成功', '200', $result);
    }


    //spu列表
    public function SpuList()
    {
        $params = $this->request->all();
        $result = $this->model->SpuList($params);
        return $this->back('获取成功', '200', $result);
    }

    
    //spu下拉框
    public function SpuSelect()
    {
        $params = $this->request->all();
        $result = $this->model->SpuSelect($params);
        return $this->back('获取成功', '200', $result);
    }


    //spu颜色添加
    public function SelfSpuColorAdd()
    {
        $params = $this->request->all();
        $result = $this->model->SelfSpuColorAdd($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }


    //spu颜色修改
    public function SelfSpuColorEdit()
    {
        $params = $this->request->all();
        $result = $this->model->SelfSpuColorEdit($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }


    //spu颜色删除
    public function SelfSpuColorDel()
    {
        $params = $this->validate->scene('SelfSpuColorDel')->check($this->request);
        $result = $this->model->SelfSpuColorDel($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }


        //spu下单颜色禁用
    public function SelfSpuColorPlaceOrderAdd()
    {
        $params = $this->request->all();
        $result = $this->model->SelfSpuColorPlaceOrderAdd($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    public function SelfSpuColorPlaceOrderDel()
    {
        $params = $this->request->all();
        $result = $this->model->SelfSpuColorPlaceOrderDel($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }



    
    //组合spu详情
    public function GetGroupSpu()
    {
        $params = $this->validate->scene('GetGroupSpu')->check($this->request);
        $result = $this->model->GetGroupSpu($params);
        return $this->back('获取成功', '200', $result);
    }   
    //sku 预览
    public function SkuPreview()
    {
        $params = $this->validate->scene('SkuPreview')->check($this->request);
        $result = $this->model->SkuPreview($params);
        if ($result['type'] == 'success') {
            return $this->back('预览成功', '200', $result['data']);
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    //库存sku 生成
    public function CustomSkuCreate()
    {
        $params = $this->validate->scene('CustomSkuCreate')->check($this->request);
        $result = $this->model->CustomSkuCreate($params);
        if ($result['type'] == 'success') {
            return $this->back('生成成功', '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    //库存sku 删除
    public function CustomSkuDel()
    {
        $params = $this->validate->scene('CustomSkuDel')->check($this->request);
        $result = $this->model->CustomSkuDel($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    //库存sku列表
    public function CustomSkuList()
    {
        $params = $this->request->all();
        $result = $this->model->CustomSkuList($params);
        return $this->back('获取成功', '200', $result);
    }  

    //销售sku 生成
    public function SkuCreate()
    {
        $params = $this->validate->scene('SkuCreate')->check($this->request);
        $result = $this->model->SkuCreate($params);
        if ($result['type'] == 'success') {
            return $this->back('生成成功', '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }
    //销售sku 删除
    public function SkuDel()
    {
        $params = $this->validate->scene('SkuDel')->check($this->request);
        $result = $this->model->SkuDel($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    //销售sku 上下架
    public function Sku_update()
    {
        $params = $this->request->all();
        $result = $this->model->Sku_update($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    //销售sku列表
    public function SkuList()
    {
        $params = $this->request->all();
        $result = $this->model->SkuList($params);
        return $this->back('获取成功', '200', $result);
    }   
        
    public function SpuOldBind()
    {
        $params = $this->request->all();
        $result = $this->model->SpuOldBind($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }
    

    //组合spu生成
    public function ZuheSkuPreview()
    {
        $params = $this->request->all();
        $result = $this->model->ZuheSkuPreview($params);
        $this->AddSysLog($params,$result,'生产组合sku',__METHOD__);
        if ($result['type'] == 'success') {
            return $this->back('预览成功', '200', $result['data']);
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    //组合spu颜色列表
    public function ZuheSpuList()
    {
         $params = $this->request->all();
         $result = $this->model->ZuheSpuList($params);
         if ($result['type'] == 'success') {
             return $this->back('预览成功', '200', $result['data']);
         } else {
             return $this->back($result['msg'], '300');
         }
     }

    //组合spu-组合库存sku详情
    public function GetZuheCustomSku()
    {
        $params = $this->request->all();
        $result = $this->model->GetZuheCustomSku($params);
        return $this->back('获取成功', '200', $result);
    }
     
    //修改组合库存sku名字 UpdateZuheCustomSku
    public function UpdateZuheCustomSku()
    {
        $params = $this->request->all();
        $result = $this->model->UpdateZuheCustomSku($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }
    


    //绑定旧sku导入
    public function ExpordBindSku()
    {
        $params =$this->request->all();
        $result = $this->model->expord_bind_sku($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    //设置类目负责人
    public function setSupervisor(){

        $params =$this->request->all();

        $supervisor = implode(',',$params['supervisor_id']);

        $update = DB::table('self_category')->where('id',$params['id'])->update(['supervisor_id'=>$supervisor]);
        Redis::Hdel('datacache:self_category',$params['id']);
        if($update){
            return $this->back('设置成功', '200');
        }else{
            return $this->back('设置失败', '200');
        }

    }


    //清除缓存
    public function DelCache()
    {
        $params = $this->request->all();
        $result = $this->model->DelCache($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back('删除失败', '300');
        }
    }



    //添加产品分类
    public function AddProductCate()
    {
        $params =$this->request->all();
        $result = $this->model->AddProductCate($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    //删除产品分类
    public function DelProductCate()
    {
        $params =$this->request->all();
        $result = $this->model->DelProductCate($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    
    //一次性获取所有分类
    public function CateListAll()
    {
         $params = $this->request->all();
         $result = $this->model->CateListAll($params);
         if ($result['type'] == 'success') {
             return $this->back('获取成功', '200', $result['data']);
         } else {
             return $this->back($result['msg'], '300');
         }
     }

     //修改产品分类
     public function UpdateProductCateInfo(){
        $params =$this->request->all();
        $result = $this->model->UpdateProductCateInfo($params);
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }

     }

    //查看产品分类
    public function GetProductCate()
    {
         $params = $this->request->all();
         $result = $this->model->GetProductCate($params);
         if ($result['type'] == 'success') {
             return $this->back('获取成功', '200', $result['data']);
         } else {
             return $this->back($result['msg'], '300');
         }
     }

     //设置面料
     public function AddProductCateInfo()
     {
         $params =$this->request->all();
         $result = $this->model->AddProductCateInfo($params);
         if ($result['type'] == 'success') {
             return $this->back($result['msg'], '200');
         } else {
             return $this->back($result['msg'], '300');
         }
     }


     //查询面料
     public function GetProductCateInfo()
     {
         $params =$this->request->all();
         $result = $this->model->GetProductCateInfo($params);
         if ($result['type'] == 'success') {
            return $this->back('获取成功', '200', $result['data']);
        } else {
            return $this->back($result['msg'], '300');
        }
     }

    //删除面料
     public function DelProductCateInfo()
     {
         $params =$this->request->all();
         $result = $this->model->DelProductCateInfo($params);
         if ($result['type'] == 'success') {
             return $this->back($result['msg'], '200');
         } else {
             return $this->back($result['msg'], '300');
         }
     }

    //根据base_spu_id查询面料选项
    public function GetProductCateInfoByBaseSpuId()
    {
        $params =$this->request->all();
        $result = $this->model->GetProductCateInfoByBaseSpuId($params);
        if ($result['type'] == 'success') {
            return $this->back('获取成功', '200', $result['data']);
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    //根据user_id,base_spu_id查询是否有权限
    public function GetProductCateAuthByBaseSpuId()
    {
        $params =$this->request->all();
        $result = $this->model->GetProductCateAuthByBaseSpuId($params);
        if ($result['type'] == 'success') {
            return $this->back('获取成功', '200', $result['data']);
        } else {
            return $this->back($result['msg'], '300');
        }
    }


    
    public function SendAndBindSkuI()
    {
        $params = $this->request->all();
        $result = $this->model->SendAndBindSkuI($params);

        if($result['type']=='success'){
            return $this->back('上传成功', '200',$result);
        }else{
            return $this->back($result['msg'], '300');
        }

    }
}
