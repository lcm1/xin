<?php
namespace App\Http\Controllers\Pc;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class SupplierController extends Controller{
//////////////////////////////////////////////// 供应商
////////////////////// 供应商列表
	/**
	 * 供应商列表--数据
	 * @param name 名称
	 * @param supplier_no 供应商编码
	 * @param level_dq 当前等级
	 */
	public function supplier_list(Request $request){
		$data = $request->all();
		if(empty($data)){
			$this->back('接收值为空');
		}
		
		$call_supplier = new \App\Libs\wrapper\Supplier();
		$return = $call_supplier->supplier_list($data);
//		var_dump($return);exit;
		if(is_array($return['list'])) {
			$this->back('获取成功', '200', $return);
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 新增供应商
	 * @param name 名称
	 * @logic 名称唯一
	 */
	public function supplier_add(Request $request){
		$this->validate($request, [
			'name' => 'required| string',
		], [
			'name.required' => 'name为空',
			'name.string' => 'name必须是字符串',
		]);
		
		$data = $request->all();
		$call_supplier = new \App\Libs\wrapper\Supplier();
		$return = $call_supplier->supplier_add($data);
//		var_dump($return);exit;
		if($return === 1) {
			$this->back('新增成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 供应商详情
	 * @param Id 供应商id
	 */
	public function supplier_info(Request $request){
		$this->validate($request, [
			'Id' => 'required| integer',
		], [
			'Id.required' => 'Id为空',
			'Id.integer' => 'Id必须是数字',
		]);
		
		$data = $request->all();
		$call_supplier = new \App\Libs\wrapper\Supplier();
		$return = $call_supplier->supplier_info($data);
		if(is_array($return)){
			$this->back('获取成功', '200', $return);
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 编辑供应商
	 * @param Id
	 * @parma name 名称
	 * @logic 名称唯一
	 */
	public function supplier_update(Request $request){
		$this->validate($request, [
			'Id' => 'required| integer',
		], [
			'Id.required' => 'Id为空',
			'Id.integer' => 'Id必须是数字',
		]);
		
		$data = $request->all();
		$call_supplier = new \App\Libs\wrapper\Supplier();
		$return = $call_supplier->supplier_update($data);
//		var_dump($return);exit;
		if($return === 1) {
			$this->back('编辑成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 删除供应商
	 * @param Id
	 */
	public function supplier_del(Request $request){
		$this->validate($request, [
			'Id' => 'required| integer',
		], [
			'Id.required' => 'Id为空',
			'Id.integer' => 'Id必须是数字',
		]);
		
		$data = $request->all();
		$call_supplier = new \App\Libs\wrapper\Supplier();
		$return = $call_supplier->supplier_del($data);
//		var_dump($return);exit;
		if($return === 1) {
			$this->back('删除成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	





}//类结束