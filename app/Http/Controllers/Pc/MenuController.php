<?php
namespace App\Http\Controllers\Pc;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MenuController extends Controller{
//////////////////////////////////////////////// 菜单
////////////////////// 菜单列表
	/**
	 * 菜单列表
	 * @param name 菜单名称
	 * @param fid 父id
	 */
	public function menu_list(Request $request){
		$data = $request->all();
		if(empty($data)){
			$this->back('接收值为空');
		}
		
		$call_menu = new \App\Libs\wrapper\Menu();
		$return = $call_menu->menu_list($data);
		if(is_array($return)){
			$this->back('成功', '200', $return);
		}else{
			$this->back('获取数据失败');
		}
	}

    /**
     * @Desc:获取菜单列表
     * @param Request $request
     * @author: Liu Sinian
     * @Time: 2023/2/21 18:03
     */
    public function getMenuList(Request $request){
        $data = $request->all();
        if(empty($data)){
            $this->back('接收值为空');
        }
        if (!isset($data['level']))
        {
            $this->back('未设置菜单level参数');
        }

        $call_menu = new \App\Libs\wrapper\Menu();
        $return = $call_menu->getMenuList($data);
        if(is_array($return)){
            $this->back('成功', '200', $return);
        }else{
            $this->back('获取数据失败');
        }
    }
	
	/**
	 * 新增菜单
	 * @param name 名称
	 * @param url 路径
	 * @param icon 图标
	 * @param fid 父id
	 * @param serial_number 序号
	 * @param identity 权限标识
	 */
	public function menu_add(Request $request){
		//验证参数
		$this->validate($request, [
			'name' => 'required| string',
		], [
			'name.required' => '请填写菜单名称',
			'name.string' => 'name必须是字符串',
		]);
		
		$data = $request->all();
		$call_menu = new \App\Libs\wrapper\Menu();
		$return = $call_menu->menu_add($data);
		if($return === 1){
			$this->back('新增成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 菜单信息
	 * @param menu_id 菜单id
	 */
	public function menu_info(Request $request){
		//验证参数
		$this->validate($request, [
			'menu_id' => 'required| integer',
		], [
			'menu_id.required' => 'menu_id为空',
			'menu_id.integer' => 'menu_id不是整型数据',
		]);
		
		$data = $request->all();
		$call_menu = new \App\Libs\wrapper\Menu();
		$return = $call_menu->menu_info($data);
		if(is_array($return)){
			$this->back('获取成功', '200', $return[0]);
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 编辑菜单
	 * @apram Id 菜单id
	 * @param name 菜单名称
	 * @param url 路径
	 * @param icon 图标
	 * @param fid 父id
	 * @param serial_number 序号
	 * @param identity 权限标识
	 */
	public function menu_update(Request $request){
		//验证参数
		$this->validate($request, [
			'Id' => 'required| integer',
			'name' => 'required| string'
		], [
			'Id.required' => 'Id为空',
			'Id.integer' => 'Id不是整型数据',
			'name.required' => '请填写菜单名称',
			'name.string' => 'name不是字符串',
		]);
		
		$data = $request->all();
//		var_dump($data);exit;
		$call_menu = new \App\Libs\wrapper\Menu();
		$return = $call_menu->menu_update($data);
//		var_dump($return);exit;
		if($return === 1){
			$this->back('编辑成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 删除菜单
	 * @param Id 菜单id
	 */
	public function menu_del(Request $request){
		//验证参数
		$this->validate($request, [
			'Id' => 'required| integer',
		], [
			'Id.required' => 'Id为空',
			'Id.integer' => 'Id必须是整型数据',
		]);
		
		$data = $request->all();
		$call_menu = new \App\Libs\wrapper\Menu();
		$return = $call_menu->menu_del($data);
		if($return === 1){
			$this->back('删除成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	
	
	
	/**
	 * 新增权限
	 * @param name 权限名称
	 * @param identity 权限标识
	 * @param type_id 权限分类id
	 * @param type 类型：1查询、2操作
	 */
	public function power_add(Request $request){
		//验证参数
		$this->validate($request, [
			'name' => 'required| string',
			'identity' => 'required| string',
			'type_id' => 'required| integer',
			'type' => 'required| integer',
		], [
			'name.required' => '请填写权限名称',
			'name.string' => 'name必须是字符串',
			'identity.required' => '请填写权限标识',
			'identity.string' => 'identity必须是字符串',
			'type_id.required' => 'type_id为空',
			'type_id.integer' => 'type_id必须是整型数据',
			'type.required' => '请选择权限类型',
			'type.integer' => 'type必须是整型数据',
		]);
		
		$data = $request->all();
		$call_powers = new \App\Libs\wrapper\Powers();
		$return = $call_powers->power_add($data);
		if($return === 1){
			$this->back('新增成功','200');
		}else{
			$this->back($return);
		}
	}
	
    
    
    /**
     * 编辑权限
     * @apram power_id 权限id
     * @param name 权限名称
     * @param identity 权限标识
     * @param type_id 权限分类id
     * @param type 类型：1查询、2操作
     */
    public function power_update(Request $request){
	    //验证参数
	    $this->validate($request, [
		    'power_id' => 'required| integer',
		    'name' => 'required| string',
		    'identity' => 'required| string',
		    'type' => 'required| integer'
	    ], [
		    'power_id.required' => 'power_id为空',
		    'power_id.integer' => 'power_id必须是整型数据',
		    'name.required' => '请填写权限名称',
		    'name.string' => 'name不是字符串',
		    'identity.required' => '请填写权限标识',
		    'identity.string' => 'identity必须是字符串',
		    'type.required' => '请选择权限类型',
		    'type.integer' => 'type必须是整型数据',
	    ]);
        $data = $request->all();
        if(empty($data)){
            $this->back('接收值为空');
        }
	    $call_powers = new \App\Libs\wrapper\Powers();
	    $return = $call_powers->power_update($data);
        if($return === 1){
            $this->back('编辑成功', '200');
        }else{
            $this->back($return);
        }
    }

    /**
     * 新增菜单说明文档
     * @param content 内容
     * @param title 标题
     * @param menus_id 菜单id
     */
    public function menu_doc_add(Request $request){
        //验证参数
        $this->validate($request, [
            'content' => 'required| string',
            'title' => 'required| string',
            'menus_id' => 'required| integer',
        ], [
            'content.required' => '文档内容不能为空',
            'title.required' => '标题不能为空',
            'menus_id.required' => 'menus_id不能为空',
            'menus_id.integer' => 'menus_id必须是整数',

        ]);

        $data = $request->all();
        $call_menu = new \App\Libs\wrapper\Menu();
        $return = $call_menu->menu_doc_add($data);
        if($return === 1){
            $this->back('操作成功', '200');
        }else{
            $this->back('操作失败', '500');
        }
    }



    /**
     * 编辑菜单说明文档
     * @param content 内容
     * @param title 标题
     * @param menus_id 菜单id
     */
    public function menu_doc_update(Request $request){
        //验证参数
        $this->validate($request, [
            'content' => 'required| string',
            'title' => 'required| string',
            'menus_id' => 'required| integer',
        ], [
            'content.required' => '文档内容不能为空',
            'title.required' => '标题不能为空',
            'menus_id.required' => 'menus_id不能为空',
            'menus_id.integer' => 'menus_id必须是整数',

        ]);

        $data = $request->all();
        $call_menu = new \App\Libs\wrapper\Menu();
        $return = $call_menu->menu_doc_update($data);
        if($return === 1){
            $this->back('修改成功', '200');
        }else{
            $this->back('修改失败', '500');
        }
    }


    /**
     * 查看菜单说明文档
     * @param content 内容
     * @param title 标题
     * @param menus_id 菜单id
     */
    public function menu_doc_select(Request $request){
        //验证参数
        $this->validate($request, [
            'menus_id' => 'required| integer',
        ], [
            'menus_id.required' => 'menus_id不能为空',
            'menus_id.integer' => 'menus_id必须是整数',

        ]);

        $data = $request->all();
        $call_menu = new \App\Libs\wrapper\Menu();
        $return = $call_menu->menu_doc_select($data);
        $this->back('请求成功', '200',$return);

    }
	
	
















}//类结束符