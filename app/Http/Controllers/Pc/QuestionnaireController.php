<?php

namespace App\Http\Controllers\Pc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\QuestionnaireModel;
use Illuminate\Support\Facades\DB;

/**
 * 问卷调查控制器
 */
class QuestionnaireController extends Controller
{
    private $request;
    protected $questionModel;
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->questionModel = new QuestionnaireModel();
    }

    /**
     * @Desc:保存海西汇入会申请表
     * @param Request $request
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/2/17 10:12
     */
    public function saveHaixihuiApplication(Request $request)
    {
        try {
            // 校验请求参数
            $this->_checkHaixihuiApplicationRequest($request);
            $res = $request->all();
            // 处理保存操作
            $result = $this->questionModel->saveHaixihuiApplication($res);
            return $this->back('保存成功！', 200, $result);
        }catch (\Exception $e){
            return $this->back('保存失败！原因：'. $e);
        }
    }

    public function saveProductResearch(Request $request)
    {
        // 校验请求参数
        $this->_checkProductResearchRequest($request);
        $res = $request->all();
        // 处理保存操作
        $result = $this->questionModel->saveProductResearch($res);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500);
    }

    /**
     * @Desc:校验请求入参
     * @param $request
     * @return true
     * @author: Liu Sinian
     * @Time: 2023/2/17 10:11
     */
    public function _checkHaixihuiApplicationRequest($request)
    {
        $this->validate($request, [
            'enterprise_name' => 'required| string',
            'platform_name' => 'required| string',
            'applicat_name' => 'required| string',
            'main_product' => 'required| string',
            'job' => 'required| string',
            'contact_number' => 'required| string',
            'email' => 'required| string',
            'wechat_number' => 'required| string',
            'membership_requirements' => 'required| string',
            'background_screenshot' => 'required',
        ], [
            'enterprise_name.required' => '企业名称不能为空',
            'enterprise_name.string' => '企业名称只能是文字',
            'platform_name.required' => '平台名称不能为空',
            'platform_name.string' => '平台名称只能是文字',
            'applicat_name.required' => '申请人姓名不能为空',
            'applicat_name.string' => '申请人姓名只能是文字',
            'main_product.required' => '主营产品不能为空',
            'main_product.string' => '主营产品只能是文字',
            'job.required' => '职务不能为空',
            'job.string' => '职务只能是文字',
            'contact_number.required' => '联系电话不能为空',
            'contact_number.string' => '联系电话只能是文字',
            'email.required' => '邮箱不能为空',
            'email.string' => '邮箱只能是文字',
            'wechat_number.required' => '微信号不能为空',
            'wechat_number.string' => '微信号只能是文字',
            'membership_requirements.required' => '入会需求不能为空',
            'membership_requirements.string' => '入会需求只能是文字',
            'background_screenshot.required' => '后台截图不能为空',
        ]);

        return true;
    }

    public function _checkProductResearchRequest($request)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
        ], [
            'title.required' => '标题不能为空',
            'content.required' => '内容不能为空',
        ]);

        return true;
    }

    /**
     * @Desc:获取海西汇入会申请表列表
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/3/2 16:41
     */
    public function getHaixihuiApplicationList()
    {
        $params = $this->request->all();
        $result = $this->questionModel->getHaixihuiApplicationList($params);

        return $this->back('获取成功', 200, $result);
    }

    /**
     * @Desc:获取海西汇产品调研数据列表
     * @author: Liu Sinian
     * @Time: 2023/3/2 16:45
     */
    public function getHaixihuiProductResearchList()
    {
        $params = $this->request->all();
        $result = $this->questionModel->getHaixihuiProductResearchList($params);

        return $this->back('获取成功', 200, $result);
    }

    /**
     * @Desc:处理导入失败的sku
     * @author: Liu Sinian
     * @Time: 2023/2/16 17:52
     */
    public function test()
    {
        $sql = "select * from import_result";
        $result = json_decode(json_encode(db::select($sql)));
        $list = [];
        foreach ($result as $item){
            $str =trim(substr(json_decode($item->result), 2, -2), "\"");
//            var_dump($str);exit();
            $arr = explode(",", $str);
            foreach ($arr as &$a){
                echo trim($a, "\"") . "\n";
            }
//            var_dump($arr);exit();
        }
//        var_dump($list);
    }
    public function getHaixihuiTableTypeList()
    {
        $params = $this->request->all();
        $result = $this->questionModel->getHaixihuiTableTypeList($params);

        return $this->back('获取成功', 200, $result);
    }
}