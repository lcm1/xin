<?php

namespace App\Http\Controllers\Pc;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\CloudHouse;
use App\Http\Requests\CloudHouse as CloudHouses;
use Illuminate\Support\Facades\Redis;


class CloudHouseController extends Controller
{
    /**
     * spu/sku生成模块
     * $model 模型
     * request 参数
     */

    private $model;
    private $request;
    private $validate;

    public function __construct(Request $request)
    {
        $this->model = new CloudHouse();
        $this->validate = new CloudHouses();
        $this->request = $request;
    }

    //合同
    public function ContractImport()
    {
        $params = $this->validate->scene('ContractImport')->check($this->request);
        try {
            $result = $this->model->ContractImport($params);
        } catch (\Throwable $th) {
            $result = ['type' => 'fail', 'msg' => $th->getMessage().'line:'.$th->getLine()];
        }

        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200',$result);
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    //走柜导入
    public function ContractZgImport()
    {
        $params = $this->validate->scene('ContractImport')->check($this->request);
        try {
            $result = $this->model->ContractZgImport($params);
        } catch (\Throwable $th) {
            //throw $th;
            $result = ['type' => 'fail', 'msg' => $th->getMessage().'line:'.$th->getLine()];
        }

        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200',$result);
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    //合同生产与出货导入
    public function ContractOutImport(){
        $params = $this->request->all();
        try {
            $result = $this->model->ContractOutImport($params);
        } catch (\Throwable $th) {

            $result = ['type' => 'fail', 'msg' => $th->getMessage().'line:'.$th->getLine()];
        }
        if($result['type'] == 'success'){
            return $this->back('获取成功', '200', $result);
        }else{
            return $this->back($result['msg'], '300');
        }
        
    }
    //手动同步商品
    public function ContractSyncDetail(){
        $params = $this->request->all();
        $result = $this->model->ContractSyncDetail($params);
        return $this->back('获取成功', '200', $result);
    }
    //获取合同订单明细列表
    public function ContractList()
    {
        $params = $this->request->all();
        $result = $this->model->ContractList($params);
        return $this->back('获取成功', '200', $result);
    }

    //获取合同订单明细列表
    public function ContractListNew()
    {
        $params = $this->request->all();
        $result = $this->model->ContractListNew($params);
        return $this->back('获取成功', '200', $result);
    }

    //工厂出货入库本地仓/工厂及存仓，生产入工厂虚拟仓
    public function ContractOut()
    {
        $params =  $this->request->all();
        try {
            $result = $this->model->ContractOut($params);
		} catch (\Throwable $th) {
			//throw $th;
			$result = ['type'=>'fail','msg'=>$th->getMessage().'line:'.$th->getLine()];
		}
      
        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    //出库批号详情
    public function ContractOutList()
    {
        $params = $this->validate->scene('ContractOutList')->check($this->request);
        $result = $this->model->ContractOutList($params);
        return $this->back('获取成功', '200', $result);
    }

    //采购入库手动推送
    public function PushInhouse()
    {
        $params =  $this->request->all();
        $result = $this->model->PushInhouse($params);
        return $this->back('获取成功', '200', $result);
    }

    //查询spu颜色
    public function GetContractColor()
    {
        $params =  $this->request->all();
        $result = $this->model->GetContractColor($params);
        return $this->back('获取成功', '200', $result);
    }

    //根据库存sku获取尺码
    public function SizeBySort()
    {
        $params =  $this->request->all();
        $result = $this->model->SizeBySort($params);
        return $this->back('获取成功', '200', $result);
    }

    
    //同步合同到一狗
    public function ConcactToOneDog()
    {
        $params =  $this->request->all();
        $result = $this->model->ConcactToOneDog($params);
        if($result['code']==200){
            return $this->back('获取成功', '200', $result);
        }else{
            return $this->back($result, '500');
        }
       
    }

    
    //取消出库
//    public function ContractCancel()
//    {
//        $params =  $this->request->all();
//        try {
//            $result = $this->model->ContractCancel($params);
//        } catch (\Throwable $th) {
//            //throw $th;
//            $result = ['type'=>'fail','msg'=>$th->getMessage()];
//        }
//
//        if ($result['type'] == 'success') {
//            return $this->back($result['msg'], '200');
//        } else {
//            return $this->back($result['msg'], '300');
//        }
//    }

    //取消出入库
    public function InhouseCancel()
    {
        $params =  $this->request->all();
        try {
            $result = $this->model->InhouseCancel($params);
        } catch (\Throwable $th) {
            //throw $th;
            $result = ['type'=>'fail','msg'=>$th->getMessage()];
        }
        $user = DB::table('users')->where('token',$params['token'])->select('Id')->first();
        //加入日志

        $d['user_id'] = $user->Id;
        $d['out_no'] = $params['order_no'];
        $d['create_time'] = date('Y-m-d H:i:s',time());
        $d['msg'] = $result['msg'];
        db::table('inhouse_err_log')->insert($d);

        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    //取消调拨
    public function transfersCancel()
    {
        $params =  $this->request->all();
        try {
            $result = $this->model->transfersCancel($params);
        } catch (\Throwable $th) {
            //throw $th;
            $result = ['type'=>'fail','msg'=>$th->getMessage()];
        }

        if ($result['type'] == 'success') {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '300');
        }
    }

    //获取合同订单列表
    public function ContractOutlog()
    {
        $params = $this->request->all();
        $result = $this->model->ContractOutlog($params);
        return $this->back('获取成功', '200', $result);
    }


    public function cloudhouse_custom_sku(){
        $params = $this->request->all();

        $result = $this->model->cloudhouse_custom_sku($params['custom_sku']);

        return $this->back('同步成功', '200');
    }

    //合同列表
    public function ContractTotalList(){
        $params = $this->request->all();

        $result = $this->model->ContractTotalList($params);

        return $this->back('获取成功', '200', $result);
    }


    //财务模块合同出货明细
    public function financeContract(){
        $params = $this->request->all();

        $result = $this->model->financeContract($params);

        return $this->back('获取成功', '200', $result);
    }

    //出入库记录

    public function inventoryInOut(){
        $params = $this->request->all();

        $result = $this->model->inventoryInOut($params);

        return $this->back('获取成功', '200', $result);
    }

    /**
     * @Desc:获取合同出货列表
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/3/1 16:33
     */
    public function getContractShipmentList()
    {
        $params = $this->request->all();

        $result = $this->model->getContractShipmentList($params);

        return $this->back('获取成功', '200', $result);
    }

    /**
     * @Desc:新增合同到货计划
     * @return null
     * @author: 严威
     * @Time: 2023/10/27
     */
    public function ContractPlan()
    {
        $params = $this->request->all();

        $result = $this->model->ContractPlan($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc:修改合同到货计划
     * @return null
     * @author: 严威
     * @Time: 2023/10/27
     */
    public function ContractPlanUpdate()
    {
        $params = $this->request->all();

        $result = $this->model->ContractPlanUpdate($params);

        if ($result['code'] ==200) {
            return $this->back($result['msg'], '200');
        } else {
            return $this->back($result['msg'], '500');
        }
    }

    /**
     * @Desc:删除合同到货计划
     * @return null
     * @author: 严威
     * @Time: 2023/10/27
     */
    public function ContractPlanDelete()
    {
        $params = $this->request->all();

        $result = $this->model->ContractPlanDelete($params);

        if ($result['code'] == 200) {
            return $this->back($result['msg'], $result['code']);
        } else {
            return $this->back($result['msg'], $result['code']);
        }
    }

    /**
     * @Desc:查询合同到货计划
     * @return null
     * @author: 严威
     * @Time: 2023/10/27
     */
    public function ContractPlanSelect()
    {
        $params = $this->request->all();

        $result = $this->model->ContractPlanSelect($params);

        return $this->back('请求成功', 200,$result);
    }

    public function auditContractPlan()
    {
        $params = $this->request->all();

        $result = $this->model->auditContractPlan($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500, $result['data'] ?? []);
    }


}
