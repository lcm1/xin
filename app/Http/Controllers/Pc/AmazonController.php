<?php

namespace App\Http\Controllers\Pc;

use App\Models\AmazonBuhuoDetail;
use App\Models\AmazonOrder;
use App\Models\AmazonOrderItem;

use App\Models\AmazonReportOld;
use App\Models\AmazonReportUnique;
use App\Models\ProductDetail;
use App\Models\SaiheProductDetail;
use App\Models\AmazonSkulist;
use App\Models\Shop;
use App\Models\AmazonReport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Qiniu\Http\Client;

/**
 * 这个类用来做一些采集结果数据验证，做一些采集结果错误数据处理，不做使用，可以删除
 * Class AmazonController
 * @package App\Http\Controllers\Pc
 */
class AmazonController extends Controller
{

//    /**
//     * 数据验证
//     * @param Request $request
//     */
//    public function index(Request $request)
//    {
//
//        set_time_limit(1500);
////        //1.验证空数据
////        $AmazonOrder = AmazonOrder::where('amazon_order_id', '')->get()->toArray();
////        echo '空数据', PHP_EOL;
////        foreach ($AmazonOrder as $item) {
////            echo 'id:' . $item['id'] . ' shop_id:' . $item['shop_id'], PHP_EOL;
////        }
//
//        echo '重复数据', PHP_EOL;
//        $time = time();
//        //2.验证订单重复的数据
//        $sql = "select count(*)  as count_repeat,amazon_order_id from amazon_order  where shop_id=34  group  by amazon_order_id    having count_repeat>1    ";
//        $repeatlist = json_decode(json_encode(db::select($sql)), true);
//        $time1 = time();
//        echo ($time1 - $time) . ' seconds', PHP_EOL;
//        foreach ($repeatlist as $repeat) {
//            $AmazonOrderRepeat = AmazonOrderRepeat::where('order_repeat', $repeat['id'])->first();
//            if (!isset($AmazonOrderRepeat['order_repeat'])) {
//                $addId = db::table("amazon_order_repeat")->insertGetId(array('order_repeat' => $repeat['id']));
//            }
//        }
//
//        $AmazonOrderRepeat = AmazonOrderRepeat::limit(1000)->all();
//
////        foreach ($AmazonOrderRepeat as $repeat) {
////
////            $AmazonOrderList = AmazonOrder::where('amazon_order_id', $repeat['amazon_order_id'])->get()->toArray();
////            $firstOrder = $AmazonOrderList[0];
////            $check = $firstOrder['amazon_order_id'] . $firstOrder['shop_id'] . $firstOrder['buyer_email'] . $firstOrder['seller_order_id'] . $firstOrder['seller_order_id']
////                . $firstOrder['amount'] . $firstOrder['postal_code'] . $firstOrder['order_status'] . $firstOrder['fulfillment_channel'] . $firstOrder['city'] . $firstOrder['country_code']
////                . $firstOrder['earliest_ship_date'] . $firstOrder['latest_ship_date'];
////
////            echo 'first---- order  data    shop_id:' . $firstOrder['shop_id']
////                . ' amazon_order_id:' . $firstOrder['amazon_order_id'] . ' buyer_email:' . $firstOrder['buyer_email']
////                . ' amount:' . $firstOrder['amount'] . ' purchase_date:' . $firstOrder['purchase_date'] . ' id:' . $firstOrder['id'], PHP_EOL;
////
////            foreach ($AmazonOrderList as $order) {
////                if ($order['id'] != $firstOrder['id']) {
////                    $orderCheck = $order['amazon_order_id'] . $order['shop_id'] . $order['buyer_email'] . $order['seller_order_id'] . $order['seller_order_id']
////                        . $order['amount'] . $order['postal_code'] . $order['order_status'] . $order['fulfillment_channel'] . $order['city'] . $order['country_code']
////                        . $order['earliest_ship_date'] . $order['latest_ship_date'];
////                    if ($orderCheck == $check) {
////                        echo 'order  data    shop_id:' . $order['shop_id']
////                            . ' amazon_order_id:' . $order['amazon_order_id'] . ' buyer_email:' . $order['buyer_email']
////                            . ' amount:' . $order['amount'] . ' purchase_date:' . $order['purchase_date'] . ' id:' . $order['id'], PHP_EOL;
////
////                        AmazonOrder::where('id', $order['id'])->delete();
////                        AmazonOrderItems::where('amazon_order', $order['id'])->delete();
////                    } else {
////                        echo ' id:' . $order['id'], PHP_EOL;
////                    }
////                }
//////                    .' seller_order_id:'.$order['seller_order_id']
//////                    .' last_update_date:'.$order['last_update_date'].' order_status:'.$order['order_status']
//////                    .' fulfillment_channel:'.$order['fulfillment_channel'].' sales_channel:'.$order['sales_channel']
//////                    .' shipping_address:'.$order['shipping_address'].' ship_service_level:'.$order['ship_service_level']
//////                    .' city:'.$order['city'].' state_or_region:'.$order['state_or_region'].' postal_code:'.$order['postal_code']
//////                    .' country_code:'.$order['country_code'].' currency_code:'.$order['currency_code']
//////                    .' amount:'.$order['amount'].' number_of_items_shipped:'.$order['number_of_items_shipped']
//////                    .' number_of_items_unshipped:'.$order['number_of_items_unshipped'], PHP_EOL;
////            }
////            echo '------------------------------------------------------------', PHP_EOL;
////            echo '', PHP_EOL;
////            echo '', PHP_EOL;
////            echo '', PHP_EOL;
////        }
//
////        //检查order表还没有获取到详情的数据的数量
////        $count = AmazonOrder::where('has_item', 0)->where('purchase_date', '>', '2022')->count();
////        echo '订单未同步到订单详情的数量' . $count, PHP_EOL;
////
////        //检查order表还没有获取到详情的数据
////        $noItemOrder = AmazonOrder::where('has_item', 0)->where('purchase_date', '>', '2022')->get()->toArrray();
////        foreach ($noItemOrder as $item) {
////            echo 'id:' . $item['id'] . " order_id:" . $item['amazon_order_id'] . ' shop_id:' . $item['shop_id']
////                . ' purchase_date:' . $item['purchase_date'] . ' 订单详情数据还没抓取', PHP_EOL;
////        }
////
////        //检查订单详情还没获取到时间的数据
////        $itemsNoTime = AmazonOrderItems::where('order_status', '')->get()->toArrray();
////        foreach ($itemsNoTime as $item) {
////            echo 'id:' . $item['id'] . " order_id:" . $item['amazon_order_id'] . ' shop_id:' . $item['shop_id']
////                . ' seller_sku:' . $item['seller_sku'] . ' 订单详情时间数据未同步', PHP_EOL;
////        }
//
//        echo '<script>';
//        echo 'function rel(){';
//        echo '	location.reload();';
//        echo '}';
//        echo '</script>';
//
//    }
//
//
//    public function report()
//    {
////        $startId = AmazonReportUnique::max("amazon_report");
////
////
////
////        \Illuminate\Support\Facades\DB::connection()->enableQueryLog();
////        $AmazonReport = AmazonReport::where('id', '>=', $startId)->limit(50000)->orderBy('id', 'asc')->get()->toArray();
////
////        foreach ($AmazonReport as $item) {
////            echo 'id:' . $item['id'] . ' shop_id:' . $item['shop_id'], PHP_EOL;
////
////            $str = 'shop_id' . $item['shop_id'] . 'order_id' . $item['order_id'] . 'sku' . $item['sku'] . 'asin' . $item['asin'] . 'fnsku' . $item['fnsku'];
////            $md5 = MD5($str);
////
////            $AmazonReportUnique = AmazonReportUnique::where('md5', $md5)->first();
////            if (isset($AmazonReportUnique['id'])) {
////                //  $AmazonReport->where('id',$item['id'])->delete();
////            } else {
////                $add = array();
////                $add['amazon_report'] = $item['id'];
////                $add['md5'] = $md5;
////                DB::table('amazon_report_unique')->insertGetId($add);
////            }
////
////        }
//
////        set_time_limit(300);
////
////        $startId = AmazonReport::max("un_id");
////         var_dump($startId);
////
////        $AmazonReportUnique = AmazonReportUnique::where('id', '>', $startId)->limit(3000)->orderBy('id', 'asc')->get()->toArray();
////        foreach ($AmazonReportUnique as $item) {
////            $AmazonReportOld = AmazonReportOld::where('id', $item['amazon_report'])->first();
////
////            unset($AmazonReportOld['id']);
////            $add = array();
////            $add['md5'] = $item['md5'];
////            $add['return_date'] = $AmazonReportOld['return_date'];
////            $add['order_id'] = $AmazonReportOld['order_id'];
////            $add['sku'] = $AmazonReportOld['sku'];
////            $add['asin'] = $AmazonReportOld['asin'];
////            $add['fnsku'] = $AmazonReportOld['fnsku'];
////            $add['product_name'] = $AmazonReportOld['product_name'];
////            $add['quantity'] = $AmazonReportOld['quantity'];
////            $add['fulfillment_center_id'] = $AmazonReportOld['fulfillment_center_id'];
////            $add['detailed_disposition'] = $AmazonReportOld['detailed_disposition'];
////            $add['reason'] = $AmazonReportOld['reason'];
////            $add['status'] = $AmazonReportOld['status'];
////            $add['license_plate_number'] = $AmazonReportOld['license_plate_number'];
////            $add['shop_id'] = $AmazonReportOld['shop_id'];
////            $add['generated_report_id'] = $AmazonReportOld['generated_report_id'];
////            $add['customer_comments'] = $AmazonReportOld['customer_comments'];
////            $add['purchase_date'] = $AmazonReportOld['purchase_date'];
////
////            $add['un_id'] = $item['id'];
////
////
////            DB::table('amazon_report')->insertGetId($add);
////        }
//
//        $sql = "select count(*) as count_e,license_plate_number,order_id,sku  from amazon_report group  by license_plate_number,order_id,sku   having count_e>1";
//        $repeatlist = json_decode(json_encode(db::select($sql)), true);
//        var_dump($repeatlist);
//
////        $AmazonReport = AmazonReport::where('license_plate_number', '')->orderBy('id', 'asc')->limit(5000)->get()->toArray();
////        var_dump(count($AmazonReport));
////        foreach ($AmazonReport as $item) {
////
////           // DB::enableQueryLog();
////            $row = AmazonReport::where('shop_id', $item['shop_id'])
////                ->where('order_id', $item['order_id'])->where('sku', $item['sku'])
////                ->where('asin', $item['asin'])->where('fnsku', $item['fnsku'])->where('license_plate_number', "<>", '')->first();
////
////           // print_r(DB::getQueryLog());
////           // exit();
////
////            if (isset($row['id'])) {
////                echo $row['id'] . ' ' . $item['id'], PHP_EOL;
////            } else {
////                var_dump($row);
////            }
////        }
//
//
//    }
//
//
//    /**
//     * 查找具体订单是否存在
//     * @param Request $request
//     */
//    public function order(Request $request)
//    {
//
//        $data = $request->all();
//        $AmazonOrder = AmazonOrder::where('id', $data['id'])->first();
//        $shop = Shop::where('Id', $AmazonOrder['shop_id'])->first();
//        echo $shop['Id'] . ' ' . $shop['shop_name'], PHP_EOL;
//        $auth = json_decode($shop['app_data'], true);
//        $this->apiOrder($auth, $AmazonOrder['amazon_order_id']);
//
//    }
//
//
//    /**
//     * Excel上传检查订单
//     * @param Request $request
//     * @return string[]
//     * @throws \PHPExcel_Exception
//     * @throws \PHPExcel_Reader_Exception
//     */
//    public function excel(Request $request)
//    {
//        set_time_limit(0);
//        $params = $request->all();
//        $shop_id = $params['shop_id'];
//
//        $file = $params['file'];
//
//        //获取文件后缀名
//        $extension = $file->getClientOriginalExtension();
//        if ($extension == 'csv') {
////            return [
////                'type' => 'fail',
////                'msg' => '不支持csv格式'
////            ];
//            $PHPExcel = new \PHPExcel_Reader_CSV();
//        } elseif ($extension == 'xlsx') {
//            $PHPExcel = new \PHPExcel_Reader_Excel2007();
//        } else {
//            $PHPExcel = new \PHPExcel_Reader_Excel5();
//        }
//        if (!$PHPExcel->canRead($file)) {
//            return [
//                'type' => 'fail',
//                'msg' => '导入失败，Excel文件错误'
//            ];
//        }
//        $PHPExcelLoad = $PHPExcel->load($file);
//        $Sheet = $PHPExcelLoad->getSheet(0);
//        /**取得一共有多少行*/
//        $allRow = $Sheet->getHighestRow();
//        $a = 0;
//        $b = 0;
//        $time = time();
//        $limit = 100;
//
//        for ($j = 2; $j < $allRow; $j++) {
//            $type = trim($Sheet->getCellByColumnAndRow(2, $j)->getValue());
//            if ($type == 'Order') {
//                $a++;
//                $orderSn = trim($Sheet->getCellByColumnAndRow(3, $j)->getValue());
//                $sku = trim($Sheet->getCellByColumnAndRow(4, $j)->getValue());
//                $quantity = trim($Sheet->getCellByColumnAndRow(6, $j)->getValue());
//                $productPrice = trim($Sheet->getCellByColumnAndRow(14, $j)->getValue());
//                $total = trim($Sheet->getCellByColumnAndRow(27, $j)->getValue());
//
//                $AmazonOrder = AmazonOrder::where('amazon_order_id', $orderSn)->where('shop_id', $shop_id)->first();
//                if (!isset($AmazonOrder['id'])) {
//                    echo $orderSn . '  数据库无数据 ', PHP_EOL;
//                } else {
//                    $AmazonOrderItems = AmazonOrderItem::where('amazon_order_id', $orderSn)->where('shop_id', $shop_id)
//                        ->where('seller_sku', $sku)->first();
//                    if (!isset($AmazonOrderItems['id'])) {
//                        echo $orderSn . '有' . $sku . '  数据', PHP_EOL;
//                    } else {
//                        echo $orderSn . '  ' . $AmazonOrderItems['id'], ' sku:' . $sku . ' shop_id:' . $AmazonOrderItems['shop_id'] . ' purchase_date:' . $AmazonOrderItems['purchase_date'], PHP_EOL;
//                    }
//                }
//            }
//
//            if ($type == 'Refund') {
//                $orderSn = trim($Sheet->getCellByColumnAndRow(3, $j)->getValue());
//                $sku = trim($Sheet->getCellByColumnAndRow(4, $j)->getValue());
//                $AmazonReport = AmazonReport::where('order_id', $orderSn)->where('shop_id', $shop_id)->where('sku', $sku)->first();
//                if (!isset($AmazonReport['id'])) {
//                    echo '退货 ' . $orderSn . '有' . $sku . '    无数据', PHP_EOL;
//                } else {
//                    echo '退货 ' . $orderSn . '  id:' . $AmazonReport['id'], ' sku:' . $sku . ' shop_id:' . $AmazonReport['shop_id'] . ' return_date:' . $AmazonReport['return_date'], PHP_EOL;
//                }
//            }
//
//
//        }
//    }
//
//
//    public function apiOrder($auth, $orderId)
//    {
//
//        require_once(app_path() . DIRECTORY_SEPARATOR . 'Libs' . DIRECTORY_SEPARATOR . 'MarketplaceWebServiceOrders' . DIRECTORY_SEPARATOR . 'Client.php');
//        require_once(app_path() . DIRECTORY_SEPARATOR . 'Libs' . DIRECTORY_SEPARATOR . 'MarketplaceWebServiceOrders' . DIRECTORY_SEPARATOR . 'Model' . DIRECTORY_SEPARATOR . 'GetOrderRequest.php');
//
//        $serviceUrl = "https://mws.amazonservices.com/Orders/2013-09-01";
//        $serviceUrl_jp = "https://mws.amazonservices.jp/Orders/2013-09-01";
//        $serviceUrl_eu = "https://mws-eu.amazonservices.com/Orders/2013-09-01";
//
//        $config = array(
//            'ServiceURL' => $serviceUrl,
//            'ProxyHost' => null,
//            'ProxyPort' => -1,
//            'ProxyUsername' => null,
//            'ProxyPassword' => null,
//            'MaxErrorRetry' => 3,
//        );
//
//
//        $service = new \MarketplaceWebServiceOrders_Client(
//            $auth['AWS_ACCESS_KEY_ID'],
//            $auth['AWS_SECRET_ACCESS_KEY'],
//            'MarketplaceWebServiceProducts PHP5 Library',
//            '2',
//            $config);
//
//        $request = new \MarketplaceWebServiceOrders_Model_GetOrderRequest();
//
//        $request->setSellerId($auth['MERCHANT_ID']);
//        $request->setMWSAuthToken($auth['MWSAuthToken']);
//
//        $request->setAmazonOrderId($orderId);
//
//
//        try {
//
//            $response = $service->GetOrder($request);
//            $dom = new \DOMDocument();
//            $dom->loadXML($response->toXML());
//            $dom->preserveWhiteSpace = false;
//            $dom->formatOutput = true;
//            $data = $dom->saveXML();
//
//            $xml = simplexml_load_string($data);
//            $data1 = json_decode(json_encode($xml), TRUE);
//            var_dump($data1);
//            return $data1;
//
//        } catch (\MarketplaceWebServiceOrders_Exception $ex) {
//            echo("Caught Exception: " . $ex->getMessage() . "\n");
//            echo("Response Status Code: " . $ex->getStatusCode() . "\n");
//            echo("Error Code: " . $ex->getErrorCode() . "\n");
//            echo("Error Type: " . $ex->getErrorType() . "\n");
//            echo("Request ID: " . $ex->getRequestId() . "\n");
//            echo("XML: " . $ex->getXML() . "\n");
//            echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
//        }
//    }
//
//
//    public function changTime()
//    {
//        $start = 0;
//        $AmazonOrder = AmazonOrder::where('id', '>', $start)->orderBy("id", "asc")->limit(100)->get()->toArray();
//        foreach ($AmazonOrder as $item) {
//            $purchase_date = $item['purchase_date'];
//            $purchase_date_show = $this->dateTimeChangeByZone($purchase_date, 'UTC', 'America/Los_Angeles', 'Y-m-d H:i:s');
//            echo $item['id'] . " " . $purchase_date . ' ---- ' . $purchase_date_show, PHP_EOL;
//            AmazonOrder::where('id', $item['id'])->update(array('purchase_date_show' => $purchase_date_show));
//        }
//    }
//
//    /**
//     * 塞盒处理
//     */
//    public function saihe()
//    {
//        set_time_limit(500);
////        $sql = "select count(*) as count2,saihe_sku,order_source_sku  from saihe_product_detail where order_source_sku!=''  group by saihe_sku,order_source_sku   having count2>1  ";
////        $repeatlist = json_decode(json_encode(db::select($sql)), true);
////        foreach ($repeatlist as $item) {
////            $SaiheProductDetail = SaiheProductDetail::where('saihe_sku', $item['saihe_sku'])->where('order_source_sku', $item['order_source_sku'])->get()->toArray();
////            // var_dump($SaiheProductDetail);
////            $xx = 0;
////            foreach ($SaiheProductDetail as $itemRow) {
////                $xx++;
////                echo $xx . '  ' . $itemRow['id'] . ' saihe_sku ' . $itemRow['saihe_sku'] . ' client_sku ' . $itemRow['client_sku'] . ' order_source_sku ' . $itemRow['order_source_sku'] . ' asin' . $itemRow['asin'], PHP_EOL;
////                if ($xx > 1) {
////                    SaiheProductDetail::where('id', $itemRow['id'])->delete();
////                }
////            }
////            echo '------------------------------------------', PHP_EOL;
////        }
//
//
////        $maxId = SaiheProductDetail::where('md5', '!=', '')->max("id");
////        if ($maxId == null) {
////            $maxId = 0;
////        }
////        var_dump($maxId);
////
////        $SaiheProductDetail = SaiheProductDetail::where('saihe_sku', '!=', '')->where('md5', '')->where('id', ">", $maxId)->orderBy("id", "asc")->limit(5000)->get()->toArray();
////        foreach ($SaiheProductDetail as $item) {
////            $md5str = 'saihe_sku' . $item['saihe_sku'] . 'order_source_sku' . $item['order_source_sku'] . 'order_source_type' . $item['order_source_type'];
////            //  echo $md5str, PHP_EOL;
////            $update = array();
////            $update['md5'] = md5($md5str);
////            //  echo $update['md5'], PHP_EOL;
////            SaiheProductDetail::where('id', $item['id'])->update($update);
////        }
////
////        echo "<script>";
////        echo "function rel(){";
////        echo "location.reload();";
////        echo "}";
////        echo 'setTimeout("rel()","1000");';
////        echo "</script>";
//
//
//        $AmazonSkulist = AmazonSkulist::where('id', '>', 7500)->where('id', '<=', 8000)->limit(3000)->get()->toArray();
//        // var_dump($AmazonSkulist);
//        foreach ($AmazonSkulist as $item) {
//            var_dump("id:".$item['id']);
//            var_dump($item['sku']);
//            var_dump($item['shop_id']);
//            $res1 = AmazonOrderItem::where('seller_sku', $item['sku'])
//                ->where('shop_id', $item['shop_id'])
//                ->update(array('category_id' => $item['category_id'], 'user_id' => $item['user_id'], 'spu' => $item['spu']));
//            $res2 = ProductDetail::where('sellersku', $item['sku'])
//                ->where('shop_id', $item['shop_id'])
//                ->where('platform_id', 5)->update(array('category_id' => $item['category_id'], 'user_id' => $item['user_id'], 'spu' => $item['spu']));
//            $res3 = SaiheProductDetail::where('order_source_sku', $item['sku'])
//                ->update(array('category_id' => $item['category_id'], 'user_id' => $item['user_id'], 'spu' => $item['spu'], 'shop_id' => $item['shop_id']));
//
//            $res4 = AmazonReport::where('sku', $item['sku'])
//                ->where('shop_id', $item['shop_id'])->update(array('category_id' => $item['category_id'], 'spu' => $item['spu']));
//
//            $res5 = AmazonBuhuoDetail::where('sku', $item['sku'])
//                ->where('shop_id', $item['shop_id'])->update(array('category_id' => $item['category_id'], 'spu' => $item['spu']));
//
//            var_dump($res1);
//            var_dump($res2);
//            var_dump($res3);
//            var_dump($res4);
//            var_dump($res5);
//        }
//
//
//    }
//
//
    public function saiheother()
    {

//        set_time_limit(300);
//        $xxx = 0;
//        $sql = "select count(*) as count_all,order_source_sku,order_source_type   from saihe_product_detail where  order_source_sku!='' and order_source_type=1 group by order_source_sku,order_source_type HAVING count_all>1  ";
//        $repeatlist = json_decode(json_encode(db::select($sql)), true);
//
//        //  $SaiheProductDetail = SaiheProductDetail::where('saihe_sku', '')->orderBy("id", "asc")->limit(10000)->get()->toArray();
//        foreach ($repeatlist as $item) {
//            $xxx++;
//            //  echo $item['order_source_type'].' '.$item['order_source_sku'], PHP_EOL;
//            $rowList = SaiheProductDetail::where('order_source_sku', $item['order_source_sku'])
//                ->where('order_source_type', $item['order_source_type'])->get()->toArray();
//            foreach ($rowList as $item2) {
//                SaiheProductDetail::where('id',$item2['id'])->delete();
////                $filter['cilent_sku'] = $item2['client_sku'];
////                $filter['PageSize'] = 50;
////                $filter['PageIndex'] = 1;
////                $result = $this->GetPlatformSKUList($filter);
////
////                 echo $item2['id'].' '. $item2['order_source_type'].' '.$item2['order_source_sku'].' '.$item2['order_source_sku'], PHP_EOL;
////
////                $check = 0;
////                if (isset($result[0]['GetPlatformSKUListResponse']['GetPlatformSKUListResult']['Status']) && $result[0]['GetPlatformSKUListResponse']['GetPlatformSKUListResult']['Status'] == "OK") {
////                    if (!empty($result[0]['GetPlatformSKUListResponse']['GetPlatformSKUListResult']['PlatformSKUList']['PlatformSKUList'])) {
////                        $ApiProductInfo = $result[0]['GetPlatformSKUListResponse']['GetPlatformSKUListResult']['PlatformSKUList']['PlatformSKUList'];
////                        foreach ($ApiProductInfo as $keySaihe => $item3) {
////                            if (isset($item3['OrderSourceSKU']) && $item3['OrderSourceSKU'] == $item['order_source_sku']) {
////                              //  echo "OrderSourceType:".$item3['OrderSourceType'] . ' ' . $item3['OrderSourceSKU'] . ' ' . $item3['ClientSKU'], PHP_EOL;
////                                $check = 1;
////                            }
////                        }
////                    }
////                }
////                echo $check, PHP_EOL;
////
////                echo '-------------', PHP_EOL;
//            }
////
////            if ($xxx > 10) {
////                exit();
////            }
//        }
    }


    /**
     * 采集sku接口
     * @param $ClientSKU
     * @return array|void
     */
    public function GetPlatformSKUList($filter)
    {
        //$filter['TimeType'] = 2;//按照修改时间
        set_time_limit(240);
        $ext = '';
        if (isset($filter['StartTime']) && isset($filter['EndTime'])) {
            $ext .= '<StartTime>' . $filter['StartTime'] . '</StartTime>';
            $ext .= '<EndTime>' . $filter['EndTime'] . '</EndTime>';
        }
        if (isset($filter['cilent_sku'])) {
            $ext .= '<ClientSKU>' . $filter['cilent_sku'] . '</ClientSKU>';
        }

        if (isset($filter['OrderSourceType'])) {
            $ext .= '<OrderSourceType>' . $filter['OrderSourceType'] . '</OrderSourceType>';
        }
        if (isset($filter['PageSize'])) {
            $ext .= '<PageSize>' . $filter['PageSize'] . '</PageSize>';
        } else {
            echo '请输入参数PageSize';
            return;
        }
        if (isset($filter['PageIndex'])) {
            $ext .= '<PageIndex>' . intval($filter['PageIndex']) . '</PageIndex>';
        } else {
            echo '请输入参数PageIndex';
            return;
        }

        if (isset($filter['TimeType'])) {
            $ext .= '<TimeType>' . $filter['TimeType'] . '</TimeType>';
        }


        $params = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <GetPlatformSKUList xmlns="http://tempuri.org/">
      <request>
        <CustomerID>1408</CustomerID>
	    <UserName>lichun</UserName>
	    <Password>zity123456</Password>
        ' . $ext . '
      </request>
    </GetPlatformSKUList>
  </soap:Body>
</soap:Envelope>';


        // echo $params, PHP_EOL;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "https://gg30.irobotbox.com/Api/API_ProductInfoManage.asmx");
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'X-AjaxPro-Method:ShowList',
                'Content-Type: text/xml; charset=utf-8')
        );
        //设置发送数据
        curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        //设置发送方式：post

        //TRUE 将curl_exec()获取的信息以字符串返回，而不是直接输出
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

        //执行cURL会话 ( 返回的数据为xml )
        $res = curl_exec($curl);
        //关闭cURL资源，并且释放系统资源
        curl_close($curl);


        try {
            $xmlObj = simplexml_load_string($res);
            $xmlObj->registerXPathNamespace('soap', 'http://schemas.xmlsoap.org/soap/envelope/');
            $result = $xmlObj->xpath("soap:Body");
            $results = $this->object_to_array($result);

            $RecordCount = $results[0]['GetPlatformSKUListResponse']['GetPlatformSKUListResult']['RecordCount'];
//            self::$logger->log(date('Y-m-d H:i:s') . ' record All:' . $RecordCount, 1, 'SaiheGetProductDetail');
//            if (self::$pageCount == 0) {
//                self::$pageCount = ceil(($RecordCount / $filter['PageSize']));
//            }
            return $results;
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
    }

    public function object_to_array($obj)
    {
        $obj = (array)$obj;
        foreach ($obj as $k => $v) {
            if (gettype($v) == 'resource') {
                return;
            }
            if (gettype($v) == 'object' || gettype($v) == 'array') {
                $obj[$k] = (array)$this->object_to_array($v);
            }
        }

        return $obj;
    }
//
//
//    /**
//     * @param string $dateTime 时间，如：2020-04-22 10:10:10
//     * @param string $fromZone 时间属于哪个时区
//     * @param string $toZone 时间转换为哪个时区的时间
//     * @param string $format 时间格式，如：Y-m-d H:i:s
//     * 时区选择参考：https://www.php.net/manual/zh/timezones.php 常见的如：UTC,Asia/Shanghai
//     * 时间格式参考：https://www.php.net/manual/zh/datetime.formats.php
//     *
//     * @return string
//     */
//    public function dateTimeChangeByZone($dateTime, $fromZone, $toZone, $format = 'Y-m-d H:i:s')
//    {
//
//        $dateTimeZoneFrom = new \DateTimeZone($fromZone);
//        $dateTimeZoneTo = new \DateTimeZone($toZone);
//        $dateTimeObj = \DateTime::createFromFormat($format, $dateTime, $dateTimeZoneFrom);
//        $dateTimeObj->setTimezone($dateTimeZoneTo);
//        return $dateTimeObj->format($format);
//
//    }

//    public function test()
//    {
//
////        $category_status = 2;  //利润款
////        $total_inventory = 4;  //合计库存
////        $plan_multiple = 1;  //计划增长倍数
////        $saleTotal = 1;  //2周销量
////
////        $DigitalModel = new \App\Models\DigitalModel();
////        $inventoryTurnover = $DigitalModel->inventoryTurnover($total_inventory, $plan_multiple, $category_status, $saleTotal);
////        var_dump($inventoryTurnover);  //0.07   57
//
//
////        $category_status = 1;  //利润款
////        $total_inventory = 385;  //合计库存
////        $plan_multiple = 1.2;  //计划增长倍数
////        $saleTotal = 50;  //1周销量
////
////        $DigitalModel = new \App\Models\DigitalModel();
////        $inventoryTurnover = $DigitalModel->inventoryTurnover($total_inventory, $plan_multiple, $category_status, $saleTotal);
////        var_dump($inventoryTurnover);  //7.14   45
//
//
////        //ZT-Z-WhiteBlackGrey-M
////        $category_status = 3;  //清仓款
////        $total_inventory = 5;  //合计库存
////        $plan_multiple = 1;  //计划增长倍数
////        $saleTotal = 6;  //月销量
////
////        $DigitalModel = new \App\Models\DigitalModel();
////        $inventoryTurnover = $DigitalModel->inventoryTurnover($total_inventory, $plan_multiple, $category_status, $saleTotal);
////        var_dump($inventoryTurnover);  //0.2   25
//
//
//        //ZT-Z-WhiteBlackGrey-M
//        $category_status = 3;  //清仓款
//        $total_inventory = 5;  //合计库存
//        $plan_multiple = 1;  //计划增长倍数
//        $saleTotal = 6;  //月销量
//        $month = '2022-02';
//
//        $DigitalModel = new \App\Models\DigitalModel();
//        $inventoryTurnover = $DigitalModel->inventoryTurnover($total_inventory, $plan_multiple, $category_status, $saleTotal, $month);
//        var_dump($inventoryTurnover);  //0.21   24
//
//
//    }

    /**
     * 亚马逊sku
     * @param Request $request
     */
    public function amazon_sku(Request $request)
    {
        $params = $request->all();
        if (!isset($params['shop_id'])) {
            return $this->back('没有shop_id参数', '300');
        }
        if (!isset($params['sku'])) {
            return $this->back('没有sku参数', '300');
        }

        $skuList = explode(",", $params['sku']);

        $Shop = Shop::where('id', $params['shop_id'])->first();

        $shop['auth'] = json_decode($Shop['app_data'], true);
        $auth = $shop['auth'];

        require_once(app_path() . DIRECTORY_SEPARATOR . 'Libs' . DIRECTORY_SEPARATOR . 'MarketplaceWebServiceProducts' . DIRECTORY_SEPARATOR . 'Client.php');
        require_once(app_path() . DIRECTORY_SEPARATOR . 'Libs' . DIRECTORY_SEPARATOR . 'MarketplaceWebServiceProducts' . DIRECTORY_SEPARATOR . 'Model' . DIRECTORY_SEPARATOR . 'GetMatchingProductForIdRequest.php');
        require_once(app_path() . DIRECTORY_SEPARATOR . 'Libs' . DIRECTORY_SEPARATOR . 'MarketplaceWebServiceProducts' . DIRECTORY_SEPARATOR . 'Model' . DIRECTORY_SEPARATOR . 'SellerSKUListType.php');
        $serviceUrl = "https://mws.amazonservices.com/Products/2011-10-01";
        // Europe
        $serviceUrl_eu = "https://mws-eu.amazonservices.com/Products/2011-10-01";
        // Japan
        $serviceUrl_jp = "https://mws.amazonservices.jp/Products/2011-10-01";

        if ($auth['MARKETPLACE_ID'] == 'A1VC38T7YXB528') $serviceUrl = $serviceUrl_jp;
        if (in_array($auth['MARKETPLACE_ID'], array('A1F83G8C2ARO7P', 'A1PA6795UKMFR9', 'A1RKKUPIHCS9HS', 'A13V1IB3VIYZZH', 'APJ6JRA9NG5V4'))) $serviceUrl = $serviceUrl_eu;

        $config = array(
            'ServiceURL' => $serviceUrl,
            'ProxyHost' => null,
            'ProxyPort' => -1,
            'ProxyUsername' => null,
            'ProxyPassword' => null,
            'MaxErrorRetry' => 3,
        );

        $service = new \MarketplaceWebServiceProducts_Client(
            $auth['AWS_ACCESS_KEY_ID'],
            $auth['AWS_SECRET_ACCESS_KEY'],
            'MarketplaceWebServiceProducts PHP5 Library',
            '2',
            $config);

        /************************************************************************
         * Uncomment to try out Mock Service that simulates MarketplaceWebServiceProducts
         * responses without calling MarketplaceWebServiceProducts service.
         *
         * Responses are loaded from local XML files. You can tweak XML files to
         * experiment with various outputs during development
         *
         * XML files available under MarketplaceWebServiceProducts/Mock tree
         *
         ***********************************************************************/
        // $service = new MarketplaceWebServiceProducts_Mock();

        /************************************************************************
         * Setup request parameters and uncomment invoke to try out
         * sample for Get Matching Product For Id Action
         ***********************************************************************/
        // @TODO: set request. Action can be passed as MarketplaceWebServiceProducts_Model_GetMatchingProductForId
        $request = new \MarketplaceWebServiceProducts_Model_GetMatchingProductForIdRequest();
        $request->setSellerId($auth['MERCHANT_ID']);
        $request->setMarketplaceId($auth['MARKETPLACE_ID']);
        if (isset($auth['MWSAuthToken'])) $request->setMWSAuthToken($auth['MWSAuthToken']);
        $request->setIdType('SellerSKU');
        $idlist = new \MarketplaceWebServiceProducts_Model_SellerSKUListType();
        $idlist->setSellerSKU($skuList);
        $request->setIdList($idlist);

        try {
            $response = $service->GetMatchingProductForId($request);
            $dom = new \DOMDocument();
            $dom->loadXML($response->toXML());
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;
            $str = $dom->saveXML();
            $str = str_replace('ns2:', '', $str);
            $xml = simplexml_load_string($str);
            return json_decode(json_encode($xml), true);
        } catch (\MarketplaceWebServiceProducts_Exception $ex) {
            echo("Caught Exception: " . $ex->getMessage() . "\n");
            echo("Response Status Code: " . $ex->getStatusCode() . "\n");
            echo("Error Code: " . $ex->getErrorCode() . "\n");
            echo("Error Type: " . $ex->getErrorType() . "\n");
            echo("Request ID: " . $ex->getRequestId() . "\n");
            echo("XML: " . $ex->getXML() . "\n");
            echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
        }

    }


}
