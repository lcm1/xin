<?php
namespace App\Http\Controllers\Pc;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon; //时间
use App\Http\Controllers\Controller;

class StoreController extends Controller{
    //列表数据
    public function store_list(Request $request){
        $data = $request->all();
//        if(empty($data)){
//            $this->back('接收值为空');
//        }

        $call_store = new \App\Libs\wrapper\Store();
        $return = $call_store->store_list($data);
		//var_dump($return);exit;
        if(is_array($return['list'])){
            $this->back('获取成功', '200', $return);
        }else{
            $this->back($return);
        }
    }
	
	/*
	 * 店铺信息获取
	 * @parma Id 店铺id
	 * */
	public function store_find(Request $request){
		$data = $request->all();
		if(empty($data)){
			$this->back('接收值为空');
		}
		
		$call_store = new \App\Libs\wrapper\Store();
		$return = $call_store->store_find($data);
//		var_dump($return);exit;
		if(is_array($return)){
			$this->back('获取成功', '200', $return);
		}else{
			$this->back($return);
		}
	}
 
    
	//平台列表数据
	public function platform_list(){
		$sql = "select *
                    	from platform
                    	where state = 1";
		$list = json_decode(json_encode(db::select($sql)),true);
		if(is_array($list)){
			$this->back('获取成功', '200', $list);
		}else{
			$this->back('获取失败');
		}
	}
	
	//获取lazada平台分类
    public function category_lzd(Request $request){
        $data = $request->all();
        $call_store = new \App\Libs\wrapper\Store();
        $return = $call_store->category_lzd($data);
        if(is_array($return)) {
            $this->back('获取成功', '200',$return);
        }else{
            $this->back($return);
        }
    }


    //获取vova平台分类
    public function category_vova(Request $request){
        $data = $request->all();
        $call_store = new \App\Libs\wrapper\Store();
        $return = $call_store->category_vova($data);
        if(is_array($return)) {
            //数据处理
            $this->back('获取成功', '200',$return);
        }else{
            $this->back($return);
        }
    }

 
	/*
	 * 新增店铺
	 * @pamra platform_id 平台id
	 * @param shop_name 店铺名
	 * @param account 账号
	 * @param description 描述
	 * */
    public function store_insert(Request $request){
        $this->validate($request, [
            'shop_name' => 'required| string',
	        'account' => 'required| string',
            'platform_id' => 'required| integer',
        ], [
            'shop_name.required' => '请填写店铺名',
            'shop_name.string' => 'shop_name必须是字符串',
	        'account.required' => '请填写店铺名',
	        'account.string' => 'account必须是字符串',
            'platform_id.required' => '请填写平台',
            'platform_id.integer' => 'platform_id必须是数字',
        ]);
		
        $data = $request->all();
        ////权限验证
        $arr = array();
        $arr['user_id'] = $data['user_info']['Id'];
        $arr['identity'] = array('store_insert');
        $powers = $this->powers($arr);
        if(!$powers['store_insert']){
            $this->back('无该权限');
        }
        $call_store = new \App\Libs\wrapper\Store();
        $return = $call_store->store_insert($data);
        if($return === 1) {
            $this->back('新增成功', '200');
        }else{
            $this->back($return);
        }
    }

    public function store_user(Request $request){
        $data = $request->all();
        $call_spu = new \App\Libs\wrapper\Store();
        $return = $call_spu->store_user();
        if(is_array($return)) {
            //数据处理
            $this->back('获取成功', '200',$return);
        }else{
            $this->back($return);
        }
    }

    public function store_country(Request $request){
        $data = $request->all();
        $call_spu = new \App\Libs\wrapper\Store();
        $return = $call_spu->store_country();
        if(is_array($return)) {
            //数据处理
            $this->back('获取成功', '200',$return);
        }else{
            $this->back($return);
        }
    }

    /*
     * 获取店铺主营分类
     */
    public function main_category(Request $request){
        $data = $request->all();
        $call_spu = new \App\Libs\wrapper\Store();
        $return = $call_spu->main_category($data);
        if(is_array($return)) {
            //数据处理
            $this->back('获取成功', '200',$return);
        }else{
            $this->back($return);
        }
    }


	/*
	 * 编辑店铺
	 * @param Id 店铺id
	 * */
    public function store_update(Request $request){
        $this->validate($request, [
	        'Id' => 'required| integer',
            'shop_name' => 'string',
	        'account' => 'string',
            'platform_id' => 'integer',
        ], [
	        'Id.required' => 'Id为空',
	        'Id.integer' => 'Id必须是数字',
            'shop_name.string' => 'account必须是字符串',
	        'account.string' => 'account必须是字符串',
            'platform_id.string' => 'platform_id必须是数字',
        ]);
        
        $data = $request->all();
        ////权限验证
        $arr = array();
        $arr['user_id'] = $data['user_info']['Id'];
        $arr['identity'] = array('store_update');
        $powers = $this->powers($arr);
        if(!$powers['store_update']){
            $this->back('无该权限');
        }
        
        $call_store = new \App\Libs\wrapper\Store();
        $return = $call_store->store_update($data);
        if($return === 1) {
            $this->back('修改成功', '200');
        }else{
            $this->back($return);
        }
    }
	
	/*
	 * 修改店铺状态
	 * @param Id 店铺id
	 * @param state 状态：1.正常，2.停用
	 * */
    public function store_state(Request $request){
	    $this->validate($request, [
		    'Id' => 'required| integer',
		    'state' => 'required| integer',
	    ], [
		    'Id.required' => 'Id为空',
		    'Id.integer' => 'Id必须是数字',
		    'state.required' => 'state为空',
		    'state.string' => 'state必须是数字',
	    ]);
    	
        $data = $request->all();
        ////权限验证
        $arr = array();
        $arr['user_id'] = $data['user_info']['Id'];
        $arr['identity'] = array('store_state');
        $powers = $this->powers($arr);
        if(!$powers['store_state']){
            $this->back('无该权限');
        }
		
	    $call_store = new \App\Libs\wrapper\Store();
	    $return = $call_store->store_update($data);
        if($return === 1){
            $this->back('修改成功', '200');
        }else{
            $this->back('修改失败');
        }
    }

/*
 * 获取平台分类
 */
    public function store_category(Request $request){
        $data = $request->all();
        $call_store = new \App\Libs\wrapper\Store();
        $return = $call_store->store_category($data);
        if($return === 1){
            $this->back('修改成功', '200');
        }else{
            $this->back('修改失败');
        }
    }


/*
* 获取平台+店铺
*/
    public function platform_store(Request $request){
        $data = $request->all();
        $call_store = new \App\Libs\wrapper\Store();
        $return = $call_store->platform_store($data);
        $this->back('获取成功', '200', $return['list']);
    }
	
    
    
 


	
//
}