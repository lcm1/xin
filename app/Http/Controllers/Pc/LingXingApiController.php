<?php

namespace App\Http\Controllers\Pc;

use Illuminate\Support\Facades\DB;

require_once app_path('Libs/LingXing/src/Services/OpenAPIRequestService.php');


class LingXingApiController
{
    private $client;

    public function __construct()
    {
        $this->client = new \Ak\OpenAPI\Services\OpenAPIRequestService(
            'https://openapi.lingxing.com/',
            'ak_NOTqwc3AWJ9iU',
            'ZKIv7PF6Ka42Nohmp5u6ow=='
        );
    }

    /**
     * 获取仓库列表
     * @return array
     */
    public function getWareHouseList()
    {
        $res = $this->client->makeRequest('/erp/sc/data/local_inventory/warehouse', 'post');
        return $res;
    }

    /**
     * @param $wid //本系统的仓库id
     * @param $name //仓库名
     * @return array
     */
    public function addWareHouse($wid, $name)
    {
        return $this->client->makeRequest('/erp/sc/storage/wareHouse/edit', 'post', [
            'wid'  => $wid,
            'name' => $name,
        ]);
    }

    /**
     * 添加库位
     * @param $wid //仓库id
     * @param $code //仓位名称
     * @param $type //5:可用 6:次品
     * @return array
     */
    public function addLocation($wid, $code, $type)
    {
        return $this->client->makeRequest('/erp/sc/routing/data/local_inventory/warehouseBin', 'post', [
            'wid'  => $wid,
            'code' => $code,
            'type' => $type
        ]);
    }

    /**
     * 查询库位列表
     * @return array
     */
    public function getLocationList()
    {
        $res = $this->client->makeRequest('/erp/sc/routing/data/local_inventory/warehouseBin', 'post');
        return $res;
    }

    /**
     * @param $params
     * @return array
     */
    public function addSku($params)
    {
        return $this->client->makeRequest('/erp/sc/routing/storage/product/set', 'post', $params);
    }

    /**
     * 添加sku 自动查询分类，图片，价格等信息
     * @param $sku
     * @return array
     */
    public function addSkuAuto($sku)
    {
        $skuInfo = DB::table('self_custom_sku')->where('custom_sku', $sku)->first();
        if (!$skuInfo) {
            return ['code' => 500, 'error_details' => 'self_custom_sku表找不到：' . $sku];
        }

        $name = $skuInfo->name;

        if (strstr($name, ' ')) {
            $name = str_replace(' ', '', $name);
        } elseif (strstr($name, "\n")) {
            $name = str_replace("\n", '', $name);
        } elseif (!$name) {
            $name = $skuInfo->custom_sku;
        }

        $categoryId = 0;

        if ($skuInfo->spu_id) {
            $threeCateId = DB::table('self_spu')->where('id', $skuInfo->spu_id)->value('three_cate_id');
            if ($threeCateId) {
                $categoryId = Db::table('self_category')->where('id', $threeCateId)->value('lingxing_id');
            }
        }

        try {
            $skuAddArr = [
                'sku'          => $skuInfo->custom_sku,
                'product_name' => $name,
//                'picture_list' => ['pic_url' => $skuInfo->img, 'is_primary' => 1],
                'cg_price'     => round($skuInfo->buy_price * 1.1, 2),
                'category_id'  => $categoryId
            ];

            if ($skuInfo->img) {
                $skuAddArr['picture_list'] = ['pic_url' => $skuInfo->img, 'is_primary' => 1];
            }

            $res = $this->addSku($skuAddArr);
            $res = DB::table('self_custom_sku')->where('id', $skuInfo->id)->update(['lingxing_id' => $res['data']['product_id']]);

            return $res;

        } catch (\Exception $exception) {
            return ['code' => 500, 'error_details' => $exception->getMessage()];
        }
    }

    /**
     * 查询sku列表
     * @param $offset
     * @param $length
     * @return array
     */
    public function getSkuList($offset = 0, $length = 20)
    {
        return $this->client->makeRequest('/erp/sc/routing/data/local_inventory/productList', 'post', [
            'offset' => $offset,
            'length' => $length,
        ]);
    }

    /**
     * 查询sku详情
     * @param array $sku
     * @return array
     */
    public function getSkuInfo($sku = '')
    {
        return $this->client->makeRequest('/erp/sc/routing/data/local_inventory/productInfo', 'post', [
            'sku' => $sku
        ]);
    }

    /**
     * 查询仓库库存明细
     * @param $wid
     * @param $offset
     * @param $length
     * @return array
     */
    public function inventoryDetails($wid, $offset = 0, $length = 20)
    {
        return $this->client->makeRequest('/erp/sc/routing/data/local_inventory/inventoryDetails', 'post', [
            'wid'    => $wid,
            'offset' => $offset,
            'length' => $length,
        ]);
    }

    /**
     * 添加入库单
     * @param $params
     *[
     *      'wid'          => 1, // 仓库id,喜马拉亚自己的id
     *      'type'         => 2, // 1:手工入库 2:采购入库 26:退货入库 27:移除入库
     *      'product_list' => [
     *          [
     *              'sku'       => 'ATSC100020LGM', //库存sku，全部用新的
     *              'good_num'  => '10',            //良品数量
     *              'bad_num'   => '0',             //次品数量
     *              'price'     => '0',             //单价
     *          ]
     *      ],
     *]
     * @return array
     */
    public function addInStorageOrder($params)
    {

        foreach ($params['product_list'] as $key => $val) {
            !isset($val['bad_num']) && $params['product_list'][$key]['bad_num'] = 0;
            !isset($val['price']) && $params['product_list'][$key]['price'] = 0;
        }

        try {
            $res = $this->client->makeRequest('/erp/sc/routing/storage/storage/orderAdd', 'post', $params);

            //库存sku未录入
            if ($res['code'] != 0 && isset($res['error_details']) &&
                (strstr($res['error_details'], '未匹配到对应的sku') ||
                    strstr($res['error_details'], '不存在'))) {

                foreach ($params['product_list'] as $val) {
                    $skuInfo = $this->getSkuInfo($val['sku']);

                    if ($skuInfo['code'] == 0 && $skuInfo['message'] == 'success') {
                        continue;
                    }

                    //添加sku产品
                    $this->addSkuAuto($val['sku']);
                }

                //重新添加入库单
                $res = $this->client->makeRequest('/erp/sc/routing/storage/storage/orderAdd', 'post', $params);
            }

            if ($res['code'] != 0) {
                self::writeLog('addInStorageOrder', $params, $res);
            }

            return $res;
        } catch (\Exception $exception) {

            self::writeLog('addInStorageOrder', $params, ['error_details' => $exception->getMessage()]);

            return ['code' => 500, 'error_details' => $exception->getMessage()];
        }
    }


    /**
     * 添加出库单
     * @param $params
     *[
     *      'wid'          => 1, // 仓库id,喜马拉亚自己的id
     *      'type'         => 11, //  11:手工出库 12:FBA出库 14:退货出库
     *      'product_list' => [
     *          [
     *              'sku'       => 'ATSC100020LGM', //库存sku，全部用新的
     *              'good_num'  => '10',            //良品数量
     *              'bad_num'   => '0',             //次品数量
     *          ]
     *      ],
     *]
     * @return array
     */
    public function addOutStorageOrder($params)
    {
        foreach ($params['product_list'] as $key => $val) {
            !isset($val['bad_num']) && $params['product_list'][$key]['bad_num'] = 0;
            !isset($val['price']) && $params['product_list'][$key]['price'] = 0;
        }

        try {
            $res = $this->client->makeRequest('/erp/sc/routing/storage/storage/orderAddOut', 'post', $params);
            if ($res['code'] != 0) {
                self::writeLog('addOutStorageOrder', $params, $res);
            }

            return $res;
        } catch (\Exception $exception) {

            self::writeLog('addOutStorageOrder', $params, ['error_details' => $exception->getMessage()]);
            return ['code' => 500, 'error_details' => $exception->getMessage()];
        }
    }

    public static function writeLog($name, $params = [], $res = '')
    {
        $log = var_export([
            'name'   => $name,
            'params' => $params ? json_encode($params, JSON_UNESCAPED_UNICODE) : '',
            'res'    => is_array($res) ? json_encode($res, JSON_UNESCAPED_UNICODE) : $res,
        ], true);

        $logFile = fopen(
            storage_path('logs' . DIRECTORY_SEPARATOR . date('Y-m-d').'lingxing_storage_order.log'),
            'a'
        );
        fwrite($logFile, date('Y-m-d H:i:s') . ': ' . $log . PHP_EOL);
        fclose($logFile);
    }

    /**
     * 获取分类
     * @param int $offset
     * @param int $length
     * @return array
     */
    public function getCategory($offset = 0, $length = 20)
    {
        return $this->client->makeRequest('/erp/sc/routing/data/local_inventory/category', 'post', [
            'offset' => $offset,
            'length' => $length,
        ]);
    }

    public function getListing($offset = 0, $length = 20, $sid = 0, $isPair = null)
    {
        $params = [
            'sid'    => $sid,
            'offset' => $offset,
            'length' => $length,
        ];

        $isPair && $params['is_pair'] = $isPair;

        return $this->client->makeRequest('/erp/sc/data/mws/listing', 'post', $params);
    }

    /**
     * 库存sku和亚马逊sku绑定
     * @param $params
     * @return array
     */
    public function bindMsku($params)
    {
        return $this->client->makeRequest('/erp/sc/storage/product/link', 'post', $params);
    }

    /**
     * 查询FBA发货单列表
     * @param $params
     * @return array
     */
    public function getInboundShipmentList($params = [])
    {
        return $this->client->makeRequest('/erp/sc/routing/storage/shipment/getInboundShipmentList', 'post', $params);
    }

    /**
     * 查看领星店铺列表
     * @param array $params
     * @return array
     */
    public function getSellerList($params = [])
    {
        return $this->client->makeRequest('/erp/sc/data/seller/lists', 'get', $params);
    }

    /**
     * 生成（已发货）发货单
     * @param array $params
     * @return array
     */
    public function createSendedOrder($params = [])
    {
        return $this->client->makeRequest('/erp/sc/routing/storage/shipment/createSendedOrder', 'post', $params);
    }

    /**
     * 生成货件
     * @param array $params
     * @return array
     */
    public function createInboundShipment($params = [])
    {
        return $this->client->makeRequest('/erp/sc/routing/fba/shipment/createInboundShipment', 'post', $params);
    }

    /**
     * 删除货件
     * @param array $params
     * @return array
     */
    public function deleteInboundShipment($params = [])
    {
        return $this->client->makeRequest('/erp/sc/routing/fba/shipment/deleteInboundShipment', 'post', $params);
    }

    /**
     * 批量删除货件
     * @param array $shipmentIds
     * @param int $sid
     */
    public function batchDeleteInboundShipment($shipmentIds = [], $sid = 0)
    {
        foreach ($shipmentIds as $shipmentId) {
            $this->deleteInboundShipment([
                'shipment_id' => $shipmentId,
                'sid'         => $sid,
            ]);
        }
    }

    /**
     * 生成货件计划
     * @param array $params
     * @return array
     */
    public function createInboundShipmentPlan($params = [])
    {
        return $this->client->makeRequest('/erp/sc/routing/fba/shipment/createInboundShipmentPlan', 'post', $params);
    }

    /**
     * 编辑FBA发货计划
     * @param array $params
     * @return array
     */
    public function updateShipmentPlan($params = [])
    {
        return $this->client->makeRequest('/erp/sc/routing/storage/shipment/updateShipmentPlan', 'post', $params);
    }

    /**
     * 查询FBA发货计划
     * @param array $params
     * @return array
     */
    public function shipmentPlanLists($params = [])
    {
        return $this->client->makeRequest('/erp/sc/data/fba_report/shipmentPlanLists', 'post', $params);
    }

    /**
     * 地址簿-发货地址列表
     * @param array $params
     * @return array
     */
    public function shipFromAddressList($params = [])
    {
        $params = [
            'offset' => 0,
            'length' => 1000,
        ];
        return $this->client->makeRequest('/erp/sc/routing/fba/shipment/shipFromAddressList', 'post', $params);
    }

    /**
     * 查询产品表现
     * @param array $params
     * @return array
     */
    public function asinList($params = [])
    {

        return $this->client->makeRequest('/bd/productPerformance/openApi/asinList', 'post', $params);
    }

    /**
     * 获取亚马逊订单
     * @param array $params
     * @return array
     */
    public function getMwsOrders($params = [])
    {
        return $this->client->makeRequest('/erp/sc/data/mws/orders', 'post', $params);
    }

    /**
     * 获取亚马逊订单
     * @param array $params
     * @return array
     */
    public function getOrdersDetail($params = [])
    {
        return $this->client->makeRequest('/erp/sc/data/mws/orderDetail', 'post', $params);
    }

    /**
     * 获取订单
     * @param array $params
     * @return array
     */
    public function getOrders($params = [])
    {
        return $this->client->makeRequest('/pb/mp/order/list', 'post', $params);
    }

    /**
     * 查询多平台店铺信息
     * @param int $offset
     * @param int $length
     * @param string $platformCode
     * @return array
     */
    public function getMpSellerList($offset = 0, $length = 20, $platformCode = '')
    {
        $params = [
            'offset'        => $offset,
            'length'        => $length,
            'platform_code' => $platformCode,
        ];

        return $this->client->makeRequest('/pb/mp/shop/v2/getSellerList', 'post', $params);
    }

    /**
     * SP广告活动报表
     * @param array $params
     * @return array
     */
    public function spCampaignReports($params = [])
    {
        return $this->client->makeRequest('/pb/openapi/newad/spCampaignReports', 'post', $params);
    }

    /**
     * SB广告活动报表
     * @param array $params
     * @return array
     */
    public function hsaCampaignReports($params = [])
    {
        return $this->client->makeRequest('/pb/openapi/newad/hsaCampaignReports', 'post', $params);
    }

    /**
     * SD广告活动报表
     * @param array $params
     * @return array
     */
    public function sdCampaignReports($params = [])
    {
        return $this->client->makeRequest('/pb/openapi/newad/sdCampaignReports', 'post', $params);
    }

    /**
     * SB广告的投放报告
     * @param array $params
     * @return array
     */
    public function listHsaTargetingReport($params = [])
    {
        return $this->client->makeRequest('/pb/openapi/newad/listHsaTargetingReport', 'post', $params);
    }

    /**
     * SP广告商品报表
     * @param array $params
     * @return array
     */
    public function spProductAdReports($params = [])
    {
        return $this->client->makeRequest('/pb/openapi/newad/spProductAdReports', 'post', $params);
    }

    /**
     * SD广告商品报表
     * @param array $params
     * @return array
     */
    public function sdProductAdReports($params = [])
    {
        return $this->client->makeRequest('/pb/openapi/newad/sdProductAdReports', 'post', $params);
    }

    /**
     * SP广告组
     * @param array $params
     * @return array
     */
    public function spAdGroups($params = [])
    {
        return $this->client->makeRequest('/pb/openapi/newad/spAdGroups', 'post', $params);
    }

    /**
     * SB广告组
     * @param array $params
     * @return array
     */
    public function sbAdGroups($params = [])
    {
        return $this->client->makeRequest('/pb/openapi/newad/hsaAdGroups', 'post', $params);
    }

    /**
     * SD广告组
     * @param array $params
     * @return array
     */
    public function sdAdGroups($params = [])
    {
        return $this->client->makeRequest('/pb/openapi/newad/sdAdGroups', 'post', $params);
    }

    /**
     * 广告组合
     * @param array $params
     * @return array
     */
    public function portfolios($params = [])
    {
        return $this->client->makeRequest('/pb/openapi/newad/portfolios', 'post', $params);
    }

    /**
     * SP广告活动
     * @param array $params
     * @return array
     */
    public function spCampaigns($params = [])
    {
        return $this->client->makeRequest('/pb/openapi/newad/spCampaigns', 'post', $params);
    }

    /**
     * SB广告活动
     * @param array $params
     * @return array
     */
    public function hsaCampaigns($params = [])
    {
        return $this->client->makeRequest('/pb/openapi/newad/hsaCampaigns', 'post', $params);
    }

    /**
     * SD广告活动
     * @param array $params
     * @return array
     */
    public function sdCampaigns($params = [])
    {
        return $this->client->makeRequest('/pb/openapi/newad/sdCampaigns', 'post', $params);
    }

    /**
     * 库龄报告
     * @param array $params
     * @return array
     */
    public function GetStorageAge($params = [])
    {
        return $this->client->makeRequest('/erp/sc/routing/fba/fbaStock/getFbaAgeList', 'get', $params);
    }



}