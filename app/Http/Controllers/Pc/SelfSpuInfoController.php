<?php

namespace App\Http\Controllers\Pc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SelfSpuInfoModel;
use Illuminate\Support\Facades\DB;

class SelfSpuInfoController extends Controller
{
    private $model;
    private $request;

    public function __construct(Request $request)
    {
        $this->model = new SelfSpuInfoModel();
        $this->request = $request;
    }

    /**
     * 上传spu图片
     */
    public function SaveSpuImgs()
    {

        $params = $this->request->all();

        $result = $this->model->SaveSpuImgsM($params);
        $this->AddSysLog($params,$result,'spu基础信息-上传spu图片',__METHOD__);

        return $this->Ts_back($result);
    }

    /**
     * 获取spu图片
     */
    public function GetSpuImgs()
    {

        $params = $this->request->all();

        $result = $this->model->GetSpuImgsM($params);

        return $this->Ts_back($result);
    }


    /**
     * 删除spu图片
     */
    public function DelSpuImgs()
    {

        $params = $this->request->all();

        $result = $this->model->DelSpuImgsM($params);
        $this->AddSysLog($params,$result,'spu基础信息-删除spu图片',__METHOD__);

        return $this->Ts_back($result);
    }

    
    /**
     * 上传spu信息
     */
    public function SaveSpuInfo()
    {

        $params = $this->request->all();

        $result = $this->model->SaveSpuInfoM($params);
        $this->AddSysLog($params,$result,'spu基础信息-修改spu信息',__METHOD__);

        return $this->Ts_back($result);
    }

    /**
     * 获取spu信息
     */
    public function GetSpuInfo()
    {

        $params = $this->request->all();

        $result = $this->model->GetSpuInfoM($params);

        return $this->Ts_back($result);
    }

    //箱规列表
    public function CustomSkuBoxList()
    {

        $params = $this->request->all();

        $result = $this->model->CustomSkuBoxList($params);

        return $this->Ts_back($result);
    }
    

        /**
     * 导入库存sku箱号
     */
    public function ImportCustomSkuBox()
    {

        $params = $this->request->all();

        $result = $this->model->ImportCustomSkuBox($params);

        return $this->Ts_back($result);
    }

    /**
     * 获取spu状态列表
     */
    public function getSpuStatusType()
    {

        $params = $this->request->all();

        $result = $this->model->getSpuStatusType($params);

        return $this->Ts_back($result);
    }

    /**
     * 修改spu状态
     */
    public function UpdateSpuStatus()
    {

        $params = $this->request->all();

        $result = $this->model->UpdateSpuStatus($params);

        return $this->Ts_back($result);
    }

    
    /**
     * 获取spu-cus信息
     */
    public function GetSpuCus()
    {

        $params = $this->request->all();

        $result = $this->model->GetSpuCusM($params);
        return $this->Ts_back($result);
    }

    /**
     * 获取spu信息-颜色
     */
    public function GetSpuColor()
    {

        $params = $this->request->all();

        $result = $this->model->GetSpuColorM($params);

        return $this->Ts_back($result);
    }

    /**
     * 获取spu信息-尺码
     */
    public function GetSpuSize()
    {

        $params = $this->request->all();

        $result = $this->model->GetSpuSizeM($params);

        return $this->Ts_back($result);
    }

    /**
     * 获取cus信息
     */
    public function GetCusInfo()
    {

        $params = $this->request->all();

        $result = $this->model->GetCusInfosM($params);
        // $this->AddSysLog($params,$result,'获取cus信息');
        return $this->Ts_back($result);
    }

    /**
     * 新增修改cus信息
     */
    public function SaveCusInfo()
    {

        $params = $this->request->all();

        $result = $this->model->SaveCusInfoM($params);
        $this->AddSysLog($params,$result,'spu基础信息-新增sku信息',__METHOD__);
        return $this->Ts_back($result);
    }


    /**
     * 批量修改cus信息
     */
    public function UpdateCusInfo()
    {

        $params = $this->request->all();

        $result = $this->model->UpdateCusInfoM($params);
        $this->AddSysLog($params,$result,'spu基础信息-批量修改sku信息',__METHOD__);
        return $this->Ts_back($result);
    }



    /**
     * 上架平台
     */
    public function SellCus()
    {

        $params = $this->request->all();

        $result = $this->model->SellCusM($params);
        $params['log_find_id'] = $params['id'];
        $this->AddSysLog($params,$result,'spu基础信息-上架平台',__METHOD__);

        return $this->Ts_back($result);
    }

    /**
     * 删除平台
     */
    public function DelSellCus()
    {

        $params = $this->request->all();

        $result = $this->model->DelSellCusM($params);
        $this->AddSysLog($params,$result,'spu基础信息-下架平台',__METHOD__);

        return $this->Ts_back($result);
    }


    /**
     * 获取平台
     */
    public function GetSellCus()
    {

        $params = $this->request->all();

        $result = $this->model->GetSellCusM($params);

        return $this->Ts_back($result);
    }


    public function GetSysLogByspuInfo()
    {

        $params = $this->request->all();

        $params['name'] = 'spu基础信息';
        $params['input'] = '"log_spu":'.'"'.$params['log_spu'].'"';
        $result = $this->model->GetSysLog($params);
        return $this->Ts_back($result);
    }

    //获取日志
    public function GetSysLog()
    {

        $params = $this->request->all();
        $result = $this->model->GetSysLog($params);
        return $this->Ts_back($result);
    }


    //模拟请求接口
    public function SysTry(){
        // $params = $this->request->all();
        // $id = $params['id'];
        // $res = db::table('sys_log')->where('id',$id)->first();
        // $function =$res->function;
        // $data = $res->input;
        // try {
        //     //code...
        //     // $return = '/'.$function.'('.json_decode($data).')';
        //     $return = call_user_func_array($function,json_decode($data));
        // } catch (\Throwable $th) {
        //     //throw $th;
        //     $return['type'] = 'err';
        //     $return['msg'] = $th->getMessage().'--line:'.$th->getLine().'--file'.$th->getFile();
        // }
        $return = array('11'=>8,'19'=>7,'5'=>15,'90'=>1);
        arsort($return);
        $data['type'] = 'success';
        $data['data'] = $return;
        return $this->Ts_back($data);
    }
    public function Ts_back($params){

        if($params['type']=='success'){
            return $this->back('获取成功', '200', $params['data']);
        }else{
            return $this->back($params['msg'], '300');
        }
    }

    public function getSpuPriceList()
    {
        $params = $this->request->all();
        $result = $this->model->getSpuPriceList($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500, $result['data'] ?? []);
    }

    
}