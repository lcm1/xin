<?php

namespace App\Http\Controllers\Pc;


use App\Http\Controllers\Jobs\CloudHouseController;
use App\Models\ApiresModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\CloudHouse;
use App\Http\Requests\CloudHouse as CloudHouses;
use mysql_xdevapi\BaseResult;


class ApiresController extends Controller
{
    /**
     * spu/sku生成模块
     * $model 模型
     * request 参数
     */

    private $model;
    private $request;
    private $validate;
    static public $appKey = 'WX_API_ERJSDH';
    static public $appSecret = 'zity666888';

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->model = new ApiresModel();
    }



    //入库单确认
    public function in_confirm(){

        $params = $this->request->all();
        $log_data['params'] = $params;
        $log_type = 3;
        $content = null;

        if($params['format']=='xml'){

            $content = $this->request->getContent();
            if(!empty($content)){
                $content = simplexml_load_string($content);
                $log_data['content'] = json_decode(json_encode($content),true);
            }

        }
        CloudHouseController::set_log($log_type,CloudHouseController::$success,json_encode($log_data));

        $sign_check = $this->model->param_check($params);

        if($sign_check==$params['sign']){

            if(isset($log_data['content']['entryOrder']['entryOrderCode'])){

                $order_no = $log_data['content']['entryOrder']['entryOrderCode'];

                if(isset($log_data['content']['orderLines']['orderLine'])){
                    $is_push = DB::table('goods_transfers')->where('order_no',$order_no)->select('is_push')->first();
                    if(!empty($is_push)){
                        if($is_push->is_push==3){
                            CloudHouseController::set_log($log_type,CloudHouseController::$fail,'重复入库确认');
                            $result = $this->model->xml_fail('data_repeat');
                            return $result;
                        }
                    }
                    $datetime = date('Y-m-d H:i:s');

                    if(isset($log_data['content']['orderLines']['orderLine']['itemCode'])){
                        $orderline = $log_data['content']['orderLines'];
                    }else{
                        $orderline = $log_data['content']['orderLines']['orderLine'];
                    }
                    //实际收货总数
                    $total_receive = 0;
                    foreach ($orderline as $order){

                        $custom_sku = DB::table('self_custom_sku')->where('custom_sku',$order['itemCode'])->orWhere('old_custom_sku',$order['itemCode'])->select('spu','id','spu_id')->first();
                        if(empty($custom_sku)){
                            CloudHouseController::set_log($log_type,CloudHouseController::$fail,'系统没有此数据');
                            $result = $this->model->xml_fail('data_none');
                            return $result;
                        }else{
                            $spudata = DB::table('self_spu')->where('spu',$custom_sku->spu)->select('old_spu')->first();
                            if(empty($spudata)){
                                CloudHouseController::set_log($log_type,CloudHouseController::$fail,'系统没有此数据');
                                $result = $this->model->xml_fail('data_none');
                                return $result;
                            }else{
                                if($spudata->old_spu){
                                    $spu = $spudata->old_spu;
                                }else{
                                    $spu = $custom_sku->spu;
                                }
                            }
                        }

                        $record = DB::table('cloudhouse_record')->where('order_no',$order_no)->where('custom_sku',$order['itemCode'])->where('type',2)->select('id')->first();
                        //新增出入库记录
                        if(!empty($record)){
                            $update_record = DB::table('cloudhouse_record')->where('id',$record->id)->update(['num'=>$order['actualQty'],'updatetime'=>$datetime]);
                        }else{
                            $insert['spu'] = $spu;
                            $insert['custom_sku'] = $order['itemCode'];
                            $insert['type'] = 2;
                            $insert['order_no'] = $order_no;
                            $insert['num'] = $order['actualQty'];
                            $insert['custom_sku_id'] = $custom_sku->id;
                            $insert['spu_id'] = $custom_sku->spu_id;
                            $insert['createtime'] = $datetime;
                            $insert['warehouse_id'] = 3;
                            $insert['type_detail'] = 1;
                            $insert_record = DB::table('cloudhouse_record')->insert($insert);
                        }
                        //修改实际收货数据
                        $update_receive = DB::table('goods_transfers_detail')->where('order_no',$order_no)->where('custom_sku',$order['itemCode'])->update(['receive_num'=>$order['actualQty'],'receive_time'=>$datetime]);
                        $total_receive += $order['actualQty'];
                        //修改库存
                        $self_custom_sku = DB::table('self_custom_sku')->where('custom_sku',$order['itemCode'])->orWhere('old_custom_sku',$order['itemCode'])->select('id','cloud_num','factory_num')->first();
                        if(!empty($self_custom_sku)){
                            $cloud_num = $self_custom_sku->cloud_num + $order['actualQty'];
                            $factory_num = $self_custom_sku->factory_num - $order['actualQty'];
                            $update_num = DB::table('self_custom_sku')->where('id',$self_custom_sku->id)->update(['cloud_num'=>$cloud_num,'factory_num'=>$factory_num]);
                        }

                    }
                    //修改实际收货总数，实际收货时间,推送状态
                    $update_sjtime = DB::table('goods_transfers')->where('order_no',$order_no)->update(['sj_arrive_time'=>$datetime,'receive_total_num'=>$total_receive,'is_push'=>3]);
                    CloudHouseController::set_log($log_type,CloudHouseController::$success,'请求成功');
                    $result = $this->model->xml_success();
                }else{
                    CloudHouseController::set_log($log_type,CloudHouseController::$fail,'XML参数错误');
                    $result = $this->model->xml_fail('data_error');
                    return $result;
                }
            }else{
                CloudHouseController::set_log($log_type,CloudHouseController::$fail,'XMl参数错误');
                $result = $this->model->xml_fail('data_error');
                return $result;
            }
        }else{
            CloudHouseController::set_log($log_type,CloudHouseController::$fail,'非法参数');
            $result = $this->model->xml_fail('invalid');
        }
        return $result;
    }

    //出库单确认
    public function out_confirm(){

        $params = $this->request->all();
        $log_data['params'] = $params;
        $log_type = 5;
        $content = null;


        if($params['format']=='xml'){

            $content = $this->request->getContent();
            if(!empty($content)){
                $content = simplexml_load_string($content);
                $log_data['content'] = json_decode(json_encode($content),true);
            }

        }
        CloudHouseController::set_log($log_type,CloudHouseController::$success,json_encode($log_data));
        $sign_check = $this->model->param_check($params);

        if($sign_check==$params['sign']){

            if(isset($log_data['content']['deliveryOrder']['deliveryOrderCode'])){
                $planid = $log_data['content']['deliveryOrder']['deliveryOrderCode'];
                $type = $log_data['content']['deliveryOrder']['orderType'];
                if($type=='DBCK'||$type=='CGTH'||$type=='PTCK'){
                    $plan = DB::table('goods_transfers')->where('order_no',$planid)->select('id','is_push','type_detail','shop_id')->first();
                }
                if($type=='B2BCK'){
                    $plan = DB::table('amazon_buhuo_request')->where('id',$planid)->select('request_status','shop_id')->first();
                }
                db::beginTransaction();    //开启事务
                if(empty($plan)){
                    $result = $this->model->xml_fail('data_none');
                    CloudHouseController::set_log($log_type,CloudHouseController::$fail,'系统没有此数据');
                    return $result;
                }else{
                    if($type=='B2BCK'){
                        if($plan->request_status==7){
                            $request_id_json = substr(substr(json_encode(array("request_id" => "{$planid}")), 1), 0, -1);
                            $task_data = Db::table('tasks')->where('ext', 'like', "%{$request_id_json}%")->where('state','!=',2)->select('Id', 'name', 'user_fz')->first();
                            if(!empty($task_data)){
                                $task_son = Db::table('tasks_son')->where('task_id',$task_data->Id)->select('Id')->get();
                                $task['Id'] = $task_son[2]->Id;
                                //修改任务状态位进行中
                                $update_son = Db::table('tasks_son')->where('Id',$task['Id'])->update(['state'=>2]);
                                $task['state'] = 3;
                                $task['feedback_type'] = 1;
                                $call_task = new \App\Libs\wrapper\Task();
                                $return = $call_task->task_son_complete($task);
                                if ($return === 1) {
                                    $update_status = DB::table('amazon_buhuo_request')->where('id',$planid)->update(['request_status'=>8]);
                                    //返回成功
                                    CloudHouseController::set_log($log_type,CloudHouseController::$success,'请求成功，已出库');
                                    $result = $this->model->xml_success();
                                } else {
                                    db::rollback();// 回调
                                    CloudHouseController::set_log($log_type,CloudHouseController::$fail,'出库任务完成错误');
                                    $result = $this->model->xml_fail('task_error');
                                    return $result;
                                }
                            }
                        }else{
                            $result = $this->model->xml_fail('data_repeat');
                            CloudHouseController::set_log($log_type,CloudHouseController::$fail,'重复确认出库');
                            return $result;
                        }
                    }

                    if($type=='DBCK'||$type=='CGTH'||$type=='PTCK'){
                        if($plan->is_push==3){
                            $result = $this->model->xml_fail('data_repeat');
                            CloudHouseController::set_log($log_type,CloudHouseController::$fail,'重复确认出库');
                            return $result;
                        }
//                        $update_goods = DB::table('goods_transfers')->where('order_no',$planid)->update(['is_push'=>3,'out_house_time'=>date('Y-m-d H:i:s')]);
                    }

                }
                if(isset($log_data['content']['orderLines']['orderLine'])){
                    $goods_transfers_box = DB::table('goods_transfers_box')->where('order_no',$planid)->select('box_num','id','onshelf_num')->first();
                    $goods_transfers = DB::table('goods_transfers')->where('order_no',$planid)->select('type_detail')->first();
                    $datetime = date('Y-m-d H:i:s');

                    if(isset($log_data['content']['orderLines']['orderLine']['itemCode'])){
                        $orderline = $log_data['content']['orderLines'];
                    }else{
                        $orderline = $log_data['content']['orderLines']['orderLine'];
                    }
                    $detail_num = 0;
                    foreach ($orderline as $order){
                        $custom_sku = DB::table('self_custom_sku as a')->leftJoin('self_spu as b','a.spu','=','b.spu')->where('a.custom_sku',$order['itemCode'])->orwhere('a.old_custom_sku',$order['itemCode'])->select('b.spu','b.old_spu','a.id','a.spu_id','a.cloud_num')->first();

                        $record = DB::table('cloudhouse_record')->where('order_no',$planid)->where('custom_sku',$order['itemCode'])->where('type',1)->select('id','num')->first();

                        if(!empty($record)){
                            if($record->num!=$order['actualQty']){
                                if($type=='DBCK'||$type=='CGTH'||$type=='PTCK'){
                                    if($record->num>$order['actualQty']){
                                        $nowNum = $custom_sku->cloud_num - ($record->num - $order['actualQty']);
                                        DB::table('self_custom_sku')->where('id',$custom_sku->id)->update(['cloud_num'=>$nowNum]);
                                    }
                                }
                                $update_record = DB::table('cloudhouse_record')->where('id',$record->id)->update(['num'=>$order['actualQty'],'updatetime'=>$datetime]);
                            }
                        }else{
                            $cloud_num = $custom_sku->cloud_num - $order['actualQty'];
                            if($custom_sku->old_spu){
                                $insert['spu'] = $custom_sku->old_spu;
                            }else{
                                $insert['spu'] = $custom_sku->spu;
                            }
                            if($type=='B2BCK'){
                                $insert['type_detail'] = 6;
                                $insert['custom_sku'] = $order['itemCode'];
                                $insert['type'] = 1;
                                $insert['order_no'] = $planid;
                                $insert['num'] = $order['actualQty'];
                                $insert['custom_sku_id'] = $custom_sku->id;
                                $insert['spu_id'] = $custom_sku->spu_id;
                                $insert['createtime'] = $datetime;
                                $insert['warehouse_id'] = 3;
                                $insert['shop_id'] = $plan->shop_id;
                                $insert['now_inventory'] = $cloud_num;
                                $insert_record = DB::table('cloudhouse_record')->insert($insert);
//                                $self_custom_sku = DB::table('self_custom_sku')->where('custom_sku',$order['itemCode'])->orWhere('old_custom_sku',$order['itemCode'])->select('id','cloud_num')->first();
//                                if(!empty($self_custom_sku)){

//                                }
                            }else{
                                $insert['type_detail'] = $goods_transfers->type_detail;
                                $insert['custom_sku'] = $order['itemCode'];
                                $insert['type'] = 1;
                                $insert['order_no'] = $planid;
                                $insert['num'] = $order['actualQty'];
                                $insert['custom_sku_id'] = $custom_sku->id;
                                $insert['spu_id'] = $custom_sku->spu_id;
                                $insert['createtime'] = $datetime;
                                $insert['warehouse_id'] = 3;
                                $insert['shop_id'] = $plan->shop_id;
                                $insert['now_inventory'] = $cloud_num;
                                $insert_record = DB::table('cloudhouse_record')->insert($insert);
                            }

                            $update_num = DB::table('self_custom_sku')->where('id',$custom_sku->id)->update(['cloud_num'=>$cloud_num]);
                            $update_num2 = DB::table('cloudhouse_location_num as a')->leftJoin('cloudhouse_warehouse_location as b','a.location_id','=','b.id')
                                ->where('a.custom_sku_id', $custom_sku->id)->where('b.warehouse_id',3)->update(['a.num'=>$cloud_num]);
                            if(!empty($goods_transfers_box)){
                                DB::table('goods_transfers_box_detail')->where('box_id',$goods_transfers_box->id)->where('custom_sku_id',$custom_sku->id)
                                    ->update(['shelf_num'=>$order['actualQty']]);
                            }


                        }
                        DB::table('goods_transfers_detail')->where('order_no',$planid)->where('custom_sku',$order['itemCode'])->update(['receive_num'=>$order['actualQty'],'receive_time'=>$datetime]);
                        $detail_num += $order['actualQty'];
                    }
                    if($detail_num!=0){
                        DB::table('goods_transfers')->where('order_no',$planid)->update(['receive_total_num'=>$detail_num,'is_push'=>3,'out_house_time'=>date('Y-m-d H:i:s'),'confirm_id'=>423]);
                        if(!empty($goods_transfers_box)){
                            DB::table('goods_transfers_box')->where('id',$goods_transfers_box->id)->update(['onshelf_num'=>$detail_num,'status'=>2]);
                        }
                    }
                    CloudHouseController::set_log($log_type,CloudHouseController::$success,'请求成功，已加入出库记录');
                    $result = $this->model->xml_success();
                    db::commit();// 确认
                }else{
                    db::rollback();// 回调
                    CloudHouseController::set_log($log_type,CloudHouseController::$fail,'非法参数');
                    $result = $this->model->xml_fail('invalid');
                    return $result;
                }
            }
        }else{
            CloudHouseController::set_log($log_type,CloudHouseController::$fail,'非法参数');
            $result = $this->model->xml_fail('invalid');

        }
        return $result;

    }

    //装箱单数据返回
    public function box_receive(){

        $params = $this->request->all();
        $log_data['params'] = $params;
        $log_type = 7;
        $content = null;


        if($params['format']=='xml'){
            $content = $this->request->getContent();
            if(!empty($content)){
                $content = simplexml_load_string($content);
                $log_data['content'] = json_decode(json_encode($content),true);
            }
        }
        CloudHouseController::set_log($log_type,CloudHouseController::$success,json_encode($log_data));
        $sign_check = $this->model->param_check($params);

        if($sign_check==$params['sign']){

            if(isset($log_data['content']['deliveryOrder']['deliveryOrderCode'])){

                $planid = $log_data['content']['deliveryOrder']['deliveryOrderCode'];
                $plan = DB::table('amazon_buhuo_request')->where('id',$planid)->select('id','request_status')->first();
                if(empty($plan)){
                    CloudHouseController::set_log($log_type,CloudHouseController::$fail,'系统没有此数据');
                    $result = $this->model->xml_fail('data_none');
                    return $result;
                }else{
                    //已装箱则直接返回成功
                    if($plan->request_status==6){
                        CloudHouseController::set_log($log_type,CloudHouseController::$success,'未生成pdf');
                        $result = $this->model->xml_data_success('');
                        return $result;
                    }
                    //已创货件则直接返回货件下载地址
                    if($plan->request_status==7){
                        $goods = DB::table('amazon_create_goods')->where('request_id',$planid)->select('file_path')->get()->toArray();
                        $data = '';
                        if(!empty($goods)){
                            foreach ($goods as $pdf){
                                $pdf->file_path = 'http://8.134.97.20:9998'.$pdf->file_path;
                                $data .= '@'.$pdf->file_path;
                            }
                        }
                        $data = substr($data, 1);
                        CloudHouseController::set_log($log_type,CloudHouseController::$fail,'已返回pdf地址');
                        $result = $this->model->xml_data_success($data);
                        return $result;
                    }
                    //未装箱则获取数据填入对应箱子
                    $detail = DB::table('amazon_buhuo_detail')->where('request_id',$planid)->select('sku','custom_sku')->get()->toArray();

                    if(empty($detail)){
                        CloudHouseController::set_log($log_type,CloudHouseController::$fail,'系统没有此数据');
                        $result = $this->model->xml_fail('data_none');
                        return $result;
                    }

                    if(isset($log_data['content']['packages'])){
                        $box_count = count($log_data['content']['packages']['package']);
                        $skulist = array();
                        foreach ($detail as $d){
                            for($i=0;$i<$box_count;$i++){
                                $skulist[$d->custom_sku][$i] = 0;
                            }
                        }
                        db::beginTransaction();    //开启事务
                        if(isset($log_data['content']['packages']['package']['items'])){
                            $packVal = $log_data['content']['packages'];
                        }else{
                            $packVal = $log_data['content']['packages']['package'];
                        }

                        foreach ($packVal as $pack) {
                            $box_num = 0;
                            if (isset($pack['sort']) && isset($pack['length']) && isset($pack['height']) && isset($pack['width']) && isset($pack['weight']) && isset($pack['items'])) {
                                if(isset($pack['items']['item']['itemCode'])){
                                    $for_item = $pack['items'];
                                }else{
                                    $for_item = $pack['items']['item'];
                                }
                                foreach ($for_item as $item) {
                                    if (isset($item['itemCode']) && $item['actualQty']) {
                                        //箱子合计装箱数量
                                        $box_num += $item['actualQty'];
                                        //键值
                                        $key = $pack['sort'] - 1;
                                        if (isset($skulist[$item['itemCode']])) {
                                            $skulist[$item['itemCode']][$key] = $item['actualQty'];
                                        }
                                    } else {
                                        CloudHouseController::set_log($log_type, CloudHouseController::$fail, 'xml参数错误');
                                        $result = $this->model->xml_fail('data_error');
                                        return $result;
                                    }
                                }
                                //修改或新增箱子的长宽高重量以及合计数量
                                $is_box = DB::table('amazon_box_data')->where('request_id', $planid)->where('box_id', $pack['sort'])->select('id')->first();
                                if (!empty($is_box)) {
                                    $update_box = DB::table('amazon_box_data')
                                        ->where('request_id', $planid)->where('box_id', $pack['sort'])
                                        ->update(['heavy' => $pack['weight'], 'Long' => $pack['length'], 'high' => $pack['height'], 'wide' => $pack['width'], 'box_num' => $box_num]);
                                } else {
                                    $insert_box = DB::table('amazon_box_data')
                                        ->insert(['request_id' => $planid, 'heavy' => $pack['weight'], 'Long' => $pack['length'], 'high' => $pack['height'], 'wide' => $pack['width'], 'box_num' => $box_num, 'box_id' => $pack['sort'], 'creat_time' => time()]);
                                }
                            } else {
                                CloudHouseController::set_log($log_type, CloudHouseController::$fail, 'xml参数错误');
                                $result = $this->model->xml_fail('data_error');
                                return $result;
                            }
                        }
                        foreach ($detail as $dd){
                            $box_data_detail = json_encode($skulist[$dd->custom_sku]);
                            $update_detail =  DB::table('amazon_buhuo_detail')->where('request_id',$planid)->where('custom_sku',$dd->custom_sku)->update(['box_data_detail'=>$box_data_detail]);
                        }
                        //数据填充成功修改计划状态未“已装箱，待创货件”,并在任务节点完成
                        $request_id_json = substr(substr(json_encode(array("request_id" => "{$planid}")), 1), 0, -1);
                        $task_data = Db::table('tasks')->where('ext', 'like', "%{$request_id_json}%")->where('state','!=',2)->select('Id', 'name', 'user_fz')->first();
                        if(!empty($task_data)){
                            $task_son = Db::table('tasks_son')->where('task_id',$task_data->Id)->select('Id')->get();
                            $task['Id'] = $task_son[0]->Id;
                            $task['state'] = 3;
                            $task['feedback_type'] = 1;
                            $call_task = new \App\Libs\wrapper\Task();
                            $return = $call_task->task_son_complete($task);
                            if ($return === 1) {
                                $update_status = DB::table('amazon_buhuo_request')->where('id',$planid)->update(['request_status'=>6]);
                                //返回成功
                                CloudHouseController::set_log($log_type,CloudHouseController::$success,'请求成功，已装箱');
                                $result = $this->model->xml_success();
                            } else {
                                CloudHouseController::set_log($log_type,CloudHouseController::$fail,'装箱任务完成错误');
                                $result = $this->model->xml_fail('task_error');
                                return $result;
                            }
                        }

                    }else{
                        CloudHouseController::set_log($log_type,CloudHouseController::$fail,'xml参数错误');
                        $result = $this->model->xml_fail('data_error');
                        return $result;
                    }
                }
            }else{
                CloudHouseController::set_log($log_type,CloudHouseController::$fail,'xml参数错误');
                $result = $this->model->xml_fail('data_error');
                return $result;
            }
        }else{
            CloudHouseController::set_log($log_type,CloudHouseController::$fail,'非法参数');
            $result = $this->model->xml_fail('invalid');
        }
        db::commit();// 确认
        return $result;
    }


    /**
     * 节点任务--（完成）用于外部调用
     */
    public function task_son_complete_go(Request $request)
    {

        $data = $request->all();
        $call_task = new \App\Libs\wrapper\Task();
        $return = $call_task->task_son_complete($data);
//		var_dump($return);exit;
        if ($return === 1) {
            $this->back('操作成功', '200');
        } else {
            $this->back($return);
        }
    }

    //查询历史下单数据,用于易老板调用
    public function GetHistoryOrder(Request $request){
        $data = $request->all();

        $return = $this->model->GetHistoryOrder($data);

        return $this->back('获取成功', '200',$return);
    }

}
