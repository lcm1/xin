<?php

namespace App\Http\Controllers\Pc;

use App\Http\Controllers\Controller;
use App\Models\BaseModel;
use App\Models\Common\Constant;
use App\Models\FinanceModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FinanceController extends Controller
{
    protected $request;
    protected $model;
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->model = new FinanceModel();
    }

    /**
     * @Desc：保存付账款单
     * @author: Liu Sinian
     * @Time: 2023/7/19 10:28
     */
    public function savePaymentBill()
    {
        $params = $this->request->all();
        $result = $this->model->savePaymentBill($params);
        $name = '新增付账款单';
        if (isset($params['bill_id'])){
            $name = '更新付账款单';
        }
        $this->AddSysLog($params, $result, $name, __METHOD__);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc:获取付账单下拉菜单选项列表
     * @author: Liu Sinian
     * @Time: 2023/7/21 8:58
     */
    public function getPaymentBillSelectionList()
    {
        $params = $this->request->all();
        $result = $this->model->getPaymentBillSelectionList($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc: 获取付账单列表
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/7/21 9:42
     */
    public function getPaymentBillList()
    {
        $params = $this->request->all();
        $result = $this->model->getPaymentBillList($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc:获取付款单详情
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/7/25 16:16
     */
    public function getPaymentBillDetail()
    {
        $params = $this->request->all();
        $result = $this->model->getPaymentBillDetail($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc:删除付款单
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/7/25 16:15
     */
    public function deletePaymentBill()
    {
        $params = $this->request->all();
        $result = $this->model->deletePaymentBill($params);
        $this->AddSysLog($params, $result, '废弃付账单', __METHOD__);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc:提交单据
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/7/30 14:23
     */
    public function submitPaymentBill()
    {
        $params = $this->request->all();
        $result = $this->model->submitPaymentBill($params);
        $this->AddSysLog($params, $result, '提交付账单', __METHOD__);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc:获取出入库详情数据
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/8/3 9:33
     */
    public function getGoodTransfersDetail()
    {
        $params = $this->request->all();
        $result = $this->model->getGoodTransfersDetail($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc:获取供应商合同列表
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/8/8 9:29
     */
    public function getSupplierContractList()
    {
        $params = $this->request->all();
        $result = $this->model->getSupplierContractList($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc:反审核票据
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/8/9 18:13
     */
    public function deApprovalPaymenBill()
    {
        $params = $this->request->all();
        $result = $this->model->deApprovalPaymenBill($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc:获取核销列表
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/8/11 14:09
     */
    public function getFinancialVerificationList()
    {
        $params = $this->request->all();
        $result = $this->model->getFinancialVerificationList($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc:付账单确认支付
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/8/12 15:47
     */
    public function confirmPaymentBill()
    {
        $params = $this->request->all();
        $result = $this->model->confirmPaymentBill($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc:核销付账单
     * @author: Liu Sinian
     * @Time: 2023/8/12 16:26
     */
    public function writeOffPaymentBill()
    {
        $params = $this->request->all();
        $result = $this->model->writeOffPaymentBill($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc:打印票据
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/8/17 20:52
     */
    public function printBillPdf()
    {
        $params = $this->request->all();
        $result = $this->model->printBillPdf($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc:获取超期采购数据
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/8/17 20:52
     */
    public function getDelayedGoodsTransfersDetail()
    {
        $params = $this->request->all();
        $result = $this->model->getDelayedGoodsTransfersDetail($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc:获取应付账单抵扣记录列表
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/8/23 15:27
     */
    public function getFinancialBillDeductionList()
    {
        $params = $this->request->all();
        $result = $this->model->getFinancialBillDeductionList($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc:合同应付账报表
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/8/31 17:46
     */
    public function getContractPaymentAccountList()
    {
        $params = $this->request->all();
        $result = $this->model->getContractPaymentAccountList($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500, $result['data'] ?? []);
    }

    /**
     * @Desc:合同应付账周期排款表
     * @return null
     * @author: Liu Sinian
     * @Time: 2023/9/21 11:45
     */
    public function getContractPeriodicArrangementList()
    {
        $params = $this->request->all();
        $result = $this->model->getContractPeriodicArrangementList($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500, $result['data'] ?? []);
    }

    public function updateFinanceBillPdfFile()
    {
        $params = $this->request->all();
        $result = $this->model->updateFinanceBillPdfFile($params);

        return $this->back($result['msg'] ?? '未知错误！', $result['code'] ?? 500, $result['data'] ?? []);
    }
}