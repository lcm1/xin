<?php

namespace App\Http\Controllers\Pc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\VarDumper\Exception\ThrowingCasterException;
use App\Models\ImportModel;

class ImportController extends Controller
{
    private $request;

    protected $importModel;
    // 导入场景定义
    const IMPORT_SCENE = [
        'IMPORT_CUSTOM_SKU_NUM' => 'IMPORT_CUSTOM_SKU_NUM',
        'IMPORT_FASIN_LJDH' => 'IMPORT_FASIN_LJDH'
    ];

    /**
     * 控制器初始化
     * @param Request $request
     */
    public function __construct(Request $request){
        $this->request = $request;
        $this->importModel = new ImportModel();
    }

    /**
     * @Desc:导入功能
     * @param Request $request
     * @author: Liu Sinian
     * @Time: 2023/2/15 14:56
     */
    public function import(Request $request)
    {
        // 获取所有请求参数
        $re = $request->all();
        // 获取场景值
        $scene = isset($re['scene']) ? $re['scene'] : '';
        // 获取文件
        $file = isset($re['file']) ? $re['file'] : '';

        // 获取条件参数
        $params = isset($re['params']) ? json_decode($re['params'], true) : [];
        // 处理导入功能
        $result = $this->importModel->import($scene, $file, $params);

        if ($result['code'] != 200){
            return $this->back($result);
        }

        return $this->back($result, 200);
    }


}