<?php

namespace App\Http\Controllers\Pc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\InventoryTurnsModel;

class InventoryTurnsController extends Controller
{
    private $model;
    private $request;

    public function __construct(Request $request)
    {
        $this->model = new InventoryTurnsModel();
        $this->request = $request;
    }

    /**
     * 走柜spu列表
     */
    public function GetZgSpuList()
    {

        $params = $this->request->all();

        $result = $this->model->GetZgSpuList($params);

        return $this->Ts_back($result);
    }
    

    /**
     * 走柜导入
     */
    public function ZgImport()
    {

        $params = $this->request->all();

        $result = $this->model->ZgImport($params);

        return $this->Ts_back($result);
    }

    /**
     * 走柜-供应链
     */
    public function SaveZg()
    {

        $params = $this->request->all();

        $result = $this->model->SaveZg($params);

        return $this->Ts_back($result);
    }


    /**
     * 走柜-详情
     */
    public function GetZg()
    {

        $params = $this->request->all();

        $result = $this->model->GetZg($params);

        return $this->Ts_back($result);
    }

    /**
     * 走柜-运营
     */
    public function UpdateZg()
    {

        $params = $this->request->all();

        $result = $this->model->UpdateZg($params);

        return $this->Ts_back($result);
    }

    /**
     * 走柜-列表
     */
    public function GetZgList()
    {

        $params = $this->request->all();

        $result = $this->model->GetZgList($params);

        return $this->Ts_back($result);
    }


    public function Ts_back($params){

        if($params['type']=='success'){
            return $this->back('获取成功', '200', $params['data']);
        }else{
            return $this->back($params['msg'], '300');
        }
    }

}