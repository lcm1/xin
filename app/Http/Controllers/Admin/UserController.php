<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller{
//////////////////////////////////////////////// 用户
////////////////////// 用户列表
	/**
	 * 用户列表--页面
	 */
	public function user_list_html(){
		
		//获取所有角色
		$sql = "select * from powers_role";
		$roles = json_decode(json_encode(db::select($sql)),true);
		
		return view('admin/user/user_list', [
			'roles' => $roles
		]);
	}
	
	/**
	 * 用户列表--数据
	 * @param name 角色名称
	 * @param type 类型：1 管理员，2 业务员， 3 跟单员， 4 QC
	 * @apram create_time 创建时间
	 * @param page 页码
	 * @param number 每页条数
	 */
	public function user_list(Request $request){
		$data = $request->all();
		if(empty($data)){
			$this->back('接收值为空');
		}
		
		$call_user = new \App\Libs\wrapper\User();
		$return = $call_user->user_list($data);
		if(is_array($return['list'])){
			$this->back('获取成功', '200', $return);
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 新增用户
	 * @parma name 用户名
	 * @param password 密码
	 * @param email 邮箱
	 * @param type 类型：1 管理员，2 业务员， 3 跟单员， 4 QC
	 */
	public function user_add(Request $request){
		$this->validate($request, [
			'name' => 'required| string',
			'password' => 'required| string',
			'type' => 'required| integer',
		], [
			'name.required' => '请填写用户名',
			'name.string' => 'name必须是字符串',
			'password.required' => '请填写登录密码',
			'password.string' => 'password必须是字符串',
			'type.required' => '请选择用户类型',
			'type.integer' => 'type必须是整型数字',
		]);
		
		$data = $request->all();
		$call_user = new \App\Libs\wrapper\User();
		$return = $call_user->user_add($data);
		if($return === 1){
			$this->back('新增成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 用户信息
	 * @param user_id 用户id
	 */
	public function user_info(Request $request){
		$this->validate($request, [
			'user_id' => 'required| integer',
		], [
			'user_id.required' => 'user_id为空',
			'user_id.integer' => 'user_id必须是整型数字',
		]);
		
		$sql = "select *
					from users
					where `Id` = {$request->user_id}";
		$find = json_decode(json_encode(db::select($sql)),true);
		if(empty($find)){
			$this->back('无该用户');
		}else{
			$this->back('获取成功', '200', $find[0]);
		}
	}
	
	/**
	 * 编辑用户
	 * @param user_id 用户id
	 * @param name 用户名
	 * @param password 密码
	 * @param email 邮箱
	 * @param state 状态：1正常 2禁用
	 * @param type 类型：1 管理员，2 业务员， 3 跟单员， 4 QC
	 */
	public function user_update(Request $request){
		$this->validate($request, [
			'user_id' => 'required| integer',
		], [
			'user_id.required' => 'user_id为空',
			'user_id.integer' => 'user_id必须是整型数字',
		]);
		
		$data = $request->all();
		$call_user = new \App\Libs\wrapper\User();
		$return = $call_user->user_update($data);
		if($return === 1) {
			$this->back('编辑成功', '200');
		}else if($return === 2){
			$this->back('该用户之前已存在，用户状态已改为正常', '200');
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 获取用户角色
	 * @param user_id 用户id
	 */
	public function user_role_get(Request $request){
		$this->validate($request, [
			'user_id' => 'required| integer',
		], [
			'user_id.required' => 'user_id为空',
			'user_id.integer' => 'user_id必须是整型数字',
		]);
		
		$sql = "select r.Id, r.name
					from powers_role_user ru
					left join powers_role r on r.Id = ru.role_id
					where ru.user_id = {$request->user_id}";
		$roles = json_decode(json_encode(db::select($sql)),true);
		if(is_array($roles)){
			$this->back('获取成功', '200', $roles);
		}else{
			$this->back('获取失败');
		}
	}
	
	/**
	 * 修改用户角色
	 * @param user_id 用户id
	 * @param id_del 删除角色id  例如：1,2,3
	 * @param id_add 新增角色id  例如：1,2,3
	 */
	public function user_role_update(Request $request){
		$this->validate($request, [
			'user_id' => 'required| integer',
		], [
			'user_id.required' => 'user_id为空',
			'user_id.integer' => 'user_id必须是整型数字',
		]);
		
		$data = $request->all();
		$call_user = new \App\Libs\wrapper\User();
		$return = $call_user->user_role_update($data);
		if($return === 1){
			$this->back('操作成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	





}//类结束
