<?php
namespace App\Http\Controllers\Admin;

use function GuzzleHttp\Psr7\stream_for;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConfigController extends Controller{
//////////////////////////////////////////////// 系统配置
////////////////////// 系统配置
	/**
	 * 系统配置--页面
	 */
	public function config_html(){
		return view('admin/config/config');
	}
	
	/**
	 * 系统配置--数据
	 * @param access_key 秘钥
	 */
	public function config_find(Request $request){
		$data = $request->all();
		if(empty($data)){
			$this->back('接收值为空');
		}
		
		//系统配置
		$sql = "select *
					from config
					where access_key = '{$data['access_key']}'";
		$find = json_decode(json_encode(db::select($sql)),true);
		if(!empty($find)){
			$this->back('秘钥正确', '200', $find[0]);
		}else{
			$this->back('秘钥错误');
		}
	}
	
	/**
	 * 编辑系统配置
	 * @param access_key 秘钥
	 */
	public function config_update(Request $request){
		$data = $request->all();
		if(empty($data)){
			$this->back('接收值为空');
		}
		
		//修改系统配置
		$sql = "update `config`
					set `access_key`='{$data['access_key']}'
					where `Id`=1";
		$update = db::update($sql);
		if($update !== false) {
			$this->back('编辑成功', '200');
		}else{
			$this->back('编辑失败');
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	





}//
