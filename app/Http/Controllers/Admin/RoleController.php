<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoleController extends Controller{
//////////////////////////////////////////////// 角色
////////////////////// 角色列表
    /**
     * 角色列表--页面
     */
    public function role_list_html(){
        return view('admin/role/role_list');
    }
	
	/**
	 * @desc 角色列表--数据
	 * @param name 角色名称
	 * @param page 页码
	 * @param number 每页条数
	 */
	public function role_list(Request $request){
		$data = $request->all();
		if(empty($data)){
			$this->back('接收值为空');
		}
		
		$call_role = new \App\Libs\wrapper\Role();
		$return = $call_role->role_list($data);
		if(is_array($return['list'])){
			$this->back('获取成功', '200', $return);
		}else{
			$this->back($return);
		}
	}

	/**
	 * 新增角色
	 * @param name 角色名称
	 * @param desc 角色描述
	 */
    public function role_add(Request $request){
	    //验证参数
	    $this->validate($request, [
		    'name' => 'required| string'
	    ], [
		    'name.required' => '请填写角色名称',
		    'name.string' => 'name不是字符串',
	    ]);
	    
	    $data = $request->all();
	    $call_role = new \App\Libs\wrapper\Role();
	    $return = $call_role->role_add($data);
	    if($return === 1){
	        $this->back('新增成功', 200);
	    }else{
	    	$this->back($return);
	    }
    }
    
    /**
     * 角色信息
     * @param role_id 角色id
     */
	public function role_info(Request $request){
		//验证参数
		$this->validate($request, [
			'role_id' => 'required| integer'
		], [
			'role_id.required' => 'role_id为空',
			'role_id.integer' => 'role_id必须是整型数字',
		]);
		
		$data = $request->all();
		$call_role = new \App\Libs\wrapper\Role();
		$return = $call_role->role_info($data);
		if(is_array($return)){
			$this->back('获取成功', 200, $return[0]);
		}else{
			$this->back($return);
		}
	}

	/**
	 * 编辑角色
	 * @param role_id 角色id
	 * @param name 角色名称
	 * @param desc 角色描述
	 */
	public function role_update(Request $request){
		//验证参数
		$this->validate($request, [
			'role_id' => 'required| integer',
		], [
			'role_id.required' => 'role_id为空',
			'role_id.integer' => 'role_id必须是整型数字',
		]);
		
		$data = $request->all();
		$call_role = new \App\Libs\wrapper\Role();
		$return = $call_role->role_update($data);
		if($return === 1){
			$this->back('编辑成功', 200);
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 删除角色
	 * @param role_id 角色id
	 */
	public function role_del(Request $request){
		//验证参数
		$this->validate($request, [
			'role_id' => 'required| integer',
		], [
			'role_id.required' => 'role_id为空',
			'role_id.integer' => 'role_id必须是整型数字',
		]);
		
		$role_id = $request->get('role_id');
		$call_role = new \App\Libs\wrapper\Role();
		$return = $call_role->role_del($role_id);
		if($return === 1){
			$this->back('删除成功', 200);
		}else{
			$this->back($return);
		}
	}

////////////////////// 角色权限
	/**
	 * 角色权限--页面
	 * @param role_id 角色id
	 * @param name 角色名称
	 */
	public function powers_role_html(Request $request){
		//验证参数
		$data = $request->all();
		if(empty($data)){
			echo '<p style="text-align: center;font-size: 18px;color: #ccc;">接收值为空</p>';exit;
		}else if(empty($data['role_id'])){
			echo '<p style="text-align: center;font-size: 18px;color: #ccc;">role_id为空</p>';exit;
		}else if(empty($data['name'])){
			echo '<p style="text-align: center;font-size: 18px;color: #ccc;">name为空</p>';exit;
		}
		
		//权限分类
		$sql = "select * from powers_type";
        $powers_type = json_decode(json_encode(db::select($sql)),true);
		
		//权限
		$sql = "select p.*, if(prj.Id, 1, 0) as is_checked
				       from powers p
				       left join powers_role_join prj on prj.power_id = p.Id and prj.role_id = {$data['role_id']}";
		$powers = json_decode(json_encode(db::select($sql)),true);
		
		foreach ($powers_type as $k=>$v){
			/*for($i=0; $i<$num_power; $i++){
				if($v['Id'] == $powers[$i]['type_id']){
					$powers_type[$k]['children'][] = $powers[$i];
					unset($powers[$i]);
					$i--;
					$num_power--;
					if($num_power == 0){
						break;
					}
				}
			}*/
			$powers_type[$k]['children'] = array();
			foreach ($powers as $k1=>$v1){
				if($v['Id'] == $v1['type_id']){
					$powers_type[$k]['children'][] = $v1;
				}
			}
		}
		return view('admin/role/powers_role', [
			'role_id' => $data['role_id'],
			'role_name' => $data['name'],
			'power_list' => $powers_type
		]);
	}

	/**
	 * 编辑角色权限
	 * @param role_id 角色id
	 * @apram power_ids 权限id 例如：1,2,3,
	 */
	public function powers_role(Request $request){
		//验证参数
		$this->validate($request, [
			'role_id' => 'required| integer',
			'power_ids' => 'required| string',
		], [
			'role_id.required' => 'role_id为空',
			'role_id.integer' => 'role_id必须是整型数字',
			'power_ids.required' => 'power_ids为空',
			'power_ids.string' => 'power_ids必须是字符串',
		]);
		
		$data = $request->all();
		$call_role = new \App\Libs\wrapper\Role();
		$return = $call_role->powers_role($data);
		if($return === 1){
			$this->back('保存成功', 200);
		}else{
			$this->back($return);
		}
	}














}//类结束符