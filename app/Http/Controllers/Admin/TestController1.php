<?php
	namespace App\Http\Controllers\Admin;
	
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\DB;
	use Illuminate\Support\Facades\Redis;
	use App\Http\Controllers\Controller;
	
	class TestController extends Controller{
////////////////////////////////////////////////  测试
		/**
		 * sql测试
		 * http://www.xiong.com:86/admin/test/sql_test
		 */
		public function sql_test(){
			//// 客户表--clients
			//获取数据
			$sql = "select `Id`, `file_khbj`
					from clients";
			$list = json_decode(json_encode(db::select($sql)),true);
			
			foreach ($list as $k=>$v){
				if(empty($v['file_khbj'])){
					continue;
				}
				
				$url = parse_url($v['file_khbj']);
				$file_url = 'http://qiniu.xiaoxionga.cn' .$url['path'];
				
				$sql = "update clients
						set `file_khbj`='{$file_url}'
						where `Id` = {$v['Id']}";
//			var_dump($sql);exit;
				$update = db::update($sql);
//			var_dump($update);exit;
			}
			
			var_dump('更新成功');exit;
			
			//// 统一文件表--unifiedfiles
			//获取数据
			$sql = "select `Id`, `file_url`
					from unifiedfiles";
			$list = json_decode(json_encode(db::select($sql)),true);
			
			foreach ($list as $k=>$v){
				if(empty($v['file_url'])){
					continue;
				}
				
				$url = parse_url($v['file_url']);
				$file_url = 'http://qiniu.xiaoxionga.cn' .$url['path'];
				
				$sql = "update unifiedfiles
						set `file_url`='{$file_url}'
						where `Id` = {$v['Id']}";
//			var_dump($sql);exit;
				$update = db::update($sql);
//			var_dump($update);exit;
			}
			
			var_dump('更新成功');exit;
			
			//// 产品文件表--products_file
			//获取数据
			$sql = "select `Id`, `file_url`
					from products_file";
			$list = json_decode(json_encode(db::select($sql)),true);
			
			foreach ($list as $k=>$v){
				if(empty($v['file_url'])){
					continue;
				}
				
				$url = parse_url($v['file_url']);
				$file_url = 'http://qiniu.xiaoxionga.cn' .$url['path'];
				
				$sql = "update products_file
						set `file_url`='{$file_url}'
						where `Id` = {$v['Id']}";
				var_dump($sql);exit;
				$update = db::update($sql);
//			var_dump($update);exit;
			}
			
			var_dump('更新成功');exit;
			
			//// 产品表--products
			//获取数据
			$sql = "select `Id`, `img`
					from products";
			$list = json_decode(json_encode(db::select($sql)),true);
			
			foreach ($list as $k=>$v){
				if(empty($v['img'])){
					continue;
				}
				
				$url = parse_url($v['img']);
				$file_url = 'http://qiniu.xiaoxionga.cn' .$url['path'];
				
				$sql = "update products
						set `img`='{$file_url}'
						where `Id` = {$v['Id']}";
//			var_dump($sql);exit;
				$update = db::update($sql);
//			var_dump($update);exit;
			}
			
			var_dump('更新成功');exit;
			
			//// 下单计划表--place_order
			//获取数据
			$sql = "select `Id`, `img`
					from place_order";
			$list = json_decode(json_encode(db::select($sql)),true);
			
			foreach ($list as $k=>$v){
				if(empty($v['img'])){
					continue;
				}
				
				$url = parse_url($v['img']);
				$file_url = 'http://qiniu.xiaoxionga.cn' .$url['path'];
				
				$sql = "update place_order
						set `img`='{$file_url}'
						where `Id` = {$v['Id']}";
//			var_dump($sql);exit;
				$update = db::update($sql);
//			var_dump($update);exit;
			}
			
			var_dump('更新成功');exit;
			
			//// 订单文件表--orders_file
			//获取数据
			$sql = "select `Id`, `file_url`
					from orders_file";
			$list = json_decode(json_encode(db::select($sql)),true);
			
			foreach ($list as $k=>$v){
				if(empty($v['file_url'])){
					continue;
				}
				
				$url = parse_url($v['file_url']);
				$file_url = 'http://qiniu.xiaoxionga.cn' .$url['path'];
				
				$sql = "update orders_file
						set `file_url`='{$file_url}'
						where `Id` = {$v['Id']}";
//			var_dump($sql);exit;
				$update = db::update($sql);
//			var_dump($update);exit;
			}
			
			var_dump('更新成功');exit;
			
			//// 订单明细表--orders_detailed
			//获取数据
			$sql = "select `Id`, `img`
					from orders_detailed";
			$list = json_decode(json_encode(db::select($sql)),true);
			
			foreach ($list as $k=>$v){
				if(empty($v['img'])){
					continue;
				}
				
				$url = parse_url($v['img']);
				$file_url = 'http://qiniu.xiaoxionga.cn' .$url['path'];
				
				$sql = "update orders_detailed
						set `img`='{$file_url}'
						where `Id` = {$v['Id']}";
				$update = db::update($sql);
//			var_dump($update);exit;
			}
			
			var_dump('更新成功');exit;
			
			//// 操作指南表--manuals
			//获取数据
			$sql = "select `Id`, `file_url`
					from manuals";
			$list = json_decode(json_encode(db::select($sql)),true);
			
			foreach ($list as $k=>$v){
				if(empty($v['file_url'])){
					continue;
				}
				
				$url = parse_url($v['file_url']);
				$file_url = 'http://qiniu.xiaoxionga.cn' .$url['path'];
				
				$sql = "update manuals
						set `file_url`='{$file_url}'
						where `Id` = {$v['Id']}";
//			var_dump($sql);exit;
				$update = db::update($sql);
//			var_dump($update);exit;
			}
			
			var_dump('更新成功');exit;
			
			//// 供应商--factorys_auxiliary
			//获取数据
			$sql = "select `Id`, `file_kp`, `attestation`, `file_img`
					from factorys_auxiliary";
			$list = json_decode(json_encode(db::select($sql)),true);
			
			foreach ($list as $k=>$v){
				$set = '';
				if(!empty($v['file_kp'])){
					$url = parse_url($v['file_kp']);
					$file_kp = 'http://qiniu.xiaoxionga.cn' .$url['path'];
					$set .= ",`file_kp`='{$file_kp}'";
				}
				if(!empty($v['attestation'])){
					$url = parse_url($v['attestation']);
					$attestation = 'http://qiniu.xiaoxionga.cn' .$url['path'];
					$set .= ",`attestation`='{$attestation}'";
				}
				if(!empty($v['file_img'])){
					$url = parse_url($v['file_img']);
					$file_img = 'http://qiniu.xiaoxionga.cn' .$url['path'];
					$set .= ",`file_img`='{$file_img}'";
				}
				if(empty($set)){
					continue;
				}
				
				$set = substr($set, 1);
				$sql = "update factorys_auxiliary
						set {$set}
						where `Id` = {$v['Id']}";
				$update = db::update($sql);
//			var_dump($update);exit;
			}
			
			var_dump('更新成功');exit;
			
			//// 供应商--factorys
			//获取数据
			$sql = "select `Id`, `license`
					from factorys";
			$list = json_decode(json_encode(db::select($sql)),true);
			
			foreach ($list as $k=>$v){
				if(empty($v['license'])){
					continue;
				}
				
				$url = parse_url($v['license']);
				$file_url = 'http://qiniu.xiaoxionga.cn' .$url['path'];
				
				$sql = "update factorys
						set `license`='{$file_url}'
						where `Id` = {$v['Id']}";
				$update = db::update($sql);
//			var_dump($update);exit;
			}
			
			var_dump('更新成功');exit;
			
			//// 产品开发--exploits_file
			//获取数据
			$sql = "select `Id`, `file_url`
					from exploits_file";
			$list = json_decode(json_encode(db::select($sql)),true);
			
			foreach ($list as $k=>$v){
				if(empty($v['file_url'])){
					continue;
				}
				
				$url = parse_url($v['file_url']);
				$img = 'http://qiniu.xiaoxionga.cn' .$url['path'];
				
				$sql = "update exploits_file
						set `file_url`='{$img}'
						where `Id` = {$v['Id']}";
				$update = db::update($sql);
				var_dump($update);exit;
			}
			
			var_dump('更新成功');exit;
			
			//// 产品开发--exploits
			//获取数据
			$sql = "select `Id`, `img`
					from exploits";
			$list = json_decode(json_encode(db::select($sql)),true);
			
			foreach ($list as $k=>$v){
				if(empty($v['img'])){
					continue;
				}
				
				$url = parse_url($v['img']);
				$img = 'http://qiniu.xiaoxionga.cn' .$url['path'];
				
				$sql = "update exploits
						set `img`='{$img}'
						where `Id` = {$v['Id']}";
				$update = db::update($sql);
				var_dump($update);exit;
			}
			
			var_dump('更新成功');exit;
			
			
			//// 询盘
			//获取数据
			$sql = "select `Id`, `img`
					from enquirys";
			$list = json_decode(json_encode(db::select($sql)),true);
			
			foreach ($list as $k=>$v){
				if(empty($v['img'])){
					continue;
				}
				
				$url = parse_url($v['img']);
				$img = 'http://qiniu.xiaoxionga.cn' .$url['path'];
				
				$sql = "update enquirys
						set `img`='{$img}'
						where `Id` = {$v['Id']}";
				$update = db::update($sql);
//			var_dump($update);exit;
			}
			
			
			var_dump('更新成功');exit;
			
			
			
		}
		
		
		
		
		
		
		
		
		
	}//类结束符