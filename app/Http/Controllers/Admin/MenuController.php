<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MenuController extends Controller{
//////////////////////////////////////////////// 菜单
////////////////////// 菜单列表
    /**
     * 菜单列表--页面
     */
    public function menu_list_html(){
	    return view('admin/menu/menu_list');
    }
	
	/**
	 * 菜单列表
	 * @param name 菜单名称
	 * @param fid 父id
	 */
	public function menu_list(Request $request){
		$data = $request->all();
		if(empty($data)){
			$this->back('接收值为空');
		}
		
		$call_menu = new \App\Libs\wrapper\Menu();
		$return = $call_menu->menu_list($data);
		if(is_array($return)){
			$this->back('成功', '200', $return);
		}else{
			$this->back('获取数据失败');
		}
	}
	
	/**
	 * 新增菜单
	 * @param name 名称
	 * @param url 路径
	 * @param icon 图标
	 * @param fid 父id
	 * @param serial_number 序号
	 * @param identity 权限标识
	 */
	public function menu_add(Request $request){
		//验证参数
		$this->validate($request, [
			'name' => 'required| string',
		], [
			'name.required' => '请填写菜单名称',
			'name.string' => 'name必须是字符串',
		]);
		
		$data = $request->all();
		$call_menu = new \App\Libs\wrapper\Menu();
		$return = $call_menu->menu_add($data);
		if($return === 1){
			$this->back('新增成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 菜单信息
	 * @param menu_id 菜单id
	 */
	public function menu_info(Request $request){
		//验证参数
		$this->validate($request, [
			'menu_id' => 'required| integer',
		], [
			'menu_id.required' => 'menu_id为空',
			'menu_id.integer' => 'menu_id不是整型数据',
		]);
		
		$data = $request->all();
		$call_menu = new \App\Libs\wrapper\Menu();
		$return = $call_menu->menu_info($data);
		if(is_array($return)){
			$this->back('获取成功', '200', $return[0]);
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 编辑菜单
	 * @apram menu_id 菜单id
	 * @param name 菜单名称
	 * @param url 路径
	 * @param icon 图标
	 * @param fid 父id
	 * @param serial_number 序号
	 * @param identity 权限标识
	 */
	public function menu_update(Request $request){
		//验证参数
		$this->validate($request, [
			'menu_id' => 'required| integer',
			'name' => 'required| string'
		], [
			'menu_id.required' => 'powertype_id为空',
			'menu_id.integer' => 'powertype_id不是整型数据',
			'name.required' => '请填写菜单名称',
			'name.string' => 'name不是字符串',
		]);
		
		$data = $request->all();
		$call_menu = new \App\Libs\wrapper\Menu();
		$return = $call_menu->menu_update($data);
		if($return === 1){
			$this->back('编辑成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 删除菜单
	 * @param menu_id 菜单id
	 */
	public function menu_del(Request $request){
		//验证参数
		$this->validate($request, [
			'menu_id' => 'required| integer',
		], [
			'menu_id.required' => 'menu_id为空',
			'menu_id.integer' => 'menu_id必须是整型数据',
		]);
		
		$data = $request->all();
		$call_menu = new \App\Libs\wrapper\Menu();
		$return = $call_menu->menu_del($data);
		if($return === 1){
			$this->back('删除成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	
	
	
	/**
	 * 新增权限
	 * @param name 权限名称
	 * @param identity 权限标识
	 * @param type_id 权限分类id
	 * @param type 类型：1查询、2操作
	 */
	public function power_add(Request $request){
		//验证参数
		$this->validate($request, [
			'name' => 'required| string',
			'identity' => 'required| string',
			'type_id' => 'required| integer',
			'type' => 'required| integer',
		], [
			'name.required' => '请填写权限名称',
			'name.string' => 'name必须是字符串',
			'identity.required' => '请填写权限标识',
			'identity.string' => 'identity必须是字符串',
			'type_id.required' => 'type_id为空',
			'type_id.integer' => 'type_id必须是整型数据',
			'type.required' => '请选择权限类型',
			'type.integer' => 'type必须是整型数据',
		]);
		
		$data = $request->all();
		$call_powers = new \App\Libs\wrapper\Powers();
		$return = $call_powers->power_add($data);
		if($return === 1){
			$this->back('新增成功','200');
		}else{
			$this->back($return);
		}
	}
	
    
    
    /**
     * 编辑权限
     * @apram power_id 权限id
     * @param name 权限名称
     * @param identity 权限标识
     * @param type_id 权限分类id
     * @param type 类型：1查询、2操作
     */
    public function power_update(Request $request){
	    //验证参数
	    $this->validate($request, [
		    'power_id' => 'required| integer',
		    'name' => 'required| string',
		    'identity' => 'required| string',
		    'type' => 'required| integer'
	    ], [
		    'power_id.required' => 'power_id为空',
		    'power_id.integer' => 'power_id必须是整型数据',
		    'name.required' => '请填写权限名称',
		    'name.string' => 'name不是字符串',
		    'identity.required' => '请填写权限标识',
		    'identity.string' => 'identity必须是字符串',
		    'type.required' => '请选择权限类型',
		    'type.integer' => 'type必须是整型数据',
	    ]);
        $data = $request->all();
        if(empty($data)){
            $this->back('接收值为空');
        }
	    $call_powers = new \App\Libs\wrapper\Powers();
	    $return = $call_powers->power_update($data);
        if($return === 1){
            $this->back('编辑成功', '200');
        }else{
            $this->back($return);
        }
    }
	
	
















}//类结束符