<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PowersController extends Controller{
//////////////////////////////////////////////// 权限
////////////////////// 权限列表
    /**
     * 权限列表--页面
     */
    public function power_list_html(){
	    //获取权限分类
	    $sql = "select * from powers_type";
	    $powers_type = json_decode(json_encode(db::select($sql)),true);
        return view('admin/powers/powers_list', [
        	'powers_type' => $powers_type
        ]);
    }
	
	/**
	 * 编辑权限分类
	 * @param powertype_id 权限分类id
	 * @param name 权限分类名称
	 */
	public function powertype_update(Request $request){
		//验证参数
		$this->validate($request, [
			'powertype_id' => 'required| integer',
			'name' => 'required| string'
		], [
			'powertype_id.required' => 'powertype_id为空',
			'powertype_id.integer' => 'powertype_id不是整型数据',
			'name.required' => '请填写权限分类名称',
			'name.string' => 'name不是字符串',
		]);
		$data = $request->all();
		if(empty($data)){
			$this->back('接收值为空');
		}
		$call_powers = new \App\Libs\wrapper\Powers();
		$return = $call_powers->powertype_update($data);
		if($return === 1){
			$this->back('编辑成功', '200');
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 新增权限分类
	 * @param name 权限分类名称
	 */
	public function powertype_add(Request $request){
		//验证参数
		$this->validate($request, [
			'name' => 'required| string',
		], [
			'name.required' => '请填写权限分类名称',
			'name.string' => 'name必须是字符串',
		]);
		
		$data = $request->all();
		$call_powers = new \App\Libs\wrapper\Powers();
		$return = $call_powers->powertype_add($data);
		if(is_numeric($return)){
			$this->back('新增成功', '200', $return);
		}else{
			$this->back($return);
		}
	}
	
	/**
	 * 权限列表
	 * @param name 权限名称
	 * @param identity 权限标识
	 * @param type_id 权限分类id
	 * @param type 类型：1查询、2操作
	 */
	public function power_list(Request $request){
		$data = $request->all();
		if(empty($data)){
			$this->back('接收值为空');
		}
		
		$call_powers = new \App\Libs\wrapper\Powers();
		$return = $call_powers->power_list($data);
		if(is_array($return)){
			$this->back('成功', '200', $return);
		}else{
			$this->back('获取数据失败');
		}
	}
	
	/**
	 * 新增权限
	 * @param name 权限名称
	 * @param identity 权限标识
	 * @param type_id 权限分类id
	 * @param type 类型：1查询、2操作
	 */
	public function power_add(Request $request){
		//验证参数
		$this->validate($request, [
			'name' => 'required| string',
			'identity' => 'required| string',
			'type_id' => 'required| integer',
			'type' => 'required| integer',
		], [
			'name.required' => '请填写权限名称',
			'name.string' => 'name必须是字符串',
			'identity.required' => '请填写权限标识',
			'identity.string' => 'identity必须是字符串',
			'type_id.required' => 'type_id为空',
			'type_id.integer' => 'type_id必须是整型数据',
			'type.required' => '请选择权限类型',
			'type.integer' => 'type必须是整型数据',
		]);
		
		$data = $request->all();
		$call_powers = new \App\Libs\wrapper\Powers();
		$return = $call_powers->power_add($data);
		if($return === 1){
			$this->back('新增成功','200');
		}else{
			$this->back($return);
		}
	}
	
    /**
     * 权限信息
     * @param power_id 权限id
     */
    public function power_info(Request $request){
	    //验证参数
	    $this->validate($request, [
		    'power_id' => 'required| integer',
	    ], [
		    'power_id.required' => 'power_id为空',
		    'power_id.integer' => 'power_id不是整型数据',
	    ]);
    	
        $data = $request->all();
        $call_powers = new \App\Libs\wrapper\Powers();
        $return = $call_powers->power_info($data);
        if(is_array($return)){
            $this->back('获取成功', '200', $return[0]);
        }else{
            $this->back($return);
        }
    }
    
    /**
     * 编辑权限
     * @apram power_id 权限id
     * @param name 权限名称
     * @param identity 权限标识
     * @param type_id 权限分类id
     * @param type 类型：1查询、2操作
     */
    public function power_update(Request $request){
	    //验证参数
	    $this->validate($request, [
		    'power_id' => 'required| integer',
		    'name' => 'required| string',
		    'identity' => 'required| string',
		    'type' => 'required| integer'
	    ], [
		    'power_id.required' => 'power_id为空',
		    'power_id.integer' => 'power_id必须是整型数据',
		    'name.required' => '请填写权限名称',
		    'name.string' => 'name不是字符串',
		    'identity.required' => '请填写权限标识',
		    'identity.string' => 'identity必须是字符串',
		    'type.required' => '请选择权限类型',
		    'type.integer' => 'type必须是整型数据',
	    ]);
        $data = $request->all();
        if(empty($data)){
            $this->back('接收值为空');
        }
	    $call_powers = new \App\Libs\wrapper\Powers();
	    $return = $call_powers->power_update($data);
        if($return === 1){
            $this->back('编辑成功', '200');
        }else{
            $this->back($return);
        }
    }
	
	/**
	 * 删除权限
	 * @param power_id 权限id
	 */
	public function power_del(Request $request){
		//验证参数
		$this->validate($request, [
			'power_id' => 'required| integer',
		], [
			'power_id.required' => 'power_id为空',
			'power_id.integer' => 'power_id必须是整型数据',
		]);
		
		$data = $request->all();
		$call_powers = new \App\Libs\wrapper\Powers();
		$return = $call_powers->power_del($data);
		if($return === 1){
			$this->back('删除成功', '200');
		}else{
			$this->back($return);
		}
	}
















}//类结束符