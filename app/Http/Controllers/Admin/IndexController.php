<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller{
////////////////////////////////////////////////  首页
    /**
     * 首页--页面
     */
    public function index_html(){
        $admininfo = session('admininfo');
        //获取用户信息
        $sql = "select *
                    from admins
                    where `id`={$admininfo['id']}";
        $admin = json_decode(json_encode(db::select($sql)),true);
        return view('admin/index/index',[
            'admin'=>$admin[0]
        ]);
    }
	
    /**
     * 默认首页--页面
     */
    public function default_index_html(){
	    return view('admin/index/default_index');
    }
	
	/**
	 * 验证登录状态
	 */
	public function islogin(){
		$this->back('调用成功','200');
	}
    
	
    
    
 //////////////////////////////// 上面有用
    
    
    
 
	
	
	
    /**
     * @desc:获取用户消息记录
     * @return array
     */
    public function sys_msg(){
        $UserID = session("userinfo.UserID");
        $UserName = session("userinfo.UserName");

        $User = new User();
        $select = $User->sys_msg($UserName,$UserID);
        //exit($this->back('成功',0001,$select));
        if($select){
            $result['num'] = count($select);
            foreach($select as $k=>$v){
                $time = $this->time_tran($v['AddTime']);
                $result['list'][$k]['MsgID'] = $v['MsgID'];
                $result['list'][$k]['AddTime'] = $time;
                $result['list'][$k]['SendID'] = $v['SendID'];
                $result['list'][$k]['MsgTitle'] = $v['Title'];
                $result['list'][$k]['relationID'] = $v['relationID'];
                $result['list'][$k]['NickName'] = $v['NickName'];
                $check = substr($v['ImageUrl'],0,4);
                if($check != "http"){
                    $v['ImageUrl'] = "http://".$_SERVER['HTTP_HOST']."/Uploads/".$v['ImageUrl'];
                }
                $result['list'][$k]['ImageUrl'] = $v['ImageUrl'];
                $result['list'][$k]['MsgType'] = $v['MsgType'];
                $result['list'][$k]['MsgUrl'] = $v['MsgUrl'];
                $result['list'][$k]['Groupid'] = $v['Groupid'];
            }
        }else{
            $result['num'] = 0;
        }
        exit($this->back('成功','0001',$result));
    }


    /**
     * @desc 消息状态修改
     * @param sn(消息ID)
     * @return array
     */
    public function set_msg_state(Request $request){
        $MsgID = $request->input("sn");
        $sql = "update sys_msg set IsRead = 1 where MsgID = {$MsgID}";
        $update = DB::update($sql);
        if($update){
            exit($this->back('成功','0001'));
        }else{
            exit($this->back('失败','0000'));
        }
    }


    /**
     * @desc 获取企业资产闲置/在用统计数据
     */
    public function get_property_report(Request $request){
        $userinfo = session('userinfo');
        $allocation = new Index();
        $result = $allocation->get_property_report($userinfo['companyID']);
        if($result){
            exit($this->back('成功','0001',$result));
        }else{
            exit($this->back('失败','0000'));
        }
    }


    /**
     * @desc 获取首页列表数据
     */
    public function get_property_oprate_list(){
        $userinfo = session('userinfo');
        $allocation = new Index();
        $num = 15;
        $result = $allocation->get_property_oprate_list($userinfo['companyID'],$num);

        if($result){
            exit($this->back('成功','0001',$result));
        }else{
            exit($this->back('失败','0000'));
        }
    }




    /**
     * @desc 获取个人领用/借用统计
     */
    public function get_user_property_count(){
        $userinfo = session('userinfo');
        $allocation = new Index();
        $result = $allocation->get_user_property_count($userinfo['UserID']);
        if($result){
            exit($this->back('成功','0001',$result));
        }else{
            exit($this->back('失败','0000'));
        }
    }


    /**
     * @desc 获取部门负责人首页统计
     */
    public function get_dep_report_data(){
        $userinfo = session('userinfo');
        $allocation = new Index();
        $result['personal'] = $allocation->get_user_property_count($userinfo['UserID']);
        $result['depart'] = $allocation->get_deps_property_state($userinfo['departID']);
        $result['deptype'] = $allocation->get_deps_property_type($userinfo['departID']);
        if($result){
            exit($this->back('成功','0001',$result));
        }else{
            exit($this->back('失败','0000'));
        }
    }


    /**
     * @desc 获取在用资产列表
     */
    public function get_inuse_property_list(){
        $userinfo = session('userinfo');
        $index = new Index();
        if($userinfo['groupid'] == 5){
            $result['zyzc'] = $index->get_inuse_property_list($userinfo['departID'],1);
        }else{
            $result['zyzc'] = $index->get_inuse_property_list($userinfo['UserID'],2);
        }
        $result['inv_list'] = $index->get_user_inventory($userinfo['UserID']);
        exit($this->back('成功','0001',$result));
    }


    /**
     * @desc 获取用户盘点单
     */
    public function get_user_inventory(){
        $userinfo = session('userinfo');
        $index = new Index();
        $result = $index->get_user_inventory($userinfo['UserID']);
        if($result){
            exit($this->back('成功','0001',$result));
        }else{
            exit($this->back('失败','0000'));
        }
    }

/**
     * @desc 获取企业列表
     * @param comname
     * @return list
     */
    public function get_company_list(Request $request){
        $data = $request->all();
        if(empty($data['comname'])){
            exit($this->back('请输入企业名称查询','0000'));
        }
        $index = new Index();
        $list = $index->get_company_list($data['comname']);
        if($list){
            exit($this->back('成功','0001',$list));
        }else{
            exit($this->back('失败','0000'));
        }
    }


    /**
     * @desc 申请加入企业
     * @param comid
     */
    public function join_com_apply(Request $request){
        $data = $request->all();
        $index = new Index();
        $userinfo = session("userinfo");
        $result = $index->join_com_apply($userinfo,$data);
        switch ($result){
            case 1:
                exit($this->back('申请成功，等待审核','0001'));
                break;
            case 2:
                exit($this->back('您已加入企业','0002'));
                break;
            case 3:
                exit($this->back('用户已被禁用','0003'));
                break;
            case 4:
                exit($this->back('已有申请加入企业待审核','0004'));
                break;
            case 5:
                exit($this->back('已有认证企业待审核','0005'));
                break;
            case 6:
                exit($this->back('加入失败','0006'));
                break;
            case 7:
                exit($this->back('消息发送失败','0007'));
                break;
        }
    }


    /**
     * @desc 提交企业认证
     * @param company_name
     * @param telnum
     * @param address
     * @param industry
     * @param company_no
     * @param license_img
     * @return int
     */
    public function add_new_company(Request $request){
        $data = $request->all();
        $userinfo = session("userinfo");
        $index = new Index();
        $res = $index->add_new_company($userinfo,$data,$_FILES);
        switch ($res){
            case 1:
                exit($this->back('申请成功，等待审核','0001'));
                break;
            case 2:
                exit($this->back('您已加入企业','0002'));
                break;
            case 3:
                exit($this->back('您的账号已被禁用','0003'));
                break;
            case 4:
                exit($this->back('您已提交申请加入企业待审核，无法提交企业认证','0004'));
                break;
            case 5:
                exit($this->back('您已提交认证企业待审核，请勿重复操作','0005'));
                break;
            case 7:
                exit($this->back('提交失败','0007'));
                break;
        }
    }


}//类结束符