<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class LoginController extends Controller{
////////////////////////////////////////////////  登录
    /**
     * @desc 后台登录--页面
     */
    public function login_html(){
        return view('admin/login/login');
    }
	
    /**
     * 后台登录
     * @param phone 账号
     * @param password 密码
     */
    public function login(Request $request){
    	//验证参数
	    $this->validate($request, [
		    'phone' => 'bail| required| string',
		    'password' => 'bail| required| string',
	    ], [
		    'phone.required' => '请输入账号',
		    'phone.string' => 'phone不是字符串',
		    'password.required' => '请输入密码',
		    'password.string' => 'password不是字符串',
	    ]);
        $data = $request->all();
        
        $call_login = new  \App\Libs\wrapper\Login();
        $return = $call_login->login($data);
        if(!empty($return['id'])){
            //如果写入session的程序中执行了die();exit();会导致session无法保存成功！换成return解决
            session(['admininfo' => $return]);
            return [
                'desc' => '登录成功',
                'code' => '200'
            ];
        }else{
            $this->back($return);
        }
    }

    /**
     * @desc 退出登录
     */
    public function quit(){
        session()->flush();
//        session(['session_identity_time' => null]);
        return [
            'desc' => '登录成功',
            'code' => '200'
        ];
    }


////////////////////////////////////////////////  测试
	/**
	 * 同步OA订单信息
	 */
	public function order_update(){
//		http://www.xiong.com:86/admin/login/order_update
		
		$host = '192.168.10.200';
		$database = 'zity'; //你的mssql库名
		$username = 'sa'; //你的mssql用户名
		$password = 'zity'; //你的mssql密码
		//连接数据库
		$conn = new \PDO("odbc:Driver={SQL Server};Server=$host;Database=$database;",$username,$password);
		
		$sql = "update OrdersInfo
					set is_synchro = 1
					where is_synchro = 2";
		$update = $conn->exec($sql);
	
		var_dump($update);exit;
		
		$time = date('Y-m-d H:i:s', time());
		
		
		$time = date('Y-m-d H:i:s', time());
		
		//// 同步订单时间节点信息
		//获取订单信息
		$sql = "select o.*, who.*
					from orders o
					left join wm_has_order who on who.order_id = o.id
					order by o.id desc
					limit 1600,400";
		$list = json_decode(json_encode(db::select($sql)),true);
		
		foreach ($list as $k=>$v){
			//获取订单明细
			$sql = "select `Id`
						from orders_detailed
						where order_no='{$v['order_number']}' and style_no='{$v['style_number']}' and `number`={$v['quantity']}";
			$detailed = json_decode(json_encode(db::select($sql)),true);
			
			if(!empty($detailed)){
				////同步节点
				$set_n = "";
				if(!empty($v['factory_delivertime'])){
					$set_n .= ",goods_time='{$v['factory_delivertime']}'";
				}
				if(!empty($v['head_mail'])){
					$set_n .= ",head_mail_sj='{$v['head_mail']}'";
				}
				if(!empty($v['head_time'])){
					$set_n .= ",head_time_sj='{$v['head_time']}'";
				}
				if(!empty($v['contract'])){
					$set_n .= ",contract_sj='{$v['contract']}'";
				}
				if(!empty($v['color_card_mail'])){
					$set_n .= ",card_mail_sj='{$v['color_card_mail']}'";
				}
				if(!empty($v['color_card_opinion'])){
					$set_n .= ",card_opinion_sj='{$v['color_card_opinion']}'";
				}
				if(!empty($v['fabric_to_warehouse'])){
					$set_n .= ",fabric_warehouse_sj='{$v['fabric_to_warehouse']}'";
				}
				if(!empty($v['product_sample'])){
					$set_n .= ",product_sample_sj='{$v['product_sample']}'";
				}
				if(!empty($v['product_sample_opinion'])){
					$set_n .= ",product_sample_opinion_sj='{$v['product_sample_opinion']}'";
				}
				if(!empty($v['cut_off'])){
					$set_n .= ",cut_off_sj='{$v['cut_off']}'";
				}
				if(!empty($v['go_online'])){
					$set_n .= ",go_online_sj='{$v['go_online']}'";
				}
				if(!empty($v['finishing'])){
					$set_n .= ",finishing_sj='{$v['finishing']}'";
				}
				if(!empty($v['inspection'])){
					$set_n .= ",inspection_sj='{$v['inspection']}'";
				}
				if(!empty($v['shipment'])){
					$set_n .= ",shipment_sj='{$v['shipment']}'";
				}
				if(!empty($v['remark'])){
					$set_n .= ",remark_gd='{$v['remark']}'";
				}
				
				$node_update = 1;
				if(!empty($set_n)){
					$set_n = substr($set_n, 1);
					$sql = "update orders_detailed_node
							set {$set_n}
							where `detailed_id` = {$detailed[0]['Id']}";
					$node_update = db::update($sql);
					if(!$node_update){
						$node_update = 0;
					}
				}
				
				if($node_update){
					var_dump('1');
				}else{
					var_dump('0');
				}
				
			}
		}
		
		exit('完成');
		
		
		
		
		//// 同步行程
		//获取订单信息
		$sql = "select *
					from orders o
					inner join qc_has_order qho on qho.order_id = o.id";
		$list = json_decode(json_encode(db::select($sql)),true);
		
		foreach ($list as $k=>$v){
			//获取订单明细
			$sql = "select `Id`,`user_yw`
						from orders_detailed
						where order_no='{$v['order_number']}' and style_no='{$v['style_number']}' and `number`={$v['quantity']}";
			$detailed = json_decode(json_encode(db::select($sql)),true);
			
			if(!empty($detailed)){
				$field = "create_time,detailed_id,founder";
				$insert = "now(),{$detailed[0]['Id']},{$detailed[0]['user_yw']}";
				
				if(!empty($v['inspection_date'])){
					$field .= ",inspection_time";
					$insert .= ",'{$v['inspection_date']}'";
				}
				if(!empty($v['inspection_link'])){
					$field .= ",inspection_link";
					$insert .= ",'{$v['inspection_link']}'";
				}
				if(!empty($v['go_online'])){
					$field .= ",online_time";
					$insert .= ",'{$v['go_online']}'";
				}
				if(!empty($v['factory_delivertime'])){
					$field .= ",factory_time";
					$insert .= ",'{$v['factory_delivertime']}'";
				}
				if(!empty($v['address'])){
					$field .= ",address";
					$insert .= ",'{$v['address']}'";
				}
				
				if(!empty($v['job_content'])){
					$field .= ",content_job";
					$insert .= ",'{$v['job_content']}'";
				}
				if(!empty($v['qc_remark'])){
					$field .= ",remark_qc";
					$insert .= ",'{$v['qc_remark']}'";
				}
				if(!empty($v['product_sample'])){
					$field .= ",remark_qc";
					$insert .= ",'{$v['qc_remark']}'";
				}
				
				if(isset($v['status'])){
					$field .= ",state";
					if($v['status']=='0'){
						$insert .= ",1";
					}else if($v['status']=='1'){
						$insert .= ",1";
					}else if($v['status']=='2'){
						$insert .= ",1";
					}else if($v['status']=='3'){
						$insert .= ",2";
					}
				}
				
				$sql = "insert into orders_trip({$field}) values ({$insert})";
				$add = db::insert($sql);
				
			}
		}
		
		exit('完成');
		
		
		//// 同步订单文件
		//获取订单信息
		$sql = "select *
					from orders o
					left join qc_has_order qho on qho.order_id = o.id
					order by o.id asc
					limit 1600,400";
		$list = json_decode(json_encode(db::select($sql)),true);
		
		foreach ($list as $k=>$v){
			//获取订单明细
			$sql = "select `Id`
						from orders_detailed
						where order_no='{$v['order_number']}' and style_no='{$v['style_number']}' and `number`={$v['quantity']}";
			$detailed = json_decode(json_encode(db::select($sql)),true);
			
			if(!empty($detailed)){
				////同步文件
				if((!empty($v['inspection_report_qq'])) or (!empty($v['inspection_report_zq'])) or (!empty($v['inspection_report_wq']))){
					$insert = '';
					if(!empty($v['inspection_report_qq'])){
						$insert .= ",({$detailed[0]['Id']}, '{$v['inspection_report_qq']}', 1)";
					}
					if(!empty($v['inspection_report_zq'])){
						$insert .= ",({$detailed[0]['Id']}, '{$v['inspection_report_zq']}', 2)";
					}
					if(!empty($v['inspection_report_wq'])){
						$insert .= ",({$detailed[0]['Id']}, '{$v['inspection_report_wq']}', 3)";
					}
					if(!empty($insert)){
						$insert = substr($insert, 1);
						$sql = "insert into orders_file(`detailed_id`, `file_url`, `type`) values {$insert}";
						$add = db::insert($sql);
					}
				}
				
				
			}
		}
		
		exit('完成');
		
		
		//// 同步订单明细信息
		//获取订单信息
		$sql = "select o.*, who.*, yw.Id as user_yw, gd.Id as user_gd, qc.Id as user_qc, f.Id as factory_id
					from orders o
					left join wm_has_order who on who.order_id = o.id
					left join users yw on yw.name = o.user_name
					left join users gd on gd.name = o.share_name
					left join users qc on qc.name = o.qc_name
					left join factorys f on f.name = o.factory
					order by o.id asc
					limit 1600,400";
		$list = json_decode(json_encode(db::select($sql)),true);
//		var_dump($list);exit;
		foreach ($list as $k=>$v){
			//获取订单明细
			$sql = "select `Id`
						from orders_detailed
						where order_no='{$v['order_number']}' and style_no='{$v['style_number']}' and `number`={$v['quantity']}";
			$detailed = json_decode(json_encode(db::select($sql)),true);
			
			if(!empty($detailed)){
				////同步订单明细
				$set = "user_yw={$v['user_yw']}";
				if(!empty($v['user_gd'])){
					$set .= ", user_gd={$v['user_gd']}";
				}
				if(!empty($v['user_qc'])){
					$set .= ", user_qc={$v['user_qc']}";
				}
				if(!empty($v['img'])){
					$set .= ", `img`='{$v['img']}'";
				}
				if(!empty($v['factory_id'])){
					$set .= ", factory_id={$v['factory_id']}";
				}
				//归单状态
				if(isset($v['state'])){
					if($v['state'] == '0'){
						$set .= ", file_state=1";
					}else if($v['state'] == '1'){
						$set .= ", file_state=2";
					}
				}
				//投诉状态
				if(isset($v['is_complain'])){
					if($v['is_complain'] == '0'){
						$set .= ", is_complain=1";
					}else if($v['is_complain'] == '1'){
						$set .= ", is_complain=2";
					}
				}
				$sql = "update orders_detailed
							set {$set}
							where `Id` = {$detailed[0]['Id']}";
				$detailed_update = db::update($sql);
				
				////同步节点
				$set_n = "";
				if(!empty($v['factory_delivertime'])){
					$set_n = ",goods_time='{$v['factory_delivertime']}'";
				}
				if(!empty($v['head_mail'])){
					$set_n = ",head_mail_sj='{$v['head_mail']}'";
				}
				if(!empty($v['head_time'])){
					$set_n = ",head_time_sj='{$v['head_time']}'";
				}
				if(!empty($v['contract'])){
					$set_n = ",contract_sj='{$v['contract']}'";
				}
				if(!empty($v['color_card_mail'])){
					$set_n = ",card_mail_sj='{$v['color_card_mail']}'";
				}
				if(!empty($v['color_card_opinion'])){
					$set_n = ",card_opinion_sj='{$v['color_card_opinion']}'";
				}
				if(!empty($v['fabric_to_warehouse'])){
					$set_n = ",fabric_warehouse_sj='{$v['fabric_to_warehouse']}'";
				}
				if(!empty($v['product_sample'])){
					$set_n = ",product_sample_sj='{$v['product_sample']}'";
				}
				if(!empty($v['product_sample_opinion'])){
					$set_n = ",product_sample_opinion_sj='{$v['product_sample_opinion']}'";
				}
				if(!empty($v['cut_off'])){
					$set_n = ",cut_off_sj='{$v['cut_off']}'";
				}
				if(!empty($v['go_online'])){
					$set_n = ",go_online_sj='{$v['go_online']}'";
				}
				if(!empty($v['finishing'])){
					$set_n = ",finishing_sj='{$v['finishing']}'";
				}
				if(!empty($v['inspection'])){
					$set_n = ",inspection_sj='{$v['inspection']}'";
				}
				if(!empty($v['shipment'])){
					$set_n = ",shipment_sj='{$v['shipment']}'";
				}
				if(!empty($v['remark'])){
					$set_n = ",remark_gd='{$v['remark']}'";
				}
				
				$node_update = 1;
				if(!empty($set_n)){
					$set_n = substr($set_n, 1);
					$sql = "update orders_detailed_node
							set {$set_n}
							where `detailed_id` = {$detailed[0]['Id']}";
					$node_update = db::update($sql);
					if(!$node_update){
						$node_update = 0;
					}
				}
				
				if($detailed_update && $node_update){
					var_dump('1');
				}else{
					var_dump('0');
				}
				
			}
		}
		
		exit('完成');
		
		
		
		//// 同步工厂
		//获取订单信息
		$sql = "select *
					from orders o
					left join wm_has_order who on who.order_id = o.id
					order by o.id asc
					limit 0,400";
		$list = json_decode(json_encode(db::select($sql)),true);
		
		foreach ($list as $k=>$v){
			//获取订单明细
			$sql = "select `Id`
						from orders_detailed
						where order_no='{$v['order_number']}' and style_no='{$v['style_number']}' and `number`={$v['quantity']}";
			$detailed = json_decode(json_encode(db::select($sql)),true);
			
			if(!empty($detailed)){
				////新增工厂
				if(!empty($v['factory'])){
					//查询工厂
					$sql = "select `Id` from factorys where `name` = '{$v['factory']}'";
					$find = json_decode(json_encode(db::select($sql)),true);
					if(empty($find)){
						//新增工厂
						$arr = array();
						$arr['name'] = $v['factory'];
						$arr['create_time'] = $time;
						$factory_add = db::table("factorys")->insertGetId($arr);
					}
				}
				
				
			}
		}
		
		exit('完成');
		
		
		
		//// 同步用户
		//获取订单信息
		$sql = "select *
					from orders o
					left join wm_has_order who on who.order_id = o.id
					order by o.id asc
					limit 0,400";
		$list = json_decode(json_encode(db::select($sql)),true);
		
		foreach ($list as $k=>$v){
			//获取订单明细
			$sql = "select `Id`
						from orders_detailed
						where order_no='{$v['order_number']}' and style_no='{$v['style_number']}' and `number`={$v['quantity']}";
			$detailed = json_decode(json_encode(db::select($sql)),true);
			
			if(!empty($detailed)){
				////新增用户
				//新增业务员
				if(!empty($v['user_name'])){
					//查询用户
					$sql = "select `Id` from users where `name` = '{$v['user_name']}'";
					$user_find = json_decode(json_encode(db::select($sql)),true);
					if(empty($user_find)){
						//新增用户
						$arr = array();
						$arr['name'] = $v['user_name'];
						$arr['password'] = md5('123456');
						$arr['heard_img'] = '/admin/img/hearimg.jpg';
						$arr['type'] = 2;
						$arr['create_time'] = $time;
						$user_add = db::table("users")->insertGetId($arr);
					}
				}
				
				//新增跟单员
				if(!empty($v['share_name'])){
					//查询用户
					$sql = "select `Id` from users where `name` = '{$v['share_name']}'";
					$user_find = json_decode(json_encode(db::select($sql)),true);
					if(empty($user_find)){
						//新增用户
						$arr = array();
						$arr['name'] = $v['share_name'];
						$arr['password'] = md5('123456');
						$arr['heard_img'] = '/admin/img/hearimg.jpg';
						$arr['type'] = 3;
						$arr['create_time'] = $time;
						$user_add = db::table("users")->insertGetId($arr);
					}
				}
				
				//新增QC人员
				if(!empty($v['qc_name'])){
					//查询用户
					$sql = "select `Id` from users where `name` = '{$v['qc_name']}'";
					$user_find = json_decode(json_encode(db::select($sql)),true);
					if(empty($user_find)){
						//新增用户
						$arr = array();
						$arr['name'] = $v['qc_name'];
						$arr['password'] = md5('123456');
						$arr['heard_img'] = '/admin/img/hearimg.jpg';
						$arr['type'] = 4;
						$arr['create_time'] = $time;
						$user_add = db::table("users")->insertGetId($arr);
					}
				}
				
			}
			
		}
		
		exit('完成');
		
	
		
	}
	
	
	/**
	 * 同步OA订单信息
	 * http://xiong.com:86/admin/login/synchro
	 */
	public function synchro(){
	    header("content-Type: text/html; charset=gb2312");
		//连接数据库
		/*$conn = new \PDO(
			"odbc:Driver={SQL Server};Server=$host;Database=$database;port=9191",
			$username,
			$password
		);*/
		$host = '27.154.56.154';//192.168.10.200(内网)、27.154.56.154（外网）
		$database = 'zity'; //你的mssql库名
		$username = 'sa'; //你的mssql用户名
		$password = 'zity'; //你的mssql密码
		//连接数据库
		$conn = new \PDO("odbc:Driver={SQL Server};Server=$host;Database=$database;",$username,$password);
		
		$sql = "select top 20 o.orderId, o.ordNO, o.userid, o.joinTime, u.Email,
					(select count(*) from orderItem where orderNO=o.ordNO) as item_num
					from OrdersInfo o
					left join userinf u on u.name = o.userid
					where o.is_synchro = 1 and joinTime >= '2018-06-01'";
		$list = $conn->query($sql);
		$order_list = array();
		$time = date('Y-m-d H:i:s', time());
		foreach ($list as $va) {
			//转码
			$va = eval('return '.iconv("gbk","utf-8",var_export($va,true).';'));
			if(!empty($va['userid'])){
				$order_list[] = $va;
			}
		}
		
		foreach($order_list as $v){
			if($v['item_num'] > 0 && (!empty($v['userid']))){
				////新增用户
				if(!empty($v['userid'])){
					//查询用户
					$sql = "select `Id` from users where `name` = '{$v['userid']}'";
					$user_find = json_decode(json_encode(db::select($sql)),true);
					
					if(empty($user_find)){
						//新增用户
						$arr = array();
						$arr['name'] = $v['userid'];
						$arr['password'] = md5('123456');
						$arr['email'] = $v['Email'];
						$arr['heard_img'] = '/admin/img/hearimg.jpg';
						$arr['type'] = 2;
						$arr['create_time'] = $time;
						$user_add = db::table("users")->insertGetId($arr);
						if($user_add){
							$user_id = $user_add;
						}else{
							continue;
						}
					}else{
						$user_id = $user_find[0]['Id'];
					}
				}else{
					continue;
				}
				
				//查询订单
				$sql = "select * from orders where order_no = '{$v['ordNO']}'";
				$order_find = json_decode(json_encode(db::select($sql)),true);
				if(empty($order_find)){
					//新增订单
					$arr = array();
					$arr['order_no'] = $v['ordNO'];
					$arr['user_add'] = $user_id;
					$arr['create_time'] = $v['joinTime'];
					$order_add = db::table("orders")->insertGetId($arr);
					
					if($order_add){
						$order_id = $order_add;
					}else{
						continue;
					}
				}else{
					$order_id = $order_find[0]['Id'];
				}
				
				//订单明细--OA平台数据
				$sql = "select * from orderItem where orderNO='{$v['ordNO']}'";
				$detailed = $conn->query($sql);
				if($detailed){
					foreach ($detailed as $v1) {
						//转码
						$v1 = eval('return '.iconv("gbk","utf-8",var_export($v1,true).';'));
						////新增工厂
						if(!empty($v1['xdgc'])){
							//查询工厂
							$sql = "select `Id` from factorys where `name` = '{$v1['xdgc']}'";
							$factory_find = json_decode(json_encode(db::select($sql)),true);
							if(empty($factory_find)){
								/*//新增工厂
								$arr = array();
								$arr['name'] = $v1['xdgc'];
								$arr['create_time'] = $time;
								$factory_add = db::table("factorys")->insertGetId($arr);
								if($factory_add){
									$factory_id = $factory_add;
								}else{
									continue;
								}*/
								$factory_id = '';
							}else{
								$factory_id = $factory_find[0]['Id'];
							}
						}else{
							$factory_id = '';
						}
						
						////新增订单明细
						$v1['kh'] = str_replace("'","", $v1['kh']);
						//新增订单明细
						$arr = array();
						$arr['order_id'] = $order_id;//订单id
						$arr['order_no'] = $v1['orderNO'];//订单号
						$arr['goods_name'] = $v1['pm'];//商品名称
						$arr['style_no'] = $v1['kh'];//款号
						$arr['user_yw'] = $user_id;//业务员
						$arr['number'] = $v1['sl'];//订单数量
						$arr['unit'] = $v1['dw'];//数量单位
						$arr['fabric'] = $v1['mlcf'];//面料成分
						$arr['factory_id'] = $factory_id;//下单工厂id
						$arr['price_gc'] = $v1['bj'];//工厂单价
						$arr['price_sj'] = $v1['sjdj'];//实际单价
						$arr['money_pi'] = $v1['pije'];//pi金额
						$arr['is_tax'] = $v1['sfhs'];//是否含税：0含税 1不含税
						$arr['customs_unit'] = $v1['bgdw'];//报关单位
						$arr['delivery_time'] = $v1['yjchrq'];//预计出货日期
						$arr['nature'] = $v1['ddxz'];//订单性质：0卖单 1采购单
						$arr['create_time'] = $v['joinTime'];
						$detailed_add = db::table("orders_detailed")->insertGetId($arr);
						if($detailed_add){
							$detailed_id = $detailed_add;
						}else{
							continue;
						}
						
						////新增订单明细节点
						$arr = array();
						$arr['detailed_id'] = $detailed_id;//订单明细id
						$arr['head_mail_yq'] = date('Y/m/d', strtotime($v1['yjchrq']) - 60 * 60 * 24 * 105);//头样寄出--预期
						$arr['head_time_yq'] = date('Y/m/d', strtotime($v1['yjchrq']) - 60 * 60 * 24 * 98);//头样意见--预期
						$arr['contract_yq'] = date('Y/m/d', strtotime($v1['yjchrq']) - 60 * 60 * 24 * 96);//下合同--预期
						$arr['card_mail_yq'] = date('Y/m/d', strtotime($v1['yjchrq']) - 60 * 60 * 24 * 93);//色卡寄出--预期
						$arr['card_opinion_yq'] = date('Y/m/d', strtotime($v1['yjchrq']) - 60 * 60 * 24 * 86);//色卡意见--预期
						$arr['fabric_warehouse_yq'] = date('Y/m/d', strtotime($v1['yjchrq']) - 60 * 60 * 24 * 61);//面料到仓--预期
						$arr['product_sample_yq'] = date('Y/m/d', strtotime($v1['yjchrq']) - 60 * 60 * 24 * 56);//产前样--预期
						$arr['product_sample_opinion_yq'] = date('Y/m/d', strtotime($v1['yjchrq']) - 60 * 60 * 24 * 48);//产前样意见--预期
						$arr['cut_off_yq'] = date('Y/m/d', strtotime($v1['yjchrq']) - 60 * 60 * 24 * 45);//开裁--预期
						$arr['go_online_yq'] = date('Y/m/d', strtotime($v1['yjchrq']) - 60 * 60 * 24 * 40);//上线--预期
						$arr['finishing_yq'] = date('Y/m/d', strtotime($v1['yjchrq']) - 60 * 60 * 24 * 2);//后整--预期
						$arr['inspection_yq'] = date('Y/m/d', strtotime($v1['yjchrq']) - 60 * 60 * 24 * 1);//验货--预期
						$arr['shipment_yq'] = date('Y/m/d', strtotime($v1['yjchrq']) - 60 * 60 * 24 * 1);//出货--预期
						$node_add = db::table("orders_detailed_node")->insertGetId($arr);
//				        var_dump('新增订单明细节点：', $detailed_id);
						if(!$node_add){
							continue;
						}
					}
				}
			}
			
			////修改OA  订单同步状态
			$sql = "update OrdersInfo
							set is_synchro = 2
							where orderId = '{$v['orderId']}'";
			$update = $conn->exec($sql);
		}
		
		var_dump('完成');
		
	}
	
	
	
	
	
	
	
	
	
	
}//类结束符