<?php

namespace App\Http\Controllers\Jobs;

use App\Http\Controllers\Controller;
use App\Jobs\InventoryDayJob;

class InventoryDayJobController extends Controller
{
    public static function runJob($data)
    {
        $job = new InventoryDayJob($data);
        $job->dispatch($data)->onQueue('inventory_day');
    }
}