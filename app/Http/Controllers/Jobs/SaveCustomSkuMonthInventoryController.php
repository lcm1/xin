<?php

namespace App\Http\Controllers\Jobs;
use App\Jobs\SaveCustomSkuMonthInventoryJob;
class SaveCustomSkuMonthInventoryController
{
    public static function runJob($data)
    {
//        var_dump($data);exit();
        $job = new SaveCustomSkuMonthInventoryJob($data);
        $job->dispatch($data)->onQueue('save_custom_sku_inventory');
    }
}