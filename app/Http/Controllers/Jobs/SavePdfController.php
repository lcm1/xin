<?php
namespace App\Http\Controllers\Jobs;


use App\Jobs\SavePdfJob;
use App\Http\Controllers\Controller;

class SavePdfController extends Controller
{

    //新品导入队列类
    public static function runJob($data)
    {
        $job = new SavePdfJob($data);
        $job ->dispatch($data)->onQueue('save_pdf');
    }
}
