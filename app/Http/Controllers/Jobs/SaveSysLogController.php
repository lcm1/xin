<?php

namespace App\Http\Controllers\Jobs;

use App\Http\Controllers\Controller;
use App\Jobs\SaveSysLogJob;

class SaveSysLogController extends Controller
{
    public static function runJob($params)
    {
        $job = new SaveSysLogJob($params);
        $job->dispatch($params)->onQueue('save_sys_log');
    }
}