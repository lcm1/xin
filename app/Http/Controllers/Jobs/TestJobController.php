<?php

namespace App\Http\Controllers\Jobs;

use App\Http\Controllers\Controller;
use App\Jobs\TestJob;

class TestJobController extends Controller
{
    public static function runJob($data)
    {
        $job = new TestJob($data);
        $job->dispatch($data)->onQueue('test_job');
    }
}