<?php
namespace App\Http\Controllers\Jobs;


use App\Jobs\AmazonSalesJob;
use App\Http\Controllers\Controller;

class AmazonSalesController extends Controller
{

    //新品导入队列类
    public static function runJob($data)
    {
        $job = new AmazonSalesJob($data);
        $job ->dispatch($data)->onQueue('amazon_sales');
    }
}
