<?php
namespace App\Http\Controllers\Jobs;


use App\Jobs\ExcelTaskJob;
use App\Http\Controllers\Controller;

class ExcelTaskController extends Controller
{

    //新品导入队列类
    public static function runJob($data)
    {
        $job = new ExcelTaskJob($data);
        $job ->dispatch($data)->onQueue('excel_task');
    }
}
