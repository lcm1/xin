<?php

namespace App\Http\Controllers\Jobs;

use App\Jobs\PublicSavePdfJob;

class PublicSavePdfController
{
    public static function runJob($data)
    {
        $job = new PublicSavePdfJob($data);
        $job ->dispatch($data)->onQueue('public_save_pdf');
    }
}