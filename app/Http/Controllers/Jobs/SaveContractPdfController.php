<?php

namespace App\Http\Controllers\Jobs;

use App\Jobs\SaveContractPdfJob;

class SaveContractPdfController
{
    public static function runJob($data)
    {
//        var_dump($data);exit();
        $job = new SaveContractPdfJob($data);
        $job->dispatch($data)->onQueue('save_contract_pdf');
    }
}