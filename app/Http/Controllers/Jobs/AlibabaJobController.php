<?php

namespace App\Http\Controllers\Jobs;

use App\Http\Controllers\Controller;
use App\Jobs\AlibabaJob;

class AlibabaJobController extends Controller
{
    public static function runJob($data)
    {
        $job = new AlibabaJob($data);
        $job->dispatch($data)->onQueue('alibaba');
    }
}