<?php
namespace App\Http\Controllers\Jobs;


use App\Jobs\CancelJob;
use App\Jobs\CloudhouseJob;
use App\Jobs\InhouseJob;
use App\Jobs\OuthouseJob;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CloudHouseController extends Controller
{

    static public $appKey = 'XM_QMESS_SKDYLB';

    static public $appSecret = '123456';

    static public $link = 'https://xywms.anport-logistics.com/out_iop_esb/ess/qm?';

    static public $success = 1;//成功执行

    static public $fail = 2;//执行失败

    //云仓同步商品接口队列
    public static function sendJob($data)
    {
        $job = new CloudhouseJob($data);
        $job ->dispatch($data)->onQueue('cloud_house');
    }

    //云仓创建入库单接口队列
    public static function inhouseJob($data)
    {
        $job = new InhouseJob($data);
        $job ->dispatch($data)->onQueue('in_house');
    }

    //云仓创建出库单接口队列
    public static function outhouseJob($data)
    {
        $job = new OuthouseJob($data);
        $job ->dispatch($data)->onQueue('out_house');
    }

    //云仓取消订单接口队列
    public static function cancelJob($data)
    {
        $job = new CancelJob($data);
        $job ->dispatch($data)->onQueue('order_cancel');
    }

    //签名函数
    public static function createSign($paramArr, $appSecret)
    {

        $sign = $appSecret;

        ksort($paramArr);

        foreach ($paramArr as $key => $val) {

            if ($key != '' && $val != '') {

                $sign .= $key . $val;

            }

        }
        $sign .= $appSecret;
        $sign = strtoupper(md5($sign));
        return $sign;
    }

//组参函数
    public static function createStrParam($paramArr)
    {
        $strParam = '';
        foreach ($paramArr as $key => $val) {
            if ($key != '' && $val != '') {
                $strParam .= $key . '=' . urlencode($val) . '&';
            }
        }
        return $strParam;
    }

    public static function curl_xml($url, $xml_data)
    {
        $header = array(
            'Content-Type:'.'application/xml',
            'charset:'.'UTF-8',
            'Connection:'.'Keep-Alive',
            'host:'.'xywms.anport-logistics.com',
            'Expect:'.''
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
        
        $result = curl_exec($ch);
        $headerInfo = curl_getinfo($ch, CURLINFO_HEADER_OUT);
        var_dump($headerInfo);
        curl_close($ch);
        return $result;

    }
    public static function xml_parser($str){
        $xml_parser = xml_parser_create();
        if(!xml_parse($xml_parser,$str,true)){
            xml_parser_free($xml_parser);
            return false;
        }else {
            return (json_decode(json_encode(simplexml_load_string($str)),true));
        }
    }

    public static function set_log($log_type,$log_status,$log_data){
        $add['log_type'] = $log_type;
        $add['createtime'] = date('Y-m-d H:i:s');
        $add['log_status'] =  $log_status;
        $add['log_data'] = $log_data;
        Db::table('system_log')->insert($add);
    }

}
