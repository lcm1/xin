<?php

namespace App\Http\Controllers\Jobs;

use App\Http\Controllers\Controller;
use App\Jobs\AutoDayreportJob;

class AutoDayreportJobController extends Controller
{
    public static function runJob($data)
    {
        $job = new AutoDayreportJob($data);
        $job->dispatch($data)->onQueue('dayreport');
    }
}