<?php

namespace App\Http\Controllers\Jobs;

use App\Jobs\SaveFinanceBillPdfJob;

class SaveFinanceBillPdfController
{
    public static function runJob($data)
    {
//        var_dump($data);exit();
        $job = new SaveFinanceBillPdfJob($data);
        $job->dispatch($data)->onQueue('save_finance_bill_pdf');
    }
}