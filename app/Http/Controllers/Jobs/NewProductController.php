<?php
namespace App\Http\Controllers\Jobs;


use App\Jobs\NewProductExportJob;
use App\Http\Controllers\Controller;

class NewProductController extends Controller
{

    //新品导入队列类
    public static function runJob($data)
    {
        $job = new NewProductExportJob($data);
        $job ->dispatch($data)->onQueue('new_product');
    }
}
