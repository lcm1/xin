<?php
/**
 * @describe spu/sku 生成
 *
 * @author lichimin
 * @since 2022/07/13
 */

namespace App\Http\Requests;


class SelfSpu extends Base
{
    public $errors = array();
    public $rules = [
        'id' => 'required|numeric',
        'name' => 'required',
        'identifying' => 'required',
        'type' => 'required|numeric',
        'platform_id' => 'required',
        'spu'=>'required',
        'spu_id' => 'required|numeric',
        'color_id'  => 'required',
        'size_id' => 'required',
        'user_id'=>'required|numeric',
        'operate_user_id'=>'required|numeric',
        'custom_sku' => 'required',
        'one_cate_id' => 'required',
        'two_cate_id' => 'required',
        'three_cate_id' => 'required',
        'file'=>'file',
    ];

    public $messages = [
        'id.required' => 'id不可为空',
        'id.numeric' => 'id类型错误',
        'name.required' => 'name不可为空',
        'identifying.required' => 'identifying不可为空',
        'type.required' => 'type不可为空',
        'type.numeric' => 'type类型错误',
        'platform_id.required' => 'platform_id不可为空',
        'platform_id.numeric' => 'platform_id类型错误',
        'spu.required'=>'spu不能为空',
        'spu_id.required' => 'spu_id不可为空',
        'spu_id.numeric' => 'spu_id类型错误',
        'color_id.required' => '颜色不可为空',
        'size_id.required' => '尺寸不可为空',
        'user_id.required' => 'user_id不可为空',
        'user_id.numeric' => 'user_id类型错误',
        'operate_user_id.required' => 'operate_user_id不可为空',
        'operate_user_id.numeric' => 'operate_user_id类型错误',
        'custom_sku.required' => 'custom_sku不可为空',
        'one_cate_id.required' => 'one_cate_id不可为空',
        'one_cate_id.numeric' => 'one_cate_id类型错误',
        'two_cate_id.required' => 'two_cate_id不可为空',
        'two_cate_id.numeric' => 'two_cate_id类型错误',
        'three_cate_id.required' => 'three_cate_id不可为空',
        'three_cate_id.numeric' => 'three_cate_id类型错误',
        'file'=>'必须上传文件',


    ];

    public $scene = [

        'CateImport' => ['user_id', 'file'],
        'CateAdd' =>['name','identifying','type','user_id'],
        'CateUpdate' => ['id','name','identifying','type','user_id'],
        'CateDel' =>['id','user_id'],


        'ColorSizeAdd' =>['name','identifying','type','user_id'],
        'ColorSizeUpdate' => ['id','name','identifying','type','user_id'],
        'ColorSizeDel' =>['id','user_id'],

        'SpuUpdate' => ['id'],
        'SpuPreview' =>['type','user_id','platform_id'],
        'SpuDel' => ['id', 'user_id'],
        'SelfSpuColorAdd' => ['color_id','spu'],
        'SelfSpuColorEdit' => ['id', 'color_id','spu'],
        'SelfSpuColorDel' => ['id'],
        'GetGroupSpu'=>['spu'],

        'SkuPreview' =>['spu_id','size_id'],
        'CustomSkuCreate' => ['user_id', 'custom_sku','operate_user_id'],
        'CustomSkuDel' =>['user_id'],
        'SkuCreate' => ['user_id', 'sku','shop_id','operate_user_id'],
        'SkuDel' =>['user_id'],
        'SpuOldBind'=>['id'],
    ];

}