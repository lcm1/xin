<?php
/**
 * @describe spu/sku 生成
 *
 * @author lichimin
 * @since 2022/07/13
 */

namespace App\Http\Requests;


class DataWhole extends Base
{
    public $errors = array();
    public $rules = [
        'id' => 'required|numeric',
        'name' => 'required',
        'supply_time' =>'required',
        'is_new' =>'required',
        'spu'=>'required',
        'user_id'=>'required|numeric',
        'operate_user_id'=>'required|numeric',
        'custom_sku' => 'required',
        'file'=>'file',
    ];

    public $messages = [
        'id.required' => 'id不可为空',
        'id.numeric' => 'id类型错误',
        'supply_time.required'=>'下单时间不能为空',
        'is_new.required'=>'是否新品不能为空',
        'name.required' => 'name不可为空',
        'spu.required'=>'spu不能为空',
        'user_id.required' => 'user_id不可为空',
        'user_id.numeric' => 'user_id类型错误',
        'operate_user_id.required' => 'operate_user_id不可为空',
        'operate_user_id.numeric' => 'operate_user_id类型错误',
        'custom_sku.required' => 'custom_sku不可为空',
        'file'=>'必须上传文件',

    ];

    public $scene = [
        'PlaceOrderImport' => ['user_id', 'file','supply_time','is_new'],
        'SupplyChainImport' => ['user_id', 'file'],
        'PlaceOrderEdit' =>['id'],
        'PlaceOrderDel'=>['id'],
        'dataWholeCustoms'=>['spu'],
        'getDatawholeCustomsSku'=>['custom_sku'],
    ];

}