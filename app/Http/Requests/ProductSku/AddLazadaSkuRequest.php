<?php

namespace App\Http\Requests\ProductSku;

use Illuminate\Foundation\Http\FormRequest;

class AddLazadaSkuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_sku_id' => 'required|numeric',
            'quantity' => 'required|numeric',
            'price' => 'required|numeric',
            'image' => 'required',
            'seller_sku' => 'required',
            'color' => 'required',
            'size' => 'required|numeric',
            'package_length' => 'required|numeric',
            'package_height' => 'required|numeric',
            'package_weight' => 'required|numeric',
            'package_width' => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'product_sku_id.required' => '缺少product_sku_id参数',
            'product_sku_id.numeric' => 'product_sku_id类型错误',
            'quantity.required' => '请输入库存数量',
            'quantity.numeric' => '库存数量只能是数字',
            'price.required' => '请输入价格',
            'price.numeric' => '价格只能是数字',
            'image.required' => '请上传图片',
            'seller_sku.required' => '请填写sku名称',
            'color.required' => '请填写颜色',
            'size.required' => '请填写尺寸',
            'size.numeric' => '尺寸只能是数字',
            'package_length.required' => '请填写包裹长',
            'package_length.numeric' => '包裹长只能是数字',
            'package_height.required' => '请填写包裹高',
            'package_height.numeric' => '包裹高只能是数字',
            'package_weight.required' => '请填写包裹重',
            'package_weight.numeric' => '包裹重只能是数字',
            'package_width.required' => '请填写包裹宽',
            'package_width.numeric' => '包裹宽只能是数字',
        ];
    }
}
