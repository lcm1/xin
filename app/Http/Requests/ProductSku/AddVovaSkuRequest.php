<?php

namespace App\Http\Requests\ProductSku;

use Illuminate\Foundation\Http\FormRequest;

class AddVovaSkuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => 'required|numeric',
            'seller_sku' => 'required',
            'quantity' => 'required|numeric',
            'price' => 'required|numeric',
            'shipping_fee' => 'required|numeric',
            'size' => 'required|numeric',
            'color' => 'required',
            'image' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'product_id.required' => '缺少product_id参数',
            'product_id.numeric' => 'product_id类型错误',
            'seller_sku.required' => '请填写sku名称',
            'quantity.required' => '请输入库存数量',
            'quantity.numeric' => '库存数量只能是数字',
            'price.required' => '请输入价格',
            'price.numeric' => '价格只能是数字',
            'shipping_fee.required' => '请输入运费',
            'shipping_fee.numeric' => '运费只能是数字',
            'size.required' => '请填写尺寸',
            'size.numeric' => '尺寸只能是数字',
            'color.required' => '请填写颜色',
            'image.required' => '请上传图片',
        ];
    }
}
