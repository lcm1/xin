<?php
/**
 * @describe 云仓
 *
 * @author lichimin
 * @since 2022/09/16
 */

namespace App\Http\Requests;


class CloudHouse extends Base
{
    public $errors = array();
    public $rules = [
        'id' => 'required|numeric',
        'type' => 'required|numeric',
        'spu'=>'required',
        'user_id'=>'required|numeric',
        'custom_sku' => 'required',
        'file'=>'file',
        'contract_no'=>'required',
        'out_no'=>'required',
    ];

    public $messages = [
        'id.required' => 'id不可为空',
        'id.numeric' => 'id类型错误',
        'type.required' => 'type不可为空',
        'type.numeric' => 'type类型错误',
        'spu.required'=>'spu不能为空',
        'user_id.required' => 'user_id不可为空',
        'user_id.numeric' => 'user_id类型错误',
        'custom_sku.required' => 'custom_sku不可为空',
        'file'=>'必须上传文件',
        'contract_no'=>'合同号不能为空',
        'out_no'=>'出库号为必传',
    ];

    public $scene = [
        'ContractImport' => ['user_id', 'file'],
        'ContractOutList'=>['out_no'],
        'ContractSyncDetail'=>['id'],
    ];

}