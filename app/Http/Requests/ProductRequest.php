<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page' => 'numeric',
            'limit' => 'numeric'
        ];
    }

    public function messages()
    {
        return [
            'page.numeric' => 'page只能是数字',
            'limit.numeric' => 'limit只能是数字',
        ];
    }
}
