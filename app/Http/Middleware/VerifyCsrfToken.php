<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'login',
	    '/pc/*',//pc端
	    
        '/home/index',
	    '/order/*',//订单
	    '/qc/*',//行程
	    '/factory/*',//工厂
	    '/finance/*',//公司
        '/user/*',//用户
        '/shipment/fileUpload',
    ];
}
