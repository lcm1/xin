<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

class LoginAdmin{
////////////////////////////////////////////////  后台验证
	/**
	 * 登录状态验证
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next){
		$admininfo = session('admininfo');
		
		
//		dump($admininfo);exit;
		if(empty($admininfo)){
			return redirect('/admin/login/login_html');
//			$this->back('请重新登录', '444');
		}else{
			$sql = "select `id`
                    from `admins`
                    where `token`='".$admininfo['token']."'";
			$admin_find = db::select($sql);
			if(empty($admin_find)){
				return redirect('/admin/login/login_html');
//				$this->back('请重新登录', '444');
			}
		}
		
		return $next($request);
	}
	
	/**
	 * desc:返回数据
	 * @param $desc(描述)
	 * @param int $code(状态码)
	 * @param array $data(数据)
	 */
	public function back($desc,$code='500',$data=array()){
		$back['code'] = (string)$code;
		$back['desc'] = $desc;
		if(!empty($data)){
			$back['data'] = $data;
		}
		
		header('Content-Type:application/json; charset=utf-8');
		exit(json_encode($back,JSON_UNESCAPED_UNICODE));
	}
	
	
	
	
}
