<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

class ValidateLogon{

    /**
     * 登录状态验证
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
//	    $this->back('系统更新中', '441');
       // return $next($request);
	    $data = $request->all();
//	    var_dump($data);exit;
	    if(empty($data['token'])){
	        $this->back('token为空，请重新登录', '441');
	    }
	    //验证用户是否存在
	    $sql = "select `Id`, `account`, `state`
					from users
					where `token`='{$data['token']}'";
	    $user_find = json_decode(json_encode(db::select($sql)),true);
	    if(empty($user_find)){
		    $this->back('token验证失败，请重新登录', '441');
	    }else if($user_find[0]['state'] == 2){
		    $this->back('账号已被禁用');
	    }
	    
	    $request['user_info'] = $user_find[0];
        return $next($request);
    }
	
	
	/**
	 * desc:返回数据
	 * @param $desc(描述)
	 * @param int $code(状态码)
	 * @param array $data(数据)
	 */
	public function back($desc,$code='500',$data=array()){
		$back['code'] = (string)$code;
		$back['desc'] = $desc;
		if(!empty($data)){
			$back['data'] = $data;
		}
		
		header('Content-Type:application/json; charset=utf-8');
		exit(json_encode($back,JSON_UNESCAPED_UNICODE));
	}
	
    
    
    
}
