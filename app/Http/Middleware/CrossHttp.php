<?php

namespace App\Http\Middleware;
use Closure;
class CrossHttp
{
    /**
     * @Desc: 跨域设置
     * @param $request
     * @param Closure $next
     * @return mixed
     * @author: Liu Sinian
     * @Time: 2023/4/24 16:34
     */
    public function handle($request, Closure $next) {
        // 制定允许其他域名访问
        header('Access-Control-Allow-Origin:*');
        // 响应类型
        header('Access-Control-Allow-Methods:*');
        //请求头
        header('Access-Control-Allow-Headers:*');
        // 响应头设置
        header('Access-Control-Allow-Credentials:false');

        return $next($request);
    }
}