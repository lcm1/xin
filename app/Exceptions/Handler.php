<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        // return json_encode(['code'=>'500','desc'=>'请求错误','data'=>$exception->getFile()]);
        return parent::render($request, $exception);
    }
    // public function render($request, Exception $exception)
    // {


    //     $errorData = [];
    //     $trace = $exception->getTrace();
    //     $errorData[] = [
    //         'line' => $exception->getLine(),
    //         'file' => $exception->getFile(),
    //         'message' => $exception->getMessage(),
    //     ];

    //     foreach ($trace as $item) {
    //         $errorData[] = [
    //             'line' => $item['line'] ?? null,
    //             'file' => $item['file'] ?? null,
    //             'message' => $item['class'] ?? null,
    //         ];
    //     }

    //         return json_encode(['code'=>'500','desc'=>'请求错误','data'=>$errorData]);
    // }

    // public function render($request, Exception $exception)
    // {
    //     if ($this->isApiException($request, $exception)) {
    //         $statusCode = $this->getStatusCode($exception);
    //         $errorMessage = $exception->getMessage();
    //         return new JsonResponse([
    //             "code"=> "error",
    //             "msg"=> $errorMessage,
    //         ], $statusCode);
    //     }
 
    //     return parent::render($request, $exception);
    // }
 
    // protected function isApiException($request, Exception $exception)   
    // {
    //     return $request->is('api/*') && $exception instanceof \Exception;
    // }
 
    // protected function getStatusCode($exception){
    //     if ($exception instanceof NotFoundHttpException) {
    //         return 404;
    //     }elseif ($exception instanceof ValidationException) {
    //         return 422;
    //     }
    //     return 200;
    // }

    
}
