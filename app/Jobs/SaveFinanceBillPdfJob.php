<?php

namespace App\Jobs;

use App\Models\AmazonPlaceOrderContractModel;
use App\Models\BaseModel;
use App\Models\FinanceModel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
class SaveFinanceBillPdfJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $params;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        //
        $this->params = $params;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
//        echo '进入队列SaveFinanceBillPdfJob';
//         var_dump($this->params);exit();
        try {
            $params = $this->params;
            $insert['name'] = $params['title'];
            $insert['type'] = $params['type'];
            $insert['user_id'] = $params['user_id'];
            $insert['create_time'] = date('Y-m-d H:i:s', time());
            $insert['status'] = 0;
            $insert['msg'] = '生成中';
            $insert['path'] = '';
            $insert['download_type'] = 2;

            $task_id = Db::table('excel_task')->insertGetId($insert);

            $params['task_id'] = $task_id;
            //消化队列
            $result = $this->printBillPdf($params);
            if ($result['code'] == 200){
                return $params['title'].'----打印成功！';
            }else{
                return $params['title'].'----打印失败！';
            }
        } catch (\Exception $e) {

            //消化失败，记录错误日志
            $errmsg = $e->getMessage() . 'line:' . $e->getLine();
            // $errmsg = json_encode($e);
            $add['job'] = 'save_contract_pdf';
            $add['time'] = time();
            $add['msg'] = $errmsg;
            $add['data'] = json_encode($this->params);
            Db::table('job_err_log')->insert($add);
//            echo '队列错误！'.$e->getMessage();
//            exit();
        }
    }

    protected function _savePdf($params)
    {
        try {
            require_once(app_path() . DIRECTORY_SEPARATOR . 'Libs' . DIRECTORY_SEPARATOR . 'tcpdf' . DIRECTORY_SEPARATOR . 'examples' . DIRECTORY_SEPARATOR . 'tcpdf_include.php');
            $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

            // 设置文档信息
            $pdf->SetCreator('L');
            $pdf->SetAuthor('L');
            $pdf->SetTitle($params['title']);
            $pdf->SetSubject($params['title']);
            $pdf->SetKeywords('TCPDF, PDF, PHP');

            // 设置页眉和页脚信息
            $pdf->SetHeaderData('', 0, '', '', [0, 0, 0], [255, 255, 255]);
            $pdf->setFooterData([0, 0, 0], [255, 255, 255]);

            // 设置页眉和页脚字体
            $pdf->setHeaderFont(['stsongstdlight', '', '10']);
            $pdf->setFooterFont(['stsongstdlight', '', '8']);

            //删除预定义的打印 页眉/页尾
//            $pdf->setPrintHeader($result['is_header'] ?? false);
//            $pdf->setPrintFooter($result['is_footer'] ?? false);

            // 设置默认等宽字体
            $pdf->SetDefaultMonospacedFont('courier');

            // 设置字体
//            $pdf->setFont('times');

            // 设置间距
            $pdf->SetMargins(10, 15, 15);//页面间隔
            $pdf->SetHeaderMargin(5);//页眉top间隔
            $pdf->SetFooterMargin(5);//页脚bottom间隔

            // 设置分页
            $pdf->SetAutoPageBreak(TRUE, 15);
            $pdf->setFontSubsetting(true);

            // set default font subsetting mode
            $pdf->setFontSubsetting(true);

            //设置字体 stsongstdlight支持中文
            $pdf->SetFont('stsongstdlight', '', 10);

            $pdf->Ln(5);

            // 设置密码
//            $pdf->SetProtection($permissions = array('print', 'modify', 'copy', 'annot-forms', 'fill-forms', 'extract', 'assemble', 'print-high'), $user_pass = '123456', $owner_pass = null, $mode = 0, $pubkeys = null );

            //第一页
            $pdf->AddPage();
            $pdf->writeHTML($params['html'], true, false, true, true, 'center');
//var_dump($params['html']);
            //输出PDF
            $path = 'pdf' . DIRECTORY_SEPARATOR . $params['title'] . '.pdf';
            $pdf->Output(public_path() . DIRECTORY_SEPARATOR . $path, 'F');//I输出、D下载
            Db::table('excel_task')->where('id', $params['task_id'])->update([
                'status' => 1,
                'msg' => '打印成功！',
                'path' => $path
            ]);
            // 更新下载地址
            $update = db::table('financial_bill')
                ->where('id', $params['id'])
                ->update([
                    'pdf_file' => $path
                ]);

//            echo $params['title'].'打印成功！';
            return;
        } catch (\Exception $e) {
            echo $e->getMessage().'，'.$e->getLine();
            return;
        }
    }

    private function printBillPdf($params)
    {
        try {
            if (!isset($params['id']) || empty($params['id'])){
                throw new \Exception('未给定票据标识！');
            }
            $billMdl = db::table('financial_bill')
                ->where('id', $params['id'])
                ->first();
            if (empty($billMdl)){
                throw new \Exception('票据不存在！');
            }
            $params['bill_no'] = $billMdl->bill_no;
//            if (!in_array($billMdl->status, [2,3])){
//                throw new \Exception('票据未审核通过，不可打印票据！');
//            }
            switch($billMdl->bill_type){
                case 1:
                    $result = $this->_getAdvancePaymentBillPdfDetail($billMdl);
                    break;
                case 2:
                    $result = $this->_getAccountsPaymentBillPdfDetail($billMdl);
                    break;
                case 3:
                    $result = $this->_getSurchargePaymentBillPdfDetail($billMdl);
                    break;
                default:
                    throw new \Exception('未定义的票据类型，不可打印票据！');
            }
            // 加入队列
            $params['html'] = $result['html'];
            $this->_savePdf($params);
//            echo '打印成功！';
            return ['code' => 200, 'msg' => '打印成功！'];
        }catch (\Exception $e){
            db::rollback();
            echo $e->getMessage().'，'.$e->getLine();
        }
    }

    private function  _getAdvancePaymentBillPdfDetail($billMdl)
    {
        $financeModel = new FinanceModel();
        $billDetail = $financeModel->getPaymentBillDetail(['id' => $billMdl->id])['data'] ?? [];
        if (empty($billDetail)){
            throw new \Exception('未获取到票据详情数据！');
        }

        $bigWrite = $financeModel->convertAmountToCn($billDetail->actual_payment);
        $html = '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table {
            border-collapse: collapse;
        }
        td, th {
            padding: 0;
        }
        table {
            border-collapse: separate;
            border-spacing: 0;
        }
        td,tr {
            font-size: 10px;
            line-height: 15px;
            vertical-align: middle;
        }
        td{
            width: 10%;
        }
        .border1{
            border-width: 0.5rem;
            border-style: double double double double;
            border-color: black;
            height: 8px;
        }
    </style>
</head>
<body>
    <table  width="100%">
        <tr>
            <td colspan="10" style="text-align: center;font-size: 20px; font-weight: bold;">预付账款单</td>
        </tr>
        <tr>
            <td colspan="7" rowspan="3" style="text-align: center;"></td>
            <td colspan="3">付款单号：'.$billDetail->bill_no.'</td>
        </tr>
        <tr>
            <td colspan="3">合同号：'.$billDetail->contract_no.'</td>
        </tr>
        <tr>
            <td colspan="3">状态：'.$billDetail->status_name.'</td>
        </tr>
        <tr>
            <td class="border1" colspan="1">供应商简称</td>
            <td class="border1" colspan="4">'.$billDetail->supplier_name.'</td>
            <td class="border1" colspan="1">付款日期</td>
            <td class="border1" colspan="4">'.$billDetail->payment_date.'</td>
        </tr>
        <tr>
            <td class="border1" colspan="1">收款人</td>
            <td class="border1" colspan="2">'.$billDetail->payee.'</td>
            <td class="border1" colspan="1">收款人开户银行</td>
            <td class="border1" colspan="2">'.$billDetail->receiving_bank_name.'</td>
            <td class="border1" colspan="1">收款人银行账号</td>
            <td class="border1" colspan="3">'.$billDetail->receiving_bank_card_number.'</td>
        </tr>
        <tr>
            <td class="border1" colspan="1">付款人</td>
            <td class="border1" colspan="2">'.$billDetail->payer.'</td>
            <td class="border1" colspan="1">付款人开户银行</td>
            <td class="border1" colspan="2">'.$billDetail->payment_bank_name.'</td>
            <td class="border1" colspan="1">付款人银行账号</td>
            <td class="border1" colspan="3">'.$billDetail->payment_bank_card_number.'</td>
        </tr>
        <tr>
            <td class="border1" colspan="1">付款方式</td>
            <td class="border1" colspan="2">T/T</td>
            <td class="border1" colspan="1">合同总金额</td>
            <td class="border1" colspan="2">'.$billDetail->contract_total_price.'</td>
            <td class="border1" colspan="1">预付比率</td>
            <td class="border1" colspan="3">'.$billDetail->ratio_name.'</td>
        </tr>
        <tr>
            <td class="border1" colspan="1" rowspan="2">本次支付</td>
            <td class="border1" colspan="1">小写</td>
            <td class="border1" colspan="1">'.$billDetail->actual_payment.'</td>
            <td class="border1" colspan="1" rowspan="2">币种</td>
            <td class="border1" colspan="2" rowspan="2">RMB</td>
            <td class="border1" colspan="1" rowspan="2">附加费用扣款</td>
            <td class="border1" colspan="3" rowspan="2"></td>
        </tr>
        <tr>
            <td class="border1" colspan="1">大写</td>
            <td class="border1" colspan="1">'.$bigWrite.'</td>
        </tr>
        <tr>
            <td class="border1" colspan="2">付款单类型</td>
            <td class="border1" colspan="8">'.$billDetail->bill_type_name.'</td>
        </tr>
        <tr>
            <td class="border1" colspan="2">备注</td>
            <td class="border1" colspan="2">'.$billDetail->memo.'</td>
            <td class="border1" colspan="2">审核意见反馈</td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="2">申请人</td>
            <td class="border1" colspan="2">'.$billDetail->user_name.'</td>
            <td class="border1" colspan="1">申请日期</td>
            <td class="border1" colspan="5">'.$billDetail->created_at.'</td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: center;"></td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: center;">预付款产品信息</td>
        </tr>
        <tr>
            <td class="border1" colspan="3">款号</td>
            <td class="border1" colspan="3">品名</td>
            <td class="border1" colspan="4">颜色</td>
        </tr>';
        foreach ($billDetail->bill_items as $v){
            $html .= '<tr>
                <td class="border1" colspan="3">'.$v['spu'].'</td>
                <td class="border1" colspan="3">'.$v['title'].'</td>
                <td class="border1" colspan="4">'.implode(',', $v['color_name']).'</td>
            </tr>';
        }

        $html .= '
        <tr>
            <td colspan="10" style="text-align: center;"></td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: center;">付款单核销详情</td>
        </tr>
        <tr>
            <td class="border1" colspan="2">预付款总金额</td>
            <td class="border1" colspan="2">累计已核销金额</td>
            <td class="border1" colspan="2">未付款金额</td>
            <td class="border1" colspan="4">备注</td>
        </tr>
        <tr>
            <td class="border1" colspan="2">'.$billDetail->total_advance_amount.'</td>
            <td class="border1" colspan="2">'.$billDetail->total_write_off_amount.'</td>
            <td class="border1" colspan="2">'.($billDetail->total_advance_amount - $billDetail->total_write_off_amount).'</td>
            <td class="border1" colspan="4">'.$billDetail->payment_method.'</td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: center;"></td>
        </tr>
        <tr>
            <td class="border1" colspan="3">审批部门</td>
            <td class="border1" colspan="3">审批签字</td>
            <td class="border1" colspan="4">审批意见</td>
        </tr>
        <tr>
            <td class="border1" colspan="3">经办人</td>
            <td class="border1" colspan="3"></td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="3">直属主管</td>
            <td class="border1" colspan="3"></td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="3">财务稽核人员</td>
            <td class="border1" colspan="3"></td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="3">财务主管</td>
            <td class="border1" colspan="3"></td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="3">财务经理</td>
            <td class="border1" colspan="3"></td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="3">总经理</td>
            <td class="border1" colspan="3"></td>
            <td class="border1" colspan="4"></td>
        </tr>
    </table>
</body>
</html>';

        return ['html' => $html, 'title' => $billDetail->bill_type_name];
    }

    private function _getSurchargePaymentBillPdfDetail($billMdl)
    {
        $financeModel = new FinanceModel();
        $billDetail = $financeModel->getPaymentBillDetail(['id' => $billMdl->id])['data'] ?? [];
        if (empty($billDetail)){
            throw new \Exception('未获取到票据详情数据！');
        }

//        $bigWrite = $this->convertAmountToCn($billDetail->actual_payment);
        $html = '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table {
            border-collapse: collapse;
        }
        td, th {
            padding: 0;
        }
        table {
            border-collapse: separate;
            border-spacing: 0;
        }
        td,tr {
            font-size: 10px;
            line-height: 15px;
            vertical-align: middle;
        }
        td{
            width: 10%;
        }
        .border1{
            border-width: 0.5rem;
            border-style: double double double double;
            border-color: black;
            height: 8px;
        }
        .fontBlod{
            font-weight: bold;
        }
    </style>
</head>
<body>
    <table  width="100%">
        <tr>
            <td colspan="10" style="text-align: center;font-size: 20px; font-weight: bold; padding-bottom: 20px;">厦门微微尔电子商务有限公司</td>
        </tr>
         <tr>
            <td colspan="10" style="text-align: center;font-size: 20px; font-weight: bold;"></td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: center;font-size: 16px; font-weight: bold; padding-top: 5%">附加费用表</td>
        </tr>
        <tr>
            <td colspan="7" rowspan="3" style="text-align: center;"></td>
            <td colspan="2">单号：'.$billDetail->bill_no.'</td>
        </tr>
        <tr>
            <td colspan="2">日期：'.date('Y-m-d', strtotime($billDetail->created_at)).'</td>
        </tr>
         <tr>
            <td colspan="2">状态：'.$billDetail->status_name.'</td>
        </tr>
        <tr>
            <td class="border1" colspan="1">采购合同号</td>
            <td class="border1" colspan="4">'.$billDetail->contract_no.'</td>
            <td class="border1" colspan="1">供应商简称</td>
            <td class="border1" colspan="4">'.$billDetail->supplier_short_name.'</td>
        </tr>
        <tr>
            <td class="border1" colspan="1">开户行(收款方)</td>
            <td class="border1" colspan="4">'.$billDetail->receiving_bank_name.'</td>
            <td class="border1" colspan="1">银行账号(收款方)</td>
            <td class="border1" colspan="4">'.$billDetail->receiving_bank_card_number.'</td>
        </tr>
         <tr>
            <td class="border1" colspan="1">开户行(我司)</td>
            <td class="border1" colspan="4">'.$billDetail->payment_bank_card_number.'</td>
            <td class="border1" colspan="1">银行账号(我司)</td>
            <td class="border1" colspan="4">'.$billDetail->payment_bank_name.'</td>
        </tr>
        <tr>
            <td class="border1" colspan="2" rowspan="">费用总额</td>
            <td class="border1" colspan="8">'.$billDetail->actual_payment.'</td>
        </tr>
        <tr>
            <td class="border1" colspan="2" rowspan="">费用理由</td>
            <td class="border1" colspan="8">'.$billDetail->application_reason.'</td>
        </tr>
        
        <tr>
            <td class="fontBlod" colspan="10" style="text-align: left;">付款明细</td>
        </tr>
        <tr>
            <td class="border1" colspan="1">采购合同号</td>
            <td class="border1" colspan="2">款号</td>
            <td class="border1" colspan="1">商品编号</td>
            <td class="border1" colspan="1">品名</td>
            <td class="border1" colspan="2">颜色</td>
            <td class="border1" colspan="1">数量</td>
            <td class="border1" colspan="1">合同单价</td>
            <td class="border1" colspan="1">合计金额</td>
        </tr>';

        foreach ($billDetail->bill_items as $v){
            $html .= '<tr>
            <td class="border1" colspan="1">'.$v['contract_no'].'</td>
            <td class="border1" colspan="2">'.$v['spu'].'</td>
            <td class="border1" colspan="1">'.$v['spu'].'</td>
            <td class="border1" colspan="1">'.$v['title'].'</td>
            <td class="border1" colspan="2">'.implode(',', $v['color_name']).'</td>
            <td class="border1" colspan="1">'.$v['num'].'</td>
            <td class="border1" colspan="1">'.$v['price'].'</td>
            <td class="border1" colspan="1">'.$v['total_price'].'</td>
        </tr>';
        }

        $html .= '<tr>
            <td class="fontBlod" colspan="10" style="text-align: left;">备注</td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: left;"></td>
        </tr>
    </table>
</body>
</html>';

        return ['html' => $html, 'title' => $billDetail->bill_type_name];
    }

    private function _getAccountsPaymentBillPdfDetail($billMdl)
    {
        try {
            $financeModel = new FinanceModel();
            $billDetail = $financeModel->getPaymentBillDetail(['id' => $billMdl->id])['data'] ?? [];
            if (empty($billDetail)) {
                throw new \Exception('未获取到票据详情数据！');
            }
            $bigAmount = $financeModel->convertAmountToCn($billDetail->contract_total_price);
            $html = '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table {
            border-collapse: collapse;
        }
        td, th {
            padding: 0;
        }
        table {
            border-collapse: separate;
            border-spacing: 0;
        }
        td,tr {
            font-size: 10px;
            line-height: 15px;
            vertical-align: middle;
        }
        td{
            width: 10%;
        }
        .border1{
            border-width: 0.5rem;
            border-style: double double double double;
            border-color: black;
            height: 8px;
        }
        .fontBlod{
            font-weight: bold;
        }
    </style>
</head>
<body>
    <table  width="100%">
        <tr>
            <td colspan="10" style="text-align: center;font-size: 20px; font-weight: bold; padding-bottom: 20px;">' . $billDetail->company_name . '</td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: center;font-size: 20px; font-weight: bold;"></td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: center;font-size: 20px; font-weight: bold;">付款申请书</td>
        </tr>
        <tr>
            <td colspan="7">单号：' . $billDetail->bill_no . '</td>
            
        </tr>
        <tr>
            <td colspan="7">合同号：' . $billDetail->contract_no . '</td>
            <td colspan="2">日期：' . date('Y-m-d', strtotime($billDetail->created_at)) . '</td>
        </tr>
        <tr>
            <td colspan="7">申请人：' . $billDetail->user_name . '</td>
            <td colspan="2">状态：' . $billDetail->status_name . '</td>
        </tr>
        <tr>
            <td class="border1" colspan="1">申请事由</td>
            <td class="border1" colspan="4">' . $billDetail->application_reason . '</td>
            <td class="border1" colspan="1">付款方式</td>
            <td class="border1" colspan="4">T/T</td>
        </tr>
        <tr>
            <td class="border1" colspan="1">收款(供货)单位</td>
            <td class="border1" colspan="4">' . $billDetail->supplier_short_name . '</td>
            <td class="border1" colspan="1">收款人</td>
            <td class="border1" colspan="4">' . $billDetail->payee . '</td>
        </tr>
         <tr>
            <td class="border1" colspan="1">开户行(供应商)</td>
            <td class="border1" colspan="4">' . $billDetail->receiving_bank_name . '</td>
            <td class="border1" colspan="1">银行账号(供应商)</td>
            <td class="border1" colspan="4">' . $billDetail->receiving_bank_card_number . '</td>
        </tr>
        <tr>
            <td class="border1" colspan="1">开户行(我司)</td>
            <td class="border1" colspan="4">' . $billDetail->payment_bank_name . '</td>
            <td class="border1" colspan="1">银行账号(我司)</td>
            <td class="border1" colspan="4">' . $billDetail->payment_bank_card_number . '</td>
        </tr>
        <tr>
            <td class="border1" colspan="1">品名</td>
            <td class="border1" colspan="4"></td>
            <td class="border1" colspan="1">交货时间</td>
            <td class="border1" colspan="4">' . $billDetail->report_date . '</td>
        </tr>
        <tr>
            <td class="border1" colspan="1">本次实付金额</td>
            <td class="border1" colspan="1">小写</td>
            <td class="border1" colspan="3">' . $billDetail->actual_payment . '</td>
            <td class="border1" colspan="1">已支付情况</td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="1">应付金额</td>
            <td class="border1" colspan="4">' . $billDetail->payable_amount . '</td>
            <td class="border1" colspan="1">币种</td>
            <td class="border1" colspan="4">RMB</td>
        </tr>
        <tr>
            <td class="border1" colspan="1">采购合同总金额</td>
            <td class="border1" colspan="1">小写</td>
            <td class="border1" colspan="3">' . $billDetail->contract_total_price . '</td>
            <td class="border1" colspan="1">大写</td>
            <td class="border1" colspan="4">' . $bigAmount . '</td>
        </tr>
        <tr>
            <td class="border1" colspan="1">是否已结汇</td>
            <td class="border1" colspan="4"></td>
            <td class="border1" colspan="1">验货情况</td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="1">增值税发票抬头</td>
            <td class="border1" colspan="4"></td>
            <td class="border1" colspan="1">增值税发票号</td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="1">其他</td>
            <td class="border1" colspan="4"></td>
            <td class="border1" colspan="1">总经理</td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="1">备注</td>
            <td class="border1" colspan="9">' . $billDetail->memo . '</td>
        </tr>
        <tr>
            <td class="border1" colspan="1">审批备注</td>
            <td class="border1" colspan="9"></td>
        </tr>
         <tr>
            <td colspan="10" style="text-align: center;font-size: 20px; font-weight: bold;"></td>
        </tr>
        <tr>
            <td class="fontBlod" colspan="10" style="text-align: center;font-size: 14px;">预付款</td>
        </tr>
         <tr>
            <td colspan="10" style="text-align: center;font-size: 20px; font-weight: bold;"></td>
        </tr>
        <tr>
            <td class="border1" colspan="1">预付款单号</td>
            <td class="border1" colspan="1">合同号</td>
            <td class="border1" colspan="2">申请事由</td>
            <td class="border1" colspan="1">预付比率</td>
            <td class="border1" colspan="1">预付金额</td>
            <td class="border1" colspan="1">实付金额</td>
            <td class="border1" colspan="1">支付时间</td>
            <td class="border1" colspan="2">备注</td>
        </tr>';
            if (!empty($billDetail->advance_detail)) {
                foreach ($billDetail->advance_detail as $v) {
                    $html .= '<tr>
                <td class="border1" colspan="1">' . $v['bill_no'] . '</td>
                <td class="border1" colspan="1">' . $v['contract_no'] . '</td>
                <td class="border1" colspan="2">' . $v['application_reason'] . '</td>
                <td class="border1" colspan="1">' . $v['ratio_name'] . '</td>
                <td class="border1" colspan="1">' . $v['payable_amount'] . '</td>
                <td class="border1" colspan="1">' . $v['actual_payment'] . '</td>
                <td class="border1" colspan="1">' . $v['payment_date'] . '</td>
                <td class="border1" colspan="2">' . $v['memo'] . '</td>
            </tr>';
                }
            } else {
                $html .= '<tr>
                <td class="border1" colspan="1"></td>
                <td class="border1" colspan="1"></td>
                <td class="border1" colspan="2"></td>
                <td class="border1" colspan="1"></td>
                <td class="border1" colspan="1"></td>
                <td class="border1" colspan="1"></td>
                <td class="border1" colspan="1"></td>
                <td class="border1" colspan="2"></td>
            </tr>';
            }

            $html .= '<tr>
            <td colspan="10" style="text-align: center;font-size: 20px; font-weight: bold;"></td>
        </tr>
        <tr>
            <td class="fontBlod" colspan="10" style="text-align: center;font-size: 14px;">已付款</td>
        </tr>
         <tr>
            <td colspan="10" style="text-align: center;font-size: 20px; font-weight: bold;"></td>
        </tr>
        <tr>
            <td class="border1" colspan="1">应付账单号</td>
            <td class="border1" colspan="1">合同号</td>
            <td class="border1" colspan="2">申请事由</td>
            <td class="border1" colspan="1">货值比率</td>
            <td class="border1" colspan="1">应付金额</td>
            <td class="border1" colspan="1">实付金额</td>
            <td class="border1" colspan="1">支付时间</td>
            <td class="border1" colspan="2">备注</td>
        </tr>';

            if (!empty($billDetail->payment_detail)) {
                foreach ($billDetail->payment_detail as $v) {
                    $html .= '<tr>
                <td class="border1" colspan="1">' . $v['bill_no'] . '</td>
                <td class="border1" colspan="1">' . $v['contract_no'] . '</td>
                <td class="border1" colspan="2">' . $v['application_reason'] . '</td>
                <td class="border1" colspan="1">' . $v['ratio_name'] . '</td>
                <td class="border1" colspan="1">' . $v['payable_amount'] . '</td>
                <td class="border1" colspan="1">' . $v['actual_payment'] . '</td>
                <td class="border1" colspan="1">' . $v['payment_date'] . '</td>
                <td class="border1" colspan="2">' . $v['memo'] . '</td>
            </tr>';
                }
            } else {
                $html .= '<tr>
                <td class="border1" colspan="1"></td>
                <td class="border1" colspan="1"></td>
                <td class="border1" colspan="2"></td>
                <td class="border1" colspan="1"></td>
                <td class="border1" colspan="1"></td>
                <td class="border1" colspan="1"></td>
                <td class="border1" colspan="1"></td>
                <td class="border1" colspan="2"></td>
            </tr>';
            }


            $html .= '<tr>
            <td colspan="10" style="text-align: center;font-size: 20px; font-weight: bold;"></td>
        </tr>
         <tr>
             <td class="fontBlod" colspan="10" style="text-align: center;font-size: 14px;">本次付款详情</td>
        </tr>
         <tr>
            <td colspan="10" style="text-align: center;font-size: 20px; font-weight: bold;"></td>
        </tr>
        <tr>
            <td class="border1" colspan="1">合同号</td>
            <td class="border1" colspan="1">平台</td>
            <td class="border1" colspan="1">款号</td>
            <td class="border1" colspan="1">中文名</td>
            <td class="border1" colspan="1">颜色</td>
            <td class="border1" colspan="1">数量</td>
            <td class="border1" colspan="1">单价</td>
            <td class="border1" colspan="1">应付金额</td>
            <td class="border1" colspan="1">实付金额</td>
            <td class="border1" colspan="1">备注</td>
        </tr>';

            $totalPrice = 0;
            $actualTotalPrice = 0;
            $totalNum = 0;
            foreach ($billDetail->bill_items as $v) {
                $totalPrice += $v['total_price'];
                $actualTotalPrice += $v['actual_total_price'];
                $totalNum += $v['num'];
                $html .= '<tr>
            <td class="border1" colspan="1">' . $v['contract_no'] . '</td>
            <td class="border1" colspan="1">' . $v['platform_name'] . '</td>
            <td class="border1" colspan="1">' . $v['spu'] . '</td>
            <td class="border1" colspan="1">' . $v['name'] . '</td>
            <td class="border1" colspan="1">' . $v['color_name'] . '/' . $v['color_identifying'] . '</td>
            <td class="border1" colspan="1">' . $v['num'] . '</td>
            <td class="border1" colspan="1">' . $v['price'] . '</td>
            <td class="border1" colspan="1">' . $v['total_price'] . '</td>
            <td class="border1" colspan="1">' . $v['actual_total_price'] . '</td>
            <td class="border1" colspan="1"></td>
        </tr>';
            }

            $html .= '<tr>
            <td colspan="5">合计：</td>
            <td colspan="1">' . $totalNum . '</td>
            <td colspan="1"></td>
            <td colspan="1">' . $totalPrice . '</td>
            <td colspan="1">' . $actualTotalPrice . '</td>
            <td colspan="1"></td>
        </tr>';

            $html .= '<tr>
            <td colspan="10" style="text-align: center;font-size: 20px; font-weight: bold;"></td>
        </tr>
         <tr>
             <td class="fontBlod" colspan="10" style="text-align: center;font-size: 14px;">本次抵扣详情</td>
        </tr>
         <tr>
            <td colspan="10" style="text-align: center;font-size: 20px; font-weight: bold;"></td>
        </tr>
        <tr>
            <td class="border1" colspan="2">合同号</td>
            <td class="border1" colspan="2">应付账单号</td>
            <td class="border1" colspan="2">抵扣来源单号</td>
            <td class="border1" colspan="1">来源单号类型</td>
            <td class="border1" colspan="1">收支类型</td>
            <td class="border1" colspan="2">金额</td>
        </tr>';
            $deductionAmount = 0;
            foreach ($billDetail->deduction_detail as $v) {
                $deductionAmount += $v->deduction_amount;
                $html .= '<tr>
            <td class="border1" colspan="2">' . $v->contract_no . '</td>
            <td class="border1" colspan="2">' . $v->bill_no . '</td>
            <td class="border1" colspan="2">' . $v->origin_bill_no . '</td>
            <td class="border1" colspan="1">' . $v->deduction_bill_type_name . '</td>
            <td class="border1" colspan="1">' . $v->type . '</td>
            <td class="border1" colspan="2">' . $v->deduction_amount . '</td>
        </tr>';
            }
            $html .= '<tr>
            <td class="border1" colspan="8">合计：</td>
            <td class="border1" colspan="2">' . $deductionAmount . '</td>
        </tr>';
            $html .= '<tr>
            <td class="border1" colspan="10">实付金额：'.$totalPrice.' - ('.$deductionAmount.') = '.$actualTotalPrice.'</td>
        </tr>';

            $html .= '<tr>
            <td colspan="10" style="text-align: center;font-size: 20px; font-weight: bold;"></td>
        </tr>
        <tr>
            <td class="border1" colspan="3">审批部门</td>
            <td class="border1" colspan="3">审批签字</td>
            <td class="border1" colspan="4">审批意见</td>
        </tr>
        <tr>
            <td class="border1" colspan="3">经办人</td>
            <td class="border1" colspan="3"></td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="3">直属主管</td>
            <td class="border1" colspan="3"></td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="3">财务稽核人员</td>
            <td class="border1" colspan="3"></td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="3">财务主管</td>
            <td class="border1" colspan="3"></td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="3">财务经理</td>
            <td class="border1" colspan="3"></td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td class="border1" colspan="3">总经理</td>
            <td class="border1" colspan="3"></td>
            <td class="border1" colspan="4"></td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: left;"></td>
        </tr>
    </table>
</body>
</html>';
            return ['html' => $html, 'title' => $billDetail->bill_type_name];
        }catch(\Exception $e){
            echo $e;
        }
    }
}