<?php

namespace App\Jobs;

use App\Http\Controllers\Jobs\CloudHouseController;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;


class SendPdfJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $params;
    protected $type;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        //
        $this->params = $params;
        $this->type = 'entryorder.create';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        echo '队列SendPdfJob';
        try {
            //消化队列
            $res = $this->send_pdf($this->params);
            if($res['code']==1){
                $data = simplexml_load_string($res['data']);
                $log_data = json_encode($data,JSON_UNESCAPED_UNICODE);
                $data = json_decode($log_data,true);
//                var_dump($data['code']);
                if($data['code']!=0){
                    Redis::Lpush('in_house_fail',$this->params['request_id']);
                }
                $add['log_type'] = 2;
                $add['createtime'] = date('Y-m-d H:i:s');
                $add['log_status'] =  1;
                $add['log_data'] = $log_data;
                Db::table('system_log')->insert($add);
            }else{
                $add['log_type'] = 2;
                $add['createtime'] = date('Y-m-d H:i:s');
                $add['log_status'] =  2;
                $add['log_data'] = $res['error'];
                Db::table('system_log')->insert($add);
            }
        } catch(\Exception $e){
            //消化失败，记录错误日志
            echo $this->params;
            $errmsg = $e->getMessage();
            $add['log_type'] = 2;
            $add['createtime'] = date('Y-m-d H:i:s');
            $add['log_status'] =  2;
            $add['log_data'] = $errmsg;
            Db::table('system_log')->insert($add);
        }
    }


    public function send_pdf($params)
    {
        $appKey = CloudHouseController::$appKey;

        $appSecret = CloudHouseController::$appSecret;

        //singleitem.synchronize=>商品同步
        //entryorder.create=>入库单创建
        //stockout.create=>出库单创建
        //order.cancel=>取消订单
        $paramArr = array(
            'app_key' => $appKey,
            'method' => $params['method'],
            'format' => 'xml',
            'v' => '2.0',
            'sign_method' => 'md5',
            'timestamp' => date('Y-m-d H:i:s'),
            'customerId' => 'XM_QMESS_SKDYLB',
        );

        $sign = CloudHouseController::createSign($paramArr, $appSecret);
        //组参
        $strParam = CloudHouseController::createStrParam($paramArr);

        $strParam .= 'sign=' . $sign;

        $createtime = date('Y-m-d H:i:s');

        $skus = DB::table('amazon_buhuo_detail')->where('request_id',$params['request_id'])->select('sku','courier_num','shipping_num','air_num')->get()->toArray();
        if(empty($skus)){
            return ['code'=>0,'data'=>'','error'=>'此计划没有sku数据'];
        }
        $skus = json_decode(json_encode($skus),true);
        //orderType=>出入库类型
        //JYCK=一般交易出库单;HHCK= 换货出库;BFCK=补发出库;PTCK=普通出库单;DBCK=调拨出库;B2BRK=B2B入库;B2BCK=B2B出库;
        //QTCK=其他出库;SCRK=生产入库;LYRK=领用入库;CCRK=残次品入库;CGRK=采购入库;DBRK= 调拨入库;QTRK=其他入库;
        //XTRK= 销退入库;THRK=退货入库;HHRK= 换货入库;CNJG= 仓内加工单;CGTH=采购退货出库单
        if ($params['method'] == 'stockout.create') {
            $xml_data = '<?xml version="1.0" encoding="UTF-8"?>
                        <request>
                          <flag>success</flag>
                            <code>0</code>                            
                            <data>'.$params['pdf_link'].'</data>                            
                            <message>处理成功</message>
                        </request>';
        }
        $xml_data = str_replace(PHP_EOL, '', $xml_data);
        //发起请求
        $url = CloudHouseController::$link . $strParam;

        $result = CloudHouseController::curl_xml($url, $xml_data);

        return ['code'=>1,'data'=>$result,'error'=>''];
    }


}
