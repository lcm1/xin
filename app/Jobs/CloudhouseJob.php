<?php

namespace App\Jobs;

use App\Http\Controllers\Jobs\CloudHouseController;
use App\Models\BaseModel;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;


class CloudhouseJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $params;
    protected $type;
    protected $log_type;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        //
        $this->params = $params;
        $this->type = 'singleitem.synchronize';
        $this->log_type = 1;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        echo '队列CloudhouseJob';
        try {
            //消化队列
            print_r($this->params);

            foreach ($this->params as $p){
                //检索是否同步过
                $check_sku = Redis::Hget('push_custom_sku',$p);
                if($check_sku){
                    //同步过跳出此次循环
                    continue;
                }
                //检索有没有这个库存sku
                $se = DB::table('self_custom_sku')->where('custom_sku',$p)->orWhere('old_custom_sku',$p)->select('custom_sku','old_custom_sku','name','is_cloud','type')->get()->toArray();
                if(!empty($se)){
                    if($se[0]->is_cloud==0){
                        DB::table('self_custom_sku')->where('custom_sku',$se[0]->custom_sku)->orWhere('old_custom_sku',$se[0]->custom_sku)->update(['is_cloud'=>1]);
                        $params['type'] = $se[0]->type;
                        $params['name'] = $se[0]->name;
                        if($se[0]->old_custom_sku){
                            $params['custom_sku'] = $se[0]->old_custom_sku;
                        }else{
                            $params['custom_sku'] = $se[0]->custom_sku;
                        }
                        //执行推送
                        $res = $this->send_goods($params);
                        
                        print_r($res);
                        //curl成功响应时code=1
                        if($res['code']==1){
                            $check = CloudHouseController::xml_parser($res['data']);
                            if(!$check){
                                //返回的不是xml数据，请求失败，记录失败日志，加入失败队列
                                CloudHouseController::set_log($this->log_type,CloudHouseController::$fail,$res['data']);
                                Redis::Lpush('send_goods_fail',$p);
                            }else{
                                $data = $check;
                                //$data => 转为数组的xml数据
                                $log_data = json_encode($data,JSON_UNESCAPED_UNICODE);
                                //code=0表示推送成功
                                if($data['code']!=0){
                                    //推送失败加入失败队列，记录失败日志
                                    $redis['custom_sku'] = $p;
                                    $redis['name'] = $params['name'];
                                    CloudHouseController::set_log($this->log_type,CloudHouseController::$fail,$log_data);
                                    Redis::Lpush('send_goods_fail',$p);
                                }else{
                                    //推送成功修改推送状态，加入已同步队列
                                    Redis::Hset('push_custom_sku',$p,1);
                                    DB::table('cloudhouse_custom_sku')->where('custom_sku',$p)->update(['is_push'=>1]);
                                    //记录成功日志
                                    CloudHouseController::set_log($this->log_type,CloudHouseController::$success,$log_data);
                                }
                            }
                        }else{
                            //code=0执行出错，记录失败日志
                            CloudHouseController::set_log($this->log_type,CloudHouseController::$fail,$res['error']);
                            Redis::Lpush('send_goods_fail',$p);
                        }
                    }else{
                        print_r($se[0]->custom_sku).'已推送过';
                    }
                }else{
                    //没匹配到，记录失败日志
                    CloudHouseController::set_log($this->log_type,CloudHouseController::$fail,'没有匹配到'.$p);
                    Redis::Lpush('send_goods_fail',$p);
                }
            }
        } catch(\Exception $e){
            //消化失败，记录错误日志
            print_r($this->params);
            $errmsg = $e->getMessage();
            CloudHouseController::set_log($this->log_type,CloudHouseController::$fail,$errmsg);
            //加入失败队列
            foreach ($this->params as $p){
                Redis::Lpush('send_goods_fail',$p);
            }
        }
    }


    public function send_goods($params)
    {

        if($params['type']==2){
            $baseModel = new BaseModel();
            $son_custom_sku = $baseModel->GetGroupCustomSku($params['custom_sku']);
            if($son_custom_sku){
                foreach ($son_custom_sku as $sk=>$sv){
                    $son_custom_sku[$sk] = '1个'.$sv;
                }
            }
            $remark = implode(',',$son_custom_sku);
        }else{
            $remark = '';
        }

        $appKey = CloudHouseController::$appKey;

        $appSecret = CloudHouseController::$appSecret;

        //singleitem.synchronize=>商品同步
        //entryorder.create=>入库单创建
        //stockout.create=>出库单创建
        //order.cancel=>取消订单

        $paramArr = array(
            'app_key' => $appKey,
            'method' =>  $this->type,
            'format' => 'xml',
            'v' => '2.0',
            'sign_method' => 'md5',
            'timestamp' => date('Y-m-d H:i:s'),
            'customerId' => 'XM_QMESS_SKDYLB',
        );

        $sign = CloudHouseController::createSign($paramArr, $appSecret);
        //组参
        $strParam = CloudHouseController::createStrParam($paramArr);

        $strParam .= 'sign=' . $sign;

        //orderType=>出入库类型
        //JYCK=一般交易出库单;HHCK= 换货出库;BFCK=补发出库;PTCK=普通出库单;DBCK=调拨出库;B2BRK=B2B入库;B2BCK=B2B出库;
        //QTCK=其他出库;SCRK=生产入库;LYRK=领用入库;CCRK=残次品入库;CGRK=采购入库;DBRK= 调拨入库;QTRK=其他入库;
        //XTRK= 销退入库;THRK=退货入库;HHRK= 换货入库;CNJG= 仓内加工单;CGTH=采购退货出库单


        $xml_data = '<?xml version="1.0" encoding="UTF-8"?>
                         <request>
                            <actionType>add</actionType>
                            <warehouseCode>XMN03</warehouseCode>
                            <ownerCode>SKD</ownerCode>
                            <item>
                            <itemCode>' . $params['custom_sku'] . '</itemCode>
                            <barCode>' . $params['custom_sku'] . '</barCode>
                            <itemId/>
                            <itemName>' . $params['name'] . '</itemName>
                            <itemType>ZC</itemType>
                            <remark>'.$remark.'</remark>
                            </item>
                        </request>';

        $xml_data = str_replace(PHP_EOL, '', $xml_data);
        //发起请求
        $url = CloudHouseController::$link . $strParam;

        try{
            $result = CloudHouseController::curl_xml($url, $xml_data);
            return ['code'=>1,'data'=>$result,'error'=>''];
        }catch(\Exception $e){
            return ['code'=>0,'data'=>'','error'=>$e];
        }
    }


}
