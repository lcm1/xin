<?php

namespace App\Jobs;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class SaveSysLogJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $params;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        //
        $this->params = $params;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $date = date('Y-m-d H:i:s', microtime(true));
            echo $date.' 队列save_sys_log';
            db::table('sys_log')->insert($this->params);
            echo ' 执行成功！';
        }catch (\Exception $e){
            //消化失败，记录错误日志
            $errmsg = $e->getMessage();
            $add['job'] = 'save_sys_log';
            $add['time'] = time();
            $add['msg'] =  $errmsg;
            $add['data'] = json_encode($this->params);
            Db::table('job_err_log')->insert($add);
        }
    }
}