<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class AlibabaJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $params;
    protected $type;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        //
        $this->params = $params;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        echo 'alibaba队列执行成功！';
        // var_dump($this->params);

        
        do {
            echo '执行队列'.$this->params."\n";
            $task = false;
            try {
                $do =  $this->RunJob();
            } catch (\Throwable $th) {
                echo $th->getFile().'--'.$th->getLine().'--'.$th->getMessage()."\n";
            }
            if($do==1){
                //执行完毕  验证队列key
                $key = redis::get('queue_task:alibaba');
                if($key==$this->params){
                    $task = true;
                }
            }
        } while ($task);

        // if(isset($data['data']['result'])){
        //     $orders = $data['data']['result'];
        //     var_dump($orders);
        // }
     
    }

    public function RunJob(){


        $list = db::table('cloudhouse_contract_total')->where('contract_status','>=',2)->where('is_end',0)->where('purchase_order_no','!=','')->get();
        foreach ($list as $v) {
                // $job::runJob(['order_id'=>$v->order_id]);
                try {
                    //code...
                    $this->savedata($v->purchase_order_no);
                } catch (\Throwable $th) {
                    $errmsg = $th->getMessage().$th->getLine().$th->getFile();
                    $add['job'] = 'alibaba';
                    $add['time'] = time();
                    $add['msg'] =  $errmsg;
                    $add['data'] = $v->order_id;
                    Db::table('job_err_log')->insert($add);
                }
        }

    
        echo '全部结束，休眠半小时'."\n";
        sleep(21600);
        return 1;
    }

    public function savedata($order_id){
        $FpxModel = new \App\Models\FpxModel();
        try {
            //code...
            $data = $FpxModel->AliBaBa(['order_id'=>$order_id]);
            // var_dump($data);
        } catch (\Throwable $th) {
            //throw $th;
            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            return;
        }

        if($data['type']=='fail'){
            var_dump($data);
            return;
        }



        if($data['type']=='success'){
            $order = $data['data'];
            $item = [];
            $base = [];
            $logistics = [];

            $i['order_id'] = $order_id;
            $i['create_time'] = date('Y-m-d H:i:s',time());
            $i['status'] = 0;

            if(isset( $order['productItems'])){
                $item = $order['productItems'];
            }

            if(isset( $order['baseInfo'])){
                $base =  $order['baseInfo'];
                $i['payTime'] = $base['payTime']??'';
                $i['subBuyerLoginId'] = $base['subBuyerLoginId']??'';
                $i['sumProductPayment'] = $base['sumProductPayment']??'';
                $i['toFullName'] = $base['receiverInfo']['toFullName']??'';
            }

            if(isset($order['nativeLogistics']['logisticsItems'][0])){
                $logistics  = $order['nativeLogistics']['logisticsItems'][0];
                $i['logisticsCode'] = $logistics['logisticsCode']??'';
                $i['logisticsCompanyName'] = $logistics['logisticsCompanyName']??'';
                $i['logisticsBillNo'] = $logistics['logisticsBillNo']??'';
                $i['status'] =1;
            }



            // var_dump($item);

            foreach ($item  as $iv) {
                # code...
                $orders = db::table('alibaba_order_item')->where('order_id',$order_id)->where('skuID',$iv['skuID'])->first();


                $i['quantity'] = $iv['quantity']??0;
                $i['statusStr'] = $iv['statusStr']??'';
                $i['name'] = $iv['name']??'';
                $i['productID'] = $iv['productID']??'';
                $i['price'] = $iv['price']??0;
                $i['itemAmount'] = $iv['itemAmount']??0;
                $i['skuID'] = $iv['skuID']??0;
                $i['productImgUrl'] = $iv['productImgUrl'][0]??'';


                // var_dump( $i);
                if($orders){
                    db::table('alibaba_order_item')->where('id',$orders->id)->update($i);
                }else{
                    db::table('alibaba_order_item')->insert($i);
                }

            }



 
            $ldata = $FpxModel->AliBaBaLogistics(['order_id'=>$order_id]);

            db::table('alibaba_order_item')->where('order_id',$order_id)->update(['Logistics'=>json_encode($ldata)]);
           

        }




        // $orders = db::table('alibaba_order')->where('order_id',$order_id)->first();
        // $i['order_id'] = $order_id;
        // $i['create_time'] = date('Y-m-d H:i:s',time());
        // $i['status'] = 0;
        // if($data['type']=='success'){
        //     $order = $data['data'];
        //     $i['other'] = json_encode($order);
        //     if(isset($order['nativeLogistics']['logisticsItems'][0])){
        //         $logistics = $order['nativeLogistics']['logisticsItems'][0];
        //         $i['logisticsCode'] = $logistics['logisticsCode'];
        //         $i['logisticsCompanyName'] = $logistics['logisticsCompanyName'];
        //         $i['logisticsBillNo'] = $logistics['logisticsBillNo'];
        //         $i['status'] =1;
        //     }

        // }

        // if($orders){
        //     db::table('alibaba_order')->where('order_id',$order_id)->update($i);
        // }else{
        //     db::table('alibaba_order')->insert($i);
        // }

    }
}