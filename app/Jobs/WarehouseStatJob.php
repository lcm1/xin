<?php

namespace App\Jobs;

use App\Models\WarehouseStatDayModel;
use App\Models\WarehouseStatMonthModel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class WarehouseStatJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $params;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        $this->params = $params;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $type = $this->params['type'];

        switch ($type) {
            case 'day':
                $model = new WarehouseStatDayModel();
                break;
            case 'month':
                $model = new WarehouseStatMonthModel();
                break;
            default:
                echo '未知参数,程序结束';
                exit;
        }

        try {
            $model->add();
            echo '执行成功';
        } catch (\Exception $e) {
            //记录错误日志
            $add['job']  = 'WarehouseStat';
            $add['time'] = time();
            $add['msg']  = $e->getMessage();
            $add['data'] = json_encode($this->params);
            Db::table('job_err_log')->insert($add);
        }
    }
}
