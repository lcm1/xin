<?php

namespace App\Jobs;

use App\Models\WalmartOrderModel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class UpdateOrderSku implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $params;

    /**
     * 更新定单渠道sku
     *
     * @return void
     */
    public function __construct($params)
    {
        $this->params = $params;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $date = date('Y-m-d H:i:s', microtime(true));
            echo $date . ' UpdateOrderSku';
            $this->updateOrderSku();
            echo ' 执行成功！';
        } catch (\Exception $e) {
            //消化失败，记录错误日志
            $add['job']  = 'UpdateOrderSku';
            $add['time'] = time();
            $add['msg']  = $e->getMessage();
            $add['data'] = json_encode($this->params);
            Db::table('job_err_log')->insert($add);
        }
    }

    public function updateOrderSku()
    {
        //沃尔玛订单
        $walmartOrders = db::table('walmart_order_item')
            ->select('id', 'sku')
            ->where('custom_sku_id', 0)
            ->orWhere('spu_id', 0)
            ->orWhere('sku_id', 0)
            ->orWhere('spu', '')
            ->get();

        foreach ($walmartOrders as $v) {

            $sku = db::table('self_sku')
                ->select('id', 'spu', 'spu_id', 'custom_sku_id')
                ->where('sku', $v->sku)
                ->orWhere('old_sku', $v->sku)
                ->first();

            if ($sku) {
                db::table('walmart_order_item')
                    ->where('id', $v->id)
                    ->update([
                        'spu'           => $sku->spu,
                        'spu_id'        => $sku->spu_id,
                        'custom_sku_id' => $sku->custom_sku_id,
                        'sku_id'        => $sku->id,
                    ]);
            }
        }

        //亚马逊订单
//        $amazonOrders = db::table('amazon_order_item')
//            ->select('id', 'seller_sku')
//            ->where('custom_sku_id', 0)
//            ->orWhere('spu_id', 0)
//            ->orWhere('sku_id', 0)
//            ->orWhere('spu', '')
//            ->get();
//
//        foreach ($amazonOrders as $v) {
//
//            $sku = db::table('self_sku')
//                ->select('id', 'spu', 'spu_id', 'custom_sku_id')
//                ->where('sku', $v->seller_sku)
//                ->orWhere('old_sku', $v->seller_sku)
//                ->first();
//
//            if ($sku) {
//                db::table('amazon_order_item')
//                    ->where('id', $v->id)
//                    ->update([
//                        'spu'           => $sku->spu,
//                        'spu_id'        => $sku->spu_id,
//                        'custom_sku_id' => $sku->custom_sku_id,
//                        'sku_id'        => $sku->id,
//                    ]);
//            }
//        }

        //lazada订单
//        $lazadaOrder = db::table('lazada_order_item')
//            ->select('id', '')



    }


}
