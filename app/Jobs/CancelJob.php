<?php

namespace App\Jobs;

use App\Http\Controllers\Jobs\CloudHouseController;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;


class CancelJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $params;
    protected $type;
    protected $log_type;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        //
        $this->params = $params;
        $this->type = 'order.cancel';
        $this->log_type = 6;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        echo '队列CancelJob';
        $json['order_no'] = $this->params['order_no'];
        $json['order_type'] = $this->params['order_type'];
        try {
            //消化队列
            print_r($this->params);
            $res = $this->order_cancel($this->params);
            if($res['code']==1){
                //curl成功响应时code=1
                $check = CloudHouseController::xml_parser($res['data']);
                if(!$check){
                    //返回的不是xml数据，推送失败记录失败日志，加入失败队列
                    CloudHouseController::set_log($this->log_type,CloudHouseController::$fail,$res['data']);
                    Redis::Lpush('order_cancel_fail',$this->params['order_no']);
                }else{
                    $data = $check;
                    //$data => 转为数组的xml数据
                    $log_data = json_encode($data,JSON_UNESCAPED_UNICODE);
                    //code=0表示推送成功
                    if($data['flag']=='success'){
                        if($this->params['order_type']=='CGRK'||$this->params['order_type']=='DBRK'||$this->params['order_type']=='DBCK'||$this->params['order_type']=='PTCK'){
                            DB::table('goods_transfers')->where('order_no',$this->params['order_no'])->update(['is_push'=>-1]);
                        }
                        if($this->params['order_type']=='B2BCK'){
                            //修改计划状态，删除任务
                            DB::table('amazon_buhuo_request')->where('id', '=', $this->params['order_no'])->update(['request_status' => 1]);
                            $task_id = DB::table('tasks')->where('ext', 'like', "%$this->params['order_no']%")->select('Id','class_id')->first();
                            if(!empty($task_id)){
                                $delete1 =  DB::table('tasks')->where('Id', '=', $task_id->Id)->update(['is_delete' => 1]);
                            }
                        }
                        CloudHouseController::set_log($this->log_type,CloudHouseController::$success,$log_data);
                    }else{

                        if($this->params['order_type']=='CGRK'||$this->params['order_type']=='DBRK'||$this->params['order_type']=='DBCK'||$this->params['order_type']=='PTCK'){

                            //is_push=>-2.取消失败 -1已取消 0.推送失败 1.待推送 2.推送成功 3.已扫描入库
                            //status=>0.取消 1.正常 2.待取消
                            DB::table('goods_transfers')->where('order_no',$this->params['order_no'])->update(['is_push'=>-2]);
                        }
                        CloudHouseController::set_log($this->log_type,CloudHouseController::$fail,$log_data);
                        Redis::Lpush('order_cancel_fail',json_encode($json));
                    }
                }
            }else{
                if($this->params['order_type']=='CGRK'||$this->params['order_type']=='DBRK'||$this->params['order_type']=='DBCK'||$this->params['order_type']=='PTCK'){
                    DB::table('goods_transfers')->where('order_no',$this->params['order_no'])->update(['is_push'=>-2]);
                }
                CloudHouseController::set_log($this->log_type,CloudHouseController::$fail,$res['error']);
                Redis::Lpush('order_cancel_fail',json_encode($json));
            }
        } catch(\Exception $e){
            //消化失败，记录错误日志
            print_r($this->params);
            if($this->params['order_type']=='CGRK'||$this->params['order_type']=='DBRK'||$this->params['order_type']=='DBCK'||$this->params['order_type']=='PTCK'){
                DB::table('goods_transfers')->where('order_no',$this->params['order_no'])->update(['is_push'=>-2]);
            }
            Redis::Lpush('order_cancel_fail',json_encode($json));
            $errmsg = $e->getMessage();
            CloudHouseController::set_log($this->log_type,CloudHouseController::$fail,$errmsg);
        }
    }


    public function order_cancel($params)
    {
        $appKey = CloudHouseController::$appKey;

        $appSecret = CloudHouseController::$appSecret;

        //singleitem.synchronize=>商品同步
        //entryorder.create=>入库单创建
        //stockout.create=>出库单创建
        //order.cancel=>取消订单
        $paramArr = array(
            'app_key' => $appKey,
            'method' => $this->type,
            'format' => 'xml',
            'v' => '2.0',
            'sign_method' => 'md5',
            'timestamp' => date('Y-m-d H:i:s'),
            'customerId' => 'XM_QMESS_SKDYLB',
        );

        $sign = CloudHouseController::createSign($paramArr, $appSecret);
        //组参
        $strParam = CloudHouseController::createStrParam($paramArr);

        $strParam .= 'sign=' . $sign;

        //orderType=>出入库类型
        //JYCK=一般交易出库单;HHCK= 换货出库;BFCK=补发出库;PTCK=普通出库单;DBCK=调拨出库;B2BRK=B2B入库;B2BCK=B2B出库;
        //QTCK=其他出库;SCRK=生产入库;LYRK=领用入库;CCRK=残次品入库;CGRK=采购入库;DBRK= 调拨入库;QTRK=其他入库;
        //XTRK= 销退入库;THRK=退货入库;HHRK= 换货入库;CNJG= 仓内加工单;CGTH=采购退货出库单

        $xml_data = '<?xml version="1.0" encoding="UTF-8"?>
                        <request>
                            <warehouseCode>XMN03</warehouseCode> 
                            <ownerCode>SKD</ownerCode>           
                            <orderCode>'.$params['order_no'].'</orderCode>
                            <orderType>'.$params['order_type'].'</orderType>
                        </request>';

        $xml_data = str_replace(PHP_EOL, '', $xml_data);

        //发起请求
        $url = CloudHouseController::$link . $strParam;
        try{
            $result = CloudHouseController::curl_xml($url, $xml_data);
            return ['code'=>1,'data'=>$result,'error'=>''];
        }catch(\Exception $e){
            return ['code'=>0,'data'=>'','error'=>$e];

        }

    }


}
