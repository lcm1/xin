<?php

namespace App\Jobs;

use App\Models\AmazonPlaceOrderContractModel;
use App\Models\BaseModel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class SaveContractPdfJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $params;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        //
        $this->params = $params;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $params = $this->params;
        echo $params['contract_no'].$params['type'].'进入队列SaveContractPdfJob';
//         var_dump($this->params);exit();
        try {
            $insert['name'] = $params['title'];
            $insert['type'] = $params['type'];
            $insert['user_id'] = $params['user_id'];
            $insert['create_time'] = date('Y-m-d H:i:s', time());
            $insert['status'] = 0;
            $insert['msg'] = '生成中';
            $insert['path'] = '';
            $insert['download_type'] = 2;

            $task_id = Db::table('excel_task')->insertGetId($insert);

            $params['task_id'] = $task_id;
            //消化队列
            $this->exportPlaceOrderContractDetail($params);
        } catch (\Exception $e) {

            //消化失败，记录错误日志
            $errmsg = $e->getMessage() . 'line:' . $e->getLine();
            // $errmsg = json_encode($e);
            $add['job'] = 'save_contract_pdf';
            $add['time'] = time();
            $add['msg'] = $errmsg;
            $add['data'] = json_encode($this->params);
            Db::table('job_err_log')->insert($add);
            echo '队列错误！'.$e->getMessage();
            exit();
        }

    }

    protected function _savePdf($params)
    {
        try {
            require_once(app_path() . DIRECTORY_SEPARATOR . 'Libs' . DIRECTORY_SEPARATOR . 'tcpdf' . DIRECTORY_SEPARATOR . 'examples' . DIRECTORY_SEPARATOR . 'tcpdf_include.php');
            $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

            // 设置文档信息
            $pdf->SetCreator('L');
            $pdf->SetAuthor('L');
            $pdf->SetTitle($params['title']);
            $pdf->SetSubject($params['title']);
            $pdf->SetKeywords('TCPDF, PDF, PHP');

            // 设置页眉和页脚信息
            $pdf->SetHeaderData('', 0, '', '', [0, 0, 0], [255, 255, 255]);
            $pdf->setFooterData([0, 0, 0], [255, 255, 255]);

            // 设置页眉和页脚字体

            $pdf->setHeaderFont(['stsongstdlight', '', '15']);
            $pdf->setFooterFont(['stsongstdlight', '', '15']);

            //删除预定义的打印 页眉/页尾
//            $pdf->setPrintHeader($result['is_header'] ?? false);
//            $pdf->setPrintFooter($result['is_footer'] ?? false);

            // 设置默认等宽字体
            $pdf->SetDefaultMonospacedFont('courier');

            // 设置字体
//            $pdf->setFont('times');

            // 设置间距
            $pdf->SetMargins(10, 10, 10);//页面间隔
            $pdf->SetHeaderMargin(10);//页眉top间隔
            $pdf->SetFooterMargin(10);//页脚bottom间隔

            // 设置分页
            $pdf->SetAutoPageBreak(TRUE, 15);
            $pdf->setFontSubsetting(true);

            // set default font subsetting mode
            $pdf->setFontSubsetting(true);

            //设置字体 stsongstdlight支持中文
            $pdf->SetFont('stsongstdlight', '', 15);

            $pdf->Ln(10);

            // 设置密码
//            $pdf->SetProtection($permissions = array('print', 'modify', 'copy', 'annot-forms', 'fill-forms', 'extract', 'assemble', 'print-high'), $user_pass = '123456', $owner_pass = null, $mode = 0, $pubkeys = null );

            //第一页
            $pdf->AddPage();
            $pdf->writeHTML($params['html'], true, false, true, true, 'center');

//            for ($i = 1; $i <= $pdf->getNumPages(); $i++) {
//                $pdf->setPage($i);
//                $pdf->Image(public_path('/mallWeb/images/official.png'), 140, 220, 50, 43, '', '', '', false, 168, '', false, false, false, false, false, false);
//                $pdf->writeHTMLCell(0, 0, '', '', $header, 0, 1, 0, true, 'R', true);
//            }
            //输出PDF
            $path = 'pdf' . DIRECTORY_SEPARATOR . $params['title'] . $params['contract_no'] . '.pdf';
            $pdf->Output(public_path() . DIRECTORY_SEPARATOR . $path, 'F');//I输出、D下载
            Db::table('excel_task')->where('id', $params['task_id'])->update([
                'status' => 1,
                'msg' => '打印成功！',
                'path' => $path
            ]);
            // 更新下载地址
            $contract = db::table('cloudhouse_contract_total')
                ->where('contract_no', $params['contract_no'])
                ->first();
            $pdfFile = json_decode($contract->pdf_file, true);
            $pdfFile[$params['print_type']] = $path;
            $update = db::table('cloudhouse_contract_total')
                ->where('contract_no', $params['contract_no'])
                ->update([
                    'pdf_file' => json_encode($pdfFile)
                ]);
            return;
        } catch (\Exception $e) {
            throw new \Exception($e);
            return;
        }
    }
    public function exportPlaceOrderContractDetail($params)
    {
        try {

            $contractModel = new AmazonPlaceOrderContractModel();
            $contractDetail = $contractModel->getAmazonPlaceOrderContractDetail(['contract_no' => $params['contract_no']])['data'] ?? [];
            $rule = [];
            foreach ($contractDetail->custom_sku_items as $cus_item){
                $rule[] = $cus_item['size'];
            }
            $rule = array_unique($rule);
            $length = count($rule);
            $width = 9 - $length;
            $priceWidth = 1;
            $memoWidth = 1;
            if ($width > 2){
                if ($width%2 == 1){
                    $priceWidth = bcdiv($width, 2) + 1;
                    $memoWidth = bcdiv($width, 2);
                }
                if ($width%2 == 0){
                    $priceWidth = bcdiv($width, 2);
                    $memoWidth = bcdiv($width, 2);
                }
            }
            $baseModel = new BaseModel();
            $process_contract_no = '';
            $process_supplier_name = '';
            if ($contractDetail->contract_class == 5){
                $process_contract_no = '实际外发加工合同号：'.$contractDetail->process_contract_no;
                $process_supplier_name = '实际外发加工工厂：'.$contractDetail->process_supplier_name;
            }
            switch ($params['print_type']) {
                case 1:
                    $printType = '购销合同-服装范本(不含税)-打印';
                    $html = '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table {
            border-collapse: collapse;
        }
        td, th {
            padding: 0;
        }
        table {
            border-collapse: separate;
            border-spacing: 0;
        }
        td,tr {
            font-size: 9px;
            line-height：9px;
        }
        td{
            width: 5%;
        }
        .border1{
            border-width: 0.5rem;
            border-style: double double double double;
            border-color: black;
            height: 8px;
        }
        .fontBlod{
            font-weight: bold;
        }
        .cell {
            display: flex;
	        justify-content: center;
	        align-items: center;
        }
    </style>
</head>
<body>
    <table   width="100%" cellpadding="2" cellspacing="0">
        <tr>
            <td colspan="20" style="text-align: center;font-size: 20px; font-weight: bold; padding-bottom: 20px;"><div class="cell">'.$contractDetail->company_name.'购销合同</div></td>
        </tr>
        <tr>
            <td colspan="20"></td>
        </tr>
        <tr>
            <td colspan="14"><div class="cell">甲方单位：'.$contractDetail->company_name.'(需方)</div></td>
            <td colspan="6"><div class="cell">合同编号：'.$contractDetail->contract_no.'</div></td>
        </tr>
        <tr>
            <td colspan="14"><div class="cell">乙方单位：'.$contractDetail->supplier_name.'(供方)</div></td>
            <td colspan="6"><div class="cell">签约时间：'.$contractDetail->sign_date.'</div></td>
        </tr>
        <tr>
            <td colspan="14"></td>
            <td colspan="6"><div class="cell">签约地点：'.$contractDetail->signing_location.'</div></td>
        </tr>
        <tr>
            <td colspan="14"></td>
            <td colspan="6"><div class="cell">交期时间：'.$contractDetail->report_date.'</div></td>
        </tr>
        <tr>
            <td colspan="20" style="text-align: center;"></td>
        </tr>
        <tr>
            <td colspan="20" style="text-align: left; font-weight: bold;"><div class="cell">为促进甲方、乙方双方合作顺利开展，保障双方权益，根据《中华人民共和国民法典》及相关法律法规的规定，经双方友好协商特订立本合同，供共同遵守。</div></td>
        </tr>
        <tr>
            <td colspan="20" style="text-align: center;"></td>
        </tr>
        <tr>
            <td colspan="20"><div class="cell">一、 1、甲方商品的款号/品名，数量，单价，金额（人民币）</div></td>
        </tr>

        <tr>
        <td class="border1" colspan="3" rowspan="2" style="text-align: center;"><div class="cell">款号</div></td>
            <td class="border1" colspan="2" rowspan="2" style="text-align: center;"><div class="cell">款式</div></td>
            <td class="border1" colspan="2" rowspan="2" style="text-align: center;"><div class="cell">品名</div></td>
            <td class="border1" colspan="'.($length+1).'" rowspan="1" style="text-align: center;"><div class="cell">颜色尺码</div></td>
            <td class="border1" colspan="1" rowspan="2" style="text-align: center;"><div class="cell">合计(件/套)</div></td>
            <td class="border1" colspan="1" rowspan="2" style="text-align: center;"><div class="cell">单价(元)(不含税)</div></td>
            <td class="border1" colspan="'.$priceWidth.'" rowspan="2" style="text-align: center;"><div class="cell">金额(不含税)</div></td>
            <td class="border1" colspan="1" rowspan="2" style="text-align: center;"><div class="cell">吊牌</div></td>
            <td class="border1" colspan="'.$memoWidth.'" rowspan="2" style="text-align: center;"><div class="cell">备注</div></td>
        </tr>';

                    $html .= '<tr>
            <td class="border1" colspan="1" rowspan="1" style="text-align: center;">颜色</td>';

                    foreach ($rule as $size){
                        $html .= '
            <td class="border1" colspan="1" style="text-align: center;">'.$size.'</td>';
                    }

                    $html .= '
        </tr>';
                    $contractSpu = $contractDetail->spu_items;
                    $sizeInfo = [];
                    $totalPrice = 0;
                    foreach ($contractSpu as $v){
                        $html .= '<tr>
                <td class="border1" colspan="3" style="text-align: center;">'.$v['spu'].'</td>';
                        if (empty($v['img'])){
                            $html .= '<td class="border1" colspan="2" style="text-align: center;"></td>';
                        }else{
                            $html .= '<td class="border1" colspan="2" style="text-align: center;"><img src="' . $v['img'] . '" width="70pt"></td>';
                        }
                $html .= '<td class="border1" colspan="2" style="text-align: center;">'.$v['title'].'</td>
                <td class="border1" colspan="1" style="text-align: center;">'.$v['color_name'].'/'.$v['color_identifying'].'</td>';
                        $numInfo = $v['num_info'];
                        foreach ($rule as $size){
                            $num = $numInfo->$size ?? 0;
                            if (isset($sizeInfo[$size])){
                                $sizeInfo[$size] += $num;
                            }else{
                                $sizeInfo[$size] = $num;
                            }
                            $totalPrice += $v['one_price'] * $num;
                            $html .= '<td class="border1" colspan="1" style="text-align: center;">'.$num.'</td>';
                        }
                        $html .= '<td class="border1" colspan="1" rowspan="1" style="text-align: center;"><div class="cell">'.$v['count'].'</div></td>
                <td class="border1" colspan="1" rowspan="1" style="text-align: center;"><div class="cell">'.$v['one_price'].'</div></td>
                <td class="border1" colspan="'.$priceWidth.'" rowspan="1" style="text-align: center;"><div class="cell">'.$v['price'].'</div></td>
                <td class="border1" colspan="1" style="text-align: center;">'.$contractDetail->brand.'</td>
                <td class="border1" colspan="'.$memoWidth.'" style="text-align: center;">'.$v['memo'].'</td>
            </tr>';
                    }
                    $html .= '<tr>
            <td class="border1" colspan="7" style="text-align: center;">合计金额</td>
            <td class="border1" colspan="1" style="text-align: center;"></td>';
                    foreach ($rule as $size){
                        $num = $sizeInfo[$size] ?? 0;
                        $html .= '
            <td class="border1" colspan="1" style="text-align: center;">'.$num.'</td>';
                    }

                    $html .= '
            <td class="border1" colspan="1" style="text-align: center;">'.array_sum($sizeInfo).'</td>
            <td class="border1" colspan="1" style="text-align: center;"></td>
            <td class="border1" colspan="'.$priceWidth.'" style="text-align: center;">'.$baseModel->floorDecimal($totalPrice).'</td>
            <td class="border1" colspan="1" style="text-align: center;"></td>
            <td class="border1" colspan="'.$memoWidth.'" style="text-align: center;"></td>
        </tr>';
                    $payment_information = $contractDetail->payment_information;
                    $count = 4;
                    if (count($payment_information) > $count){
                        $count = count($payment_information);
                    }
                    for ($i = 0; $i<$count; $i++){
                        $title = $payment_information[$i]['title'] ?? '';
                        $money = $payment_information[$i]['money'] ?? '';
                        $html .= '<tr>
                <td class="border1" colspan="7" style="text-align: center;">'.$title.'</td>
                <td class="border1" colspan="1" style="text-align: center;"></td>';

                        foreach ($rule as $size){
                            $html .= '<td class="border1" colspan="1" style="text-align: center;"></td>';
                        }

                        $html .= '
                <td class="border1" colspan="1" style="text-align: center;"></td>
                <td class="border1" colspan="1" style="text-align: center;"></td>
                <td class="border1" colspan="'.$priceWidth.'" style="text-align: center;">'.$money.'</td>
                <td class="border1" colspan="1" style="text-align: center;"></td>
                <td class="border1" colspan="'.$memoWidth.'" style="text-align: center;"></td>
            </tr>';
                    }
                    $html .= '<tr>
            <td colspan="20" style="text-align: center;"></td>
        </tr>

        <tr>
            <td colspan="20">2、需提供产前确认样（拍照样）齐色码'.implode('、', $rule).'各一件。</td>
        </tr>

        <tr>
            <td colspan="20">3、出货良品率保持99%，按合同规定的交货期交货，若逾期大于8天以上的，以规定合同交期而产生的交期逾期，以逾期货款金额为基数按每日万分之7.5计算违约金扣款，≥30天除了扣款还需承担甲方运费。</td>
        </tr>
        <tr>
            <td colspan="20">（如有其他原因延期交货，双方友好协商交货日期，并以书面或者邮件给以通知，须甲方同意为准，不可延期后再进行延期补签申请）。</td>
        </tr>

        <tr>
            <td colspan="20">二、质量要求技术标准、乙方对质量负责的条件:</td>
        </tr>
        <tr>
            <td colspan="20">1、产品质量和材料以需方提供的原样一致及双方最终签章确认产前样为准！如有不一致，甲方有权取消本次合同。</td>
        </tr>
        <tr>
            <td colspan="20">2、必须符合国家法定商检标准及甲方提出的质量标准,按照AQL2.5质量标准进行验货。</td>
        </tr>
        <tr>
            <td colspan="20">三、交货地点、方式及运输费用</td>
        </tr>
        <tr>
            <td colspan="20">1、须按甲方提供的交货期及指定的地点交货，我方地址分别为：厦门同安仓、泉州仓、厦门软二仓。</td>
        </tr>
        <tr>
            <td colspan="20">2、乙方按甲方指定地点送货，在保证交货期情况下乙方可自主选择运输方式，运输费用乙方承担。</td>
        </tr>
        <tr>
            <td colspan="20">四、包装标准：按我司要求包装（详细按理单资料）</td>
        </tr>
        <tr>
            <td colspan="20">五、验收标准、方法及提出异议期限：</td>
        </tr>
        <tr>
            <td colspan="20">1、甲方派出的QC以确认样和国家法定商检标准为准，对产品的品质、规格、数量、包装等外观质检验。乙方须严格按甲方要求执行。</td>
        </tr>
        <tr>
            <td colspan="20">2、 按确认样和制作单要求生产。乙方未能按质按量按时交货引起的损失，概由乙方承担。</td>
        </tr>
        <tr>
            <td colspan="20">3、甲方在收货后有权对因工厂生产技术、材料等方面造成的质量问题提出索赔、严重者可提出退货及索赔，乙方应予以接受。</td>
        </tr>
        <tr>
            <td colspan="20">4、大货只接受订单数量±3%（按各码数）。</td>
        </tr>
        <tr>
            <td colspan="20">六、货款的计算及结算方式：</td>
        </tr>
        <tr>
            <td colspan="20">1、付款方式：T/T</td>
        </tr>';

                    $html .= '
        <tr>
            <td colspan="20">'.$contractDetail->payment_settlement_method.'</td>
        </tr>';

                    $html .= '<tr>
            <td colspan="20">七、违约责任：</td>
        </tr>
        <tr>
            <td colspan="20">2、乙方所交货品与本合同规定质量不符导致甲方在转售该批服装时被客人拒绝接收或降价或其它救济方式时，甲方有权对乙方采取相应的措施，乙方不得拒绝。</td>
        </tr>
        <tr>
            <td colspan="20">3、因乙方不能按期交货，甲方不同意延期所引起的各项运费或制约款等费用由乙方承担。</td>
        </tr>
        <tr>
            <td colspan="20">八、解决合同纠纷方式：</td>
        </tr>
        <tr>
            <td colspan="20">合同履行过程中若发生纠纷，经友好协商未果提起诉讼，由签约地（厦门思明区）法院管辖。</td>
        </tr>
        <tr>
            <td colspan="20">九、其它约定事项:</td>
        </tr>
        <tr>
            <td colspan="20">1、由甲方提供的理单资料（即大货要求）均为本合同附件，其它未尽事宜可经双方商定进行书面修改补充并视为本合同不可分割的部分，同样具有法律效力。</td>
        </tr>
        <tr>
            <td colspan="20">2、大货材料必须由甲方确认后才可生产，否则由此引起的责任及后果由乙方承担;</td>
        </tr>
        <tr>
            <td colspan="20">3、甲方品牌产品、组合方式、设计款式、包装方式等待均不可销售其他电商卖家。如发现乙方向其他电商卖家销售以上规定的产品造成甲方损失应由乙方承担。</td>
        </tr>
        <tr>
            <td colspan="20">4、如果是因为乙方质量等原因，造成甲方派QC前往乙方工厂验货超过2次的，所产生的验货费用由乙方承担。 （根据验货地点费用起：500元-1000元以上）</td>
        </tr>
        <tr>
            <td colspan="20">5、乙方须在交货前免费提供符合甲方质量要求的初样，修改样，产前样，大货样，若不符合要求，须重新提供。</td>
        </tr>
        <tr>
            <td colspan="20">6、乙方必须做产前样给甲方确认后，方可开裁&生产大货，否则后果将由乙方负全部责任。</td>
        </tr>
        <tr>
            <td colspan="10" rowspan="1">'.$process_contract_no.'</td>
            <td colspan="10" rowspan="1">'.$process_supplier_name.'</td>
        </tr>
        <tr>
            <td colspan="20" rowspan="1"></td>
        </tr>
        <tr>
            <td colspan="20" rowspan="1"></td>
        </tr>
        <tr>
            <td colspan="20" rowspan="1"></td>
        </tr>
        <tr>
            <td colspan="2">甲方单位：</td>
            <td colspan="8">'.$contractDetail->company_name.'</td>
            <td colspan="2">乙方单位：</td>
            <td colspan="8">'.$contractDetail->supplier_name.'</td>
        </tr>
        <tr>
            <td colspan="2">地址：</td>
            <td colspan="8">'.$contractDetail->company_address.'</td>
            <td colspan="2">地址：</td>
            <td colspan="8">'.$contractDetail->supplier_address.'</td>
        </tr>
        <tr>
            <td colspan="10">甲方代表：'.$contractDetail->maker_name.'</td>
            <td colspan="10">乙方代表：</td>
        </tr>
        <tr>
            <td colspan="10">签章：</td>
            <td colspan="10">签章：</td>
        </tr>
        <tr>
            <td colspan="10">日期：'.$contractDetail->sign_date.'</td>
            <td colspan="10">日期：'.$contractDetail->sign_date.'</td>
        </tr>
    </table>
</body>
</html>';
                    break;
                case 2:
                    // 铁艺范本
                    $printType = '购销合同-铁艺范本(不含税)-打印';
                    $html = '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table {
            border-collapse: collapse;
        }
        td, th {
            padding: 0;
        }
        table {
            border-collapse: separate;
            border-spacing: 0;
        }
        td,tr {
            font-size: 9px;
            line-height：9px;
        }
        td{
            width: 5%;
        }
        .border1{
            border: 1rem black double;
            height: 8px;
        }
        .fontBlod{
            font-weight: bold;
        }
        .cell {
            display: flex;
	        justify-content: center;
	        align-items: center;
        }
    </style>
</head>
<body>
    <table   width="100%" cellpadding="2" cellspacing="0">
        <tr>
            <td colspan="20" style="text-align: center;font-size: 20px; font-weight: bold; padding-bottom: 20px;"><div class="cell">'.$contractDetail->company_name.'购销合同</div></td>
        </tr>
        <tr>
            <td colspan="20"></td>
        </tr>
        <tr>
            <td colspan="14"><div class="cell">甲方单位：'.$contractDetail->company_name.'</div></td>
            <td colspan="6"><div class="cell">合同编号：'.$contractDetail->contract_no.'</div></td>
        </tr>
        <tr>
            <td colspan="14"><div class="cell">乙方单位：'.$contractDetail->supplier_name.'</div></td>
            <td colspan="6"><div class="cell">签约时间：'.$contractDetail->sign_date.'</div></td>
        </tr>
        <tr>
            <td colspan="14"></td>
            <td colspan="6"><div class="cell">签约地点：'.$contractDetail->signing_location.'</div></td>
        </tr>
        <tr>
            <td colspan="14"></td>
            <td colspan="6"><div class="cell">交期时间：'.$contractDetail->report_date.'</div></td>
        </tr>
        <tr>
            <td colspan="20" style="text-align: center;"></td>
        </tr>
        <tr>
            <td colspan="20" style="text-align: center; font-weight: bold;"><div class="cell">依照国家有关法律法规，签约双方就采购出口货物有关事项协商一致，达成本合同。</div></td>
        </tr>
        <tr>
            <td colspan="20" style="text-align: center;"></td>
        </tr>
        <tr>
            <td colspan="20"><div class="cell">一、 1、甲方商品的款号/品名，数量，单价，金额（人民币）</div></td>
        </tr>

        <tr>
        <td class="border1" colspan="3" rowspan="2" style="text-align: center;"><div class="cell">款号</div></td>
            <td class="border1" colspan="2" rowspan="2" style="text-align: center;"><div class="cell">款式</div></td>
            <td class="border1" colspan="2" rowspan="2" style="text-align: center;"><div class="cell">品名</div></td>
            <td class="border1" colspan="'.($length+1).'" rowspan="1" style="text-align: center;"><div class="cell">颜色尺码</div></td>
            <td class="border1" colspan="1" rowspan="2" style="text-align: center;"><div class="cell">合计(件/套)</div></td>
            <td class="border1" colspan="1" rowspan="2" style="text-align: center;"><div class="cell">单价(元)(不含税)</div></td>
            <td class="border1" colspan="'.$priceWidth.'" rowspan="2" style="text-align: center;"><div class="cell">金额(不含税)</div></td>
            <td class="border1" colspan="1" rowspan="2" style="text-align: center;"><div class="cell">吊牌</div></td>
            <td class="border1" colspan="'.$memoWidth.'" rowspan="2" style="text-align: center;"><div class="cell">备注</div></td>
        </tr>';

                    $html .= '<tr>
            <td class="border1" colspan="1" rowspan="1" style="text-align: center;">颜色</td>';

                    foreach ($rule as $size){
                        $html .= '
            <td class="border1" colspan="1" style="text-align: center;">'.$size.'</td>';
                    }

                    $html .= '
        </tr>';

                    $contractSpu = $contractDetail->spu_items;
                    $sizeInfo = [];
                    $totalPrice = 0;
                    foreach ($contractSpu as $v){
                        $html .= '<tr>
                <td class="border1" colspan="3" style="text-align: center;">'.$v['spu'].'</td>';
                        if (empty($v['img'])){
                            $html .= '<td class="border1" colspan="2" style="text-align: center;"></td>';
                        }else{
                            $html .= '<td class="border1" colspan="2" style="text-align: center;"><img src="' . $v['img'] . '" width="70pt"></td>';
                        }
                        $html .= '<td class="border1" colspan="2" style="text-align: center;">'.$v['title'].'</td>
                <td class="border1" colspan="1" style="text-align: center;">'.$v['color_name'].'/'.$v['color_identifying'].'</td>';
                        $numInfo = $v['num_info'];
                        foreach ($rule as $size){
                            $num = $numInfo->$size ?? 0;
                            if (isset($sizeInfo[$size])){
                                $sizeInfo[$size] += $num;
                            }else{
                                $sizeInfo[$size] = $num;
                            }
                            $totalPrice += $v['one_price'] * $num;
                            $html .= '<td class="border1" colspan="1" style="text-align: center;">'.$num.'</td>';
                        }
                        $html .= '<td class="border1" colspan="1" rowspan="1" style="text-align: center;"><div class="cell">'.$v['count'].'</div></td>
                <td class="border1" colspan="1" rowspan="1" style="text-align: center;"><div class="cell">'.$v['one_price'].'</div></td>
                <td class="border1" colspan="'.$priceWidth.'" rowspan="1" style="text-align: center;"><div class="cell">'.$v['price'].'</div></td>
                <td class="border1" colspan="1" style="text-align: center;">'.$contractDetail->brand.'</td>
                <td class="border1" colspan="'.$memoWidth.'" style="text-align: center;">'.$v['memo'].'</td>
            </tr>';
                    }
                    $html .= '<tr>
            <td class="border1" colspan="7" style="text-align: center;">合计金额</td>
            <td class="border1" colspan="1" style="text-align: center;"></td>';
                    foreach ($rule as $size){
                        $num = $sizeInfo[$size] ?? 0;
                        $html .= '
            <td class="border1" colspan="1" style="text-align: center;">'.$num.'</td>';
                    }
                    $html .= '
            <td class="border1" colspan="1" style="text-align: center;">'.array_sum($sizeInfo).'</td>
            <td class="border1" colspan="1" style="text-align: center;"></td>
            <td class="border1" colspan="'.$priceWidth.'" style="text-align: center;">'.$baseModel->floorDecimal($totalPrice).'</td>
            <td class="border1" colspan="1" style="text-align: center;"></td>
            <td class="border1" colspan="'.$memoWidth.'" style="text-align: center;"></td>
        </tr>';
                    $payment_information = $contractDetail->payment_information;
                    $count = 4;
                    if (count($payment_information) > $count){
                        $count = count($payment_information);
                    }
                    for ($i = 0; $i<$count; $i++){
                        $title = $payment_information[$i]['title'] ?? '';
                        $money = $payment_information[$i]['money'] ?? '';
                        $html .= '<tr>
                <td class="border1" colspan="7" style="text-align: center;">'.$title.'</td>
                <td class="border1" colspan="1" style="text-align: center;"></td>';

                        foreach ($rule as $size){
                            $html .= '<td class="border1" colspan="1" style="text-align: center;"></td>';
                        }

                        $html .= '
                <td class="border1" colspan="1" style="text-align: center;"></td>
                <td class="border1" colspan="1" style="text-align: center;"></td>
                <td class="border1" colspan="'.$priceWidth.'" style="text-align: center;">'.$money.'</td>
                <td class="border1" colspan="1" style="text-align: center;"></td>
                <td class="border1" colspan="'.$memoWidth.'" style="text-align: center;"></td>
            </tr>';
                    }

                    $html .= '<tr>
            <td colspan="20" style="text-align: center;"></td>
        </tr>

        <tr>
            <td colspan="2">品牌：</td>
            <td colspan="8"></td>
            <td colspan="10">，需提供产前确认样（拍照样）齐色码数各一件</td>
        </tr>

        <tr>
            <td colspan="20">出货良品率保持99%，按合同规定的交货期交货，若逾期大于8天以上的，延一天按万分之7.5扣款，≥31天除了扣款还需承担空运费。</td>
        </tr>
        <tr>
            <td colspan="20">（如有其他原因延期交货，双方友好协商交货日期，并以书面或者邮件申请，甲方同意为准，不可延期后再进行延期补签申请）</td>
        </tr>

        <tr>
            <td colspan="20">二、质量要求技术标准、乙方对质量负责的条件: </td>
        </tr>
        <tr>
            <td colspan="20">产品质量和材料以需方提供的原样一致及双方最终签章确认产前样为准！如有不一致，甲方有权取消合同。 符合国家法定商检标准及甲方提出的质量标准,按照AQL2.5质量标准进行验货。</td>
        </tr>
        <tr>
            <td colspan="20">三、交货地点、方式及运输费用</td>
        </tr>
        <tr>
            <td colspan="20">1、按甲方提供的交货期及指定的地点交货，送货地址：厦门同安仓、泉州仓</td>
        </tr>
        <tr>
            <td colspan="20">2、乙方按甲方指定地点送货，在保证交货期情况下乙方可自主选择运输方式，运输费用乙方承担。</td>
        </tr>
        <tr>
            <td colspan="20">四、包装标准：按我司要求包装（详细按理单资料）</td>
        </tr>
        <tr>
            <td colspan="20">五、验收标准、方法及提出异议期限：</td>
        </tr>
        <tr>
            <td colspan="20">1、甲方派出的QC以确认样和国家法定商检标准为准，对产品的品质、规格、数量、包装等外观质检验。乙方须严格按确认样和制作单要求生产。乙方未能按质按量按时交货引起的损失，概由乙方承担。</td>
        </tr>
        <tr>
            <td colspan="20">2、甲方在收货后90天内有权对因工厂生产技术、材料等方面造成的质量问题提出索赔、严重者可提出退货及索赔，乙方应予接受。</td>
        </tr>
        <tr>
            <td colspan="20">六、货款的计算及结算方式：</td>
        </tr>
        <tr>
            <td colspan="20">1、付款方式：</td>
        </tr>';

                    $html .= '
        <tr>
            <td colspan="20">付款方式：'.$contractDetail->payment_settlement_method.'</td>
        </tr>';

                    $html .= '<tr>
            <td colspan="20">七、违约责任：</td>
        </tr>
        <tr>
            <td colspan="20">1、乙方所交货品与本合同规定质量不符导致甲方在转售该批服装时被客人拒绝接收或降价或其它救济方式时，甲方有权对乙方采取相应的措施，乙方不得拒绝。</td>
        </tr>
        <tr>
            <td colspan="20">2、因乙方不能按期交货，甲方不同意延期所引起的空运费或制约款等费用由乙方承担。</td>
        </tr>
        <tr>
            <td colspan="20">八、解决合同纠纷方式：</td>
        </tr>
        <tr>
            <td colspan="20">合同履行过程中若发生纠纷，经友好协商未果提起诉讼，由签约地法院管辖。</td>
        </tr>
        <tr>
            <td colspan="20">九、其它约定事项:</td>
        </tr>
        <tr>
            <td colspan="20">1、由甲方提供的理单资料（即大货要求）均为本合同附件，其它未尽事宜可经双方商定进行书面修改补充并视为本合同不可分割的部分，同样具有法律效力。</td>
        </tr>
        <tr>
            <td colspan="20">2、大货材料必须由甲方确认后才可生产，否则由此引起的责任及后果由乙方承担;</td>
        </tr>
        <tr>
            <td colspan="20">3、我司品牌产品、组合方式、设计款式、包装方式等待均不可销售其他电商卖家。如发现贵司向其他电商卖家销售以上规定的产品造成我司损失应由贵司承担。</td>
        </tr>
        <tr>
            <td colspan="20">4、如果是因为乙方质量等原因，造成甲方派QC前往乙方工厂验货超过2次的，所产生的费用由乙方承担。 （根据验货地点费用起：500元-1000元以上）</td>
        </tr>
        <tr>
            <td colspan="20">6、乙方必须做产前样给甲方确认后，方可开裁&生产大货，否则后果将由乙方负全部责任。</td>
        </tr>
        <tr>
            <td colspan="10" rowspan="1">'.$process_contract_no.'</td>
            <td colspan="10" rowspan="1">'.$process_supplier_name.'</td>
        </tr>
        <tr>
            <td colspan="20" rowspan="1"></td>
        </tr>
        <tr>
            <td colspan="20" rowspan="1"></td>
        </tr>
        <tr>
            <td colspan="20" rowspan="1"></td>
        </tr>
        <tr>
            <td colspan="2">甲方单位：</td>
            <td colspan="8">'.$contractDetail->company_name.'</td>
            <td colspan="2">乙方单位：</td>
            <td colspan="8">'.$contractDetail->supplier_name.'</td>
        </tr>
        <tr>
            <td colspan="2">地址：</td>
            <td colspan="8">'.$contractDetail->company_address.'</td>
            <td colspan="2">地址：</td>
            <td colspan="8">'.$contractDetail->supplier_address.'</td>
        </tr>
        <tr>
            <td colspan="10">甲方代表：'.$contractDetail->maker_name.'</td>
            <td colspan="10">乙方代表：</td>
        </tr>
        <tr>
            <td colspan="10">签章：</td>
            <td colspan="10">签章：</td>
        </tr>
        <tr>
            <td colspan="10">日期：'.$contractDetail->sign_date.'</td>
            <td colspan="10">日期：'.$contractDetail->sign_date.'</td>
        </tr>
    </table>
</body>
</html>';
                    break;
                case 3:
                    $printType = '购销合同-服装范本(含税)-打印';
                    $html = '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table {
            border-collapse: collapse;
        }
        td, th {
            padding: 0;
        }
        table {
            border-collapse: separate;
            border-spacing: 0;
        }
        td,tr {
            font-size: 9px;
            line-height：9px;
        }
        td{
            width: 5%;
        }
        .border1{
            border-width: 0.5rem;
            border-style: double double double double;
            border-color: black;
            height: 8px;
        }
        .fontBlod{
            font-weight: bold;
        }
        .cell {
            display: flex;
	        justify-content: center;
	        align-items: center;
        }
    </style>
</head>
<body>
    <table   width="100%" cellpadding="2" cellspacing="0">
        <tr>
            <td colspan="20" style="text-align: center;font-size: 20px; font-weight: bold; padding-bottom: 20px;"><div class="cell">'.$contractDetail->company_name.'购销合同</div></td>
        </tr>
        <tr>
            <td colspan="20"></td>
        </tr>
        <tr>
            <td colspan="14"><div class="cell">甲方单位：'.$contractDetail->company_name.'(需方)</div></td>
            <td colspan="6"><div class="cell">合同编号：'.$contractDetail->contract_no.'</div></td>
        </tr>
        <tr>
            <td colspan="14"><div class="cell">乙方单位：'.$contractDetail->supplier_name.'(供方)</div></td>
            <td colspan="6"><div class="cell">签约时间：'.$contractDetail->sign_date.'</div></td>
        </tr>
        <tr>
            <td colspan="14"></td>
            <td colspan="6"><div class="cell">签约地点：'.$contractDetail->signing_location.'</div></td>
        </tr>
        <tr>
            <td colspan="14"></td>
            <td colspan="6"><div class="cell">交期时间：'.$contractDetail->report_date.'</div></td>
        </tr>
        <tr>
            <td colspan="20" style="text-align: center;"></td>
        </tr>
        <tr>
            <td colspan="20" style="text-align: left; font-weight: bold;"><div class="cell">为促进甲方、乙方双方合作顺利开展，保障双方权益，根据《中华人民共和国民法典》及相关法律法规的规定，经双方友好协商特订立本合同，供共同遵守。</div></td>
        </tr>
        <tr>
            <td colspan="20" style="text-align: center;"></td>
        </tr>
        <tr>
            <td colspan="20"><div class="cell">一、 1、甲方商品的款号/品名，数量，单价，金额（人民币）</div></td>
        </tr>

        <tr>
        <td class="border1" colspan="3" rowspan="2" style="text-align: center;"><div class="cell">款号</div></td>
            <td class="border1" colspan="2" rowspan="2" style="text-align: center;"><div class="cell">款式</div></td>
            <td class="border1" colspan="2" rowspan="2" style="text-align: center;"><div class="cell">品名</div></td>
            <td class="border1" colspan="'.($length+1).'" rowspan="1" style="text-align: center;"><div class="cell">颜色尺码</div></td>
            <td class="border1" colspan="1" rowspan="2" style="text-align: center;"><div class="cell">合计(件/套)</div></td>
            <td class="border1" colspan="1" rowspan="2" style="text-align: center;"><div class="cell">单价(元)(含税)</div></td>
            <td class="border1" colspan="'.$priceWidth.'" rowspan="2" style="text-align: center;"><div class="cell">金额(含税)</div></td>
            <td class="border1" colspan="1" rowspan="2" style="text-align: center;"><div class="cell">吊牌</div></td>
            <td class="border1" colspan="'.$memoWidth.'" rowspan="2" style="text-align: center;"><div class="cell">备注</div></td>
        </tr>';

                    $html .= '<tr>
            <td class="border1" colspan="1" rowspan="1" style="text-align: center;">颜色</td>';

                    foreach ($rule as $size){
                        $html .= '
            <td class="border1" colspan="1" style="text-align: center;">'.$size.'</td>';
                    }

                    $html .= '
        </tr>';
                    $contractSpu = $contractDetail->spu_items;
                    $sizeInfo = [];
                    $totalPrice = 0;
                    foreach ($contractSpu as $v){
                        $html .= '<tr>
                <td class="border1" colspan="3" style="text-align: center;">'.$v['spu'].'</td>';
                        if (empty($v['img'])){
                            $html .= '<td class="border1" colspan="2" style="text-align: center;"></td>';
                        }else{
                            $html .= '<td class="border1" colspan="2" style="text-align: center;"><img src="' . $v['img'] . '" width="70pt"></td>';
                        }
                        $html .= '<td class="border1" colspan="2" style="text-align: center;">'.$v['title'].'</td>
                <td class="border1" colspan="1" style="text-align: center;">'.$v['color_name'].'/'.$v['color_identifying'].'</td>';
                        $numInfo = $v['num_info'];
                        foreach ($rule as $size){
                            $num = $numInfo->$size ?? 0;
                            if (isset($sizeInfo[$size])){
                                $sizeInfo[$size] += $num;
                            }else{
                                $sizeInfo[$size] = $num;
                            }
                            $totalPrice += $v['one_price'] * $num;
                            $html .= '<td class="border1" colspan="1" style="text-align: center;">'.$num.'</td>';
                        }
                        $html .= '<td class="border1" colspan="1" rowspan="1" style="text-align: center;"><div class="cell">'.$v['count'].'</div></td>
                <td class="border1" colspan="1" rowspan="1" style="text-align: center;"><div class="cell">'.$v['one_price'].'</div></td>
                <td class="border1" colspan="'.$priceWidth.'" rowspan="1" style="text-align: center;"><div class="cell">'.$v['price'].'</div></td>
                <td class="border1" colspan="1" style="text-align: center;">'.$contractDetail->brand.'</td>
                <td class="border1" colspan="'.$memoWidth.'" style="text-align: center;">'.$v['memo'].'</td>
            </tr>';
                    }
                    $html .= '<tr>
            <td class="border1" colspan="7" style="text-align: center;">合计金额</td>
            <td class="border1" colspan="1" style="text-align: center;"></td>';
                    foreach ($rule as $size){
                        $num = $sizeInfo[$size] ?? 0;
                        $html .= '
            <td class="border1" colspan="1" style="text-align: center;">'.$num.'</td>';
                    }

                    $html .= '
            <td class="border1" colspan="1" style="text-align: center;">'.array_sum($sizeInfo).'</td>
            <td class="border1" colspan="1" style="text-align: center;"></td>
            <td class="border1" colspan="'.$priceWidth.'" style="text-align: center;">'.$baseModel->floorDecimal($totalPrice).'</td>
            <td class="border1" colspan="1" style="text-align: center;"></td>
            <td class="border1" colspan="'.$memoWidth.'" style="text-align: center;"></td>
        </tr>';
                    $payment_information = $contractDetail->payment_information;
                    $count = 4;
                    if (count($payment_information) > $count){
                        $count = count($payment_information);
                    }
                    for ($i = 0; $i<$count; $i++){
                        $title = $payment_information[$i]['title'] ?? '';
                        $money = $payment_information[$i]['money'] ?? '';
                        $html .= '<tr>
                <td class="border1" colspan="7" style="text-align: center;">'.$title.'</td>
                <td class="border1" colspan="1" style="text-align: center;"></td>';

                        foreach ($rule as $size){
                            $html .= '<td class="border1" colspan="1" style="text-align: center;"></td>';
                        }

                        $html .= '
                <td class="border1" colspan="1" style="text-align: center;"></td>
                <td class="border1" colspan="1" style="text-align: center;"></td>
                <td class="border1" colspan="'.$priceWidth.'" style="text-align: center;">'.$money.'</td>
                <td class="border1" colspan="1" style="text-align: center;"></td>
                <td class="border1" colspan="'.$memoWidth.'" style="text-align: center;"></td>
            </tr>';
                    }
                    $html .= '<tr>
            <td colspan="20" style="text-align: center;"></td>
        </tr>

        <tr>
            <td colspan="20">2、需提供产前确认样（拍照样）齐色码'.implode('、', $rule).'各一件。</td>
        </tr>

        <tr>
            <td colspan="20">3、出货良品率保持99%，按合同规定的交货期交货，若逾期大于8天以上的，以规定合同交期而产生的交期逾期，以逾期货款金额为基数按每日万分之7.5计算违约金扣款，≥30天除了扣款还需承担甲方运费。</td>
        </tr>
        <tr>
            <td colspan="20">（如有其他原因延期交货，双方友好协商交货日期，并以书面或者邮件给以通知，须甲方同意为准，不可延期后再进行延期补签申请）。</td>
        </tr>

        <tr>
            <td colspan="20">二、质量要求技术标准、乙方对质量负责的条件:</td>
        </tr>
        <tr>
            <td colspan="20">1、产品质量和材料以需方提供的原样一致及双方最终签章确认产前样为准！如有不一致，甲方有权取消本次合同。</td>
        </tr>
        <tr>
            <td colspan="20">2、必须符合国家法定商检标准及甲方提出的质量标准,按照AQL2.5质量标准进行验货。</td>
        </tr>
        <tr>
            <td colspan="20">三、交货地点、方式及运输费用</td>
        </tr>
        <tr>
            <td colspan="20">1、须按甲方提供的交货期及指定的地点交货，我方地址分别为：厦门同安仓、泉州仓、厦门软二仓。</td>
        </tr>
        <tr>
            <td colspan="20">2、乙方按甲方指定地点送货，在保证交货期情况下乙方可自主选择运输方式，运输费用乙方承担。</td>
        </tr>
        <tr>
            <td colspan="20">四、包装标准：按我司要求包装（详细按理单资料）</td>
        </tr>
        <tr>
            <td colspan="20">五、验收标准、方法及提出异议期限：</td>
        </tr>
        <tr>
            <td colspan="20">1、甲方派出的QC以确认样和国家法定商检标准为准，对产品的品质、规格、数量、包装等外观质检验。乙方须严格按甲方要求执行。</td>
        </tr>
        <tr>
            <td colspan="20">2、 按确认样和制作单要求生产。乙方未能按质按量按时交货引起的损失，概由乙方承担。</td>
        </tr>
        <tr>
            <td colspan="20">3、甲方在收货后有权对因工厂生产技术、材料等方面造成的质量问题提出索赔、严重者可提出退货及索赔，乙方应予以接受。</td>
        </tr>
        <tr>
            <td colspan="20">4、大货只接受订单数量±3%（按各码数）。</td>
        </tr>
        <tr>
            <td colspan="20">六、货款的计算及结算方式：</td>
        </tr>
        <tr>
            <td colspan="20">1、付款方式：T/T</td>
        </tr>';

                    $html .= '
        <tr>
            <td colspan="20">'.$contractDetail->payment_settlement_method.'</td>
        </tr>';

                    $html .= '<tr>
            <td colspan="20">七、违约责任：</td>
        </tr>
        <tr>
            <td colspan="20">2、乙方所交货品与本合同规定质量不符导致甲方在转售该批服装时被客人拒绝接收或降价或其它救济方式时，甲方有权对乙方采取相应的措施，乙方不得拒绝。</td>
        </tr>
        <tr>
            <td colspan="20">3、因乙方不能按期交货，甲方不同意延期所引起的各项运费或制约款等费用由乙方承担。</td>
        </tr>
        <tr>
            <td colspan="20">八、解决合同纠纷方式：</td>
        </tr>
        <tr>
            <td colspan="20">合同履行过程中若发生纠纷，经友好协商未果提起诉讼，由签约地（厦门思明区）法院管辖。</td>
        </tr>
        <tr>
            <td colspan="20">九、其它约定事项:</td>
        </tr>
        <tr>
            <td colspan="20">1、由甲方提供的理单资料（即大货要求）均为本合同附件，其它未尽事宜可经双方商定进行书面修改补充并视为本合同不可分割的部分，同样具有法律效力。</td>
        </tr>
        <tr>
            <td colspan="20">2、大货材料必须由甲方确认后才可生产，否则由此引起的责任及后果由乙方承担;</td>
        </tr>
        <tr>
            <td colspan="20">3、甲方品牌产品、组合方式、设计款式、包装方式等待均不可销售其他电商卖家。如发现乙方向其他电商卖家销售以上规定的产品造成甲方损失应由乙方承担。</td>
        </tr>
        <tr>
            <td colspan="20">4、如果是因为乙方质量等原因，造成甲方派QC前往乙方工厂验货超过2次的，所产生的验货费用由乙方承担。 （根据验货地点费用起：500元-1000元以上）</td>
        </tr>
        <tr>
            <td colspan="20">5、乙方须在交货前免费提供符合甲方质量要求的初样，修改样，产前样，大货样，若不符合要求，须重新提供。</td>
        </tr>
        <tr>
            <td colspan="20">6、乙方必须做产前样给甲方确认后，方可开裁&生产大货，否则后果将由乙方负全部责任。</td>
        </tr>
        <tr>
            <td colspan="10" rowspan="1">'.$process_contract_no.'</td>
            <td colspan="10" rowspan="1">'.$process_supplier_name.'</td>
        </tr>
        <tr>
            <td colspan="20" rowspan="1"></td>
        </tr>
        <tr>
            <td colspan="20" rowspan="1"></td>
        </tr>
        <tr>
            <td colspan="20" rowspan="1"></td>
        </tr>
        <tr>
            <td colspan="2">甲方单位：</td>
            <td colspan="8">'.$contractDetail->company_name.'</td>
            <td colspan="2">乙方单位：</td>
            <td colspan="8">'.$contractDetail->supplier_name.'</td>
        </tr>
        <tr>
            <td colspan="2">地址：</td>
            <td colspan="8">'.$contractDetail->company_address.'</td>
            <td colspan="2">地址：</td>
            <td colspan="8">'.$contractDetail->supplier_address.'</td>
        </tr>
        <tr>
            <td colspan="10">甲方代表：'.$contractDetail->maker_name.'</td>
            <td colspan="10">乙方代表：</td>
        </tr>
        <tr>
            <td colspan="10">签章：</td>
            <td colspan="10">签章：</td>
        </tr>
        <tr>
            <td colspan="10">日期：'.$contractDetail->sign_date.'</td>
            <td colspan="10">日期：'.$contractDetail->sign_date.'</td>
        </tr>
    </table>
</body>
</html>';
                    break;
                case 4:
                    $printType = '购销合同-铁艺范本(含税)-打印';
                    // 铁艺范本
                    $html = '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table {
            border-collapse: collapse;
        }
        td, th {
            padding: 0;
        }
        table {
            border-collapse: separate;
            border-spacing: 0;
        }
        td,tr {
            font-size: 9px;
            line-height：9px;
        }
        td{
            width: 5%;
        }
        .border1{
            border: 1rem black double;
            height: 8px;
        }
        .fontBlod{
            font-weight: bold;
        }
        .cell {
            display: flex;
	        justify-content: center;
	        align-items: center;
        }
    </style>
</head>
<body>
    <table   width="100%" cellpadding="2" cellspacing="0">
        <tr>
            <td colspan="20" style="text-align: center;font-size: 20px; font-weight: bold; padding-bottom: 20px;"><div class="cell">'.$contractDetail->company_name.'购销合同</div></td>
        </tr>
        <tr>
            <td colspan="20"></td>
        </tr>
        <tr>
            <td colspan="14"><div class="cell">甲方单位：'.$contractDetail->company_name.'</div></td>
            <td colspan="6"><div class="cell">合同编号：'.$contractDetail->contract_no.'</div></td>
        </tr>
        <tr>
            <td colspan="14"><div class="cell">乙方单位：'.$contractDetail->supplier_name.'</div></td>
            <td colspan="6"><div class="cell">签约时间：'.$contractDetail->sign_date.'</div></td>
        </tr>
        <tr>
            <td colspan="14"></td>
            <td colspan="6"><div class="cell">签约地点：'.$contractDetail->signing_location.'</div></td>
        </tr>
        <tr>
            <td colspan="14"></td>
            <td colspan="6"><div class="cell">交期时间：'.$contractDetail->report_date.'</div></td>
        </tr>
        <tr>
            <td colspan="20" style="text-align: center;"></td>
        </tr>
        <tr>
            <td colspan="20" style="text-align: center; font-weight: bold;"><div class="cell">依照国家有关法律法规，签约双方就采购出口货物有关事项协商一致，达成本合同。</div></td>
        </tr>
        <tr>
            <td colspan="20" style="text-align: center;"></td>
        </tr>
        <tr>
            <td colspan="20"><div class="cell">一、 1、甲方商品的款号/品名，数量，单价，金额（人民币）</div></td>
        </tr>

        <tr>
        <td class="border1" colspan="3" rowspan="2" style="text-align: center;"><div class="cell">款号</div></td>
            <td class="border1" colspan="2" rowspan="2" style="text-align: center;"><div class="cell">款式</div></td>
            <td class="border1" colspan="2" rowspan="2" style="text-align: center;"><div class="cell">品名</div></td>
            <td class="border1" colspan="'.($length+1).'" rowspan="1" style="text-align: center;"><div class="cell">颜色尺码</div></td>
            <td class="border1" colspan="1" rowspan="2" style="text-align: center;"><div class="cell">合计(件/套)</div></td>
            <td class="border1" colspan="1" rowspan="2" style="text-align: center;"><div class="cell">单价(元)(含税)</div></td>
            <td class="border1" colspan="'.$priceWidth.'" rowspan="2" style="text-align: center;"><div class="cell">金额(含税)</div></td>
            <td class="border1" colspan="1" rowspan="2" style="text-align: center;"><div class="cell">吊牌</div></td>
            <td class="border1" colspan="'.$memoWidth.'" rowspan="2" style="text-align: center;"><div class="cell">备注</div></td>
        </tr>';

                    $html .= '<tr>
            <td class="border1" colspan="1" rowspan="1" style="text-align: center;">颜色</td>';

                    foreach ($rule as $size){
                        $html .= '
            <td class="border1" colspan="1" style="text-align: center;">'.$size.'</td>';
                    }

                    $html .= '
        </tr>';

                    $contractSpu = $contractDetail->spu_items;
                    $sizeInfo = [];
                    $totalPrice = 0;
                    foreach ($contractSpu as $v){
                        $html .= '<tr>
                <td class="border1" colspan="3" style="text-align: center;">'.$v['spu'].'</td>';
                        if (empty($v['img'])){
                            $html .= '<td class="border1" colspan="2" style="text-align: center;"></td>';
                        }else{
                            $html .= '<td class="border1" colspan="2" style="text-align: center;"><img src="' . $v['img'] . '" width="70pt"></td>';
                        }
                        $html .= '<td class="border1" colspan="2" style="text-align: center;">'.$v['title'].'</td>
                <td class="border1" colspan="1" style="text-align: center;">'.$v['color_name'].'/'.$v['color_identifying'].'</td>';
                        $numInfo = $v['num_info'];
                        foreach ($rule as $size){
                            $num = $numInfo->$size ?? 0;
                            if (isset($sizeInfo[$size])){
                                $sizeInfo[$size] += $num;
                            }else{
                                $sizeInfo[$size] = $num;
                            }
                            $totalPrice += $v['one_price'] * $num;
                            $html .= '<td class="border1" colspan="1" style="text-align: center;">'.$num.'</td>';
                        }
                        $html .= '<td class="border1" colspan="1" rowspan="1" style="text-align: center;"><div class="cell">'.$v['count'].'</div></td>
                <td class="border1" colspan="1" rowspan="1" style="text-align: center;"><div class="cell">'.$v['one_price'].'</div></td>
                <td class="border1" colspan="'.$priceWidth.'" rowspan="1" style="text-align: center;"><div class="cell">'.$v['price'].'</div></td>
                <td class="border1" colspan="1" style="text-align: center;">'.$contractDetail->brand.'</td>
                <td class="border1" colspan="'.$memoWidth.'" style="text-align: center;">'.$v['memo'].'</td>
            </tr>';
                    }
                    $html .= '<tr>
            <td class="border1" colspan="7" style="text-align: center;">合计金额</td>
            <td class="border1" colspan="1" style="text-align: center;"></td>';
                    foreach ($rule as $size){
                        $num = $sizeInfo[$size] ?? 0;
                        $html .= '
            <td class="border1" colspan="1" style="text-align: center;">'.$num.'</td>';
                    }
                    $html .= '
            <td class="border1" colspan="1" style="text-align: center;">'.array_sum($sizeInfo).'</td>
            <td class="border1" colspan="1" style="text-align: center;"></td>
            <td class="border1" colspan="'.$priceWidth.'" style="text-align: center;">'.$baseModel->floorDecimal($totalPrice).'</td>
            <td class="border1" colspan="1" style="text-align: center;"></td>
            <td class="border1" colspan="'.$memoWidth.'" style="text-align: center;"></td>
        </tr>';
                    $payment_information = $contractDetail->payment_information;
                    $count = 4;
                    if (count($payment_information) > $count){
                        $count = count($payment_information);
                    }
                    for ($i = 0; $i<$count; $i++){
                        $title = $payment_information[$i]['title'] ?? '';
                        $money = $payment_information[$i]['money'] ?? '';
                        $html .= '<tr>
                <td class="border1" colspan="7" style="text-align: center;">'.$title.'</td>
                <td class="border1" colspan="1" style="text-align: center;"></td>';

                        foreach ($rule as $size){
                            $html .= '<td class="border1" colspan="1" style="text-align: center;"></td>';
                        }

                        $html .= '
                <td class="border1" colspan="1" style="text-align: center;"></td>
                <td class="border1" colspan="1" style="text-align: center;"></td>
                <td class="border1" colspan="'.$priceWidth.'" style="text-align: center;">'.$money.'</td>
                <td class="border1" colspan="1" style="text-align: center;"></td>
                <td class="border1" colspan="'.$memoWidth.'" style="text-align: center;"></td>
            </tr>';
                    }

                    $html .= '<tr>
            <td colspan="20" style="text-align: center;"></td>
        </tr>

        <tr>
            <td colspan="2">品牌：</td>
            <td colspan="8"></td>
            <td colspan="10">，需提供产前确认样（拍照样）齐色码数各一件</td>
        </tr>

        <tr>
            <td colspan="20">出货良品率保持99%，按合同规定的交货期交货，若逾期大于8天以上的，延一天按万分之7.5扣款，≥31天除了扣款还需承担空运费。</td>
        </tr>
        <tr>
            <td colspan="20">（如有其他原因延期交货，双方友好协商交货日期，并以书面或者邮件申请，甲方同意为准，不可延期后再进行延期补签申请）</td>
        </tr>

        <tr>
            <td colspan="20">二、质量要求技术标准、乙方对质量负责的条件: </td>
        </tr>
        <tr>
            <td colspan="20">产品质量和材料以需方提供的原样一致及双方最终签章确认产前样为准！如有不一致，甲方有权取消合同。 符合国家法定商检标准及甲方提出的质量标准,按照AQL2.5质量标准进行验货。</td>
        </tr>
        <tr>
            <td colspan="20">三、交货地点、方式及运输费用</td>
        </tr>
        <tr>
            <td colspan="20">1、按甲方提供的交货期及指定的地点交货，送货地址：厦门同安仓、泉州仓</td>
        </tr>
        <tr>
            <td colspan="20">2、乙方按甲方指定地点送货，在保证交货期情况下乙方可自主选择运输方式，运输费用乙方承担。</td>
        </tr>
        <tr>
            <td colspan="20">四、包装标准：按我司要求包装（详细按理单资料）</td>
        </tr>
        <tr>
            <td colspan="20">五、验收标准、方法及提出异议期限：</td>
        </tr>
        <tr>
            <td colspan="20">1、甲方派出的QC以确认样和国家法定商检标准为准，对产品的品质、规格、数量、包装等外观质检验。乙方须严格按确认样和制作单要求生产。乙方未能按质按量按时交货引起的损失，概由乙方承担。</td>
        </tr>
        <tr>
            <td colspan="20">2、甲方在收货后90天内有权对因工厂生产技术、材料等方面造成的质量问题提出索赔、严重者可提出退货及索赔，乙方应予接受。</td>
        </tr>
        <tr>
            <td colspan="20">六、货款的计算及结算方式：</td>
        </tr>
        <tr>
            <td colspan="20">1、付款方式：</td>
        </tr>';

                    $html .= '
        <tr>
            <td colspan="20">付款方式：'.$contractDetail->payment_settlement_method.'</td>
        </tr>';

                    $html .= '<tr>
            <td colspan="20">七、违约责任：</td>
        </tr>
        <tr>
            <td colspan="20">1、乙方所交货品与本合同规定质量不符导致甲方在转售该批服装时被客人拒绝接收或降价或其它救济方式时，甲方有权对乙方采取相应的措施，乙方不得拒绝。</td>
        </tr>
        <tr>
            <td colspan="20">2、因乙方不能按期交货，甲方不同意延期所引起的空运费或制约款等费用由乙方承担。</td>
        </tr>
        <tr>
            <td colspan="20">八、解决合同纠纷方式：</td>
        </tr>
        <tr>
            <td colspan="20">合同履行过程中若发生纠纷，经友好协商未果提起诉讼，由签约地法院管辖。</td>
        </tr>
        <tr>
            <td colspan="20">九、其它约定事项:</td>
        </tr>
        <tr>
            <td colspan="20">1、由甲方提供的理单资料（即大货要求）均为本合同附件，其它未尽事宜可经双方商定进行书面修改补充并视为本合同不可分割的部分，同样具有法律效力。</td>
        </tr>
        <tr>
            <td colspan="20">2、大货材料必须由甲方确认后才可生产，否则由此引起的责任及后果由乙方承担;</td>
        </tr>
        <tr>
            <td colspan="20">3、我司品牌产品、组合方式、设计款式、包装方式等待均不可销售其他电商卖家。如发现贵司向其他电商卖家销售以上规定的产品造成我司损失应由贵司承担。</td>
        </tr>
        <tr>
            <td colspan="20">4、如果是因为乙方质量等原因，造成甲方派QC前往乙方工厂验货超过2次的，所产生的费用由乙方承担。 （根据验货地点费用起：500元-1000元以上）</td>
        </tr>
        <tr>
            <td colspan="20">6、乙方必须做产前样给甲方确认后，方可开裁&生产大货，否则后果将由乙方负全部责任。</td>
        </tr>
        <tr>
            <td colspan="10" rowspan="1">'.$process_contract_no.'</td>
            <td colspan="10" rowspan="1">'.$process_supplier_name.'</td>
        </tr>
        <tr>
            <td colspan="20" rowspan="1"></td>
        </tr>
        <tr>
            <td colspan="20" rowspan="1"></td>
        </tr>
        <tr>
            <td colspan="20" rowspan="1"></td>
        </tr>
        <tr>
            <td colspan="2">甲方单位：</td>
            <td colspan="8">'.$contractDetail->company_name.'</td>
            <td colspan="2">乙方单位：</td>
            <td colspan="8">'.$contractDetail->supplier_name.'</td>
        </tr>
        <tr>
            <td colspan="2">地址：</td>
            <td colspan="8">'.$contractDetail->company_address.'</td>
            <td colspan="2">地址：</td>
            <td colspan="8">'.$contractDetail->supplier_address.'</td>
        </tr>
        <tr>
            <td colspan="10">甲方代表：'.$contractDetail->maker_name.'</td>
            <td colspan="10">乙方代表：</td>
        </tr>
        <tr>
            <td colspan="10">签章：</td>
            <td colspan="10">签章：</td>
        </tr>
        <tr>
            <td colspan="10">日期：'.$contractDetail->sign_date.'</td>
            <td colspan="10">日期：'.$contractDetail->sign_date.'</td>
        </tr>
    </table>
</body>
</html>';
                    break;
                case 5:
                    $printType = '购销合同-卖单现货范本(不含税)-打印';
                    $html = '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table {
            border-collapse: collapse;
        }
        td, th {
            padding: 0;
        }
        table {
            border-collapse: separate;
            border-spacing: 0;
        }
        td,tr {
            font-size: 9px;
            line-height：9px;
        }
        td{
            width: 5%;
        }
        .border1{
            border-width: 0.5rem;
            border-style: double double double double;
            border-color: black;
            height: 8px;
        }
        .fontBlod{
            font-weight: bold;
        }
        .cell {
            display: flex;
	        justify-content: center;
	        align-items: center;
        }
    </style>
</head>
<body>
    <table   width="100%" cellpadding="2" cellspacing="0">
        <tr>
            <td colspan="20" style="text-align: center;font-size: 20px; font-weight: bold; padding-bottom: 20px;"><div class="cell">'.$contractDetail->company_name.'购销合同</div></td>
        </tr>
        <tr>
            <td colspan="20"></td>
        </tr>
        <tr>
            <td colspan="14"><div class="cell">甲方单位：'.$contractDetail->company_name.'(需方)</div></td>
            <td colspan="6"><div class="cell">合同编号：'.$contractDetail->contract_no.'</div></td>
        </tr>
        <tr>
            <td colspan="14"><div class="cell">乙方单位：'.$contractDetail->supplier_name.'(供方)</div></td>
            <td colspan="6"><div class="cell">签约时间：'.$contractDetail->sign_date.'</div></td>
        </tr>
        <tr>
            <td colspan="14"></td>
            <td colspan="6"><div class="cell">签约地点：'.$contractDetail->signing_location.'</div></td>
        </tr>
        <tr>
            <td colspan="14"></td>
            <td colspan="6"><div class="cell">交期时间：'.$contractDetail->report_date.'</div></td>
        </tr>
        <tr>
            <td colspan="20" style="text-align: center;"></td>
        </tr>
        <tr>
            <td colspan="20" style="text-align: left; font-weight: bold;"><div class="cell">依照国家有关法律法规，签约双方就采购货物有关事项协商一致，达成本合同。</div></td>
        </tr>
        <tr>
            <td colspan="20" style="text-align: center;"></td>
        </tr>
        <tr>
            <td colspan="20"><div class="cell">一、 1、甲方商品的款号/品名，数量，单价，金额（人民币）</div></td>
        </tr>

        <tr>
        <td class="border1" colspan="3" rowspan="2" style="text-align: center;"><div class="cell">款号</div></td>
            <td class="border1" colspan="2" rowspan="2" style="text-align: center;"><div class="cell">款式</div></td>
            <td class="border1" colspan="2" rowspan="2" style="text-align: center;"><div class="cell">品名</div></td>
            <td class="border1" colspan="'.($length+1).'" rowspan="1" style="text-align: center;"><div class="cell">颜色尺码</div></td>
            <td class="border1" colspan="1" rowspan="2" style="text-align: center;"><div class="cell">合计(件/套)</div></td>
            <td class="border1" colspan="1" rowspan="2" style="text-align: center;"><div class="cell">单价(元)(不含税)</div></td>
            <td class="border1" colspan="'.$priceWidth.'" rowspan="2" style="text-align: center;"><div class="cell">金额(不含税)</div></td>
            <td class="border1" colspan="1" rowspan="2" style="text-align: center;"><div class="cell">吊牌</div></td>
            <td class="border1" colspan="'.$memoWidth.'" rowspan="2" style="text-align: center;"><div class="cell">备注</div></td>
        </tr>';

                    $html .= '<tr>
            <td class="border1" colspan="1" rowspan="1" style="text-align: center;">颜色</td>';

                    foreach ($rule as $size){
                        $html .= '
            <td class="border1" colspan="1" style="text-align: center;">'.$size.'</td>';
                    }

                    $html .= '
        </tr>';
                    $contractSpu = $contractDetail->spu_items;
                    $sizeInfo = [];
                    $totalPrice = 0;
                    foreach ($contractSpu as $v){
                        $html .= '<tr>
                <td class="border1" colspan="3" style="text-align: center;">'.$v['spu'].'</td>';
                        if (empty($v['img'])){
                            $html .= '<td class="border1" colspan="2" style="text-align: center;"></td>';
                        }else{
                            $html .= '<td class="border1" colspan="2" style="text-align: center;"><img src="' . $v['img'] . '" width="70pt"></td>';
                        }
                        $html .= '<td class="border1" colspan="2" style="text-align: center;">'.$v['title'].'</td>
                <td class="border1" colspan="1" style="text-align: center;">'.$v['color_name'].'/'.$v['color_identifying'].'</td>';
                        $numInfo = $v['num_info'];
                        foreach ($rule as $size){
                            $num = $numInfo->$size ?? 0;
                            if (isset($sizeInfo[$size])){
                                $sizeInfo[$size] += $num;
                            }else{
                                $sizeInfo[$size] = $num;
                            }
                            $totalPrice += $v['one_price'] * $num;
                            $html .= '<td class="border1" colspan="1" style="text-align: center;">'.$num.'</td>';
                        }
                        $html .= '<td class="border1" colspan="1" rowspan="1" style="text-align: center;"><div class="cell">'.$v['count'].'</div></td>
                <td class="border1" colspan="1" rowspan="1" style="text-align: center;"><div class="cell">'.$v['one_price'].'</div></td>
                <td class="border1" colspan="'.$priceWidth.'" rowspan="1" style="text-align: center;"><div class="cell">'.$v['price'].'</div></td>
                <td class="border1" colspan="1" style="text-align: center;">'.$contractDetail->brand.'</td>
                <td class="border1" colspan="'.$memoWidth.'" style="text-align: center;">'.$v['memo'].'</td>
            </tr>';
                    }
                    $html .= '<tr>
            <td class="border1" colspan="7" style="text-align: center;">合计金额</td>
            <td class="border1" colspan="1" style="text-align: center;"></td>';
                    foreach ($rule as $size){
                        $num = $sizeInfo[$size] ?? 0;
                        $html .= '
            <td class="border1" colspan="1" style="text-align: center;">'.$num.'</td>';
                    }

                    $html .= '
            <td class="border1" colspan="1" style="text-align: center;">'.array_sum($sizeInfo).'</td>
            <td class="border1" colspan="1" style="text-align: center;"></td>
            <td class="border1" colspan="'.$priceWidth.'" style="text-align: center;">'.$baseModel->floorDecimal($totalPrice).'</td>
            <td class="border1" colspan="1" style="text-align: center;"></td>
            <td class="border1" colspan="'.$memoWidth.'" style="text-align: center;"></td>
        </tr>';
                    $payment_information = $contractDetail->payment_information;
                    $count = 4;
                    if (count($payment_information) > $count){
                        $count = count($payment_information);
                    }
                    for ($i = 0; $i<$count; $i++){
                        $title = $payment_information[$i]['title'] ?? '';
                        $money = $payment_information[$i]['money'] ?? '';
                        $html .= '<tr>
                <td class="border1" colspan="7" style="text-align: center;">'.$title.'</td>
                <td class="border1" colspan="1" style="text-align: center;"></td>';

                        foreach ($rule as $size){
                            $html .= '<td class="border1" colspan="1" style="text-align: center;"></td>';
                        }

                        $html .= '
                <td class="border1" colspan="1" style="text-align: center;"></td>
                <td class="border1" colspan="1" style="text-align: center;"></td>
                <td class="border1" colspan="'.$priceWidth.'" style="text-align: center;">'.$money.'</td>
                <td class="border1" colspan="1" style="text-align: center;"></td>
                <td class="border1" colspan="'.$memoWidth.'" style="text-align: center;"></td>
            </tr>';
                    }
                    $html .= '<tr>
            <td colspan="20" style="text-align: center;"></td>
        </tr>

        <tr>
            <td colspan="20">发货前需提供大货样确认</td>
        </tr>

        <tr>
            <td colspan="20">（如有其他原因延期交货，双方友好协商交货日期，并以书面或者邮件申请，甲方同意为准，不可延期后再进行延期补签申请）</td>
        </tr>
        
        <tr>
            <td colspan="20">二、质量要求技术标准、乙方对质量负责的条件：</td>
        </tr>
        
        <tr>
            <td colspan="20">现货,货品如有严重脏污和破洞或其它，影响卖相需挑出来再发货。</td>
        </tr>
        
        <tr>
            <td colspan="20">三、交货地点、方式及运输费用</td>
        </tr>
        
        <tr>
            <td colspan="20">1、按甲方提供的交货期及指定的地点交货，送货地址：厦门同安仓、泉州仓（具体以甲方指定交货地址为准）</td>
        </tr>
        
        <tr>
            <td colspan="20">2、乙方按甲方指定地点送货，在保证交货期情况下乙方可自主选择运输方式，运输费用乙方承担。</td>
        </tr>
        
        <tr>
            <td colspan="20">四、包装标准：按现货完整包装, 无破损，有吊牌有洗唛成份标</td>
        </tr>
        
        <tr>
            <td colspan="20">五、验收标准、方法及提出异议期限</td>
        </tr>
        
        <tr>
            <td colspan="20">甲方在收货后30天内发现货品严重质量问题退货，余款抵扣，乙方应予接受。若余款不足抵扣，乙方必须在3天之内将造成的退货金额打入甲方指定的银行账户。</td>
        </tr>
        
        <tr>
            <td colspan="20">六、货款的计算及结算方式：</td>
        </tr>
        
        <tr>
            <td colspan="20">1、付款方式：T/T</td>
        </tr>';

                    $html .= '
        <tr>
            <td colspan="20">'.$contractDetail->payment_settlement_method.'</td>
        </tr>';

                    $html .= '
        <tr>
            <td colspan="20">七、违约责任：</td>
        </tr>
        
        <tr>
            <td colspan="20">1、因乙方不能按期交货，甲方不同意延期所造成的额外费用损失由乙方承担。</td>
        </tr>
        
        <tr>
            <td colspan="20">2、因乙方不能按期交货，甲方不同意延期所引起的空运费或制约款等费用由乙方承担。</td>
        </tr>
        
        <tr>
            <td colspan="20">八、解决合同纠纷方式：</td>
        </tr>
        
        <tr>
            <td colspan="20">合同履行过程中若发生纠纷，经友好协商未果提起诉讼，由厦门市思明区法院管辖。</td>
        </tr>
        
        <tr>
            <td colspan="10" rowspan="1">'.$process_contract_no.'</td>
            <td colspan="10" rowspan="1">'.$process_supplier_name.'</td>
        </tr>
        
        <tr>
            <td colspan="2">甲方单位：</td>
            <td colspan="8">'.$contractDetail->company_name.'</td>
            <td colspan="2">乙方单位：</td>
            <td colspan="8">'.$contractDetail->supplier_name.'</td>
        </tr>
        <tr>
            <td colspan="2">地址：</td>
            <td colspan="8">'.$contractDetail->company_address.'</td>
            <td colspan="2">地址：</td>
            <td colspan="8">'.$contractDetail->supplier_address.'</td>
        </tr>
        <tr>
            <td colspan="10">甲方代表：'.$contractDetail->maker_name.'</td>
            <td colspan="10">乙方代表：</td>
        </tr>
        <tr>
            <td colspan="10">签章：</td>
            <td colspan="10">签章：</td>
        </tr>
        <tr>
            <td colspan="10">日期：'.$contractDetail->sign_date.'</td>
            <td colspan="10">日期：'.$contractDetail->sign_date.'</td>
        </tr>
    </table>
</body>
</html>';
                    break;
                case 6:
                    $printType = '购销合同-卖单现货范本(含税)-打印';
                    $html = '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table {
            border-collapse: collapse;
        }
        td, th {
            padding: 0;
        }
        table {
            border-collapse: separate;
            border-spacing: 0;
        }
        td,tr {
            font-size: 9px;
            line-height：9px;
        }
        td{
            width: 5%;
        }
        .border1{
            border-width: 0.5rem;
            border-style: double double double double;
            border-color: black;
            height: 8px;
        }
        .fontBlod{
            font-weight: bold;
        }
        .cell {
            display: flex;
	        justify-content: center;
	        align-items: center;
        }
    </style>
</head>
<body>
    <table   width="100%" cellpadding="2" cellspacing="0">
        <tr>
            <td colspan="20" style="text-align: center;font-size: 20px; font-weight: bold; padding-bottom: 20px;"><div class="cell">'.$contractDetail->company_name.'购销合同</div></td>
        </tr>
        <tr>
            <td colspan="20"></td>
        </tr>
        <tr>
            <td colspan="14"><div class="cell">甲方单位：'.$contractDetail->company_name.'(需方)</div></td>
            <td colspan="6"><div class="cell">合同编号：'.$contractDetail->contract_no.'</div></td>
        </tr>
        <tr>
            <td colspan="14"><div class="cell">乙方单位：'.$contractDetail->supplier_name.'(供方)</div></td>
            <td colspan="6"><div class="cell">签约时间：'.$contractDetail->sign_date.'</div></td>
        </tr>
        <tr>
            <td colspan="14"></td>
            <td colspan="6"><div class="cell">签约地点：'.$contractDetail->signing_location.'</div></td>
        </tr>
        <tr>
            <td colspan="14"></td>
            <td colspan="6"><div class="cell">交期时间：'.$contractDetail->report_date.'</div></td>
        </tr>
        <tr>
            <td colspan="20" style="text-align: center;"></td>
        </tr>
        <tr>
            <td colspan="20" style="text-align: left; font-weight: bold;"><div class="cell">依照国家有关法律法规，签约双方就采购货物有关事项协商一致，达成本合同。</div></td>
        </tr>
        <tr>
            <td colspan="20" style="text-align: center;"></td>
        </tr>
        <tr>
            <td colspan="20"><div class="cell">一、 1、甲方商品的款号/品名，数量，单价，金额（人民币）</div></td>
        </tr>

        <tr>
        <td class="border1" colspan="3" rowspan="2" style="text-align: center;"><div class="cell">款号</div></td>
            <td class="border1" colspan="2" rowspan="2" style="text-align: center;"><div class="cell">款式</div></td>
            <td class="border1" colspan="2" rowspan="2" style="text-align: center;"><div class="cell">品名</div></td>
            <td class="border1" colspan="'.($length+1).'" rowspan="1" style="text-align: center;"><div class="cell">颜色尺码</div></td>
            <td class="border1" colspan="1" rowspan="2" style="text-align: center;"><div class="cell">合计(件/套)</div></td>
            <td class="border1" colspan="1" rowspan="2" style="text-align: center;"><div class="cell">单价(元)(含税)</div></td>
            <td class="border1" colspan="'.$priceWidth.'" rowspan="2" style="text-align: center;"><div class="cell">金额(含税)</div></td>
            <td class="border1" colspan="1" rowspan="2" style="text-align: center;"><div class="cell">吊牌</div></td>
            <td class="border1" colspan="'.$memoWidth.'" rowspan="2" style="text-align: center;"><div class="cell">备注</div></td>
        </tr>';

                    $html .= '<tr>
            <td class="border1" colspan="1" rowspan="1" style="text-align: center;">颜色</td>';

                    foreach ($rule as $size){
                        $html .= '
            <td class="border1" colspan="1" style="text-align: center;">'.$size.'</td>';
                    }

                    $html .= '
        </tr>';
                    $contractSpu = $contractDetail->spu_items;
                    $sizeInfo = [];
                    $totalPrice = 0;
                    foreach ($contractSpu as $v){
                        $html .= '<tr>
                <td class="border1" colspan="3" style="text-align: center;">'.$v['spu'].'</td>';
                        if (empty($v['img'])){
                            $html .= '<td class="border1" colspan="2" style="text-align: center;"></td>';
                        }else{
                            $html .= '<td class="border1" colspan="2" style="text-align: center;"><img src="' . $v['img'] . '" width="70pt"></td>';
                        }
                        $html .= '<td class="border1" colspan="2" style="text-align: center;">'.$v['title'].'</td>
                <td class="border1" colspan="1" style="text-align: center;">'.$v['color_name'].'/'.$v['color_identifying'].'</td>';
                        $numInfo = $v['num_info'];
                        foreach ($rule as $size){
                            $num = $numInfo->$size ?? 0;
                            if (isset($sizeInfo[$size])){
                                $sizeInfo[$size] += $num;
                            }else{
                                $sizeInfo[$size] = $num;
                            }
                            $totalPrice += $v['one_price'] * $num;
                            $html .= '<td class="border1" colspan="1" style="text-align: center;">'.$num.'</td>';
                        }
                        $html .= '<td class="border1" colspan="1" rowspan="1" style="text-align: center;"><div class="cell">'.$v['count'].'</div></td>
                <td class="border1" colspan="1" rowspan="1" style="text-align: center;"><div class="cell">'.$v['one_price'].'</div></td>
                <td class="border1" colspan="'.$priceWidth.'" rowspan="1" style="text-align: center;"><div class="cell">'.$v['price'].'</div></td>
                <td class="border1" colspan="1" style="text-align: center;">'.$contractDetail->brand.'</td>
                <td class="border1" colspan="'.$memoWidth.'" style="text-align: center;">'.$v['memo'].'</td>
            </tr>';
                    }
                    $html .= '<tr>
            <td class="border1" colspan="7" style="text-align: center;">合计金额</td>
            <td class="border1" colspan="1" style="text-align: center;"></td>';
                    foreach ($rule as $size){
                        $num = $sizeInfo[$size] ?? 0;
                        $html .= '
            <td class="border1" colspan="1" style="text-align: center;">'.$num.'</td>';
                    }

                    $html .= '
            <td class="border1" colspan="1" style="text-align: center;">'.array_sum($sizeInfo).'</td>
            <td class="border1" colspan="1" style="text-align: center;"></td>
            <td class="border1" colspan="'.$priceWidth.'" style="text-align: center;">'.$baseModel->floorDecimal($totalPrice).'</td>
            <td class="border1" colspan="1" style="text-align: center;"></td>
            <td class="border1" colspan="'.$memoWidth.'" style="text-align: center;"></td>
        </tr>';
                    $payment_information = $contractDetail->payment_information;
                    $count = 4;
                    if (count($payment_information) > $count){
                        $count = count($payment_information);
                    }
                    for ($i = 0; $i<$count; $i++){
                        $title = $payment_information[$i]['title'] ?? '';
                        $money = $payment_information[$i]['money'] ?? '';
                        $html .= '<tr>
                <td class="border1" colspan="7" style="text-align: center;">'.$title.'</td>
                <td class="border1" colspan="1" style="text-align: center;"></td>';

                        foreach ($rule as $size){
                            $html .= '<td class="border1" colspan="1" style="text-align: center;"></td>';
                        }

                        $html .= '
                <td class="border1" colspan="1" style="text-align: center;"></td>
                <td class="border1" colspan="1" style="text-align: center;"></td>
                <td class="border1" colspan="'.$priceWidth.'" style="text-align: center;">'.$money.'</td>
                <td class="border1" colspan="1" style="text-align: center;"></td>
                <td class="border1" colspan="'.$memoWidth.'" style="text-align: center;"></td>
            </tr>';
                    }
                    $html .= '<tr>
            <td colspan="20" style="text-align: center;"></td>
        </tr>

        <tr>
            <td colspan="20">发货前需提供大货样确认</td>
        </tr>

        <tr>
            <td colspan="20">（如有其他原因延期交货，双方友好协商交货日期，并以书面或者邮件申请，甲方同意为准，不可延期后再进行延期补签申请）</td>
        </tr>
        
        <tr>
            <td colspan="20">二、质量要求技术标准、乙方对质量负责的条件：</td>
        </tr>
        
        <tr>
            <td colspan="20">现货,货品如有严重脏污和破洞或其它，影响卖相需挑出来再发货。</td>
        </tr>
        
        <tr>
            <td colspan="20">三、交货地点、方式及运输费用</td>
        </tr>
        
        <tr>
            <td colspan="20">1、按甲方提供的交货期及指定的地点交货，送货地址：厦门同安仓、泉州仓（具体以甲方指定交货地址为准）</td>
        </tr>
        
        <tr>
            <td colspan="20">2、乙方按甲方指定地点送货，在保证交货期情况下乙方可自主选择运输方式，运输费用乙方承担。</td>
        </tr>
        
        <tr>
            <td colspan="20">四、包装标准：按现货完整包装, 无破损，有吊牌有洗唛成份标</td>
        </tr>
        
        <tr>
            <td colspan="20">五、验收标准、方法及提出异议期限</td>
        </tr>
        
        <tr>
            <td colspan="20">甲方在收货后30天内发现货品严重质量问题退货，余款抵扣，乙方应予接受。若余款不足抵扣，乙方必须在3天之内将造成的退货金额打入甲方指定的银行账户。</td>
        </tr>
        
        <tr>
            <td colspan="20">六、货款的计算及结算方式：</td>
        </tr>
        
        <tr>
            <td colspan="20">1、付款方式：T/T</td>
        </tr>';

                    $html .= '
        <tr>
            <td colspan="20">'.$contractDetail->payment_settlement_method.'</td>
        </tr>';

                    $html .= '
        <tr>
            <td colspan="20">七、违约责任：</td>
        </tr>
        
        <tr>
            <td colspan="20">1、因乙方不能按期交货，甲方不同意延期所造成的额外费用损失由乙方承担。</td>
        </tr>
        
        <tr>
            <td colspan="20">2、因乙方不能按期交货，甲方不同意延期所引起的空运费或制约款等费用由乙方承担。</td>
        </tr>
        
        <tr>
            <td colspan="20">八、解决合同纠纷方式：</td>
        </tr>
        
        <tr>
            <td colspan="20">合同履行过程中若发生纠纷，经友好协商未果提起诉讼，由厦门市思明区法院管辖。</td>
        </tr>
        
        <tr>
            <td colspan="10" rowspan="1">'.$process_contract_no.'</td>
            <td colspan="10" rowspan="1">'.$process_supplier_name.'</td>
        </tr>
       
        <tr>
            <td colspan="2">甲方单位：</td>
            <td colspan="8">'.$contractDetail->company_name.'</td>
            <td colspan="2">乙方单位：</td>
            <td colspan="8">'.$contractDetail->supplier_name.'</td>
        </tr>
        <tr>
            <td colspan="2">地址：</td>
            <td colspan="8">'.$contractDetail->company_address.'</td>
            <td colspan="2">地址：</td>
            <td colspan="8">'.$contractDetail->supplier_address.'</td>
        </tr>
        <tr>
            <td colspan="10">甲方代表：'.$contractDetail->maker_name.'</td>
            <td colspan="10">乙方代表：</td>
        </tr>
        <tr>
            <td colspan="10">签章：</td>
            <td colspan="10">签章：</td>
        </tr>
        <tr>
            <td colspan="10">日期：'.$contractDetail->sign_date.'</td>
            <td colspan="10">日期：'.$contractDetail->sign_date.'</td>
        </tr>
    </table>
</body>
</html>';
                    break;
                case 7:
                    $printType = '购销合同-众拓范本(不含税)-打印';
                    $html = '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table {
            border-collapse: collapse;
        }
        td, th {
            padding: 0;
        }
        table {
            border-collapse: separate;
            border-spacing: 0;
        }
        td,tr {
            font-size: 9px;
            line-height：9px;
        }
        td{
            width: 5%;
        }
        .border1{
            border-width: 0.5rem;
            border-style: double double double double;
            border-color: black;
            height: 8px;
        }
        .fontBlod{
            font-weight: bold;
        }
        .cell {
            display: flex;
	        justify-content: center;
	        align-items: center;
        }
    </style>
</head>
<body>
    <table   width="100%" cellpadding="2" cellspacing="0">
        <tr>
            <td colspan="20" style="text-align: center;font-size: 20px; font-weight: bold; padding-bottom: 20px;"><div class="cell">'.$contractDetail->company_name.'购销合同</div></td>
        </tr>
        <tr>
            <td colspan="20"></td>
        </tr>
        <tr>
            <td colspan="14"><div class="cell">甲方单位：'.$contractDetail->company_name.'(需方)</div></td>
            <td colspan="6"><div class="cell">合同编号：'.$contractDetail->contract_no.'</div></td>
        </tr>
        <tr>
            <td colspan="14"><div class="cell">乙方单位：'.$contractDetail->supplier_name.'(供方)</div></td>
            <td colspan="6"><div class="cell">签约时间：'.$contractDetail->sign_date.'</div></td>
        </tr>
        <tr>
            <td colspan="14"></td>
            <td colspan="6"><div class="cell">签约地点：'.$contractDetail->signing_location.'</div></td>
        </tr>
        <tr>
            <td colspan="14"></td>
            <td colspan="6"><div class="cell">交期时间：'.$contractDetail->report_date.'</div></td>
        </tr>
        <tr>
            <td colspan="20" style="text-align: center;"></td>
        </tr>
        <tr>
            <td colspan="20" style="text-align: left; font-weight: bold;"><div class="cell">为促进甲方、乙方双方合作顺利开展，保障双方权益，根据《中华人民共和国民法典》及相关法律法规的规定，经双方友好协商特订立本合同，供共同遵守。</div></td>
        </tr>
        <tr>
            <td colspan="20" style="text-align: center;"></td>
        </tr>
        <tr>
            <td colspan="20"><div class="cell">一、 1、甲方商品的款号/品名，数量，单价，金额（人民币）</div></td>
        </tr>

        <tr>
        <td class="border1" colspan="3" rowspan="2" style="text-align: center;"><div class="cell">款号</div></td>
            <td class="border1" colspan="2" rowspan="2" style="text-align: center;"><div class="cell">款式</div></td>
            <td class="border1" colspan="2" rowspan="2" style="text-align: center;"><div class="cell">品名</div></td>
            <td class="border1" colspan="'.($length+1).'" rowspan="1" style="text-align: center;"><div class="cell">颜色尺码</div></td>
            <td class="border1" colspan="1" rowspan="2" style="text-align: center;"><div class="cell">合计(件/套)</div></td>
            <td class="border1" colspan="1" rowspan="2" style="text-align: center;"><div class="cell">单价(元)(不含税)</div></td>
            <td class="border1" colspan="'.$priceWidth.'" rowspan="2" style="text-align: center;"><div class="cell">金额(不含税)</div></td>
            <td class="border1" colspan="1" rowspan="2" style="text-align: center;"><div class="cell">吊牌</div></td>
            <td class="border1" colspan="'.$memoWidth.'" rowspan="2" style="text-align: center;"><div class="cell">备注</div></td>
        </tr>';

                    $html .= '<tr>
            <td class="border1" colspan="1" rowspan="1" style="text-align: center;">颜色</td>';

                    foreach ($rule as $size){
                        $html .= '
            <td class="border1" colspan="1" style="text-align: center;">'.$size.'</td>';
                    }

                    $html .= '
        </tr>';
                    $contractSpu = $contractDetail->spu_items;
                    $sizeInfo = [];
                    $totalPrice = 0;
                    foreach ($contractSpu as $v){
                        $html .= '<tr>
                <td class="border1" colspan="3" style="text-align: center;">'.$v['spu'].'</td>';
                        if (empty($v['img'])){
                            $html .= '<td class="border1" colspan="2" style="text-align: center;"></td>';
                        }else{
                            $html .= '<td class="border1" colspan="2" style="text-align: center;"><img src="' . $v['img'] . '" width="70pt"></td>';
                        }
                        $html .= '<td class="border1" colspan="2" style="text-align: center;">'.$v['title'].'</td>
                <td class="border1" colspan="1" style="text-align: center;">'.$v['color_name'].'/'.$v['color_identifying'].'</td>';
                        $numInfo = $v['num_info'];
                        foreach ($rule as $size){
                            $num = $numInfo->$size ?? 0;
                            if (isset($sizeInfo[$size])){
                                $sizeInfo[$size] += $num;
                            }else{
                                $sizeInfo[$size] = $num;
                            }
                            $totalPrice += $v['one_price'] * $num;
                            $html .= '<td class="border1" colspan="1" style="text-align: center;">'.$num.'</td>';
                        }
                        $html .= '<td class="border1" colspan="1" rowspan="1" style="text-align: center;"><div class="cell">'.$v['count'].'</div></td>
                <td class="border1" colspan="1" rowspan="1" style="text-align: center;"><div class="cell">'.$v['one_price'].'</div></td>
                <td class="border1" colspan="'.$priceWidth.'" rowspan="1" style="text-align: center;"><div class="cell">'.$v['price'].'</div></td>
                <td class="border1" colspan="1" style="text-align: center;">'.$contractDetail->brand.'</td>
                <td class="border1" colspan="'.$memoWidth.'" style="text-align: center;">'.$v['memo'].'</td>
            </tr>';
                    }
                    $html .= '<tr>
            <td class="border1" colspan="7" style="text-align: center;">合计金额</td>
            <td class="border1" colspan="1" style="text-align: center;"></td>';
                    foreach ($rule as $size){
                        $num = $sizeInfo[$size] ?? 0;
                        $html .= '
            <td class="border1" colspan="1" style="text-align: center;">'.$num.'</td>';
                    }

                    $html .= '
            <td class="border1" colspan="1" style="text-align: center;">'.array_sum($sizeInfo).'</td>
            <td class="border1" colspan="1" style="text-align: center;"></td>
            <td class="border1" colspan="'.$priceWidth.'" style="text-align: center;">'.$baseModel->floorDecimal($totalPrice).'</td>
            <td class="border1" colspan="1" style="text-align: center;"></td>
            <td class="border1" colspan="'.$memoWidth.'" style="text-align: center;"></td>
        </tr>';
                    $payment_information = $contractDetail->payment_information;
                    $count = 4;
                    if (count($payment_information) > $count){
                        $count = count($payment_information);
                    }
                    for ($i = 0; $i<$count; $i++){
                        $title = $payment_information[$i]['title'] ?? '';
                        $money = $payment_information[$i]['money'] ?? '';
                        $html .= '<tr>
                <td class="border1" colspan="7" style="text-align: center;">'.$title.'</td>
                <td class="border1" colspan="1" style="text-align: center;"></td>';

                        foreach ($rule as $size){
                            $html .= '<td class="border1" colspan="1" style="text-align: center;"></td>';
                        }

                        $html .= '
                <td class="border1" colspan="1" style="text-align: center;"></td>
                <td class="border1" colspan="1" style="text-align: center;"></td>
                <td class="border1" colspan="'.$priceWidth.'" style="text-align: center;">'.$money.'</td>
                <td class="border1" colspan="1" style="text-align: center;"></td>
                <td class="border1" colspan="'.$memoWidth.'" style="text-align: center;"></td>
            </tr>';
                    }
                    $html .= '<tr>
            <td colspan="20" style="text-align: center;"></td>
        </tr>

        <tr>
            <td colspan="20">2、需提供产前确认样（拍照样）齐色码'.implode('、', $rule).'各一件。</td>
        </tr>

        <tr>
            <td colspan="20">3、出货良品率保持99%，按合同规定的交货期交货，若逾期大于8天以上的，以规定合同交期而产生的交期逾期，以逾期货款金额为基数按每日万分之7. 5计算违约金扣款，≥30天除了扣款还需承担甲方运费。</td>
        </tr>
        <tr>
            <td colspan="20">（如有其他原因延期交货，双方友好协商交货日期，并以书面或者邮件给以通知，须甲方同意为准，不可延期后再进行延期补签申请）。</td>
        </tr>

        <tr>
            <td colspan="20">二、质量要求技术标准、乙方对质量负责的条件:</td>
        </tr>
        <tr>
            <td colspan="20">1、产品质量和材料以需方提供的原样一致及双方最终签章确认产前样为准！如有不一致，甲方有权取消本次合同。</td>
        </tr>
        <tr>
            <td colspan="20">2、必须符合国家法定商检标准及甲方提出的质量标准,按照AQL2.5质量标准进行验货。</td>
        </tr>
        <tr>
            <td colspan="20">三、交货地点、方式及运输费用</td>
        </tr>
        <tr>
            <td colspan="20">1、须按甲方提供的交货期及指定的地点交货，我方地址分别为：厦门同安仓、泉州仓、厦门软二仓。</td>
        </tr>
        <tr>
            <td colspan="20">2、乙方按甲方指定地点送货，在保证交货期情况下乙方可自主选择运输方式，运输费用乙方承担。</td>
        </tr>
        <tr>
            <td colspan="20">四、包装标准：按我司要求包装（详细按理单资料）</td>
        </tr>
        <tr>
            <td colspan="20">五、验收标准、方法及提出异议期限：</td>
        </tr>
        <tr>
            <td colspan="20">1、甲方派出的QC以确认样和国家法定商检标准为准，对产品的品质、规格、数量、包装等外观质检验。乙方须严格按甲方要求执行。</td>
        </tr>
        <tr>
            <td colspan="20">2、 按确认样和制作单要求生产。乙方未能按质按量按时交货引起的损失，概由乙方承担。</td>
        </tr>
        <tr>
            <td colspan="20">3、甲方在收货后有权对因工厂生产技术、材料等方面造成的质量问题提出索赔、严重者可提出退货及索赔，乙方应予以接受。</td>
        </tr>
        <tr>
            <td colspan="20">4、甲方需于到货后15天内进行抽验，若存在数量及质量问题的，需向乙方书面提出;逾期视为检验合格。 </td>
        </tr>
        <tr>
            <td colspan="20">5、大货只接受订单数量±3%（按各码数）。</td>
        </tr>
        <tr>
            <td colspan="20">六、货款的计算及结算方式：</td>
        </tr>
        <tr>
            <td colspan="20">1、付款方式：T/T</td>
        </tr>';

                    $html .= '
        <tr>
            <td colspan="20">'.$contractDetail->payment_settlement_method.'</td>
        </tr>';

                    $html .= '<tr>
            <td colspan="20">七、违约责任：</td>
        </tr>
        <tr>
            <td colspan="20">2、乙方所交货品与本合同规定质量不符导致甲方在转售该批服装时被客人拒绝接收或降价或其它救济方式时，甲方有权对乙方采取相应的措施，乙方不得拒绝。</td>
        </tr>
        <tr>
            <td colspan="20">3、因乙方不能按期交货，甲方不同意延期所引起的各项运费或制约款等费用由乙方承担。</td>
        </tr>
        <tr>
            <td colspan="20">八、解决合同纠纷方式：</td>
        </tr>
        <tr>
            <td colspan="20">合同履行过程中若发生纠纷，经友好协商未果提起诉讼，由签约地（厦门思明区）法院管辖。</td>
        </tr>
        <tr>
            <td colspan="20">九、其它约定事项:</td>
        </tr>
        <tr>
            <td colspan="20">1、由甲方提供的理单资料（即大货要求）均为本合同附件，其它未尽事宜可经双方商定进行书面修改补充并视为本合同不可分割的部分，同样具有法律效力。</td>
        </tr>
        <tr>
            <td colspan="20">2、大货材料必须由甲方确认后才可生产，否则由此引起的责任及后果由乙方承担;</td>
        </tr>
        <tr>
            <td colspan="20">3、甲方品牌产品、组合方式、设计款式、包装方式等待均不可销售其他电商卖家。如发现乙方向其他电商卖家销售以上规定的产品造成甲方损失应由乙方承担。</td>
        </tr>
        <tr>
            <td colspan="20">4、如果是因为乙方质量等原因，造成甲方派QC前往乙方工厂验货超过2次的，所产生的验货费用由乙方承担。 （根据验货地点费用起：500元-1000元以上）</td>
        </tr>
        <tr>
            <td colspan="20">5、乙方须在交货前免费提供符合甲方质量要求的初样，修改样，产前样，大货样，若不符合要求，须重新提供。</td>
        </tr>
        <tr>
            <td colspan="20">6、乙方必须做产前样给甲方确认后，方可开裁&生产大货，否则后果将由乙方负全部责任。</td>
        </tr>
        <tr>
            <td colspan="10" rowspan="1">'.$process_contract_no.'</td>
            <td colspan="10" rowspan="1">'.$process_supplier_name.'</td>
        </tr>
        <tr>
            <td colspan="20" rowspan="1"></td>
        </tr>
        <tr>
            <td colspan="20" rowspan="1"></td>
        </tr>
        <tr>
            <td colspan="20" rowspan="1"></td>
        </tr>
        <tr>
            <td colspan="2">甲方单位：</td>
            <td colspan="8">'.$contractDetail->company_name.'</td>
            <td colspan="2">乙方单位：</td>
            <td colspan="8">'.$contractDetail->supplier_name.'</td>
        </tr>
        <tr>
            <td colspan="2">地址：</td>
            <td colspan="8">'.$contractDetail->company_address.'</td>
            <td colspan="2">地址：</td>
            <td colspan="8">'.$contractDetail->supplier_address.'</td>
        </tr>
        <tr>
            <td colspan="10">甲方代表：'.$contractDetail->maker_name.'</td>
            <td colspan="10">乙方代表：</td>
        </tr>
        <tr>
            <td colspan="10">签章：</td>
            <td colspan="10">签章：</td>
        </tr>
        <tr>
            <td colspan="10">日期：'.$contractDetail->sign_date.'</td>
            <td colspan="10">日期：'.$contractDetail->sign_date.'</td>
        </tr>
    </table>
</body>
</html>';
                    break;
                case 8:
                    $printType = '购销合同-众拓范本(含税)-打印';
                    $html = '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table {
            border-collapse: collapse;
        }
        td, th {
            padding: 0;
        }
        table {
            border-collapse: separate;
            border-spacing: 0;
        }
        td,tr {
            font-size: 9px;
            line-height：9px;
        }
        td{
            width: 5%;
        }
        .border1{
            border-width: 0.5rem;
            border-style: double double double double;
            border-color: black;
            height: 8px;
        }
        .fontBlod{
            font-weight: bold;
        }
        .cell {
            display: flex;
	        justify-content: center;
	        align-items: center;
        }
    </style>
</head>
<body>
    <table   width="100%" cellpadding="2" cellspacing="0">
        <tr>
            <td colspan="20" style="text-align: center;font-size: 20px; font-weight: bold; padding-bottom: 20px;"><div class="cell">'.$contractDetail->company_name.'购销合同</div></td>
        </tr>
        <tr>
            <td colspan="20"></td>
        </tr>
        <tr>
            <td colspan="14"><div class="cell">甲方单位：'.$contractDetail->company_name.'(需方)</div></td>
            <td colspan="6"><div class="cell">合同编号：'.$contractDetail->contract_no.'</div></td>
        </tr>
        <tr>
            <td colspan="14"><div class="cell">乙方单位：'.$contractDetail->supplier_name.'(供方)</div></td>
            <td colspan="6"><div class="cell">签约时间：'.$contractDetail->sign_date.'</div></td>
        </tr>
        <tr>
            <td colspan="14"></td>
            <td colspan="6"><div class="cell">签约地点：'.$contractDetail->signing_location.'</div></td>
        </tr>
        <tr>
            <td colspan="14"></td>
            <td colspan="6"><div class="cell">交期时间：'.$contractDetail->report_date.'</div></td>
        </tr>
        <tr>
            <td colspan="20" style="text-align: center;"></td>
        </tr>
        <tr>
            <td colspan="20" style="text-align: left; font-weight: bold;"><div class="cell">为促进甲方、乙方双方合作顺利开展，保障双方权益，根据《中华人民共和国民法典》及相关法律法规的规定，经双方友好协商特订立本合同，供共同遵守。</div></td>
        </tr>
        <tr>
            <td colspan="20" style="text-align: center;"></td>
        </tr>
        <tr>
            <td colspan="20"><div class="cell">一、 1、甲方商品的款号/品名，数量，单价，金额（人民币）</div></td>
        </tr>

        <tr>
        <td class="border1" colspan="3" rowspan="2" style="text-align: center;"><div class="cell">款号</div></td>
            <td class="border1" colspan="2" rowspan="2" style="text-align: center;"><div class="cell">款式</div></td>
            <td class="border1" colspan="2" rowspan="2" style="text-align: center;"><div class="cell">品名</div></td>
            <td class="border1" colspan="'.($length+1).'" rowspan="1" style="text-align: center;"><div class="cell">颜色尺码</div></td>
            <td class="border1" colspan="1" rowspan="2" style="text-align: center;"><div class="cell">合计(件/套)</div></td>
            <td class="border1" colspan="1" rowspan="2" style="text-align: center;"><div class="cell">单价(元)(含税)</div></td>
            <td class="border1" colspan="'.$priceWidth.'" rowspan="2" style="text-align: center;"><div class="cell">金额(含税)</div></td>
            <td class="border1" colspan="1" rowspan="2" style="text-align: center;"><div class="cell">吊牌</div></td>
            <td class="border1" colspan="'.$memoWidth.'" rowspan="2" style="text-align: center;"><div class="cell">备注</div></td>
        </tr>';

                    $html .= '<tr>
            <td class="border1" colspan="1" rowspan="1" style="text-align: center;">颜色</td>';

                    foreach ($rule as $size){
                        $html .= '
            <td class="border1" colspan="1" style="text-align: center;">'.$size.'</td>';
                    }

                    $html .= '
        </tr>';
                    $contractSpu = $contractDetail->spu_items;
                    $sizeInfo = [];
                    $totalPrice = 0;
                    foreach ($contractSpu as $v){
                        $html .= '<tr>
                <td class="border1" colspan="3" style="text-align: center;">'.$v['spu'].'</td>';
                        if (empty($v['img'])){
                            $html .= '<td class="border1" colspan="2" style="text-align: center;"></td>';
                        }else{
                            $html .= '<td class="border1" colspan="2" style="text-align: center;"><img src="' . $v['img'] . '" width="70pt"></td>';
                        }
                        $html .= '<td class="border1" colspan="2" style="text-align: center;">'.$v['title'].'</td>
                <td class="border1" colspan="1" style="text-align: center;">'.$v['color_name'].'/'.$v['color_identifying'].'</td>';
                        $numInfo = $v['num_info'];
                        foreach ($rule as $size){
                            $num = $numInfo->$size ?? 0;
                            if (isset($sizeInfo[$size])){
                                $sizeInfo[$size] += $num;
                            }else{
                                $sizeInfo[$size] = $num;
                            }
                            $totalPrice += $v['one_price'] * $num;
                            $html .= '<td class="border1" colspan="1" style="text-align: center;">'.$num.'</td>';
                        }
                        $html .= '<td class="border1" colspan="1" rowspan="1" style="text-align: center;"><div class="cell">'.$v['count'].'</div></td>
                <td class="border1" colspan="1" rowspan="1" style="text-align: center;"><div class="cell">'.$v['one_price'].'</div></td>
                <td class="border1" colspan="'.$priceWidth.'" rowspan="1" style="text-align: center;"><div class="cell">'.$v['price'].'</div></td>
                <td class="border1" colspan="1" style="text-align: center;">'.$contractDetail->brand.'</td>
                <td class="border1" colspan="'.$memoWidth.'" style="text-align: center;">'.$v['memo'].'</td>
            </tr>';
                    }
                    $html .= '<tr>
            <td class="border1" colspan="7" style="text-align: center;">合计金额</td>
            <td class="border1" colspan="1" style="text-align: center;"></td>';
                    foreach ($rule as $size){
                        $num = $sizeInfo[$size] ?? 0;
                        $html .= '
            <td class="border1" colspan="1" style="text-align: center;">'.$num.'</td>';
                    }

                    $html .= '
            <td class="border1" colspan="1" style="text-align: center;">'.array_sum($sizeInfo).'</td>
            <td class="border1" colspan="1" style="text-align: center;"></td>
            <td class="border1" colspan="'.$priceWidth.'" style="text-align: center;">'.$baseModel->floorDecimal($totalPrice).'</td>
            <td class="border1" colspan="1" style="text-align: center;"></td>
            <td class="border1" colspan="'.$memoWidth.'" style="text-align: center;"></td>
        </tr>';
                    $payment_information = $contractDetail->payment_information;
                    $count = 4;
                    if (count($payment_information) > $count){
                        $count = count($payment_information);
                    }
                    for ($i = 0; $i<$count; $i++){
                        $title = $payment_information[$i]['title'] ?? '';
                        $money = $payment_information[$i]['money'] ?? '';
                        $html .= '<tr>
                <td class="border1" colspan="7" style="text-align: center;">'.$title.'</td>
                <td class="border1" colspan="1" style="text-align: center;"></td>';

                        foreach ($rule as $size){
                            $html .= '<td class="border1" colspan="1" style="text-align: center;"></td>';
                        }

                        $html .= '
                <td class="border1" colspan="1" style="text-align: center;"></td>
                <td class="border1" colspan="1" style="text-align: center;"></td>
                <td class="border1" colspan="'.$priceWidth.'" style="text-align: center;">'.$money.'</td>
                <td class="border1" colspan="1" style="text-align: center;"></td>
                <td class="border1" colspan="'.$memoWidth.'" style="text-align: center;"></td>
            </tr>';
                    }
                    $html .= '<tr>
            <td colspan="20" style="text-align: center;"></td>
        </tr>

        <tr>
            <td colspan="20">2、需提供产前确认样（拍照样）齐色码'.implode('、', $rule).'各一件。</td>
        </tr>

        <tr>
            <td colspan="20">3、出货良品率保持99%，按合同规定的交货期交货，若逾期大于8天以上的，以规定合同交期而产生的交期逾期，以逾期货款金额为基数按每日万分之7. 5计算违约金扣款，≥30天除了扣款还需承担甲方运费。</td>
        </tr>
        <tr>
            <td colspan="20">（如有其他原因延期交货，双方友好协商交货日期，并以书面或者邮件给以通知，须甲方同意为准，不可延期后再进行延期补签申请）。</td>
        </tr>

        <tr>
            <td colspan="20">二、质量要求技术标准、乙方对质量负责的条件:</td>
        </tr>
        <tr>
            <td colspan="20">1、产品质量和材料以需方提供的原样一致及双方最终签章确认产前样为准！如有不一致，甲方有权取消本次合同。</td>
        </tr>
        <tr>
            <td colspan="20">2、必须符合国家法定商检标准及甲方提出的质量标准,按照AQL2.5质量标准进行验货。</td>
        </tr>
        <tr>
            <td colspan="20">三、交货地点、方式及运输费用</td>
        </tr>
        <tr>
            <td colspan="20">1、须按甲方提供的交货期及指定的地点交货，我方地址分别为：厦门同安仓、泉州仓、厦门软二仓。</td>
        </tr>
        <tr>
            <td colspan="20">2、乙方按甲方指定地点送货，在保证交货期情况下乙方可自主选择运输方式，运输费用乙方承担。</td>
        </tr>
        <tr>
            <td colspan="20">四、包装标准：按我司要求包装（详细按理单资料）</td>
        </tr>
        <tr>
            <td colspan="20">五、验收标准、方法及提出异议期限：</td>
        </tr>
        <tr>
            <td colspan="20">1、甲方派出的QC以确认样和国家法定商检标准为准，对产品的品质、规格、数量、包装等外观质检验。乙方须严格按甲方要求执行。</td>
        </tr>
        <tr>
            <td colspan="20">2、 按确认样和制作单要求生产。乙方未能按质按量按时交货引起的损失，概由乙方承担。</td>
        </tr>
        <tr>
            <td colspan="20">3、甲方在收货后有权对因工厂生产技术、材料等方面造成的质量问题提出索赔、严重者可提出退货及索赔，乙方应予以接受。</td>
        </tr>
        <tr>
            <td colspan="20">4、甲方需于到货后15天内进行抽验，若存在数量及质量问题的，需向乙方书面提出;逾期视为检验合格。 </td>
        </tr>
        <tr>
            <td colspan="20">5、大货只接受订单数量±3%（按各码数）。</td>
        </tr>
        <tr>
            <td colspan="20">六、货款的计算及结算方式：</td>
        </tr>
        <tr>
            <td colspan="20">1、付款方式：T/T</td>
        </tr>';

                    $html .= '
        <tr>
            <td colspan="20">'.$contractDetail->payment_settlement_method.'</td>
        </tr>';

                    $html .= '<tr>
            <td colspan="20">七、违约责任：</td>
        </tr>
        <tr>
            <td colspan="20">2、乙方所交货品与本合同规定质量不符导致甲方在转售该批服装时被客人拒绝接收或降价或其它救济方式时，甲方有权对乙方采取相应的措施，乙方不得拒绝。</td>
        </tr>
        <tr>
            <td colspan="20">3、因乙方不能按期交货，甲方不同意延期所引起的各项运费或制约款等费用由乙方承担。</td>
        </tr>
        <tr>
            <td colspan="20">八、解决合同纠纷方式：</td>
        </tr>
        <tr>
            <td colspan="20">合同履行过程中若发生纠纷，经友好协商未果提起诉讼，由签约地（厦门思明区）法院管辖。</td>
        </tr>
        <tr>
            <td colspan="20">九、其它约定事项:</td>
        </tr>
        <tr>
            <td colspan="20">1、由甲方提供的理单资料（即大货要求）均为本合同附件，其它未尽事宜可经双方商定进行书面修改补充并视为本合同不可分割的部分，同样具有法律效力。</td>
        </tr>
        <tr>
            <td colspan="20">2、大货材料必须由甲方确认后才可生产，否则由此引起的责任及后果由乙方承担;</td>
        </tr>
        <tr>
            <td colspan="20">3、甲方品牌产品、组合方式、设计款式、包装方式等待均不可销售其他电商卖家。如发现乙方向其他电商卖家销售以上规定的产品造成甲方损失应由乙方承担。</td>
        </tr>
        <tr>
            <td colspan="20">4、如果是因为乙方质量等原因，造成甲方派QC前往乙方工厂验货超过2次的，所产生的验货费用由乙方承担。 （根据验货地点费用起：500元-1000元以上）</td>
        </tr>
        <tr>
            <td colspan="20">5、乙方须在交货前免费提供符合甲方质量要求的初样，修改样，产前样，大货样，若不符合要求，须重新提供。</td>
        </tr>
        <tr>
            <td colspan="20">6、乙方必须做产前样给甲方确认后，方可开裁&生产大货，否则后果将由乙方负全部责任。</td>
        </tr>
        <tr>
            <td colspan="10" rowspan="1">'.$process_contract_no.'</td>
            <td colspan="10" rowspan="1">'.$process_supplier_name.'</td>
        </tr>
        <tr>
            <td colspan="20" rowspan="1"></td>
        </tr>
        <tr>
            <td colspan="20" rowspan="1"></td>
        </tr>
        <tr>
            <td colspan="20" rowspan="1"></td>
        </tr>
        <tr>
            <td colspan="2">甲方单位：</td>
            <td colspan="8">'.$contractDetail->company_name.'</td>
            <td colspan="2">乙方单位：</td>
            <td colspan="8">'.$contractDetail->supplier_name.'</td>
        </tr>
        <tr>
            <td colspan="2">地址：</td>
            <td colspan="8">'.$contractDetail->company_address.'</td>
            <td colspan="2">地址：</td>
            <td colspan="8">'.$contractDetail->supplier_address.'</td>
        </tr>
        <tr>
            <td colspan="10">甲方代表：'.$contractDetail->maker_name.'</td>
            <td colspan="10">乙方代表：</td>
        </tr>
        <tr>
            <td colspan="10">签章：</td>
            <td colspan="10">签章：</td>
        </tr>
        <tr>
            <td colspan="10">日期：'.$contractDetail->sign_date.'</td>
            <td colspan="10">日期：'.$contractDetail->sign_date.'</td>
        </tr>
    </table>
</body>
</html>';
                    break;
            }
            // 加入队列
            $params['html'] = $html;
            $params['title'] = $printType;
            $this->_savePdf($params);
            return;
        }catch (\Exception $e){
            var_dump($e->getMessage().'；位置：'.$e->getLine());exit();
            throw new \Exception('数据获取节点错误：'.$e->getMessage().'；位置：'.$e->getLine());
        }
    }
}