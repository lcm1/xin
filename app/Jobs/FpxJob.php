<?php

namespace App\Jobs;

use App\Http\Controllers\Pc\WalmartApiController;
use App\Libs\wrapper\Task;
use App\Models\BaseModel;
use App\Models\FpxModel;
use App\Models\InventoryModel;
use App\Models\TaskClass;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use phpseclib3\Math\BigInteger\Engines\PHP;

class FpxJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $params;

    //FPX订单类型对应的平台
    protected $platformIdKeyByType = [
        1 => 5,
        2 => 14,
        3 => 3,
    ];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        $this->params = $params;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $date = date('Y-m-d H:i:s', microtime(true));
            echo $date . ' fpx';

            $orderIds = $this->params['order_ids'];
            $orderIds = array_unique(explode(',', $orderIds));


            foreach ($orderIds as $orderId) {
                $this->autoDeliverGoods($orderId);
            }


            echo ' 执行成功！';
        } catch (\Exception $e) {
            //消化失败，记录错误日志
            $add['job']  = 'fpx';
            $add['time'] = time();
            $add['msg']  = $e->getMessage();
            $add['data'] = json_encode($this->params);
            Db::table('job_err_log')->insert($add);
        }

    }


    //自动发货
    public function autoDeliverGoods($orderId)
    {
        $type = $this->params['type'];

        $platformId = $this->platformIdKeyByType[$type];

        switch ($type) {
            case 1://亚马逊
                $orderItemList = DB::table('lingxing_amazon_order_item')
                    ->select([
                        'id',
                        'lingxing_amazon_order_id as local_order_id',
                        'amazon_order_id as order_id',
                        'shop_id',
                        'quantity as num',
                        'msku as sku',
                        'local_sku as custom_sku',
                        'custom_sku_id',
                        'spu_id',
                        'sku_id',
                        'spu',
                    ])
                    ->where('lingxing_amazon_order_id', $orderId)
                    ->get();

                if ($orderItemList->isEmpty()) {
                    return true;
                }

                $address = DB::table('lingxing_amazon_order')
                    ->select([
                        'receiver_name as first_name',
                        'receiver_mobile as phone',
                        'postal_code as post_code',
                        'receiver_country_code as country',
                        'state_or_region as state',
                        'city',
                        'address_line1 as street',
                        'address_line2',
                        'address_line3',
                    ])
                    ->where('id', $orderItemList[0]->local_order_id)
                    ->first();
                if ($address) {
                    if ($address->address_line2) {
                        $address->state .= ' , ' . $address->address_line2;
                    }
                    if ($address->address_line3) {
                        $address->state .= ' , ' . $address->address_line3;
                    }

                    unset($address->address_line2, $address->address_line3);
                }

                break;
            case 2:
                $orderItemList = DB::table('walmart_order_item')
                    ->select([
                        'id',
                        'walmart_order_id as local_order_id',
                        'purchase_order_id as order_id',
                        'shop_id',
                        'order_line_quantity_amount as num',
                        'sku',
                        'custom_sku_id',
                        'spu_id',
                        'sku_id',
                        'spu',
                        'line_number',// 回填物流单号要用
                    ])
                    ->where('walmart_order_id', $orderId)
                    ->get();

                if ($orderItemList->isEmpty()) {
                    return true;
                }

                $address = DB::table('walmart_order')
                    ->select([
                        'postal_address_name as first_name',
                        'phone',
                        'postal_code as post_code',
                        DB::raw('left(country, 2) country'),
                        'postal_address_state as state',
                        'city',
                        'address1 as street',
                        'address2',
                    ])
                    ->where('id', $orderItemList[0]->local_order_id)
                    ->first();

                if ($address && $address->address2) {
                    $address->street .= ' , ' . $address->address2;
                }
                unset($address->address2);

                break;
            default:
                return true;

        }

        $address = json_decode(json_encode($address), true);
        if (!$address['first_name'] ||
            !$address['phone'] ||
            !$address['post_code'] ||
            !$address['country'] ||
            !$address['state'] ||
            !$address['city'] ||
            !$address['street']
        ) {

            self::writeLog(
                'FPX',
                [
                    'order_id' => $orderId,
                    'type'     => $type,
                ],
                '买家信息不完整'
            );

            $this->updateAutoShipmentsLog($orderId, $type, 2, '买家信息不完整');
            return true;
        }


        $operationUserId      = 0;//运营人员id
        $itemKeyByCustomSkuId = [];
        foreach ($orderItemList as $orderItem) {
            //先匹配库存sku
            if (!$orderItem->custom_sku_id || !$orderItem->sku_id) {

                $skuInfo = DB::table("self_sku")->where('sku', $orderItem->sku)->orwhere('old_sku', $orderItem->sku)->first();

                if (!$skuInfo) {
                    self::writeLog(
                        'FPX',
                        [
                            'order_id' => $orderId,
                            'type'     => $type,
                        ],
                        'sku暂未录入系统，无法发货'
                    );
                    $this->updateAutoShipmentsLog($orderId, $type, 2, 'sku暂未录入系统，无法发货');
                    return false;
                }
                $operationUserId = $skuInfo->user_id;
                $updateArr       = [
                    'spu_id'        => $skuInfo->spu_id,
                    'spu'           => $skuInfo->spu,
                    'custom_sku_id' => $skuInfo->custom_sku_id,
                    'sku_id'        => $skuInfo->id,
                ];

                if ($type == 1) {
                    DB::table('lingxing_amazon_order_item')->where('id', $orderItem->id)->update($updateArr);
                } elseif ($type == 2) {
                    DB::table('walmart_order_item')->where('id', $orderItem->id)->update($updateArr);
                }

                $orderItem->custom_sku_id = $skuInfo->custom_sku_id;
                $orderItem->sku_id        = $skuInfo->id;
            }

            if (isset($itemKeyByCustomSkuId [$orderItem->custom_sku_id])) {
                $itemKeyByCustomSkuId [$orderItem->custom_sku_id]['num'] += $orderItem->num;
            } else {
                $itemKeyByCustomSkuId [$orderItem->custom_sku_id]['custom_sku_id'] = $orderItem->custom_sku_id;
                $itemKeyByCustomSkuId [$orderItem->custom_sku_id]['num']           = $orderItem->num;
                $itemKeyByCustomSkuId [$orderItem->custom_sku_id]['sku_id']        = $orderItem->sku_id;
            }
        }


        $customSkuListKeyId = DB::table('self_custom_sku')
            ->select('id', 'custom_sku', 'old_custom_sku', 'name')
            ->whereIn('id', $orderItemList->pluck('custom_sku_id'))
            ->get()
            ->keyBy('id');

        $titleKeySkuId = DB::table('product_detail')
            ->whereIn('sku_id', array_unique(array_column($itemKeyByCustomSkuId, 'sku_id')))
            ->pluck('title', 'sku_id');

        $productParams        = [];//组装 生成 4px单 产品参数
        $skuData              = [];//生成出库单参数
        $fpxOrderDetailInsert = [];//4px单详情插入

        foreach ($itemKeyByCustomSkuId as $customSkuId => $item) {
            if (!isset($customSkuListKeyId[$customSkuId])) {

                self::writeLog(
                    'FPX',
                    [
                        'order_id' => $orderId,
                        'type'     => $type,
                    ],
                    "库存sku不存在：$customSkuId"
                );

                $this->updateAutoShipmentsLog($orderId, $type, 2, "库存sku不存在：$customSkuId");

                return false;
            }


            $customSkuInfo = $customSkuListKeyId[$customSkuId];
            $customSku     = $customSkuInfo->old_custom_sku ?: $customSkuInfo->custom_sku;

            $productParams[] = [
                'custom_sku_id' => $customSkuId,
                'custom_sku'    => $customSku,
                'name'          => $customSkuInfo->name,
                'num'           => $item['num'],
                'title'         => isset($titleKeySkuId[$item['sku_id']])
                    ? ($titleKeySkuId[$item['sku_id']] ?: 'noname')
                    : 'noname',
            ];

            $skuData[] = [
                'custom_sku' => $customSku,
                'num'        => $item['num'],
            ];

            $fpxOrderDetailInsert[] = [
                'custom_sku_id' => $customSkuId,
                'custom_sku'    => $customSku,
                'num'           => $item['num'],
            ];
        }

        //判断库存
        $warehouseId = $this->params['warehouse_id'];
        $res         = $this->judgeInventory($productParams, $warehouseId, $platformId);
        if ($res !== true) {
            $errInfo1 = [
                'custom_sku'   => implode(',', $res),
                'warehouse_id' => $warehouseId,
            ];

            $warehouseId = $warehouseId == 1 ? 2 : 1;//默认仓不够选其他仓库

            $res = $this->judgeInventory($productParams, $warehouseId, $platformId);

            if ($res !== true) {
                $errInfo2 = [
                    'custom_sku'   => implode(',', $res),
                    'warehouse_id' => $warehouseId,
                ];
                self::writeLog(
                    'FPX',
                    [
                        'order_id' => $orderId,
                        'type'     => $type,
                        'detail'   => [$errInfo1, $errInfo2]
                    ],
                    '库存不足'
                );

                $this->updateAutoShipmentsLog($orderId, $type, 2, implode(',', $res) . '库存不足');

                return false;
            }
        }


        DB::beginTransaction();
        //生成出库清单
        $invetoryModel = new InventoryModel();

        $userId                      = $this->params['user_id'];
        $shopId                      = $orderItemList[0]->shop_id;
        $outInventory['out_house']   = $warehouseId;
        $outInventory['user_id']     = $userId;
        $outInventory['skuData']     = $skuData;
        $outInventory['type']        = 2;
        $outInventory['shop_id']     = $shopId;
        $outInventory['type_detail'] = 12;
        $outInventory['platform_id'] = $this->platformIdKeyByType[$type];

        $outMsg = $invetoryModel->goods_transfers($outInventory);

        if ($outMsg['code'] != 200) {
            DB::rollback();

            self::writeLog(
                'FPX',
                [
                    'order_id'     => $orderId,
                    'type'         => $type,
                    'outInventory' => $outInventory,
                ],
                '生成出库清单失败' . $outMsg['msg']
            );

            $this->updateAutoShipmentsLog($orderId, $type, 2, '生成出库清单失败' . $outMsg['msg']);
            return false;
        }

        $pushMsg = $invetoryModel->goods_cloud(['order_no' => $outMsg['order_no']]);

        if ($pushMsg['code'] != 200) {
            DB::rollback();

            self::writeLog(
                'FPX',
                [
                    'order_id' => $orderId,
                    'type'     => $type,
                ],
                '推送失败' . $pushMsg['msg']
            );

            $this->updateAutoShipmentsLog($orderId, $type, 2, '推送失败' . $pushMsg['msg']);

            return false;
        }

        //生成出库单任务
        $classId       = $pushMsg['data']['class_id'];
        $transfersId   = $pushMsg['data']['transfers_id'];
        $transfersLink = $pushMsg['data']['transfers_link'];

        if (!$operationUserId) {
            $operationUserId = DB::table("self_sku")
                ->where('sku', $orderItemList[0]->sku)
                ->orwhere('old_sku', $orderItemList[0]->sku)
                ->value('user_id');
        }

        $taskObj = new Task();
        $taskId  = DB::table('task_class')->where('id', $classId)->value('task_id');

        $taskInfo                         = $taskObj->task_info(['task_id' => $taskId]);
        $taskAdd                          = $taskInfo['basic'];
        $taskAdd['node']                  = $taskInfo['node'];
        $taskAdd['user_fz']               = $operationUserId;
        $taskAdd['name']                  = $taskAdd['name'] . $transfersId;
        $taskAdd['account_fz']            = $userId;
        $taskAdd['ext']['transfers_id']   = $transfersId;
        $taskAdd['ext']['transfers_link'] = $transfersLink;
        $taskAddMsg                       = $taskObj->task_add($taskAdd);
        if (!is_array($taskAddMsg) || $taskAddMsg['type'] == 'fail') {
            DB::rollback();

            self::writeLog(
                'FPX',
                [
                    'order_id' => $orderId,
                    'type'     => $type,
                ],
                ['生成出库单失败:' . $taskAddMsg]
            );

            $this->updateAutoShipmentsLog($orderId, $type, 2, '生成出库单失败:' . $taskAddMsg);

            return false;
        }

        //生成4px单号
        $params = [
            'template_id'    => $this->params['template_id'],
            'product_params' => $productParams,
            'recipient_info' => $address,//买家信息
        ];
        $fpxRes = $this->getFpx($params);
        if ($fpxRes['msg'] == 'System processing failed') {
            DB::rollback();

            self::writeLog(
                'FPX',
                [
                    'order_id' => $orderId,
                    'type'     => $type,
                ],
                '生成4px单号失败:' . $fpxRes['errors'][0]['error_msg']
            );

            $this->updateAutoShipmentsLog($orderId, $type, 2, '生成4px单号失败:' . $fpxRes['errors'][0]['error_msg']);

            return false;
        }

        $fpxRes                         = $fpxRes['data'];
        $insert['order_id']             = $orderItemList[0]->order_id;
        $insert['shop_id']              = $shopId;
        $insert['type']                 = $type;
        $insert['request_no']           = $fpxRes['4px_tracking_no'];//4PX跟踪号
        $insert['ds_consignment_no']    = $fpxRes['ds_consignment_no'];//直发委托单号
        $insert['label_barcode']        = $fpxRes['label_barcode']; //@ deprecated   标签条码号。*注：参数为deprecated状态
        $insert['ref_no']               = $fpxRes['ref_no']; //客户单号/客户参考号
        $insert['logistics_channel_no'] = $fpxRes['logistics_channel_no'];//物流渠道号码。如果结果返回为空字符，表示暂时没有物流渠道号码，请稍后主动调用查询直发委托单接口查询

        $insert['create_time']    = date('Y-m-d H:i:s', time());
        $insert['status']         = 0;
        $insert['user_id']        = $userId;
        $insert['warehouse_id']   = $warehouseId;
        $insert['recipient_info'] = json_encode($address);//买家信息
        $insert['box_id']         = $pushMsg['box_id'];
        $insert['out_no']         = $outMsg['order_no'];

        DB::table('fpx_order')->insert($insert);

        foreach ($fpxOrderDetailInsert as $key => $value) {
            $fpxOrderDetailInsert[$key]['request_no'] = $fpxRes['4px_tracking_no'];//4PX跟踪号
        }

        DB::table('fpx_order_detail')->insert($fpxOrderDetailInsert);

        DB::commit();


        if ($type == 1) {
            //回填亚马逊物流单号
            $shipmentRes = $this->amazonConfirmShipment($shopId, $orderItemList[0]->order_id, $fpxRes['4px_tracking_no']);
            //更新发货日志
            if (isset($shipmentRes['code']) && $shipmentRes['code'] == 200) {
                $this->updateAutoShipmentsLog($orderId, $type, 1);
            } else {
                $msg = '';
                isset($shipmentRes['msg']) && $msg = $shipmentRes['msg'];
                $this->updateAutoShipmentsLog($orderId, $type, 2, '回填物流单号失败：' . $msg);
            }

        } elseif ($type == 2) {
            //回填沃尔玛物流单号
//            $shipmentRes = $this->walmartShipping($orderItemList, $fpxRes['logistics_channel_no']);

            //更新状态为待回填物流单号
            $this->updateAutoShipmentsLog($orderId, $type, 3);
        }

        return true;
    }

    //生成4px单号
    public function getFpx($params)
    {
        $template_id = $params['template_id'];
        $template    = DB::table('fpx_template')->where('id', $template_id)->value('template');
        $template    = json_decode($template, true);

        //买家信息
        $template['recipient_info'] = $params['recipient_info'];

        $template["4px_tracking_no"] = '';
        $template["ref_no"]          = 'Z' . time() . rand(100, 999) . 'T';

        //模版信息
        //包裹寄出信息
        $weight          = $template['parcel_list'][0]['weight'];
        $length          = $template['parcel_list'][0]['length'];
        $width           = $template['parcel_list'][0]['width'];
        $height          = $template['parcel_list'][0]['height'];
        $parcel_value    = $template['parcel_list'][0]['parcel_value'];
        $include_battery = $template['parcel_list'][0]['include_battery'];
        $battery_type    = $template['parcel_list'][0]['battery_type'];
        //投保物
        $product_unit_price       = $template['parcel_list'][0]['product_list'][0]['product_unit_price'];
        $currency                 = $template['parcel_list'][0]['product_list'][0]['currency'];
        $standard_product_barcode = $template['parcel_list'][0]['product_list'][0]['standard_product_barcode'];
        //海关申报
        $declare_product_code      = $template['parcel_list'][0]['declare_product_info'][0]['declare_product_code'];
        $de_uses                   = $template['parcel_list'][0]['declare_product_info'][0]['uses'];
        $specification             = $template['parcel_list'][0]['declare_product_info'][0]['specification'];
        $component                 = $template['parcel_list'][0]['declare_product_info'][0]['component'];
        $unit_net_weight           = $template['parcel_list'][0]['declare_product_info'][0]['unit_net_weight'];
        $unit_gross_weight         = $template['parcel_list'][0]['declare_product_info'][0]['unit_gross_weight'];
        $material                  = $template['parcel_list'][0]['declare_product_info'][0]['material'];
        $unit_declare_product      = $template['parcel_list'][0]['declare_product_info'][0]['unit_declare_product'];
        $origin_country            = $template['parcel_list'][0]['declare_product_info'][0]['origin_country'];
        $country_export            = $template['parcel_list'][0]['declare_product_info'][0]['country_export'];
        $country_import            = $template['parcel_list'][0]['declare_product_info'][0]['country_import'];
        $hscode_export             = $template['parcel_list'][0]['declare_product_info'][0]['hscode_export'];
        $hscode_import             = $template['parcel_list'][0]['declare_product_info'][0]['hscode_import'];
        $declare_unit_price_export = $template['parcel_list'][0]['declare_product_info'][0]['declare_unit_price_export'];
        $currency_export           = $template['parcel_list'][0]['declare_product_info'][0]['currency_export'];
        $declare_unit_price_import = $template['parcel_list'][0]['declare_product_info'][0]['declare_unit_price_import'];
        $brand_export              = $template['parcel_list'][0]['declare_product_info'][0]['brand_export'];
        $brand_import              = $template['parcel_list'][0]['declare_product_info'][0]['brand_import'];
        $sales_url                 = $template['parcel_list'][0]['declare_product_info'][0]['sales_url'];
        $currency_import           = $template['parcel_list'][0]['declare_product_info'][0]['currency_import'];


        //包裹信息
        $parcel['weight']          = $weight;//预报重量（g）
        $parcel['length']          = $length;//包裹长（cm）
        $parcel['width']           = $width;//包裹宽（cm）
        $parcel['height']          = $height;//	包裹高（cm）
        $parcel['parcel_value']    = $parcel_value;//包裹申报价值（最多4位小数）
        $parcel['currency']        = $currency;//包裹申报价值币别（按照ISO标准三字码；支持的币种，根据物流产品+收件人国家配置；币种需和进出口国申报币种一致）
        $parcel['include_battery'] = $include_battery;//是否含电池（Y/N）
        $parcel['battery_type']    = $battery_type;//是否含电池（Y/N）

        //包裹列表
        foreach ($params['product_params'] as $param) {
            # code...
            //包裹投保物
            $en_name = (new FpxModel())->cutSubstr($param['title']);

            $product['sku_code']                 = $param['custom_sku']; //[@即将废弃]投保SKU（客户自定义SKUcode）（数字或字母或空格）
            $product['product_name']             = $param['name'];//[@即将废弃]投保商品名称
            $product['qty']                      = $param['num'];//	[@即将废弃]投保商品数量（单位为pcs）
            $product['product_description']      = $en_name;//[@即将废弃]投保商品描述
            $product['standard_product_barcode'] = $standard_product_barcode; //[@即将废弃]投保商品标准条码（UPC、EAN、JAN…）
            $product['product_unit_price']       = $product_unit_price;//[@即将废弃]投保商品单价（按对应币别的法定单位，最多4位小数点）
            $product['currency']                 = $currency;//[@即将废弃]投保商品单价币别（按照ISO标准三字码；支持的币种，根据物流产品+收件人国家配置；币种需和进出口国申报币种一致）
            //海关申报信息
            $declare_product['declare_product_name_cn']  = $param['name'];//申报品名(当地语言)
            $declare_product['declare_product_name_en']  = $en_name;//申报品名（英语）
            $declare_product['declare_product_code_qty'] = $param['num'];//申报数量
            $declare_product['package_remarks']          = $param['custom_sku'] . '*' . $param['num'];//配货字段（打印标签选择显示配货信息是将会显示：package_remarks*qty）

            $declare_product['declare_product_code']      = $declare_product_code;//申报产品代码（在4PX已备案申报产品的代码）
            $declare_product['uses']                      = $de_uses;//用途
            $declare_product['specification']             = $specification;//规格
            $declare_product['component']                 = $component;//成分
            $declare_product['unit_net_weight']           = $unit_net_weight;//单件商品净重（默认以g为单位）
            $declare_product['unit_gross_weight']         = $unit_gross_weight;//单件商品毛重（默认以g为单位）
            $declare_product['material']                  = $material;//材质
            $declare_product['unit_declare_product']      = $unit_declare_product;//单位（点击查看详情；默认值：PCS）
            $declare_product['origin_country']            = $origin_country;//原产地（ISO标准2字码）点击查看详情
            $declare_product['country_export']            = $country_export;//出口国/起始国/发件人国家（ISO标准2字码）
            $declare_product['country_import']            = $country_import;//进口国/目的国/收件人国家（ISO标准2字码）
            $declare_product['hscode_export']             = $hscode_export;//出口国/起始国/发件人国家_海关编码(只支持数字)
            $declare_product['hscode_import']             = $hscode_import;//进口国/目的国/收件人国家_海关编码(只支持数字)
            $declare_product['declare_unit_price_export'] = $declare_unit_price_export;//出口国/起始国/发件人国家_申报单价（按对应币别的法定单位，最多4位小数点）
            $declare_product['currency_export']           = $currency_export;//出口国/起始国/发件人国家_申报单价币种（按照ISO标准；支持的币种，根据物流产品+收件人国家配置；币种需和进口国申报币种一致）
            $declare_product['declare_unit_price_import'] = $declare_unit_price_import;//USD	进口国/目的国/收件人国家_申报单价币种（按照ISO标准；支持的币种，根据物流产品+收件人国家配置；币种需和出口国申报币种一致）
            $declare_product['brand_export']              = $brand_export;//出口国/起始国/发件人国家_品牌(必填；若无，填none即可)
            $declare_product['brand_import']              = $brand_import;//进口国/目的国/收件人国家_品牌(必填；若无，填none即可)
            $declare_product['sales_url']                 = $sales_url;//配货字段（打印标签选择显示配货信息是将会显示：package_remarks*qty）
            $declare_product['currency_import']           = $currency_import;//进口国/目的国/收件人国家_申报单价币种（按照ISO标准；支持的币种，根据物流产品+收件人国家配置；币种需和出口国申报币种一致）

            $product_list[]         = $product;
            $declare_product_info[] = $declare_product;
        }


        $parcel['product_list']         = $product_list;
        $parcel['declare_product_info'] = $declare_product_info;//[@即将废弃]投保物品信息（投保、查验、货物丢失作为参考依据）
        $parcel_list[]                  = $parcel;
        $template['parcel_list']        = $parcel_list;

        $data['method'] = 'ds.xms.order.create';
        $data['vesion'] = '1.1.0';
        $data['data']   = $template;


        $fpxRes = (new FpxModel())->getFpx($data);
        $fpxRes = json_decode($fpxRes, true);
//        dump($fpxRes);


//        $fpxRes = [
//            'data' => [
//                "4px_tracking_no"      => "4PX3000750506910CN",
//                "ds_consignment_no"    => "DS4PX3000750506910CN",
//                "label_barcode"        => "4PX3000750506910CN",
//                "logistics_channel_no" => "",
//                "oda_result_sign"      => "N",
//                "ref_no"               => "Z1695611234802T",
//            ],
//            'msg'  => 'System processing succeeded'
//        ];

        return $fpxRes;
    }

    //判断仓库仍在是否充足
    public function judgeInventory($skuData, $warehouseId, $platformId)
    {
        $skuErr = [];

        foreach ($skuData as $skuInfo) {
            $inventoryDetail = (new BaseModel())->inventoryPlatformDetail($skuInfo['custom_sku_id'], $warehouseId, [$platformId]);
            $inventoryDetail = array_column($inventoryDetail, null, 'platform_id');
            if ($inventoryDetail[$platformId]['intsock_num'] < $skuInfo['num']) {
                $skuErr[] = $skuInfo['custom_sku'];
            }
        }

        if ($skuErr) {
            return $skuErr;
        }

        return true;
    }

    //亚马逊物流单号回填
    public function amazonConfirmShipment($shopId, $amazonOrderId, $fpxNo)
    {
        $url = "http://tpapi.zity.cn//Amazon/confirmShipment?shop_id=$shopId&amazon_order_id=$amazonOrderId&4px_no=$fpxNo";

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE); // https请求 不验证证书和hosts
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $output = curl_exec($curl);

        return json_decode($output, true);
    }

//    //沃尔玛物流单号回填
//    public function walmartShipping($orderItemList, $logisticsChannelNo)
//    {
//        $match = substr($logisticsChannelNo, 0, 2);
//
//        if ($match != '42') {
//            //物流渠道号码 不是42开头，不回填物流单号
//            return ['code' => '500', 'msg' => '物流渠道号码不正确'];
//        }
//
//        $orderId = $orderItemList[0]->order_id;
//
//        $orderLines = [];
//        foreach ($orderItemList as $orderItem) {
//            $orderLines['orderLine'][] = [
//                'lineNumber'        => $orderItem->line_number,
//                'sellerOrderId'     => $orderItem->order_id,
//                'orderLineStatuses' => [
//                    'orderLineStatus' => [
//                        [
//                            'status'         => 'Shipped',
//                            'statusQuantity' => [
//                                'unitOfMeasurement' => 'EACH',
//                                'amount'            => $orderItem->num,//数量
//                            ],
//                            'trackingInfo'   => [
//                                'shipDateTime'   => time() * 1000,
//                                'carrierName'    => ['carrier' => '4PX'],
//                                'methodCode'     => 'Standard',
//                                'trackingNumber' => $logisticsChannelNo,
//                            ],
//                        ]
//                    ]
//                ],
//            ];
//        }
//
//        $params = ['orderShipment' => ['orderLines' => $orderLines]];
//
//        $shop = (new BaseModel())->GetShop($orderItemList[0]->shop_id);
//
//        $accessToken = json_decode($shop['access_token'], true);
//        $walmart     = new WalmartApiController($shop['Id'], $accessToken['client_id'], $accessToken['client_secret']);
//
//        $result = $walmart->walmartApi('post', "/v3/orders/$orderId/shipping", $params);
//
//        if (is_null($result) || isset($result['errors'])) {
//            $msg = '';
//            isset($result['errors']) && $msg = json_encode($result, JSON_UNESCAPED_UNICODE);
//            return ['code' => 500, 'msg' => $msg];
//        }
//
//        return ['code' => 200];
//    }


    //更新自动发货日志
    public function updateAutoShipmentsLog($localOrderId, $type, $status = 1, $msg = '')
    {
        $lastId = DB::table('auto_shipments_log')
            ->where('local_order_id', $localOrderId)
            ->where('type', $type)
            ->orderBy('id', 'desc')
            ->value('id');


        DB::table('auto_shipments_log')
            ->where('id', $lastId)
            ->update([
                'status'      => $status,
                'msg'         => is_array($msg) ? json_encode($msg, JSON_UNESCAPED_UNICODE) : $msg,
                'update_time' => date('Y-m-d H:i:s'),
            ]);
    }


    private static function writeLog($name, $params = [], $res = '')
    {
        $log = var_export([
            'name'   => $name,
            'params' => $params ? json_encode($params, JSON_UNESCAPED_UNICODE) : '',
            'res'    => is_array($res) ? json_encode($res, JSON_UNESCAPED_UNICODE) : $res,
        ], true);

        $logFile = fopen(
            storage_path('logs' . DIRECTORY_SEPARATOR . 'fpx_job.log'),
            'a+'
        );
        fwrite($logFile, date('Y-m-d H:i:s') . ': ' . $log . PHP_EOL);
        fclose($logFile);
    }


}
