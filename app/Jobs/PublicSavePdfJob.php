<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class PublicSavePdfJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $params;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        //
        $this->params = $params;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        echo '队列PublicSavePdfJob';
        // var_dump($this->params);
        try {
            $params = $this->params;
            $insert['name'] = $params['title'];
            $insert['type'] = $params['type'];
            $insert['user_id'] = $params['user_id'];
            $insert['create_time'] = date('Y-m-d H:i:s',time());
            $insert['status'] = 0;
            $insert['msg'] = '生成中';
            $insert['path'] = '';
            $insert['download_type'] = 2;

            $task_id = Db::table('excel_task')->insertGetId($insert);

            $params['task_id'] = $task_id;
            //消化队列
            $this->_savePdf($params);

        } catch(\Exception $e){

            //消化失败，记录错误日志
            // var_dump($e);
            $errmsg = $e->getMessage().'line:'.$e->getLine();
            // $errmsg = json_encode($e);
            $add['job'] = 'public_save_pdf';
            $add['time'] = time();
            $add['msg'] =  $errmsg;
            $add['data'] = json_encode($this->params);
            Db::table('job_err_log')->insert($add);
        }
    }

    protected function _savePdf($params)
    {
        try {
            require_once(app_path() . DIRECTORY_SEPARATOR . 'Libs' . DIRECTORY_SEPARATOR . 'tcpdf' . DIRECTORY_SEPARATOR . 'examples' . DIRECTORY_SEPARATOR . 'tcpdf_include.php');
            $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

            // 设置文档信息
            $pdf->SetCreator('L');
            $pdf->SetAuthor('L');
            $pdf->SetTitle($params['title']);
            $pdf->SetSubject($params['title']);
            $pdf->SetKeywords('TCPDF, PDF, PHP');

            // 设置页眉和页脚信息
            $pdf->SetHeaderData('', 0, '', '', [0, 0, 0], [255, 255, 255]);
            $pdf->setFooterData([0, 0, 0], [255, 255, 255]);

            // 设置页眉和页脚字体
            $pdf->setHeaderFont(['stsongstdlight', '', '10']);
            $pdf->setFooterFont(['stsongstdlight', '', '8']);

            //删除预定义的打印 页眉/页尾
//            $pdf->setPrintHeader($result['is_header'] ?? false);
//            $pdf->setPrintFooter($result['is_footer'] ?? false);

            // 设置默认等宽字体
            $pdf->SetDefaultMonospacedFont('courier');

            // 设置字体
//            $pdf->setFont('times');

            // 设置间距
            $pdf->SetMargins(10, 15, 15);//页面间隔
            $pdf->SetHeaderMargin(5);//页眉top间隔
            $pdf->SetFooterMargin(5);//页脚bottom间隔

            // 设置分页
            $pdf->SetAutoPageBreak(TRUE, 15);
            $pdf->setFontSubsetting(true);

            // set default font subsetting mode
            $pdf->setFontSubsetting(true);

            //设置字体 stsongstdlight支持中文
            $pdf->SetFont('stsongstdlight', '', 10);

            $pdf->Ln(5);

            // 设置密码
//            $pdf->SetProtection($permissions = array('print', 'modify', 'copy', 'annot-forms', 'fill-forms', 'extract', 'assemble', 'print-high'), $user_pass = '123456', $owner_pass = null, $mode = 0, $pubkeys = null );

            //第一页
            $pdf->AddPage();
            $pdf->writeHTML($params['html'], true, false, true, true, 'center');

            //输出PDF
            $path = 'pdf' . DIRECTORY_SEPARATOR. time() .$params['title'].$params['id'].'.pdf';
            $pdf->Output(public_path().DIRECTORY_SEPARATOR.$path, 'F');//I输出、D下载
            Db::table('excel_task')->where('id', $params['task_id'])->update([
                'status' => 1,
                'msg' => '打印成功！',
                'path' => $path
            ]);
        }catch (\Exception $e){
            Db::table('excel_task')->where('id', $params['task_id'])->update([
                'status' => 2,
                'msg' => $e->getMessage().'；'.$e->getLine()
            ]);
        }
    }
}