<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class AutoDayreportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $params;
    protected $type;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        //
        $this->params = $params;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {


        echo '单独执行';
        $datawhole = new \App\Models\Datawhole();
        $msg = $datawhole->ReportAutoM(['user_id'=>270]);

        var_dump($msg);

        echo "\n";

        echo '自动填写日志队列1！';
        try {
            //code...
            $this->RunJob();
        } catch (\Throwable $th) {
            throw $th;
        }
  
        echo '自动填写日志队列2！';

        try {
            //code...
            $this->RunJob();
        } catch (\Throwable $th) {
            throw $th;
        }


     
    }

    public function RunJob(){
        $amazon_fasin = db::table('amazon_fasin as a')->leftjoin('users as b','a.user_id','=','b.Id')->leftjoin('organizes_member as c','c.user_id','=','b.Id')->where('c.organize_ids','like','%'.'22'.'%')->where('b.state',1)->get()->toarray();
        $users = array_unique(array_column($amazon_fasin,'user_id'));



        $datawhole = new \App\Models\Datawhole();
        foreach ($users as $v) {
            # code...
            // if($v==312){
                // echo $v."\n";

                try {
                    //code...
                    $msg = $datawhole->ReportAutoM(['user_id'=>$v]);
                } catch (\Throwable $e) {
                    //throw $th;
                    Redis::Hset('dayreport_err_log:users', $v, '失败'.$e->getMessage().'-'.$e->getLine());
       
                }

                // var_dump($msg['type']);
                // if($msg['type']!='success'){
                //     // $msg['user_id'] = $v;
                //     // var_dump($msg);
                //     Redis::Hset('dayreport_err_log:users', $v, json_encode($msg));
                // }
                Redis::Hset('dayreport_err_log:users', $v, json_encode($msg));
  
            // }
        }

        echo '结束';
        return;
     
    }

}