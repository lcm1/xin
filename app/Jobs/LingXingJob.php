<?php

namespace App\Jobs;

use App\Http\Controllers\Pc\LingXingApiController;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class LingXingJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $params;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        $this->params = $params;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {

            $fun   = $this->params['fun'];
            $lxApi = new LingXingApiController();

            switch ($fun) {
                case 'addInStorageOrder' :
                    $res = $lxApi->addInStorageOrder($this->params['params']);
                    break;
                case 'addOutStorageOrder' :
                    $res = $lxApi->addOutStorageOrder($this->params['params']);
                    break;
                default:
                    echo '未知参数';
            }


            echo ' 执行成功！';
        } catch (\Exception $e) {
            //消化失败，记录错误日志
            $add['job']  = 'LingXing';
            $add['time'] = time();
            $add['msg']  = $e->getMessage();
            $add['data'] = json_encode($this->params);
            Db::table('job_err_log')->insert($add);
        }
    }


}
