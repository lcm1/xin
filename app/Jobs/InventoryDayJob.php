<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class InventoryDayJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $params;
    protected $type;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        //
        $this->params = $params;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        // echo  $this->params;

        do {
            echo '执行队列'.$this->params."\n";
            $task = false;
            try {
                $do =  $this->DoTask();
            } catch (\Throwable $th) {
                echo $th->getFile().'--'.$th->getLine().'--'.$th->getMessage()."\n";
            }
            if($do==1){
                //执行完毕  验证队列key
                $key = redis::get('queue_task:inventory_day');
                if($key==$this->params){
                    $task = true;
                }
            }
        } while ($task);

    }


    public function DoTask(){
        echo 'spu周转天数-7'."\n";
        $this->SpuData(1);
        echo 'spu周转天数-30'."\n";
        $this->SpuData(2);
        echo 'spu周转天数-90'."\n";
        $this->SpuData(3);
        echo 'fasin周转天数-7'."\n";
        $this->FasinData(1);
        echo 'fasin周转天数-30'."\n";
        $this->FasinData(2);
        echo 'fasin周转天数-90'."\n";
        $this->FasinData(3);
        //结束休眠
        echo '全部结束，休眠半小时'."\n";
        sleep(1800);
        return 1;
    }

    public function FasinData($type){
        if($type==1){
            $start_time = date('Y-m-d 00:00:00',time()-86400*7);
            $end_time = date('Y-m-d 23:59:59',time()-86400);
            $i['find_day'] = 7;
        }
        if($type==2){
            $start_time = date('Y-m-d 00:00:00',time()-86400*30);

            $end_time = date('Y-m-d 23:59:59',time()-86400);
            $i['find_day'] = 30;
        }
        if($type==3){
            $start_time = date('Y-m-d 00:00:00',time()-86400*90);
            $end_time = date('Y-m-d 23:59:59',time()-86400);
            $i['find_day'] = 90;
        }
        $i['start_time'] = $start_time;
        $i['end_time'] = $end_time;
        echo '周转天数fasin缓存考试--天数-'.$i['find_day'].'--开始时间'. $i['start_time'].'--结束时间'. $i['end_time'] ."开始--\n";
        $i['update_time'] = date('Y-m-d h:i:s',time());
        $today = date('Y-m-d',time());

        //查询fasin对应的sku  库存sku
        $fasin_sku = db::table('self_sku')->where('fasin','!=','')->select(db::raw('group_concat(custom_sku_id) as custom_sku_ids'),'fasin')->groupby('fasin')->get();
        $fasin_skus = [];
        foreach ($fasin_sku as $fasv) {
            # code...
            if($cus_ids = explode(',',$fasv->custom_sku_ids)){
                $fasin_skus[$fasv->fasin] = $cus_ids;
            }
        }


        //获取库存sku库存
        $cus_inventorys = db::table('self_custom_sku')->get();

        $cus_inventory = [];
        foreach ($cus_inventorys as $cv) {
            # code...
            $cus_inventory[$cv->id] = $cv->quanzhou_inventory+$cv->tongan_inventory+$cv->cloud_num;
        }

        //查询fasin fba库存
        $fba = [];
        $cus_cache = db::table('self_sku as a')->leftjoin('product_detail as b','a.id','=','b.sku_id')->select(db::raw('sum(b.in_stock_num) as in_stock_num'),db::raw('sum(b.transfer_num) as transfer_num'),db::raw('sum(b.in_bound_num) as in_bound_num'),'a.fasin')->groupby('a.fasin')->get();
        foreach ($cus_cache as $csv) {
            # code...
            $fba[$csv->fasin] = $csv->in_stock_num+$csv->transfer_num+$csv->in_bound_num;
        }


        //查询日报销量

        $reports = Db::table('amazon_day_report')->whereBetween('ds', [$start_time, $end_time])->get()->toarray();
        $report_ids = array_column($reports,'id');
        
        $day_report =  Db::table('amazon_day_report_detail')->whereIn('report_id', $report_ids)->select('fasin',DB::raw('sum(sales_num) as orders'))->groupby('fasin')->get();


        $fasin_order = [];
        foreach ($day_report as $dv) {
            # code...
            $fasin_order[$dv->fasin] = $dv->orders;
        }


        $fasin_list = db::table('amazon_fasin')->where('is_self',1)->get();
        foreach ($fasin_list as $key => $fv) {
            # code...
            echo $key.'/'.count($fasin_list).'--fasin:'.$fv->fasin."\n";
            //查询fasin 订单
            $fv->order = 0;
            if(isset($fasin_order[$fv->fasin])){
                $fv->order =$fasin_order[$fv->fasin];
            }
            $s_start_time = strtotime($start_time);
            $s_end_time = strtotime($end_time);
            $day = ceil(($s_end_time - $s_start_time) / 86400);
            $day_quantity_ordered = 0;
            if ($fv->order > 0) {
                $day_quantity_ordered = round($fv->order / $day, 2);
            }


            //查询fasin的库存sku
            $f_cus_ids = [];
            if(isset($fasin_skus[$fv->fasin])){
                $f_cus_ids = $fasin_skus[$fv->fasin];
            }


            //获取库存
            $here_inventory = 0;
            $fba_inventory = 0;
            foreach ($f_cus_ids as $f_cus_v) {
                # code...
                if(isset($cus_inventory[$f_cus_v])){
                    $here_inventory +=$cus_inventory[$f_cus_v];
                }
            }

            if(isset($fba[$fv->fasin])){
                $fba_inventory  = $fba[$fv->fasin];
            }


            $fv->here_inventory = $here_inventory;

            $fv->fba_inventory = $fba_inventory;

                  // //本地仓库存
            $fv->here_inventory_day = 999;
            //本地仓库存周转天数
            $fv->fba_inventory_day = 999;
            //fba仓库存周转天数
            if ($day_quantity_ordered == 0) {
                $fv->here_inventory_day = 999;
                $fv->fba_inventory_day = 999;
            }
    
            if ($fv->here_inventory == 0) {
                $fv->here_inventory_day = 0;
            }
            if ($fv->fba_inventory == 0) {
                $fv->fba_inventory_day = 0;
            }
    
            if ($day_quantity_ordered > 0 && $fv->here_inventory > 0) {
                //本地仓库存周转天数=同安库存/日销
                $fv->here_inventory_day = round($fv->here_inventory / $day_quantity_ordered, 2);
            }
            if ($day_quantity_ordered > 0 && $fv->fba_inventory > 0) {
                //fba库存周转天数=在仓+预留+在途/日销
                $fv->fba_inventory_day = round($fv->fba_inventory / $day_quantity_ordered, 2);
            }
            $i['here_days'] = $fv->here_inventory_day;
            $i['fba_days'] = $fv->fba_inventory_day;
            $i['fasin'] = $fv->fasin;
            $i['order'] = $fv->order;
            $i['fba_inventory'] =$fv->fba_inventory;
            $i['here_inventory'] =$fv->here_inventory;

            $re = db::table('inventory_day_fasin')->where('fasin',$i['fasin'])->where('find_day',$i['find_day'])->first();
            if($re){
                db::table('inventory_day_fasin')->where('id',$re->id)->update($i);
            }else{
                db::table('inventory_day_fasin')->insert($i);
            }
        }
        echo '周转天数fasin缓存--天数-'.$i['find_day'].'--开始时间'. $i['start_time'].'--结束时间'. $i['end_time'] ."结束--\n";
    }


    public function SpuData($type){
        if($type==1){
            $start_time = date('Y-m-d 00:00:00',time()-86400*7);
            $end_time = date('Y-m-d 23:59:59',time()-86400);
            $i['find_day'] = 7;
        }
        if($type==2){
            $start_time = date('Y-m-d 00:00:00',time()-86400*30);
            $end_time = date('Y-m-d 23:59:59',time()-86400);
            $i['find_day'] = 30;
        }
        if($type==3){
            $start_time = date('Y-m-d 00:00:00',time()-86400*90);
            $end_time = date('Y-m-d 23:59:59',time()-86400);
            $i['find_day'] = 90;
        }

        $i['start_time'] = $start_time;
        $i['end_time'] = $end_time;
        echo '周转天数spu缓存--天数-'.$i['find_day'].'--开始时间'. $i['start_time'].'--结束时间'. $i['end_time'] ."开始--\n";
        $i['update_time'] = date('Y-m-d h:i:s',time());
        $today = date('Y-m-d',time());
        //获取实时数据
        $cachetodays = Db::table('cache_spu')->where('ds','like', "{$today}%")->get();
        $fba = [];
        if($cachetodays){
            foreach ($cachetodays as $cv) {
                # code...
                $fba[$cv->spu_id] = $cv->in_stock_num+$cv->transfer_num+$cv->in_bound_num;
            }

        }

        //获取最近库存数据
        $cusdata =  Db::table('self_custom_sku')->select(db::raw('sum(quanzhou_inventory) as quanzhou_inventory'),db::raw('sum(tongan_inventory) as tongan_inventory'),db::raw('sum(cloud_num) as cloud_num'),'spu_id')->groupby('spu_id')->get();
        $here = [];
        if($cusdata){
            foreach ($cusdata as $cdv) {
                # code...
                $here[$cdv->spu_id] = $cdv->quanzhou_inventory+$cdv->tongan_inventory+$cdv->cloud_num;
            }
        
        }


        $list =  Db::table('cache_spu')->whereBetween('ds', [$start_time, $end_time])->select(db::raw('sum(order_num) as order_num'),'spu_id')->groupby('spu_id')->get();

        
        foreach ($list as $k=>$v) {
            # code...
            echo $k.'/'.count($list).'--id:'.$v->spu_id."\n";
            $s_start_time = strtotime($start_time);
            $s_end_time = strtotime($end_time);
            $day = ceil(($s_end_time - $s_start_time) / 86400);
            $day_quantity_ordered = 0;
            if ($v->order_num > 0) {
                $day_quantity_ordered = round($v->order_num / $day, 2);
            }
            
            $v->fba_inventory = 0;
            if(isset($fba[$v->spu_id])){
                $v->fba_inventory  = $fba[$v->spu_id];
            }

            $v->here_inventory  = 0;
            if(isset($here[$v->spu_id])){
                $v->here_inventory  = $here[$v->spu_id];
            }

            // //本地仓库存
            $v->here_inventory_day = 999;
            //本地仓库存周转天数
            $v->fba_inventory_day = 999;
            //fba仓库存周转天数
            if ($day_quantity_ordered == 0) {
                $v->here_inventory_day = 999;
                $v->fba_inventory_day = 999;
            }
    
            if ($v->here_inventory == 0) {
                $v->here_inventory_day = 0;
            }
            if ($v->fba_inventory == 0) {
                $v->fba_inventory_day = 0;
            }
    
            if ($day_quantity_ordered > 0 && $v->here_inventory > 0) {
                //本地仓库存周转天数=同安库存/日销
                $v->here_inventory_day = round($v->here_inventory / $day_quantity_ordered, 2);
            }
            if ($day_quantity_ordered > 0 && $v->fba_inventory > 0) {
                //fba库存周转天数=在仓+预留+在途/日销
                $v->fba_inventory_day = round($v->fba_inventory / $day_quantity_ordered, 2);
            }
            $i['here_days'] = $v->here_inventory_day;
            $i['fba_days'] = $v->fba_inventory_day;
            $i['spu_id'] = $v->spu_id;
            $i['order'] =$v->order_num;
            $i['fba_inventory'] =$v->fba_inventory;
            $i['here_inventory'] =$v->here_inventory;

            $re = db::table('inventory_day_spu')->where('spu_id',$i['spu_id'])->where('find_day',$i['find_day'])->first();
            if($re){
                db::table('inventory_day_spu')->where('id',$re->id)->update($i);
            }else{
                db::table('inventory_day_spu')->insert($i);
            }
        }
        echo '周转天数spu缓存--天数-'.$i['find_day'].'--开始时间'. $i['start_time'].'--结束时间'. $i['end_time'] ."结束--\n";
    }
}