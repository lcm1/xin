<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\AmazonPlanStatus;
use App\Models\AmazonSkulist;
use Illuminate\Support\Facades\DB;

class AmazonSalesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $params;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        //
        $this->params = $params;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // foreach ($this->params as $key => $value) {
        //     # code...
        //     var_dump($value);
        //     sleep(3);
        // }
        //
        try {

            $update['status'] = 0;
            $update['msg'] =  "进行中";
            Db::table('amazon_sales_task')->where('id',$this->params['task_id'])->update($update);
            //消化队列
            $this->excelInsertNew($this->params);

        } catch(\Exception $e){
            $errmsg = $e->getMessage();
            $add['job'] = 'amazon_salse';
            $add['time'] = time();
            $add['msg'] =  $errmsg;
            $add['data'] = json_encode($this->params);
            Db::table('job_err_log')->insert($add);

            $update['status'] = 2;
            $update['msg'] =  $errmsg;
            Db::table('amazon_sales_task')->where('id',$this->params['task_id'])->update($update);
            //消化失败，记录错误日志

        }
    }


  //亚马逊会话次数导入
  public function excelInsertNew($params)
  {

    $day = $params['day'];
    $start_time = $params['start_time'];
    $end_time = $params['end_time'];
    unset($params['day']);
    unset($params['start_time']);
    unset($params['end_time']);
    $time = time();
    $a = 0;
    $b = 0;
    $num = 0;
    $total = count($params);

    foreach ($params as $cv){
        if($cv['sku']){
            $spu =  $cv['spu'];
            $errspu = [];
            if(!$spu){
                $errspu[] = $cv['sku'];
            }
        }
    }

    if(count($errspu)>=1){
        $update['status'] = 2;
        $update['msg'] =  'spu为空'.json_encode($errspu);
        Db::table('amazon_sales_task')->where('id',$this->params['task_id'])->update($update);
        return;
    }

    foreach ($params as $v) {
        # code...
        if($v['sku']){
            echo $num.'/'.$total.$v['sku']."\n";
            $num++;
            $sku = $v['sku'];
            $sku_id = $v['sku_id']??0;
            $custom_sku_id = $v['custom_sku_id']??0;
            $spu_id = $v['spu_id']??0;
            $session_percentage = $v['session_percentage'];
            $shop_id = $v['shop_id'];
            $spu = $v['spu']??'';
            $user_id = $v['user_id'];
            $category_id = $v['category_id'];
    
            for ($i = 0; $i < $day; $i++) {
                # code...
                $addtime = $i * 86400;
                $start_date = date('Y-m-d', $start_time + $addtime);
        
                $saleSql = "select * from amazon_sale_percentage where `sku`='$sku' and `ds`='$start_date' ";
                $checkSaleSku = DB::select($saleSql);
                if ($checkSaleSku) {
                    $id = $checkSaleSku[0]->id;
                    $updatesql = "UPDATE amazon_sale_percentage
                        SET `sku` = '$sku',
                        `sku_id` = '$sku_id',
                        `custom_sku_id` = '$custom_sku_id',
                        `spu_id` = '$spu_id',
                         `session_percentage` = '$session_percentage',
                         `shop_id` = $shop_id,
                         `spu` = '$spu',
                         `user_id` = $user_id,
                         `category_id` = '$category_id',
                         `ds` = '$start_date',
                         `create_time` = '$time',
                         `update_time` = '$time'
                        WHERE
                            `id` = '$id'";
                    $result = DB::update($updatesql);
                    $b++;
                    // if (!$result) {
                    //     throw new \Exception("导入错误，数据错误".$sku, 1);
                    //     return;
                    // } else {
                    //     $b++;
                    // }
        
                } else {
                    $sql = "INSERT INTO amazon_sale_percentage (
                            `sku`,
                            `sku_id`,
                            `custom_sku_id`,
                            `spu_id`,
                            `session_percentage`,
                            `shop_id`,
                            `spu`,
                            `user_id`,
                            `category_id`,
                            `ds`,
                            `create_time`,
                            `update_time`
                        )
                        VALUES
                            (
                                '$sku',
                                '$sku_id',
                                '$custom_sku_id',
                                '$spu_id',
                                '$session_percentage',
                                '$shop_id',
                                '$spu',
                                '$user_id',
                                '$category_id',
                                '$start_date',
                                $time,
                                $time
                            )";
                    $result = DB::insert($sql);
                    $a++;
                    // if (!$result) {
                    //     throw new \Exception("导入错误，数据错误".$sku, 1);
                    //     return;
                    // } else {
                    //     $a++;
                    // }
                }
        
        
            }
        }
        // sleep(0.3);
        // echo '导入成功';
        // $add['job'] = 'amazon_salse';
        // $add['time'] = time();
        // $add['msg'] =  '导入成功，新增'.$a.'条,修改'.$b;
        // $add['data'] = '';
        // Db::table('job_err_log')->insert($add);    

    }



    $update['status'] = 1;
    $update['msg'] =  '导入成功，新增'.$a.'条,修改'.$b;
    Db::table('amazon_sales_task')->where('id', $params['task_id'])->update($update);
      
  }


}
