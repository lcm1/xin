<?php

namespace App\Jobs;

use App\Http\Controllers\Pc\WalmartApiController;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class WalmartOrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $params;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        $this->params = $params;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $date = date('Y-m-d H:i:s', microtime(true));
            echo $date . ' WalmartOrder';
            $this->walmartOrder();
            echo ' 执行成功！';
        } catch (\Exception $e) {
            //消化失败，记录错误日志
            $add['job']  = 'WalmartOrder';
            $add['time'] = time();
            $add['msg']  = $e->getMessage();
            $add['data'] = json_encode($this->params);
            Db::table('job_err_log')->insert($add);
        }
    }

    public function walmartOrder()
    {
        $type = $this->params['type']; // 1:拉取新订单 2:修改订单
        if ($type == 1) {
            $date = date('Y-m-d H:i:s', strtotime('-56 hours'));// - 2天 8小时
        } else {
            $date = date('Y-m-d H:i:s', strtotime('-9 hours 2 seconds'));// -9小时
        }

        $date = implode('T', explode(" ", $date)) . 'Z'; // 2023-08-13T03:33:22Z

        $shops = DB::table('shop as a')
            ->select(['a.id', 'a.access_token', 'b.user_id'])
            ->leftJoin('shop_users as b', 'a.Id', '=', 'b.shop_id')
            ->where('a.platform_id', 14)
            ->where('a.state', 1)
            ->get();
        //没传参数，默认查询最近7天
        $params = '';
        if ($type == 1) {
            $params .= 'createdStartDate=' . $date . '&';
        } else {
            $params .= 'lastModifiedStartDate=' . $date . '&';
        }

        $params = $params . 'limit=100';

        foreach ($shops as $shop) {
            $accessToken = json_decode($shop->access_token, true);
            if (!$accessToken) {
                continue;
            }

            $walmart = new WalmartApiController($shop->id, $accessToken['client_id'], $accessToken['client_secret']);


            $log['name'] = 'walmartOrder';
            $log['shop_id'] = $shop->id;
            $log['data'] = $walmart;
            $log['client_id'] = $accessToken['client_id'];
            $log['client_secret'] = $accessToken['client_secret'];
            $this->writeLog(var_export($log, true));

            $this->pageOrder($walmart, $params, $shop, $type);
        }

        return true;
    }

    private function pageOrder(WalmartApiController $walmart, $params, $shop, $type)
    {
        $result = $walmart->walmartApi('get', '/v3/orders', $params);
//        Redis::set('walmart_order',json_encode($result));exit;
//        $result = json_decode(Redis::get('walmart_order'), true);
        if (!$result || !isset($result['list']['elements']['order'])) {
            throw new \Exception('沃尔请求出错！');
        }
        $orders = $result['list']['elements']['order'];
        foreach ($orders as $order) {
            $time      = time();
            $insertArr = ['shop_id' => $walmart->shop_id, 'add_time' => $time, 'update_time' => $time];
            isset($order['purchaseOrderId']) && $insertArr['purchase_order_id'] = $order['purchaseOrderId'];
            isset($order['customerOrderId']) && $insertArr['customer_order_id'] = $order['customerOrderId'];
            isset($order['customerEmailId']) && $insertArr['customer_email_id'] = $order['customerEmailId'];
            $orderDate               = isset($order['orderDate']) ? $this->formatDate($order['orderDate']) : null;
            $insertArr['order_date'] = $orderDate;
            if (isset($order['shippingInfo'])) {
                $shippingInfo = $order['shippingInfo'];
                isset($shippingInfo['phone']) && $insertArr['phone'] = $shippingInfo['phone'];
                isset($shippingInfo['estimatedDeliveryDate']) && $insertArr['estimated_delivery_date'] = $this->formatDate($shippingInfo['estimatedDeliveryDate']);
                isset($shippingInfo['estimatedShipDate']) && $insertArr['estimated_ship_date'] = $this->formatDate($shippingInfo['estimatedShipDate']);
                isset($shippingInfo['methodCode']) && $insertArr['method_code'] = $shippingInfo['methodCode'];

                if (isset($shippingInfo['postalAddress'])) {
                    $postalAddress = $shippingInfo['postalAddress'];
                    isset($postalAddress['name']) && $insertArr['postal_address_name'] = $postalAddress['name'];
                    isset($postalAddress['address1']) && $insertArr['address1'] = $postalAddress['address1'];
                    isset($postalAddress['address2']) && $insertArr['address2'] = $postalAddress['address2'];
                    isset($postalAddress['city']) && $insertArr['city'] = $postalAddress['city'];
                    isset($postalAddress['state']) && $insertArr['postal_address_state'] = $postalAddress['state'];
                    isset($postalAddress['postalCode']) && $insertArr['postal_code'] = $postalAddress['postalCode'];
                    isset($postalAddress['country']) && $insertArr['country'] = $postalAddress['country'];
                    isset($postalAddress['addressType']) && $insertArr['address_type'] = $postalAddress['addressType'];
                }
            }
            $orderFind = DB::table('walmart_order')
                ->where('shop_id', $walmart->shop_id)
                ->where('purchase_order_id', $order['purchaseOrderId'])
                ->first();
            if ($type == 1 && !$orderFind) {
                $orderId = DB::table('walmart_order')->insertGetId($insertArr);
            } elseif ($type == 2 && $orderFind) {
                unset($insertArr['add_time']);
                $orderId   = $orderFind->id;
                $orderFind = json_decode(json_encode($orderFind), true);
                $diff      = array_diff($insertArr, $orderFind);
                if (count($diff) > 1) {//排除掉只更新update_time字段
                    DB::table('walmart_order')->where('id', $orderId)->update($diff);
                }
            } else {
                continue;
            }
            unset($orderFind);
            //商品信息
            if (isset($order['orderLines']['orderLine'])) {
                $orderLines = $order['orderLines']['orderLine'];
                foreach ($orderLines as $orderLine) {
                    $insertItemArr ['shop_id']           = $walmart->shop_id;
                    $insertItemArr ['user_id']           = $shop->user_id ?: 0;
                    $insertItemArr ['walmart_order_id']  = $orderId;
                    $insertItemArr ['purchase_order_id'] = $order['purchaseOrderId'];
                    $insertItemArr ['order_date']        = $orderDate;
                    $insertItemArr ['add_time']          = $time;
                    $insertItemArr ['update_time']       = $time;
                    isset($orderLine['lineNumber']) && $insertItemArr['line_number'] = (int)$orderLine['lineNumber'];
                    isset($orderLine['item']['productName']) && $insertItemArr['name'] = $orderLine['item']['productName'];
                    isset($orderLine['item']['sku']) && $insertItemArr['sku'] = $orderLine['item']['sku'];
                    if (isset($orderLine['charges']['charge'][0])) {
                        $charge = $orderLine['charges']['charge'][0];
                        isset($charge['chargeType']) && $insertItemArr['charge_type'] = $charge['chargeType'];
                        isset($charge['chargeName']) && $insertItemArr['charge_name'] = $charge['chargeName'];
                        isset($charge['chargeAmount']['currency']) && $insertItemArr['charge_currency'] = $charge['chargeAmount']['currency'];
                        isset($charge['chargeAmount']['amount']) && $insertItemArr['charge_amount'] = $charge['chargeAmount']['amount'];
                        isset($charge['tax']['taxName']) && $insertItemArr['tax_name'] = $charge['tax']['taxName'];
                        isset($charge['tax']['taxAmount']['currency']) && $insertItemArr['tax_currency'] = $charge['tax']['taxAmount']['currency'];
                        isset($charge['tax']['taxAmount']['amount']) && $insertItemArr['tax_amount'] = $charge['tax']['taxAmount']['amount'];

                    }
                    isset($orderLine['orderLineQuantity']['unitOfMeasurement']) && $insertItemArr['unit_of_measurement'] = $orderLine['orderLineQuantity']['unitOfMeasurement'];
                    isset($orderLine['orderLineQuantity']['amount']) && $insertItemArr['order_line_quantity_amount'] = $orderLine['orderLineQuantity']['amount'];
                    isset($orderLine['statusDate']) && $insertItemArr['status_date'] = $this->formatDate($orderLine['statusDate']);
                    isset($orderLine['orderLineStatuses']['orderLineStatus']['0']) && $insertItemArr['order_line_status'] = $orderLine['orderLineStatuses']['orderLineStatus']['0']['status'];
                    if (isset($orderLine['fulfillment'])) {
                        $fulfillment = $orderLine['fulfillment'];
                        isset($fulfillment['fulfillmentOption']) && $insertItemArr['fulfillment_option'] = $fulfillment['fulfillmentOption'];
                        isset($fulfillment['shipMethod']) && $insertItemArr['ship_method'] = $fulfillment['shipMethod'];
                        isset($fulfillment['spickUpDateTimehipMethod']) && $insertItemArr['pick_up_date_time'] = $this->formatDate($fulfillment['pickUpDateTime']);
                    }
                    $itemFind = DB::table('walmart_order_item')
                        ->where('walmart_order_id', $orderId)
                        ->where('line_number', $orderLine['lineNumber'])
                        ->first();
                    if ($type == 1 && !$itemFind) {
                        DB::table('walmart_order_item')->insert($insertItemArr);
                    } elseif ($type == 2 && $itemFind) {
                        unset($insertItemArr['add_time']);
                        $itemFind = json_decode(json_encode($itemFind), true);
                        $diff     = array_diff($insertItemArr, $itemFind);
                        if (count($diff) > 1) {
                            DB::table('walmart_order_item')->where('id', $itemFind['id'])->update($diff);
                        }
                    } else {
                        continue;
                    }

                    unset($itemFind);
                }
            }
            unset($order);
        }
        unset($orders);
        //翻页查询
        if (isset($result['list']['meta']['nextCursor'])) {
            $nextCursor = $result['list']['meta']['nextCursor'];
            unset($result);
            $this->pageOrder($walmart, $nextCursor, $shop, $type);
        } else {
            return true;
        }
    }

    private function formatDate($time)
    {
        return date('Y-m-d H:i:s', $time / 1000);
    }

    private function writeLog($log)
    {
        $logFile = fopen(
            storage_path('logs' . DIRECTORY_SEPARATOR . date('Y-m-d').'walmart_order_job.log'),
            'a'
        );
        fwrite($logFile, date('Y-m-d H:i:s') . ': ' . $log . PHP_EOL);
        fclose($logFile);
    }
}
