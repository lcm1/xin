<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Shop;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

//新品导入队列-任务类
class NewProductExportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $params;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        //
        $this->params = $params;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        var_dump(1);
        try {

            //消化队列
            $this->savedata($this->params);
            // throw new \Exception("Error Processing Request", 1);

        } catch(\Exception $e){

            //消化失败，记录错误日志
            $errmsg = $e->getMessage();
            foreach ($this->params as $v ) {
                # code...
                $add['job'] = 'new_product_export';
                $add['time'] = time();
                $add['msg'] =  $errmsg;
                $add['data'] = json_encode($v);
                Db::table('job_err_log')->insert($add);
            }

        }
    }




    /**
     * 亚马逊sku
     * @param Request $request
     */
    public function amazon_sku($params)
    {
        if (!isset($params['shop_id'])) {
            throw new \Exception("没有shop_id参数", 1);
            return;
        }
        if (!isset($params['sku'])) {
            throw new \Exception("没有sku", 1);
            return;
        }

        $skuList = explode(",", $params['sku']);
        $Shop = Shop::where('id', $params['shop_id'])->first();

        $shop['auth'] = json_decode($Shop['app_data'], true);
        $auth = $shop['auth'];

        require_once(app_path() . DIRECTORY_SEPARATOR . 'Libs' . DIRECTORY_SEPARATOR . 'MarketplaceWebServiceProducts' . DIRECTORY_SEPARATOR . 'Client.php');
        require_once(app_path() . DIRECTORY_SEPARATOR . 'Libs' . DIRECTORY_SEPARATOR . 'MarketplaceWebServiceProducts' . DIRECTORY_SEPARATOR . 'Model' . DIRECTORY_SEPARATOR . 'GetMatchingProductForIdRequest.php');
        require_once(app_path() . DIRECTORY_SEPARATOR . 'Libs' . DIRECTORY_SEPARATOR . 'MarketplaceWebServiceProducts' . DIRECTORY_SEPARATOR . 'Model' . DIRECTORY_SEPARATOR . 'SellerSKUListType.php');
        $serviceUrl = "https://mws.amazonservices.com/Products/2011-10-01";
        // Europe
        $serviceUrl_eu = "https://mws-eu.amazonservices.com/Products/2011-10-01";
        // Japan
        $serviceUrl_jp = "https://mws.amazonservices.jp/Products/2011-10-01";

        if ($auth['MARKETPLACE_ID'] == 'A1VC38T7YXB528') $serviceUrl = $serviceUrl_jp;
        if (in_array($auth['MARKETPLACE_ID'], array('A1F83G8C2ARO7P', 'A1PA6795UKMFR9', 'A1RKKUPIHCS9HS', 'A13V1IB3VIYZZH', 'APJ6JRA9NG5V4'))) $serviceUrl = $serviceUrl_eu;

        $config = array(
            'ServiceURL' => $serviceUrl,
            'ProxyHost' => null,
            'ProxyPort' => -1,
            'ProxyUsername' => null,
            'ProxyPassword' => null,
            'MaxErrorRetry' => 3,
        );

        $service = new \MarketplaceWebServiceProducts_Client(
            $auth['AWS_ACCESS_KEY_ID'],
            $auth['AWS_SECRET_ACCESS_KEY'],
            'MarketplaceWebServiceProducts PHP5 Library',
            '2',
            $config);

        $request = new \MarketplaceWebServiceProducts_Model_GetMatchingProductForIdRequest();
        $request->setSellerId($auth['MERCHANT_ID']);
        $request->setMarketplaceId($auth['MARKETPLACE_ID']);
        if (isset($auth['MWSAuthToken'])) $request->setMWSAuthToken($auth['MWSAuthToken']);
        $request->setIdType('SellerSKU');
        $idlist = new \MarketplaceWebServiceProducts_Model_SellerSKUListType();
        $idlist->setSellerSKU($skuList);
        $request->setIdList($idlist);

        try {
            $response = $service->GetMatchingProductForId($request);
            $dom = new \DOMDocument();
            $dom->loadXML($response->toXML());
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;
            $str = $dom->saveXML();
            $str = str_replace('ns2:', '', $str);
            $xml = simplexml_load_string($str);
            return json_decode(json_encode($xml), true);
        } catch (\MarketplaceWebServiceProducts_Exception $ex) {
            throw new \Exception("没获取到数据", 1);
            return;
        }

    }




    public function savedata($params){
        $user_id = $params['user_id'];
        unset($params['user_id']);
        $add_list = $params;
        $insert_product = '';
        $insert_sku = '';
        foreach ($add_list as $key => $value) {
            echo'进入循环';

            $prodoct_detail = Db::table('product_detail')->where('sellersku', $value['sku'])->first();
            //查询导入的sku在amazon_skulist是否存在
            $amazon_skulist = Db::table('amazon_skulist')->where('sku', $value['sku'])->first();
            if ($prodoct_detail&&$amazon_skulist){
                //都有数据，无需插入
                echo '已存在无需插入';
                return;
            }
            try {
                //code...
                $amazon_sku = $this->amazon_sku($value);
                $data = $amazon_sku['GetMatchingProductForIdResult'];
                $parent_asin = $data['Products']['Product']['Relationships']['VariationParent']['Identifiers']['MarketplaceASIN']['ASIN']??'';
                $asin = $data['Products']['Product']['Identifiers']['MarketplaceASIN']['ASIN']??'';
                $itemAttributes = $data['Products']['Product']['AttributeSets']['ItemAttributes'];
                $brand = $itemAttributes['Brand']??'';
                $color = $itemAttributes['Color']??'';
                $binding = $itemAttributes['Binding']??'';
                $department = $itemAttributes['Department']??'';
                $productGroup = $itemAttributes['ProductGroup']??'';
                $productTypeName = $itemAttributes['ProductTypeName']??'';
                $smallImage = $itemAttributes['SmallImage']['URL']??'';
                $title = $itemAttributes['Title']??'';
                $title = addslashes($title);
                $size = $itemAttributes['Size']??'';
                $time = time();
            } catch (\Throwable $th) {
                //throw $th;
                echo '旧接口拿不到数据-----'."\n";
                $parent_asin = '';
                $asin = '';
                $brand = '';
                $color ='';
                $binding = '';
                $department = '';
                $productGroup = '';
                $productTypeName = '';
                $smallImage = '';
                $title = '';
                $title = '';
                $size = '';
                $time = time();
            }
  
            // $parent_asin = $data['Products']['Product']['Relationships']['VariationParent']['Identifiers']['MarketplaceASIN']['ASIN']??'';
            // $asin = $data['Products']['Product']['Identifiers']['MarketplaceASIN']['ASIN']??'';
            // $itemAttributes = $data['Products']['Product']['AttributeSets']['ItemAttributes'];
            // $brand = $itemAttributes['Brand']??'';
            // $color = $itemAttributes['Color']??'';
            // $binding = $itemAttributes['Binding']??'';
            // $department = $itemAttributes['Department']??'';
            // $productGroup = $itemAttributes['ProductGroup']??'';
            // $productTypeName = $itemAttributes['ProductTypeName']??'';
            // $smallImage = $itemAttributes['SmallImage']['URL']??'';
            // $title = $itemAttributes['Title']??'';
            // $title = addslashes($title);
            // $size = $itemAttributes['Size']??'';
            // $time = time();

            if (empty($prodoct_detail)) {
                $insert_product .= "('{$value['shop_id']}',5,'{$value['sku']}','{$parent_asin}','{$asin}','{$brand}','{$color}','{$binding}','{$department}','{$productGroup}','{$productTypeName}','{$smallImage}','{$title}','{$size}','{$time}',{$user_id},1),";
            }
            if (empty($amazon_skulist)) {
                $insert_sku .= "('{$value['shop_id']}','{$value['sku']}','{$asin}','{$time}','{$user_id}','{$parent_asin}'),";
            }

        }
        //去除多余的逗号
        $new_insert_product = substr($insert_product, 0, strlen($insert_product) - 1);

        var_dump(   $new_insert_product );
        $new_insert_sku = substr($insert_sku, 0, strlen($insert_sku) - 1);
        Db::beginTransaction();
        try {
            if (!empty($new_insert_product)) {
                $insert_product_sql = "INSERT INTO product_detail (
                        `shop_id`,
                        `platform_id`,
                        `sellersku`,
                        `parent_asin`,
                        `asin`,
                        `brand`,
                        `color`,
                        `binding`,
                        `department`,
                        `product_group`,
                        `product_type_name`,
                        `small_image`,
                        `title`,
                        `size`,
                        `update_time`,
                        `user_id`,
                        `is_query`
                    )
                    VALUES
                       {$new_insert_product}";
                $insert_product_res = Db::insert($insert_product_sql);
            }
            if (!empty($new_insert_sku)) {
                $insert_sku_sql = "INSERT INTO amazon_skulist (
                        `shop_id`,
                        `sku`,
                        `asin`,
                        `time`,
                        `user_id`,
                        `father_asin`   
                    )
                    VALUES
                       {$new_insert_sku}";
                $insert_res = Db::insert($insert_sku_sql);
            }
            DB::commit();
            echo '插入成功';
            return;
        } catch (\Exception $e) {
            DB::rollBack();
            $code = 500;
            $msg = $e->getMessage();
            echo '插入错误';
            throw new \Exception("插入失败".$msg, 1);
            return;
        }

    }
}
