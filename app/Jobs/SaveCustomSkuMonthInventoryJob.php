<?php

namespace App\Jobs;
use App\Models\BaseModel;
use App\Models\InventoryModel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
class SaveCustomSkuMonthInventoryJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $params;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        //
        $this->params = $params;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $params = $this->params;
        echo '进入队列SaveCustomSkuMonthInventoryJob';
//         var_dump($this->params);exit();
        try {
            $result = $this->saveCustomSkuInventoryMonth($this->params);
            if ($result['code'] == 200){
                echo '更新成功！';
            }else{
                echo '更新失败！错误：'.$result['msg'];
            }
        }catch (\Exception $e){
            echo '错误原因：'.$e;
        }
    }

    public function saveCustomSkuInventoryMonth($params)
    {
        try {
            $last = date('Y-m-01 00:00:00', strtotime('-1 month'));
            $now = date('Y-m-01 00:00:00');
            $customSkuMdl = db::table('self_custom_sku')
                ->get()
                ->keyBy('id');
            $priceMdl = db::table('inventory_total_pirce')
                ->get(['warehouse_id', 'custom_sku_id', 'num', 'price']);
            $priceList = [];
            foreach ($priceMdl as $p){
                $key = $p->warehouse_id.'-'.$p->custom_sku_id;
                $p->avg_price = $p->num <= 0 ? 0.00 : bcdiv($p->price, $p->num, 2);
                $priceList[$key] = $p;
            }

            $goodsRecordMdl = db::table('cloudhouse_record as a')
                ->leftJoin('goods_transfers as b', 'a.order_no', '=', 'b.order_no')
                ->whereBetween('a.createtime', [$last, $now])
                ->get(['a.*', 'b.platform_id']);
            $list = [];
            $warehouseList = [
                1 => 'tongan_inventory',
                2 => 'quanzhou_inventory',
                3 => 'cloud_num',
                4 => 'factory_num',
                6 => 'deposit_inventory',
                21 => 'direct_inventory',
                23 => 'shenzhen_inventory',
            ];
            $platformMdl = db::table('platform')
                ->get();
            db::begintransaction();
            foreach($customSkuMdl as $c){
                foreach ($warehouseList as $k => $w){
                    foreach ($platformMdl as $p){
                        $priceFirst = $priceList[$k.'-'.$c->id]->avg_price ?? 0.00;
                        $numFirst = $c->$w ?? 0;
                        $insert = db::table('custom_sku_inventory_month')
                            ->insertGetId([
                                'warehouse_id' => $k,
                                'custom_sku_id' => $c->id,
                                'spu_id' => $c->spu_id,
                                'platform_id' => $p->Id,
                                'price_first' => $priceFirst * $numFirst,
                                'num_first' => $numFirst,
                                'year' => date('Y', strtotime($now)),
                                'month' => date('m', strtotime($now)),
                                'created_at' => date('Y-m-d H:i:s'),
                            ]);
                        if (empty($insert)){
                            throw new \Exception('库存skuId：'.$c->id.'，仓库id：'.$k.'，平台：'.$p->Id.'新增数据失败！');
                        }
                    }
                }
            }
            foreach ($goodsRecordMdl as $v){
                $key = $v->warehouse_id.'-'.$v->platform_id.'-'.$v->custom_sku_id;
                if (isset($list[$key])){
                    $list[$key]['price_type_detail_'.$v->type_detail] += $v->num * $v->price;
                    $list[$key]['num_type_detail_'.$v->type_detail] += $v->num;
                }else{
                    $priceFirst = $priceList[$v->warehouse_id.'-'.$v->custom_sku_id]->avg_price ?? 0.00;
                    $object = $warehouseList[$v->warehouse_id] ?? '';
                    $numFirst = $customSkuMdl[$v->custom_sku_id]->$object ?? 0;
                    $list[$key] = [
                        'warehouse_id' => $v->warehouse_id,
                        'platform_id' => $v->platform_id,
                        'spu_id' => $v->spu_id,
                        'custom_sku_id' => $v->custom_sku_id,
                        'price_end' => $priceFirst * $numFirst,
                        'num_end' => $numFirst,
                        'year' => date('Y', strtotime($last)),
                        'month' => date('m', strtotime($last)),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ];
                    $arr = [1,2,3,4,5,6,7,8,9,10,11,12,17,18];
                    foreach ($arr as $i){
                        $list[$key]['price_type_detail_'.$i] = 0;
                        $list[$key]['num_type_detail_'.$i] = 0;
                    }
                    $list[$key]['price_type_detail_'.$v->type_detail] = $v->num * $v->price;
                    $list[$key]['num_type_detail_'.$v->type_detail] = $v->num;
                }
            }

            foreach ($list as $v){
                $update = db::table('custom_sku_inventory_month')
                    ->where('warehouse_id', $v['warehouse_id'])
                    ->where('platform_id', $v['platform_id'])
                    ->where('spu_id', $v['spu_id'])
                    ->where('custom_sku_id', $v['custom_sku_id'])
                    ->where('year', $v['year'])
                    ->where('month', $v['month'])
                    ->update($v);
            }

            db::commit();
            return ['code' => 200, 'msg' => '更新时间：'.$last.'~'.$now.'库存sku数据成功！'];
        }catch (\Exception $e){
            db::rollback();
            return ['code' => 500, 'msg' => '保存失败！原因：'.$e->getMessage().'；位置：'.$e->getLine()];
        }
    }
}