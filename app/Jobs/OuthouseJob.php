<?php

namespace App\Jobs;

use App\Http\Controllers\Jobs\CloudHouseController;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;


class OuthouseJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $params;
    protected $type;
    protected $log_type;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        //
        $this->params = $params;
        $this->type = 'stockout.create';
        https://xywms.anport-logistics.com/out_iop_esb/ess/qm?method=singleitem.synchronize
        $this->log_type = 4;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        echo '队列outhouseJob';
        try {
            //消化队列
            print_r($this->params);
            //执行推送
            $res = $this->send_outstock($this->params);
            print_r($res);
            if($res['code']==1){
                //curl成功响应时code=1
                $check = CloudHouseController::xml_parser($res['data']);
                if(!$check){
                    //返回的不是xml数据，推送失败记录失败日志，加入失败队列
                    CloudHouseController::set_log($this->log_type,CloudHouseController::$fail,$res['data']);
                    Redis::Lpush('out_house_fail',$this->params['order_no']);
                }else{
                    $data = $check;
                    //$data => 转为数组的xml数据
                    $log_data = json_encode($data,JSON_UNESCAPED_UNICODE);
                    //code=0表示推送成功
                    if($data['code']!=0){
                        //推送失败，记录失败日志，加入失败队列
                        CloudHouseController::set_log($this->log_type,CloudHouseController::$fail,$log_data);
                        Redis::Lpush('out_house_fail',$this->params['order_no']);
                    }else{
                        //发货计划出库
                        if($this->params['type']=='B2BCK'){
                            $update_status = DB::table('amazon_buhuo_request')->where('id',$this->params['order_no'])->update(['request_status'=>5]);
                            $update_status2 = DB::table('goods_transfers')->where('order_no',$this->params['order_no'])->update(['is_push'=>2]);
                            CloudHouseController::set_log($this->log_type,CloudHouseController::$success,$log_data);
                            //任务系统接受装箱任务
                            $request_id_json = substr(substr(json_encode(array("request_id" => $this->params['order_no'])), 1), 0, -1);
                            $task_data = Db::table('tasks')->where('ext', 'like', "%{$request_id_json}%")->where('state','!=',2)->select('Id', 'name', 'user_fz')->first();
                            if(!empty($task_data)){
                                $task_node = Db::table('tasks_node')->where('task_id',$task_data->Id)->select("Id")->get();
                                $task_son = Db::table('tasks_son')->where('task_id',$task_data->Id)->select("Id")->get();
                                $user = Db::table('users')->where('Id',423)->select('token')->first();
                                $task['Id'] = $task_son[0]->Id;
                                $task['tasks_node_id'] = $task_node[0]->Id;
                                $task['task_id'] = $task_data->Id;
                                $task['token'] = $user->token;
                                $call_task = new \App\Libs\wrapper\Task();
                                $call_task->task_accept_model($task);
                            }
                        }
                        //调拨出库
                        if($this->params['type']=='DBCK'||$this->params['type']=='CGTH'||$this->params['type']=='PTCK'){
                            //推送状态改为已推送
                            DB::table('goods_transfers')->where('order_no',$this->params['order_no'])->update(['is_push'=>2]);
                        }

                    }
                }
            }else{
                //code=0，推送过程出错，记录失败日志，加入失败队列
                CloudHouseController::set_log($this->log_type,CloudHouseController::$fail,$res['error']);
                Redis::Lpush('out_house_fail',$this->params['order_no']);
            }
        } catch(\Exception $e){
            //消化失败，记录错误日志了，加入失败队列
            print_r($this->params);
            Redis::Lpush('out_house_fail',$this->params['order_no']);
            $errmsg = $e->getMessage();
            CloudHouseController::set_log($this->log_type,CloudHouseController::$fail,$errmsg);
        }
    }


    public function send_outstock($params)
    {
        $appKey = CloudHouseController::$appKey;

        $appSecret = CloudHouseController::$appSecret;

        //singleitem.synchronize=>商品同步
        //entryorder.create=>入库单创建
        //stockout.create=>出库单创建
        //order.cancel=>取消订单
        //stockout.confirm=>出库单确认
        $paramArr = array(
            'app_key' => $appKey,
            'method' => $this->type,
            'format' => 'xml',
            'v' => '2.0',
            'sign_method' => 'md5',
            'timestamp' => date('Y-m-d H:i:s'),
            'customerId' => 'XM_QMESS_SKDYLB',
        );

        $sign = CloudHouseController::createSign($paramArr, $appSecret);
        //组参
        $strParam = CloudHouseController::createStrParam($paramArr);

        $strParam .= 'sign=' . $sign;

        $createtime = date('Y-m-d H:i:s');

        //发货计划出库
        if($params['type']=='B2BCK'){

            $skus = DB::table('amazon_buhuo_detail')->where('request_id',$params['order_no'])->select('sku','custom_sku','request_num','transportation_mode_name','shop_id','custom_sku_id')->get()->toArray();

            $tag_data = DB::table('amazon_buhuo_request as a')->leftJoin('amazon_tag as b','a.tag','=','b.id')->where('a.id',$params['order_no'])->where('a.tag','!=',0)->select('b.name')->first();
            if(!empty($tag_data)){
                $tag = $tag_data->name;
            }else{
                $tag = '无吊牌信息';
            }
            if(empty($skus)){
                return ['code'=>0,'data'=>'','error'=>'此计划没有sku数据'];
            }
            $skus = json_decode(json_encode($skus),true);
            $transport = $skus[0]['transportation_mode_name'];

            $shop = DB::table('shop')->where('Id',$skus[0]['shop_id'])->select('region')->first();

            if(!empty($shop)){
                if($shop->region=='NA'){
                    $site = 'US';
                }elseif($shop->region=='EU'){
                    $site = 'EU';
                }elseif($shop->region=='FE'){
                    $site = 'JP';
                }else{
                    return ['code'=>0,'data'=>'','error'=>'地区标识错误'];
                }
            }else{
                return ['code'=>0,'data'=>'','error'=>'店铺错误'];
            }
        }

        //调拨出库
        if($params['type']=='DBCK'||$this->params['type']=='CGTH'||$params['type']=='PTCK'){
            $skus = DB::table('goods_transfers_detail')->where('order_no',$params['order_no'])->select('custom_sku','transfers_num')->get();
            if(empty($skus)){
                return ['code'=>0,'data'=>'','error'=>'此调拨单没有sku数据'];
            }
            $site = '';
            $skus = json_decode(json_encode($skus),true);
        }



        //orderType=>出入库类型
        //JYCK=一般交易出库单;HHCK= 换货出库;BFCK=补发出库;PTCK=普通出库单;DBCK=调拨出库;B2BRK=B2B入库;B2BCK=B2B出库;
        //QTCK=其他出库;SCRK=生产入库;LYRK=领用入库;CCRK=残次品入库;CGRK=采购入库;DBRK= 调拨入库;QTRK=其他入库;
        //XTRK= 销退入库;THRK=退货入库;HHRK= 换货入库;CNJG= 仓内加工单;CGTH=采购退货出库单

        $request_id = $params['order_no'];
        $xml_data = '<?xml version="1.0" encoding="UTF-8"?>
                        <request>
                          <deliveryOrder>
                                <deliveryOrderCode>' . $request_id . '</deliveryOrderCode>

                                <orderType>'.$params['type'].'</orderType>
                                <warehouseCode>XMN03</warehouseCode>
                                
                                <createTime>' . $createtime . '</createTime> 
                                <receiverInfo>
                                <name>Matias villegas</name>
                                <mobile>+59898334112</mobile>
                                <province>fujian</province>
                                <city>montevideo</city>                             
                                <countryCode>'.$site.'</countryCode>
                                <detailAddress>Grito de asencio 1339</detailAddress>
                                </receiverInfo>
                            </deliveryOrder>
                            <orderLines>';
        if($params['type']=='B2BCK'){
            foreach ($skus as $detail) {
                $product = DB::table('product_detail')->where('sellersku',$detail['sku'])->select('fnsku','title')->first();
                $skuData = DB::table('self_custom_sku')->where('id',$detail['custom_sku_id'])->select('custom_sku','old_custom_sku')->first();
                if(!empty($skuData)){
                    if($skuData->old_custom_sku){
                        $custom_sku = $skuData->old_custom_sku;
                    }else{
                        $custom_sku = $skuData->custom_sku;
                    }
                }else{
                    return ['code'=>0,'data'=>'','error'=>$detail['sku'].'系统没有查到id为'.$detail['custom_sku_id'].'的库存sku'];
                }
//                var_dump($custom_sku);
                $fnsku = '';
                $title = '';
                if(!empty($product)){
                    if($product->fnsku){
                        $fnsku = $product->fnsku;
                    }else{
                        return ['code'=>0,'data'=>'','error'=>$detail['sku'].'还未获取到条码，无法推送'];
                    }
                    if($product->title){
                        $title = $product->title;
                    }else{
                        return ['code'=>0,'data'=>'','error'=>$detail['sku'].'还未获取到英文名，无法推送'];
                    }

                }else{
                    return ['code'=>0,'data'=>'','error'=>$detail['sku'].'还未获取到条码，无法推送'];
                }
                $num = $detail['request_num'];
                $xml_data .= '<orderLine>
                                <ownerCode>SKD</ownerCode>
                                <itemCode>' . $custom_sku . '</itemCode>
                                <planQty>' . $num . '</planQty>
                                <extendProps>
                                    <mabang_FNSKU>'.$fnsku.'</mabang_FNSKU>
                                    <mabang_title>'.mb_substr($title,0,21).'...'.mb_substr($title,-21).'</mabang_title>
                                    <mabang_conditions>New</mabang_conditions>  
                                </extendProps>
                             </orderLine>';
            }
            $xml_data .= '</orderLines>
                          <extendProps>
                            <mabang>                 
                                <orderRemark>' . $site . '-' . $transport . '-吊牌：'. $tag .'</orderRemark>
                            </mabang>
                          </extendProps>  
                        </request>';
        }

        if($params['type']=='DBCK'||$params['type']=='CGTH'||$params['type']=='PTCK'){
            foreach ($skus as $detail) {
                $product = DB::table('self_custom_sku')->where('custom_sku',$detail['custom_sku'])->orWhere('old_custom_sku',$detail['custom_sku'])->select('custom_sku','old_custom_sku','id')->first();
                if(!empty($product)){
                    if($product->old_custom_sku){
                        $custom_sku = $product->old_custom_sku;
                    }else{
                        $custom_sku = $product->custom_sku;
                    }
                }else{
                    return ['code'=>0,'data'=>'','error'=>$detail['sku'].'系统没有查到id为'.$detail['custom_sku_id'].'的库存sku'];
                }
                $xml_data .= '<orderLine>
                                <ownerCode>SKD</ownerCode>
                                <itemCode>' . $custom_sku . '</itemCode>
                                <planQty>' . $detail['transfers_num'] . '</planQty>
                                <extendProps>
                                    <mabang_FNSKU></mabang_FNSKU>
                                    <mabang_title></mabang_title>
                                    <mabang_conditions></mabang_conditions>  
                                </extendProps>
                             </orderLine>';
            }
            $xml_data .= '</orderLines>
                          <extendProps>
                            <mabang>                 
                                <orderRemark>' . $params['text'] .'</orderRemark>
                            </mabang>
                          </extendProps>  
                        </request>';

        }
//        <orderRemark>' . $site . '-' . $transport . '-吊牌：'. $tag .'</orderRemark>
        $xml_data = str_replace(PHP_EOL, '', $xml_data);
//        $logFile = fopen(
//            storage_path('logs' . DIRECTORY_SEPARATOR . 'outhouse_job.log'),
//            'a+'
//        );
//        fwrite($logFile,  $xml_data . PHP_EOL);
//        var_dump($xml_data);
//        print_r($xml_data);
        //发起请求
        $url = CloudHouseController::$link . $strParam;


        try{
            $result = CloudHouseController::curl_xml($url, $xml_data);
            return ['code'=>1,'data'=>$result,'error'=>''];
        }catch(\Exception $e){
            return ['code'=>0,'data'=>'','error'=>$e];
        }
    }





}
