<?php

namespace App\Jobs;

use App\Http\Controllers\Jobs\CloudHouseController;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;


class InhouseJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $params;
    protected $type;
    protected $log_type;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        //
        $this->params = $params;
        $this->type = 'entryorder.create';
        $this->log_type = 2;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        echo '队列inhouseJob';
        try {
            //消化队列
            print_r($this->params);

            //检索此批号推送状态是否为1
            $push = DB::table('goods_transfers')->where('order_no',$this->params['order_no'])->whereIn('is_push',[-2,-1,0,1])->select('id')->first();
            if(!$push){
                CloudHouseController::set_log($this->log_type,CloudHouseController::$fail,'没有此批号或推送状态不对');
            }

            if($push){
                //执行推送
                $res = $this->send_instock($this->params);
                //curl成功响应时code=1
                if($res['code']==1){
                    $check = CloudHouseController::xml_parser($res['data']);
                    if(!$check){
                        //返回的不是xml数据，推送失败记录失败日志，加入失败队列
                        DB::table('goods_transfers')->where('order_no',$this->params['order_no'])->update(['is_push'=>0]);
                        CloudHouseController::set_log($this->log_type,CloudHouseController::$fail,$res['data']);
                        Redis::Lpush('in_house_fail',$this->params['order_no']);
                    }else{
                        $data = $check;
                        //$data => 转为数组的xml数据
                        $log_data = json_encode($data,JSON_UNESCAPED_UNICODE);
                        //code=0表示推送成功
                        if($data['code']!=0){
                            DB::table('goods_transfers')->where('order_no',$this->params['order_no'])->update(['is_push'=>0]);
                            CloudHouseController::set_log($this->log_type,CloudHouseController::$fail,$log_data);
                            Redis::Lpush('in_house_fail',$this->params['order_no']);
                        }else{
                            //推送成功，记录成功日志，修改数据推送状态
                            //-1.取消入库 0.推送失败 1.待推送 2.推送成功 3.已确认
                            DB::table('goods_transfers')->where('order_no',$this->params['order_no'])->update(['is_push'=>2]);
                            CloudHouseController::set_log($this->log_type,CloudHouseController::$success,$log_data);
                        }
                    }
                }else{
                    //code=0，推送过程出错，记录失败日志，加入失败队列
                    DB::table('goods_transfers')->where('order_no',$this->params['order_no'])->update(['is_push'=>0]);
                    CloudHouseController::set_log($this->log_type,CloudHouseController::$fail,$res['error']);
                    Redis::Lpush('in_house_fail',$this->params['order_no']);
                }
            }
            //记录成功日志
        } catch(\Exception $e){
            print_r($this->params);
            //消化失败，记录错误日志,加入失败日志
            Redis::Lpush('in_house_fail',$this->params['order_no']);
            DB::table('goods_transfers')->where('order_no',$this->params['order_no'])->update(['is_push'=>0]);
            $errmsg = $e->getMessage();
            CloudHouseController::set_log($this->log_type,2,$errmsg);
        }
    }


    public function send_instock($params)
    {
        $appKey = CloudHouseController::$appKey;

        $appSecret = CloudHouseController::$appSecret;

        //singleitem.synchronize=>商品同步
        //entryorder.create=>入库单创建
        //stockout.create=>出库单创建
        //order.cancel=>取消订单
        $paramArr = array(
            'app_key' => $appKey,
            'method' => $this->type,
            'format' => 'xml',
            'v' => '2.0',
            'sign_method' => 'md5',
            'timestamp' => date('Y-m-d H:i:s'),
            'customerId' => 'XM_QMESS_SKDYLB',
        );

        $sign = CloudHouseController::createSign($paramArr, $appSecret);
        //组参
        $strParam = CloudHouseController::createStrParam($paramArr);

        $strParam .= 'sign=' . $sign;

        $createtime = date('Y-m-d H:i:s');
        //orderType=>出入库类型
        //JYCK=一般交易出库单;HHCK= 换货出库;BFCK=补发出库;PTCK=普通出库单;DBCK=调拨出库;B2BRK=B2B入库;B2BCK=B2B出库;
        //QTCK=其他出库;SCRK=生产入库;LYRK=领用入库;CCRK=残次品入库;CGRK=采购入库;DBRK= 调拨入库;QTRK=其他入库;
        //XTRK= 销退入库;THRK=退货入库;HHRK= 换货入库;CNJG= 仓内加工单;CGTH=采购退货出库单

        $xml_data = '<?xml version="1.0" encoding="UTF-8"?>
                        <request>
                           <entryOrder>
                                <entryOrderCode>'.$params['order_no'].'</entryOrderCode>
                                <ownerCode>SKD</ownerCode>
                                <warehouseCode>XMN03</warehouseCode>
                                <orderCreateTime>'.$createtime.'</orderCreateTime>
                                <orderType>CGRK</orderType>
                                <operatorName>郑</operatorName>
                            </entryOrder>
                            <orderLines>';
        foreach ($params['detail'] as $detail) {
            //检索有没有这个库存sku
            $se = DB::table('self_custom_sku')->where('custom_sku',$detail['custom_sku'])->orWhere('old_custom_sku',$detail['custom_sku'])->select('custom_sku','old_custom_sku','name')->get()->toArray();
            if(!empty($se)) {
                if ($se[0]->old_custom_sku) {
                    $detail['custom_sku'] = $se[0]->old_custom_sku;
                } else {
                    $detail['custom_sku'] = $se[0]->custom_sku;
                }
            }else{
                return ['code'=>0,'data'=>'','error'=>'系统没有'.$detail['custom_sku']];
            }
            $xml_data .= '<orderLine>
                                <itemCode>'.$detail['custom_sku'].'</itemCode>
                                <planQty>'. $detail['num'] .'</planQty>
                                <ownerCode>SKD</ownerCode>
                          </orderLine>';
        }
        $xml_data .= '</orderLines>
                         </request>';
        $xml_data = str_replace(PHP_EOL, '', $xml_data);

        //发起请求
        $url = CloudHouseController::$link . $strParam;

        try{
            $result = CloudHouseController::curl_xml($url, $xml_data);
            return ['code'=>1,'data'=>$result,'error'=>''];
        }catch(\Exception $e){
            return ['code'=>0,'data'=>'','error'=>$e];
        }
    }


}
