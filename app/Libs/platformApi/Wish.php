<?php
namespace App\Libs\platformApi;
use Illuminate\Support\Facades\DB;

class Wish{
//////////////////////////////////////////////// wish--api
	public $client_id;          //客户id
	public $client_secret;      // 秘钥
	public $refresh_token;      //刷新令牌
	public $access_token;      //访问令牌
	public $shop_id;          //店铺id
	
	/*
	 *  初始化
	 * @param client_id 客户id
	 * @param client_secret 秘钥
	 * @param refresh_token 刷新令牌
	 * @param access_token 访问令牌
	 * @parma shop_id 店铺id
	 * */
	public function __construct($data=array())
	{
		$this->client_id = empty($data['client_id']) ? '' : $data['client_id'];
		$this->client_secret = empty($data['client_secret']) ? '' : $data['client_secret'];
		$this->refresh_token = empty($data['refresh_token']) ? '' : $data['refresh_token'];
		$this->access_token = empty($data['access_token']) ? '' : $data['access_token'];
		$this->shop_id = empty($data['shop_id']) ? '' : $data['shop_id'];
	}
	
	/*
	 * 上传产品
	 * */
	public function product_add($data)
	{
		$url = "https://merchant.wish.com/api/v2/product/add";
        /*$extra_images = $data['spu']['extra_images'];
        $extra_images = explode(',',$extra_images);
        $extra_images = implode('|',$extra_images);*/
		$post_data = array();
		$post_data['name'] = $data['spu']['name'];// 产品名称
		$post_data['parent_sku'] = $data['spu']['parent_sku'];// 父sku
		$post_data['description'] = $data['spu']['description'];// 产品说明
		$post_data['tags'] = $data['spu']['tags'];// 标签
		$post_data['shipping'] = $data['spu']['shipping'];// 运费
		$post_data['localized_shipping'] = $data['spu']['localized_shipping'];//运费  可选 对于非美元商人，这是必需的
//		$post_data['requested_product_brand_id'] = $data['spu']['requested_product_brand_id'];// 品牌id   可选
		$post_data['landing_page_url'] = $data['spu']['landing_page_url'];//产品详细信息  可选
		$post_data['extra_images'] = $data['spu']['extra_images'];//产品多余照片    可选
		$post_data['clean_image'] = $data['spu']['clean_image'];//产品的干净图片     可选
        if($data['spu']['max_quantity']){
            $post_data['max_quantity'] = $data['spu']['max_quantity'];//每个订单的最大产品数量   可选
        }

		$post_data['demo_video_asset_url'] = $data['spu']['demo_video_asset_url'];//产品介绍视频    可选
		
		$post_data['sku'] = $data['sku']['sku'];//sku
		$post_data['color'] = $data['sku']['color'];//sku颜色  可选
		$post_data['size'] = $data['sku']['size'];//sku尺码  可选
		$post_data['inventory'] = $data['sku']['inventory'];//sku库存
		$post_data['price'] = $data['sku']['price'];//sku价格
		$post_data['localized_price'] = $data['sku']['localized_price'];//商户本国货币的变动价格  可选   对于非美元商人，这是必需的
		$post_data['localized_currency_code'] = 'CNY';//$data['sku']['localized_currency_code'];
		//$post_data['shipping_time'] = $data['sku']['shipping_time'];//运送时间  可选  下限不能少于2天。示例：15-20
		$post_data['shipping_time'] = '10-15';//运送时间  可选  下限不能少于2天。示例：15-20
		$post_data['main_image'] = $data['sku']['main_image'];//sku 主图
		$post_data['length'] = $data['sku']['length'];//sku包装后长度，单位厘米   可选
		$post_data['width'] = $data['sku']['width'];//sku包装后宽度，单位厘米   可选
		$post_data['height'] = $data['sku']['height'];// sku包装后高度，单位厘米   可选
		$post_data['weight'] = $data['sku']['weight'];//sku包装后重量，单位克   可选
		//$post_data['declared_name'] = $data['sku']['declared_name'];//物流申报的产品名称   可选
		//$post_data['pieces'] = $data['sku']['pieces'];//件   可选
		//$post_data['declared_value'] = $data['sku']['declared_value'];//将要声明为定制的产品价格   可选
		//$post_data['origin_country'] = $data['sku']['origin_country'];//自造国家   可选
		//$post_data['has_powder'] = $data['sku']['has_powder'];//产品是否包含粉末  true，false   可选
		//$post_data['has_liquid'] = $data['sku']['has_liquid'];//产品是否包含液体  true，false   可选
		//$post_data['has_battery'] = $data['sku']['has_battery'];//产品是否包含电池  true，false   可选
		//$post_data['has_metal'] = $data['sku']['has_metal'];//产品是否包含金属  true，false   可选
        $post_data['MSRP'] = $data['sku']['msrp'];//建议零售价  可选
		$post_data = http_build_query($post_data);
		
		$head = array(
			'Expect:',
			"authorization: Bearer {$this->access_token}"
		);
		
		$call_comm = new \App\Libs\wrapper\Comm();// wish 类
		$curl_res = $call_comm->curls($url, $post_data, $head);//返回json
		$curl_res = json_decode($curl_res, true);
        //var_dump($curl_res);die;
		if ($curl_res['code'] != 0) {
			$is_overdue_token = strstr($curl_res['message'], 'access token');
			if (!$is_overdue_token) {
				return $curl_res;
			}
			
			//This access token has been revoked ：此访问令牌已被吊销
			// 刷新 access_token
			$refresh_token = $this->refresh_token();
			if ($refresh_token !== true) {
				return $refresh_token;
			}
			
			// 重新调用
			$head = array(
				'Expect:',
				"authorization: Bearer {$this->access_token}"
			);
			$curl_res = $call_comm->curls($url, $post_data, $head);//返回json
			$curl_res = json_decode($curl_res, true);
		}
		
		return $curl_res;
	}
	
	/*
	 * 新增SKU
	 * */
	public function product_sku_add($data){
		$url = "https://www.merchant.wish.com/api/v2/variant/add";
		
		$post_data = array();
		$post_data['parent_sku'] = $data['parent_sku'];// 父SKU
		$post_data['sku'] = $data['sku'];// sku
		$post_data['color'] = $data['color'];// sku颜色  可选
		$post_data['size'] = $data['size'];// sku尺码  可选
		$post_data['inventory'] = $data['inventory'];// sku库存
		$post_data['price'] = $data['price'];// sku价格
		$post_data['localized_price'] = $data['localized_price'];//商户本国货币的变动价格  可选   对于非美元商人，这是必需的
		$post_data['localized_currency_code'] = 'CNY';// 商户的本地货币代码    可选
//		$post_data['shipping_time'] = $data['shipping_time'];// 运送时间  可选  下限不能少于2天。示例：15-20
		$post_data['shipping_time'] = '10-15';// 运送时间  可选  下限不能少于2天。示例：15-20
		$post_data['main_image'] = $data['main_image'];// sku 主图
		$post_data['length'] = $data['length'];// sku包装后长度，单位厘米   可选
		$post_data['width'] = $data['width'];// sku包装后宽度，单位厘米   可选
		$post_data['height'] = $data['height'];// sku包装后高度，单位厘米   可选
		$post_data['weight'] = $data['weight'];// sku包装后重量，单位克   可选
		//$post_data['declared_name'] = $data['declared_name'];// 物流申报的产品名称   可选
		//$post_data['pieces'] = $data['pieces'];// 件   可选
		//$post_data['declared_value'] = $data['declared_value'];// 将要声明为定制的产品价格   可选
		//$post_data['origin_country'] = $data['origin_country'];// 自造国家   可选
		//$post_data['has_powder'] = $data['has_powder'];// 产品是否包含粉末  true，false   可选
		//$post_data['has_liquid'] = $data['has_liquid'];// 产品是否包含液体  true，false   可选
		//$post_data['has_battery'] = $data['has_battery'];// 产品是否包含电池  true，false   可选
		//$post_data['has_metal'] = $data['has_metal'];// 产品是否包含金属  true，false   可选
		
		$post_data = http_build_query($post_data);
		
		$head = array(
			'Expect:',
			"authorization: Bearer {$this->access_token}"
		);
		
		$call_comm = new \App\Libs\wrapper\Comm();// wish 类
		$curl_res = $call_comm->curls($url, $post_data, $head);//返回json
		$curl_res = json_decode($curl_res, true);
		if ($curl_res['code'] != 0) {
			$is_overdue_token = strstr($curl_res['message'], 'access token');
			if (!$is_overdue_token) {
				return $curl_res;
			}
			
			//This access token has been revoked ：此访问令牌已被吊销
			// 刷新 access_token
			$refresh_token = $this->refresh_token();
			if ($refresh_token !== true) {
				return $refresh_token;
			}
			
			// 重新调用
			$head = array(
				'Expect:',
				"authorization: Bearer {$this->access_token}"
			);
			$curl_res = $call_comm->curls($url, $post_data, $head);//返回json
			$curl_res = json_decode($curl_res, true);
		}
		
		return $curl_res;
	}
	
	
	
	/**
	 * 获取商品信息
	 * @param access_token
	 * @param start 偏移数
	 * @param limit 每页条数
	 * @param since 开始时间  UTC时间
	 * @param upto 结束时间  UTC时间
	 */
	public function product_get($data){
		if(empty($data)){
			return '接收值为空';
		}
		
		
		$start = ($data['page'] - 1) * $data['limit'];
		$limit = $data['limit'];
		$since = gmdate('Y-m-d\TH:i:s', strtotime($data['time_start']));//开始时间
		$upto = gmdate('Y-m-d\TH:i:s', strtotime($data['time_end']));//结束时间
//		$since = '2021-02-20T05:09:56';
//		$upto = '2021-04-20T05:09:56';
		$show_rejected = 'true';//可选如果指定为“ true”，则此API将返回所有产品，包括在审核过程中被拒绝的不适当产品
		
		//// 获取商品
		$url='https://merchant.wish.com/api/v2/product/multi-get';
		$url .= "?start={$start}";//偏移数
		$url .= "&limit={$limit}";//每页条数
		$url .= "&show_rejected={$show_rejected}";// 是否显示所有数据
		$url .= "&since={$since}";//开始时间
		$url .= "&upto={$upto}";//结束时间
		
		$head = array(
			'Expect:',
			"authorization: Bearer {$this->access_token}"
		);
		
		$call_comm = new \App\Libs\wrapper\Comm();// wish 类
		$curl_res = $call_comm->curls($url, false , $head);//返回json
		$curl_res = json_decode($curl_res,true);
		if ($curl_res['code'] != 0) {
			$is_overdue_token = strstr($curl_res['message'], 'access token');
			if (!$is_overdue_token) {
				return $curl_res;
			}
			
			//This access token has been revoked ：此访问令牌已被吊销
			// 刷新 access_token
			$refresh_token = $this->refresh_token();
			if ($refresh_token !== true) {
				return $refresh_token;
			}
			
			// 重新调用
			$head = array(
				'Expect:',
				"authorization: Bearer {$this->access_token}"
			);
			$curl_res = $call_comm->curls($url, false, $head);//返回json
			$curl_res = json_decode($curl_res, true);
		}
		
		return $curl_res;
	}

////////////////////// access_token
	/**
	 * 获取 access_token
	 * @param client_id
	 * @parma client_secret
	 * @param code
	 * @parma grant_type 默认  authorization_code
	 * @Parma redirect_uri 回调地址
	 * */
	public function access_token_get($data){
		if(empty($data['code'])){
			return 'code为空';
		}
		
		//// 获取商品
		$url='https://www.merchant.wish.com/api/v3/oauth/access_token';
		$url .= "?client_id={$data['client_id']}";//偏移数
		$url .= "&client_secret={$data['client_secret']}";//每页条数
		$url .= "&code={$data['code']}";// 是否显示所有数据
		$url .= "&grant_type=authorization_code";//开始时间
		$url .= "&redirect_uri={$data['redirect_uri']}";//结束时间
		
		$head = array(
			'Expect:',
		);
		
		$call_comm = new \App\Libs\wrapper\Comm();// wish 类
		$curl_res = $call_comm->curls($url, false , $head);//返回json
		$curl_res = json_decode($curl_res,true);
		return $curl_res;
	}
 
	/*
	 * 刷新 access_token
	 * url：https://merchant.wish.com/api/v3/oauth/refresh_token
	 * 请求：Get
	 * @param shop_id 店铺id
	 * @param client_id 客户id
	 * @param client_secret 秘钥
	 * @param refresh_token 获取access_token（上个接口获取到的数据）
	 * @param grant_type 默认  refresh_token
	 *
	 * http://newzity.com/pc/goods/refresh_token
	 * */
	public function refresh_token(){
		//// 获取access_token
		$url = "https://merchant.wish.com/api/v3/oauth/refresh_token";
		$url .= "?client_id={$this->client_id}";
		$url .= "&client_secret={$this->client_secret}";
		$url .= "&refresh_token={$this->refresh_token}";
		$url .= "&grant_type=refresh_token";
		
		$call_comm = new \App\Libs\wrapper\Comm();// wish 类
		$curl_res = $call_comm->curls($url);//返回json
		$curl_res = json_decode($curl_res,true);
		if($curl_res['code'] != 0){
			return $curl_res;
		}
		
		//// 更新access_token
		$sql = "update shop
					set access_token='{$curl_res['data']['access_token']}'
					where `Id` = {$this->shop_id}";
		$update = db::update($sql);
		
		$this->access_token = $curl_res['data']['access_token'];
		return true;
	}

    //更新产品基本信息
    public function updateProduct($token, $productId, $name, $desc, $mainImage){
        $access_token = urlencode($token);
        //产品id
        $id = urlencode($productId);
        $name = urlencode($name);
        $description = urlencode($desc);
    //    $tags = urlencode($tags);

//        $url = sprintf(
//            "https://merchant.wish.com/api/v2/product/update?access_token=%s&id=%s&name=%s&description=%s&tags=%s",
//            $access_token, $id, $name, $description, $tags);
        $url = sprintf(
            "https://merchant.wish.com/api/v2/product/update?access_token=%s&id=%s&name=%s&description=%s&main_image=%s",
            $access_token, $id, $name, $description, $mainImage);

        $context = stream_context_create(array(
            'http' => array(
                'method'        => 'POST',
                'ignore_errors' => true,
            ),
        ));

        // Send the request
        $response = file_get_contents($url, TRUE, $context);
        return (array)json_decode($response,true);
    }
    //更新父sku
    public function changeSku($token, $sku, $newSku){
        $access_token = urlencode($token);
        $parent_sku = urlencode($sku);
        $new_parent_sku = urlencode($newSku);

        $url = sprintf(
            "https://merchant.wish.com/api/v2/product/change-sku?access_token=%s&parent_sku=%s&new_parent_sku=%s",
            $access_token, $parent_sku, $new_parent_sku);

        $context = stream_context_create(array(
            'http' => array(
                'method'        => 'POST',
                'ignore_errors' => true,
            ),
        ));

        $response = file_get_contents($url, TRUE, $context);
        return (array)json_decode($response,true);
    }
    //启用产品
    public function enableProduct($token, $productId){
        $access_token = urlencode($token);

        $id = urlencode($productId);

        $url = sprintf(
            "https://merchant.wish.com/api/v2/product/enable?access_token=%s&id=%s",
            $access_token, $id);

        $context = stream_context_create(array(
            'http' => array(
                'method'        => 'POST',
                'ignore_errors' => true,
            ),
        ));

        // Send the request
        $response = file_get_contents($url, TRUE, $context);
        return (array)json_decode($response,true);
    }
    //禁用产品
    public function disableProduct($token, $productId){
        $access_token = urlencode($token);

        $id = urlencode($productId);

        $url = sprintf(
            "https://merchant.wish.com/api/v2/product/disable?access_token=%s&id=%s",
            $access_token, $id);

        $context = stream_context_create(array(
            'http' => array(
                'method'        => 'POST',
                'ignore_errors' => true,
            ),
        ));

        // Send the request
        $response = file_get_contents($url, TRUE, $context);
        return (array)json_decode($response,true);
    }
    //更新库存
    public function updateInventory($token, $childSku, $inventory){
        $access_token = urlencode($token);
        //子sku
        $sku = urlencode($childSku);
        $inventory = urlencode($inventory);

        $url = sprintf(
            "https://merchant.wish.com/api/v2/variant/update-inventory?access_token=%s&sku=%s&inventory=%s",
            $access_token, $sku, $inventory);

        $context = stream_context_create(array(
            'http' => array(
                'method'        => 'POST',
                'ignore_errors' => true,
            ),
        ));

        // Send the request
        $response = file_get_contents($url, TRUE, $context);
        return (array)json_decode($response,true);
    }
	//更新规格
    public function updateVariant($token, $sku, $price, $inventory, $color, $size, $image){
        $access_token = urlencode($token);
        $sku = urlencode($sku);
       // $sku = urlencode('red-shoe-222');
        $localized_price = $price;
        $localized_currency_code = 'CNY';
      //  $color = urldecode($color);
        $size = urldecode($size);
//        $url = sprintf(
//            "https://merchant.wish.com/api/v2/variant/update?access_token=%s&sku=%s&price=%s",
//            $access_token, $sku,$new_price);

        //&main_image=%s&update_product_image=%s  &color=%s
        //, 'https://canary.contestimg.wish.com/api/webimage/5ea164af0775195d52c8b3b2-tiny.jpg','false'
        $url = sprintf(
            "https://merchant.wish.com/api/v2/variant/update?access_token=%s&sku=%s&localized_price=%s&localized_currency_code=%s&inventory=%s&size=%s&main_image=%s&update_product_image=%s",
            $access_token, $sku, $localized_price, $localized_currency_code, $inventory, $size, $image, 'false');
        $context = stream_context_create(array(
            'http' => array(
                'method'        => 'POST',
                'ignore_errors' => true,
            ),
        ));
        // Send the request
        $response = file_get_contents($url, TRUE, $context);
        return (array)json_decode($response,true);
    }
    //更新邮费
    public function updateShipping($token,$productId,$shipping){
        $access_token = urlencode($token);
        $id = urlencode($productId);

        $localized_AD=urlencode($shipping);
        $localized_AE=urlencode($shipping);
        $localized_AG=urlencode($shipping);
        $localized_AI=urlencode($shipping);
        $localized_AL=urlencode($shipping);
        $localized_AM=urlencode($shipping);
        $localized_AR=urlencode($shipping);
        $localized_AT=urlencode($shipping);
        $localized_AU=urlencode($shipping);
        $localized_AW=urlencode($shipping);
        $localized_AZ=urlencode($shipping);
        $localized_BA=urlencode($shipping);
        $localized_BB=urlencode($shipping);
        $localized_BD=urlencode($shipping);
        $localized_BE=urlencode($shipping);
        $localized_BG=urlencode($shipping);
        $localized_BH=urlencode($shipping);
        $localized_BM=urlencode($shipping);
        $localized_BO=urlencode($shipping);
        $localized_BR=urlencode($shipping);
        $localized_BS=urlencode($shipping);
        $localized_BT=urlencode($shipping);
        $localized_BW=urlencode($shipping);
        $localized_BZ=urlencode($shipping);
        $localized_CA=urlencode($shipping);
        $localized_CH=urlencode($shipping);
        $localized_CL=urlencode($shipping);
        $localized_CO=urlencode($shipping);
        $localized_CR=urlencode($shipping);
        $localized_CY=urlencode($shipping);
        $localized_CZ=urlencode($shipping);
        $localized_DE=urlencode($shipping);
        $localized_DK=urlencode($shipping);
        $localized_DM=urlencode($shipping);
        $localized_DO=urlencode($shipping);
        $localized_DZ=urlencode($shipping);
        $localized_EC=urlencode($shipping);
        $localized_EE=urlencode($shipping);
        $localized_EG=urlencode($shipping);
        $localized_ES=urlencode($shipping);
        $localized_FI=urlencode($shipping);
        $localized_FJ=urlencode($shipping);
        $localized_FR=urlencode($shipping);
        $localized_GA=urlencode($shipping);
        $localized_GB=urlencode($shipping);
        $localized_GD=urlencode($shipping);
        $localized_GE=urlencode($shipping);
        $localized_GH=urlencode($shipping);
        $localized_GI=urlencode($shipping);
        $localized_GR=urlencode($shipping);
        $localized_GT=urlencode($shipping);
        $localized_GY=urlencode($shipping);
        $localized_HN=urlencode($shipping);
        $localized_HR=urlencode($shipping);
        $localized_HU=urlencode($shipping);
        $localized_ID=urlencode($shipping);
        $localized_IE=urlencode($shipping);
        $localized_IL=urlencode($shipping);
        $localized_IN=urlencode($shipping);
        $localized_IS=urlencode($shipping);
        $localized_IT=urlencode($shipping);
        $localized_JE=urlencode($shipping);
        $localized_JM=urlencode($shipping);
        $localized_JO=urlencode($shipping);
        $localized_JP=urlencode($shipping);
        $localized_KE=urlencode($shipping);
        $localized_KG=urlencode($shipping);
        $localized_KH=urlencode($shipping);
        $localized_KR=urlencode($shipping);
        $localized_KW=urlencode($shipping);
        $localized_KY=urlencode($shipping);
        $localized_KZ=urlencode($shipping);
        $localized_LC=urlencode($shipping);
        $localized_LI=urlencode($shipping);
        $localized_LK=urlencode($shipping);
        $localized_LT=urlencode($shipping);
        $localized_LU=urlencode($shipping);
        $localized_LV=urlencode($shipping);
        $localized_MA=urlencode($shipping);
        $localized_MC=urlencode($shipping);
        $localized_MD=urlencode($shipping);
        $localized_ME=urlencode($shipping);
        $localized_MK=urlencode($shipping);
        $localized_MM=urlencode($shipping);
        $localized_MN=urlencode($shipping);
        $localized_MS=urlencode($shipping);
        $localized_MT=urlencode($shipping);
        $localized_MU=urlencode($shipping);
        $localized_MV=urlencode($shipping);
        $localized_MX=urlencode($shipping);
        $localized_MY=urlencode($shipping);
        $localized_NA=urlencode($shipping);
        $localized_NG=urlencode($shipping);
        $localized_NI=urlencode($shipping);
        $localized_NL=urlencode($shipping);
        $localized_NO=urlencode($shipping);
        $localized_NP=urlencode($shipping);
        $localized_NU=urlencode($shipping);
        $localized_NZ=urlencode($shipping);
        $localized_OM=urlencode($shipping);
        $localized_PA=urlencode($shipping);
        $localized_PE=urlencode($shipping);
        $localized_PH=urlencode($shipping);
        $localized_PK=urlencode($shipping);
        $localized_PL=urlencode($shipping);
        $localized_PR=urlencode($shipping);
        $localized_PT=urlencode($shipping);
        $localized_PY=urlencode($shipping);
        $localized_QA=urlencode($shipping);
        $localized_RE=urlencode($shipping);
        $localized_RO=urlencode($shipping);
        $localized_RS=urlencode($shipping);
        $localized_RU=urlencode($shipping);
        $localized_SA=urlencode($shipping);
        $localized_SC=urlencode($shipping);
        $localized_SE=urlencode($shipping);
        $localized_SG=urlencode($shipping);
        $localized_SH=urlencode($shipping);
        $localized_SI=urlencode($shipping);
        $localized_SK=urlencode($shipping);
        $localized_SM=urlencode($shipping);
        $localized_SR=urlencode($shipping);
        $localized_SV=urlencode($shipping);
        $localized_SZ=urlencode($shipping);
        $localized_TH=urlencode($shipping);
        $localized_TN=urlencode($shipping);
        $localized_TO=urlencode($shipping);
        $localized_TR=urlencode($shipping);
        $localized_TT=urlencode($shipping);
        $localized_TW=urlencode($shipping);
        $localized_UA=urlencode($shipping);
        $localized_US=urlencode($shipping);
        $localized_UY=urlencode($shipping);
        $localized_UZ=urlencode($shipping);
        $localized_VE=urlencode($shipping);
        $localized_VG=urlencode($shipping);
        $localized_VI=urlencode($shipping);
        $localized_VN=urlencode($shipping);
        $localized_ZA=urlencode($shipping);
        $localized_ZM=urlencode($shipping);



        $localized_currency_code = urlencode('CNY');
      //  $disabled_countries = urlencode('MX,BR');
     //   $wish_express_add_countries = urlencode('FR,ES');
     //   $wish_express_remove_countries = urlencode('US,CA');
        $default_shipping_price = urlencode($shipping);
        $localized_default_shipping_price = urlencode($shipping);
		
        $url = sprintf(
            "https://merchant.wish.com/api/v2/product/update-multi-shipping?access_token=%s&id=%s&localized_currency_code=%s&default_shipping_price=%s&localized_default_shipping_price=%s&localized_AD=%s&localized_AE=%s&localized_AG=%s&localized_AI=%s&localized_AL=%s&localized_AM=%s&localized_AR=%s&localized_AT=%s&localized_AU=%s&localized_AW=%s&localized_AZ=%s&localized_BA=%s&localized_BB=%s&localized_BD=%s&localized_BE=%s&localized_BG=%s&localized_BH=%s&localized_BM=%s&localized_BO=%s&localized_BR=%s&localized_BS=%s&localized_BT=%s&localized_BW=%s&localized_BZ=%s&localized_CA=%s&localized_CH=%s&localized_CL=%s&localized_CO=%s&localized_CR=%s&localized_CY=%s&localized_CZ=%s&localized_DE=%s&localized_DK=%s&localized_DM=%s&localized_DO=%s&localized_DZ=%s&localized_EC=%s&localized_EE=%s&localized_EG=%s&localized_ES=%s&localized_FI=%s&localized_FJ=%s&localized_FR=%s&localized_GA=%s&localized_GB=%s&localized_GD=%s&localized_GE=%s&localized_GH=%s&localized_GI=%s&localized_GR=%s&localized_GT=%s&localized_GY=%s&localized_HN=%s&localized_HR=%s&localized_HU=%s&localized_ID=%s&localized_IE=%s&localized_IL=%s&localized_IN=%s&localized_IS=%s&localized_IT=%s&localized_JE=%s&localized_JM=%s&localized_JO=%s&localized_JP=%s&localized_KE=%s&localized_KG=%s&localized_KH=%s&localized_KR=%s&localized_KW=%s&localized_KY=%s&localized_KZ=%s&localized_LC=%s&localized_LI=%s&localized_LK=%s&localized_LT=%s&localized_LU=%s&localized_LV=%s&localized_MA=%s&localized_MC=%s&localized_MD=%s&localized_ME=%s&localized_MK=%s&localized_MM=%s&localized_MN=%s&localized_MS=%s&localized_MT=%s&localized_MU=%s&localized_MV=%s&localized_MX=%s&localized_MY=%s&localized_NA=%s&localized_NG=%s&localized_NI=%s&localized_NL=%s&localized_NO=%s&localized_NP=%s&localized_NU=%s&localized_NZ=%s&localized_OM=%s&localized_PA=%s&localized_PE=%s&localized_PH=%s&localized_PK=%s&localized_PL=%s&localized_PR=%s&localized_PT=%s&localized_PY=%s&localized_QA=%s&localized_RE=%s&localized_RO=%s&localized_RS=%s&localized_RU=%s&localized_SA=%s&localized_SC=%s&localized_SE=%s&localized_SG=%s&localized_SH=%s&localized_SI=%s&localized_SK=%s&localized_SM=%s&localized_SR=%s&localized_SV=%s&localized_SZ=%s&localized_TH=%s&localized_TN=%s&localized_TO=%s&localized_TR=%s&localized_TT=%s&localized_TW=%s&localized_UA=%s&localized_US=%s&localized_UY=%s&localized_UZ=%s&localized_VE=%s&localized_VG=%s&localized_VI=%s&localized_VN=%s&localized_ZA=%s&localized_ZM=%s",
            $access_token, $id, $localized_currency_code, $default_shipping_price, $localized_default_shipping_price,$localized_AD,$localized_AE,$localized_AG,$localized_AI,$localized_AL,$localized_AM,$localized_AR,$localized_AT,$localized_AU,$localized_AW,$localized_AZ,$localized_BA,$localized_BB,$localized_BD,$localized_BE,$localized_BG,$localized_BH,$localized_BM,$localized_BO,$localized_BR,$localized_BS,$localized_BT,$localized_BW,$localized_BZ,$localized_CA,$localized_CH,$localized_CL,$localized_CO,$localized_CR,$localized_CY,$localized_CZ,$localized_DE,$localized_DK,$localized_DM,$localized_DO,$localized_DZ,$localized_EC,$localized_EE,$localized_EG,$localized_ES,$localized_FI,$localized_FJ,$localized_FR,$localized_GA,$localized_GB,$localized_GD,$localized_GE,$localized_GH,$localized_GI,$localized_GR,$localized_GT,$localized_GY,$localized_HN,$localized_HR,$localized_HU,$localized_ID,$localized_IE,$localized_IL,$localized_IN,$localized_IS,$localized_IT,$localized_JE,$localized_JM,$localized_JO,$localized_JP,$localized_KE,$localized_KG,$localized_KH,$localized_KR,$localized_KW,$localized_KY,$localized_KZ,$localized_LC,$localized_LI,$localized_LK,$localized_LT,$localized_LU,$localized_LV,$localized_MA,$localized_MC,$localized_MD,$localized_ME,$localized_MK,$localized_MM,$localized_MN,$localized_MS,$localized_MT,$localized_MU,$localized_MV,$localized_MX,$localized_MY,$localized_NA,$localized_NG,$localized_NI,$localized_NL,$localized_NO,$localized_NP,$localized_NU,$localized_NZ,$localized_OM,$localized_PA,$localized_PE,$localized_PH,$localized_PK,$localized_PL,$localized_PR,$localized_PT,$localized_PY,$localized_QA,$localized_RE,$localized_RO,$localized_RS,$localized_RU,$localized_SA,$localized_SC,$localized_SE,$localized_SG,$localized_SH,$localized_SI,$localized_SK,$localized_SM,$localized_SR,$localized_SV,$localized_SZ,$localized_TH,$localized_TN,$localized_TO,$localized_TR,$localized_TT,$localized_TW,$localized_UA,$localized_US,$localized_UY,$localized_UZ,$localized_VE,$localized_VG,$localized_VI,$localized_VN,$localized_ZA,$localized_ZM);

        $context = stream_context_create(array(
            'http' => array(
                'method'        => 'POST',
                'ignore_errors' => true,
            ),
        ));
		
        // Send the request
        $response = file_get_contents($url, TRUE, $context);
        return (array)json_decode($response,true);
    }

    public static function variantAdd($token, $spuName, $sellerSku, $quantity, $price, $size, $color, $image){
        $access_token = urlencode($token);

        $parent_sku = urlencode($spuName);
        $sku = urlencode($sellerSku);
        $inventory = urlencode($quantity);

        $localized_price = urlencode($price);
        $price = urlencode($price);
        $localized_currency_code = 'CNY';

        $size = urlencode($size);
        $color = urlencode($color);
        $image = urlencode($image);
        $url = sprintf(
            "https://merchant.wish.com/api/v2/variant/add?access_token=%s&sku=%s&inventory=%s&price=%s&parent_sku=%s&localized_price=%s&localized_currency_code=%s&size=%s&color=%s&main_image=%s",
            $access_token, $sku, $inventory, $price, $parent_sku, $localized_price, $localized_currency_code,
            $size, $color, $image);
        $context = stream_context_create(array(
            'http' => array(
                'method'        => 'POST',
                'ignore_errors' => true,
            ),
        ));
        // Send the request
        $response = file_get_contents($url, TRUE, $context);
        return (array)json_decode($response,true);
    }

//
}