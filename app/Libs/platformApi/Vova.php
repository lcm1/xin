<?php

namespace App\Libs\platformApi;

use Illuminate\Support\Facades\DB;

class Vova
{
//////////////////////////////////////////////// vova--api
    public $access_token;      //访问令牌

    /*
     *  初始化
     * @param access_token 访问令牌
     * */
    public function __construct($data = array())
    {
        $this->access_token = empty($data['access_token']) ? '' : $data['access_token'];
    }

    /*
     * 上传产品
     * */
    public function product_add($data)
    {
        $url = "https://merchant.vova.com.hk/api/v1/product/uploadGoods";
        
        $items = array();
        $items['cat_id'] = $data['spu']['cat_id'];// 分类id
        $items['parent_sku'] = $data['spu']['parent_sku'];// 商品父sku
        $items['goods_name'] = $data['spu']['goods_name'];// 商品名称
		
        $items['goods_name_fr'] = $data['spu']['goods_name_fr'];// 商品名称(法语)  非必需
        $items['goods_name_de'] = $data['spu']['goods_name_de'];// 商品名称(德语)  非必需
        $items['goods_name_es'] = $data['spu']['goods_name_es'];// 商品名称(西班牙语)  非必需
        $items['goods_name_it'] = $data['spu']['goods_name_it'];//  商品名称(意大利语)  非必需
        $items['goods_description'] = $data['spu']['goods_description'];// 商品描述
        $items['goods_description_fr'] = $data['spu']['goods_description_fr'];// 商品描述(法语)  非必需
        $items['goods_description_de'] = $data['spu']['goods_description_de'];// 商品描述(德语)  非必需
        $items['goods_description_es'] = $data['spu']['goods_description_es'];// 商品描述(西班牙语)  非必需
        $items['goods_description_it'] = $data['spu']['goods_description_it'];// 商品描述(意大利语)  非必需
        $items['tags'] = !empty($data['spu']['tags']) ? $data['spu']['tags'] : '';// 商品标签或关键词，非必需
        $items['goods_brand'] = !empty($data['spu']['goods_brand']) ? $data['spu']['goods_brand'] : '';// 商品品牌  非必需
        $items['shipping_weight'] = $data['spu']['shipping_weight'];// 商品重量
        $items['shipping_time'] = $data['spu']['shipping_time'];// 运输时间  非必需
	    $items['main_image'] = $data['spu']['main_image'];// 商品主图
	    if(isset($data['spu']['extra_image_list'])){
		    // 其他附图
		    $extra_image_list = $data['spu']['extra_image_list'];
		    $items['extra_image_list'] = implode(',',$extra_image_list);
	    }
		
        $items['goods_sku'] = $data['sku'][0]['goods_sku'];// 商品子sku
        $items['market_price'] = $data['sku'][0]['market_price'];// 商品市场价  非必需
        $items['shop_price'] = $data['sku'][0]['shop_price'];// 商品价格
        $items['storage'] = $data['sku'][0]['storage'];// 库存
        $items['shipping_fee'] = $data['sku'][0]['shipping_fee'];// 商品运费
        $items['style_size'] = $data['sku'][0]['style_size'];// 尺码
        $items['style_color'] = $data['sku'][0]['style_color'];// 颜色
        $items['style_quantity'] = $data['sku'][0]['style_quantity'];// 批量售卖数量  非必需
        $items['extra_image'] = $data['sku'][0]['extra_image'];// 商品附图
//        return $items;
        $arr = array(
            "token" => $this->access_token, //$access_token
            "items" => array($items), // 产品信息
            'ignore_warning' => 0 // 上传商品时是否忽略警告信息。如忽略警告信息，请填“1”
        );
        $post_data = json_encode($arr);
		
        $call_comm = new \App\Libs\wrapper\Comm();// wish 类
        $curl_res = $call_comm->curls($url, $post_data);//返回json
        $curl_res = json_decode($curl_res, true);
        if (!is_array($curl_res)) {
            $is_overdue_token = strstr($curl_res, 'Token');
            if (!$is_overdue_token) {
                return 'Token错误，请确认Token是否正确';
            }
        }

        return $curl_res;
    }
	
    /*
     * 尺码表
     * */
    public function size_vova($data)
    {
	    $arr = array(
		    "token" => $this->access_token, //商家后台API Token
		    "product_id" => $data['product_id'], // 产品id
		    'size_data' => $data['size_data'], // 上传商品时是否忽略警告信息。如忽略警告信息，请填“1”
		    'size_remark' => 0, //0 尺码正常, 1 产品尺码偏大, 2 产品尺码偏小
	    );
		
	    $post_data = json_encode($arr);
        //var_dump($post);die;
        $url = "https://merchant.vova.com.hk/api/v1/product/uploadSizeChart";
        $call_comm = new \App\Libs\wrapper\Comm();// wish 类
        $curl_res = $call_comm->curls($url, $post_data);//返回json
        $curl_res = json_decode($curl_res, true);
        if (!is_array($curl_res)) {
            $is_overdue_token = strstr($curl_res, 'Token');
            if (!$is_overdue_token) {
                return 'Token错误，请确认Token是否正确';
            }
        }
        return $curl_res;
    }

    /*
     * 新增产品SKU
     * */
    public function product_sku_add($data)
    {
        $url = "https://merchant.vova.com.hk/api/v1/product/addProductSku";
			
        $items = array();
        $items['parent_sku'] = $data['parent_sku'];// 商品父sku

        $items['goods_sku'] = $data['goods_sku'];// 商品子sku
        $items['shop_price'] = $data['shop_price'];// 商品价格
        $items['market_price'] = $data['market_price'];// 商品市场价
        $items['storage'] = $data['storage'];// 库存
        $items['shipping_fee'] = $data['shipping_fee'];// 商品运费

        $items['style_array']['size'] = $data['style_size'];// 尺码
        $items['style_array']['color'] = $data['style_color'];// 颜色
        $items['style_array']['style_quantity'] = $data['style_quantity'];//批量售卖数量  非必需

        $items['sku_image'] = $data['extra_image'];// 商品附图

        $arr = array(
            "token" => $this->access_token, //$access_token
            "items" => array($items), // 产品信息
            'ignore_warning' => 0 // 上传商品时是否忽略警告信息。如忽略警告信息，请填“1”
        );

        $post_data = json_encode($arr);

        $call_comm = new \App\Libs\wrapper\Comm();// wish 类
        $curl_res = $call_comm->curls($url, $post_data);//返回json
        $curl_res = json_decode($curl_res, true);
        if (!is_array($curl_res)) {
            $is_overdue_token = strstr($curl_res, 'Token');
            if (!$is_overdue_token) {
                return 'Token错误，请确认Token是否正确';
            }
        }

        return $curl_res;
    }


    /**
     * 获取商品信息
     * @param $data 查询参数
     */
    public function product_get($data)
    {
        if (empty($data)) {
            return '接收值为空';
        }

        $conditions = array();//查询条件
        /*
         * goods_state_filter 查询商品信息参数    上下架状态 缺省或传值为空时，默认 only_on_sale
         * only_on_sale 已上架
         * only_off_sale 已下架
         * only_wait_for_check 待上架
         * on_sale_and_off_sale 上架和下架
         * on_sale_and_wait_for_check 上架和待上架
         * off_sale_and_wait_for_check 下架和待上架
         * on_sale_and_off_sale_and_wait_for_check 上下架和待上架，使用该参数时，可以查询到已删除的商品
         * */
        $conditions['goods_state_filter'] = 'on_sale_and_off_sale_and_wait_for_check';//上下架和待上架
        if (!empty($data['goods_state_filter'])) {
            $conditions['goods_state_filter'] = $data['goods_state_filter'];
        }
        /*
         * middle_east_sale 集运状态 缺省或传值为空时，默认全部
         * middle_east_on_sale 中东地区上架
         * middle_east_off_sale 中东地区下架
         * fr_collection 法国地区集运
         * fr_not_collection 法国地区不集运
         * */
        if (!empty($data['middle_east_sale'])) {
            $conditions['middle_east_sale'] = $data['middle_east_sale'];
        }
        /*
         * goods_promotion 锁定状态 缺省或传值为空时，默认全部
         * only_on_promotion 已锁定
         * only_not_promotion 未锁定
         * */
        if (!empty($data['goods_promotion'])) {
            $conditions['goods_promotion'] = $data['goods_promotion'];
        }
        /*
         * ban_status 禁售状态 缺省或传值为空时，默认全部
         * banned 已禁售
         * unbanned 未禁售
         * */
        if (!empty($data['ban_status'])) {
            $conditions['ban_status'] = $data['ban_status'];
        }
        /*
         * upload_batch_id 上传商品批次ID，需要配合goods_state_filter值为 only_wait_for_check（待上架状态）时使用
         * */
        if (!empty($data['upload_batch_id'])) {
            $conditions['upload_batch_id'] = $data['upload_batch_id'];
        }
        /*
         * search 查询  array
         * search_type 查询类型
         *      product_id 网站产品id
         *      product_name 产品名称
         *      goods_sku 子sku
         *      parent_sku 父sku
         *      cat_id 商品分类id
         * search_value 查询类型对应的值
         * */
        if (!empty($data['search_product_id'])) {
            //product_id 网站产品id
            $conditions['search']['search_type'] = 'product_id';
            $conditions['search']['search_value'] = $data['search_product_id'];
        }
        if (!empty($data['search_product_name'])) {
            //product_name 产品名称
            $conditions['search']['search_type'] = 'product_name';
            $conditions['search']['search_value'] = $data['search_product_name'];
        }
        if (!empty($data['search_goods_sku'])) {
            //goods_sku 子sku
            $conditions['search']['search_type'] = 'goods_sku';
            $conditions['search']['search_value'] = $data['search_goods_sku'];
        }
        if (!empty($data['search_parent_sku'])) {
            //parent_sku 父sku
            $conditions['search']['search_type'] = 'parent_sku';
            $conditions['search']['search_value'] = $data['search_parent_sku'];
        }
        if (!empty($data['search_cat_id'])) {
            //cat_id 商品分类id
            $conditions['search']['search_type'] = 'cat_id';
            $conditions['search']['search_value'] = $data['search_cat_id'];
        }
        /*
         * add_time 上传时间  array
         * start  非必须，查询开始时间，UTC时间，格式Y-m-d H:i:s
         * end  非必须，查询结束时间，UTC时间，格式Y-m-d H:i:s
         * */
        if (!empty($data['add_time_start'])) {
            //上传时间--开始时间
            $conditions['add_time']['start'] = $data['add_time_start'];
        }
        if (!empty($data['search_product_end'])) {
            //上传时间--结束时间
            $conditions['add_time']['end'] = $data['search_product_end'];
        }
        /*
         * last_update_time 更新时间  array
         * start  非必须，查询开始时间，UTC时间，格式Y-m-d H:i:s
         * end  非必须，查询结束时间，UTC时间，格式Y-m-d H:i:s
         * */
        if (!empty($data['last_update_time_start'])) {
            //更新时间--开始时间
            $conditions['last_update_time']['start'] = gmdate('Y-m-d\TH:i:s', strtotime($data['last_update_time_start']));
        }
        if (!empty($data['last_update_time_end'])) {
            //更新时间--结束时间
            $conditions['last_update_time']['end'] = gmdate('Y-m-d\TH:i:s', strtotime($data['last_update_time_end']));
        }
        /*
         * page_arr 分页  array
         * perPage  每页条数，默认100，最大不能超过200条
         * page     页码，默认 1
         * */
        if (!empty($data['page_arr_perPage'])) {
            //每页条数
            $conditions['page_arr']['perPage'] = $data['page_arr_perPage'];
        }
        if (!empty($data['page_arr_page'])) {
            //页码
            $conditions['page_arr']['page'] = $data['page_arr_page'];
        }

        //// 获取商品
        $url = 'https://merchant.vova.com.hk/api/v1/product/productList';
        $post_data = array(
            'token' => $this->access_token,
            'conditions' => $conditions,
        );
        $post_data = json_encode($post_data);

        $call_comm = new \App\Libs\wrapper\Comm();// wish 类
        $curl_res = $call_comm->curls($url, $post_data);//返回json
        $curl_res = json_decode($curl_res, true);
        return $curl_res;
    }

    //更新spu与sku名称,更新sku价格,运费,库存
    public function updateGoodsData($token, $goodsInfo, $skuUpdateInfo)
    {
        $params = [
            'token' => $token,
        ];
        if (!empty($goodsInfo)) {
            //更新子sku价格,运费,库存
            $params['goods_info'][] = $goodsInfo;
        }

        if (!empty($skuUpdateInfo)) {
            $params['sku_update_info'][] = $skuUpdateInfo;
        }
        $url = 'https://merchant.vova.com.hk/api/v1/product/updateGoodsData';
        $call_comm = new \App\Libs\wrapper\Comm();
        $curl_res = $call_comm->curls($url, json_encode($params));//返回json
        return (array)json_decode($curl_res, true);
    }

    //更新spu 产品名 描述 分类
    public function updateGoodsByType($token, $type, $updateInfo)
    {
        $params = [
            'token' => $token,
            //要修改的商品类型，共有3个值，分别为goods_name(名称), goods_desc(描述), goods_category(分类)
            'type' => $type,
            'update_info' => [
                $updateInfo
            ]
        ];
        $url = 'https://merchant.vova.com.hk/api/v1/product/updateGoodsByType';
        $call_comm = new \App\Libs\wrapper\Comm();
        $curl_res = $call_comm->curls($url, json_encode($params));//返回json
        return json_decode($curl_res, true);
    }

    //下架产品
    public function disableSale($token, $platformProductId)
    {
        $params = [
            'token' => $token,
            'goods_list' => [
                $platformProductId
            ]
        ];
        $url = 'https://merchant.vova.com.hk/api/v1/product/disableSale';
        $call_comm = new \App\Libs\wrapper\Comm();
        $curl_res = $call_comm->curls($url, json_encode($params));//返回json
        return json_decode($curl_res, true);
    }

    //上架产品
    public function enableSale($token, $platformProductId)
    {
        $params = [
            'token' => $this->access_token,
            'goods_list' => [
                $platformProductId
            ]
        ];
        $url = 'https://merchant.vova.com.hk/api/v1/product/enableSale';
        $call_comm = new \App\Libs\wrapper\Comm();
        $curl_res = $call_comm->curls($url, json_encode($params));//返回json
        return json_decode($curl_res, true);
    }

    //添加sku
    public function addSku($accessToken, $spu, $goodsSku, $storage, $price, $shippingFee, $size, $color, $image){
        $proSku = [
            "token" => $accessToken,
            "items" => [
                [
                    'parent_sku'=> $spu,
                    'goods_sku'=> $goodsSku,
                    'storage'=> $storage,
                    'market_price'=> $price,
                    'shop_price'=> $price,
                    'shipping_fee'=> $shippingFee,
                    'style_array'=>[
                        'size'=>$size,
                        'color'=>$color,
                        'style_quantity'=>$color
                    ],
                    'sku_image'=> $image
                ]
            ]
        ];
        $postSku = json_encode($proSku,JSON_UNESCAPED_UNICODE);
        $call_comm = new \App\Libs\wrapper\Comm();
        $url = "https://merchant.vova.com.hk/api/v1/product/addProductSku";
        $curl_res = $call_comm->curls($url, $postSku);//返回json
        return json_decode($curl_res, true);
    }
}