<?php

namespace App\Libs\wrapper;

use Illuminate\Support\Facades\DB;

class Authorize extends Comm
{
    public function authorize_list($data)
    {
        $store_id = $data['store_id'];
        $sql = "select access_token,app_data from shop
                   where Id = {$store_id}";
        $list = json_decode(json_encode(db::select($sql)), true);
        $return['Redirect_URI'] = 'http://new.zity.cn/pc/authorize/authorize_code?store_id=' . $store_id;
        $return['list'] = $list;
        return $return;
    }


    //获取店铺授权跳转地址
    public function authorize_website($data)
    {
        //var_dump($data);die;
        $storeId = $data['store_id'];
        //var_dump($data['token']);die;
        //var_dump($appData);die;
        //$appData = json_decode($data['app_data'],true);
        $mark = $data['mark'];
        $sql = "SELECT * 
                    FROM shop s
                    inner join platform p on p.Id=s.platform_id
                    WHERE s.Id = '{$storeId}'";
        $shop = json_decode(json_encode(db::select($sql)), true);
        if (empty($shop)) {
            return '平台或店铺不存在';
        }
        if(isset($data['access_token'])){
            if ($mark==2){
                //vova平台
                $token = $data['access_token'];
                //var_dump($token);die;
                $sql = "update shop set access_token='{$token}',authorization_status=2 where Id={$storeId}";
                $res = db::update($sql);
                if($res!==false){
                    //echo "11";die;
                    return 1;
                }
            }
        }else{
            $appData = $data['app_data'];
            $update['app_data'] = json_encode($data['app_data']);
            $res = DB::table('shop')->where('Id', '=', $storeId)->update($update);
            if ($res !== false) {

                // var_dump($shop);die;
                if ($mark == 1) {
                    //wish平台
                    $website = 'https://merchant.wish.com/v3/oauth/authorize?client_id=';

                    //$appData = $shop[0]['app_data'];
                    $appArr = json_encode($appData);
                    $Client_ID = $appData['client_id'];
                    $Client_Secret = $appData['client_secret'];
                    if (!isset($Client_ID)) {
                        return 'Client_ID为空';
                    }
                    if (!isset($Client_Secret)) {
                        return 'Client_Secret为空';
                    }
                    $sql = "update shop
                            set app_data='{$appArr}'
                            where `Id`={$storeId}";
                    //var_dump($sql);die;
                    $res = db::update($sql);
                    if ($res !== false) {
                        $website .= $Client_ID;
                        $return['website'] = $website;
                        return $return['website'];
                    } else {
                        return '申请失败';
                    }
                    //echo "222";

                }elseif($mark == 3){
                    //Lazada平台
                    $website = 'https://auth.lazada.com/oauth/authorize?';
                    $appArr = json_encode($appData);
                    $app_key = $appData['app_key'];
                    $app_secret = $appData['app_secret'];
                    $redirect_url = 'https://new.zity.cn/pc/authorize/authorize_code?store_id='.$storeId;
                    if (!isset($app_key)) {
                        return 'app_key为空';
                    }
                    if (!isset($app_secret)) {
                        return 'app_secret';
                    }
                    $sql = "update shop
                            set app_data='{$appArr}'
                            where `Id`={$storeId}";
                    //var_dump($sql);die;
                    $res = db::update($sql);
                    if ($res !== false) {
                        $website = $website.'client_id='.$app_key.'&redirect_uri='.$redirect_url.'&response_type=code&force_auth=true';
                        $return['website'] = $website;
                        //var_dump($return);die;
                        return $return['website'];
                    } else {
                        return '申请失败';
                    }


                }else {
                    return '暂未接入授权';
                }
            } else {
                return '请重新填写';
            }
        }

    }

    //店铺授权回调信息接收
    public function authorize_code($data)
    {
        //var_dump($data);die;
        $storeId = $data['store_id'];

        $sql = "select s.app_data,p.mark
                    from shop s
                    inner join platform p on p.Id=s.platform_id
                    where s.Id={$storeId}";
        $store = json_decode(json_encode(db::select($sql)), true);
        $app_data = json_decode($store[0]['app_data'], true);
        //var_dump($app_data);die;

        /* if(isset($data['data'])){
             $access_token = $data['access_token'];
             $refresh_token = $data['refresh_token'];
             $app_data['refresh_token'] = $refresh_token;*/
        /*echo "<pre>";
        var_dump($app_data);die;*/
        /* $sql = "update shop
                 set access_token={$access_token},app_data={$app_data},authorization_status=2
                 where `Id`={$storeId}";
         $res = $res = db::update($sql);
         return '获取成功';
     }else*/
        if (isset($data['code'])) {

            if ($store[0]['mark'] == 1) {

                $client_id = $app_data['client_id'];
                $client_secret = $app_data['client_secret'];
                $code = $data['code'];
                $grant_type = 'authorization_code';
                $redirect_uri = 'http://new.zity.cn/pc/authorize/authorize_code?store_id=' . $storeId;
                $http = 'https://merchant.wish.com/api/v3/oauth/access_token?client_id=' . $client_id . '&client_secret=' . $client_secret . '&code=' . $code . '&grant_type=' . $grant_type . '&redirect_uri=' . $redirect_uri;
                // 初始化一个 cURL 对象
                $curl = curl_init();
                // 设置你需要抓取的URL
                curl_setopt($curl, CURLOPT_URL, $http);
                // 设置header 响应头是否输出
                curl_setopt($curl, CURLOPT_HEADER, 0);
                // 设置cURL 参数，要求结果保存到字符串中还是输出到屏幕上。
                // 1如果成功只将结果返回，不自动输出任何内容。如果失败返回FALSE
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                // 运行cURL，请求网页
                $data = curl_exec($curl);
                // 关闭URL请求
                curl_close($curl);
                //echo "<pre>";
                $getData = json_decode($data, true);
                //var_dump($getData);
                // die;
                if (isset($getData['data'])) {
                    $access_token = $getData['data']['access_token'];
                    $refresh_token = $getData['data']['refresh_token'];
                    $app_data['refresh_token'] = $refresh_token;
                    /*echo "<pre>";
                    var_dump($app_data);die;*/
                    $appJson = json_encode($app_data);//var_dump($appJson);die;
                    $sql = "update shop
                    set access_token='{$access_token}',app_data='{$appJson}',authorization_status=2
                    where `Id`={$storeId}";
                    //var_dump($sql);die;
                    $res = db::update($sql);
                    //var_dump($res);die;
                    if($res){
                        //echo "11";die;
                        return 1;
                    }else{
                        return 2;
                    }
                }


            }elseif($store[0]['mark'] == 3){
                // var_dump('123');exit;
                $code = $data['code'];
                $app_key = $app_data['app_key'];
                $app_secret = $app_data['app_secret'];
                $timestamp = time();
                /*$url = 'https://api.lazada.com/rest/auth/token/create';*/
                $url = 'https://auth.lazada.com/rest';
                $urls = "../app/Libs/lazop/php/Autoloader.php";
                include_once($urls);
                $lazada = new \LazopClient($url, $app_key, $app_secret);
                $request = new \LazopRequest('/auth/token/create');
                $request->addApiParam('code',$code);
                //$request->addApiParam('uuid','38284839234');
                $resData = $lazada->execute($request);//die;
                $resData = json_decode($resData,true);
                //var_dump($resData);die;
                $access_token = $resData['access_token'];

                $appJson = json_encode($app_data);//var_dump($appJson);die;
                $sql = "update shop
                    set access_token='{$access_token}',app_data='{$appJson}',authorization_status=2
                    where `Id`={$storeId}";
                //var_dump($sql);die;
                $res = db::update($sql);
                //var_dump($res);die;
                if($res){
                    //echo "11";die;
                    return 1;
                }else{
                    return 2;
                }
                //var_dump($access_token);
                //die;
            }


        } else {
            return 2;

        }


    }


    public function authorize_status($data){
        $storeId = $data['store_id'];
        $status = DB::table('shop')->where('Id','=',$storeId)->value('authorization_status');
        $return['status'] = $status;
        return $return;
    }

    public function authorize_test($data){
        $url = "D:\phpstudy_pro\WWW\zity\zity\app\Libs\lazop\php\Autoloader.php";
        include_once($url);


        $app_key = 123;
        $app_secret = 456;
        $url = '/auth/token/create';

        $c = new \LazopClient($url, $app_key, $app_secret);
        var_dump($c);exit;






//        $lazada = new \App\Libs\lazop\php\lazop\php\lazop\LazopClient($url,$app_key,$app_secret);




        echo "11";die;


    }


}