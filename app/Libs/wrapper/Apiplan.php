<?php

namespace App\Libs\wrapper;

use App\Models\SaiheWarehouse;
use App\Models\Shop;
use Illuminate\Support\Facades\DB;
use function GuzzleHttp\Psr7\str;

class Apiplan extends Comm
{

    /**
     * 获取计划任务的店铺---亚马逊
     * @table shop
     * @param platform_id 平台（shopee平台==7）
     * @param state 店铺状态
     */
    public function plan_shop()
    {
        $sql = "select Id,shop_name from shop where platform_id in(5) and state=1";
        $shop = json_decode(json_encode(db::select($sql)), true);
        $return['list'] = $shop;
        return $return;
    }

    /**
     * 处理数据
     * shop 店铺   warehouse 仓库
     */
    public function deal($apiPlanList, $append = array())
    {
        $arr = array('AmazonListOrders' => '亚马逊采集订单列表','AmazonListOrderItems' => '亚马逊采集订单详情'
        , 'SaiheGetProduct' => '塞盒采集产品'
        , 'SaiheGetProductDetail' => '塞盒采集产品详情'
        , 'SaiheGetProductInventory' => '塞盒采集库存');


        foreach ($apiPlanList as $item) {
            if (isset($arr[$item['api_name']])) {
                $item['api_desc'] = $arr[$item['api_name']];
            } else {
                $item['api_desc'] = '';
            }
            if ($item['type'] == 1) {
                $item['type_show'] = '完全采集';
            } elseif ($item['type'] == 2) {
                $item['type_show'] = '时间段采集';
            } else {
                $item['type_show'] = $item['type'];
            }
            if ($item['is_finish'] == 1) {
                $item['finish_show'] = '是';
            } else {
                $item['finish_show'] = '否';
            }
            if ($item['error'] == 1) {
                $item['error_show'] = '是';
            } else {
                $item['error_show'] = '否';
            }
            if ($item['param_start_time'] > 0) {
                $item['param_start_time_show'] = date("Y-m-d", $item['param_start_time']);
            } else {
                $item['param_start_time_show'] = '';
            }
            if ($item['param_end_time'] > 0) {
                $item['param_end_time_show'] = date("Y-m-d H:i:s", $item['param_end_time']);
            } else {
                $item['param_end_time_show'] = '';
            }

            if (in_array('shop', $append)) {
                if ($item['shop_id'] > 0) {
                    $shop = Shop::where('id', $item['shop_id'])->first();
                    if (isset($shop['shop_name'])) {
                        $item['shop_name'] = $shop['shop_name'];
                    } else {
                        $item['shop_name'] = '';
                    }
                } else {
                    $item['shop_name'] = '';
                }
            }

            if ($item['type'] == 2) {
                $item['edit'] = 1;
            } else {
                $item['edit'] = 0;
            }

            if (in_array('warehouse', $append)) {
                if ($item['warehouse_id'] > 0) {
                    $SaiheWarehouse = SaiheWarehouse::where('id', $item['warehouse_id'])->first();
                    if (isset($SaiheWarehouse['name'])) {
                        $item['warehouse_name'] = $SaiheWarehouse['id'] . " " . $SaiheWarehouse['name'];
                    } else {
                        $item['warehouse_name'] = $SaiheWarehouse['id'];
                    }
                } else {
                    $item['warehouse_name'] = $item['warehouse_id'];
                }
            }
        }
    }

    /**
     * @param $ApiPlanModel  模型
     * @param $param         post,get提交的参数
     */
    public function pageSearch($ApiPlanModel, $param)
    {
        if (isset($param['shop_id']) && $param['shop_id'] > 0) {
            $ApiPlanModel = $ApiPlanModel->where('shop_id', $param['shop_id']);
        }
        if (isset($param['api_name']) && $param['api_name']!='') {
            $ApiPlanModel = $ApiPlanModel->where('api_name', $param['api_name']);
        }
        if (isset($param['warehouse_id']) && $param['warehouse_id'] > 1) {
            $ApiPlanModel = $ApiPlanModel->where('warehouse_id', $param['warehouse_id']);
        }
        if (isset($param['type']) && $param['type'] > 0) {
            $ApiPlanModel = $ApiPlanModel->where('type', $param['type']);
        }
        if (isset($param['param_start_time']) && $param['param_start_time'] != '') {
            $startTime = strtotime($param['param_start_time']);
            $ApiPlanModel = $ApiPlanModel->where('param_start_time', '>', $param['param_start_time']);
        }
        if (isset($param['param_end_time']) && $param['param_end_time'] != '') {
            $endTime = strtotime($param['param_end_time']);
            $ApiPlanModel = $ApiPlanModel->where('param_end_time', '>', $param['param_end_time']);
        }
        return $ApiPlanModel;
    }


}
