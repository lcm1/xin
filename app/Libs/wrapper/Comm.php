<?php
namespace App\Libs\wrapper;
use Illuminate\Support\Facades\DB;
class Comm{
////////////////////////////////////////////////  公共
	/**
	 * curl请求
	 * @param  string $url [请求的URL地址]
	 * @param  string $post [请求的参数]
	 * @return  string
	 */
	public function curls($url, $post=false, $head=''){
		$curl = curl_init(); // 启动一个CURL会话
		curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址
		
		// 优化curl 速度
		curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0); //强制协议为1.0
		curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);//强制使用IPV4协议解析域名
		// 头部
		if(!empty($head)){
//			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Expect:')); //头部要送出'Expect: '
			curl_setopt($curl, CURLOPT_HTTPHEADER, $head); // 请求头部
		}
		
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2); // 从证书中检查SSL加密算法是否存在
//		curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
		curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
		if(!empty($post)){
			curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
			curl_setopt($curl, CURLOPT_POSTFIELDS, $post); // Post提交的数据包
		}
		curl_setopt($curl, CURLOPT_TIMEOUT, 120); // 设置超时限制防止死循环
		curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
		$res = curl_exec($curl); // 执行操作
		
		if (curl_errno($curl)) {
			echo 'Errno'.curl_error($curl);//捕抓异常
		}
		curl_close($curl); // 关闭CURL会话
		return $res; // 返回数据，json格式
	}
	
	/**
	 * 毫秒转日期
	 */
	public function time_hm($msectime)
	{
		$msectime = $msectime * 0.001;
		if(strstr($msectime,'.')){
			sprintf("%01.3f",$msectime);
			list($usec, $sec) = explode(".",$msectime);
			$sec = str_pad($sec,3,"0",STR_PAD_RIGHT);
		}else{
			$usec = $msectime;
			$sec = "000";
		}
		$date = date("Y-m-d H:i:s",$usec);
//		$mescdate = str_replace('x', $sec, $date);
		return $date;
	}
	
	/**
	 * @desc 检测数据类型是否正确
	 * @param $data
	 * @param $key
	 * @param $type
	 * @return bool|string
	 */
	public function datatype($data,$key,$type){
		
		//判断$data,$key是不是数组，是否为空
		if(empty($data) OR empty($key)){
			return '$data或$key为空';
		}else if(is_array($data)==false OR is_array($key)==false OR is_array($type)==false){
			return '$data或$key或$type不是数组';
		}
		//$key长度
		$num=count($key);
		for($i=0;$i<$num;$i++){
			//判断是否为空
			if(empty($data[$key[$i]])){
				return "$key[$i]为空";
			}
			//判断是不是字符串:string
			if($type[$i]=='string'){
				if(is_string($data[$key[$i]])==false){
					return "$key[$i]不是字符串";
				}
			}
			//判断是不是数字或数字字符串
			if($type[$i]=='numeric'){
				if(is_numeric($data[$key[$i]])==false){
					return "$key[$i]不是数字或数字字符串";
				}
			}
			//正则邮箱：email
			if($type[$i]=='email'){
				$preg_email='/^[a-zA-Z0-9]+([-_.][a-zA-Z0-9]+)*@([a-zA-Z0-9]+[-.])+([a-z]{2,5})$/ims';
				if(!preg_match($preg_email,$data[$key[$i]])){
					return "$key[$i]不是邮箱";
				}
			}
			//正则手机号：phone
			if($type[$i]=='phone'){
				$preg_phone='/^1[34578]\d{9}$/ims';
				if(!preg_match($preg_phone,$data[$key[$i]])){
					return "$key[$i]不是手机号";
				}
			}
			//验证身份证号码：idcard
			if($type[$i]=='idcard'){
				$preg_card='/^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/';
				if(!preg_match($preg_card,$data[$key[$i]])){
					return "$key[$i]不是身份证号码";
				}
			}
			//验证银行卡号
			if($type[$i]=='bankcard'){
				$preg_bankcard='/^(\d{15}|\d{16}|\d{19})$/isu';
				if(!preg_match($preg_bankcard,$data[$key[$i]])){
					return "$key[$i]不是银行卡号";
				}
			}
			//验证QQ号码
			if($type[$i]=='qq'){
				$preg_QQ='/^\d{5,12}$/isu';
				if(!preg_match($preg_QQ,$data[$key[$i]])){
					return "$key[$i]不是QQ号码";
				}
			}
			//验证微信号
			if($type[$i]=='wx'){
				$preg_wechat='/^[_a-zA-Z0-9]{5,19}+$/isu';
				if(!preg_match($preg_wechat,$data[$key[$i]])){
					return "$key[$i]不是微信号";
				}
			}
			//验证特殊符号(如需要验证其他字符，自行转义 "\X" 添加)
			if($type[$i]=='spacial'){
				$preg_spacial="/\/|\~|\!|\@|\#|\\$|\%|\^|\&|\*|\(|\)|\+|\{|\}|\:|\<|\>|\?|\[|\]|\,|\/|\;|\\' | \`|\-|\=|\\\|\|/isu";
				if(preg_match($preg_spacial,$data[$key[$i]])){
					return "$key[$i]特殊符号";
				}
			}
		}
		return true;
		
	}
	
	/**
	 * 云片网短信验证
	 * 接收值：apikey:用户唯一标识，在管理控制台获取,mobile:手机号,text:已审核短信模板,如【小熊平台】您的验证码是".$code
	 *
	 * 1.添加签名,例如：'小熊平台','小熊店铺'
	 * 2.添加模板,例如：您的验证码是#code#
	 *
	 *  $data['apikey']='10ab3ae5fdd158e0130c265aae8bc23e';
	$data['mobile']='18205981034';
	$data['text']="【小熊平台】您的验证码是{$code}";
	 **/
	public function yunpian($data){
		$apikey = '041e0fcf92c26e981f51698544997604';
		$url = 'https://sms.yunpian.com/v2/sms/single_send.json';
		
		$data = array(
			'apikey'=>$apikey,
			'text'=>$data['text'],
			'mobile'=>$data['phone']
		);
		
		//创建新会话
		$ch = curl_init();	//创建会话
		/* 设置返回结果为流 */
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		/* 设置超时时间*/
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		/* 设置通信方式 */
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);		//参数跟随重定向
		curl_setopt ($ch, CURLOPT_URL,$url);		//设置访问路径
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);		//将内容作为变量储存
		//将返回值赋值给$json_data
		$json_data = curl_exec($ch);
		//关闭当前会话
		curl_close($ch);
		//解析返回结果（json格式字符串）
		$array = json_decode($json_data,true);
		return $array;
	}
	
	
	/**
	 * @desc 计算时间
	 * @param $the_time
	 * @return string
	 */
	function time_tran($the_time){
		$now_time = time();
		if($now_time<$the_time){
			$dur = $the_time-$now_time;
			if($dur < 60){
				return '剩余'.floor($dur / 1).'秒';
			}else {
				if ($dur < 3600) {
					return '剩余'.floor($dur / 60) . '分钟';
				} else {
					if ($dur < 86400) {
						return '剩余'.floor($dur / 3600) . '小时';
					} else {
						if ($dur < 2592000) {
							return '剩余'.floor($dur / 86400) . '天';
						}else{
							if($dur < 31536000){
								return '剩余'.floor($dur / 2592000) . '个月';
							}else{
								return '剩余'.floor($dur / 31536000) . '年';
							}
						}
					}
				}
			}
		}else{
			$dur = $now_time - $the_time;
			if($dur < 60){
				return floor($dur / 1).'秒前';
			}else {
				if ($dur < 3600) {
					return floor($dur / 60) . '分钟前';
				} else {
					if ($dur < 86400) {
						return floor($dur / 3600) . '小时前';
					} else {
						if ($dur < 2592000) {
							return floor($dur / 86400) . '天前';
						}else{
							if($dur < 31536000){
								return floor($dur / 2592000) . '个月前';
							}else{
								return floor($dur / 31536000) . '年前';
							}
						}
					}
				}
			}
		}
		
	}
	
	/**
	 * 生成Token
	 */
	public function generate_token(){
		mt_srand((double)microtime()*10000);
		$charid = strtoupper(md5(uniqid(rand(), true)));
		$hyphen = chr(45);
		$Token   = substr($charid, 0, 8)
			.substr($charid, 8, 4)
			.substr($charid,12, 4)
			.substr($charid,16, 4)
			.substr($charid,20,12);
		return $Token;
	}
	
	/**
	 * @desc 获取时间字符串(yyyyMMddHHmmssSSS)
	 * @return string
	 */
	public function getOrderTime(){
		$microtime = explode('.',microtime(true))[1];
		if(strlen($microtime)==3){
			$microtime = $microtime.'0';
		}
		$sn = date('YmdHis',time()).$microtime;
		return $sn;
		
	}
	
	
	
}//