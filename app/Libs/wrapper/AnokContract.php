<?php

namespace App\Libs\wrapper;

use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Collection;

class AnokContract extends Comm
{
    /**
     * 获取表格数据
     * @return array
     */
    public static function index()
    {
        $page = request()->post('page', 1);
        $page_size = request()->post('page_size', 10);
        $start_date = request()->post('start_time', '');
        $end_date = request()->post('end_time', '');
        $billno = request()->post('billno', '');
        $goods_no = request()->post('goods_no', '');
        $color_name = request()->post('color_name', '');
        $input_userid = request()->post('input_userid', '');

        $res = DB::table('anok_contract_clothing as acc')
            ->select(
                [
                    'acc.id', 'acc.anok_id', 'u.account', 'acc.input_date', 'acc.billno', 'acc.order_type', 'acc.goods_no', 'acc.cnname', 'acc.size_num',
                    'acc.goods_amount', 'acc.price', 'acc.color_name', 'acc.color_code', 'acc.XXS', 'acc.XS', 'acc.S', 'acc.M', 'acc.L', 'acc.XL', 'acc.2XL',
                    'acc.3XL', 'acc.4XL', 'acc.5XL', 'acc.6XL', 'acc.7XL', 'acc.8XL'
                ])
            ->leftJoin('users as u', 'u.id', '=', 'acc.input_userid')
            ->where(function ($query) use ($start_date, $end_date) {
                if (!empty($start_date) && !empty($end_date))
                    return $query->whereBetween('input_date', [$start_date, $end_date]);
            })
            ->where(function ($query) use ($input_userid) {
                if ($input_userid) return $query->where('acc.input_userid', '=', $input_userid);
            })
            ->where(function ($query) use ($billno) {
                if ($billno) return $query->where('acc.billno', 'like', '%' . $billno . '%');
            })
            ->where(function ($query) use ($goods_no) {
                if ($goods_no) return $query->where('acc.goods_no', 'like', '%' . $goods_no . '%');
            })
            ->where(function ($query) use ($color_name) {
                if ($color_name) return $query->where('acc.color_name', 'like', '%' . $color_name . '%');
            })
            ->forPage($page, $page_size)
            ->paginate($page_size);
        $count = $res->total();
        return [$res->items(), $count];
    }
}