<?php
namespace App\Libs\wrapper;
use Illuminate\Support\Facades\DB;
require (app_path().'/Libs/Qiniu/autoload.php');

//require_once(app_path()."/Libs/Qiniu/rs.php");

class Qiniu{
/////////////////////////////////////////////// 七牛云存储
	
	var $accessKey = 'JXdz_T6uDQHUS2jSjRyjy9O31Sj6qfAvB_WSf8rG';//n0x081pedncMA4mbVYBeDHasGc_AJ3mF7woZ5A4q
	var $secretKey = '9XygLKrd3-i8kpflo7jUTAZ8kzXBftpCOaGvPG12';//Bt0RCEyCfiliZFvfFcFy9y3yySHScwqmA6OQYOqg
	var $bucket = 'zity';//存储空间
	var $visit = 'http://q.zity.cn/';//访问网址
	
	/*
	var $accessKey = 'n0x081pedncMA4mbVYBeDHasGc_AJ3mF7woZ5A4q';
	var $secretKey = 'Bt0RCEyCfiliZFvfFcFy9y3yySHScwqmA6OQYOqg';
	var $bucket = 'onedog-xiong';//存储空间
	var $visit = 'http://www.xiaoxionga.cn/';//访问网址
	*/
	
	/**
	 * 添加图片(七牛云)
	 * 说明：可添加多张图片
	 * 逻辑：检验接收值是否为空 --> 添加图片
	 * 		检验接收值是否为空 --> 循环添加图片,判断上传图片成功数和$data数量是否相等,相等返回$keylist(数组):添加成功的key,不想等返回false:失败
	 * 接收值：$data(数组)：图片流
	 * 返回值：$keylist(数组):添加成功的key,提示语句
	 * */
	public function file_add($data=array()){
		// 需要填写你的 Access Key 和 Secret Key
		$accessKey = $this->accessKey;
		$secretKey = $this->secretKey;
		// 要上传的空间
		$bucket = $this->bucket;
		//访问网址
		$visit = $this->visit;
		
		// 构建鉴权对象,初始化AccessKey和SecretKey       位置src/Qiniu/Auth.php
		$auth = new \Qiniu\Auth($accessKey, $secretKey);
		// 初始化 UploadManager 对象并进行文件的上传。
		$uploadMgr = new \Qiniu\Storage\UploadManager();
		$keylist=array();
		foreach ($data as $k => $v){
			if(!empty($v)){
				//文件类型
				$type = explode('/', $v['type']);
				if($type[0] == 'image'){
					//图片
					// 上传到七牛后保存的文件名
					$name = explode('.', $v['name']);
					$random = $this->generate_token();
					$key = date('Ymd', time()) .'_'. $random .'.'. $name[(count($name)-1)];//文件名称
				}else{
					//非图片
					// 上传到七牛后保存的文件名
					$name = explode('.', $v['name']);
					$extension = '.'. $name[(count($name)-1)]; //扩展名
					$filename = str_replace($extension, "", $v['name']);//去除 扩展名
					$filename_num = mb_strlen($filename, "utf-8");//文件名称长度
					if($filename_num > 100){
						$filename = substr($filename , 0 , 100);//字符串截取 ;
					}
					$key = $filename."_".time() .$extension;//重命名
				}
				
				// 要上传文件的本地路径
				$filePath = $v['tmp_name'];
				// 生成上传 Token
				$token = $auth->uploadToken($bucket,$key);
				$classdata = $uploadMgr->putFile($token, $key, $filePath);	//上传凭证,上传文件名,上传文件的路径
				if(!empty($classdata[0]['key'])){
					$keylist[] = $visit . $classdata[0]['key'];
				}
			}
		}
		
		if(count($keylist) == count($data)){
			return $keylist;
		}else{
			return '上传失败';
		}
		
	}
	
	/**
	 * 添加图片(七牛云) -- 用于Excel导入
	 * 说明：可添加多张图片
	 * */
	public function file_add_import($data){
		// 需要填写你的 Access Key 和 Secret Key
		$accessKey = $this->accessKey;
		$secretKey = $this->secretKey;
		// 要上传的空间
		$bucket = $this->bucket;
		//访问网址
		$visit = $this->visit;
		
		// 构建鉴权对象,初始化AccessKey和SecretKey       位置src/Qiniu/Auth.php
		$auth = new \Qiniu\Auth($accessKey, $secretKey);
		// 初始化 UploadManager 对象并进行文件的上传。
		$uploadMgr = new \Qiniu\Storage\UploadManager();
		
		$keylist=array();
		foreach ($data as $k => $v){
			if(!empty($v)){
				$type = explode('.', $v);//文件类型
				$type_arr = array('png', 'jpg', 'jpeg', 'gif');
				if(in_array($type[1], $type_arr)){
					$random = $this->generate_token();
					$key = date('Ymd', time()) .'_'. $random .'.'. $type[1];//文件名称
				}
				// 要上传文件的本地路径
				$filePath = $v;
				// 生成上传 Token
				$token = $auth->uploadToken($bucket,$key);
				$classdata = $uploadMgr->putFile($token, $key, $filePath);	//上传凭证,上传文件名,上传文件的路径
				if(!empty($classdata[0]['key'])){
					$keylist[$k] = $visit . $classdata[0]['key'];
				}
			}
		}
		
		if(count($keylist) == count($data)){
			return $keylist;
		}else{
			return '上传失败';
		}
		
	}
	
	/**
	 * 下载图片(七牛云)
	 * */
	public function file_download($data){
		//判断文件夹是否已存在
		$wenjian = 'assess/test';
		if (!is_dir($wenjian)) {
			mkdir($wenjian, 0777, true);
		}
		
		$file_n =  $_SERVER['DOCUMENT_ROOT'] .'/assess/images/content/2020-02-20/1582183984_71000.jpg';
		$res = unlink($file_n);
		return $res;
		
		
		
		//七牛云图片下载到本地
		$url = "http://q5toqx79j.bkt.clouddn.com/4548354d9d87171d71a96f17a2c08de.png";
		//return $url;
		ob_start();  //打开缓冲区
		readfile($url);  //该函数读入一个文件并写入到输出缓冲
		$img  = ob_get_contents();  //返回内部缓冲区的内容
		ob_end_clean();  //删除内部缓冲区的内容，并且关闭内部缓冲区,这个函数不会输出内部缓冲区的内容而是把它删除
		$size = strlen($img);	//函数返回字符串的长度
		$filename = time().'_'.(rand(100,999)*rand(1,999));
		$file_n =  $_SERVER['DOCUMENT_ROOT'] .'/assess/test/'.$filename.'.jpg';;
		$fp = fopen($file_n, 'w+');//打开文件 一定要添相对路径
		fwrite($fp, $img); //写入信息到
		fclose($fp);//关闭文件
		
	}
	
	
	/**
	 * 删除图片(七牛云)
	 * 说明：可删除多张图片
	 * 逻辑：检验接收值是否为空 --> 删除图片
	 * 		检验接收值是否为空 --> 循环删除图片,判断删除图片成功数和$data数量是否相等,相等返回$keylist(数组):删除成功的key,不相等返回false:失败
	 * 接收值：$data(字符串)：图片字符串，如(150993468854562669350.jpeg,150993468925260313171.jpeg)
	 * 返回值：$keylist(数组):删除成功的key,提示语句
	 * */
	public function file_del($data){
		// 需要填写你的 Access Key 和 Secret Key
		$accessKey = $this->accessKey;	
		$secretKey = $this->secretKey;
		//你要测试的空间， 并且这个key在你空间中存在
		$bucket = $this->bucket;
		// 构建鉴权对象,初始化AccessKey和SecretKey    src/Qiniu/Auth.php
		$auth = new \Qiniu\Auth($accessKey, $secretKey);
		//初始化BucketManager
		$bucketMgr = new \Qiniu\Storage\BucketManager($auth);
		
		//临时数组,存放key
		$keylist=array();
		foreach ($data as $k=>$v){
			if(!empty($v)){
				$err = $bucketMgr->delete($bucket, $v);
				if($err === NULL) {
					$keylist[] = $v;
				}
			}
			
		}
		if(count($keylist)>0){
			return $keylist;
		}else{
			return '删除文件失败';
		}
		
	}
	
	/**
	 * 获取七牛云上传 Token
	 * 说明：用于前端上传图片，将Token返回给前端
	 * */
	public function upload_token(){
		$accessKey = $this->accessKey;
		$secretKey = $this->secretKey;	
		// 构建鉴权对象,初始化AccessKey和SecretKey
		$auth = new \Qiniu\Auth($accessKey, $secretKey);	//	src/Qiniu/Auth.php
		// 要上传的空间
		$bucket = $this->bucket;
		// 上传到七牛后保存的文件名
//		$key = time().'_'.rand(0,9).'.jpeg';
		$key = time().'_'.rand(0,9).'.mp4';
		// 生成上传 Token
		$token = $auth->uploadToken($bucket);
//		$token = $auth->uploadToken($bucket,$key);
//		return $token;
		$return['token']=$token;
//		$retu['key']=$key;
		return $return;
	}
	
	/**
	 * 上传录音
	 * 接收值：accessKey,secretKey,bucket:要上传的空间,filePath:要上传文件的本地路径,key:上传到七牛后保存的文件名
	 * 返回值：
	 */
	public function UploadChange($data){
		Vendor('QiNiuYun.autoload');
 		Vendor('QiNiuYun.src.Qiniu.Auth');
		Vendor('QiNiuYun.src.Qiniu.Storage.UploadManager');//上传文件
		Vendor('QiNiuYun.src.Qiniu.Storage.BucketManager');//删除文件
		
		// 需要填写你的 Access Key 和 Secret Key
		$accessKey = $data['accessKey'];	//Access_Key
		$secretKey = $data['secretKey'];	//Secret_Key
		// 构建鉴权对象,初始化AccessKey和SecretKey
		$auth = new \Qiniu\Auth($accessKey, $secretKey);	//	src/Qiniu/Auth.php
		//$auth = new Auth($accessKey, $secretKey);	//	src/Qiniu/Auth.php
		$bucket = $data['bucket'];
		
		//数据处理队列名称,不设置代表不使用私有队列，使用公有队列。  
		$pipeline = 'wx_voice';	//队列名
			  
		//通过添加'|saveas'参数，指定处理后的文件保存的bucket和key  
		//不指定默认保存在当前空间，bucket为目标空间，后一个参数为转码之后文件名   
		//return $data['key'].'.mp3';
		$savekey =  \Qiniu\base64_urlSafeEncode($bucket.':'.$data['key'].'.mp3');  
		//设置转码参数  
		$fops = "avthumb/mp3/ab/320k/ar/44100/acodec/libmp3lame";  
		$fops = $fops.'|saveas/'.$savekey;  
		if(!empty($pipeline)){  //使用私有队列  
			$policy = array(  
				'persistentOps' => $fops,  
				'persistentPipeline' => $pipeline  
			);  
		}else{                  //使用公有队列  
			$policy = array(  
				'persistentOps' => $fops  
			);  
		}  
		
		//指定上传转码命令  
		$uptoken = $auth->uploadToken($bucket, null, 3600, $policy);  
		
		$key = $data['key'].'.amr'; //七牛云中保存的amr文件名  
		//$uploadMgr = new UploadManager();  
		$uploadMgr = new \Qiniu\Storage\UploadManager();
			 
		//上传文件并转码$filePath为本地文件路径  
		list($ret, $err) = $uploadMgr->putFile($uptoken, $key, $data['filePath']);
		if ($err !== null) {
			return($err);
		} else {
			//为节省空间,删除amr格式文件  
			$bucketMgr = new \Qiniu\Storage\BucketManager($auth);
			$bucketMgr->delete($bucket, $key);  
			return($ret);
		}
		
//		if ($err !== null) {  
// 			        return false;  
// 			    }else {  
// 			        //此时七牛云中同一段音频文件有amr和MP3两个格式的两个文件同时存在  
// 			        // $bucketMgr = new BucketManager($auth);  
// 					$bucketMgr = new \Qiniu\Storage\BucketManager($auth);
// 			        //为节省空间,删除amr格式文件  
// 			        $bucketMgr->delete($bucket, $key);  
// 			        return $ret['key'];  
// 			    }  
	}
	
	
	
	/**
	 * 上传录音
	 * 接收值：accessKey,secretKey,bucket:要上传的空间,filePath:要上传文件的本地路径,key:上传到七牛后保存的文件名
	 * 返回值：
	 */
	public function UploadChange01($data){
		//return 'Qiniu:调用成功';
		
		//检验接收值
		if(empty($data)){
			return 'Qiniu:接收值为空';
		}else if(is_array($data)){
			//if
			
			//键值数组
			$key=array("accessKey","secretKey","bucket","filePath","key");
			//数据对应类型
			$type=array("string","string","string","string","string");
			$types=new \MyClass\CommSys();
			$typedata=$types->datatype($data,$key,$type);
			//return $typedata;
			if($typedata!==true){
				return $typedata;
			}else if($typedata===true){
				//if
				
				// 需要填写你的 Access Key 和 Secret Key
				$accessKey = $data['accessKey'];	//Access_Key
				$secretKey = $data['secretKey'];	//Secret_Key
				// 构建鉴权对象,初始化AccessKey和SecretKey
				$auth = new Auth($accessKey, $secretKey);	//	src/Qiniu/Auth.php
			    $bucket = $data['bucket'];
			    
			    //数据处理队列名称,不设置代表不使用私有队列，使用公有队列。  
				$pipeline = 'arsenal_test';	//队列名
			          
			    //通过添加'|saveas'参数，指定处理后的文件保存的bucket和key  
			    //不指定默认保存在当前空间，bucket为目标空间，后一个参数为转码之后文件名   
			    $savekey = Qiniu\base64_urlSafeEncode($bucket.':'.$data['key'].'.mp3');  
			    //设置转码参数  
			    $fops = "avthumb/mp3/ab/320k/ar/44100/acodec/libmp3lame";  
			    $fops = $fops.'|saveas/'.$savekey;  
			    if(!empty($pipeline)){  //使用私有队列  
			        $policy = array(  
			            'persistentOps' => $fops,  
			            'persistentPipeline' => $pipeline  
			        );  
			    }else{                  //使用公有队列  
			        $policy = array(  
			            'persistentOps' => $fops  
			        );  
			    }  
				
			    //指定上传转码命令  
			    $uptoken = $auth->uploadToken($bucket, null, 3600, $policy);  
			    
			   	$key = $data['key'].'.amr'; //七牛云中保存的amr文件名  
			    $uploadMgr = new UploadManager();  
			         
			    //上传文件并转码$filePath为本地文件路径  
			    list($ret, $err) = $uploadMgr->putFile($uptoken, $key, $data['filePath']);
			    if ($err !== null) {
				    return($err);
				} else {
				    return($ret);
				}
//			    if ($err !== null) {  
//			        return false;  
//			    }else {  
//			        //此时七牛云中同一段音频文件有amr和MP3两个格式的两个文件同时存在  
//			        $bucketMgr = new BucketManager($auth);  
//			        //为节省空间,删除amr格式文件  
//			        $bucketMgr->delete($bucket, $key);  
//			        return $ret['key'];  
//			    }  
				
				exit;
				
			}	
		}
		
		
	}
	
	/**
	 * 生成随机字符串
	 */
	public function generate_token(){
		mt_srand((double)microtime()*10000);
		$charid = strtoupper(md5(uniqid(rand(), true)));
		$hyphen = chr(45);
		$Token   = substr($charid, 0, 8)
			.substr($charid, 8, 4)
			.substr($charid,12, 4)
			.substr($charid,16, 4)
			.substr($charid,20,12);
		return $Token;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
?>