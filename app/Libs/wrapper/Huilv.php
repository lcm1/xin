<?php

namespace App\Libs\wrapper;

use Illuminate\Support\Facades\DB;

class Huilv extends Comm
{

    /**
     * 汇率列表--数据
     * @param name 名称
     * @param name_yw 英文名
     */
    public function huilv_list($data)
    {
        $where = array();
        // name 名称
        if (!empty($data['currency'])) {
            $where[] = "  currency='{$data['currency']}'";
        }
        // name_yw 英文名
        if (!empty($data['add_date'])) {
            $where[] = "  add_date='{$data['add_date']}'";
        }

        //分页
        $limit = "";
        if ((!empty($data['page'])) and (!empty($data['page_count']))) {
            $limit = " limit " . ($data['page'] - 1) * $data['page_count'] . ",{$data['page_count']}";
        }

        if (!empty($where)) {
            $sql = "select SQL_CALC_FOUND_ROWS *
                    from huilv 
                    where " . implode(' and ', $where) . "
                    order by id desc {$limit}";
        } else {
            $sql = "select SQL_CALC_FOUND_ROWS *
                    from huilv 
                    order by id desc {$limit}";
        }

        $list = json_decode(json_encode(db::select($sql)), true);

        //// 总条数
        if (isset($data['page']) and isset($data['page_count'])) {
            $total = db::select("SELECT FOUND_ROWS() as total");
            $return['total'] = $total[0]->total;
        }

        foreach ($list as &$item) {
            $item['add_date_time'] = date('Y-m-d H:i:s', $item['add_time']);
            if ($item['stat'] == 1) {
                $item['stat_show'] = '启用';
            } else {
                $item['stat_show'] = '禁用';
            }
        }
        $return['list'] = $list;
        return $return;
    }
}