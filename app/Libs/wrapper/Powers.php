<?php
namespace App\Libs\wrapper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class Powers extends Comm{
//////////////////////////////////////////////// 权限
////////////////////// 权限类别
	/**
	 * 新增权限类别
	 * @parma name 权限类别名称
	 */
	public function powers_type_add($data){
		//验证权限分类是否已存在
		$sql = "select `Id`
					from xt_powers_type
					where `name` = '{$data['name']}'";
		$powers_type = db::select($sql);
		if(!empty($powers_type)){
			return '该权限类别已存在';
		}
		
		//添加权限类别
		$sql = "insert ignore into xt_powers_type(`name`)
							values ('{$data['name']}')";
		$add = db::insert($sql);
//		$add = db::table('powers_type')->insertGetId($arr);
		if($add){
			return 1;
		}else{
			return '新增失败';
		}
	}
	
	/**
	 * 编辑权限类别
	 * @param Id 权限类别id
	 * @param name 权限类别名称
	 */
	public function powers_type_update($data){
		//检验权限分类是否已存在
		$sql = "select `Id`
						from xt_powers_type
						where `name`='{$data['name']}' and `Id`!={$data['Id']}";
		$powertype_find = db::select($sql);
		if(!empty($powertype_find)){
			return '该权限类别已存在';
		}
		
		$sql = "update xt_powers_type
                    set `name`='{$data['name']}'
                    where `Id`={$data['Id']}";
		$update = db::update($sql);
		if($update !== false){
			return 1;
		}else{
			return '编辑失败';
		}
	}
	
	/**
	 * 权限列表
	 * @param name 权限名称
	 * @param identity 权限标识
	 * @param type_id 权限类别id
	 * @param type 类型：1查询、2操作
	 */
	public function powers_list($data){
		$where = "1=1";
		//name 权限名称
		if(!empty($data['name'])){
			$where .= " and `name` like '%{$data['name']}%'";
		}
		//identity 权限标识
		if(!empty($data['identity'])){
			$where .= " and identity='{$data['identity']}'";
		}
		//type_id 权限分类id
		if(!empty($data['type_id'])){
			$where .= " and type_id={$data['type_id']}";
		}
		//type 类型：1查询、2操作
		if(!empty($data['type'])){
			$where .= " and `type`={$data['type']}";
		}
		$limit = "";
		if((!empty($data['page'])) and (!empty($data['page_count']))){
			$limit = " limit ".($data['page']-1)*$data['page_count'].",{$data['page_count']}";
		}
		
		$sql = "select SQL_CALC_FOUND_ROWS p.*,
						(select `name` from xt_powers_type where `Id`=p.type_id) as powertype_name
					from xt_powers p
					where {$where}";
		$list = json_decode(json_encode(db::select($sql)),true);
		////总条数
		$count = db::select("SELECT FOUND_ROWS() as total");
		
		$return['list'] = $list;
		$return['total'] = $count[0]->total;
		return $return;
	}
	
	/**
	 * 新增权限
	 * @param name 权限名称
	 * @param identity 权限标识
	 * @param type_id 权限类别id
	 * @param type 类型：1查询、2操作
	 */
	public function powers_add($data){
		//验证 权限名称、权限标识 唯一
		$sql = "select *
					from xt_powers
					where `name`='{$data['name']}' or `identity`='{$data['identity']}'";
		$powers_find = json_decode(json_encode(db::select($sql)),true);
		if(!empty($powers_find)){
			if($powers_find[0]['name'] == $data['name']){
				return '该权限已存在';
			}else if($powers_find[0]['identity'] == $data['identity']){
				return '权限标识已被使用';
			}
		}
		
		//新增
		$sql = "insert into xt_powers(`name`, `identity`, `type_id`, `type`)
					values ('{$data['name']}', '{$data['identity']}', {$data['type_id']}, {$data['type']})";
		$add = db::insert($sql);
		if($add){
			return 1;
		}else{
			return '新增失败';
		}
	}
	
	/**
	 * 权限信息
	 * @param power_id 权限id
	 */
    public function powers_info($data){
        $where = "1=1";
        //power_id 权限id
        if(!empty($data['power_id'])){
            $where .= " and `Id`={$data['power_id']}";
        }
		
        //权限信息
        $sql = "select * from powers where {$where}";
        $find = json_decode(json_encode(db::select($sql)),true);
        if(is_array($find)){
            return $find;
        }else{
            return '获取失败';
        }
    }
	
	/**
	 * @apram Id 权限id
	 * @param name 权限名称
	 * @param identity 权限标识
	 * @param type_id 权限类别id
	 * @param type 类型：1查询、2操作
	 */
	public function powers_update($data){
		//验证 权限名称、权限标识 唯一
		$sql = "select *
					from xt_powers
					where `Id`!={$data['Id']} and (`name`='{$data['name']}' or `identity`='{$data['identity']}')";
		$powers_find = json_decode(json_encode(db::select($sql)),true);
		if(!empty($powers_find)){
			if($powers_find[0]['name'] == $data['name']){
				return '该权限已存在';
			}else if($powers_find[0]['identity'] == $data['identity']){
				return '权限标识已被使用';
			}
		}
		
		
		$set = "";
		//name 权限名称
		if(!empty($data['name'])){
			$set .= ",`name`='{$data['name']}'";
		}
		//identity 权限标识
		if(!empty($data['identity'])){
			$set .= ",`identity`='{$data['identity']}'";
		}
		//type_id 权限分类id
		if(!empty($data['type_id'])){
			$set .= ",`type_id`={$data['type_id']}";
		}
		//type 类型：1查询、2操作
		if(!empty($data['type'])){
			$set .= ",`type`={$data['type']}";
		}
		
		if(empty($set)){
			return '修改值为空';
		}
		$set = substr($set,1);
		
		$sql = "update xt_powers
                    set {$set}
                    where `Id`={$data['Id']}";
		$update = db::update($sql);
		if($update !== false){
			return 1;
		}else{
			return '编辑失败';
		}
	}
	
	/**
	 * 删除权限
	 * @param Id 权限id
	 */
	public function powers_del($data){
		$where = "1=1";
		//Id 权限id
		if(!empty($data['Id'])){
			$where .= " and p.Id={$data['Id']}";
		}
		
		if($where == '1=1'){
			return '删除条件为空';
		}
		
		//权限表、角色权限关联表
		$sql = "delete p, prj
					from xt_powers p
					left join xt_powers_role_join prj on prj.power_id = p.Id
					where {$where}";
		$del = db::delete($sql);
		if($del){
			return 1;
		}else{
			return '删除失败';
		}
	}
		
		
		
	
	
















}//结束符