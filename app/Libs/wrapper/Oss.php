<?php
namespace App\Libs\anzhiyun;

require (app_path().'/libs/oss/autoload.php');

class Oss
{
    //OSSID
    private $accessid = "LTAIxSDwww8gQTx4" ;
    //OSSkey
    private $accesskey = "4Fx0ZkJqeypy88ULerw275L1rSGEuO";
    //OSS外域名
    private $endpoint = "oss-cn-hangzhou.aliyuncs.com";
    //OSS buket名
    private $bucket = "azyun2";
    //图片大小
    private $maxsize=10485760;
    //图片格式
    private $exts=array(
        'image/jpg',
        'image/JPG',
        'image/gif',
        'image/png',
        'image/jpeg',
        'image/JPEG',
    );


    //阿里云上传图片入口
    public function ossup($files=array(),$img_type){
        $bucketName =$this->bucket;
        $ossClient = new \OSS\OssClient($this->accessid, $this->accesskey, $this->endpoint, false);
        $web="http://".$bucketName.".".$this->endpoint."/";
        $rs=$this->ossUpPicAll($files,$img_type,$ossClient,$bucketName,$web);
        return $rs;
    }


    /* OSS批量上传图片
    *
    * $fFiles $_FILES 中的参数
    * $img_type 图片存放文件名
    */
    public function ossUpPicAll($fFiles=array(),$img_type,$ossClient,$bucketName,$web){
        foreach($fFiles as $key=>$val){
            $fname=$val['name'];
            $fup_n=$val['tmp_name'];
            $file_n=time().'_'.(rand(100,999)*rand(1,999));
            if($img_type){
                $object = "{$img_type}/{$file_n}.{$fname}";//目标文件名
            }else{
                $object = "{$file_n}.{$fname}";//目标文件名
            }
            if (is_null($ossClient)) exit(1);
            $ossClient->uploadFile($bucketName, $object, $fup_n);
            $back[$key]["url"]=$web.$object;
            if(!empty($val['sort'])){
                $back[$key]["sort"]=$val['sort'];
            }
        }
        return $back;
    }

    /* OSS图片批量删除
    *
    * 参数1 可以是单张图片路径或多张
    */
    public function OssDel($objects){
        $bucketName =$this->bucket;
        $ossClient = new \OSS\OssClient($this->accessid, $this->accesskey, $this->endpoint, false);
        $options = array(
            'quiet' => true,
        );
        $return=$ossClient->deleteObjects($bucketName,$objects,$options);
        return $return;
    }

    //64位上传
    public function oss_upload($oss_path,$content){
        $bucketName =$this->bucket;
        $ossClient = new \OSS\OssClient($this->accessid,$this->accesskey,$this->endpoint,false);
        $ossClient->putObject($bucketName,$oss_path,$content);

        $bucket = $this->bucket;
        $end_point = $this->endpoint;
        return  'http://'.$bucket.'.'.$end_point.'/'.$oss_path;
    }

    /**
     * coder:小黑
     * date:2018-08-17
     * desc:JavaScript图片直传OSS
     * @param:
     * @return:
     */
    public function getSignature(){
        $id= 'LTAId4Z7ln0BFcNQ';
        $key= 'zJKCJXwx3TdKeiEMeA4m3HE8FdAmIh';
        $host = 'http://azydemo.oss-cn-hangzhou.aliyuncs.com';

        $now = time();
        $expire = 30; //设置该policy超时时间是10s. 即这个policy过了这个有效时间，将不能访问
        $end = $now + $expire;
        $expiration = $this->gmt_iso8601($end);

        $dir = 'user-dir/';

        //最大文件大小.用户可以自己设置
        $condition = array(0=>'content-length-range', 1=>0, 2=>1048576000);
        $conditions[] = $condition;

        //表示用户上传的数据,必须是以$dir开始, 不然上传会失败,这一步不是必须项,只是为了安全起见,防止用户通过policy上传到别人的目录
        $start = array(0=>'starts-with', 1=>'$key', 2=>$dir);
        $conditions[] = $start;


        $arr = array('expiration'=>$expiration,'conditions'=>$conditions);
        //echo json_encode($arr);
        //return;
        $policy = json_encode($arr);
        $base64_policy = base64_encode($policy);
        $string_to_sign = $base64_policy;
        $signature = base64_encode(hash_hmac('sha1', $string_to_sign, $key, true));

        $response = array();
        $response['accessid'] = $id;
        $response['host'] = $host;
        $response['policy'] = $base64_policy;
        $response['signature'] = $signature;
        $response['expire'] = $end;
        //这个参数是设置用户上传指定的前缀
        $response['dir'] = $dir;
        return $response;
        //echo json_encode($response);
    }

    //获取超时时间
    function gmt_iso8601($time) {
        $dtStr = date("c", $time);
        $mydatetime = new DateTime($dtStr);
        $expiration = $mydatetime->format(DateTime::ISO8601);
        $pos = strpos($expiration, '+');
        $expiration = substr($expiration, 0, $pos);
        return $expiration."Z";
    }

}