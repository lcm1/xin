<?php

namespace App\Libs\wrapper;


use App\Models\AmazonCategory;
use App\Models\BaseModel;
use App\Models\Shop;
use App\Models\UserModel;
use Illuminate\Support\Facades\DB;

use function GuzzleHttp\Psr7\str;

class AmazonOrderItems extends Comm
{

    /**
     * @param $AmazonOrderItemsModel  模型
     * @param $param         post,get提交的参数
     */
    public function pageSearch($AmazonOrderItemsModel, $param)
    {

        if (isset($param['search_type']) && $param['search_type']== 1) {

            if (isset($param['sku']) && $param['sku'] != '') {
                $AmazonOrderItemsModel = $AmazonOrderItemsModel->where('seller_sku', $param['sku']);
            }
            if (isset($param['amazon_order_id']) && $param['amazon_order_id'] != '') {
                $AmazonOrderItemsModel = $AmazonOrderItemsModel->where('amazon_order_id', $param['amazon_order_id']);
            }
            if (isset($param['spu']) && $param['spu'] != '') {
                $baseMode = new BaseModel();
                $spu_id = $baseMode->GetSpuId($param['spu']);
                $AmazonOrderItemsModel = $AmazonOrderItemsModel->where('spu_id', $spu_id);
            }
            if (isset($param['asin']) && $param['asin'] != '') {
                $AmazonOrderItemsModel = $AmazonOrderItemsModel->where('asin', $param['asin']);
            }

        } else {

            if (isset($param['sku']) && $param['sku'] != '') {
                $AmazonOrderItemsModel = $AmazonOrderItemsModel->where('seller_sku', 'like', "" . $param['sku'] . "%");
            }
            if (isset($param['amazon_order_id']) && $param['amazon_order_id'] != '') {
                $AmazonOrderItemsModel = $AmazonOrderItemsModel->where('amazon_order_id', 'like', "" . $param['amazon_order_id'] . "%");
            }
            if (isset($param['spu']) && $param['spu'] != '') {
                $AmazonOrderItemsModel = $AmazonOrderItemsModel->where('spu', 'like', "" . $param['spu'] . "%");
            }
            if (isset($param['asin']) && $param['asin'] != '') {
                $AmazonOrderItemsModel = $AmazonOrderItemsModel->where('asin', 'like', "" . $param['asin'] . "%");
            }

        }

        if (isset($param['user_id']) && $param['user_id'] > 0) {
            $AmazonOrderItemsModel = $AmazonOrderItemsModel->where('user_id', $param['user_id']);
        }
        if (isset($param['category_id']) && $param['category_id'] > 0) {
            $AmazonOrderItemsModel = $AmazonOrderItemsModel->where('category_id', $param['category_id']);
        }
        if (isset($param['shop_id']) && $param['shop_id'] > 0) {
            $AmazonOrderItemsModel = $AmazonOrderItemsModel->where('shop_id', $param['shop_id']);
        }
        if (isset($param['param_start_time']) && $param['param_start_time'] != '') {
           
            $AmazonOrderItemsModel = $AmazonOrderItemsModel->where('amazon_time', '>=', $param['param_start_time']." 00:00:00");
        }
        if (isset($param['param_end_time']) && $param['param_end_time'] != '') {
            $AmazonOrderItemsModel = $AmazonOrderItemsModel->where('amazon_time', '<=', $param['param_end_time']." 23:59:59");
        }

        return $AmazonOrderItemsModel;

    }

    /**
     * 处理输出的数据结果
     * @param $AmazonPageList
     * @param array $append
     */
    public function dealResult($AmazonPageList, $append = array())
    {

        foreach ($AmazonPageList as $item) {
            if ($item['shop_id'] > 0) {
                $shop = Shop::where('id', $item['shop_id'])->first();
                if (isset($shop['shop_name'])) {
                    $item['shop_name'] = $shop['shop_name'];
                } else {
                    $item['shop_name'] = '';
                }
            } else {
                $item['shop_name'] = '';
            }

            if ($item['category_id'] > 0) {

                $AmazonCategory = AmazonCategory::where('id', $item['category_id'])->first();

                if (isset($AmazonCategory['product_typename'])) {
                    $item['category'] = $AmazonCategory['product_typename'];
                } else {
                    $item['category'] = '';
                }
            } else {
                $item['category'] = '';
            }

            if ($item['user_id'] > 0) {
                $User = UserModel::where('id', $item['user_id'])->first();
                if (isset($User['account'])) {
                    $item['user'] = $User['account'];
                } else {
                    $item['user'] = '';
                }
            }

        }
    }
}
