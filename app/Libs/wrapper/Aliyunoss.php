<?php
namespace App\Libs\wrapper;
require (base_path().'/vendor/aliyuncs/oss-sdk-php/autoload.php');

class Aliyunoss
{
///////////////////////////////// 	  阿里云对象存储OSS		///////////////////////////////
	
	var $accessKeyId='LTAIU7LKXPgfjLPB';
	var $accessKeySecret='tbU7205TYyqzbcqpXOzoa7MXGxp6e5';
	var $endpoint='oss-cn-shanghai.aliyuncs.com';//地域节点
	var $bucket='zity';//存储空间
	var $ossclient='';//初始化
	
	
	// 初始化
	public function __construct(){
//		parent::__construct();
		
		$this->ossclient=new \OSS\OssClient($this->accessKeyId,$this->accessKeySecret,$this->endpoint);
	}
	
	/**
     * oss上传图片
     * 说明：批量
     * 接收值：array(图片流数组)
     * 返回值：array(添加成功的图片的URL);提示语句
     * */
    public function oss_upimg($data){
		$imgs = array();//临时数组，存储添加成功的图片的url
		$num = 0;//存储添加成功数
		foreach($data as $k=>$v){
			$bucket = $this->bucket;//存储空间
			$object = time() ."_". $num++ .'.'. explode('/', $v['type'])[1];//文件名称
			$file = $v['tmp_name']; //本地文件路径
			$uploadFile = $this->ossclient->uploadFile($bucket, $object, $file);
			if(!empty($uploadFile['info']['url'])){
				$imgs[$k] = $uploadFile['info']['url'];
			}else{
				break;
			}
		}
		if(count($imgs)==count($data)){
			return $imgs;
		}else{
			return 'logic:添加失败';
		}
    	
    }
	
	/**
	 * oss删除文件
	 * 说明：批量
	 * 接收值：字符串例如：15279933900.jpeg,15279933901.jpeg
	 * 返回值：bool(删除成功);提示语句
	 * */
	public function oss_delimg($delimg){
		$delimg = explode(',', $delimg);
		$num=0;//存储删除成功数
		foreach($delimg as $k=>$v){
			$bucket = $this->bucket;//存储空间
			$dei = explode('/', $v);
			$object = $dei[3];//文件名称
			$deleteObject = $this->ossclient->deleteObject($bucket, $object);
//			return $deleteObject;
			if(!empty($deleteObject['info']['url'])){
				$num++;
			}else{
				break;
			}
		}
		if($num==count($delimg)){
			return true;
		}else{
			return 'logic:删除失败';
		}
		
	}
	
	/**
	 * coder:小黑
	 * date:2018-08-17
	 * desc:JavaScript图片直传OSS
	 * @param:
	 * @return:
	 */
	public function getSignature(){
		$id= 'LTAId4Z7ln0BFcNQ';
		$key= 'zJKCJXwx3TdKeiEMeA4m3HE8FdAmIh';
		$host = 'http://azydemo.oss-cn-hangzhou.aliyuncs.com';
		
		$now = time();
		$expire = 30; //设置该policy超时时间是10s. 即这个policy过了这个有效时间，将不能访问
		$end = $now + $expire;
		$expiration = $this->gmt_iso8601($end);
		
		$dir = 'user-dir/';
		
		//最大文件大小.用户可以自己设置
		$condition = array(0=>'content-length-range', 1=>0, 2=>1048576000);
		$conditions[] = $condition;
		
		//表示用户上传的数据,必须是以$dir开始, 不然上传会失败,这一步不是必须项,只是为了安全起见,防止用户通过policy上传到别人的目录
		$start = array(0=>'starts-with', 1=>'$key', 2=>$dir);
		$conditions[] = $start;
		
		
		$arr = array('expiration'=>$expiration,'conditions'=>$conditions);
		//echo json_encode($arr);
		//return;
		$policy = json_encode($arr);
		$base64_policy = base64_encode($policy);
		$string_to_sign = $base64_policy;
		$signature = base64_encode(hash_hmac('sha1', $string_to_sign, $key, true));
		
		$response = array();
		$response['accessid'] = $id;
		$response['host'] = $host;
		$response['policy'] = $base64_policy;
		$response['signature'] = $signature;
		$response['expire'] = $end;
		//这个参数是设置用户上传指定的前缀
		$response['dir'] = $dir;
		return $response;
		//echo json_encode($response);
	}
	
	
	
	
	
	
	
}
?>