<?php
namespace App\Libs\wrapper;
use Illuminate\Support\Facades\DB;
class Login extends Comm{
////////////////////////////////////////////////  登录

//////////////////////// 管理员登录
	/**
     * 登录
     * @param account 账号
	 * @param password 密码
     */
    public function login($data){
        //查询用户信息
        $sql = "select *
                    from users
                    where `state`=1 and `account`='{$data['account']}' and password='".md5($data['password'])."'";
        $user_find = json_decode(json_encode(db::select($sql)),true);
//        if ($data['account'] == '蔡碧云'){
//            var_dump($user_find);exit();
//        }
        if(empty($user_find)){
            return '账号或密码错误';
        }

        if (!(isset($data['source']) && $data['source'] == 'mobile')) {
            //更新token
            $token = $this->generate_token();//获取token

            $sql = "update users
                    set `token`='{$token}'
                    where `Id`='{$user_find[0]['Id']}'";

            $update = db::update($sql);
            if (!$update) {
                return '更新token失败';
            }
        } else {
            $token = $user_find[0]['token'];
        }
	
	    $user_find[0]['token'] = $token;
	    return $user_find[0];
    }
	
	/**
	 * 自动登录
	 * @param account 账号
	 */
	public function login_zd($data){
		//查询用户信息
		$sql = "select *
                    from users
                    where `account`='{$data['account']}'
                    limit 1";
		$user_find = json_decode(json_encode(db::select($sql)),true);
		if(empty($user_find)){
			// 新增用户
			$arr = array();
			$arr['account'] = $data['account'];
			$arr['password'] = md5(123456);
			$arr['heard_img'] = 'http://q.zity.cn/headimg.jpg';
			$arr['create_time'] = date('Y-m-d H:i:s', time());
			$user_add = db::table('users')->insertGetId($arr);
			if(!$user_add){
				return '新增用户失败';
			}
			
			// 添加权限
			$sql = "insert into xt_role_user_join(`user_id`, `role_id`)
						values ({$user_add}, 3)";
			$role_user_add = db::insert($sql);
			
			$user_find[0]['Id'] = $user_add;
			$user_find[0]['account'] = $data['account'];
			$user_find[0]['heard_img'] = 'http://q.zity.cn/headimg.jpg';
			$user_find[0]['sex'] = 1;
		}else if($user_find[0]['state'] == 2){
			return '用户已被禁用，无法登录';
		}
		
		//更新token
		$token = $this->generate_token();//获取token
		$sql = "update users
                    set `token`='{$token}'
                    where `Id`='{$user_find[0]['Id']}'";
		$update = db::update($sql);
		if(!$update){
			return '更新token失败';
		}
		
		$user_find[0]['token'] = $token;
		return $user_find[0];
	}
	
	/**
	 * 注册
	 * @parma account 账号
	 * @param password 密码
	 * @paran phone 联系电话
	 */
	public function register($data){
		//用户唯一性
		$sql = "select `Id`, `state`
					from `users`
					where `account` = '{$data['account']}'";
		$user_find = json_decode(json_encode(db::select($sql)),true);
		if(!empty($user_find)){
			return '该账号已被注册';
		}
		
		//开启事务
		db::beginTransaction();
		
		//新增用户
		$arr = array();
		$arr['account'] = $data['account'];
		$arr['password'] = md5($data['password']);
		$arr['heard_img'] = "http://q.zity.cn/headimg.jpg";
		$arr['phone'] = $data['phone'];
		$arr['create_time'] = date('Y-m-d H:i:s', time());
		$user_add = db::table('users')->insertGetId($arr);
		if(!$user_add){
			db::rollback();// 回调
			return '新增用户失败';
		}
		
		// 添加权限
		$sql = "insert into xt_role_user_join(`user_id`, `role_id`)
						values ({$user_add}, 3)";
		$role_user_add = db::insert($sql);
		if($user_add && $role_user_add){
			db::commit();// 确认
			return 1;
		}else{
			db::rollback();// 回调
			return '新增失败';
		}
	}
	





}//类结束符