<?php
//============================================================+
// File name   : example_002.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 002 for TCPDF class
//               Removing Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Removing Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->setCreator(PDF_CREATOR);
$pdf->setAuthor('Nicola Asuni');
$pdf->setTitle('TCPDF Example 002');
$pdf->setSubject('TCPDF Tutorial');
$pdf->setKeywords('TCPDF, PDF, example, test, guide');

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->setDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->setMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

// set auto page breaks
$pdf->setAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->setFont('times', 'BI', 20);

// add a page
$pdf->AddPage();

// set some text to print
$txt = <<<EOD
<div style="width:800px;margin:0 auto;">
    <div style="margin:0 auto;width:100%;text-align: center;font-size:30px;">
        个人信息查询授权书V1.0
    </div>
    <div style="height: 30px;"></div>
    <div style="font-size:30px;">
        厦门思科迪电子商务有限公司：
    </div>
    <div style="height: 30px;"></div>
    <div style="font-size:20px;line-height: 40px;">
        <p style="text-indent: 50px;">
            一、本人同意并授权：<span style="font-weight: bold;">贵司采集本人身份信息、公民身份号码、学历信息、工作经历体检信息、家庭成员、紧急联络人、通信方式等信息。</span>
        </p>
        <p>二、本人同意并授权：<span style="font-weight: bold;">贵司及贵司合作的第三方公司在办理涉及本人的业务时，有权向信息基础数据库及其他依法设立的信息提供机构查询、打印、保存、评估本人的信息，并用于下列用途：</span>
        </p>
        <p>（一）入职前、中、后的个人信息调查；</p>
        <p>（二） 签订劳动合同；</p>
        <p>（三）个人的劳动所得经由银行发放；</p>
        <p>（四）公司开展的活动中拍摄的照片或者视频资料，用于对外宣传；</p>
        <p>（五）公司和公司投资及公司合作的第三方公司合法经营范围内的其他业务中使用。</p>
        <p>三、本授权书自签署之日起生效。</p>
        <p style="font-weight: bold;">四、本授权书签署后，未经本人向公司正式提出撤销授权，该授权持续有效。</p>
        <p style="font-weight: bold;">本人已知悉本授权书全部内容的含义以及由此产生的法律效力，自愿作出以上授权。本授权书是本人真实意思表示，本人同意承担由此所产生的一切法律后果。</p>
    </div>

    <div style="height:50px;"></div>
    <div  style="width:100%;font-size: 22px;padding-left: 50px;">
        <div style="width:100%;margin-top:20px; ">
            <div style="float:left;">
                授权人姓名：
            </div>
            <div style="float:left;border-bottom: 1px solid #000000;width:242px;">
               &nbsp;
            </div>
            <div style="clear:both;"></div>
        </div>
        <div  style="width:100%;margin-top:20px;">
            <div style="float:left;">
                授权人证件类型：
            </div>
            <div style="float:left;border-bottom: 1px solid #000000;width:200px;text-align: center">
                身份证
            </div>
            <div style="clear:both;"></div>
        </div>
        <div  style="width:100%;margin-top:20px;">
            <div style="float:left;">
                授权人证件号码：
            </div>
            <div style="float:left;border-bottom: 1px solid #000000;width:200px;">
                &nbsp;
            </div>
            <div style="clear:both;"></div>
        </div>
        <div  style="width:100%;margin-top:20px;">
            <div style="float:left;">
                授权日期：
            </div>
            <div style="float:left;border-bottom: 1px solid #000000;width:78px;">
                &nbsp;
            </div>
            <div style="float:left;border-bottom: 1px solid #000000;width:30px;">
                年
            </div>
            <div style="float:left;border-bottom: 1px solid #000000;width:50px;">
                &nbsp;
            </div>
            <div style="float:left;border-bottom: 1px solid #000000;width:30px;">
                月
            </div>
            <div style="float:left;border-bottom: 1px solid #000000;width:50px;">
                &nbsp;
            </div>
            <div style="float:left;border-bottom: 1px solid #000000;width:30px;">
                日
            </div>
            <div style="clear:both;"></div>
        </div>
    </div>
    <div style="height:50px;"></div>
</div>
EOD;

// print a block of text using Write()
$pdf->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('test1.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
