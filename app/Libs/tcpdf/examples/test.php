<?php
//============================================================+
// File name   : example_002.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 002 for TCPDF class
//               Removing Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Removing Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->setCreator(PDF_CREATOR);
$pdf->setAuthor('Nicola Asuni');
$pdf->setTitle('TCPDF Example 002');
$pdf->setSubject('TCPDF Tutorial');
$pdf->setKeywords('TCPDF, PDF, example, test, guide');

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->setDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->setMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

// set auto page breaks
$pdf->setAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
    require_once(dirname(__FILE__) . '/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->setFont('stsongstdlight', '', 20);

// add a page
$pdf->AddPage();

// set some text to print
$html = '<div style="text-indent:200px;">个人信息查询授权书V1.0</div>';
$pdf->writeHTML($html, false, false, true, false, 'center');

$html = '<div>&nbsp;</div>';
$pdf->writeHTML($html, false, false, true, false, '');
$html = ' <div style="font-size:16px;line-height:38px;margin-top:10px;">
厦门思科迪电子商务有限公司：<br>
        &nbsp;&nbsp;&nbsp;&nbsp;一、本人同意并授权： <strong>贵司采集本人身份信息、公民身份号码、学历信息、工作经历体检信息、家庭成员、紧急联络人、通信方式等信息。</strong>
        二、本人同意并授权：<strong>贵司及贵司合作的第三方公司在办理涉及本人的业务时，有权向信息基础数据库及其他依法设立的信息提供机构查询、打印、保存、评估本人的信息，并用于下列用途：</strong>
（一）入职前、中、后的个人信息调查；<br>
（二） 签订劳动合同；<br>
（三）个人的劳动所得经由银行发放；
        （四）公司开展的活动中拍摄的照片或者视频资料，用于对外宣传；
        （五）公司和公司投资及公司合作的第三方公司合法经营范围内的其他业务中使用。
        <b>三、本授权书自签署之日起生效。</b>
        <b>四、本授权书签署后，未经本人向公司正式提出撤销授权，该授权持续有效。</b>
        <b>本人已知悉本授权书全部内容的含义以及由此产生的法律效力，自愿作出以上授权。本授权书是本人真实意思表示，本人同意承担由此所产生的一切法律后果。</b>
   </div>
';

// print a block of text using Write()
$pdf->writeHTML($html, false, false, true, false, '');
//
//$file = fopen('C:\www\tcpdf\examples\1.jpg',"r");
//$imgdata=fread($file,filesize('C:\www\tcpdf\examples\1.jpg'));
//fclose($file);
//
//exit();

//$pdf->Image('@'.$imgdata);


$pdf->setJPEGQuality(175);
$pdf->Image('http://local.tcpdf.com/examples/2.jpg', 15, 230);



$pdf->Image('http://local.tcpdf.com/examples/1.png', 45, 225,30,10,'PNG',true);  //签名
$pdf->Image('http://local.tcpdf.com/examples/6.png', 55, 245,30,10,'PNG',true);   //身份证

$pdf->Image('http://local.tcpdf.com/examples/2.png', 42, 255,30,10,'PNG',true);  //年
$pdf->Image('http://local.tcpdf.com/examples/4.png', 58, 255,30,10,'PNG',true);  //年
$pdf->Image('http://local.tcpdf.com/examples/5.png', 70, 255,30,10,'PNG',true);  //年
//
//$pdf->Image('http://local.tcpdf.com/examples/2.png', 170, 230);
//$pdf->Image('http://local.tcpdf.com/examples/3.png', 190, 230);

//Close and output PDF document
$pdf->Output('test.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
