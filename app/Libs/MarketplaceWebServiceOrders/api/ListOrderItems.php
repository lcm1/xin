<?php

require_once '.config.inc.php';
include_once 'Base.php';

class ListOrderItems extends Base
{
    public static function getData($auth, $orderId)
    {

        $serviceUrl = "https://mws.amazonservices.com/Orders/2013-09-01";
        $serviceUrl_jp = "https://mws.amazonservices.jp/Orders/2013-09-01";
        $serviceUrl_eu = "https://mws-eu.amazonservices.com/Orders/2013-09-01";
        $config = array(
            'ServiceURL' => $serviceUrl,
            'ProxyHost' => null,
            'ProxyPort' => -1,
            'ProxyUsername' => null,
            'ProxyPassword' => null,
            'MaxErrorRetry' => 3,
        );
        $service = new MarketplaceWebServiceOrders_Client(
            $auth['AWS_ACCESS_KEY_ID'],
            $auth['AWS_SECRET_ACCESS_KEY'],
            APPLICATION_NAME,
            APPLICATION_VERSION,
            $config);
        $request = new MarketplaceWebServiceOrders_Model_ListOrderItemsRequest();
        $request->setSellerId($auth['MERCHANT_ID']);
        $request->setAmazonOrderId($orderId);

        $request->setMWSAuthToken($auth['MWSAuthToken']);
        try {
            $response = $service->ListOrderItems($request);

            $dom = new DOMDocument();
            $dom->loadXML($response->toXML());
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;
            // echo("ResponseHeaderMetadata: " . $response->getResponseHeaderMetadata() . "\n");
            $data=$dom->saveXML();
            $xml = simplexml_load_string($data);
            $data1 = json_decode(json_encode($xml),TRUE);
            var_dump($data1);

        } catch (MarketplaceWebServiceOrders_Exception $ex) {
            echo("Caught Exception: " . $ex->getMessage() . "\n");
            echo("Response Status Code: " . $ex->getStatusCode() . "\n");
            echo("Error Code: " . $ex->getErrorCode() . "\n");
            echo("Error Type: " . $ex->getErrorType() . "\n");
            echo("Request ID: " . $ex->getRequestId() . "\n");
            echo("XML: " . $ex->getXML() . "\n");
            echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
        }

    }
}




















