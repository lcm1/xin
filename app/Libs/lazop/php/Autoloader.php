<?php

class Autoloader{
  
  /**
     * Autoloade Class in SDK.
     * PS: only load SDK class
     * @param string $class class name
     * @return void
     */
    public static function autoload($class) {
        $name = $class;
        if(false !== strpos($name,'\\')){
          $name = strstr($class, '\\', true);
        }
//        var_dump($class);exit;
//        $filename = LAZOP_AUTOLOADER_PATH."/lazop/".$name.".php";
        $filename = "../app/Libs/lazop/php/lazop/".$name.".php";
    //var_dump($filename);exit;
       // var_dump(is_file($filename));die;
        if(is_file($filename)) {

            include $filename;
            return;
        }
    }
    
}

spl_autoload_register('Autoloader::autoload');
?>