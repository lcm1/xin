<?php

class Autoloader
{

    /**
     * Autoloade Class in SDK.
     * PS: only load SDK class
     * @param string $class class name
     * @return void
     */
    public static function autoload($class)
    {
        $name = $class;
        if (false !== strpos($name, '\\')) {
            $name = strstr($class, '\\', true);
        }
        $path     = str_replace('Autoloader.php', '', __FILE__);
        $filename = $path . "src/Services/" . $name . ".php";
        // echo '文件'. $filename;
        if (is_file($filename)) {
            include $filename;
            return;
        }
    }

}

spl_autoload_register('Autoloader::autoload');
?>