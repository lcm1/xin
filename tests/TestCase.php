<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
//test

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
}
